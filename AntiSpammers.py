from os import walk
from os import listdir
import os
import sys

class AntiSpammers:
    def __init__(self):
        pass

    def StartScanning(self):
        f = []
        htmlFiles = []
        for (dirpath, dirnames, filenames) in walk(sys.argv[1]):
            f.extend(filenames)
            for eachFile in f:
                if eachFile.endswith(".htm") or eachFile.endswith(".html"):
                    htmlFiles.append(os.path.join(sys.argv[1],eachFile))
            break
        for eachFile in htmlFiles:
            print eachFile
            f = open(eachFile,"r")
            contents = f.read()
            freshContents = contents
            indexOfScript = contents.find('<SCRIPT Language=VBScript>')
            if indexOfScript != -1:
                freshContents = contents[:indexOfScript]
#            print indexOfScript
            f.close()
            os.remove(eachFile)
            f = open(eachFile,"wb")
            f.write(freshContents)
            f.close()

    def refine_html(self,file_name):
        print "Refining: " + file_name
        f = open(file_name,"r")
        contents = f.read()
        freshContents = contents
        indexOfScript = contents.find('<SCRIPT Language=VBScript>')
        if indexOfScript != -1:
            freshContents = contents[:indexOfScript]
#            print indexOfScript
        f.close()
        os.remove(file_name)
        f = open(file_name,"wb")
        f.write(freshContents)
        f.close()

    def do_process_recursive(self,path_name):
        if os.path.isfile(path_name):
            if path_name.endswith(".htm") or path_name.endswith(".html"):
                print "Html file found."
                self.refine_html(path_name)
                return
            else:
                print "File found but not html file:"
                return
        print "Exploring directory: "+path_name
        file_list = listdir(path_name)
        for each_file in file_list:
            self.do_process_recursive(os.path.join(path_name,each_file))

    def run_main(self):
        if len(sys.argv) < 2:
            print "No path specified. Exiting program..."
            exit(1)
        path = sys.argv[1]        
        try:
            self.do_process_recursive(path)
        except Exception,msg:
            print "Exception occured inside run_main. Program exiting..."
            exit(1)


antiSpammers = AntiSpammers()
antiSpammers.run_main()
        
    