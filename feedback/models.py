from django.db import models
from core.generics.models import GenericModel

__author__ = 'Sohel'

class Feedback(GenericModel):
    email = models.CharField(max_length=500, blank=True)
    name = models.CharField(max_length=500)
    report_type = models.CharField(max_length=100)
    message = models.TextField()

    class Meta:
        db_table = 'feedback'

class UserFeedback(Feedback):

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.report_type = self.__class__.__name__
        super(GenericModel,self).save(force_insert, force_update, using,update_fields)


    class Meta:
        proxy = True

class BugReport(Feedback):

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.report_type = self.__class__.__name__
        super(GenericModel,self).save(force_insert, force_update, using,update_fields)

    class Meta:
        proxy = True

class PaymentIssue(Feedback):

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.report_type = self.__class__.__name__
        super(GenericModel,self).save(force_insert, force_update, using,update_fields)

    class Meta:
        proxy = True

class LessonIssue(Feedback):

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.report_type = self.__class__.__name__
        super(GenericModel,self).save(force_insert, force_update, using,update_fields)

    class Meta:
        proxy = True
