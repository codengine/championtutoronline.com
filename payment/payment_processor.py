from django.core.urlresolvers import reverse
from django.conf import settings
from core.library.email_scheduler import EmailScheduler
from core.library.email_template_util import EmailTemplateUtil
from core.models3 import PaymentLog, LiveLesson, WrittenLesson, Job
from notification.utils.notification_manager import NotificationManager
from payment.braintree_util import BrainTreePayment
from payment.models import PaymentMethod, SupportedCurrencies, MerchantAccount, Transaction

__author__ = 'Sohel'

class PaymentProcessor(object):
    def __init__(self):
        self.braintree_payment = BrainTreePayment()

    def get_users_credit_card(self,user_id):
        payment_methods = PaymentMethod.objects.filter(customer__user__id=user_id).order_by('is_primary')
        return payment_methods

    # def process_payment(self, student_id,transaction_amount):
    #     payment_methods = self.get_users_credit_card(student_id)
    #     for payment_method in payment_methods:
    #         success, transaction_id = self.braintree_payment.create_sale_transaction(payment_method.payment_method_token,transaction_amount,settlement=True)
    #         if success:
    #             return True

    def process_lesson_payment(self, lesson_id, lesson_type, transaction_amount, currency, base_payment=True, extra_time=0, payment_methods=[]):
        lesson_object = None
        if lesson_type == 'LIVE_LESSON':
            lesson_object = LiveLesson.objects.filter(pk=lesson_id)
            if lesson_object.exists():
                lesson_object = lesson_object.first()
            else:
                lesson_object = None
        else:
            lesson_object = WrittenLesson.objects.filter(pk=lesson_id)
            if lesson_object.exists():
                lesson_object = lesson_object.first()
            else:
                lesson_object = None
        SUPPORTED_CURRENCIES = [c[0] for c in SupportedCurrencies.objects.values_list("code")]
        if lesson_object and currency in SUPPORTED_CURRENCIES:
            mode = "SANDBOX" if settings.DEBUG else "LIVE"
            braintree_merchant_account_id = MerchantAccount.objects.get(mode=mode,currency=currency).braintree_user_id
            payer = lesson_object.job_set.first().payer
            if not payment_methods:
                payment_methods = self.get_users_credit_card(payer.pk)
            job = Job.objects.get(lesson_id=lesson_id)
            for payment_method in payment_methods:
                success, transaction_id = self.braintree_payment.create_sale_transaction(payment_method.payment_method_token,transaction_amount,currency,lesson_object.request_user.pk,settlement=True,merchant_account_id=braintree_merchant_account_id)
                if success:
                    if base_payment:
                        lesson_object.base_transaction_id = transaction_id
                        lesson_object.payment_status = 1 ###Payment Successful.
                        lesson_object.save()
                    else:
                        tx = Transaction.objects.get(pk=transaction_id)
                        lesson_object.transactions.add(tx)

                    payment_amount = transaction_amount

                    payment_process_email_html, text = EmailTemplateUtil.render_payment_processed_email(job.pk, lesson_object.pk, lesson_type, job.payer.champuser.fullname, payment_amount, 'USD')
                    if base_payment:
                        recipients = []
                        recipients += [ job.posted_by.email ]
                        recipients += [ lesson_object.request_user.email ]
                        target_users = [ job.posted_by.pk, lesson_object.request_user.pk ]
                        for other_user in job.other_students.all():
                            recipients += [ other_user.email ]
                            target_users += [ other_user.pk ]
                        email_object = {
                            "recipients": recipients,
                            'subject': 'Payment Processed Successfully',
                            'html_body': payment_process_email_html,
                            'text_body': text
                        }
                        EmailScheduler.place_to_queue(email_object)

                        for _u_id in target_users:
                            NotificationManager.push_notification("payment_processed",job.pk,lesson_object.request_user.pk,_u_id)
                    else:
                        recipients = [ payer.email ]
                        target_users = [ payer.pk ]
                        email_object = {
                            "recipients": recipients,
                            'subject': 'Payment Processed Successfully',
                            'html_body': payment_process_email_html,
                            'text_body': text
                        }
                        EmailScheduler.place_to_queue(email_object)

                        for _u_id in target_users:
                            NotificationManager.push_notification("payment_processed",job.pk,lesson_object.request_user.pk,_u_id)

                    # lesson_scheduled, text = EmailTemplateUtil.render_lesson_scheduled_email()
                    if lesson_type == 'LIVE_LESSON':
                        lesson_url = reverse("live_lesson", kwargs={ "pk": lesson_object.pk })
                    else:
                        lesson_url = reverse("written_lesson_request_view")+"?lesson=%s" % str(lesson_object.pk)

                    lesson_scheduled_email_receivers = [ lesson_object.user, lesson_object.request_user ]
                    lesson_scheduled_email_receivers += [ user for user in lesson_object.other_users.all() ]

                    if base_payment:
                        for receiver in lesson_scheduled_email_receivers:
                            lesson_scheduled_email, text = EmailTemplateUtil.render_lesson_scheduled_email(receiver.champuser.fullname,lesson_object.pk, lesson_object.type)
                            email_object = {
                                "recipients": [ receiver.email ],
                                'subject': 'Lesson Scheduled',
                                'html_body': lesson_scheduled_email,
                                'text_body': text
                            }
                            EmailScheduler.place_to_queue(email_object)
                        l_t = 0 if lesson_type == "LIVE_LESSON" else 1
                        for _u_id in target_users:
                            NotificationManager.push_notification("lesson_scheduled",job.pk,lesson_object.request_user.pk,_u_id,lesson_type=l_t)
                    else:
                        for receiver in lesson_scheduled_email_receivers:
                            lesson_extended_email, text = EmailTemplateUtil.render_lesson_time_extended_email(lesson_object.pk, lesson_object.type, receiver.champuser, extra_time)
                            email_object = {
                                "recipients": [ receiver.email ],
                                'subject': 'Lesson Time Extended',
                                'html_body': lesson_extended_email,
                                'text_body': text
                            }
                            EmailScheduler.place_to_queue(email_object)
                        l_t = 0 if lesson_type == "LIVE_LESSON" else 1
                        for _u_id in target_users:
                            NotificationManager.push_notification("lesson_time_extended",job.pk,lesson_object.request_user.pk,_u_id,lesson_type=l_t,extra_time=extra_time)
                    return True
            payment_fail_reason = ""
            if payment_methods.count() == 0:
                payment_fail_reason = "No payment method setup found"
            else:
                payment_fail_reason = "Invalid credit card details"
            if not lesson_object.base_transaction:
                NotificationManager.push_notification("payment_failed",job.pk,lesson_object.request_user.pk,job.payer.pk)

                lesson_scheduled_email, text = EmailTemplateUtil.render_payment_process_failed_email(job.pk, lesson_id, lesson_object.type, job.payer.champuser.fullname, transaction_amount, currency,payment_fail_reason)
                email_object = {
                    "recipients": [ job.payer.email ],
                    'subject': 'Payment Failed',
                    'html_body': lesson_scheduled_email,
                    'text_body': text
                }
                EmailScheduler.place_to_queue(email_object)


