from django.db import models
from django.contrib.auth.models import User
from core.enums import TransactionStatus
from core.generics.models import GenericModel

# Create your models here.

class SupportedPaymentMethod(GenericModel):
    name = models.CharField(max_length=255)
    short_name = models.CharField(max_length=10)

    class Meta:
        db_table=u'supported_payment_methods'

class BrainTreeCustomer(GenericModel):
    user = models.ForeignKey(User)
    braintree_customer_id = models.CharField(max_length=255, blank=True)

    class Meta:
        db_table = 'braintree_customer'

class PaymentMethod(GenericModel):
    customer = models.ForeignKey(BrainTreeCustomer)
    name = models.CharField(max_length=255, blank=True)
    payment_method_token = models.CharField(max_length=500)
    number_last_4digit = models.CharField(max_length=4)
    card_type = models.CharField(max_length=10)
    is_primary = models.IntegerField() ###1 means yes and 0 means no

    @property
    def number_mask(self):
        return 'X'*4+'-'+'X'*4+'-'+'X'*4+'-'+self.number_last_4digit

    class Meta:
        db_table=u'payment_method'

class WithdrawalMethod(GenericModel):
    name = models.CharField(max_length=255)

    class Meta:
        db_table = 'withdrawal_methods'

class WalletBalance(GenericModel):
    currency = models.CharField(max_length=7)
    amount = models.DecimalField(decimal_places=2,max_digits=10,default=0)

    class Meta:
        db_table = "wallet_balance"

class Wallet(GenericModel):
    wallet_balance = models.ManyToManyField(WalletBalance)

    class Meta:
        db_table = 'wallet'

class CreditCard(GenericModel):
    card_type = models.CharField(max_length=100)
    number = models.CharField(max_length=100)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    cvv2 = models.CharField(max_length=50)
    expiry_month = models.IntegerField()
    expiry_year = models.IntegerField()
    billing_address_line1 = models.CharField(max_length=255,null=True, blank=True)
    billing_address_line2 = models.CharField(max_length=255, null=True, blank=True)
    city = models.CharField(max_length=100,null=True,blank=True)
    state = models.CharField(max_length=100,null=True,blank=True)
    zip = models.CharField(max_length=100,null=True,blank=True)
    phone = models.CharField(max_length=100,null=True,blank=True)
    is_default = models.IntegerField(default=0) #value 1 means default.

    @property
    def credit_card_label(self):
        l = len(self.number)
        r = "*".join(['' for i in range(l - 3)])
        return r + self.number[(l-3):]

    def __str__(self):
        return """
            card_type: %s,
            number: %s,
            first_name: %s,
            last_name: %s,
            cvv2: %s,
            expiry_month: %s,
            expiry_year: %s
        """ % (  self.card_type, self.number, self.first_name, self.last_name, self.cvv2, str(self.expiry_month), str(self.expiry_year))

    @property
    def fullname(self):
        return self.first_name+" "+self.last_name

    class Meta:
        db_table = 'credit_card'

class PaypalStoredCreditCard(GenericModel):
    card_id = models.CharField(max_length=255)
    credit_card = models.ForeignKey(CreditCard)

    class Meta:
        db_table = 'paypal_stored_credit_card'

# class MerchantAccount(GenericModel):
#     account_id = models.CharField(max_length=255)
#     account_email = models.CharField(max_length=255)
#     account_password = models.CharField(max_length=255)
#     api_key = models.TextField()
#     api_secret = models.TextField()
#     current_account = models.IntegerField(default=0) ###1 means current account.
#     balance = models.DecimalField(decimal_places=2,max_digits=20,default=0)
#
#     class Meta:
#         db_table = 'merchant_account'

class MerchantAccount(GenericModel):
    braintree_user_id = models.CharField(max_length=100)
    merchant_id = models.CharField(max_length=500)
    public_key = models.CharField(max_length=500)
    private_key = models.CharField(max_length=500)
    currency = models.CharField(max_length=5)
    balance = models.DecimalField(decimal_places=2,max_digits=10,default=0)
    mode = models.CharField(max_length=7) ###SANDBOX or LIVE

    class Meta:
        db_table = 'merchant_account'

class SupportedCurrencies(GenericModel):
    code = models.CharField(max_length=7)
    name = models.CharField(max_length=20)

    class Meta:
        db_table = 'supported_currencies'

class Transaction(GenericModel):
    transaction_id = models.CharField(max_length=200,blank=True)
    transaction_type = models.CharField(max_length=10) ##TransactionType.sale or TransactionType.withdraw or TransactionType.refund or TransactionType.refund2
    user = models.ForeignKey(User,null=True,related_name='transaction_created_by') ###If sale
    user_2 = models.ForeignKey(User,null=True,related_name='transaction_created_for')
    merchant_acount_id = models.CharField(max_length=200)
    amount = models.DecimalField(max_digits=10,decimal_places=2)
    currency = models.CharField(max_length=7)
    total_fees = models.DecimalField(max_digits=10,decimal_places=2)
    fees_percentage = models.DecimalField(max_digits=10, decimal_places=2,null=True)
    transaction_status = models.CharField(max_length=255,default=TransactionStatus.not_initiated)
    payment_method = models.ForeignKey(PaymentMethod)
    # credit_card = models.ForeignKey(PaypalStoredCreditCard,null=True)
    # recipient_email = models.CharField(max_length=255,null=True,blank=True)
    description = models.TextField(blank=True)

    class Meta:
        db_table='payment_transactions'

# class WithdrawTransaction(GenericModel):
#     transaction_id = models.CharField(max_length=200,blank=True)
#     user = models.ForeignKey(User,null=True,related_name='tx_created_by') ###If sale
#     user_2 = models.ForeignKey(User,null=True,related_name='tx_created_for')
#     merchant_acount_id = models.CharField(max_length=200)
#     amount = models.DecimalField(max_digits=10,decimal_places=2)
#     total_fees = models.DecimalField(max_digits=10,decimal_places=2)
#     fees_percentage = models.DecimalField(max_digits=10, decimal_places=2,null=True)
#     transaction_status = models.CharField(max_length=255,default=TransactionStatus.not_initiated)
#     payment_method = models.ForeignKey(PaymentMethod)
#     # credit_card = models.ForeignKey(PaypalStoredCreditCard,null=True)
#     # recipient_email = models.CharField(max_length=255,null=True,blank=True)
#     description = models.TextField(blank=True)
#
#     class Meta:
#         db_table='withdraw_transactions'

class PaypalReferenceId(GenericModel):
    reference_number = models.CharField(max_length=500, blank=True)
    expired_time = models.BigIntegerField()
    card_number = models.CharField(max_length=500)
    user = models.ForeignKey(User)

    class Meta:
        db_table = 'paypal_reference_id'
