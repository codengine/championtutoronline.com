
from suds.client import Client
from django.conf import settings

__author__ = 'Sohel'

class PaymentUtil1(object):
    _instance = None
    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(PaymentUtil1, cls).__new__(
                                cls, *args, **kwargs)
        else:
            print('Already exists')
        return cls._instance

    def set_authentication_header(self):
        CustomSecurityHeaderType = self.client.factory.create('ns3:CustomSecurityHeaderType')
        UserIdPasswordType = self.client.factory.create('ns3:UserIdPasswordType')
        UserIdPasswordType.Username = settings.PAYPAL_API_USERNAME
        UserIdPasswordType.Password = settings.PAYPAL_API_PASSWORD
        UserIdPasswordType.Signature = settings.PAYPAL_API_SIGNATURE
        CustomSecurityHeaderType.Credentials = UserIdPasswordType
        # print(CustomSecurityHeaderType)
        self.client.set_options(soapheaders=CustomSecurityHeaderType)

    def __init__(self):
        try:
            self.client = Client(settings.PAYPAL_WSDL)
            self.api_endpoint = settings.PAYPAL_API_ENDPOINT_TEST if settings.PAYPAL_OPERATION_MODE == 'Testing' else settings.PAYPAL_API_ENDPOINT_PRODUCTION
            # print(self.api_endpoint)
            self.client.set_options(port='PayPalAPIAA',location='https://api-3t.sandbox.paypal.com/2.0/')
            self.set_authentication_header()
            # print(self)
        except Exception, exp:
            self.client = None

    def CreditCard(self,card_type,number, month, year, cvv2, first_name, last_name):
        if self.client:
            credit_card = self.client.factory.create('ns3:CreditCardDetailsType')

            credit_card_type = self.client.factory.create('ns3:CreditCardTypeType')
            _card_type = None
            if card_type == 'VISA':
                _card_type = credit_card_type.Visa
            elif card_type == 'MASTER_CARD':
                _card_type = credit_card_type.MasterCard

            if _card_type:
                credit_card.CreditCardType = _card_type
            else:
                return None

            credit_card.CreditCardNumber = number
            credit_card.ExpMonth = month ###Require int
            credit_card.ExpYear = year ###Require int
            credit_card.CVV2 = cvv2 ###Require string

            payer_info_type = self.client.factory.create('ns3:PayerInfoType')

            payer_info_person_first_and_last_name = self.client.factory.create('ns3:PersonNameType')
            payer_info_person_first_and_last_name.FirstName = first_name
            payer_info_person_first_and_last_name.MiddleName = last_name

            # email_address = self.client.factory.create('ns3:EmailAddressType')
            # email_address.value = 'codenginebd@gmail.com'

            email_address_element = self.client.factory.create('ns3:EmailAddressType')
            email_address_element.value = 'codenginebd@gmail.com'

            payer_info_type.Payer = email_address_element

            # payer_info_type.PayerName = payer_info_person_first_and_last_name

            credit_card.CardOwner = payer_info_type

            return credit_card



    def check_if_credit_card_valid(self,credit_card):
        payment_action_type = self.client.factory.create('ns3:PaymentActionCodeType')
        DoDirectPaymentRequestParam = self.client.factory.create('DoDirectPaymentRequestType')

        DoDirectPaymentRequestParam.PaymentAction = payment_action_type.Sale
        DoDirectPaymentRequestParam.CreditCard = credit_card

        payment_details_type = self.client.factory.create('ns3:PaymentDetailsType')

        basic_amount = self.client.factory.create('ns1:BasicAmountType')
        basic_amount.currencyID = 'USD'
        # basic_amount.value = '1.00'

        payment_details_type.OrderTotal = basic_amount

        DoDirectPaymentRequestParam.PaymentDetails = payment_details_type
        DoDirectPaymentRequestParam.IPAddress = '192.168.225.11'

        DoDirectPaymentRequestParam.Version = '124.0'
        response = self.client.service.DoDirectPayment(DoDirectPaymentRequestParam)
        return response

    def store_credit_card(self,payer_id,credit_card):
        pass

    def create_transaction(self, transaction_type, card_id, amount, user_id, user_id2, merchant_account_id, fees, fees_percentage, tx_status, payment_method_id, credit_card_id,recipient_email):
        pass

    def create_sale_payment(self, created_by_id, lesson_id, pp_credit_card, amount, currency="USD"):
        pass

    def refund_request(self, payment_id, amount, currency, type="sale"):
        pass

