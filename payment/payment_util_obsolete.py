from django.db.models.loading import get_model
from core.enums import TransactionType, TransactionStatus
from payment.models import CreditCard, Transaction
import paypalrestsdk
from paypalrestsdk import Payment
from paypalrestsdk.payments import Authorization
from paypalrestsdk import Sale
from paypalrestsdk import Capture

__author__ = 'Sohel'

class PaymentUtilObsolete(object):
    def __init__(self):
        paypalrestsdk.configure({
          'mode': 'sandbox',
          'client_id': 'AQkquBDf1zctJOWGKWUEtKXm6qVhueUEMvXO_-MCI4DQQ4-LWvkDLIN2fGsd',
          'client_secret': 'EL1tVxAjhT7cJimnz5-Nsx9k2reTKSVfErNQF-CmrwJgxRtylkGTKlU4RvrX'
        })

    def check_if_credit_card_valid(self,credit_card):

        cc = {
                "type": credit_card.get("card_type",""),
                "number": credit_card.get("card_number",""),
                "expire_month": credit_card.get("expiry_month",""),
                "expire_year": credit_card.get("expiry_year",""),
                "cvv2": credit_card.get("cvv2",""),
                "first_name": credit_card.get("first_name",""),
                "last_name": credit_card.get("last_name","")
            }

        payment = Payment({
            "intent": "authorize",

            "payer": {
                "payment_method": "credit_card",

                "funding_instruments": [{

                    # CreditCard
                    # A resource representing a credit card that can be
                    # used to fund a payment.
                    "credit_card": cc
                    }]},
            "transactions": [{

                "amount": {
                    "total": "0.01",
                    "currency": "USD"}
            }]})

        # Create Payment and return status( True or False )
        if payment.create():
            print("Payment[%s] created successfully" % (payment.id))
            authorization_id = payment.transactions[0].related_resources[0].authorization.id
            authorization = Authorization.find(authorization_id)
            return authorization.void()
        return False

    def store_credit_card(self,payer_id,credit_card):
        cc = {
                "type": credit_card.get("card_type",""),
                "number": credit_card.get("card_number",""),
                "expire_month": credit_card.get("expiry_month",""),
                "expire_year": credit_card.get("expiry_year",""),
                "first_name": credit_card.get("first_name",""),
                "last_name": credit_card.get("last_name",""),
                "payer_id": payer_id
            }
        credit_card_object = paypalrestsdk.CreditCard(cc)

        if credit_card_object.create(): # Return True or False
            return True, credit_card_object.id
        return False, ""

    def create_transaction(self, transaction_type, card_id, amount, user_id, user_id2, merchant_account_id, fees, fees_percentage, tx_status, payment_method_id, credit_card_id,recipient_email):
        tx = Transaction()
        if transaction_type == TransactionType.sale.value:
            tx.transaction_type = TransactionType.sale.value
            tx.user_id = user_id
            tx.merchant_acount_id = merchant_account_id
            tx.total_fees = fees
            tx.fees_percentage = fees_percentage
            tx.transaction_status = tx_status
            tx.payment_method_id = payment_method_id
            tx.credit_card_id = credit_card_id
            tx.save()
            return tx.pk
        elif transaction_type == TransactionType.withdraw.value:
            tx.transaction_type = TransactionType.withdraw.value
            tx.user_2_id = user_id2
            tx.merchant_acount_id = merchant_account_id
            tx.total_fees = fees
            tx.fees_percentage = fees_percentage
            tx.transaction_status = tx_status
            tx.payment_method_id = payment_method_id
            tx.credit_card_id = credit_card_id
            tx.save()
            return tx.pk
        elif transaction_type == TransactionType.refund.value:
            pass
        elif transaction_type == TransactionType.refund2.value:
            pass

    def create_sale_payment(self, created_by_id, lesson_id, pp_credit_card, amount, currency="USD"):
        payment = Payment({
            "intent": "sale",

            "payer": {
                "payment_method": "credit_card",

                "funding_instruments": [{
                    "credit_card_token": {
                        "credit_card_id": pp_credit_card.card_id
                    }
                }]
            },
            "transactions": [{

                "amount": {
                    "total": amount,
                    "currency": currency}
            }]})

        # Create Payment and return status( True or False )
        if payment.create():
            payment_id = payment.id
            return payment_id
        else:
            PaymentLog = get_model("core","PaymentLog")
            payment_log = PaymentLog()
            payment_log.created_by_id = created_by_id
            payment_log.status = TransactionStatus.declined.value
            payment_log.message = "Payment not approved using card token %s. Card details: %s" % (pp_credit_card.card_id, pp_credit_card.credi_card)
            payment_log.lesson_id = lesson_id
            payment_log.save()
            return False

    def refund_request(self, payment_id, amount, currency, type="sale"):
        if type == "sale":
            sale = Sale.find(payment_id)

            refund = sale.refund({
                "amount": {
                    "total": amount,
                    "currency": currency}})

            # Check refund status
            if refund.success():
                print("Refund[%s] Success" % (refund.id))
                return True,refund.id
            else:
                print("Unable to Refund")
                print(refund.error)
                return False, refund.error
        elif type == "capture":
            capture = Capture.find(payment_id)
            refund = capture.refund({
                "amount": {
                    "currency": currency,
                    "total": amount}})

            if refund.success():
                print("Refund[%s] Success" % (refund.id))
                return True,refund.id
            else:
                print("Unable to Refund")
                print(refund.error)
                return False, refund.error
        return False, "Invalid transaction type."

