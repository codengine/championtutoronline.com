from decimal import Decimal
import braintree
from django.conf import settings
from braintree.credit_card import CreditCard
from braintree.payment_method_nonce import PaymentMethodNonce
from core.models3 import PaymentLog, ChampUser
from payment.models import BrainTreeCustomer, PaymentMethod, Transaction, MerchantAccount, WalletBalance

__author__ = 'Sohel'


class BrainTreePayment(object):
    def __init__(self):
        pass

    def create_customer(self, user_id, name, email):
        result = braintree.Customer.create({
            "first_name": name,
            "email": email,
        })

        if result.is_success:
            customer_id = result.customer.id
            braintree_customer = BrainTreeCustomer()
            braintree_customer.user_id = user_id
            braintree_customer.braintree_customer_id = customer_id
            braintree_customer.save()

            return braintree_customer.pk
        return None

    def create_customer_if_not_exists(self, user_id, name, email):
        if not BrainTreeCustomer.objects.filter(user_id=user_id).exists():
            customer_id = self.create_customer(user_id, name, email)

            if customer_id:
                return customer_id, True

            return None, False
        else:
            braintree_customer = BrainTreeCustomer.objects.get(user_id=user_id)
            return braintree_customer.pk, False

    """
    Response code:
    -1001 = Credit Card Create Failed,
    -1002 = Payment Method Nonce Create Failed
    -1002 = Payment Method Create Failed
    """
    def create_payment_method(self, user_id, number, exp_month, exp_year, card_type, cvv, cardholder_name,is_default):

        bt_customer = BrainTreeCustomer.objects.get(user_id=user_id)

        if exp_month <= 9:
            exp_month = '0'+str(exp_month)
        cc_result = CreditCard.create({
            "customer_id": bt_customer.braintree_customer_id,
            "number": number,
            "expiration_date": str(exp_month)+'/'+str(exp_year),
            "cvv": cvv,
            "cardholder_name": cardholder_name
        })

        if not cc_result.is_success:
            payment_log = PaymentLog()
            payment_log.created_by_id = user_id
            payment_log.message = 'Credit card create failed for credit card number ends with %s' % number[-4:]
            payment_log.status = 'Failed'
            payment_log.save()

            return False,-1001

        nonce_result = PaymentMethodNonce.create(cc_result.credit_card.token)

        if not nonce_result.is_success:
            CreditCard.delete(credit_card_token=cc_result.credit_card.token)
            payment_log = PaymentLog()
            payment_log.created_by_id = user_id
            payment_log.message = 'Payment method nonce create failed for credit card number ends with %s' % number[-4:]
            payment_log.status = 'Failed'
            payment_log.save()
            return False,-1002

        nonce_response = nonce_result.payment_method_nonce

        nonce = nonce_response.nonce

        pm_result = braintree.payment_method.PaymentMethod.create({
            "customer_id": bt_customer.braintree_customer_id,
            "payment_method_nonce": nonce,
            "options": {"make_default": is_default},
        })

        if not pm_result.is_success:
            CreditCard.delete(credit_card_token=cc_result.credit_card.token)
            payment_log = PaymentLog()
            payment_log.created_by_id = user_id
            payment_log.message = 'Payment method create failed for credit card number ends with %s' % number[-4:]
            payment_log.status = 'Failed'
            payment_log.save()
            return False,-1003

        token = pm_result.payment_method.token

        if is_default:
            pm_objects = PaymentMethod.objects.filter(customer_id = bt_customer.pk)
            for pm_object in pm_objects:
                pm_object.is_primary = 0
                pm_object.save()

        payment_method = PaymentMethod()
        payment_method.customer_id = bt_customer.pk
        payment_method.name = cardholder_name
        payment_method.payment_method_token = token
        payment_method.number_last_4digit = number[-4:]
        payment_method.card_type = card_type
        payment_method.is_primary = is_default
        payment_method.save()

        payment_log = PaymentLog()
        payment_log.created_by_id = user_id
        payment_log.message = 'Payment method created with credit card number ends with %s' % number[-4:]
        payment_log.status = 'Successful'
        payment_log.save()

        return True,payment_method.pk

    def create_sale_transaction(self,payment_method_token,amount,currency,transaction_for_id,settlement,merchant_account_id):
        options = {
            "amount": amount,
            "payment_method_token": payment_method_token,
            "merchant_account_id": merchant_account_id,
            "options": {
                "submit_for_settlement": settlement
            }
        }
        result = braintree.Transaction.sale(options)
        if result.is_success:
            transaction_id = result.transaction.id

            ###Create payment Log
            payment_method_object = PaymentMethod.objects.filter(payment_method_token=payment_method_token)
            if payment_method_object.exists():
                payment_method_object = payment_method_object.first()
                transaction = Transaction()
                transaction.transaction_id = result.transaction.id
                transaction.transaction_type = 'SALE' ##This is Sale transaction.
                transaction.user_id = payment_method_object.customer.user_id
                transaction.user_2_id = transaction_for_id
                transaction.currency = currency
                transaction.merchant_acount_id = merchant_account_id
                transaction.amount = Decimal(amount)
                transaction.total_fees = 0
                transaction.fees_percentage = 0
                transaction.transaction_status = 'SETTLED'
                transaction.payment_method_id = payment_method_object.pk
                transaction.save()

                ###Update Wallet.
                champ_user = ChampUser.objects.get(user_id=transaction_for_id)
                wallet = champ_user.wallet

                wallet_balance = wallet.wallet_balance.filter(currency=currency)
                if wallet_balance.exists():
                    _wallet_balance = wallet_balance.first()
                    _wallet_balance.amount = _wallet_balance.amount + Decimal(amount)
                    _wallet_balance.save()
                else:
                    _wallet_balance = WalletBalance()
                    _wallet_balance.currency = currency
                    _wallet_balance.amount = Decimal(amount)
                    _wallet_balance.save()
                    wallet.wallet_balance.add(_wallet_balance)

                ##Update Merchant Account Balance.
                braintree_user_id = merchant_account_id
                mode = "SANDBOX" if settings.DEBUG else "LIVE"
                merchant_account = MerchantAccount.objects.get(braintree_user_id=braintree_user_id,mode=mode)
                merchant_account.balance = merchant_account.balance + Decimal(amount)
                merchant_account.save()
                
                ##Create Payment Log
                payment_log = PaymentLog()
                payment_log.created_by_id = payment_method_object.customer.user_id
                payment_log.message = 'Sale Transaction Successful.'
                payment_log.status = "SUCCESSFUL"
                payment_log.transaction_id = transaction.pk
                payment_log.save()

            return True,transaction.pk
        else:
            reason = result.message
            ###Create payment Log
            payment_method_object = PaymentMethod.objects.filter(payment_method_token=payment_method_token)
            if payment_method_object.exists():
                payment_method_object = payment_method_object.first()
                transaction = Transaction()
                transaction.transaction_type = 'SALE' ##This is Sale transaction.
                transaction.user_id = payment_method_object.customer.user_id
                transaction.user_2_id = transaction_for_id
                transaction.currency = currency
                transaction.merchant_acount_id = merchant_account_id if merchant_account_id else settings.BRAINTREE_MERCHANTS["DEFAULT"]["ID"]
                transaction.amount = Decimal(amount)
                transaction.total_fees = 0
                transaction.fees_percentage = 0
                transaction.transaction_status = 'FAILED'
                transaction.payment_method_id = payment_method_object.pk
                transaction.description = reason
                transaction.save()

                ##Create Payment Log
                payment_log = PaymentLog()
                payment_log.created_by_id = payment_method_object.customer.user_id
                payment_log.message = 'Sale Transaction Failed.'
                payment_log.status = "FAILED"
                payment_log.transaction_id = transaction.pk
                payment_log.save()

            return False, transaction.pk






