from braintree.merchant_account.merchant_account import MerchantAccount
from payment.braintree_util import BrainTreePayment
from payment.models import PaymentMethod, Transaction
from payment.payment_processor import PaymentProcessor

__author__ = 'Sohel'

# braintree_payment = BrainTreePayment()
#
# users = [
#     ( 3, "Mr Test Student 1", "s1@gmail.com" ),
#     ( 5, "Mr Test Student 2", "s2@gmail.com" ),
# ]
#
# payment_method_tokens = []
#
# for user in users:
#     braintree_payment.create_customer_if_not_exists(*user)
#     braintree_payment.create_payment_method(user[0],"4111111111111111", 5, 2018, "Visa", "100", "John Doe",True)
#     payment_method_tokens += [ PaymentMethod.objects.filter(customer__user__id=user[0]).order_by("-date_created").first().payment_method_token ]
#
# print(payment_method_tokens)
#
# payment_method_tokens2 = []
#
# for user in users:
#     braintree_payment.create_customer_if_not_exists(*user)
#     braintree_payment.create_payment_method(user[0],"4012888888881881", 5, 2018, "Visa", "100", "John Doe",True)
#     payment_method_tokens2 += [ PaymentMethod.objects.filter(customer__user__id=user[0]).order_by("-date_created").first().payment_method_token ]
#
# print(payment_method_tokens2)
#
# braintree_payment.create_sale_transaction(payment_method_tokens[0],200,"SGD",2,settlement=True,merchant_account_id="codenginebd1")
# import time
# time.sleep(10)
# braintree_payment.create_sale_transaction(payment_method_tokens2[0],200,"MYR",2,settlement=True,merchant_account_id="codenginebd2")
#
# print("Done!")

# merchant_account = MerchantAccount.find("codenginebd1")
# print(merchant_account)
# print(merchant_account.funding_details)
# print(merchant_account.individual_details)
# print(merchant_account.business_details)

# payment_processor = PaymentProcessor()
# payment_processor.process_lesson_payment(1,"LIVE_LESSON", 130, "SGD")

id = Transaction.objects.filter(pk=31).first().lessonrequest_set.all().first().id
print(id)
