from decimal import Decimal
import os
import uuid
import time
from django.core.files.base import File
from django.core.urlresolvers import reverse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from core.generics.ajaxmixins import JSONResponseMixin
from core.generics.mixins import LoginRequiredMixin
from core.library.Clock import Clock
from core.models3 import Country, CharUnit, Language, Major, TempAttachment
from payment.braintree_util import BrainTreePayment

__author__ = 'codengine'

from django.views.generic.base import View
from django.template import loader, Context
from django.shortcuts import render
from django.http import HttpResponse
from forms import PaymentMethodForm
from models import *
from datetime import datetime
import json

class PaymentMethodAjaxView(View):
    def get(self,request,*args,**kwargs):
        return HttpResponse("Invalid")
    def post(self,request,*args,**kwargs):
        response = {
            "STATUS": "",
            "MESSAGE": ""
        }
        if request.is_ajax():
            pm_form = PaymentMethodForm(request.POST)
            if pm_form.is_valid():
                try:
                    first_name = pm_form.cleaned_data["first_name"]
                    last_name = pm_form.cleaned_data["last_name"]
                    card_type = pm_form.cleaned_data["card_type"]
                    card_number = pm_form.cleaned_data["card_number"]
                    expired_date = pm_form.cleaned_data["exp_date"]
                    cvn = pm_form.cleaned_data["cvn"]
                    # primary = pm_form.cleaned_data["primary"]
                    # active = pm_form.cleaned_data["active"]

                    # primary = 1 if primary == "primary" else 0
                    #
                    # active = 1 if active else 0

                    date_obj = datetime.strptime(expired_date,"%m/%Y")

                    payment_method = PaymentMethod()
                    payment_method.user = request.user
                    payment_method.first_name = first_name
                    payment_method.last_name = last_name
                    payment_method.card_type = card_type
                    payment_method.card_number = card_number
                    payment_method.month = date_obj.month
                    payment_method.year = date_obj.year
                    payment_method.code = cvn
                    # payment_method.is_primary = primary
                    # payment_method.active = active
                    payment_method.save()

                    response["STATUS"] = "SUCCESS"
                    response["MESSAGE"] = "Payment Method Added Successfully."
                    return HttpResponse(json.dumps(response))
                except Exception,msg:
                    print msg
                    response["STATUS"] = "FAILURE"
                    response["MESSAGE"] = "Server error"
                    return HttpResponse(json.dumps(response))

            else:
                response["STATUS"] = "FAILURE"
                response["MESSAGE"] = "Please fill up the form correctly."
                return HttpResponse(json.dumps(response))
        else:
            response["STATUS"] = "FAILURE"
            response["MESSAGE"] = "Invalid operation"
            return HttpResponse(json.dumps(response))

class CreditCardRemoveAjaxView(JSONResponseMixin,View):
    def post(self,request,*args,**kwargs):
        payment_method_id = request.POST.get("pid")
        try:
            payment_method_id = int(payment_method_id)
            PaymentMethod.objects.filter(pk=payment_method_id).delete()
            response = {
                "status": "SUCCESS",
                "message": "Successful",
                "data": ""
            }
            return self.render_to_json(response)
        except Exception,msg:
            pass
        response = {
            "status": "FAILURE",
            "message": "Failed",
            "data": ""
        }
        return self.render_to_json(response)

class CreditCardAjaxView(JSONResponseMixin,View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(CreditCardAjaxView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        template_name = "ajax/credit_card_form_dialog.html"
        template = loader.get_template(template_name)
        cc_months = []
        context = {
            "dialog_title": "Add New Credit Card"
        }
        cntxt = Context(context)
        rendered = template.render(cntxt)
        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": {
                "dialog_content": rendered
            }
        }
        return self.render_to_json(response)

    def post(self,request,*args,**kwargs):

        get_param = lambda x: request.POST.get(x)

        card_type = get_param("card_type")
        first_name = get_param("first_name")
        last_name = get_param("last_name")
        card_number = get_param("card_number")
        expiry_month = get_param("expiry_month")
        expiry_year = get_param("expiry_year")
        card_cvv = get_param("card_cvv")
        primary_card = get_param("primary_card")

        if primary_card == 'on':
            primary_card = True
        else:
            primary_card = False

        if any([ False for key, value in request.POST.items() if not get_param(key).strip() and key != "primary_card" ]):
            response = {
                "status": "FAILURE",
                "message": "Parameter Missing",
                "data": []
            }
            return self.render_to_json(response)

        try:
            int(expiry_month)
            int(expiry_year)
        except:
            response = {
                "status": "FAILURE",
                "message": "Invalid month and year",
                "data": []
            }
            return self.render_to_json(response)

        braintree = BrainTreePayment()
        braintree.create_customer_if_not_exists(request.user.pk,request.user.champuser.fullname,request.user.email)
        success, pm_id = braintree.create_payment_method(request.user.pk,card_number,expiry_month,expiry_year,card_type,card_cvv,first_name+' '+last_name,primary_card)
        if success:
            response = {
                "status": "SUCCESS",
                "message": "Successful",
                "data": {
                    "pm": pm_id
                }
            }
            return self.render_to_json(response)
        response = {
            "status": "FAILURE",
            "message": "FAILURE",
            "data": []
        }
        return self.render_to_json(response)

class JobPostPreviewAjaxView(LoginRequiredMixin, JSONResponseMixin, View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(JobPostPreviewAjaxView, self).dispatch(*args, **kwargs)

    def post(self,request,*args,**kwargs):
        lesson_post_type = request.POST.get("job_post_lesson_type")
        title = request.POST.get("title")
        description = request.POST.get("description")
        nationality_list = request.POST.getlist("nationality-list[]")
        gender = request.POST.get("gender")
        rate = request.POST.get("rate")
        currency = request.POST.get("currency")
        timezone_list = request.POST.getlist("timezone-list[]")
        language_list = request.POST.getlist("languages-list[]")
        subject_list = request.POST.getlist("subject-list[]")
        study_partners = request.POST.getlist("study_partners-list[]")
        payer = request.POST.get("payer")
        availability = request.POST.get("availability")
        end_date = request.POST.get("job_end_date")
        job_start_date = request.POST.get("lesson_start_date")
        lesson_duration = request.POST.get("duration")
        lesson_repeat = request.POST.get("lesson_repeat")

        response_url = reverse("job_post_create")

        duration = int(lesson_duration)
        context = {
            "data": request.POST
        }
        nationality_list = [int(nid) for nid in nationality_list]
        country_objects = Country.objects.filter(pk__in=nationality_list)
        context["nationalities"] = country_objects

        try:
            Decimal(rate)
        except:
            print("Exception")

        timezones = []
        for tz in timezone_list:
            char_unit = {}
            char_unit["value"] = tz
            timezones += [char_unit]

        context["timezones"] = timezones

        context["currency"] = currency

        languages = []
        for lang in language_list:
            char_unit = CharUnit()
            char_unit.value = lang
            languages += [char_unit]

        context["languages"] = languages

        context["availability"] = ' '.join(availability.capitalize().split('_'))

        subjects = []
        for subject_name in subject_list:
            char_unit = {}
            char_unit["value"] = subject_name
            subjects += [char_unit]

        context["subjects"] = subjects

        context["duration"] = duration

        try:
            job_start_datetime = datetime.strptime(job_start_date,"%d/%m/%Y %H:%M")
        except:
            pass

        try:
            end_date = datetime.strptime(end_date,"%d/%m/%Y")
        except:
            pass

        lesson_timestamp = None
        try:
            lesson_timestamp = Clock.convert_local_to_utc_timestamp(request.user.champuser.timezone,time.mktime(job_start_datetime.timetuple()))
        except Exception,msg:
            lesson_timestamp = None

        tday_date = datetime.now()
        tday_date_str = str(tday_date.year)+"_"+str(tday_date.month)+"_"+str(tday_date.day)+"_"

        try:
            fcount = int(request.POST.get("fcount"))
        except:
            fcount = 0

        attachments = []
        for i in range(int(fcount)):
            attachments += [ "#" ]

        context["attachments"] = attachments

        context["lesson_timestamp"] = lesson_timestamp

        context["date_created"] = Clock.utc_timestamp()
        context["end_date"] = Clock.utc_timestamp()

        if lesson_repeat == "1":
            lesson_repeat = "No Repeat"
        elif lesson_repeat == "2":
            lesson_repeat = "Daily"
        elif lesson_repeat == "3":
            lesson_repeat = "Weekly"

        context["lesson_repeat"] = lesson_repeat

        other_students = [ request.user ]
        for s in study_partners:
            other_students += [ User.objects.get(pk=int(s)) ]

        context["other_students"] = other_students

        # return render(request,"job_post_preview.html",context)

        template_name = "job_post_preview.html"
        template = loader.get_template(template_name)
        cntxt = Context(context)
        rendered = template.render(cntxt)
        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": { "dialog_content": rendered }
        }
        return self.render_to_json(response)

class QuestionPostPreviewAjaxView(LoginRequiredMixin, JSONResponseMixin, View):
    def post(self,request,*args,**kwargs):
        context = {
            "date_created": Clock.utc_timestamp(),
            "created_by": request.user
        }
        q_title = request.POST.get("q_title")
        q_description = request.POST.get("q_description")
        q_tags = request.POST.getlist("qtag-list[]")
        files = request.POST.getlist("file_name_list[]")

        tags = []
        for q_tag in q_tags:
            tags += [Major.objects.get(pk=int(q_tag)).name]

        question = {
            "title": q_title,
            "description": q_description,
            "tags": tags,
            "files": files
        }

        try:
            fcount = int(request.POST.get("fcount"))
        except:
            fcount = 0

        attachments = []
        for i in range(int(fcount)):
            attachments += [ "#" ]

        context["attachments"] = attachments

        context["request"] = request
        context["question"] = question
        template_name = "question_preview.html"
        template = loader.get_template(template_name)
        cntxt = Context(context)
        rendered = template.render(cntxt)
        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": { "dialog_content": rendered }
        }
        return self.render_to_json(response)
