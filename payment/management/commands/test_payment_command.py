from decimal import Decimal
from django.core.management.base import BaseCommand
import braintree
from braintree.credit_card import CreditCard
from braintree.customer import Customer
from braintree.payment_method import PaymentMethod
from braintree.payment_method_nonce import PaymentMethodNonce
from braintree.test.nonces import *
from braintree.transaction import Transaction


class Command(BaseCommand):
    def handle(self, *args, **options):
        from payment.braintree_usage import *
        # customer = Customer.create().customer
        # cc_result = CreditCard.create({
        #     "customer_id": customer.id,
        #     "number": "4111111111111111",
        #     "expiration_date": "05/2014",
        #     "cvv": "100",
        #     "cardholder_name": "John Doe"
        # })
        #
        # result = PaymentMethodNonce.create(cc_result.credit_card.token)
        #
        # print(result.is_success)
        #
        # nonce = result.payment_method_nonce
        #
        # print("Nonce: ")
        # print(nonce.nonce)
        #
        # presult = PaymentMethod.create({
        #     "customer_id": customer.id,
        #     "payment_method_nonce": nonce.nonce,
        #     "options": {"make_default": True},
        # })
        #
        # print(presult.is_success)
        # print(presult.payment_method.token)
        #
        # result = braintree.Transaction.sale({
        #     "amount": "10.00",
        #     "payment_method_token": presult.payment_method.token,
        #     "merchant_account_id": "codenginebd2",
        #     "options": {
        #         "submit_for_settlement": True
        #     }
        # })
        #
        # print("Transaction Status")
        # print(result)

        # payment_method = PaymentMethod.find(presult.payment_method.token)
        # print(payment_method.last_4)
        #
        # payment_method = PaymentMethod.find(presult.payment_method.token)
        # print(payment_method.last_4)
        #
        # payment_method = PaymentMethod.find(presult.payment_method.token)
        # print(payment_method.last_4)
        #
        # payment_method = PaymentMethod.find(presult.payment_method.token)
        # print(payment_method.last_4)
        #
        # payment_method = PaymentMethod.find(presult.payment_method.token)
        # print(payment_method.last_4)


        # print(braintree.ClientToken.generate())
        #
        # ###Create a customer.
        # result = braintree.Customer.create({
        #     "email": "s100@gmail.com",
        #     "first_name": "Mr Test",
        #     "last_name": "Student 100"
        # })
        #
        # print(result.customer.id)
        # print(result.customer.first_name)
        #
        # result = braintree.PaymentMethod.create({
        #     "customer_id": result.customer.id,
        #     "payment_method_nonce": "fake-valid-visa-nonce",
        #     "cardholder_name": "John Doe",
        #     "cvv": "123",
        #     "expiration_month": "12",
        #     "expiration_year": "2016",
        #     "number":"4111111111111111",
        #     "billing_address": {
        #         "first_name": "John",
        #         "last_name": "Doe",
        #         "company": "Braintree",
        #         "street_address": "111 First Street",
        #         "extended_address": "Unit 1",
        #         "locality": "Chicago",
        #         "postal_code": "60606",
        #         "region": "IL",
        #         "country_name": "United States of America"
        #     },
        #     "options": {
        #         "verify_card": True
        #     }
        # })

        # "billing_address_id",
        #     "cardholder_name",
        #     "customer_id",
        #     "cvv",
        #     "device_data",
        #     "device_session_id",
        #     "expiration_date",
        #     "expiration_month",
        #     "expiration_year",
        #     "number",
        #     "payment_method_nonce",
        #     "token",
        #     {"billing_address": Address.create_signature()},
        #     {"options": [
        #         "fail_on_duplicate_payment_method",
        #         "make_default",
        #         "verification_merchant_account_id",
        #         "verify_card",
        #         ]
        #     }


        # print(result.is_success)
        # print(result.credit_card_verification)

        # result = Transaction.sale({
        #     "credit_card": {
        #         "number": "4111111111111111",
        #         "expiration_date": "05/2010",
        #         "cvv": "100"
        #     },
        #     "amount": "100.00",
        #     "options": {
        #         "submit_for_settlement": "true"
        #     }
        # })
        #
        # if result.is_success:
        #     print("success!: " + result.transaction.id)
        # elif result.transaction:
        #     print("Error processing transaction:")
        #     print("  code: " + result.transaction.processor_response_code)
        #     print("  text: " + result.transaction.processor_response_text)
        # else:
        #     for error in result.errors.deep_errors:
        #         print("attribute: " + error.attribute)
        #         print("  code: " + error.code)
        #         print("  message: " + error.message)