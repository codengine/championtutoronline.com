from django.db import models
from admin.servey.renderer import ServeyRenderer
from core.generics.models import GenericModel

__author__ = 'Sohel'

class ServeyQuestionOption(GenericModel):
    value = models.CharField(max_length=500)
    label = models.CharField(max_length=500)

    class Meta:
        db_table = 'feedback_question_option'

class ServeyQuestion(GenericModel):
    title = models.CharField(max_length=500)
    qtype = models.CharField(max_length=100, default='text')
    options = models.ManyToManyField(ServeyQuestionOption)
    answer = models.CharField(max_length=500, default='', blank=True, null=True)

    @property
    def to_html(self):
        return ServeyRenderer.render_servey_question(self)

    class Meta:
        db_table = 'servey_question'

class Servey(GenericModel):
    title = models.CharField(max_length=500)
    published = models.IntegerField(default=0) ###0 means unpublish, 1 means publish
    publish_date = models.BigIntegerField(default=0)
    questions = models.ManyToManyField(ServeyQuestion)

    class Meta:
        db_table = 'servey'

class UserServey(GenericModel):
    email = models.CharField(max_length=500),
    servey = models.ForeignKey(Servey)

    class Meta:
        db_table = 'user_servey'
