from django.forms.models import ModelForm
from servey.models import ServeyQuestionOption

__author__ = 'Sohel'

class ServeyQuestionOptionForm(ModelForm):
    class Meta:
        model = ServeyQuestionOption
