from django.conf.urls.static import static
from django.conf import settings
from django.conf.urls import url, patterns
from feedback.views import FeedbackListView

urlpatterns = patterns('',
    url(r'^user-feedback/', FeedbackListView.as_view(), name='user-feedback'),

)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
js_info_dict = {
    'packages': ('django.conf',),
}

if settings.DEBUG:
    urlpatterns += patterns('django.views.static',
                            (r'media/(?P<path>.*)', 'serve', {'document_root': settings.MEDIA_ROOT}),
                            )
