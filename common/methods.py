from core.models3 import ChampUser,Role
from core.enums import UserTypes
from django.db.models import Q

def check_login(request):
	if request.session.get("is_login"):
		return True
	return False

def get_buddies_online(request):
    buddies = []
    _this_user_id = request.user.id
    user_objs = ChampUser.objects.filter(user__id=_this_user_id)
    if user_objs:
        user_obj = user_objs[0]
        #data["utype"] = user_obj.type

        query = None

        if user_obj.type.name == UserTypes.student.value:
            #query to get teachers with recent chat and also teachers who initiated a chat with this student.
            buddies = ChampUser.objects.filter(~Q(user=request.user),type=Role.objects.get(name=UserTypes.teacher.value))
            #query = "select * from champ_user where type_id=1 and user_id != %s" % _this_user_id
        else:
            #query to get students with recent chat and also students who initiated a chat with this teacher.
            #query = "select * from champ_user where type_id=2 and user_id != %s" % _this_user_id
            buddies = ChampUser.objects.filter(~Q(user=request.user),type=Role.objects.get(name=UserTypes.student.value))

            # if query:
            #     data["buddies"] = User.objects.raw(query)

    return buddies