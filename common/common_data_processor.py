from django.db.models.query_utils import Q
from core.models3 import ChampUser, UserMessage, ChatSettings, Conversation, UserBlockSettings
from forms import LoginForm, SignUpForm
from notification.models import Notification
from engine.config import social_apps

__author__ = 'codengine'

def get_unread_message_count(request):
    query = "select * from user_message where receiver_id=%s group by sender_id,receiver_id order by id DESC;" % (request.user.pk, )
    user_messages = UserMessage.objects.raw(query) #.filter(Q(sender=self.request.user) | Q(receiver=self.request.user)).group_by('sender_id','receiver_id').order_by("-date_created")
    umessage_ids = []
    uids = []
    for umessage in user_messages:
        if umessage.sender_id != request.user.pk:
            if not umessage.sender_id in uids:
                # umessage_ids += [ umessage.pk ]
                uids += [ umessage.sender_id ]
        else:
            if not umessage.receiver_id in uids:
                # umessage_ids += [ umessage.pk ]
                uids += [ umessage.receiver_id ]

    for uid in uids:
        umessage = UserMessage.objects.filter((Q(sender_id=uid) & Q(receiver_id=request.user.pk) & Q(message__is_read=0))).order_by('-id')
        if umessage.exists():
            umessage = umessage.first()
            umessage_ids += [ umessage.pk ]

    return len(umessage_ids)

def get_chat_settings(request):
    chat_settings = ChatSettings.objects.filter(user_id=request.user.pk)
    data = {}
    if chat_settings.exists():
        chat_setting = chat_settings.first()
        data = {
            "online_status": chat_setting.online_status
        }
        chatboxes = []
        all_chatboxs = chat_setting.chatboxes.all()
        for chatbox in all_chatboxs:
            chtbx_data = {}
            if chatbox.chat_type == 0: ###p2p chat
                try:
                    champ_user = ChampUser.objects.get(user_id=int(chatbox.chat_id))
                    chtbx_data['chat_type'] = 0
                    chtbx_data['chat_id'] = int(chatbox.chat_id)
                    chtbx_data['remote_peers'] = chatbox.chat_id
                    chtbx_data['title'] = champ_user.fullname
                    chtbx_data['online_status'] = champ_user.render_online_status_verbal
                    chtbx_data["show"] = 1 if (chatbox.active_status == 1) else 0
                    chtbx_data["blocked"] = 0
                    blocked = 0
                    block_settings = UserBlockSettings.objects.filter(user_id=request.user.pk)
                    if block_settings.exists():
                        block_settings = block_settings.first()
                        blocked_users = block_settings.blocked_users.filter(user_id=int(chatbox.chat_id))
                        if blocked_users.exists():
                            blocked = 1

                    if blocked == 0:
                        block_settings = UserBlockSettings.objects.filter(user_id=int(chatbox.chat_id))
                        if block_settings.exists():
                            block_settings = block_settings.first()
                            blocked_users = block_settings.blocked_users.filter(user_id=request.user.pk)
                            if blocked_users.exists():
                                blocked = 1
                    chtbx_data["blocked"] = blocked
                except:
                    pass

            elif chatbox.chat_type == 1:
                conversation = Conversation.objects.filter(conversation_id=chatbox.chat_id)
                if conversation.exists():
                    conversation = conversation.first()
                    remote_peers = [ str(user.pk) for user in conversation.other_users.all() ]
                    chtbx_data['chat_type'] = 1
                    chtbx_data['chat_id'] = chatbox.chat_id
                    chtbx_data["remote_peers"] = ','.join(remote_peers)
                    chtbx_data['title'] = conversation.title if conversation.title else 'Group Chat'
                    chtbx_data['online_status'] = "offline"
                    chtbx_data["show"] = 1 if (chatbox.active_status == 1) else 0
                    chtbx_data["blocked"] = 0
                    conversation_object = Conversation.objects.filter(conversation_id=chatbox.chat_id)
                    if conversation_object.exists():
                        conversation_object = conversation_object.first()
                        if conversation_object.users_left.filter(pk=request.user.pk).exists():
                            chtbx_data["blocked"] = 1

            chatboxes += [ chtbx_data ]
        data['chatboxes'] = chatboxes
    return data

def user_common_info(request):
    if not request.is_ajax():
        champ_user = None
        champ_users = ChampUser.objects.filter(user_id=request.user.id)
        if champ_users.exists():
            champ_user = champ_users[0]
        context = {}
        if not request.user.is_authenticated():
            pass
            # context["login_form"] = LoginForm()
            # context["signup_form"] = SignUpForm()
        else:
            user_id = request.user.pk
            unread_notification_count = Notification.objects.filter(target_user_id=user_id,read=0).count()
            context['unread_notification_count'] = unread_notification_count
            context["unread_message_count"] = get_unread_message_count(request)
            context['chat_settings'] = get_chat_settings(request)
        context["login_form"] = LoginForm()
        context["signup_form"] = SignUpForm()
        context["champ_user"] = champ_user
        context["FACEBOOK_APP_ID"] = social_apps.FACEBOOK_APP_ID
        return context
    else:
        return {}
