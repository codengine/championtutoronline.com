__author__ = 'codengine'


CHANNEL = {
    "send_message": "MESSAGE_SENT",
    "send_chat_message": "CHAT_MESSAGE_SENT",
    "wb_msg": "WHITEBOARD_MESSAGE",
    "message_unread_count": "MESSAGE_UNREAD_COUNT",
    "conversation_title_change": "CONVERSATION_TITLE_CHANGED",
    "conversation_buddy_added": "CONVERSATION_BUDDY_ADDED",
    "conversation_buddy_left": "CONVERSATION_BUDDY_LEFT",
    "conversation_buddy_removed": "CONVERSATION_BUDDY_REMOVED",
    "chat_settings_updated": "CHAT_SETTINGS_UPDATED",
    "user_blocked": "USER_BLOCKED",
    "user_unblocked": "USER_UNBLOCKED",
    "notify_drawingboard_added": "NOTIFY_DRAWINGBOARD_ADDED",
    "notify_text_pad_added": "NOTIFY_TEXT_PAD_ADDED",
    "notify_code_pad_added": "NOTIFY_CODE_PAD_ADDED",
    "notify_code_pad_lan_added": "NOTIFY_CODE_PAD_LAN_ADDED"
}