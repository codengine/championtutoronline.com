from datetime import datetime

__author__ = 'codengine'

def get_date_diff(value,format_string):
    if isinstance(value,datetime):
        datetime_obj = value.replace(tzinfo=None)
    else:
        datetime_obj = datetime.strptime(value,format_string)
    datetime_now = datetime.now()
    diff = datetime_now - datetime_obj
    diff_days = diff.days
    if diff_days > 30:
        return datetime_obj.strftime("%b %d, %Y %H:%M%p")
    diff_seconds = diff.seconds
    diff_str = ""
    if diff_days > 0:
        diff_str = str(diff_days)+" days ago"
        return diff_str
    hours = diff_seconds / 3600
    if hours > 0:
        diff_str = str(hours)+" hours ago"
        return diff_str
    remaining_seconds = diff_seconds % 3600
    mins = remaining_seconds / 60
    if mins > 0:
        diff_str = str(mins)+" minutes ago"
        return diff_str
    remaining_seconds = remaining_seconds % 60
    if remaining_seconds >= 0:
        diff_str += str(remaining_seconds)+" seconds "
    if diff_str:
        diff_str += " ago"
    return diff_str
