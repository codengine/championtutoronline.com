from django.core.paginator import Paginator
from django.views.generic.list import ListView

from core.generics.mixins import LoginRequiredMixin
from notification.models import Notification
from notification.utils.notification_item_renderer import NotificationItemRenderer

__author__ = 'Sohel'

class NotificationListView(LoginRequiredMixin, ListView):
    template_name = "notification_list.html"
    model = Notification
    paginate_by = 20

    def get_queryset(self):
        return Notification.objects.filter(target_user_id=self.request.user.pk).order_by("-notification_date")

    def get_context_data(self, **kwargs):
        context = super(NotificationListView, self).get_context_data(**kwargs)
        unread_notifications = self.get_queryset()
        p = 1
        try:
            p = int(self.request.GET.get("page"))
        except:
            p = 1
        paginator = Paginator(unread_notifications,self.paginate_by)
        object_list = []
        try:
            page = paginator.page(p)
            object_list = page.object_list
        except Exception as msg:
            object_list = []

        context_data = []
        for notification in object_list:
            notification_item = NotificationItemRenderer.render_notification_item(notification)
            context_data += [ notification_item ]
        context['notification_list'] = context_data
        return context
