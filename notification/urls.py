from notification.views import NotificationListView
from notification.viewsajax import NotificationActionAjaxView

__author__ = 'Sohel'

from django.conf.urls import patterns, url

urlpatterns = patterns('',
        url(r'^$', NotificationListView.as_view(), name='notification_list_view'),
        url(r'^ajax/notification_action/$', NotificationActionAjaxView.as_view(), name='ajax_notification_action'),
    )

