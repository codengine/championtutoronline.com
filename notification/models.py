from django.contrib.auth.models import User
from django.db import models
from core.enums import ReadStatus
from core.generics.models import GenericModel
from core.library.Clock import Clock

__author__ = 'Sohel'

class Notification(GenericModel):
    read = models.IntegerField(ReadStatus.unread.value) ###0 means unread and 1 means read.
    notif_type = models.CharField(max_length=200,null=True,blank=True)  ###job_invited, job_recommended, invite_applied, invite_rejected, offer_accepted, offer_rejected, hire_offered
    object_class = models.CharField(max_length=20) ###if job_invited then JobInvitation etc
    object_id = models.IntegerField() ###if job_invited then JobInvitation object id.
    text = models.TextField(null=True,blank=True)
    title = models.TextField(null=True,blank=True)
    url = models.CharField(max_length=500,null=True,blank=True)
    created_by = models.ForeignKey(User,related_name='notification_created_by')
    target_user = models.ForeignKey(User,related_name='notification_for')
    notification_date = models.BigIntegerField()

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None, update_date=True):
        if update_date:
            self.notification_date = Clock.utc_timestamp()
        super(Notification, self).save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)

    class Meta:
        db_table = 'notification'
