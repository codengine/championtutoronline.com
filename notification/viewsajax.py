from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import View
from core.generics.ajaxmixins import JSONResponseMixin
from core.generics.mixins import LoginRequiredMixin
from notification.models import Notification
from notification.utils.notification_item_renderer import NotificationItemRenderer
from notification.utils.notification_manager import NotificationManager

__author__ = 'Sohel'


class NotificationActionAjaxView(LoginRequiredMixin, JSONResponseMixin, View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(NotificationActionAjaxView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        # if request.is_ajax():
        action = request.GET.get("action")
        domain = request.GET.get("domain")
        mark_read = request.GET.get("mark_read")
        pagination_by = 5
        nitems = request.GET.get('pagination_by')
        try:
            nitems = int(nitems)
            pagination_by = nitems
        except:
            pagination_by = 5
        if action == "load":
            if domain == "all":
                unread_notifications = Notification.objects.filter(target_user_id=request.user.pk).order_by('-notification_date')[:pagination_by]
            elif domain == "unread":
                unread_notifications = Notification.objects.filter(target_user_id=request.user.pk,read=0).order_by('-notification_date')[:pagination_by]
            elif domain == "read":
                unread_notifications = Notification.objects.filter(target_user_id=request.user.pk,read=1).order_by('-notification_date')[:pagination_by]
            else:
                unread_notifications = []

            context_data = []
            for notification in unread_notifications:
                if mark_read:
                    notification.read = 1
                    notification.save(update_date=False)
                notification_item = NotificationItemRenderer.render_notification_item(notification)
                context_data += [ notification_item ]

            NotificationManager.notify_all_read(request.user.pk)

            response = {
                "status": "SUCCESS",
                "message": "Successful"
            }
            response['data'] = { 'unread_notifications': context_data }
            return self.render_to_json(response)
    def post(self,request, *args, **kwargs):
        action = request.POST.get("action")
        mark_read = request.POST.get("mark_read")
        notification_id = request.POST.get("object_id")
        if action == "update":
            try:
                mark_read = int(mark_read)
                if mark_read == 1 or mark_read == 0:
                    notification_id = int(notification_id)
                    notification_object = Notification.objects.get(pk=notification_id)
                    notification_object.read = mark_read
                    notification_object.save()
                    response = {
                        "status": "SUCCESS",
                        "message": "Successful"
                    }
                    NotificationManager.notify_change('modify',notification_id,notification_object.target_user_id)
                    return self.render_to_json(response)
            except:
                pass
        elif action == "delete":
            try:
                notification_id = int(notification_id)
                notification_object = Notification.objects.get(pk=notification_id)
                target_user_id = notification_object.target_user_id
                notification_object.delete()
                NotificationManager.notify_change('delete',notification_id,target_user_id)
                response = {
                    "status": "SUCCESS",
                    "message": "Successful"
                }
                return self.render_to_json(response)
            except:
                pass
        response = {
            "status": "FAILURE",
            "message": "Failed"
        }
        return self.render_to_json(response)