from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from core.models3 import Job
from notification.models import Notification
from notification.utils.notification_item_renderer import NotificationItemRenderer
from notification.utils.notification_publisher import NotificationPublisher

__author__ = 'Sohel'

class NotificationJobApplication(object):
    def __init__(self):
        pass

    @classmethod
    def handle_job_application_rejected(cls,notif_type,object_id,created_by_id,target_user_id):
        notification = Notification()
        notification.notif_type = notif_type
        notification.object_class = Job.__name__
        notification.object_id = object_id
        notification.created_by_id = created_by_id
        notification.target_user_id = target_user_id
        notification.read = 0 ###0 means unread

        ###Job details url.
        url = reverse('job_post_details',kwargs={ 'pk': object_id })
        notification.url = url

        job = Job.objects.filter(pk=object_id)
        if not job.exists():
            return None

        job = job.first()

        ###Create notification message
        msg = """Your application to the job %s rejected.""" % (job.title,)
        notification.text = msg
        notification.save()

        notification_item = NotificationItemRenderer.render_notification_item(notification)

        unread_notification_count = Notification.objects.filter(target_user_id=target_user_id,read=0).count()

        push_message = {
            'id': notification.id,
            'action': 'new',
            'target_user': target_user_id,
            'notif_item': notification_item,
            'object_id': object_id,
            'url': url,
            'status': 'SUCCESS',
            'unread_count': unread_notification_count
        }

        NotificationPublisher.publish_notification('PN_CHANNEL',push_message)

    @classmethod
    def handle_job_application_withdrawn(cls,notif_type,object_id,created_by_id,target_user_id):
        notification = Notification()
        notification.notif_type = notif_type
        notification.object_class = Job.__name__
        notification.object_id = object_id
        notification.created_by_id = created_by_id
        notification.target_user_id = target_user_id
        notification.read = 0 ###0 means unread

        ###Job details url.
        url = reverse('job_post_details',kwargs={ 'pk': object_id })
        notification.url = url

        job = Job.objects.filter(pk=object_id)
        if not job.exists():
            return None

        created_by_user = User.objects.get(pk=created_by_id)

        job = job.first()

        ###Create notification message
        msg = """%s has withdrawn %s application for the job %s""" % (created_by_user.champuser.fullname, "his" if created_by_user.champuser.gender == "male" else "her", job.title,)
        notification.text = msg
        notification.save()

        notification_item = NotificationItemRenderer.render_notification_item(notification)

        unread_notification_count = Notification.objects.filter(target_user_id=target_user_id,read=0).count()

        push_message = {
            'id': notification.id,
            'action': 'new',
            'target_user': target_user_id,
            'notif_item': notification_item,
            'object_id': object_id,
            'url': url,
            'status': 'SUCCESS',
            'unread_count': unread_notification_count
        }

        NotificationPublisher.publish_notification('PN_CHANNEL',push_message)

    @classmethod
    def handle_job_application_shortlisted(cls,notif_type,object_id,created_by_id,target_user_id):
        notification = Notification()
        notification.notif_type = notif_type
        notification.object_class = Job.__name__
        notification.object_id = object_id
        notification.created_by_id = created_by_id
        notification.target_user_id = target_user_id
        notification.read = 0 ###0 means unread

        ###Job details url.
        url = reverse('job_post_details',kwargs={ 'pk': object_id })
        notification.url = url

        job = Job.objects.filter(pk=object_id)
        if not job.exists():
            return None

        job = job.first()

        ###Create notification message
        msg = """Your application has been shortlisted for the job: %s""" % (job.title,)
        notification.text = msg
        notification.save()

        notification_item = NotificationItemRenderer.render_notification_item(notification)

        unread_notification_count = Notification.objects.filter(target_user_id=target_user_id,read=0).count()

        push_message = {
            'id': notification.id,
            'action': 'new',
            'target_user': target_user_id,
            'notif_item': notification_item,
            'object_id': object_id,
            'url': url,
            'status': 'SUCCESS',
            'unread_count': unread_notification_count
        }

        NotificationPublisher.publish_notification('PN_CHANNEL',push_message)
