from notification.models import Notification
from notification.utils.notification_invitation_item_renderer import NotificationInvitationItemRenderer

__author__ = 'Sohel'


class NotificationItemRenderer(object):
    @classmethod
    def render_notification_item(cls,notification):
        if isinstance(notification, Notification):
            notification_object = notification
        else:
            notification_object = Notification.objects.filter(pk=notification)
            if notification_object.exists():
                notification_object = notification_object.first()
            else:
                notification_object = None
        if notification_object:
            if 'question_' in notification_object.notif_type:
                return NotificationInvitationItemRenderer.render_question_item(notification_object)
            else:
                return NotificationInvitationItemRenderer.render_invitation_item(notification_object)





