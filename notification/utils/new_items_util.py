from core.models3 import JobRecommendation, JobInvitation

__author__ = 'Sohel'

class NewItemRenderer(object):

    @classmethod
    def render_new_items_count_profile(cls,target_user_id):
        active_job_invite_count = JobInvitation.objects.filter(status=1,user_id=target_user_id).count()
        active_job_recommend_count = JobRecommendation.objects.filter(status=1,user_id=target_user_id).count()

        data = {
            "new_invite_count": active_job_invite_count,
            "new_recommend_count": active_job_recommend_count
        }
        return data
