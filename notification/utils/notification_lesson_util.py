from django.core.urlresolvers import reverse
from core.models3 import LiveLesson, WrittenLesson
from notification.models import Notification
from notification.utils.notification_item_renderer import NotificationItemRenderer
from notification.utils.notification_publisher import NotificationPublisher

__author__ = 'Sohel'

class NotificationLessonUtil(object):
    def __init__(self):
        pass

    @classmethod
    def handle_lesson_scheduled(cls,notif_type,object_id,created_by_id,target_user_id,lesson_type):
        notification = Notification()
        notification.notif_type = notif_type
        notification.object_class = LiveLesson.__name__ if lesson_type == 0 else WrittenLesson.__name__
        notification.object_id = object_id
        notification.created_by_id = created_by_id
        notification.target_user_id = target_user_id
        notification.read = 0 ###0 means unread

        ###Job details url.

        lesson = None
        url = None
        if lesson_type == 0:
            live_lesson = LiveLesson.objects.filter(pk=object_id)
            if not live_lesson.exists():
                return None
            lesson = live_lesson.first()
            url = reverse('live_lesson',kwargs={ 'pk': object_id })
        else:
            written_lesson = WrittenLesson.objects.filter(pk=object_id)
            if not written_lesson.exists():
                return None
            lesson = written_lesson.first()
            url = reverse('written_lesson_request_view')+'?lesson=%s' % (str(object_id),)

        if not lesson or not url:
            return

        notification.url = url

        ###Create notification message
        msg = """Your lesson %s has been scheduled""" % (lesson.title,)
        notification.text = msg
        notification.save()

        notification_item = NotificationItemRenderer.render_notification_item(notification)

        unread_notification_count = Notification.objects.filter(target_user_id=target_user_id,read=0).count()

        push_message = {
            'id': notification.id,
            'action': 'new',
            'target_user': target_user_id,
            'notif_item': notification_item,
            'object_id': object_id,
            'url': url,
            'status': 'SUCCESS',
            'unread_count': unread_notification_count
        }

        NotificationPublisher.publish_notification('PN_CHANNEL',push_message)

    @classmethod
    def handle_lesson_started(cls,notif_type,object_id,created_by_id,target_user_id,lesson_type):
        notification = Notification()
        notification.notif_type = notif_type
        notification.object_class = LiveLesson.__name__ if lesson_type == 0 else WrittenLesson
        notification.object_id = object_id
        notification.created_by_id = created_by_id
        notification.target_user_id = target_user_id
        notification.read = 0 ###0 means unread

        ###Job details url.

        lesson = None
        url = None
        if lesson_type == 0:
            live_lesson = LiveLesson.objects.filter(pk=object_id)
            if not live_lesson.exists():
                return None
            lesson = live_lesson.first()
            url = reverse('live_lesson',kwargs={ 'pk': object_id })
        else:
            written_lesson = WrittenLesson.objects.filter(pk=object_id)
            if not written_lesson.exists():
                return None
            lesson = written_lesson.first()
            url = reverse('written_lesson_request_view')+'?lesson=%s' % (str(object_id),)

        if not lesson or not url:
            return

        notification.url = url

        ###Create notification message
        msg = """Your lesson %s started""" % (lesson.title,)
        notification.text = msg
        notification.save()

        notification_item = NotificationItemRenderer.render_notification_item(notification)

        unread_notification_count = Notification.objects.filter(target_user_id=target_user_id,read=0).count()

        push_message = {
            'id': notification.id,
            'action': 'new',
            'target_user': target_user_id,
            'notif_item': notification_item,
            'object_id': object_id,
            'url': url,
            'status': 'SUCCESS',
            'unread_count': unread_notification_count
        }

        NotificationPublisher.publish_notification('PN_CHANNEL',push_message)
    @classmethod
    def handle_lesson_rescheduled(cls,notif_type,object_id,created_by_id,target_user_id,lesson_type,reschedule_time):
        notification = Notification()
        notification.notif_type = notif_type
        notification.object_class = LiveLesson.__name__ if lesson_type == 0 else WrittenLesson
        notification.object_id = object_id
        notification.created_by_id = created_by_id
        notification.target_user_id = target_user_id
        notification.read = 0 ###0 means unread

        ###Job details url.

        lesson = None
        url = None
        if lesson_type == 0:
            live_lesson = LiveLesson.objects.filter(pk=object_id)
            if not live_lesson.exists():
                return None
            lesson = live_lesson.first()
            url = reverse('live_lesson',kwargs={ 'pk': object_id })
        else:
            written_lesson = WrittenLesson.objects.filter(pk=object_id)
            if not written_lesson.exists():
                return None
            lesson = written_lesson.first()
            url = reverse('written_lesson_request_view')+'?lesson=%s' % (str(object_id),)

        if not lesson or not url:
            return

        notification.url = url

        ###Create notification message
        msg = """Your lesson %s has been rescheduled at %s """ % (lesson.title, reschedule_time)
        notification.text = msg
        notification.save()

        notification_item = NotificationItemRenderer.render_notification_item(notification)

        unread_notification_count = Notification.objects.filter(target_user_id=target_user_id,read=0).count()

        push_message = {
            'id': notification.id,
            'action': 'new',
            'target_user': target_user_id,
            'notif_item': notification_item,
            'object_id': object_id,
            'url': url,
            'status': 'SUCCESS',
            'unread_count': unread_notification_count
        }

        NotificationPublisher.publish_notification('PN_CHANNEL',push_message)
    @classmethod
    def handle_lesson_schedule_failed(cls,notif_type,object_id,created_by_id,target_user_id,lesson_type):
        notification = Notification()
        notification.notif_type = notif_type
        notification.object_class = LiveLesson.__name__ if lesson_type == 0 else WrittenLesson
        notification.object_id = object_id
        notification.created_by_id = created_by_id
        notification.target_user_id = target_user_id
        notification.read = 0 ###0 means unread

        ###Job details url.

        lesson = None
        url = None
        if lesson_type == 0:
            live_lesson = LiveLesson.objects.filter(pk=object_id)
            if not live_lesson.exists():
                return None
            lesson = live_lesson.first()
        else:
            written_lesson = WrittenLesson.objects.filter(pk=object_id)
            if not written_lesson.exists():
                return None
            lesson = written_lesson.first()

        if not lesson or not url:
            return

        notification.url = reverse('notification_list_view')

        ###Create notification message
        msg = """Your lesson %s has not been rescheduled.""" % (lesson.title,)
        notification.text = msg
        notification.save()

        notification_item = NotificationItemRenderer.render_notification_item(notification)

        unread_notification_count = Notification.objects.filter(target_user_id=target_user_id,read=0).count()

        push_message = {
            'id': notification.id,
            'action': 'new',
            'target_user': target_user_id,
            'notif_item': notification_item,
            'object_id': object_id,
            'url': url,
            'status': 'SUCCESS',
            'unread_count': unread_notification_count
        }

        NotificationPublisher.publish_notification('PN_CHANNEL',push_message)

    @classmethod
    def handle_lesson_missed(cls,notif_type,object_id,created_by_id,target_user_id,lesson_type):
        notification = Notification()
        notification.notif_type = notif_type
        notification.object_class = LiveLesson.__name__ if lesson_type == 0 else WrittenLesson
        notification.object_id = object_id
        notification.created_by_id = created_by_id
        notification.target_user_id = target_user_id
        notification.read = 0 ###0 means unread

        ###Job details url.

        lesson = None
        url = None
        if lesson_type == 0:
            live_lesson = LiveLesson.objects.filter(pk=object_id)
            if not live_lesson.exists():
                return None
            lesson = live_lesson.first()
            url = reverse('live_lesson',kwargs={ 'pk': object_id })
        else:
            written_lesson = WrittenLesson.objects.filter(pk=object_id)
            if not written_lesson.exists():
                return None
            lesson = written_lesson.first()
            url = reverse('written_lesson_request_view')+'?lesson=%s' % (str(object_id),)

        if not lesson or not url:
            return

        notification.url = url

        ###Create notification message
        msg = """Your have missed the lesson %s.""" % (lesson.title,)
        notification.text = msg
        notification.save()

        notification_item = NotificationItemRenderer.render_notification_item(notification)

        unread_notification_count = Notification.objects.filter(target_user_id=target_user_id,read=0).count()

        push_message = {
            'id': notification.id,
            'action': 'new',
            'target_user': target_user_id,
            'notif_item': notification_item,
            'object_id': object_id,
            'url': url,
            'status': 'SUCCESS',
            'unread_count': unread_notification_count
        }

        NotificationPublisher.publish_notification('PN_CHANNEL',push_message)
    @classmethod
    def handle_lesson_feedback_received(cls,notif_type,object_id,created_by_id,target_user_id,lesson_type):
        notification = Notification()
        notification.notif_type = notif_type
        notification.object_class = LiveLesson.__name__ if lesson_type == 0 else WrittenLesson
        notification.object_id = object_id
        notification.created_by_id = created_by_id
        notification.target_user_id = target_user_id
        notification.read = 0 ###0 means unread

        ###Job details url.

        lesson = None
        url = None
        if lesson_type == 0:
            live_lesson = LiveLesson.objects.filter(pk=object_id)
            if not live_lesson.exists():
                return None
            lesson = live_lesson.first()
            url = reverse('live_lesson',kwargs={ 'pk': object_id })
        else:
            written_lesson = WrittenLesson.objects.filter(pk=object_id)
            if not written_lesson.exists():
                return None
            lesson = written_lesson.first()
            url = reverse('written_lesson_request_view')+'?lesson=%s' % (str(object_id),)

        if not lesson or not url:
            return

        notification.url = url

        ###Create notification message
        msg = """Feedback received for the lesson %s.""" % (lesson.title,)
        notification.text = msg
        notification.save()

        notification_item = NotificationItemRenderer.render_notification_item(notification)

        unread_notification_count = Notification.objects.filter(target_user_id=target_user_id,read=0).count()

        push_message = {
            'id': notification.id,
            'action': 'new',
            'target_user': target_user_id,
            'notif_item': notification_item,
            'object_id': object_id,
            'url': url,
            'status': 'SUCCESS',
            'unread_count': unread_notification_count
        }

        NotificationPublisher.publish_notification('PN_CHANNEL',push_message)

    @classmethod
    def handle_lesson_time_extended(cls,notif_type,object_id,created_by_id,target_user_id,lesson_type,extra_time):
        notification = Notification()
        notification.notif_type = notif_type
        notification.object_class = LiveLesson.__name__ if lesson_type == 0 else WrittenLesson
        notification.object_id = object_id
        notification.created_by_id = created_by_id
        notification.target_user_id = target_user_id
        notification.read = 0 ###0 means unread

        ###Job details url.

        lesson = None
        url = None
        if lesson_type == 0:
            live_lesson = LiveLesson.objects.filter(pk=object_id)
            if not live_lesson.exists():
                return None
            lesson = live_lesson.first()
            url = reverse('live_lesson',kwargs={ 'pk': object_id })
        else:
            written_lesson = WrittenLesson.objects.filter(pk=object_id)
            if not written_lesson.exists():
                return None
            lesson = written_lesson.first()
            url = reverse('written_lesson_request_view')+'?lesson=%s' % (str(object_id),)

        if not lesson or not url:
            return

        notification.url = url

        ###Create notification message
        msg = """Lesson: %s has been extended for %s min""" % (lesson.title, extra_time)
        notification.text = msg
        notification.save()

        notification_item = NotificationItemRenderer.render_notification_item(notification)

        unread_notification_count = Notification.objects.filter(target_user_id=target_user_id,read=0).count()

        push_message = {
            'id': notification.id,
            'action': 'new',
            'target_user': target_user_id,
            'notif_item': notification_item,
            'object_id': object_id,
            'url': url,
            'status': 'SUCCESS',
            'unread_count': unread_notification_count
        }

        NotificationPublisher.publish_notification('PN_CHANNEL',push_message)
