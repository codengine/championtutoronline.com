import json
from core.generics.redis_client import RedisClient
from core.models3 import ErrorLog

__author__ = 'Sohel'

class NotificationPublisher(object):
    @classmethod
    def publish_notification(cls, channel, message):
        ###Now publish the status with data in MESSAGE_SENT signal.
        try:
            redis_client = RedisClient.Instance()
            redis_client.publish_channel(channel, json.dumps(message))
        except Exception as msg:
            error_log = ErrorLog()
            error_log.url = ""
            error_log.stacktrace = str(msg)
            error_log.save()
