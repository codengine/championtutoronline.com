from django.core.urlresolvers import reverse
from core.models3 import LiveLesson, WrittenLesson, Question
from notification.models import Notification
from notification.utils.notification_item_renderer import NotificationItemRenderer
from notification.utils.notification_publisher import NotificationPublisher


class NotificationQuestionUtil(object):
    def __init__(self):
        pass

    @classmethod
    def handle_question_reply(cls, notif_type, object_id, created_by, target_user_id, qanswer):
        notification = Notification()
        notification.notif_type = notif_type
        notification.object_class = Question.__name__
        notification.object_id = object_id
        notification.created_by_id = created_by.pk
        notification.target_user_id = target_user_id
        notification.read = 0 ###0 means unread

        msg = """%s has posted a reply: \"%s\" """ % (created_by.champuser.fullname, qanswer.answer)
        title = """%s has posted a reply""" % (created_by.champuser.fullname)
        notification.title = title
        notification.text = msg


        ### question details url
        url = reverse('question_details_view', kwargs={'pk': object_id})
        notification.url = url
        notification.save()

        notification_item = NotificationItemRenderer.render_notification_item(notification)

        unread_notification_count = Notification.objects.filter(target_user_id=target_user_id, read=0).count()

        push_message = {
            'id': notification.id,
            'action': 'new',
            'target_user': target_user_id,
            'notif_item': notification_item,
            'object_id': object_id,
            'url': url,
            'status': 'SUCCESS',
            'unread_count': unread_notification_count
        }

        NotificationPublisher.publish_notification('PN_CHANNEL', push_message)

    @classmethod
    def handle_question_reply_like(cls, notif_type, object_id, created_by, target_user_id, action, qanswer):
        notification = Notification()
        notification.notif_type = notif_type
        notification.object_class = Question.__name__
        notification.object_id = object_id
        notification.created_by_id = created_by.pk
        notification.target_user_id = target_user_id
        notification.read = 0 ###0 means unread

        msg = """%s has %sd a reply: \"%s\" """ % (created_by.champuser.fullname, action, qanswer.answer)
        title = """%s has %sd a reply""" % (created_by.champuser.fullname, action)
        notification.title = title
        notification.text = msg
        ### question details url
        url = reverse('question_details_view', kwargs={'pk': object_id})
        notification.url = url
        notification.save()

        notification_item = NotificationItemRenderer.render_notification_item(notification)

        unread_notification_count = Notification.objects.filter(target_user_id=target_user_id, read=0).count()

        push_message = {
            'id': notification.id,
            'action': 'new',
            'target_user': target_user_id,
            'notif_item': notification_item,
            'object_id': object_id,
            'url': url,
            'status': 'SUCCESS',
            'unread_count': unread_notification_count
        }

        NotificationPublisher.publish_notification('PN_CHANNEL', push_message)

    @classmethod
    def handle_question_reply_edit(cls, notif_type, object_id, created_by, target_user_id, qanswer):
        notification = Notification()
        notification.notif_type = notif_type
        notification.object_class = Question.__name__
        notification.object_id = object_id
        notification.created_by_id = created_by.pk
        notification.target_user_id = target_user_id
        notification.read = 0 ###0 means unread

        msg = """%s has edited a reply: \"%s\" """ % (created_by.champuser.fullname, qanswer.answer)
        title = """%s has edited a reply""" % (created_by.champuser.fullname)
        notification.title = title
        notification.text = msg
        ### question details url
        url = reverse('question_details_view', kwargs={'pk': object_id})
        notification.url = url
        notification.save()

        notification_item = NotificationItemRenderer.render_notification_item(notification)

        unread_notification_count = Notification.objects.filter(target_user_id=target_user_id, read=0).count()

        push_message = {
            'id': notification.id,
            'action': 'new',
            'target_user': target_user_id,
            'notif_item': notification_item,
            'object_id': object_id,
            'url': url,
            'status': 'SUCCESS',
            'unread_count': unread_notification_count
        }

        NotificationPublisher.publish_notification('PN_CHANNEL', push_message)

    @classmethod
    def handle_question_edit(cls, notif_type, question_object, created_by, target_user_id):
        notification = Notification()
        notification.notif_type = notif_type
        notification.object_class = Question.__name__
        notification.object_id = question_object.pk
        notification.created_by_id = created_by.pk
        notification.target_user_id = target_user_id
        notification.read = 0 ###0 means unread

        msg = """%s has edited a question: \"%s\" """ % (created_by.champuser.fullname, question_object.title)
        title = """%s has edited a question""" % (created_by.champuser.fullname)
        notification.title = title
        notification.text = msg
        ### question details url
        url = reverse('question_details_view', kwargs={'pk': question_object.pk})
        notification.url = url
        notification.save()

        notification_item = NotificationItemRenderer.render_notification_item(notification)

        unread_notification_count = Notification.objects.filter(target_user_id=target_user_id, read=0).count()

        push_message = {
            'id': notification.id,
            'action': 'new',
            'target_user': target_user_id,
            'notif_item': notification_item,
            'object_id': question_object.pk,
            'url': url,
            'status': 'SUCCESS',
            'unread_count': unread_notification_count
        }

        NotificationPublisher.publish_notification('PN_CHANNEL', push_message)

    @classmethod
    def handle_question_reply_comment(cls, notif_type, object_id, created_by, target_user_id, qcomment):
        notification = Notification()
        notification.notif_type = notif_type
        notification.object_class = Question.__name__
        notification.object_id = object_id
        notification.created_by_id = created_by.pk
        notification.target_user_id = target_user_id
        notification.read = 0 ###0 means unread

        msg = """%s has commented on a reply: \"%s\" """ % (created_by.champuser.fullname, qcomment.comment)
        title = """%s has commented on a reply""" % (created_by.champuser.fullname)
        notification.title = title
        notification.text = msg
        ### question details url
        url = reverse('question_details_view', kwargs={'pk': object_id})
        notification.url = url
        notification.save()

        notification_item = NotificationItemRenderer.render_notification_item(notification)

        unread_notification_count = Notification.objects.filter(target_user_id=target_user_id, read=0).count()

        push_message = {
            'id': notification.id,
            'action': 'new',
            'target_user': target_user_id,
            'notif_item': notification_item,
            'object_id': object_id,
            'url': url,
            'status': 'SUCCESS',
            'unread_count': unread_notification_count
        }

        NotificationPublisher.publish_notification('PN_CHANNEL', push_message)

    @classmethod
    def handle_question_post(cls, notif_type, question_object, created_by, target_user_id, subject):
        notification = Notification()
        notification.notif_type = notif_type
        notification.object_class = Question.__name__
        notification.object_id = question_object.pk
        notification.created_by_id = created_by.pk
        notification.target_user_id = target_user_id
        notification.read = 0 ###0 means unread

        msg = """Qns: %s""" % (subject)
        notification.text = msg
        notification.title = msg
        ### question details url
        url = reverse('question_details_view', kwargs={'pk': question_object.pk})
        notification.url = url
        notification.save()

        notification_item = NotificationItemRenderer.render_notification_item(notification)

        unread_notification_count = Notification.objects.filter(target_user_id=target_user_id, read=0).count()

        push_message = {
            'id': notification.id,
            'action': 'new',
            'target_user': target_user_id,
            'notif_item': notification_item,
            'object_id': question_object.pk,
            'url': url,
            'status': 'SUCCESS',
            'unread_count': unread_notification_count
        }

        NotificationPublisher.publish_notification('PN_CHANNEL', push_message)
