from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from core.models3 import Job
from notification.models import Notification
from notification.utils.notification_item_renderer import NotificationItemRenderer
from notification.utils.notification_publisher import NotificationPublisher
from payment.models import Wallet

__author__ = 'Sohel'

class NotificationPaymentUtil(object):
    def __init__(self):
        pass

    @classmethod
    def handle_payment_processed(cls,notif_type,object_id,created_by_id,target_user_id):
        notification = Notification()
        notification.notif_type = notif_type
        notification.object_class = Job.__name__
        notification.object_id = object_id
        notification.created_by_id = created_by_id
        notification.target_user_id = target_user_id
        notification.read = 0 ###0 means unread

        ###Job details url.
        url = reverse('job_post_details',kwargs={ 'pk': object_id })
        notification.url = url

        job = Job.objects.filter(pk=object_id)
        if not job.exists():
            return None

        job = job.first()

        ###Create notification message
        msg = """The payment has been processed successfully for the job %s""" % (job.title,)
        notification.text = msg
        notification.save()

        notification_item = NotificationItemRenderer.render_notification_item(notification)

        unread_notification_count = Notification.objects.filter(target_user_id=target_user_id,read=0).count()

        push_message = {
            'id': notification.id,
            'action': 'new',
            'target_user': target_user_id,
            'notif_item': notification_item,
            'object_id': object_id,
            'url': url,
            'status': 'SUCCESS',
            'unread_count': unread_notification_count
        }

        NotificationPublisher.publish_notification('PN_CHANNEL',push_message)
    @classmethod
    def handle_payment_failed(cls,notif_type,object_id,created_by_id,target_user_id):
        notification = Notification()
        notification.notif_type = notif_type
        notification.object_class = Job.__name__
        notification.object_id = object_id
        notification.created_by_id = created_by_id
        notification.target_user_id = target_user_id
        notification.read = 0 ###0 means unread

        ###Job details url.
        url = reverse('job_post_details',kwargs={ 'pk': object_id })
        notification.url = url

        job = Job.objects.filter(pk=object_id)
        if not job.exists():
            return None

        job = job.first()

        ###Create notification message
        msg = """The payment was not successful for the job %s""" % (job.title,)
        notification.text = msg
        notification.save()

        notification_item = NotificationItemRenderer.render_notification_item(notification)

        unread_notification_count = Notification.objects.filter(target_user_id=target_user_id,read=0).count()

        push_message = {
            'id': notification.id,
            'action': 'new',
            'target_user': target_user_id,
            'notif_item': notification_item,
            'object_id': object_id,
            'url': url,
            'status': 'SUCCESS',
            'unread_count': unread_notification_count
        }

        NotificationPublisher.publish_notification('PN_CHANNEL',push_message)

    @classmethod
    def handle_payment_withdraw_request_accepted(cls,notif_type,object_id,created_by_id,target_user_id,currency,amount):
        notification = Notification()
        notification.notif_type = notif_type
        notification.object_class = Wallet.__name__
        notification.object_id = object_id
        notification.created_by_id = created_by_id
        notification.target_user_id = target_user_id
        notification.read = 0 ###0 means unread

        ###Create notification message
        msg = """Your request to withdraw $%s (%s) has been accepted. You will be notified when it will be processed.""" % (currency, amount,)
        notification.text = msg
        notification.save()

        notification_item = NotificationItemRenderer.render_notification_item(notification)

        unread_notification_count = Notification.objects.filter(target_user_id=target_user_id,read=0).count()

        push_message = {
            'id': notification.id,
            'action': 'new',
            'target_user': target_user_id,
            'notif_item': notification_item,
            'object_id': object_id,
            'url': reverse('notification_list_view'),
            'status': 'SUCCESS',
            'unread_count': unread_notification_count
        }

        NotificationPublisher.publish_notification('PN_CHANNEL',push_message)
    @classmethod
    def handle_payment_withdraw_processed(cls,notif_type,object_id,created_by_id,target_user_id,currency,amount):
        notification = Notification()
        notification.notif_type = notif_type
        notification.object_class = Wallet.__name__
        notification.object_id = object_id
        notification.created_by_id = created_by_id
        notification.target_user_id = target_user_id
        notification.read = 0 ###0 means unread

        ###Create notification message
        msg = """Your request to withdraw $%s (%s) has been processed.""" % (currency, amount,)
        notification.text = msg
        notification.save()

        notification_item = NotificationItemRenderer.render_notification_item(notification)

        unread_notification_count = Notification.objects.filter(target_user_id=target_user_id,read=0).count()

        push_message = {
            'id': notification.id,
            'action': 'new',
            'target_user': target_user_id,
            'notif_item': notification_item,
            'object_id': object_id,
            'url': reverse('notification_list_view'),
            'status': 'SUCCESS',
            'unread_count': unread_notification_count
        }

        NotificationPublisher.publish_notification('PN_CHANNEL',push_message)
