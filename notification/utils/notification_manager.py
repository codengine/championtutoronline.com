from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from core.models3 import JobInvitation, Job
from notification.models import Notification
from notification.utils.notification_invitation_util import NotificationInvitationUtil
from notification.utils.notification_job_application import NotificationJobApplication
from notification.utils.notification_job_offer_util import NotificationJobOfferUtil
from notification.utils.notification_lesson_util import NotificationLessonUtil
from notification.utils.notification_payment_util import NotificationPaymentUtil
from notification.utils.notification_publisher import NotificationPublisher
from notification.utils.notification_question_util import NotificationQuestionUtil
from notification.utils.notification_recommendation_util import NotificationRecommendationUtil

__author__ = 'Sohel'

class NotificationManager(object):
    def __init__(self):
        pass

    """
    notif_type list:
    job_invited, invite_applied, invite_rejected, recommend_applied, job_application_rejected, job_application_shortlisted, job_offered, application_withdrawn, job_hired,
    offer_expired, offer_accepted, offer_rejected, payment_processed, lesson_scheduled,
    payment_failed, lesson_started, lesson_schedule_failed, lesson_rescheduled, lesson_missed, payment_withdraw_request_accepted, payment_withdraw_processed
    lesson_feedback_received, question_reply, question_reply_like, question_reply_edit, question_edit, question_reply_comment, question_post
    """
    @classmethod
    def push_notification(cls,notif_type,object_id,created_by_id,target_user_id,**kwargs):
        if notif_type == "job_invited":
            NotificationInvitationUtil.handle_job_invited(notif_type,object_id,created_by_id,target_user_id)
        elif notif_type == "invite_applied":
            NotificationInvitationUtil.handle_invite_applied(notif_type,object_id,created_by_id,target_user_id)
        elif notif_type == "invite_rejected":
            NotificationInvitationUtil.handle_invite_rejected(notif_type,object_id,created_by_id,target_user_id)
        elif notif_type == "recommend_applied":
            NotificationRecommendationUtil.handle_recommend_applied(notif_type,object_id,created_by_id,target_user_id)
        elif notif_type == "job_application_rejected":
            NotificationJobApplication.handle_job_application_rejected(notif_type,object_id,created_by_id,target_user_id)
        elif notif_type == "application_withdrawn":
            NotificationJobApplication.handle_job_application_withdrawn(notif_type,object_id,created_by_id,target_user_id)
        elif notif_type == "job_application_shortlisted":
            NotificationJobApplication.handle_job_application_shortlisted(notif_type,object_id,created_by_id,target_user_id)
        elif notif_type == "job_offered":
            NotificationJobOfferUtil.handle_job_offered(notif_type,object_id,created_by_id,target_user_id)
        elif notif_type == "offer_expired":
            NotificationJobOfferUtil.handle_offer_expired(notif_type,object_id,created_by_id,target_user_id)
        elif notif_type == "offer_accepted":
            NotificationJobOfferUtil.handle_offer_accepted(notif_type,object_id,created_by_id,target_user_id)
        elif notif_type == "job_hired":
            NotificationJobOfferUtil.handle_job_hired(notif_type,object_id,created_by_id,target_user_id)
        elif notif_type == "offer_rejected":
            NotificationJobOfferUtil.handle_offer_rejected(notif_type,object_id,created_by_id,target_user_id)
        elif notif_type == "payment_processed":
            NotificationPaymentUtil.handle_payment_processed(notif_type,object_id,created_by_id,target_user_id)
        elif notif_type == "payment_failed":
            NotificationPaymentUtil.handle_payment_failed(notif_type,object_id,created_by_id,target_user_id)
        elif notif_type == "payment_withdraw_request_accepted":
            NotificationPaymentUtil.handle_payment_withdraw_request_accepted(notif_type,object_id,created_by_id,target_user_id,kwargs.get('currency'), kwargs.get('amount'))
        elif notif_type == "payment_withdraw_processed":
            NotificationPaymentUtil.handle_payment_withdraw_processed(notif_type,object_id,created_by_id,target_user_id,kwargs.get('currency'), kwargs.get('amount'))
        elif notif_type == "lesson_scheduled":
            NotificationLessonUtil.handle_lesson_scheduled(notif_type,object_id,created_by_id,target_user_id,kwargs.get('lesson_type'))
        elif notif_type == "lesson_started":
            NotificationLessonUtil.handle_lesson_started(notif_type,object_id,created_by_id,target_user_id,kwargs.get('lesson_type'))
        elif notif_type == "lesson_rescheduled":
            NotificationLessonUtil.handle_lesson_rescheduled(notif_type,object_id,created_by_id,target_user_id,kwargs.get('lesson_type'),kwargs.get('reschedule_time'))
        elif notif_type == "lesson_schedule_failed":
            NotificationLessonUtil.handle_lesson_schedule_failed(notif_type,object_id,created_by_id,target_user_id,kwargs.get('lesson_type'))
        elif notif_type == "lesson_missed":
            NotificationLessonUtil.handle_lesson_missed(notif_type,object_id,created_by_id,target_user_id,kwargs.get('lesson_type'))
        elif notif_type == "lesson_feedback_received":
            NotificationLessonUtil.handle_lesson_feedback_received(notif_type,object_id,created_by_id,target_user_id,kwargs.get('lesson_type'))
        elif notif_type == "lesson_time_extended":
            NotificationLessonUtil.handle_lesson_time_extended(notif_type,object_id,created_by_id,target_user_id,kwargs.get('lesson_type'),kwargs.get('extra_time'))
        elif notif_type == "question_reply":
            NotificationQuestionUtil.handle_question_reply(notif_type, object_id, created_by_id, target_user_id, kwargs.get('answer'))
        elif notif_type == "question_reply_like":
            NotificationQuestionUtil.handle_question_reply_like(notif_type, object_id, created_by_id, target_user_id, kwargs.get('action'), kwargs.get('answer'))
        elif notif_type == "question_reply_edit":
            NotificationQuestionUtil.handle_question_reply_edit(notif_type, object_id, created_by_id, target_user_id, kwargs.get('answer'))
        elif notif_type == "question_edit":
            NotificationQuestionUtil.handle_question_edit(notif_type, object_id, created_by_id, target_user_id)
        elif notif_type == "question_reply_comment":
            NotificationQuestionUtil.handle_question_reply_comment(notif_type, object_id, created_by_id, target_user_id, kwargs.get('comment'))
        elif notif_type == "question_post":
            NotificationQuestionUtil.handle_question_post(notif_type, object_id, created_by_id, target_user_id,  kwargs.get('subject'))


    @classmethod
    def notify_change(cls,action,object_id,target_user_id): ###Action can be modify or delete
        unread_notification_count = Notification.objects.filter(target_user_id=target_user_id,read=0).count()
        push_message = {
            'id': object_id,
            'action': action,
            'target_user': target_user_id,
            'object_id': object_id,
            'status': 'SUCCESS',
            'unread_count': unread_notification_count
        }

        NotificationPublisher.publish_notification('PN_CHANNEL',push_message)

    @classmethod
    def notify_all_read(cls,target_user_id): ###Action can be modify or delete
        push_message = {
            'action': 'modify_all',
            'target_user': target_user_id,
            'status': 'SUCCESS'
        }

        NotificationPublisher.publish_notification('PN_CHANNEL',push_message)


