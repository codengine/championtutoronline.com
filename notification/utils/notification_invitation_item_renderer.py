from django.core.urlresolvers import reverse
from django.template import loader, Context
from core.models3 import Job, Question
from notification.models import Notification

__author__ = 'Sohel'

class NotificationInvitationItemRenderer(object):
    @classmethod
    def render_invitation_item(cls, notification_object, *args, **kwargs):
        job = Job.objects.filter(pk=notification_object.object_id)
        if job.exists():
            template_name = "ajax/notification_dd_item.html"
            template = loader.get_template(template_name)
            url = reverse('job_post_details',kwargs={ 'pk': notification_object.object_id })
            context = {
                "notification_target_link": url,
                "notification_description": notification_object.text,
                "notification_title": notification_object.title,
                "notification_date": notification_object.notification_date,
                "read": True if notification_object.read == 1 else False,
                "is_read": notification_object.read,
                "object_id": notification_object.pk
            }
            cntxt = Context(context)
            rendered = template.render(cntxt)
            return rendered

    @classmethod
    def render_question_item(cls, notification_object, *args, **kwargs):
        question = Question.objects.filter(pk=notification_object.object_id)
        if question.exists():
            template_name = "ajax/notification_dd_item.html"
            template = loader.get_template(template_name)
            url = reverse('question_details_view', kwargs={'pk': notification_object.object_id })
            context = {
                "notification_target_link": url,
                "notification_description": notification_object.text,
                "notification_title": notification_object.title,
                "notification_date": notification_object.notification_date,
                "read": True if notification_object.read == 1 else False,
                "is_read": notification_object.read,
                "object_id": notification_object.pk
            }
            cntxt = Context(context)
            rendered = template.render(cntxt)
            return rendered
