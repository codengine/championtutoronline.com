from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from core.models3 import Job
from notification.models import Notification
from notification.utils.notification_item_renderer import NotificationItemRenderer
from notification.utils.notification_publisher import NotificationPublisher

__author__ = 'Sohel'

class NotificationJobOfferUtil(object):
    def __init__(self):
        pass

    @classmethod
    def handle_job_offered(cls,notif_type,object_id,created_by_id,target_user_id):
        notification = Notification()
        notification.notif_type = notif_type
        notification.object_class = Job.__name__
        notification.object_id = object_id
        notification.created_by_id = created_by_id
        notification.target_user_id = target_user_id
        notification.read = 0 ###0 means unread

        ###Job details url.
        url = reverse('job_post_details',kwargs={ 'pk': object_id })
        notification.url = url

        job = Job.objects.filter(pk=object_id)
        if not job.exists():
            return None

        job = job.first()

        ###Create notification message
        created_by = User.objects.get(pk=created_by_id)
        msg = """You have job offer from %s for the following job %s""" % (created_by.champuser.fullname,job.title)
        notification.text = msg
        notification.save()

        notification_item = NotificationItemRenderer.render_notification_item(notification)

        unread_notification_count = Notification.objects.filter(target_user_id=target_user_id,read=0).count()

        push_message = {
            'id': notification.id,
            'action': 'new',
            'target_user': target_user_id,
            'notif_item': notification_item,
            'object_id': object_id,
            'url': url,
            'status': 'SUCCESS',
            'unread_count': unread_notification_count
        }

        NotificationPublisher.publish_notification('PN_CHANNEL',push_message)
    @classmethod
    def handle_offer_expired(cls,notif_type,object_id,created_by_id,target_user_id):
        notification = Notification()
        notification.notif_type = notif_type
        notification.object_class = Job.__name__
        notification.object_id = object_id
        notification.created_by_id = created_by_id
        notification.target_user_id = target_user_id
        notification.read = 0 ###0 means unread

        ###Job details url.
        url = reverse('job_post_details',kwargs={ 'pk': object_id })
        notification.url = url

        job = Job.objects.filter(pk=object_id)
        if not job.exists():
            return None

        job = job.first()

        ###Create notification message
        created_by = User.objects.get(pk=created_by_id)
        msg = """The job offer %s from %s has expired""" % (job.title,created_by.champuser.fullname)
        notification.text = msg
        notification.save()

        notification_item = NotificationItemRenderer.render_notification_item(notification)

        unread_notification_count = Notification.objects.filter(target_user_id=target_user_id,read=0).count()

        push_message = {
            'id': notification.id,
            'action': 'new',
            'target_user': target_user_id,
            'notif_item': notification_item,
            'object_id': object_id,
            'url': url,
            'status': 'SUCCESS',
            'unread_count': unread_notification_count
        }

        NotificationPublisher.publish_notification('PN_CHANNEL',push_message)
    @classmethod
    def handle_offer_accepted(cls,notif_type,object_id,created_by_id,target_user_id):
        notification = Notification()
        notification.notif_type = notif_type
        notification.object_class = Job.__name__
        notification.object_id = object_id
        notification.created_by_id = created_by_id
        notification.target_user_id = target_user_id
        notification.read = 0 ###0 means unread

        ###Job details url.
        url = reverse('job_post_details',kwargs={ 'pk': object_id })
        notification.url = url

        job = Job.objects.filter(pk=object_id)
        if not job.exists():
            return None

        job = job.first()

        ###Create notification message
        created_by = User.objects.get(pk=created_by_id)
        msg = """%s has accepted the job offer %s from you""" % (created_by.champuser.fullname,job.title)
        notification.text = msg
        notification.save()

        notification_item = NotificationItemRenderer.render_notification_item(notification)

        unread_notification_count = Notification.objects.filter(target_user_id=target_user_id,read=0).count()

        push_message = {
            'id': notification.id,
            'action': 'new',
            'target_user': target_user_id,
            'notif_item': notification_item,
            'object_id': object_id,
            'url': url,
            'status': 'SUCCESS',
            'unread_count': unread_notification_count
        }

        NotificationPublisher.publish_notification('PN_CHANNEL',push_message)

    @classmethod
    def handle_job_hired(cls,notif_type,object_id,created_by_id,target_user_id):
        notification = Notification()
        notification.notif_type = notif_type
        notification.object_class = Job.__name__
        notification.object_id = object_id
        notification.created_by_id = created_by_id
        notification.target_user_id = target_user_id
        notification.read = 0 ###0 means unread

        ###Job details url.
        url = reverse('job_post_details',kwargs={ 'pk': object_id })
        notification.url = url

        job = Job.objects.filter(pk=object_id)
        if not job.exists():
            return None

        job = job.first()

        ###Create notification message
        created_by = User.objects.get(pk=created_by_id)
        msg = """You have been hired by %s for job %s""" % (created_by.champuser.fullname,job.title)
        notification.text = msg
        notification.save()

        notification_item = NotificationItemRenderer.render_notification_item(notification)

        unread_notification_count = Notification.objects.filter(target_user_id=target_user_id,read=0).count()

        push_message = {
            'id': notification.id,
            'action': 'new',
            'target_user': target_user_id,
            'notif_item': notification_item,
            'object_id': object_id,
            'url': url,
            'status': 'SUCCESS',
            'unread_count': unread_notification_count
        }

        NotificationPublisher.publish_notification('PN_CHANNEL',push_message)

    @classmethod
    def handle_offer_rejected(cls,notif_type,object_id,created_by_id,target_user_id):
        notification = Notification()
        notification.notif_type = notif_type
        notification.object_class = Job.__name__
        notification.object_id = object_id
        notification.created_by_id = created_by_id
        notification.target_user_id = target_user_id
        notification.read = 0 ###0 means unread

        ###Job details url.
        url = reverse('job_post_details',kwargs={ 'pk': object_id })
        notification.url = url

        job = Job.objects.filter(pk=object_id)
        if not job.exists():
            return None

        job = job.first()

        ###Create notification message
        created_by = User.objects.get(pk=created_by_id)
        msg = """%s has rejected the job offer %s""" % (created_by.champuser.fullname,job.title)
        notification.text = msg
        notification.save()

        notification_item = NotificationItemRenderer.render_notification_item(notification)

        unread_notification_count = Notification.objects.filter(target_user_id=target_user_id,read=0).count()

        push_message = {
            'id': notification.id,
            'action': 'new',
            'target_user': target_user_id,
            'notif_item': notification_item,
            'object_id': object_id,
            'url': url,
            'status': 'SUCCESS',
            'unread_count': unread_notification_count
        }

        NotificationPublisher.publish_notification('PN_CHANNEL',push_message)
