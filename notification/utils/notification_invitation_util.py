from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from core.enums import JobInvitationStatus
from core.models3 import JobInvitation, Job, JobApplication
from notification.models import Notification
from notification.utils.new_items_util import NewItemRenderer
from notification.utils.notification_item_renderer import NotificationItemRenderer
from notification.utils.notification_publisher import NotificationPublisher

__author__ = 'Sohel'

class NotificationInvitationUtil(object):
    def __init__(self):
        pass
    
    @classmethod
    def handle_job_invited(cls,notif_type,object_id,created_by_id,target_user_id):
        notification = Notification()
        notification.notif_type = notif_type
        notification.object_class = Job.__name__
        notification.object_id = object_id
        notification.created_by_id = created_by_id
        notification.target_user_id = target_user_id
        notification.read = 0 ###0 means unread

        ###Job details url.
        url = reverse('job_post_details',kwargs={ 'pk': object_id })
        notification.url = url

        job = Job.objects.filter(pk=object_id)
        if not job.exists():
            return None

        job = job.first()

        ###Create notification message
        created_by = User.objects.get(pk=created_by_id)
        msg = """You have been invited to apply to the job: %s by %s """ % (job.title, created_by.champuser.fullname)
        notification.text = msg
        notification.save()

        notification_item = NotificationItemRenderer.render_notification_item(notification)

        unread_notification_count = Notification.objects.filter(target_user_id=target_user_id,read=0).count()

        push_message = {
            'id': notification.id,
            'action': 'new',
            'target_user': target_user_id,
            'notif_item': notification_item,
            'object_id': object_id,
            'url': url,
            'status': 'SUCCESS',
            'unread_count': unread_notification_count,
            'active_new': NewItemRenderer.render_new_items_count_profile(target_user_id)
        }

        NotificationPublisher.publish_notification('PN_CHANNEL',push_message)

    @classmethod
    def handle_invite_applied(cls,notif_type,object_id,created_by_id,target_user_id): ###object is is the application id

        job = Job.objects.filter(pk=object_id)
        if not job.exists():
            return None

        job = job.first()

        url = reverse('job_post_details',kwargs={ 'pk': object_id })

        notification_objects = Notification.objects.filter(notif_type=notif_type,object_class=Job.__name__,object_id=object_id)
        if notification_objects.exists():
            notification = notification_objects.first()
            notification.read = 0
        else:
            notification = Notification()
            notification.notif_type = notif_type
            notification.object_class = Job.__name__
            notification.object_id = object_id  ###It will keep job is.
            notification.created_by_id = created_by_id
            notification.target_user_id = target_user_id
            notification.read = 0 ###0 means unread

            ###Job details url.
            notification.url = url

        ###Create notification message
        created_by = User.objects.get(pk=created_by_id)

        applicants = job.applications.all().order_by('-date_created')
        if applicants.count() == 1:
            msg = """%s applied to your job: %s""" % (created_by.champuser.fullname,job.title)
        else:
            msg = """%s and %s other tutor applied to your job: %s""" % (created_by.champuser.fullname, str(applicants.count() -1), job.title)
        notification.text = msg
        notification.save()

        notification_item = NotificationItemRenderer.render_notification_item(notification)

        unread_notification_count = Notification.objects.filter(target_user_id=target_user_id,read=0).count()

        push_message = {
            'id': notification.id,
            'action': 'new',
            'target_user': target_user_id,
            'notif_item': notification_item,
            'object_id': notification.pk,
            'url': url,
            'status': 'SUCCESS',
            'unread_count': unread_notification_count,
            'active_new': NewItemRenderer.render_new_items_count_profile(target_user_id)
        }

        NotificationPublisher.publish_notification('PN_CHANNEL',push_message)

    @classmethod
    def handle_invite_rejected(cls,notif_type,object_id,created_by_id,target_user_id): ###object is is the application id

        job = Job.objects.filter(pk=object_id)
        if not job.exists():
            return None

        job = job.first()

        url = reverse('job_post_details',kwargs={ 'pk': object_id })

        notification_objects = Notification.objects.filter(notif_type=notif_type,object_class=Job.__name__,object_id=object_id)
        if notification_objects.exists():
            notification = notification_objects.first()
            notification.read = 0
        else:
            notification = Notification()
            notification.notif_type = notif_type
            notification.object_class = Job.__name__
            notification.object_id = object_id  ###It will keep job is.
            notification.created_by_id = created_by_id
            notification.target_user_id = target_user_id
            notification.read = 0 ###0 means unread

            ###Job details url.
            notification.url = url

        ###Create notification message
        created_by = User.objects.get(pk=created_by_id)

        applicants = job.applications.all().order_by('-date_created')
        if applicants.count() <= 1:
            msg = """%s rejected your job invitation: %s""" % (created_by.champuser.fullname,job.title)
        else:
            msg = """%s and %s other tutor rejected your job invitation: %s""" % (created_by.champuser.fullname, str(applicants.count() -1), job.title)
        notification.text = msg
        notification.save()

        notification_item = NotificationItemRenderer.render_notification_item(notification)

        unread_notification_count = Notification.objects.filter(target_user_id=target_user_id,read=0).count()

        push_message = {
            'id': notification.id,
            'action': 'new',
            'target_user': target_user_id,
            'notif_item': notification_item,
            'object_id': notification.pk,
            'url': url,
            'status': 'SUCCESS',
            'unread_count': unread_notification_count,
            'active_new': NewItemRenderer.render_new_items_count_profile(target_user_id)
        }

        NotificationPublisher.publish_notification('PN_CHANNEL',push_message)
