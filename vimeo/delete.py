__author__ = 'Sohel'

class DeleteVideoMixin(object):

    DELETE_ENDPOINT = '/videos/{video_id}'

    def delete_video(self, video_id, upgrade_to_1080=False):

        uri = self.DELETE_ENDPOINT.format(video_id=video_id)

        return self.delete(uri)
