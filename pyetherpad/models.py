from django.db import models
from django.contrib.auth.models import User
from core.models3 import *
from core.generics.models import GenericModel

# Create your models here.
class PadAuthor(GenericModel):
    user = models.ForeignKey(User,null=True)
    author_id = models.CharField(max_length=255,blank=False)
    #created_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table=u'pad_author'

class PadGroup(GenericModel):
    user = models.ForeignKey(User,null=True)
    group_id = models.CharField(max_length=255,blank=False)
    #group_created_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table=u'pad_group'

class Pad(GenericModel):
    pad_nid = models.IntegerField()
    pad_id = models.CharField(max_length=255)
    pad_group = models.ForeignKey(PadGroup)
    pad_name = models.CharField(max_length=255)
    pad_authors = models.ManyToManyField(PadAuthor)
    pad_created_by = models.IntegerField(null=True,blank=True)
    #pad_created_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table=u'pad'

class PadLastNid(GenericModel):
    id_or_group = models.CharField(max_length=255)
    nid = models.IntegerField()

    class Meta:
        db_table = 'pad_last_nid'

class PublicPad(GenericModel):
    pad_nid = models.IntegerField()
    pad_created_by = models.ForeignKey(User)
    #pad_create_date = models.DateTimeField(auto_now_add=True,blank=False)

    class Meta:
        db_table=u'public_pad'

class PadSession(GenericModel):
    author_id = models.CharField(max_length=255)
    group_id = models.CharField(max_length=255)
    session_id = models.CharField(max_length=255)
    expired_time = models.DateTimeField()
    #session_created = models.DateTimeField()

    class Meta:
        db_table=u'pad_session'