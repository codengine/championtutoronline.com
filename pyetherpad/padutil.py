__author__ = 'codengine'

from django.conf import settings
from py_etherpad import EtherpadLiteClient
from pyetherpad.models import *
from django.contrib.auth.models import User
from datetime import datetime
from datetime import timedelta
import json
import time
import uuid

class PadUtil:
    def __init__(self):
        self.epclient = EtherpadLiteClient(settings.ETHERPAD_API_KEY, settings.ETHERPAD_API_URL)

    def create_or_get_padauthor(self,author_id,name):
        utype = type(author_id)
        try:
            author_id = int(author_id)
        except Exception,msg:
            author_id = utype(author_id)
        if type(author_id) is int:
            user_objs = User.objects.filter(pk=author_id)
            if user_objs.exists():
                user_obj = user_objs.first()
                pad_author_objs = PadAuthor.objects.filter(author_id=author_id)
                if pad_author_objs.exists():
                    return pad_author_objs.first().author_id
                result = self.epclient.createAuthorIfNotExistsFor(author_id,name)
                #self.authorID = result['authorID']
                ###Now save user to database table.
                pad_author = PadAuthor()
                pad_author.user = user_obj
                pad_author.author_id = result['authorID']
                #pad_author.created_on = datetime.now()
                pad_author.save()
                return result['authorID']
        else:
            result = self.epclient.createAuthorIfNotExistsFor(author_id,name)
            pad_author_objs = PadAuthor.objects.filter(author_id=author_id)
            if pad_author_objs.exists():
                return pad_author_objs[0].author_id
            pad_author = PadAuthor()
            pad_author.author_id = result['authorID']
            #pad_author.created_on = datetime.now()
            pad_author.save()
            return result['authorID']


    def create_or_get_padgroup(self,group_id,user_id=None,demo_pad=False):
        if demo_pad:
            if user_id:
                user_objs = User.objects.filter(pk=user_id)
                if user_objs.exists():
                    user_obj = user_objs.first()
                    pad_group_objs = PadGroup.objects.filter(group_id=group_id)
                    if pad_group_objs.exists():
                        return pad_group_objs.first().group_id
                    result = self.epclient.createGroupIfNotExistsFor(group_id)
                    pad_group = PadGroup()
                    pad_group.user = user_obj
                    pad_group.group_id = result['groupID']
                    pad_group.save()
                    return result['groupID']
            else:
                pad_group_objs = PadGroup.objects.filter(group_id=group_id)
                if pad_group_objs.exists():
                    return pad_group_objs.first().group_id
                result = self.epclient.createGroupIfNotExistsFor(group_id)
                pad_group = PadGroup()
                pad_group.group_id = result['groupID']
                pad_group.save()
                return result['groupID']
        else:
            user_objs = User.objects.filter(pk=user_id)
            if user_objs.exists():
                user_obj = user_objs.first()
                pad_group_objs = PadGroup.objects.filter(group_id=group_id)
                if pad_group_objs:
                    return pad_group_objs.first().group_id
                result = self.epclient.createGroupIfNotExistsFor(group_id)
                #self.authorID = result['authorID']
                ###Now save user to database table.
                pad_group = PadGroup()
                pad_group.user = user_obj
                pad_group.group_id = result['groupID']
                #pad_group.group_created_on = datetime.now()
                pad_group.save()
                return result['groupID']

    def create_group_pad(self,pad_group_id,pad_name=str(uuid.uuid4()),text='',user_id=None,author_id=None):
        result = self.epclient.createGroupPad(pad_group_id,pad_name,text)

        pad_obj = Pad()
        pad_obj.pad_id = result['padID']
        pad_group_objs = PadGroup.objects.filter(group_id=pad_group_id)
        if pad_group_objs.exists():
            pad_group_obj = pad_group_objs.first()

            pad_obj.pad_group = pad_group_obj
            pad_obj.pad_name = pad_name
            if user_id:
                pad_obj.pad_created_by_id = user_id
            #pad_obj.pad_created_on = datetime.now()

            pad_last_nid = PadLastNid.objects.filter(id_or_group=pad_group_id)
            nid = 1
            if pad_last_nid.exists():
                pad_last_nid = pad_last_nid.first()
                nid = pad_last_nid.nid + 1
                pad_last_nid.nid = nid
                pad_last_nid.save()
            else:
                pad_last_nid = PadLastNid()
                pad_last_nid.id_or_group = pad_group_obj.group_id
                pad_last_nid.nid = 1
                pad_last_nid.save()
            pad_obj.pad_nid = nid
            pad_obj.save()
            if author_id:
                pad_author = PadAuthor.objects.filter(author_id=author_id)
                if pad_author.exists():
                    pad_obj.pad_authors.add(pad_author.first())
        return result['padID']

    def create_public_pad(self,pad_id):
        try:
            result = self.epclient.createPad(pad_id)
            return result["padID"]
        except Exception,msg:
            print "Exception occured. Exception message: %s" % str(msg)
            return False

    def make_pad_public(self,pad_id):
        return self.epclient.setPublicStatus(pad_id,"true")

    def rename_group_pad(self,pad_id,name):
        pad_objs = Pad.objects.filter(pad_id=pad_id)
        if not pad_objs:
            return False
        pad_objs[0].pad_name = name
        pad_objs[0].save()
        return True

    def delete_group_pad(self,pad_id):
        Pad.objects.filter(pad_id=pad_id).delete()
        self.epclient.deletePad(pad_id)
        return True

    def getPadAuthorIDFor(self,user_id):
        pad_author_objs = PadAuthor.objects.filter(user_id=user_id)
        if pad_author_objs:
            return pad_author_objs[0].author_id

    def getPadGroupFor(self,user_id):
        pad_group_objs = PadGroup.objects.filter(user_id=user_id)
        if pad_group_objs:
            return pad_group_objs[0].group_id

    def create_session(self,group_id,author_id):
        nowtime = datetime.now()
        one_week_ahead = nowtime + timedelta(days=7)
        one_week_ahead_seconds = time.mktime(one_week_ahead.timetuple())
        result = self.epclient.createSession(group_id,author_id,one_week_ahead_seconds)
        if result.get("sessionID"):
            pad_session_obj = PadSession()
            pad_session_obj.author_id = author_id
            pad_session_obj.group_id = group_id
            pad_session_obj.session_id = result["sessionID"]
            #pad_session_obj.session_created = datetime.now()
            pad_session_obj.expired_time = one_week_ahead
            pad_session_obj.save()
        return result.get("sessionID")

    # {code: 0, message:"ok", data: {authorID: "a.s8oes9dhwrvt0zif", groupID: g.s8oes9dhwrvt0zif, validUntil: 1312201246}}
    # {code: 1, message:"sessionID does not exist", data: null}
    def get_session_info(self,session_id):
        return self.epclient.getSessionInfo(session_id)

    def delete_ep_session(self,session_id):
        self.epclient.deleteSession(session_id)

    def get_ep_client(self):
        return self.epclient