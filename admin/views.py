from django.contrib.auth.models import User
from django.contrib.sessions.models import Session
from django.core.urlresolvers import reverse
from django.db.models.query_utils import Q
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import View
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from core.enums import VerificationStatus
from core.generics.ajaxmixins import JSONResponseMixin
from core.generics.mixins import AdminLoginRequired, AjaxRequestRequired
from core.models3 import ErrorLog, NRICVerification, ChampUser, SystemLog, PaymentLog, GlobalSettings, EmailTemplate
from forms import LoginForm

__author__ = 'Sohel'

class AdminHomeView(AdminLoginRequired, View):
    def get(self,request,*args,**kwargs):
        return HttpResponseRedirect(reverse("admin_dashboard"))

class AdminDashboardView(AdminLoginRequired, View):
    def get(self,request,*args,**kwargs):
        context = {
            "total_users": ChampUser.objects.all().exclude(user__is_staff=True).count(),
            "total_active_users": ChampUser.objects.filter(user__is_active=True).exclude(user__is_staff=True).count(),
            "total_students": ChampUser.objects.filter(type__name="student").exclude(user__is_staff=True).count(),
            "total_active_students": ChampUser.objects.filter(type__name="student",user__is_active=True).exclude(user__is_staff=True).count(),
            "total_tutors": ChampUser.objects.filter(type__name="teacher").exclude(user__is_staff=True).count(),
            "total_active_tutors": ChampUser.objects.filter(type__name="teacher",user__is_active=True).exclude(user__is_staff=True).count(),
            "total_inactive_users": ChampUser.objects.filter(user__is_active=False).exclude(user__is_staff=True).count(),
            "nric_request_count": NRICVerification.objects.filter(status=VerificationStatus.submitted.value).count()
        }
        return render(request, 'admin_dashboard.html', context)

class AdminLoginView(View):
    def get(self,request,*args,**kwargs):
        if request.user.is_authenticated() and request.user.is_staff:
            return HttpResponseRedirect(reverse("admin_home"))
        context = {
            "login_form": LoginForm()
        }
        return render(request, 'admin_login.html', context)
    def post(self,request,*args,**kwargs):
        login_form = LoginForm(request.POST)
        if login_form.is_valid():
            if login_form.authenticate_stuff(request):
                return HttpResponseRedirect(reverse("admin_home"))

        context = {
            "error": True,
            "error_msg": "Invalid stuff username and password",
            "login_form": login_form
        }
        return render(request, 'admin_login.html', context)

class AdminLogoutView(View):
    def get(self,request,*args,**kwargs):
        #Get the user session object.
        session_objs = Session.objects.filter(session_key=request.session.session_key)
        if session_objs:
            session_objs[0].delete()
        return HttpResponseRedirect(reverse("admin_login"))

class SystemLogListView(ListView):
    template_name = "system_log_list.html"
    model = SystemLog
    queryset = SystemLog.objects.all().order_by("-date_created")
    paginate_by = 20

    def get_context_data(self, **kwargs):
        context = super(SystemLogListView, self).get_context_data(**kwargs)
        return context

class PaymentLogListView(ListView):
    template_name = "payment_log_list.html"
    model = PaymentLog
    queryset = PaymentLog.objects.all().order_by("-date_created")
    paginate_by = 20

    def get_context_data(self, **kwargs):
        context = super(PaymentLogListView, self).get_context_data(**kwargs)
        return context

class PaymentLogDetailsView(DetailView):
    template_name = "payment_log_details.html"
    model = PaymentLog

class VerificationRequestListView(ListView):
    template_name = "verification_requests.html"
    model = NRICVerification
    # queryset = NRICVerification.objects.filter(Q(status=VerificationStatus.not_verified.value) | Q(status=VerificationStatus.submitted.value)).order_by("-date_created")
    paginate_by = 10

    def get_queryset(self):
        queryset = super(VerificationRequestListView, self).get_queryset()
        if self.request.GET.get("filter") == "true":
            request_filter = self.request.GET.get("request-filter")
            if request_filter == "nr":
                queryset = queryset.filter(status=VerificationStatus.submitted.value)
            elif request_filter == "ur":
                queryset = queryset.filter(status=VerificationStatus.under_review.value)
            elif request_filter == "nv":
                queryset = queryset.filter(status=VerificationStatus.not_verified.value)
            elif request_filter == "rr":
                queryset = queryset.filter(status=VerificationStatus.rejected.value)
            elif request_filter == "vr":
                queryset = queryset.filter(status=VerificationStatus.verified.value)

        queryset = queryset.order_by("-date_created")

        return queryset

    def get_context_data(self, **kwargs):
        context = super(VerificationRequestListView, self).get_context_data(**kwargs)
        context["object_list"] = self.get_queryset()
        return context

class VerificationRequestDetailsView(DetailView):
    template_name = "nric/nric_details_view.html"
    model = NRICVerification

    def get_context_data(self, **kwargs):
        context = super(VerificationRequestDetailsView, self).get_context_data(**kwargs)
        return context

class VerificationRequestMutateView(AdminLoginRequired,AjaxRequestRequired,JSONResponseMixin, View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(VerificationRequestMutateView, self).dispatch(*args, **kwargs)

    def post(self,request,*args,**kwargs):
        pk_val = kwargs.get("pk")
        status = int(request.POST.get("value"))
        if status >= 0 and status <= 4:
            nric = NRICVerification.objects.get(pk=pk_val)
            nric.status = status
            nric.save()
            response = {
                "status": "SUCCESS",
                "message": "Successful",
                "data": ""
            }
            return self.render_to_json(response)
        response = {
            "status": "FAILURE",
            "message": "Failure",
            "data": ""
        }
        return self.render_to_json(response)

class AdminHomePageVideoView(AdminLoginRequired, View):
    def get(self,request,*args,**kwargs):
        settings_obj, created = GlobalSettings.objects.get_or_create(pk=1)
        return render(request, 'home_page_video.html', { 'settings': settings_obj })

    def post(self, request, *args, **kwargs):
        url = request.POST.get('url')
        if url and not "embed" in url and "watch" in url:
                url = url.replace("watch?v=","embed/")

        settings_obj, created = GlobalSettings.objects.get_or_create(pk=1)
        settings_obj.home_page_video = url
        settings_obj.save()

        return HttpResponseRedirect(reverse('admin_home_page_video'))

class AdminIntroVideoView(AdminLoginRequired, View):
    def get(self,request,*args,**kwargs):
        settings_obj, created = GlobalSettings.objects.get_or_create(pk=1)
        return render(request, 'intro_video.html', { 'settings': settings_obj })

    def post(self, request, *args, **kwargs):
        url = request.POST.get('url')
        if url and not "embed" in url and "watch" in url:
                url = url.replace("watch?v=","embed/")

        settings_obj, created = GlobalSettings.objects.get_or_create(pk=1)
        settings_obj.intro_video = url
        settings_obj.save()

        return HttpResponseRedirect(reverse('admin_intro_video'))

class AdminEmailTemplateListView(AdminLoginRequired, ListView):
    model = EmailTemplate
    template_name = 'email_templates_list.html'
    paginate_by = 20
