from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from payment.models import Transaction

__author__ = 'Sohel'

class SaleTransactionListView(ListView):
    template_name = "transactions/sale_transaction_list.html"
    model = Transaction
    queryset = Transaction.objects.all().order_by("-date_created")
    paginate_by = 20

    def get_context_data(self, **kwargs):
        context = super(SaleTransactionListView, self).get_context_data(**kwargs)
        return context

class SaleTransactionDetailsView(DetailView):
    template_name = "transactions/sale_transaction_details.html"
    model = Transaction
