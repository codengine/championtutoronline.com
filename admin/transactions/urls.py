from admin.transactions.views import SaleTransactionListView, SaleTransactionDetailsView

__author__ = 'Sohel'

from django.conf.urls import patterns, url

urlpatterns = patterns('',
        url(r'^sale_transactions/$', SaleTransactionListView.as_view(), name='admin_sale_transaction_list'),
        url(r'^sale_transaction/(?P<pk>[0-9]+)/details/$', SaleTransactionDetailsView.as_view(), name='admin_sale_transaction_details'),
    )