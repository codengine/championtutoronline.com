from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.http.response import HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import View
from core.generics.ajaxmixins import JSONResponseMixin

__author__ = 'Sohel'

class AdminUserActionAjaxView(JSONResponseMixin,View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(AdminUserActionAjaxView, self).dispatch(*args, **kwargs)

    def post(self,request,*args,**kwargs):
        response = {
            "status": "FAILURE",
            "message": "FAILURE",
            "data": []
        }
        user_id = request.POST.get("user_id")
        action = request.POST.get("action")
        if action == "enable":
            user = User.objects.get(pk=int(user_id))
            user.is_active = True
            user.save()
        elif action == "disable":
            user = User.objects.get(pk=int(user_id))
            user.is_active = False
            user.save()
        return HttpResponseRedirect(reverse("admin_user_details",kwargs={ "pk": int(user_id) }))