from admin.jobs.views import AdminJobDetailsView, AllJobListView, ActiveJobListView, AdminActiveJobDetailsView, \
    ArchivedJobListView, AdminArchivedJobDetailsView

__author__ = 'Sohel'

from django.conf.urls import patterns, url

urlpatterns = patterns('',
        url(r'^$', AllJobListView.as_view(), name='admin_all_joblist'),
        url(r'^(?P<pk>[0-9]+)/details/$', AdminJobDetailsView.as_view(), name='admin_job_details'),
        url(r'^active-jobs/$', ActiveJobListView.as_view(), name='admin_active_joblist'),
        url(r'^active-jobs/(?P<pk>[0-9]+)/details/$', AdminActiveJobDetailsView.as_view(), name='admin_active_job_details'),
        url(r'^archived-jobs/$', ArchivedJobListView.as_view(), name='admin_archived_joblist'),
        url(r'^archived-jobs/(?P<pk>[0-9]+)/details/$', AdminArchivedJobDetailsView.as_view(), name='admin_archived_job_details'),
    )

