from django.core.urlresolvers import reverse
from django.db.models.query_utils import Q
from admin.generics.views.generic_details_view import GenericDetailsView
from admin.generics.views.generic_list_view import GenericListView
from core.library.Clock import Clock
from core.models3 import Job

__author__ = 'Sohel'

class GenericJobListView(GenericListView):
    model = Job
    def get_left_menu_items(self):

        return [
            ( "All Jobs", reverse("admin_all_joblist"), "" ),
            ( "Active Jobs", reverse("admin_active_joblist"), "" ),
            ( "Archived Jobs", reverse("admin_archived_joblist"), "" ),
        ]

    def get_object_details_url_name(self):
        return "admin_job_details"

    def get_table_columns(self):
        return self.model.table_columns()

class AllJobListView(GenericJobListView):
    def get_selected_left_menu_item(self):
        return "All Jobs"

    def get_page_title(self):
        return "All Jobs"

    def get_queryset(self):
        return self.model.objects.all()

class AdminJobDetailsView(GenericDetailsView):
    model = Job

    def get_left_menu_items(self):

        return [
            ( "All Jobs", reverse("admin_all_joblist"), "" ),
            ( "Active Jobs", reverse("admin_active_joblist"), "" ),
            ( "Archived Jobs", reverse("admin_archived_joblist"), "" ),
        ]

    def get_selected_left_menu_item(self):
        return "All Jobs"

    def get_page_title(self):
        return "Job Details"

class ActiveJobListView(GenericJobListView):
    def get_selected_left_menu_item(self):
        return "Active Jobs"

    def get_page_title(self):
        return "Active Jobs"

    def get_queryset(self):
        return self.model.objects.filter(status=1,end_date__gte=Clock.utc_timestamp())

class AdminActiveJobDetailsView(AdminJobDetailsView):
    model = Job

    def get_selected_left_menu_item(self):
        return "Active Jobs"

    def get_page_title(self):
        return "Active Details"

class ArchivedJobListView(GenericJobListView):
    def get_selected_left_menu_item(self):
        return "Archived Jobs"

    def get_page_title(self):
        return "Archived Jobs"

    def get_queryset(self):
        return self.model.objects.filter(~Q(status=1) | Q(end_date__lt=Clock.utc_timestamp()))

class AdminArchivedJobDetailsView(AdminJobDetailsView):
    model = Job

    def get_selected_left_menu_item(self):
        return "Archived Jobs"

    def get_page_title(self):
        return "Archived Details"