from django.core.paginator import Paginator
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import View
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from core.generics.ajaxmixins import JSONResponseMixin
from core.generics.mixins import AdminLoginRequired
from servey.models import Servey, ServeyQuestion, ServeyQuestionOption
from django.template import loader, Context

__author__ = 'Sohel'

class ServeyCreateView(AdminLoginRequired, View):
    def get(self, request, *args, **kwargs):
        context = {}
        return render(request, 'servey/servey_create_form.html', context)

    def post(self, request, *args, **kwargs):
        pass

class ServeyListView(AdminLoginRequired, ListView):
    template_name = "servey/servey_list.html"
    model = Servey
    paginate_by = 10

    def get_queryset(self):
        queryset = super(ServeyListView, self).get_queryset()
        queryset = queryset.order_by('-date_created')
        return queryset

    def get_context_data(self, **kwargs):
        context = super(ServeyListView, self).get_context_data(**kwargs)
        return context

class ServeyDetailsView(AdminLoginRequired, DetailView):
    template_name = "servey/servey_details.html"
    model = Servey

class ServeyAjaxQAction(AdminLoginRequired, View, JSONResponseMixin):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ServeyAjaxQAction, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        response = {
            "status": "FAILURE",
            "message": "Failed"
        }
        sid = request.POST.get("sid")
        qid = request.POST.get("qid")
        stitle = request.POST.get("stitle")
        qtitle = request.POST.get("qtitle")
        qtype = request.POST.get("qtype")
        choices = request.POST.get("choices")
        try:
            sid = int(sid)
            qid = int(qid)
        except:
            response["message"] = "Invalid id given"
            return self.render_to_json(response)
        """
        if sid is not given then a new servey has to be created.
        if sid is given then check for qid. if qid is given then edit else add new question.
        """
        if sid == -1:
            if not stitle:
                response["message"] = "Servey title required"
                return self.render_to_json(response)
            servey = Servey()
            servey.title = stitle
            servey.save()
        else:
            serveys = Servey.objects.filter(pk=sid)
            if not serveys.exists():
                response["message"] = "No servey found with the specified id."
                return self.render_to_json(response)
            servey = serveys.first()

        choice_split = choices.split("|")
        choice_list = []
        for choice in choice_split:
            if not choice:
                continue
            ch_split = choice.split(":")
            if len(ch_split) != 3:
                response["message"] = "Invalid choice given"
                return self.render_to_json(response)
            if not ch_split[0] or not ch_split[1] or not ch_split[2]:
                response["message"] = "Invalid choice given"
                return self.render_to_json(response)
            try:
                ch_id = int(ch_split[0])
                if ch_id != -1:
                    ServeyQuestionOption.objects.get(pk=ch_id)
            except:
                response["message"] = "Invalid choice given"
                return self.render_to_json(response)
            choice_list += [{
                "id": ch_id,
                "value": ch_split[1],
                "label": ch_split[2]
            }]

        if qtype != "text" and not choice_list:
            response["message"] = "Invalid choice given"
            return self.render_to_json(response)
        if not qtitle:
            response["message"] = "Question title required."
            return self.render_to_json(response)
        if not qtype:
            response["message"] = "Question type required."
            return self.render_to_json(response)

        if qid == -1:
            if not qtitle:
                response["message"] = "Question title required"
                return self.render_to_json(response)
            question = ServeyQuestion()
            question.title = qtitle
            question.qtype = qtype
            question.save()
            servey.questions.add(question)
        else:
            questions = ServeyQuestion.objects.filter(pk=sid)
            if not questions.exists():
                response["message"] = "No question found with the specified id."
                return self.render_to_json(response)
            question = questions.first()

        opts = []
        for choice_dict in choice_list:
            id = choice_dict["id"]
            label = choice_dict["label"]
            value = choice_dict["value"]
            if id == -1:
                choice_option = ServeyQuestionOption()
                choice_option.value = value
                choice_option.label = label
                choice_option.save()
                question.options.add(choice_option)

            else:
                choice_option = ServeyQuestionOption.objects.get(pk=id)
                choice_option.value = value
                choice_option.label = label
                choice_option.save()
            opts += [ ( choice_option.pk, choice_option.label, choice_option.value ) ]

        template = loader.get_template("servey/ajax/servey_question.html")
        cntxt = Context({
            "qtitle": qtitle,
            "qtype": qtype,
            "qid": question.pk,
            "options": opts
        })
        rendered = template.render(cntxt)

        response["status"] = "SUCCESS"
        response["message"] = "Successful"
        response["data"] = {
            "sid": servey.pk,
            "qid": question.pk,
            "question": rendered
        }
        return self.render_to_json(response)






