from django.template import loader, Context

__author__ = 'Sohel'

class ServeyRenderer(object):

    @classmethod
    def render_servey_question(cls, question):

        opts = []

        for opt in question.options.all():
            opts += [ ( opt.pk, opt.label, opt.value ) ]

        template = loader.get_template("servey/ajax/servey_question.html")
        cntxt = Context({
            "qtitle": question.title,
            "qtype": question.qtype,
            "qid": question.pk,
            "options": opts
        })
        rendered = template.render(cntxt)
        return rendered
