from django.conf.urls.static import static
from django.conf import settings
from django.conf.urls import url, patterns
from admin.servey.views import ServeyCreateView, ServeyAjaxQAction, ServeyListView, ServeyDetailsView

urlpatterns = patterns('',
    url(r'^$', ServeyListView.as_view(), name='servey_list'),
    url(r'^(?P<pk>[0-9]+)/details/$', ServeyDetailsView.as_view(), name='servey_details'),
    url(r'^create/', ServeyCreateView.as_view(), name='servey_create'),
    url(r'^ajax/qaction/', ServeyAjaxQAction.as_view(), name='ajax_servey_qaction'),

)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
js_info_dict = {
    'packages': ('django.conf',),
}

if settings.DEBUG:
    urlpatterns += patterns('django.views.static',
                            (r'media/(?P<path>.*)', 'serve', {'document_root': settings.MEDIA_ROOT}),
                            )
