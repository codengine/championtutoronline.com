from django.template import loader, Context
from admin.generics.views.generic_list_view import GenericListView
from admin.mixins.context_model_mixin import TabContextModelMixin

__author__ = 'Sohel'

class GenericTabListView(TabContextModelMixin,GenericListView):
    template_name = "generics/partial/_generic_tab_list.html"
    paginate_by = 10

    def get_context_data(self, **kwargs):
        #context = super(GenericTabListView, self).get_context_data(**kwargs)
        context = {}
        object_list = self.apply_search_filter(self.request,self.get_tab_queryset(),**kwargs)

        self.model = object_list.model

        page = 1
        try:
            page = int(self.request.GET.get("page"))
        except:
            pass

        context['search_terms'] = self.collect_search_terms()
        columns = self.model.table_columns()
        context["columns"] = columns
        context["column_headers"] = self.render_partial_column_headers(columns)
        context["manage_actions"] = self.get_manage_actions()
        page, paginator, page_object_list, num_pages = self.render_partial_paginfo(object_list,page=page,paginate_by=self.paginate_by)
        context["object_list"] = page_object_list
        total_records = 0
        try:
            total_records = object_list.count()
        except:
            pass
        context["total_records"] = total_records
        context["page_obj"] = page
        context["details_url_name"] = self.get_object_details_url_name()
        context["request_path"] = self.request.path
        context["searchable_columns"] = self.model.get_searchable_columns()
        context["exclude_search_pane"] = self.request.GET.get("exclude_search_pane") == "1"
        context["is_paginated"] = num_pages > 1
        context['request'] = self.request
        return context

    def render_to_response(self, context, **response_kwargs):
        if self.request.is_ajax() or self.request.GET.get('json'):
            template = loader.get_template(self.template_name)
            cntxt = Context(context)
            rendered = template.render(cntxt)
            response = {
                'status': 'SUCCESS',
                'message': 'Successful',
                'data': rendered
            }
            return self.render_to_json(response)
        return super(GenericTabListView, self).render_to_response(context,**response_kwargs)
