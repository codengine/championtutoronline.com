from django.views.generic.detail import DetailView
from admin.mixins.left_menu_model_mixin import LeftMenuModelMixin

__author__ = 'Sohel'

class GenericDetailsView(LeftMenuModelMixin, DetailView):
    template_name = "generics/_generic_details.html"

    def get_page_title(self):
        return ""

    def get_manage_actions(self):
        return []

    def get_context_data(self, **kwargs):
        context = super(GenericDetailsView, self).get_context_data(**kwargs)
        context["left_menu_items"] = self.get_final_left_menu_items()
        context["page_title"] = self.get_page_title()
        context["manage_actions"] = self.get_manage_actions()
        return context
