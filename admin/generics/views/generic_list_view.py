from django.views.generic.list import ListView
from admin.mixins.left_menu_model_mixin import LeftMenuModelMixin
from admin.mixins.pagination_mixin import PaginationMixin
from admin.mixins.partial_column_mixin import PartialColumnMixin
from admin.mixins.searchable_model_mixin import SearchableModelMixin
from core.generics.ajaxmixins import JSONResponseMixin
from core.generics.mixins import AdminLoginRequired

__author__ = 'Sohel'

class GenericListView(AdminLoginRequired, JSONResponseMixin, PaginationMixin, PartialColumnMixin, LeftMenuModelMixin, SearchableModelMixin, ListView):
    template_name = "generics/_generic_list.html"
    paginate_by = 10

    def get_page_title(self):
        return ""

    def get_table_columns(self):
        return []

    def get_object_details_url_name(self):
        return None

    def get_manage_actions(self):
        return []

    def get_queryset(self):
        return []

    def collect_search_terms(self):
        terms = []
        d = dict()
        for key, value in self.request.GET.items():
            if key != 'q' and key != 'page' and key != 'exclude_search_pane':
                if not key in d:
                    d[key] = [ value ]
                else:
                    d[key] += [ value ]
        for k,values in d.items():
            terms += [ (k, ','.join(values)) ]
        return terms

    def get_context_data(self, **kwargs):
        context = super(GenericListView, self).get_context_data(**kwargs)

        page = 1
        try:
            page = int(self.request.GET.get("page"))
        except:
            pass

        queryset = self.get_queryset()
        queryset = queryset.order_by('-date_created')

        object_list = self.apply_search_filter(self.request,queryset,**kwargs)

        context['search_terms'] = self.collect_search_terms()

        context["left_menu_items"] = self.get_final_left_menu_items()
        context["page_title"] = self.get_page_title()
        columns = self.get_table_columns()
        context["columns"] = columns
        context["column_headers"] = self.render_partial_column_headers(columns)
        context["manage_actions"] = self.get_manage_actions()
        page, paginator, page_object_list, num_pages = self.render_partial_paginfo(object_list,page=page,paginate_by=self.paginate_by)
        context["object_list"] = page_object_list
        total_records = 0
        try:
            total_records = object_list.count()
        except:
            pass
        context["total_records"] = total_records
        context["page_obj"] = page
        context["details_url_name"] = self.get_object_details_url_name()
        context["searchable_columns"] = self.model.get_searchable_columns()
        return context
