from admin.generics.views.generic_tab_list_view import GenericTabListView

__author__ = 'Sohel'

from django.conf.urls import patterns, url

urlpatterns = patterns('',
        url(r'^(?P<parent_model_name>[^/]+)/(?P<parent_id>[0-9]+)/tab/(?P<tab_model_name>[^/]+)/$', GenericTabListView.as_view(), name='admin_tab_list_view'),

    )

