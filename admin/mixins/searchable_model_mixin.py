from django.db import models

__author__ = 'Sohel'

class SearchableModelMixin(object):

    @classmethod
    def get_searchable_fields(cls,request,**kwargs):
        fields = []
        for key,value in request.GET.items():
            if key == "search":
                continue
            field = cls._meta.get_field_by_name(key)
            if isinstance(field[0], models.CharField):
                fields += [key]
            elif key in cls.get_datetime_fields():
                fields += [key]
        return fields

    @classmethod
    def apply_search_filter(cls, request, queryset, **kwargs):
        return queryset.model.filter_search(request,queryset,**kwargs)
