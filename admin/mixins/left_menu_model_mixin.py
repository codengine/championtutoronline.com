__author__ = 'Sohel'

class LeftMenuModelMixin(object):
    def get_left_menu_items(self): ###title, url, icon
        return []

    def get_selected_left_menu_item(self):
        return None

    def get_final_left_menu_items(self):
        item_list = self.get_left_menu_items()
        final_list = []
        selected_item = self.get_selected_left_menu_item()
        for title, url, icon in item_list:
            if title == selected_item:
                final_list += [ ( url, title, icon, True ) ]
            else:
                final_list += [ ( url, title, icon, False ) ]
        return final_list
