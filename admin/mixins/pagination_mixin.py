from django.core.paginator import Paginator

__author__ = 'Sohel'

class PaginationMixin(object):
    def render_partial_paginfo(self, object_list, page=1, paginate_by=2):
        page_object_list = []
        paginator = Paginator(object_list,paginate_by)
        try:
            page = paginator.page(page)
            page_object_list = page.object_list
        except Exception,msg:
            page = paginator.page(1)
            page_object_list = [] #page.object_list
        return page, paginator, page_object_list, paginator.num_pages
