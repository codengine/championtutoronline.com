from core.models3 import Job, JobApplication, JobInvitation, JobRecommendation

__author__ = 'Sohel'

class TabContextModelMixin(object):
    def get_tab_queryset(self):
        parent_model_name = self.kwargs.get("parent_model_name")
        parent_id = self.kwargs.get("parent_id")
        tab_model_name = self.kwargs.get("tab_model_name")

        if parent_model_name == "job":
            job = Job.objects.get(pk=parent_id)
            if tab_model_name == "jobapplication":
                return JobApplication.objects.filter(job__id=job.pk)
            elif tab_model_name == "jobinvitation":
                return JobInvitation.objects.filter(job_id=job.pk)
            elif tab_model_name == "jobrecommendation":
                return JobRecommendation.objects.filter(job_id=job.pk)

    def get_object_details_url_name(self):
        parent_model_name = self.kwargs.get("parent_model_name")
        parent_id = self.kwargs.get("parent_id")
        tab_model_name = self.kwargs.get("tab_model_name")

        if parent_model_name == "job":
            job = Job.objects.get(pk=parent_id)
            if tab_model_name == "jobapplication":
                return ""
            elif tab_model_name == "jobinvitation":
                return ""
            elif tab_model_name == "jobrecommendation":
                return ""
