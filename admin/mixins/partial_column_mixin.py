__author__ = 'Sohel'

class PartialColumnMixin(object):

    def render_partial_column_headers(self, columns):
        column_headers = []
        for col in columns:
            if col.startswith("render_"):
                temp = col.replace("render_","")
                if ":" in temp:
                    temp = temp[temp.rindex(":")+1:]
                    temp = temp.split(" ")
                else:
                    temp = temp.split("_")
                temp = " ".join([ word.capitalize() for word in temp ])
                column_headers += [ temp ]
            elif ":" in col:
                temp = col
                temp = temp[temp.rindex(":")+1:]
                temp = temp.split(" ")
                temp = " ".join([ word.capitalize() for word in temp ])
                column_headers += [ temp ]
            else:
                temp = col
                temp = temp.split("_")
                temp = " ".join([ word.capitalize() for word in temp ])
                column_headers += [ temp ]
        return column_headers
