__author__ = 'Sohel'

class ProtectedQuerySetMixin(object):
    def get_queryset(self, **kwargs):
        queryset = self.model.get_queryset()
        return self.model.apply_search_filter(self.request, queryset=queryset, **kwargs)
