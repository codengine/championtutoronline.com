from admin.logs.views import EmailLogListView, EmailLogDetailsView, ErrorLogListView, ErrorLogDetailsView, \
    PaymentLogListView, PaymentLogDetailsView

__author__ = 'Sohel'

from django.conf.urls import patterns, url

urlpatterns = patterns('',
        url(r'^email_logs/$', EmailLogListView.as_view(), name='admin_email_log_list'),
        url(r'^email/(?P<pk>[0-9]+)/details/$', EmailLogDetailsView.as_view(), name='email_log_details'),
        url(r'^error_logs/$', ErrorLogListView.as_view(), name='admin_error_log_list'),
        url(r'^error_log/(?P<pk>[0-9]+)/details/$', ErrorLogDetailsView.as_view(), name='admin_error_log_details'),
        url(r'^payment_logs/$', PaymentLogListView.as_view(), name='admin_payment_log_list'),
        url(r'^payment_log/(?P<pk>[0-9]+)/details/$', PaymentLogDetailsView.as_view(), name='admin_payment_log_details'),
    )

