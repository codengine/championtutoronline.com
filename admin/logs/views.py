from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from core.generics.mixins import AdminLoginRequired
from core.models3 import EmailLog, ErrorLog, PaymentLog

__author__ = 'Sohel'

class EmailLogListView(AdminLoginRequired,ListView):
    template_name = "logs/admin_email_log_list.html"
    paginate_by = 15
    model = EmailLog
    queryset = EmailLog.objects.all().order_by('-date_created')

class EmailLogDetailsView(AdminLoginRequired,DetailView):
    template_name = "logs/admin_email_log_details.html"
    model = EmailLog

class ErrorLogListView(ListView):
    template_name = "logs/admin_error_log_list.html"
    model = ErrorLog
    queryset = ErrorLog.objects.all().order_by("-date_created")
    paginate_by = 20

    def get_context_data(self, **kwargs):
        context = super(ErrorLogListView, self).get_context_data(**kwargs)
        return context

class ErrorLogDetailsView(DetailView):
    template_name = "logs/admin_error_log_details.html"
    model = ErrorLog

class PaymentLogListView(ListView):
    template_name = "logs/admin_payment_log_list.html"
    model = PaymentLog
    queryset = PaymentLog.objects.all().order_by("-date_created")
    paginate_by = 20

    def get_context_data(self, **kwargs):
        context = super(PaymentLogListView, self).get_context_data(**kwargs)
        return context

class PaymentLogDetailsView(DetailView):
    template_name = "logs/admin_payment_log_details.html"
    model = PaymentLog
