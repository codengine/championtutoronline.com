from admin.scheduler.views import SchedulerStatView, ScheduledJobDetailsView, QueueListView

__author__ = 'Sohel'

from django.conf.urls import patterns, url

urlpatterns = patterns('',
        url(r'^$', SchedulerStatView.as_view(), name='admin_scheduler_stat_view'),
        url(r'^queues/$', QueueListView.as_view(), name='admin_queue_list_view'),
        url(r'^scheduler/job/(?P<job_id>[^/]+)/$', ScheduledJobDetailsView.as_view(), name='admin_scheduled_job_details_view'),
    )

