from itertools import groupby
from django.conf import settings
from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
import redis
from django.views.generic.base import View
from redis.client import Redis
from core.generics.mixins import AdminLoginRequired
from rq.job import Job
from rq.queue import Queue
from rq.worker import Worker
from rq_scheduler.scheduler import Scheduler

__author__ = 'root'

class SchedulerStatView(AdminLoginRequired,View):
    def connect_redis(self):
        self.connection = redis.Redis()

    def __init__(self):
        self.connect_redis()

    def get_context_data(self):
        ctx = {}
        ctx.update({
            'queues': Queue.all(connection=self.connection),
            'workers': Worker.all(connection=self.connection),
        })
        scheduler = Scheduler(connection=self.connection)
        get_queue = lambda job: job.origin
        all_jobs = sorted(scheduler.get_jobs(), key=get_queue)
        ctx['scheduler'] = scheduler
        ctx['scheduled_queues'] = [
            {'name': queue, 'job_count': len(list(jobs))}
            for queue, jobs in groupby(all_jobs, get_queue)]
        return ctx

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        return render(request,"scheduler/stats.html",context)

class QueueListView(AdminLoginRequired, View):

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        return render(request,"scheduler/queues.html",context)

    def get_context_data(self, **kwargs):
        context = {}
        context["queues"] = Queue.all(connection=redis.Redis())
        return context

class ScheduledJobDetailsView(DetailView):
    model = Job
    template_name = "scheduler/job_details.html"

    def get_object(self, queryset=None):
        _object = Job.fetch(id=self.kwargs.get("job_id"), connection=Redis())
        return _object