from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from core.enums import VerificationStatus
from core.models3 import NRICVerification, EducationVerification

__author__ = 'Sohel'

class VerificationRequestListView(ListView):
    template_name = "nric/nric_verification_list.html"
    model = NRICVerification
    # queryset = NRICVerification.objects.filter(Q(status=VerificationStatus.not_verified.value) | Q(status=VerificationStatus.submitted.value)).order_by("-date_created")
    paginate_by = 10

    def get_queryset(self):
        queryset = super(VerificationRequestListView, self).get_queryset()
        if self.request.GET.get("filter") == "true":
            request_filter = self.request.GET.get("request-filter")
            if request_filter == "nr":
                queryset = queryset.filter(status=VerificationStatus.submitted.value)
            elif request_filter == "ur":
                queryset = queryset.filter(status=VerificationStatus.under_review.value)
            elif request_filter == "nv":
                queryset = queryset.filter(status=VerificationStatus.not_verified.value)
            elif request_filter == "rr":
                queryset = queryset.filter(status=VerificationStatus.rejected.value)
            elif request_filter == "vr":
                queryset = queryset.filter(status=VerificationStatus.verified.value)

        queryset = queryset.order_by("-date_created")

        return queryset

    def get_context_data(self, **kwargs):
        context = super(VerificationRequestListView, self).get_context_data(**kwargs)
        context["object_list"] = self.get_queryset()
        return context

class VerificationDetailsView(DetailView):
    template_name = "nric/nric_details_view.html"
    model = NRICVerification

class CertificateVerfication(ListView):

    template_name = "certificate/certificate_verification_list.html"
    model = EducationVerification
    # queryset = NRICVerification.objects.filter(Q(status=VerificationStatus.not_verified.value) | Q(status=VerificationStatus.submitted.value)).order_by("-date_created")
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(CertificateVerfication, self).get_context_data(**kwargs)
        context["submitted_list"] = EducationVerification.objects.filter(status=VerificationStatus.submitted.value).order_by("-date_created")
        context["not_verified_list"] = EducationVerification.objects.filter(status=VerificationStatus.not_verified.value).order_by("-date_created")
        context["under_review_list"] = EducationVerification.objects.filter(status=VerificationStatus.under_review.value).order_by("-date_created")
        context["rejected_list"] = EducationVerification.objects.filter(status=VerificationStatus.rejected.value).order_by("-date_created")
        context["verified_list"] = EducationVerification.objects.filter(status=VerificationStatus.verified.value).order_by("-date_created")
        return context

class CertificateVerificationDetails(DetailView):
    template_name = "certificate/certificate_details_view.html"
    model = EducationVerification
