from admin.verification.viewsajax import VerificationRequestMutateView

__author__ = 'Sohel'

from django.conf.urls import patterns, include, url
from views import *

urlpatterns = patterns('',
        url(r'^nric/$', VerificationRequestListView.as_view(), name='verification_requests'),
        url(r'^nric/(?P<pk>[0-9]+)/mutate/$', VerificationRequestMutateView.as_view(), name='verification_mutate_details'),
        url(r'^nric/(?P<pk>[0-9]+)/details/$', VerificationDetailsView.as_view(), name='verification_details_view'),
        url(r'^cert/$', CertificateVerfication.as_view(), name='cert_verification_requests'),
        url(r'^cert/(?P<pk>[0-9]+)/details/$', VerificationDetailsView.as_view(), name='cert_verification_details_view'),
    )

