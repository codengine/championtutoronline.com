from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import View
from core.generics.ajaxmixins import JSONResponseMixin
from core.generics.mixins import AdminLoginRequired, AjaxRequestRequired
from core.models3 import NRICVerification, EducationVerification

__author__ = 'Sohel'

class VerificationRequestMutateView(AdminLoginRequired,AjaxRequestRequired,JSONResponseMixin, View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(VerificationRequestMutateView, self).dispatch(*args, **kwargs)

    def post(self,request,*args,**kwargs):
        pk_val = kwargs.get("pk")
        status = int(request.POST.get("value"))
        if status >= 0 and status <= 4:
            nric = NRICVerification.objects.get(pk=pk_val)
            nric.status = status
            nric.save()
            response = {
                "status": "SUCCESS",
                "message": "Successful",
                "data": ""
            }
            return self.render_to_json(response)
        response = {
            "status": "FAILURE",
            "message": "Failure",
            "data": ""
        }
        return self.render_to_json(response)

class CertVerificationRequestMutateView(AdminLoginRequired,AjaxRequestRequired,JSONResponseMixin, View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(CertVerificationRequestMutateView, self).dispatch(*args, **kwargs)

    def post(self,request,*args,**kwargs):
        pk_val = kwargs.get("pk")
        status = int(request.POST.get("value"))
        if status >= 0 and status <= 4:
            nric = EducationVerification.objects.get(pk=pk_val)
            nric.status = status
            nric.save()
            response = {
                "status": "SUCCESS",
                "message": "Successful",
                "data": ""
            }
            return self.render_to_json(response)
        response = {
            "status": "FAILURE",
            "message": "Failure",
            "data": ""
        }
        return self.render_to_json(response)
