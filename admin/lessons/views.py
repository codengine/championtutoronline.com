from django.core.urlresolvers import reverse
from admin.generics.views.generic_details_view import GenericDetailsView
from admin.generics.views.generic_list_view import GenericListView
from core.library.lesson_util import LessonUtil
from core.models3 import LessonRequest

__author__ = 'Sohel'

class GenericLessonListView(GenericListView):
    model = LessonRequest
    def get_left_menu_items(self):

        return [
            ( "Unconfirmed Lessons", reverse("admin_unconfirmed_lesson_list"), "" ),
            ( "Coming Lessons", reverse("admin_coming_lesson_list"), "" ),
            ( "Past Lessons", reverse("admin_past_lesson_list"), "" ),
            ( "Missed Lessons", reverse("admin_missed_lesson_list"), "" ),
        ]

    def get_table_columns(self):
        return self.model.table_columns()

class GenericLessonDetailsView(GenericDetailsView):
    model = LessonRequest
    def get_left_menu_items(self):

        return [
            ( "Unconfirmed Lessons", reverse("admin_unconfirmed_lesson_list"), "" ),
            ( "Coming Lessons", reverse("admin_coming_lesson_list"), "" ),
            ( "Past Lessons", reverse("admin_past_lesson_list"), "" ),
            ( "Missed Lessons", reverse("admin_missed_lesson_list"), "" ),
        ]

class UnconfirmedLessonListView(GenericLessonListView):
    def get_selected_left_menu_item(self):
        return "Unconfirmed Lessons"

    def get_page_title(self):
        return "Unconfirmed Lessons"

    def get_object_details_url_name(self):
        return "admin_unconfirmed_lesson_details"

    def get_queryset(self):
        return LessonUtil.get_unconfirmed_lessons()

class UnconfirmedLessonDetailsView(GenericLessonDetailsView):
    def get_selected_left_menu_item(self):
        return "Unconfirmed Lessons"

    def get_page_title(self):
        return "Unconfirmed Lesson Details"

class ComingLessonListView(GenericLessonListView):
    def get_selected_left_menu_item(self):
        return "Coming Lessons"

    def get_page_title(self):
        return "Coming Lessons"

    def get_object_details_url_name(self):
        return "admin_coming_lesson_details"

    def get_queryset(self):
        return LessonUtil.get_coming_lessons()

class ComingLessonDetailsView(GenericLessonDetailsView):
    def get_selected_left_menu_item(self):
        return "Coming Lessons"

    def get_page_title(self):
        return "Coming Lesson Details"


class PastLessonListView(GenericLessonListView):
    def get_selected_left_menu_item(self):
        return "Past Lessons"

    def get_page_title(self):
        return "Past Lessons"

    def get_object_details_url_name(self):
        return "admin_past_lesson_details"

    def get_queryset(self):
        return LessonUtil.get_past_lessons()

class PastLessonDetailsView(GenericLessonDetailsView):
    def get_selected_left_menu_item(self):
        return "Past Lessons"

    def get_page_title(self):
        return "Past Lesson Details"

class MissedLessonListView(GenericLessonListView):
    def get_selected_left_menu_item(self):
        return "Missed Lessons"

    def get_page_title(self):
        return "Missed Lessons"

    def get_object_details_url_name(self):
        return "admin_missed_lesson_details"

    def get_queryset(self):
        return LessonUtil.get_missed_lessons()

class MissedLessonDetailsView(GenericLessonDetailsView):
    def get_selected_left_menu_item(self):
        return "Missed Lessons"

    def get_page_title(self):
        return "Missed Lesson Details"
