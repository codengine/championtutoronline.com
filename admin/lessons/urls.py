from admin.lessons.views import PastLessonListView, PastLessonDetailsView, UnconfirmedLessonDetailsView, \
    UnconfirmedLessonListView, ComingLessonListView, ComingLessonDetailsView, MissedLessonListView, \
    MissedLessonDetailsView

__author__ = 'Sohel'

from django.conf.urls import patterns, url

urlpatterns = patterns('',
        url(r'^unconfirmed-lessons/$', UnconfirmedLessonListView.as_view(), name='admin_unconfirmed_lesson_list'),
        url(r'^unconfirmed-lesson/(?P<pk>[0-9]+)/details/$', UnconfirmedLessonDetailsView.as_view(), name='admin_unconfirmed_lesson_details'),
        url(r'^coming-lessons/$', ComingLessonListView.as_view(), name='admin_coming_lesson_list'),
        url(r'^coming-lesson/(?P<pk>[0-9]+)/details/$', ComingLessonDetailsView.as_view(), name='admin_coming_lesson_details'),
        url(r'^past-lessons/$', PastLessonListView.as_view(), name='admin_past_lesson_list'),
        url(r'^past-lesson/(?P<pk>[0-9]+)/details/$', PastLessonDetailsView.as_view(), name='admin_past_lesson_details'),
        url(r'^missed-lessons/$', MissedLessonListView.as_view(), name='admin_missed_lesson_list'),
        url(r'^missed-lesson/(?P<pk>[0-9]+)/details/$', MissedLessonDetailsView.as_view(), name='admin_missed_lesson_details'),
    )

