from django.contrib.auth.models import User
from django.core.paginator import Paginator
from django.core.urlresolvers import reverse, resolve
from django.db.models.loading import get_model
from django.db.models.query_utils import Q
from django.http.response import Http404, HttpResponseRedirect
from django.views.generic.base import View
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from admin.generics.views.generic_list_view import GenericListView
from admin.mixins.pagination_mixin import PaginationMixin
from admin.mixins.partial_column_mixin import PartialColumnMixin
from core.generics.ajaxmixins import JSONResponseMixin
from core.generics.mixins import AdminLoginRequired
from core.library.lesson_util import LessonUtil
from core.library.user_util import UserUtil
from core.models3 import ChampUser, ProfileMyVideo, Question, QuestionAnswer, QuestionComment, Job, JobApplication, \
    JobInvitation, JobRecommendation, LessonRequest
from payment.models import Transaction
from django.template import loader, Context

__author__ = 'Sohel'

class UsersView(AdminLoginRequired,ListView):
    template_name = "users/admin_user_list.html"
    model = ChampUser
    paginate_by = 20
    queryset = ChampUser.objects.all().exclude(type__name="stuff").order_by("-date_created")

    def get_queryset(self):
        queryset = super(UsersView,self).get_queryset()
        if self.request.GET.get("type"):
            if self.request.GET.get("type") == "student":
                queryset = queryset.filter(type__name="student")
            elif self.request.GET.get("type") == "tutor":
                queryset = queryset.filter(type__name="teacher")
        return queryset

    def get_context_data(self, **kwargs):
        context = super(UsersView, self).get_context_data(**kwargs)
        context["type"] = self.request.GET.get("type")
        return context

class UserListView(GenericListView):
    model = ChampUser
    def get_left_menu_items(self):

        return [
            ( "Tutors", reverse("admin_user_list")+"?type=teacher", "" ),
            ( "Students", reverse("admin_user_list")+"?type=student", "" ),
        ]

    def get_table_columns(self):
        return self.model.table_columns()

    def get_page_title(self):
        if self.request.GET.get("type") == "student":
            return "Students"
        return "Tutors"

    def get_object_details_url_name(self):
        return "admin_user_details"

    def get_selected_left_menu_item(self):
        if self.request.GET.get("type") == "student":
            return "Students"
        return "Tutors"

    def get_queryset(self):
        return ChampUser.objects.all()

class UserDetailsView(PaginationMixin, PartialColumnMixin, DetailView):
    template_name = "users/user_details.html"
    model = ChampUser
    paginate_by = 10

    def resolve_partial_tab_context_data(self):
        partial_object_list,partial_columns = [],[]
        partial_tab_name = self.kwargs.get("partial_tab_name")
        user_id = int(self.kwargs.get('pk'))
        partial_model = None
        if partial_tab_name == "videos":
            partial_object_list = ProfileMyVideo.objects.filter(user_id=user_id)
            partial_model = ProfileMyVideo
        elif partial_tab_name == "participation":
            partial_object_list = Question.objects.filter(Q(answers__answer_by_id=user_id) | Q(answers__comments__created_by_id=user_id))
            partial_model = Question
        elif partial_tab_name == "jobs":
            partial_object_list = Job.objects.filter(posted_by_id=user_id)
            partial_model = Job
        elif partial_tab_name == "invitation":
            partial_object_list = JobInvitation.objects.filter(user_id=user_id)
            partial_model = JobInvitation
        elif partial_tab_name == "recommendation":
            partial_object_list = JobRecommendation.objects.filter(user_id=user_id)
            partial_model = JobRecommendation
        elif partial_tab_name == "application":
            partial_object_list = JobApplication.objects.filter(tutor_id=user_id)
            partial_model = JobApplication
        elif partial_tab_name == "active-students":
            partial_object_list = UserUtil.get_all_active_students(tutor_id=user_id)
            partial_model = ChampUser
        elif partial_tab_name == "inactive-students":
            partial_object_list = UserUtil.get_all_inactive_students(tutor_id=user_id)
            partial_model = ChampUser
        elif partial_tab_name == "active-tutors":
            partial_object_list = UserUtil.get_all_active_tutors(student_id=user_id)
            partial_model = ChampUser
        elif partial_tab_name == "inactive-tutors":
            partial_object_list = UserUtil.get_all_inactive_tutors(student_id=user_id)
            partial_model = ChampUser
        elif partial_tab_name == "unconfirmed-lessons":
            partial_object_list = LessonUtil.get_unconfirmed_lessons(User.objects.get(pk=user_id))
            partial_model = LessonRequest
        elif partial_tab_name == "coming-lessons":
            partial_object_list = LessonUtil.get_coming_lessons(User.objects.get(pk=user_id))
            partial_model = LessonRequest
        elif partial_tab_name == "past-lessons":
            partial_object_list = LessonUtil.get_past_lessons(User.objects.get(pk=user_id))
            partial_model = LessonRequest
        elif partial_tab_name == "missed-lessons":
            partial_object_list = LessonUtil.get_missed_lessons(User.objects.get(pk=user_id))
            partial_model = LessonRequest
        if partial_model:
            partial_columns = partial_model.table_columns()
        return partial_object_list, partial_columns

    def get_partial_object(self):
        partial_object = None
        partial_tab_name = self.kwargs.get("partial_tab_name")
        if "partial_object_id" in self.kwargs:
            partial_object_id = self.kwargs.get("partial_object_id")
            if partial_tab_name == "videos":
                partial_object = ProfileMyVideo.objects.get(pk=partial_object_id)
            elif partial_tab_name == "participation":
                partial_object = Question.objects.get(pk=partial_object_id)
            elif partial_tab_name == "jobs":
                partial_object = Job.objects.get(pk=partial_object_id)
        return partial_object

    def get_admin_partial_details_url_name(self):
        partial_tab_name = self.kwargs.get("partial_tab_name")
        if partial_tab_name == "jobs":
            return "admin_job_details"
        elif partial_tab_name == "invitation":
            return ""
        elif partial_tab_name == "recommendation":
            return ""
        elif partial_tab_name == "application":
            return ""
        elif partial_tab_name == "active-students":
            return "admin_user_details"
        elif partial_tab_name == "inactive-students":
            return "admin_user_details"
        elif partial_tab_name == "active-tutors":
            return "admin_user_details"
        elif partial_tab_name == "inactive-tutors":
            return "admin_user_details"
        elif partial_tab_name == "unconfirmed-lessons":
            return ""
        elif partial_tab_name == "coming-lessons":
            return ""
        elif partial_tab_name == "past-lessons":
            return ""
        elif partial_tab_name == "missed-lessons":
            return ""
        return ""


    def get_context_data(self, **kwargs):
        context = super(UserDetailsView, self).get_context_data(**kwargs)
        partial_tab_name = self.kwargs.get("partial_tab_name")
        context["partial_tab_name"] = partial_tab_name
        if "partial_object_id" in self.kwargs:
            context["partial_template_name"] = "partial/_details_view.html"
            context["partial_object"] = self.get_partial_object()
            return context
        context["partial_template_name"] = "partial/_list_view.html"

        partial_object_list, context["partial_columns"] = self.resolve_partial_tab_context_data()
        if "partial_columns" in context:
            column_headers = self.render_partial_column_headers(context["partial_columns"])
            context["partial_column_headers"] = column_headers

        try:
            page = self.kwargs.get("page")
            if page:
                page = int(page.replace("page-",""))
            else:
                page = 1
        except:
            page = 1

        page, paginator, page_object_list, num_pages = self.render_partial_paginfo(partial_object_list, page=page, paginate_by=self.paginate_by)
        context["page_obj"] = page

        context["partial_objects"] = page_object_list
        context["is_paginated"] = num_pages > 1

        context["admin_partial_details_url"] = self.get_admin_partial_details_url_name()

        return context

    def get_landing_url(self):
        user = self.model.objects.get(pk=self.kwargs.get('pk'))
        landing_url = reverse("admin_user_details_partial",kwargs={ "pk": self.kwargs.get('pk'), "partial_tab_name": "jobs" })
        if user.is_tutor:
            landing_url = reverse("admin_user_details_partial",kwargs={ "pk": self.kwargs.get('pk'), "partial_tab_name": "videos" })
        return landing_url

    def get(self, request, *args, **kwargs):
        if resolve(self.request.path).url_name == "admin_user_details":
            return HttpResponseRedirect(self.get_landing_url())
        return super(UserDetailsView,self).get(request,*args,**kwargs)

class PartialListView(AdminLoginRequired, JSONResponseMixin, PaginationMixin, PartialColumnMixin, View):
    template_name = "partial/_partial_list_view.html"
    paginate_by = 10

    def resolve_secondary_partial_context_data(self):
        object_list, columns = [],[]
        partial_parent_model_name = self.kwargs.get("partial_parent_model_name")
        partial_parent = self.kwargs.get("partial_parent")
        partial_model_name = self.kwargs.get("partial_model_name")
        partial_model = None
        if partial_parent_model_name == "profilemyvideo":
            if partial_model_name == "transactions":
                object_list = Transaction.objects.all()
                partial_model = Transaction
        elif partial_parent_model_name == "question":
            if partial_model_name == "qanswer":
                question = Question.objects.get(pk=partial_parent)
                object_list = question.answers.all()
                partial_model = QuestionAnswer
            elif partial_model_name == "qanswer_me":
                question = Question.objects.get(pk=partial_parent)
                object_list = question.answers.filter(answer_by_id=self.kwargs.get('pk'))
                partial_model = QuestionAnswer
            elif partial_model_name == "qcomment":
                question = Question.objects.get(pk=partial_parent)
                answers = question.answers.all()
                qs = []
                for ans in answers:
                    my_comments = [int(id_) for id_ in ans.comments.filter(created_by_id=self.kwargs.get('pk')).values_list('pk', flat=True)]
                    qs += my_comments
                object_list = QuestionComment.objects.filter(pk__in=qs)
                partial_model = QuestionComment
        elif partial_parent_model_name == "job":
            if partial_model_name == "jobapplication":
                job_object = Job.objects.get(pk=partial_parent)
                object_list = job_object.applications.all()
                partial_model = JobApplication
        if partial_model:
            columns = partial_model.table_columns()
        return object_list, columns

    def get_context_data(self, **kwargs):
        context = {}

        partial_object_list, context["partial_columns"] = self.resolve_secondary_partial_context_data()
        if "partial_columns" in context:
            column_headers = self.render_partial_column_headers(context["partial_columns"])
            context["partial_column_headers"] = column_headers

        try:
            page = self.kwargs.get("page")
            if page:
                page = int(page.replace("page-",""))
            else:
                page = 1
        except:
            page = 1

        page, paginator, page_object_list, num_pages = self.render_partial_paginfo(partial_object_list, page=page, paginate_by=self.paginate_by)
        context["page_obj"] = page

        context["partial_objects"] = page_object_list
        context["is_paginated"] = num_pages > 1

        partial_parent_model_name = self.kwargs.get("partial_parent_model_name")
        partial_parent = self.kwargs.get("partial_parent")
        partial_model_name = self.kwargs.get("partial_model_name")
        context["partial_parent_model_name"] = partial_parent_model_name
        context["partial_parent"] = partial_parent
        context["partial_model_name"] = partial_model_name

        return context

    def render_to_response(self, context, **response_kwargs):
        response = {
            "status": "SUCCESS"
        }
        template = loader.get_template(self.template_name)
        cntxt = Context(context)
        rendered = template.render(cntxt)
        response["contents"] = rendered
        return self.render_to_json(response)

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context, **kwargs)
