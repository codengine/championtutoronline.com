from admin.users.views import UsersView, UserDetailsView, PartialListView, UserListView

__author__ = 'Sohel'

from django.conf.urls import patterns, url

urlpatterns = patterns('',
        url(r'^$', UserListView.as_view(), name='admin_user_list'),
        url(r'^(?P<pk>[0-9]+)/details/$', UserDetailsView.as_view(), name='admin_user_details'),
        url(r'^(?P<pk>[0-9]+)/details/(?P<partial_tab_name>[^/]+)/$', UserDetailsView.as_view(), name='admin_user_details_partial'),
        url(r'^(?P<pk>[0-9]+)/details/(?P<partial_tab_name>[^/]+)/page/(?P<page>[0-9]+)/$', UserDetailsView.as_view(), name='admin_user_details_partial_paginated'),
        url(r'^(?P<pk>[0-9]+)/details/(?P<partial_tab_name>[^/]+)/(?P<partial_object_id>[0-9]+)/details/$', UserDetailsView.as_view(), name='admin_user_details_partial_details'),
        url(r'^(?P<partial_parent_model_name>[^/]+)/(?P<partial_parent>[0-9]+)/partial/(?P<partial_model_name>[^/]+)/$', PartialListView.as_view(), name='admin_partial_details_view'),
        url(r'^(?P<partial_parent_model_name>[^/]+)/(?P<partial_parent>[0-9]+)/partial/(?P<partial_model_name>[^/]+)/page/(?P<page>[0-9]+)/$', PartialListView.as_view(), name='admin_partial_details_view_paginated'),
    )

