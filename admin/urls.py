from admin.viewsajax import AdminUserActionAjaxView

__author__ = 'Sohel'

from django.conf.urls import patterns, include, url
from views import *

urlpatterns = patterns('',
        url(r'^$', AdminHomeView.as_view(), name='admin_home'),
        url(r'^dashboard$', AdminDashboardView.as_view(), name='admin_dashboard'),
        url(r'^login/$', AdminLoginView.as_view(), name='admin_login'),
        url(r'^logout$', AdminLogoutView.as_view(), name='admin_logout'),
        url(r'^payment_logs/$', PaymentLogListView.as_view(), name='payment_log_list'),
        url(r'^payment_log/(?P<pk>[0-9]+)/details/$', PaymentLogDetailsView.as_view(), name='payment_log_details'),
        # url(r'^verification-requests/$', VerificationRequestListView.as_view(), name='verification_requests'),
        # url(r'^verification-request/(?P<pk>[0-9]+)/details/$', VerificationRequestDetailsView.as_view(), name='verification_request_details'),
        # url(r'^verfication-request/(?P<pk>[0-9]+)/mutate/$', VerificationRequestMutateView.as_view(), name='verification_mutate_details'),
        # url(r'^users/$', UsersView.as_view(), name='users_view'),

        url(r'^user_action/$', AdminUserActionAjaxView.as_view(), name='user_action'),
        url(r'^home_page_video/$', AdminHomePageVideoView.as_view(), name='admin_home_page_video'),
        url(r'^intro_video/$', AdminIntroVideoView.as_view(), name='admin_intro_video'),
        url(r'^admin_email_templates/$', AdminEmailTemplateListView.as_view(), name='admin_email_template_list'),
        (r'^users/', include('admin.users.urls')),
        (r'^logs/', include('admin.logs.urls')),
        (r'^scheduler/', include('admin.scheduler.urls')),
        (r'^transactions/', include('admin.transactions.urls')),
        (r'^verification/', include('admin.verification.urls')),
        (r'^lessons/', include('admin.lessons.urls')),
        (r'^jobs/', include('admin.jobs.urls')),
        (r'^generics/', include('admin.generics.urls')),
        (r'^servey/', include('admin.servey.urls')),
    )

