from django.contrib.auth.models import User
import simplejson as json
from django.core.management.base import BaseCommand
from core.enums import UserTypes
from core.models3 import Role, Country, SupportedCountry, ChampUser, TeachingExperiences, Major, SubjectLevel, UserMajor
from payment.models import Wallet
from countryinfo import countries as countryzone_info

__author__ = 'Sohel'

import os

class Command(BaseCommand):

    subjects = {}
    subject_levels = {}
    peferred_level_subjects = {}
    countries = {}
    common_levels = {}
    common_education = {}
    country_timezones = {}

    def read_country_timezones(self):
        for ctz in countryzone_info:
            self.country_timezones[ctz['name']] = ctz['timezones']

    def read_subjects(self):
        file_path = os.path.dirname(os.path.realpath(__file__))
        file_path = os.path.join(file_path, 'tutor_migrations')
        file_name = os.path.join(file_path, 'common_subjects.json')
        common_subject_file = open(file_name,"r")
        content = common_subject_file.read()
        json_data = json.loads(content)
        for record in json_data:
            self.subjects[record["subject_id"]] = record["subject_name"]

    def read_common_levels(self):
        file_path = os.path.dirname(os.path.realpath(__file__))
        file_path = os.path.join(file_path, 'tutor_migrations')
        file_name = os.path.join(file_path, 'common_level.json')
        common_subject_file = open(file_name,"r")
        content = common_subject_file.read()
        json_data = json.loads(content)
        for record in json_data:
            self.common_levels[record["level_id"]] = record["level_name"]

    def read_subject_levels(self):
        file_path = os.path.dirname(os.path.realpath(__file__))
        file_path = os.path.join(file_path, 'tutor_migrations')
        file_name = os.path.join(file_path, 'common_level_subject.json')
        common_subject_file = open(file_name,"r")
        content = common_subject_file.read()
        json_data = json.loads(content)
        for record in json_data:
            self.subject_levels[record["subject_id_fk"]] = record["level_id_fk"]

    def read_preferred_level_subjects(self):
        file_path = os.path.dirname(os.path.realpath(__file__))
        file_path = os.path.join(file_path, 'tutor_migrations')
        file_name = os.path.join(file_path, 'preferred_level_subject.json')
        common_subject_file = open(file_name,"r")
        content = common_subject_file.read()
        json_data = json.loads(content)
        for record in json_data:
            if not self.peferred_level_subjects.get(record["tutor_id"]):
                self.peferred_level_subjects[record["tutor_id"]] = [{
                    "level_id": record["level_id_fk"],
                    "subject_id": record["subject_id_fk"]
                }]
            else:
                self.peferred_level_subjects[record["tutor_id"]] += [{
                    "level_id": record["level_id_fk"],
                    "subject_id": record["subject_id_fk"]
                }]

    def read_common_education(self):
        file_path = os.path.dirname(os.path.realpath(__file__))
        file_path = os.path.join(file_path, 'tutor_migrations')
        file_name = os.path.join(file_path, 'common_education.json')
        common_subject_file = open(file_name,"r")
        content = common_subject_file.read()
        json_data = json.loads(content)
        for record in json_data:
            self.common_education[record["educational_id"]] = record["educational_name"]

    def read_countries(self):
        file_path = os.path.dirname(os.path.realpath(__file__))
        file_path = os.path.join(file_path, 'tutor_migrations')
        file_name = os.path.join(file_path, 'countries.json')
        common_subject_file = open(file_name,"r")
        content = common_subject_file.read()
        json_data = json.loads(content)
        for record in json_data:
            cid = record["id"]
            name = record["name"]
            alpha2 = record["alpha_2"]
            alpha3 = record["alpha_3"]
            if not Country.objects.filter(name=name,code=alpha2).exists():
                country_object = Country()
                country_object.name = name
                country_object.code = alpha2
                country_object.save()

            if not SupportedCountry.objects.filter(type=SupportedCountry.__name__,name=name,code=alpha2).exists():
                sc_ = SupportedCountry()
                sc_.name = name
                sc_.code = alpha2
                sc_.type = SupportedCountry.__name__
                sc_.save()
                country_object = sc_
            else:
                country_object = SupportedCountry.objects.filter(type=SupportedCountry.__name__,name=name,code=alpha2).first()
            self.countries[cid] = country_object


    def create_tutor(self, email, username, nationality,name,last_name,current_school,major,gender,timezone,experience,subject_levels):
        ###create teacher
        try:
            tutor = User.objects.create_user(email=email, username=email, password="123456")
        except Exception,msg:
            tutor = User.objects.get(username=email)
        #nationality = nationalities[randint(0,len(nationalities) - 1)]
        print "Creating tutor..."
        if ChampUser.objects.filter(user_id=tutor.pk).exists():
            wallet = ChampUser.objects.filter(user_id=tutor.pk).first().wallet
        else:
            wallet = Wallet()
            wallet.save()
        champ_users = ChampUser.objects.filter(user=tutor,fullname=name, type=self.teacher_role,wallet_id=wallet.pk)
        if champ_users.exists():
            champ_user = champ_users.first()
        else:
            champ_user = ChampUser()
            champ_user.user = tutor
        champ_user.fullname = name
        champ_user.lastname = last_name
        champ_user.type = self.teacher_role
        champ_user.wallet_id = wallet.pk
        champ_user.current_school = current_school
        champ_user.major = major
        champ_user.gender = gender.lower()
        champ_user.nationality_id = nationality.pk
        champ_user.timezone = timezone
        champ_user.save()
        print "Tutor created."

        print("Adding Majors")

        for subject_level in subject_levels:
            subject = subject_level["subject"]
            level = subject_level["level"]

            level_object = SubjectLevel.objects.filter(name=level)
            if level_object.exists():
                level_object = level_object.first()
            else:
                level_object = SubjectLevel()
                level_object.name = level
                level_object.save()

            if not Major.objects.filter(name=subject,level=level_object).exists():
                major_object = Major()
                major_object.name = subject
                major_object.level = level_object
                major_object.creator = tutor
                major_object.save()
            else:
                major_object = Major.objects.filter(name=subject, level=level_object).first()


            if not UserMajor.objects.filter(user=tutor,major=major_object).exists():
                user_major_object = UserMajor()
                user_major_object.user = tutor
                user_major_object.major = major_object
                user_major_object.save()


        if TeachingExperiences.objects.filter(user=tutor).exists():
            teaching_experiences = TeachingExperiences.objects.filter(user=tutor).first()
        else:
            teaching_experiences = TeachingExperiences()
        teaching_experiences.experience = experience
        teaching_experiences.user = tutor
        teaching_experiences.save()

    def get_timezone_from_country_name(self, country_name, include_class=False):
        try:
            return self.country_timezones[country_name][0]
        except:
            return ""

    def handle(self, *args, **options):

        self.read_subjects()
        self.read_common_levels()
        self.read_subject_levels()
        self.read_preferred_level_subjects()
        self.read_countries()
        self.read_common_education()
        self.read_country_timezones()

        role_teacher,created = Role.objects.get_or_create(name=UserTypes.teacher.value)
        self.teacher_role = role_teacher

        file_path = os.path.dirname(os.path.realpath(__file__))
        file_path = os.path.join(file_path, 'tutor_migrations')
        file_name = os.path.join(file_path, 'tutor.json')
        tutor_file = open(file_name,"r")
        content = tutor_file.read()
        json_data = json.loads(content)
        tutor_file.close()

        for record in json_data:
            email = record["email"]
            password = "123456"
            name = record["name"]
            last_name = record["lname"]
            dob = record["dob"]
            address = record["address"]
            tutor_experience = record["tutor_experience"]
            university = record["university"]
            country = record["nationality_id_fk"]
            education_id = record["educational_id_fk"]
            gender = record["gender"]
            university_course = record["university_course"]
            if country:
                country_object = self.countries.get(country)
                timezone = ""
            else:
                country_name = "Singapore"
                country_code = "sg"
                if not Country.objects.filter(name=country_name,code=country_code).exists():
                    country_object = Country()
                    country_object.name = country_name
                    country_object.code = country_code
                    country_object.save()

                if not SupportedCountry.objects.filter(type=SupportedCountry.__name__,name=country_name,code=country_code).exists():
                    sc_ = SupportedCountry()
                    sc_.name = country_name
                    sc_.code = country_code
                    sc_.type = SupportedCountry.__name__
                    sc_.save()
                    country_object = sc_
                else:
                    country_object = SupportedCountry.objects.filter(type=SupportedCountry.__name__,name=country_name,code=country_code).first()

            current_school = self.common_education.get(education_id)
            if not current_school:
                current_school = ""

            s_l_list = []
            psubject_levels = self.peferred_level_subjects.get(record["tutor_id"])
            if psubject_levels:
                for s_l in psubject_levels:
                    subject_name = self.subjects.get(s_l["subject_id"])
                    level_name = self.common_levels.get(s_l["level_id"])
                    s_l_list += [
                        {
                            "subject": subject_name,
                            "level": level_name
                        }
                    ]

            try:
                self.create_tutor(email, email, country_object,name,last_name,current_school,university_course,gender,self.get_timezone_from_country_name(country_object.name),tutor_experience,s_l_list)
            except:
                print("Exception Found! Skipping...")


