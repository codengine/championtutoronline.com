
from django.core.management.base import BaseCommand

from django_rq import job

import redis
from django.conf import settings
from rq import Queue, Worker, get_failed_queue, push_connection
from rq_scheduler import Scheduler

def add_task():
    print("Task Added!")

class Command(BaseCommand):
    def handle(self, *args, **options):
        import django_rq
        queue = django_rq.get_queue('high')
        print(queue)
        queue.enqueue(add_task)

        opts = getattr(settings, 'RQ', {}).copy()
        opts.pop('eager', None)
        connection = redis.Redis()
        print(connection)
        queues = Queue.all(connection=connection)
        print(queues)
        workers = Worker.all(connection=connection)
        print(workers)
        scheduler = Scheduler(connection=connection)
        get_queue = lambda job: job.origin
        all_jobs = sorted(scheduler.get_jobs(), key=get_queue)
        print(scheduler.get_jobs())
