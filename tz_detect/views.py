# -*- coding: utf-8 -*-

from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View


class SetOffsetView(View):
    http_method_names = ['post']

    def post(self, request, *args, **kwargs):
        offset = request.POST.get('offset', None)
        if not offset:
            return HttpResponse("No 'offset' parameter provided", status=400)

        try:
            offset = int(offset)
        except ValueError:
            return HttpResponse("Invalid 'offset' value provided", status=400)

        hour_offset = offset/60



        request.session['detected_tz'] = int(offset)

        return HttpResponse("OK")

class DetectUserInfoView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(DetectUserInfoView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        country = request.POST.get("country")
        if country:
            request.session["visitor_info"] = {
                "country": country
            }
        return HttpResponse("OK")
