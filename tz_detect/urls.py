# -*- coding: utf-8 -*-

from django.conf.urls import url
from tz_detect.views import DetectUserInfoView

from .views import SetOffsetView

urlpatterns = [
    url(r'^set/$', SetOffsetView.as_view(), name="tz_detect__set"),
    url(r'^set-user-info/$', DetectUserInfoView.as_view(), name="tz_detect_user_info"),
]
