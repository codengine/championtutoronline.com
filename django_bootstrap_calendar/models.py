# -*- coding: utf-8 -*-
from django.contrib.auth.models import User

__author__ = 'sandlbn'

from django.db import models
from django.utils.translation import ugettext_lazy as _
from utils import datetime_to_timestamp


class CalendarEvent(models.Model):
    """
    Calendar Events
    """
    CSS_CLASS_CHOICES = (
        ('', _('Normal')),
        ('event-warning', _('Warning')),
        ('event-info', _('Info')),
        ('event-success', _('Success')),
        ('event-inverse', _('Inverse')),
        ('event-special', _('Special')),
        ('event-important', _('Important')),
    )
    title = models.CharField(max_length=255, verbose_name=_('Title'))
    url = models.URLField(verbose_name=_('URL'), null=True, blank=True)
    css_class = models.CharField(max_length=20, verbose_name=_('CSS Class'),
                                 choices=CSS_CLASS_CHOICES)
    start = models.DateTimeField(verbose_name=_('Start Date'))
    end = models.DateTimeField(verbose_name=_('End Date'), null=True,
                               blank=True)
    user = models.ForeignKey(User)

    @property
    def start_timestamp(self):
        """
        Return end date as timestamp
        """
        return datetime_to_timestamp(self.start)

    @property
    def end_timestamp(self):
        """
        Return end date as timestamp
        """
        return datetime_to_timestamp(self.end)

    def __unicode__(self):
        return self.title

    @classmethod
    def update_event_time(cls, user_id, start_time, end_time, new_start_time, new_end_time):
        calender_events = cls.objects.filter(user_id=user_id, start__gte=start_time, end__lte=end_time)
        if calender_events.exists():
            calendar_event = calender_events.first()
            calendar_event.start = new_start_time
            calendar_event.end = new_end_time
            calendar_event.save()
            return calendar_event
