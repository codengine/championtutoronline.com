# -*- coding: utf-8 -*-
__author__ = 'sandlbn'

from django import template
from django.conf import settings
from django.db.models.query_utils import Q
from datetime import datetime, date, time, timedelta
from time import mktime
from jsmin import jsmin
from calendar import HTMLCalendar
from core.library.Clock import Clock


def timestamp_to_datetime(timestamp):
    """
    Converts string timestamp to datetime
    with json fix
    """
    if isinstance(timestamp, (str, unicode)):

        if len(timestamp) == 13:
            timestamp = int(timestamp) / 1000

        return datetime.fromtimestamp(timestamp)
    else:
        return ""


def datetime_to_timestamp(date):
    """
    Converts datetime to timestamp
    with json fix
    """
    if isinstance(date, datetime):

        timestamp = mktime(date.timetuple())
        json_timestamp = int(timestamp) * 1000

        return '{0}'.format(json_timestamp)
    else:
        return ""


class MinifyJs(template.Node):

    def __init__(self, nodelist):
        self.nodelist = nodelist

    def render(self, context):
        return jsmin(self.nodelist.render(context))


# Constants for months
January = 1
February = 2


class MonthCalendar(HTMLCalendar):

    def __init__(self, lessons, timezone, table, **kwargs):
        super(MonthCalendar, self).__init__()
        self.lessons = lessons
        self.tz = timezone
        self.table = table
        self.kwargs = kwargs

    def formatday(self, date_row):
        """
        Return a day as a table cell.
        """

        day_lessons = [lesson.id for lesson in self.lessons if datetime.fromtimestamp(float(lesson.lesson_time)).date()==date_row]
        day_lessons_queryset = self.lessons.filter(id__in=day_lessons)
        if date_row.month != self.month:
            return '<td class="next_month">%d</td>' % date_row.day  # day outside month
        else:
            return '<td>{0}</td>'.format(self.format_td_data(day_lessons_queryset, date_row.day))


    def formatweek(self, week):
        """
        Return a complete week as a table row.
        """
        s = ''.join(self.formatday(date_row) for date_row in week)
        return '<tr>%s</tr>' % s

    def formatmonth(self, year, month, css_class="champion_calendar",
                    withyear=True, table="month", formatheadmonthname=False):
        self.year, self.month = year, month
        """
        Return a formatted month as a table.
        """
        v = []
        a = v.append
        if formatheadmonthname:
            a('<h4 class="yearly_heading">%s</h4>' % self.formatmonthname(year, month, withyear=True))
        a('<table class="%s">' % css_class)
        a('<tbody>')
        if self.table == "month":  # formatmonthname is not needed in yearly calendar
            a('\n')
            a(self.formatmonthname(year, month, withyear=withyear))
        a('\n')
        a(self.formatweekheader())
        a('\n')
        dates = list(self.itermonthdates(year, month))
        self.month = month
        records = [ dates[i:i+7] for i in range(0, len(dates), 7) ]
        for week in records:
            a(self.formatweek(week))
            a('\n')
        a('</tbody>')
        a('</table>')
        a('\n')
        return ''.join(v)

    def formatyear(self, year, width=3):
        """
        Return a formatted year as a table of tables.
        """

        v = []
        a = v.append

        a('\n')

        for i in range(January, January+12, width):
            # months in this row
            months = range(i, min(i+width, 13))
            a('<div class="yearly_calendar">')
            for month in months:
                a('<div class="yearly_calendar_inner">')
                a('<h4 class="yearly_heading">%s</h4>' % self.formatmonthname(year, month, withyear=False))

                a(self.formatmonth(year, month, withyear=False, css_class="yearly_cal"))

                a('</div>')

            a('</div>')
            a('\n')

        return ''.join(v)

    def format_td_data(self, day_lessons_queryset, day):

        if day_lessons_queryset.count() == 0:
            return day
        else:
            now_ts = Clock.utc_timestamp()

            upcoming_lessons = day_lessons_queryset.filter(Q(lesson_time__gte=now_ts) &
                                                           ~Q(status=4)).exists()

            attended_lessons_list = day_lessons_queryset.filter((Q(lesson_time__lte=now_ts) &
                                                               Q(payment_status=1) & Q(type=0)) |
                                                              (Q(due_date__lte=now_ts) &
                                                               Q(payment_status=1) & Q(type=1)))
            attended_lessons = attended_lessons_list.exists()

            missed_lessons_list = day_lessons_queryset.filter((Q(lesson_time__lte=now_ts) & Q(type=0)) |
                                                              (Q(due_date__lte=now_ts) & Q(type=1)))
            missed_lessons = missed_lessons_list.exists()

            cancelled_lessons_list = day_lessons_queryset.filter(status=4)
            cancelled_lessons = cancelled_lessons_list.exists()

            v = []
            a = v.append

            a('\n')
            if missed_lessons or attended_lessons or cancelled_lessons:
                if self.table == 'month':  # Format data for month table
                    a('<div class="cal_bg_yellow"><span>%d</span></div>' % day)
                    a('<div class="lesson_detail">')
                    a('<span class="lesson_chap_detail">')
                    if attended_lessons:
                        a('<img src="%simages/lesson-icon1.png" width="19" height="20" alt="img">' % settings.STATIC_URL)
                    if missed_lessons:
                        a('<img src="%simages/lesson-icon2.png" width="14" height="14" alt="img">' % settings.STATIC_URL)
                    if cancelled_lessons:
                        a('<img src="%simages/lesson-icon2.png" width="14" height="14" alt="img">' % settings.STATIC_URL)
                    a('</span>')
                    a('<div class="lesson_chap_summary displaynone">')
                    for lesson in day_lessons_queryset:
                        job = lesson.lesson_request.all().first()
                        status = ''
                        if lesson in missed_lessons_list:
                            status = "Missed"
                        if lesson in cancelled_lessons_list:
                            status = "Cancelled"
                        a('<p>')
                        a('%s - %s, %s (<a href="#">%s</a>) <em>%s</em>' %
                          (datetime.fromtimestamp(float(lesson.lesson_time)).strftime("%d %b"),
                           job.title, lesson.render_subjects, lesson.request_user.champuser.fullname, status))
                        a('</p>')

                    a('</div>')
                    a('</div>')
                if self.table == "year":  # Format data for year tables

                    if self.kwargs.get("view", "full") == "small":
                        a('<div class="cal_bg_yellow"><span>%d</span></div>' % day)
                        a('<div class="lesson_detail">')
                        a('<span class="lesson_chap_detail">')
                        if attended_lessons:
                            a(
                                '<img src="%simages/lesson-icon1.png" width="19" height="20" alt="img">' % settings.STATIC_URL)
                        if missed_lessons:
                            a(
                                '<img src="%simages/lesson-icon2.png" width="14" height="14" alt="img">' % settings.STATIC_URL)
                        if cancelled_lessons:
                            a(
                                '<img src="%simages/lesson-icon2.png" width="14" height="14" alt="img">' % settings.STATIC_URL)
                        a('</span>')
                        a('<div class="lesson_chap_summary displaynone">')
                        for lesson in day_lessons_queryset:
                            job = lesson.lesson_request.all().first()
                            status = ''
                            if lesson in missed_lessons_list:
                                status = "Missed"
                            if lesson in cancelled_lessons_list:
                                status = "Cancelled"
                            a('<p>')
                            a('%s - %s, %s (<a href="#">%s</a>) <em>%s</em>' %
                              (datetime.fromtimestamp(float(lesson.lesson_time)).strftime("%d %b"),
                               job.title, lesson.render_subjects, lesson.request_user.champuser.fullname, status))
                            a('</p>')

                        a('</div>')
                        a('</div>')
                    else:
                        a('<div class="year_topic">%d' % day)
                        a('<span class="year_topic_icon">')
                        if attended_lessons:
                            a('<img src="%simages/chapter-icon.png" width="10" height="10" alt="icon">' % settings.STATIC_URL)
                        if missed_lessons:
                            a('<img src="%simages/chapter-icon2.png" width="8" height="8" alt="icon">' % settings.STATIC_URL)
                            #a('<img src="/static/images/chapter-icon3.png" width="10" height="10" alt="icon">')
                        if cancelled_lessons:
                            a('<img src="%simages/chapter-icon2.png" width="8" height="8" alt="icon">' % settings.STATIC_URL)
                        a('</span>')
                        a('<div class="lesson_chap_summary displaynone">')
                        for lesson in day_lessons_queryset:
                            job = lesson.lesson_request.all().first()
                            status = ''
                            if lesson in missed_lessons_list:
                                status = "Missed"
                            if lesson in cancelled_lessons_list:
                                status = "Cancelled"
                            a('<p>')
                            a('%s - %s, %s (<a href="#">%s</a>) <em>%s</em>' %
                              (datetime.fromtimestamp(float(lesson.lesson_time)).strftime("%d %b"),
                               job.title, lesson.render_subjects, lesson.request_user.champuser.fullname, status))
                            a('</p>')
                        a('</div>')
                        a('</div>')
                if self.table == "week":  # Format data for week table

                    a('<div class="cal_bg_yellow">')
                    a('<div class="lesson_weekly">')
                    if attended_lessons:
                        a('<img width="19" height="20" alt="img" src="%simages/lesson-icon1.png">' % settings.STATIC_URL)
                    if missed_lessons:
                        a('<img width="14" height="14" alt="img" src="%simages/lesson-icon2.png">' % settings.STATIC_URL)
                    if cancelled_lessons:
                        a('<img width="14" height="14" alt="img" src="%simages/lesson-icon2.png">' % settings.STATIC_URL)
                    a('</div>')
                    a('</div>')

            elif upcoming_lessons:
                if self.table == 'month': # Format data for month table for upcoming lessons
                    a('<span class="calendar_detail">%d</span>' % day)
                    a('<div class="chap_summary_outer">')
                    a('<div class="lesson_chap_summary displaynone">')
                    for lesson in day_lessons_queryset:
                        job = lesson.lesson_request.all().first()
                        if job.lesson_type == 0:
                            lesson_date = (datetime.fromtimestamp(float(lesson.lesson_time)).strftime("%d %b (%I:%M %p)"))
                        elif job.lesson_type == 1:
                            lesson_date = (datetime.fromtimestamp(float(lesson.due_date)).strftime("%d %b (%I:%M %p)"))

                        a('<p>%s - %s, %s (<a href="#">%s</a>)</p>' %
                          (lesson_date, job.title, lesson.render_subjects, lesson.request_user.champuser.fullname))
                    a('</div>')
                    a('</div>')

                if self.table == "year":  # Format data for year tables for upcoming lessons
                    if self.kwargs.get("view", "full") == "small":
                        a('<span class="calendar_detail">%d</span>' % day)
                        a('<div class="chap_summary_outer">')
                        a('<div class="lesson_chap_summary displaynone">')
                        for lesson in day_lessons_queryset:
                            job = lesson.lesson_request.all().first()
                            if job.lesson_type == 0:
                                lesson_date = (
                                datetime.fromtimestamp(float(lesson.lesson_time)).strftime("%d %b (%I:%M %p)"))
                            elif job.lesson_type == 1:
                                lesson_date = (
                                datetime.fromtimestamp(float(lesson.due_date)).strftime("%d %b (%I:%M %p)"))

                            a('<p>%s - %s, %s (<a href="#">%s</a>)</p>' %
                              (lesson_date, job.title, lesson.render_subjects, lesson.request_user.champuser.fullname))
                        a('</div>')
                        a('</div>')
                    else:
                        a('<span class="year_chap">%d</span>' % day)
                        a('<div class="lesson_chap_summary displaynone">')
                        for lesson in day_lessons_queryset:
                            job = lesson.lesson_request.all().first()
                            if job.lesson_type == 0:
                                lesson_date = (
                                datetime.fromtimestamp(float(lesson.lesson_time)).strftime("%d %b (%I:%M %p)"))
                            elif job.lesson_type == 1:
                                lesson_date = (
                                datetime.fromtimestamp(float(lesson.due_date)).strftime("%d %b (%I:%M %p)"))

                            a('<p>%s - %s, %s (<a href="#">%s</a>)</p>' %
                              (lesson_date, job.title, lesson.render_subjects, lesson.request_user.champuser.fullname))
                        a('</div>')

            else:
                a(str(day))

        return ''.join(v)


def week_range(day):
    """Find all dates in a week for the given day.
    Assuming weeks start on Monday and end on Sunday.

    Returns a list of all dates of the week within a given day.

    """
    # isocalendar calculates the year, week of the year, and day of the week.
    # dow is Mon = 1, Sat = 6, Sun = 7
    year, week, dow = day.isocalendar()

    # Find the first day of the week.
    start_date = day - timedelta(dow-1)

    dates = []
    for i in range(7):
        dates.append(start_date + timedelta(days=i))

    return dates


def day_calendar(day, lessons, tz, table):
    """
    Return a formatted day as a table.
    """
    v = []
    a = v.append
    a('<table class="calendar_daily">')
    a('<tr>')
    a('<th><img src="%simages/timer.png" src="Time"></th>' % settings.STATIC_URL)
    a('<th> %s </th>' % day.strftime("%A, %d %B %Y"))
    a('</tr>')
    hours = [time(i).strftime('%I %p') for i in range(24)]
    for hour in hours:
        a('<tr>')
        a('<td>%s</td>'% hour)
        hour_lessons = get_hour_lessons(day, hour, lessons)
        month_calendar = MonthCalendar(lessons, tz, table)
        if hour_lessons.count() == 0:
            a('<td></td>')
        else:
            a('<td>{0}</td>'.format(month_calendar.format_td_data(hour_lessons, day.day)))

        a('</tr>')
    a('</table>')
    return ''.join(v)


def week_calendar(week, lessons, tz, table):
    """
    Return a formatted week as a table.
    """

    v = []
    a = v.append
    a('<table class="calendar_weekely">')
    s = ''.join('<th>%s <span class="timer_date">%s</span> </th>' % (day.strftime("%A"), day.day) for day in week)
    a('<tr>')
    a('<th><img src="%simages/timer.png" src="Time"></th>' % settings.STATIC_URL)
    a(s)
    a('</tr>')
    hours = [time(i).strftime('%I %p') for i in range(24)]
    for hour in hours:
        a('<tr>')
        a('<td>%s</td>'% hour)
        for day in week:

            hour_lessons = get_hour_lessons(day, hour, lessons)

            month_calendar = MonthCalendar(lessons, tz, table)
            if hour_lessons.count() == 0:
                a('<td></td>')
            else:
                a('<td>{0}</td>'.format(month_calendar.format_td_data(hour_lessons, day.day)))
        a('</tr>')
    a('</table>')
    return ''.join(v)


def get_hour_lessons(day, hour, lessons):
    lesson_hour = datetime.strptime(hour, '%I %p').time()

    lesson_hour_date = datetime.combine(day, lesson_hour).strftime('%Y %B %d %I %p')

    day_lessons = [lesson.id for lesson in lessons if
                           datetime.fromtimestamp(float(lesson.lesson_time))
                                   .strftime('%Y %B %d %I %p') == lesson_hour_date]
    hour_lessons_queryset = lessons.filter(id__in=day_lessons)

    return hour_lessons_queryset
