import os
from easy_thumbnails.conf import Settings as thumbnail_settings
from engine.config.debug_settings import DEBUG_ENABLED

from engine.config.directories import STATIC_FILES_DIRS
from engine.config.servers import APP_SERVER_IP, APP_SERVER_PORT

from engine.config.social_apps import VIMEO_API_CLIENT_ID, VIMEO_API_CLIENT_SECRET, VIMEO_API_ACCESS_TOKEN, \
    FACEBOOK_EXTENDED_PERMISSIONS

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

ENVIRONMENT = os.environ

DEBUG = bool(ENVIRONMENT.get('DEBUG', DEBUG_ENABLED))
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Md Shariful Islam', 'codenginebd@gmail.com'),
    # ('Wilson Tan', 'tanwilson83@yahoo.com.sg'),
)

MANAGERS = ADMINS

ALLOWED_HOSTS = ("127.0.0.1", "166.62.85.122", "188.166.211.13")


# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.mysql',
#         'NAME': 'champ_db',
#         'USER': 'root',
#         'PASSWORD': '',
#         'HOST': '127.0.0.1',
#         'PORT': '',
#         }
# }

from engine.config.database import DATABASES_CONFIG

DATABASES = DATABASES_CONFIG

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.mysql',
#         'NAME': 'champ_db',
#         'USER': 'champ_user',
#         'PASSWORD': '123456',
#         'HOST': '127.0.0.1',
#         'PORT': '',
#         }
# }


# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
#     }
# }

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'America/New_York'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True
TIME_ZONE = 'UTC'

SITE_URL = APP_SERVER_IP+":"+str(APP_SERVER_PORT)+"/" #'http://'+APP_SERVER_HOST+":"+str(APP_SERVER_PORT)+"/"

# http://morethanseven.net/2009/02/11/django-settings-tip-setting-relative-paths.html
SITE_ROOT = os.path.dirname(os.path.realpath(__file__))

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.path.join(SITE_ROOT, 'media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = os.path.join(SITE_ROOT, 'static')

MAX_UPLOAD_SIZE = "20971520" #20M

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

ADMIN_MEDIA_PREFIX = '/static/admin/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

STATICFILES_DIRS += STATIC_FILES_DIRS


# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = ENVIRONMENT.get('SECRET_KEY', 'a^&%6yp#2eudk%+5v-7tkhn+hxfm2_zmm83rpp&!oe7(n@-tta')

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.filesystem.Loader',
#     'django.template.loaders.eggs.Loader',
)

FILE_UPLOAD_HANDLERS = ("django.core.files.uploadhandler.MemoryFileUploadHandler",
                        "django.core.files.uploadhandler.TemporaryFileUploadHandler",)

MIDDLEWARE_CLASSES = (
    #'django.middleware.cache.UpdateCacheMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'engine.middleware.detect_country_middleware.DetectCountryMiddleware',
    'crequest.middleware.CrequestMiddleware',
    'engine.middleware.error_log_middleware.ErrorLogMiddleware',
    'engine.middleware.create_session_middleware.CreateSessionMiddleware',
    'engine.middleware.timezone_middleware.TimezoneMiddleware',
    'engine.middleware.exception_handler_middleware.ExceptionHandleMiddleware',
    'engine.middleware.update_breadcumb.UpdateBreadcumbMiddleware'
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    'django.core.context_processors.request',
    "django.contrib.messages.context_processors.messages",
    "common.common_data_processor.user_common_info"
    )

ROOT_URLCONF = 'urls'


from engine.config.servers import *

#FILE_SIZE_25MB_IN_BYTES = 26214400

from engine.config.API import *

from engine.config.email import *

from engine.config.redis_queue import *

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'wsgi.application'

SESSIONENGINE = 'django.contrib.sessions.backends.db'
SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(SITE_ROOT, 'templates'),
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    'django.contrib.admindocs',
    'django.contrib.humanize',
    'crequest',
    'easy_thumbnails',
    'image_cropping',
    'django_bootstrap_calendar',
    # 'social_auth',
    'core',
    'authentication',
    'common',
    'pyetherpad',
    'pysharejs',
    'payment',
    'notification',
    'forever_tasks',
    "django_rq",
    "feedback",
    "servey",
    "admin",
    "admin.users",
    "admin.logs",
    "admin.transactions",
    "admin.lessons",
    "admin.jobs",
    "captcha"
    #'south'
)

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'core.backends.facebook.FacebookBackend',
    'core.backends.email_verification_auto_login.AutoLoginBackend'
)

VIMEO_API = {
    "CLIENT_ID": VIMEO_API_CLIENT_ID,
    "CLIENT_SECRET": VIMEO_API_CLIENT_SECRET,
    "ACCESS_TOKEN": VIMEO_API_ACCESS_TOKEN
}

LOGIN_URL          = '/login-form/'
LOGIN_REDIRECT_URL = '/logged-in/'
SOCIAL_AUTH_BACKEND_ERROR_URL = '/login-error/'

FACEBOOK_EXTENDED_PERMISSIONS = FACEBOOK_EXTENDED_PERMISSIONS

# SESSION_SERIALIZER='django.contrib.sessions.serializers.PickleSerializer'

#INSTALLED_APPS += APPS

#INSTALLED_APPS += ('south',)

from datetime import timedelta
CELERYBEAT_SCHEDULE = {
    'add-every-30-seconds': {
        'task': 'forever_tasks.tasks.process_clicks',
        'schedule': timedelta(seconds=30),
        'args': ()
    },
    }

CELERY_TIMEZONE = 'UTC'

RQ_QUEUES = RQ_QUEUES

BROKER_URL = BROKER_URL

LOGIN_URL = '/'
LOGIN_REDIRECT_URL = '/'
LOGOUT_URL = '/logout/'

CODEPAD_DEFAULT_LANGUAGE = "javascript"

RESOURCE_DIR = os.path.join(SITE_ROOT,"resources")

NID_FILES_DIR = os.path.join(MEDIA_ROOT,"NRIC_Images")

IMAGE_DIR = os.path.join(MEDIA_ROOT,"image")

MSG_ATTACHMENT_DIR = os.path.join(MEDIA_ROOT,"Msg_Attachment")

TEMP_DIR = os.path.join(MEDIA_ROOT,"TEMP_DIR")

DRAWINGBOARD_IMAGE_DIR = os.path.join(MEDIA_ROOT,"drawingboard_images")

DRAWINGBOARD_IMAGE_URL = os.path.join(MEDIA_URL,"drawingboard_images")

CIPHER_KEY = "lapsso065120145hg%$#8&6%$@ghkpod"

TEMP_DIR = os.path.join(MEDIA_ROOT,"Temp_Upload")

Q_ATTACHMENT_DIR = os.path.join(MEDIA_ROOT,"QAttachment")

CERTIFICATES_DIR = os.path.join(MEDIA_ROOT, "Certificates")

CERTIFICATES_THUMNAILS_DIR = os.path.join(CERTIFICATES_DIR, "Thumbnails")

MSG_ATTACHMENT_DIR_NAME = "Msg_Attachment"

Q_ATTACHMENT_DIR_NAME = "QAttachment"

def create_all_dirs():
    dir_list = [
        RESOURCE_DIR, NID_FILES_DIR, IMAGE_DIR, MSG_ATTACHMENT_DIR, TEMP_DIR, DRAWINGBOARD_IMAGE_DIR,
        Q_ATTACHMENT_DIR, CERTIFICATES_DIR, CERTIFICATES_THUMNAILS_DIR
    ]

    for dir in dir_list:
        try:
            if not os.path.exists(dir):
                os.makedirs(dir)
        except Exception as exp:
            pass

create_all_dirs()


#Captcha settings.

RECAPTCHA_PRIVATE_KEY = "6Lc1tiQTAAAAABY6_N_aQPEwU5wh01PRxH6K_T26"
RECAPTCHA_PUBLIC_KEY = "6Lc1tiQTAAAAADB2jYwRNmBMQxAMnaby9QmEzyuJ"


SUPPORTED_LANGUAGES = [
    ("EN","English"),
    ("BN","Banglad")
]

###Opentok Account: tokboxtest2

##Braintree part begins

import braintree
if DEBUG:
    braintree.Configuration.configure(
        braintree.Environment.Sandbox,
        BRAINTREE_SANDBOX_MERCHANT_ID,
        BRAINTREE_SANDBOX_PUBLIC_KEY,
        BRAINTREE_SANDBOX_PRIVATE_KEY
    )
else:
    braintree.Configuration.configure(
        braintree.Environment.Production,
        BRAINTREE_PRODUCTION_MERCHANT_ID,
        BRAINTREE_PRODUCTION_PUBLIC_KEY,
        BRAINTREE_PRODUCTION_PRIVATE_KEY
    )

##Braintree part ends

COMMON_DATA_CONTEXT = {
    "socketio_url": SOCKETIO_HOST+'/'+str(SOCKETIO_PORT)
}

THUMBNAIL_PROCESSORS = (
    'image_cropping.thumbnail_processors.crop_corners',
) + thumbnail_settings.THUMBNAIL_PROCESSORS

SERVER_TOKEN = "r@#$Mecs2014$$*&%$#hdsj$#@%()*^%4647234135HFFG45%$(*#$@JHGFT!()(*&*28*(&$#@%&^$%"
