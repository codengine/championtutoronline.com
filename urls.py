from django.conf.urls import patterns, url, include
from core.lessonviews import WrittenLessonRequestView, LessonsView, StudentProgressReportView, ProgressReportListView, ProgressReportDetailsView, CreatePostView, CreateQPostView, \
    CurrencyRateListView, CurrencyRateDetailsView, LessonFeedbackView, JobApplicationListView, \
    JobApplicationDetailsView, JobApplicationReplyView, JobApplicationAgreementActionView, ApplyJobAjaxView, \
    RejectJobAjaxView, RejectJobApplicationAjaxView, RejectJobOfferAjaxView, JobApplicationWithdrawView, \
    RejectJobRecommendationAjaxView
from core.lessonviewsajax import LessonRequestAjaxView, WrittenLessonSubjectAjaxView, WrittenLessonCommentView, LiveLessonAjaxView, LiveLessonRequestMutate, \
    JobApplicationUpdateAjaxView, NegotiateJobAjaxView, OpenHireDialogAjaxView, ShortlistJobAjaxView, \
    InviteTutorAjaxView, GetEmailTemplateAjax, LessonStatusUpdateAjaxView, LessonRescheduleAjaxView
from core.search_views_ajax import SearchTutorListAjaxView, AjaxMarketplaceJobsView, AjaxRecommendedTutorsView
from core.views import *
from core.viewsajax import *
from core.viewsajax import FetchTutorListView
from core.viewsmessages import MessageView, MessageDetailView, MessageConversationDetailView
from core.viewsmessagesajax import MessageAjaxView, SendMessageAjaxView, TutorListAjaxView, LoadMessagesDetailsAjaxView, TutorMessageAjaxView, \
    ChatboxMessageAjaxView, SendChatMessageAjaxView, LoadConversationDetailsAjaxView, MarkMessageMutateAjaxView, \
    AddBuddyToMessageConversationAjaxView, LeaveMessageConversationAjaxView, DeleteMessageAjaxView, ChatSettingsAjaxView, \
    SendBatchMessagesAjaxView, ConversationRenameAjaxView, ManageChatBlockListAjaxView, FetchWbMessages, \
    UpdateWbChatboxStatus, DisableWbVideoSharingAjaxView
from core.viewssearch import *
from core.decorators import user_login_required
from django_bootstrap_calendar.views import CalendarJsonListView
from forms import AboutMeUpdateForm
import pyetherpad
from django.conf.urls.static import static
from django.conf import settings
from payment.viewsajax import *
from core.search_views import TutorSearchView, MarketplaceView, MarketplaceDetailsView
from core.viewssearchajax import TutorSearchAjaxView, JobsStudentSearchAjaxView, FetchSubjectsLevelAjaxView
from django.conf.urls import (
handler400, handler403, handler404, handler500
)

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'championtutoronline.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include('admin.urls')),
    url(r'^$', HomePage.as_view(), name='home_page'),
    url(r'^login$',LoginView.as_view(),name='user_login'),
    url(r'^signup-success/$',SignUpSuccessView.as_view(),name='user_signup_success'),
    url(r'^logout$',LogoutView.as_view(),name='user_logout'),
    url(r'^signup$',SignUpView.as_view(),name='sign_up'),
    url(r'^social-signup/$',SocialSignUpFormView.as_view(),name='social_sign_up'),
    url(r'^myaccount$',MyAccountView.as_view(),name='my_account'),
    url(r'^billing',BillingView.as_view(),name='billing'),
    url(r'^email_verification/$',EmailVerification.as_view(),name='email_verification'),
    url(r'^access_violation/$',AccessViolation.as_view(),name='access_violation'),

    ##All whiteboard related urls here.
    url(r'^whiteboard$', user_login_required(WhiteboardView.as_view()), name='whiteboard'),
    url(r'^lesson/(?P<pk>[0-9]+)/', LiveLessonView.as_view(), name='live_lesson'),
    url(r'^lesson-feedback/(?P<pk>[0-9]+)/$', LessonFeedbackView.as_view(), name='lesson_feedback'),
    url(r'^lesson/demo/$', WhiteboardDemo.as_view(), name='lesson_demo'),
    url(r'^update-status/$', UpdateAvailableStatus.as_view(), name='update_available_status'),
    url(r'^(?P<pk>[0-9]+)/lesson-post/create/$', CreatePostView.as_view(), name='create_post'),
    url(r'^(?P<pk>[0-9]+)/q-post/create/$', CreateQPostView.as_view(), name='create_qpost'),
    url(r'^ajax/action_whiteboard_tab$', WhiteboardAjaxView.as_view(), name='ajax_action_whiteboard_tab'),
    url(r'^ajax/load_fx_menu$', LoadDrawingboardFXAjaxView.as_view(), name='ajax_load_fx_menu'),
    url(r'^ajax/whiteboard_video_settings$', WhiteboardVideoSetingsAjaxView.as_view(), name='ajax_whiteboard_video_settings_update'),
    url(r'^ajax/process_payment/', ProcessPaymentAjaxView.as_view(), name='ajax_process_payment_view'),

    url(r'^ajax/profile-section$', ProfileSectionAjaxView.as_view(), name='ajax_profile_section'),
    url(r'^ajax/fetch-job-update$', FetchJobUpdate.as_view(), name='ajax_fetch_job_update'),
    url(r'^ajax/fetch-profile-section$', FetchProfileSectionAjaxView.as_view(), name='ajax_fetch_profile_section'),
    url(r'^ajax/language-update/$', ProfileLanguageUpdate.as_view(), name='ajax_profile_language_update'),
    url(r'^ajax/language-delete/$', ProfileLanguageDelete.as_view(), name='ajax_profile_language_delete'),
    url(r'^ajax/fetch_buddies/$', FetchBuddyListView.as_view(), name='ajax_fetch_buddies'),
    url(r'^ajax/fetch_chat_tutors/$', FetchTutorListView.as_view(), name='ajax_fetch_chat_tutors'),
    url(r'^ajax/profile-picture/$', ProfilePictureAjaxView.as_view(), name='ajax_profile_picture_view'),



    ###Sticky Notes
    url(r'^ajax/stickynote$', StickyNoteAjaxView.as_view(), name='ajax_sticky_note'),

    ###Verification
    url(r'^ajax/verification$', VerificationAjaxView.as_view(), name='ajax_verification_view'),
    url(r'^ajax/cert_verification/$', CertVerificationAjaxView.as_view(), name='ajax_cert_verification_view'),
    url(r'^ajax/subject-rates/$', SubjectRateAjaxView.as_view(), name='ajax_subject_rate_view'),

    #url(r'^profile', ProfileView.as_view(), name='user_profile'),
    url(r'^profile/(?P<pk>[0-9]+)/', ProfileView.as_view(), name='user_profile'),
    url(r'^secure-profile/', SecureProfileView.as_view(), name='secure_user_profile'),
    url(r'^profile/about/(?P<pk>[0-9]+)/$', ProfileAboutView.as_view(), name='user_profile_about'),
    url(r'^profile/job_invitation/(?P<pk>[0-9]+)/$', JobInvitationView.as_view(), name='user_profile_job_invitation'),
    url(r'^profile/job_recommendation/(?P<pk>[0-9]+)/$', JobRecommendationView.as_view(), name='user_profile_job_recommendation'),
    url(r'^job/create/$', JobCreateView.as_view(), name='job_post_create'),
    url(r'^ajax/job/post-preview/$', JobPostPreviewAjaxView.as_view(), name='job_post_preview'),
    url(r'^ajax/job/repost/$', JobRepostPreviewAjaxView.as_view(), name='job_repost_preview'),
    url(r'^ajax/question/post-preview/$', QuestionPostPreviewAjaxView.as_view(), name='question_post_preview'),
    url(r'^job/feed/$', JobFeedView.as_view(), name='job_feed'),
    url(r'^job/details/(?P<pk>[0-9]+)/$', LessonPostDetails.as_view(), name='job_post_details'),
    url(r'^job/(?P<job_id>[0-9]+)/offer/(?P<user_id>[0-9]+)/details/$', JobOfferDetailsView.as_view(), name='job_offer_details'),
    url(r'^job/(?P<pk>[0-9]+)/edit/$', JobEditView.as_view(), name='job_post_edit'),
    url(r'^job/(?P<pk>[0-9]+)/delete/$', JobDeleteView.as_view(), name='job_post_delete'),
    url(r'^job-invitation/action/$', LessonPostAction.as_view(), name='job_post_action'),
    url(r'^ajax/job_tutor_list/$', JobTutorListAjaxView.as_view(), name='ajax_job_tutor_list'),
    url(r'^question/create/$', QuestionCreateView.as_view(), name='question_create_view'),
    url(r'^question/feed/$', QuestionFeedView.as_view(), name='question_feed_view'),
    url(r'^question/details/(?P<pk>[0-9]+)/$', QuestionDetailsView.as_view(), name='question_details_view'),
    url(r'^question/(?P<pk>[0-9]+)/edit/$', QuestionEditView.as_view(), name='question_edit_view'),
    url(r'^question/(?P<pk>[0-9]+)/delete/$', QuestionDeleteView.as_view(), name='question_delete_view'),
    url(r'^qanswer/comment/post/(?P<pk>[0-9]+)/$', QuestionAnswerCommentPostView.as_view(), name='qcomment_post_details'),
    url(r'^qanswer/comment/edit/(?P<pk>[0-9]+)/$', QuestionAnswerCommentEditView.as_view(), name='qcomment_edit_details'),
    url(r'^qanswer/comment/delete/(?P<pk>[0-9]+)/(?P<question_pk>[0-9]+)/$', QuestionAnswerCommentDeleteView.as_view(), name='qcomment_delete_details'),
    url(r'^qanswer/post/(?P<pk>[0-9]+)/$', QuestionAnswerPostView.as_view(), name='qanswer_ans_post_details'),
    url(r'^qanswer/edit/(?P<pk>[0-9]+)/$', QuestionAnswerEditView.as_view(), name='qanswer_ans_edit'),
    url(r'^qanswer/delete/(?P<pk>[0-9]+)/(?P<question_pk>[0-9]+)/$', QuestionAnswerDeleteView.as_view(), name='qanswer_ans_delete'),
    url(r'^question/post/(?P<pk>[0-9]+)/$', QuestionPostDetails.as_view(), name='question_post_details'),
    url(r'^ajax/qlike/$', QLikeAjaxView.as_view(), name='ajax_qlike'),
    url(r'^ajax/profile/about/(?P<pk>[0-9]+)$', UpdateProfileAboutAjax.as_view(), name='ajax_user_profile_about'),
    url(r'^profile/my-video-intro/(?P<pk>[0-9]+)/$', ProfileVideoIntroView.as_view(), name='user_profile_video_intro'),
    url(r'^ajax/login$', LoginAjaxView.as_view(), name='ajax_login'),
    url(r'^recommended-jobs$', JobRecommendationListView.as_view(), name='job_recommendation'),
    url(r'^ajax/check_user/$', UserCheckAjaxView.as_view(), name='ajax_check_user'),
    url(r'^ajax/add_cc/$', CreditCardAjaxView.as_view(), name='ajax_cc_add'),
    url(r'^ajax/remove_cc/$', CreditCardRemoveAjaxView.as_view(), name='ajax_cc_remove'),
    url(r'^ajax/update-email-notification/$', UserUpdateNotificationView.as_view(), name='ajax_update_email_notification'),

    url(r'^ajax/signup$', SignUpAjaxView.as_view(), name='ajax_signup'),
    url(r'^ajax/video_url/', VideoURLViewAjax.as_view(), name='ajax_load_video_url'),
    url(r'^ajax/myaccount$', MyAccountAjaxView.as_view(), name='ajax_myaccount'),
    url(r'^ajax/fetch-user-list', UserListAjaxView.as_view(), name='ajax_fetch_user_list'),
    url(r'^invite_users', InviteUsersLessonView.as_view(), name='invite_users_view'),
    url(r'^ajax/invite_user/', InviteUsersLessonAjaxView.as_view(), name='invite_users_ajax_view'),
    url(r'^ajax/major', MajorAjaxView.as_view(), name='ajax_add_major_content'),
    url(r'^ajax/teaching_experience', TeachingExperienceAjaxView.as_view(), name='ajax_teaching_experience'),
    url(r'^ajax/extra_curricular_interest', ExtracurricularInterestAjaxView.as_view(), name='ajax_extra_curricular_interest'),
    url(r'^ajax/top_subjects', TopSubjectsAjaxView.as_view(), name='ajax_top_subjects'),
    url(r'^ajax/profile_rates', ProfileRateAjaxView.as_view(), name='ajax_profile_rates'),
    url(r'^ajax/school_attended', SchoolAttendedAjaxView.as_view(), name='ajax_school_attended'),
    url(r'^ajax/update_tutor_profile_info', UpdateProfileInfoAjax.as_view(), name='ajax_update_profile_info'),
    url(r'^ajax/update_student_profile_summary', UpdateStudentProfileSummary.as_view(), name='ajax_update_student_profile_summary'),
    url(r'^ajax/update_student_private_info', UpdateStudentPrivateInfo.as_view(), name='ajax_update_student_private_info'),
    url(r'^ajax/suggestions/cert', SuggestionsCertificationsAjaxView.as_view(), name='ajax_cert_suggestions_view'),
    url(r'^ajax/suggestions/level', SuggestionsLevelAjaxView.as_view(), name='ajax_level_suggestions_view'),
    url(r'^ajax/suggestions/school', SuggestionsSchoolsAjaxView.as_view(), name='ajax_school_suggestions_view'),
    url(r'^ajax/suggestions/location', SuggestionsLocationAjaxView.as_view(), name='ajax_location_suggestions_view'),
    url(r'^ajax/suggestions/subject', SuggestionsSubjectsAjaxView.as_view(), name='ajax_subject_suggestions_view'),
    url(r'^ajax/suggestions/category', SuggestionsCategoryAjaxView.as_view(), name='ajax_category_suggestions_view'),
    url(r'^ajax/payment_method$', PaymentMethodAjaxView.as_view(), name='ajax_payment_method'),
    url(r'^ajax/initsession$',VideoSessionTokens.as_view(), name='ajax_video_session_init'),
    url(r'^ajax/startsession$',VideoSessionStart.as_view(), name='ajax_video_session_start'),
    url(r'^ajax/drawing_board$', DrawingBoardAjaxView.as_view(), name='ajax_drawing_board'),
    url(r'^ajax/drawing_data$', DrawingBoardDataAjaxView.as_view(), name='ajax_drawing_board_data'),
    url(r'^ajax/text_editor$', TextEditorAjaxView.as_view(), name='ajax_text_editor'),
    url(r'^ajax/code_editor$', CodeEditorAjaxView.as_view(), name='ajax_code_editor'),
    url(r'^ajax/search_user$', SearchUserByKeyword.as_view(), name='ajax_user_search'),
    url(r'^ajax/search_major$', SearchUserByKeyword.as_view(), name='ajax_major_search'),
    url(r'^ajax/student_private_info$', StudentPrivateInfo.as_view(), name='ajax_student_private_info'),
    url(r'^ajax/student_education$', UpdateStudentEducation.as_view(), name='ajax_student_education'),
    url(r'^ajax/student_cover_picture$', StudentCoverPictureAjaxView.as_view(), name='ajax_student_cover_picture'),
    url(r'^ajax/student_about_section$', StudentAboutSectionAjaxView.as_view(), name='ajax_student_about_section$'),
    url(r'^reset_password_confirm/$', PasswordResetConfirmView.as_view(), name='reset_password_confirm'),
    url(r'^reset_password/$', ResetPasswordRequestView.as_view(), name="reset_password"),
    url(r'^email-recovery-request/$', EmailRecoveryRequestView.as_view(), name="email_recovery_request_view"),
    url(r'^texteditor/', include('pyetherpad.urls')),
    url(r'^change_profile_picture/(?P<image_id>\d+)/$', ChangeProfilePictureView.as_view(), name="crop_pp"),
    url(r'^change_profile_picture/', ChangeProfilePictureView.as_view(), name="change_pp"),
    url(r'^change_major_subject/', ProtectedFormView.as_view(form_class=SubjectMajorUpdateForm), name="change_major"),
    url(r'^change_about_me/', ProtectedFormView.as_view(form_class=AboutMeUpdateForm), name="change_about_me"),
    url(r'^add_education/', EducationAddView.as_view(), name="add_education"),
    url(r'^update_education/(?P<pk>[0-9]+)/', EducationUpdateView.as_view(), name='update_education'),
    url(r'^delete_education/(?P<pk>[0-9]+)/', EducationDeleteView.as_view(), name='delete_education'),
    url(r'^update_profile/(?P<params>\w+)/', UpdateProfileView.as_view(), name='profile_update'),
    url(r'^lessons/$', LessonsView.as_view(), name='lessons_view'),
    url(r'^written-lesson/$', WrittenLessonRequestView.as_view(), name='written_lesson_request_view'),
    url(r'^message/$', MessageView.as_view(), name='message_view'),
    url(r'^message/(?P<user_id>[0-9]+)/$', MessageDetailView.as_view(), name='message_detail_view'),
    url(r'^message/(?P<conversation_id>[^/]+)/$', MessageConversationDetailView.as_view(), name='message_conversation_detail_view'),
    url(r'^reviews/$', LessonReviewListView.as_view(), name='lesson_review_list'),
    url(r'^review/details/(?P<pk>[0-9]+)/$', LessonReviewDetailsView.as_view(), name='lesson_details_view'),
    url(r'^progress_report/new/$', StudentProgressReportView.as_view(), name='student_progress_report'),
    url(r'^progress_report/details/(?P<pk>[0-9]+)/$', ProgressReportDetailsView.as_view(), name='student_progress_report_details'),
    url(r'^progress_reports/$', ProgressReportListView.as_view(), name='student_progress_report_list'),
    url(r'^currency_rates/$', CurrencyRateListView.as_view(), name='currency_rate_list'),
    url(r'^currency_rate_details/(?P<pk>[0-9]+)/$', CurrencyRateDetailsView.as_view(), name='currency_rate_details'),
    url(r'^job-applications/$', JobApplicationListView.as_view(), name='job_application_list'),
    url(r'^job/(?P<job_id>[0-9]+)/application/(?P<pk>[0-9]+)/details/$', JobApplicationDetailsView.as_view(), name='job_application_details_view'),
    url(r'^ajax/job-application/withdraw/$', JobApplicationWithdrawView.as_view(), name='job_application_withdraw_view'),
    url(r'^job/(?P<job_id>[0-9]+)/application/(?P<pk>[0-9]+)/reply/$', JobApplicationReplyView.as_view(), name='job_application_reply_view'),
    url(r'^job/(?P<job_id>[0-9]+)/application/(?P<pk>[0-9]+)/agreement-action/$', JobApplicationAgreementActionView.as_view(), name='job_application_agreement_action_view'),
    url(r'^ajax/apply_job/$', ApplyJobAjaxView.as_view(), name='job_apply_ajax_view'),
    url(r'^ajax/reject_job/$', RejectJobAjaxView.as_view(), name='job_reject_ajax_view'),
    url(r'^ajax/reject_job_recommendation/$', RejectJobRecommendationAjaxView.as_view(), name='job_recommendation_reject_ajax_view'),
    url(r'^ajax/reject_job_application/$', RejectJobApplicationAjaxView.as_view(), name='job_application_reject_ajax_view'),
    url(r'^ajax/reject_job_offer/$', RejectJobOfferAjaxView.as_view(), name='job_offer_reject_ajax_view'),
    url(r'^ajax/update_status_negotiating/$', NegotiateJobAjaxView.as_view(), name='job_negotiate_ajax_view'),
    url(r'^ajax/update_job_status_shortlisted/$', ShortlistJobAjaxView.as_view(), name='job_shortlist_ajax_view'),
    url(r'^ajax/update-job-application/$', JobApplicationUpdateAjaxView.as_view(), name='ajax_job_application_view'),
    url(r'^ajax/applicant_hire/$', OpenHireDialogAjaxView.as_view(), name='open_applicant_hire_dialog'),
    url(r'^ajax/invite_tutor/$', InviteTutorAjaxView.as_view(), name='ajax_invite_tutor'),
    url(r'^ajax/get/email/$', GetEmailTemplateAjax.as_view(), name='ajax_get_email_template'),
                       
                       
    ####Utility urls here
    url(r'^rate-conversion$', RateConversionView.as_view(), name='rate_conversion'),


    ##Marketplace urls start here
    url(r'^marketplace/$', MarketplaceView.as_view(), name='marketplace'),
    url(r'^marketplace/(?P<pk>[0-9]+)/details/$', MarketplaceDetailsView.as_view(), name='marketplace_details'),
    url(r'^ajax/marketplace/jobs/$', AjaxMarketplaceJobsView.as_view(), name='ajax_marketplace_jobs'),
    url(r'^ajax/recommendation/tutors/', AjaxRecommendedTutorsView.as_view(), name='ajax_recommended_tutors_view'),
    ###Marketplace url end here

    url(r'^tutor-search/', TutorSearchView.as_view(), name='tutor_search'),
    url(r'^ajax/search-tutor/', SearchTutorListAjaxView.as_view(), name='ajax_search_tutor'),
    url(r'^ajax/tutor-search/', TutorSearchAjaxView.as_view(), name='ajax_tutor_search'),
    url(r'^ajax/fetch-subjects/$', FetchSubjectsLevelAjaxView.as_view(), name='ajax_subjects_search_level'),
    url(r'^ajax/jobs-student-search/', JobsStudentSearchAjaxView.as_view(), name='ajax_jobs_student_search'),
    url(r'^ajax/tutor-list/', TutorListAjaxView.as_view(), name='ajax_tutor_list'),
    url(r'^ajax/tutor-chat-submit/$', TutorMessageAjaxView.as_view(), name='ajax_tutor_chat'),
    url(r'^ajax/message/', MessageAjaxView.as_view(), name='ajax_message'),
    url(r'^ajax/load-message/', LoadMessagesDetailsAjaxView.as_view(), name='ajax_load_message'),
    url(r'^ajax/load-conversation/(?P<conversation_id>[^/]+)/$', LoadConversationDetailsAjaxView.as_view(), name='ajax_load_conversation'),
    url(r'^ajax/lesson-request/', LessonRequestAjaxView.as_view(), name='ajax_lesson_request'),
    url(r'^ajax/lesson_status_update/', LessonStatusUpdateAjaxView.as_view(), name='ajax_lesson_status_update'),
    url(r'^ajax/live-lesson-request-mutate/', LiveLessonRequestMutate.as_view(), name='ajax_live_lesson_request_mutate'),
    url(r'^ajax/live-lesson-request/', LiveLessonAjaxView.as_view(), name='ajax_live_lesson_request'),
    url(r'^ajax/written-lesson-subjects/', WrittenLessonSubjectAjaxView.as_view(), name='ajax_written_lesson_subjects'),
    url(r'^ajax/lesson-reschedule/', LessonRescheduleAjaxView.as_view(), name='ajax_reschedule_view'),
    url(r'^ajax/fetch_chat_messages/$', ChatboxMessageAjaxView.as_view(), name='ajax_fetch_chat_messages'),
    url(r'^ajax/fetch_wb_messages/(?P<whiteboard_id>[0-9]+)/$', FetchWbMessages.as_view(), name='ajax_fetch_wb_messages'),
    url(r'^ajax/send-message/', SendChatMessageAjaxView.as_view(), name='ajax_send_message'),
    url(r'^ajax/send-chat-message/', SendChatMessageAjaxView.as_view(), name='ajax_send_chat_message'),
    url(r'^ajax/send-batch-message/', SendBatchMessagesAjaxView.as_view(), name='ajax_send_batch_message'),
    url(r'^ajax/mark_message_mutate/', MarkMessageMutateAjaxView.as_view(), name='ajax_mark_message_mutate_view'),
    url(r'^ajax/written-lesson/comment/create/', WrittenLessonCommentView.as_view(), name='ajax_written_lesson_comment_create'),
    url(r'^ajax/video_introduction/', GuideVideoIntro.as_view(), name='ajax_two_min_video_introduction'),
    url(r'^ajax/add_buddy_to_conversation/', AddBuddyToMessageConversationAjaxView.as_view(), name='add_buddy_to_message_conversation_ajax_view'),
    url(r'^ajax/leave_conversation/', LeaveMessageConversationAjaxView.as_view(), name='leave_message_conversation_ajax_view'),
    url(r'^ajax/delete_message/', DeleteMessageAjaxView.as_view(), name='delete_message_ajax_view'),
    url(r'^ajax/chat_settings/', ChatSettingsAjaxView.as_view(), name='chat_settings_ajax_view'),
    url(r'^ajax/rename_conversation/', ConversationRenameAjaxView.as_view(), name='conversation_rename_ajax_view'),
    url(r'^ajax/manage_chat_blocklist/', ManageChatBlockListAjaxView.as_view(), name='manage_chat_blocklist_ajax_view'),
    url(r'^ajax/update_wb_chatbox_status/', UpdateWbChatboxStatus.as_view(), name='ajax_update_wb_chabox_status'),
    url(r'^ajax/disable_wb_video_sharing/', DisableWbVideoSharingAjaxView.as_view(), name='ajax_disable_wb_video_sharing'),


    #url(r'^change_profile_picture/(?P<image_id>\d+)/$', login_required(ChangeProfilePictureView.as_view(), name="crop_pp"),
    #url(r'^change_profile_picture/', login_required(ChangeProfilePictureView.as_view()), name="change_pp"),

    url(r'^calendar-events/$', CalendarView.as_view(), name='calendar_events'),
    url(r'^calendar-events/json/$', CalendarJsonListView.as_view(), name='calendar_json'),

    url(r'^calendar/$', FullCalendarView.as_view(), name='full_calendar'),
    url(r'^wishlist/$', WishlistView.as_view(), name='wish_list'),
    url(r'^faq/$', FAQView.as_view(), name='faq'),
    url(r'^privacy-policy/$', PrivacyPolicyView.as_view(), name='privacy_policy'),
    url(r'^contact-us/$', ContactUsView.as_view(), name='contact_us_view'),
    #(r'^calendar/', include('django_bootstrap_calendar.urls')),
    (r'^notification/', include('notification.urls')),
    (r'^django-rq/', include('django_rq.urls')),
    (r'^admin/rq/', include('django_rq_dashboard.urls')),


)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
js_info_dict = {
    'packages': ('django.conf',),
}

if settings.DEBUG:
    urlpatterns += patterns('django.views.static',
                            (r'media/(?P<path>.*)', 'serve', {'document_root': settings.MEDIA_ROOT}),
                            )


# handler400 = 'ore.error_handler_views.champ_error_handler400'
# handler403 = 'ore.error_handler_views.champ_error_handler403'
# handler404 = 'core.error_handler_views.champ_error_handler404'
# handler500 = 'ore.error_handler_views.champ_error_handler500'
