from core.library.subject_util import SubjectUtil
from core.models3 import SubjectLevel

__author__ = 'Sohel'

class CountryLevelMapper(object):
    def write_mapping_to_db(self):
        all_levels = SubjectUtil.get_all_levels()
        level_objects = []
        for country_name, levels in all_levels.items():
            for level_name in levels:
                level_object = SubjectLevel.objects.filter(name=level_name)
                if not level_object.exists():
                    level_object = SubjectLevel()
                    level_object.name = level_name
                    level_objects += [ level_object ]
