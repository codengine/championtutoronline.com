__author__ = 'codengine'
from django.http import HttpResponse
import json

class JSONResponseMixin(object):
    def render_to_json(self,data):
        return HttpResponse(json.dumps(data))

class CommonMixin(object):
    context = {}