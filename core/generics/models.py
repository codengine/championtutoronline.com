import datetime

from django.conf import settings

from core.library.Clock import Clock
from core.library.timezone_info import TimezoneUtil

__author__ = 'codengine'
from django.db import models

class GenericModel(models.Model):
    # create_date = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    # modified_date = models.DateTimeField(auto_now=True,null=True,blank=True)

    date_created = models.BigIntegerField(null=True,blank=True)
    date_modified = models.BigIntegerField(null=True,blank=True)

    @classmethod
    def get_datetime_fields(cls):
        return [ 'date_created', 'date_modified' ]

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.pk:
            self.date_created = Clock.utc_timestamp()
        self.date_modified = Clock.utc_timestamp()
        super(GenericModel,self).save(force_insert, force_update, using,update_fields)

    def convert_date_created(self):
        return datetime.datetime.fromtimestamp(self.date_created)

    def convert_date_modified(self):
        return datetime.datetime.fromtimestamp(self.date_modified)

    @property
    def has_date_modified(self):
        return self.date_modified > self.date_created

    @classmethod
    def table_columns(cls):
        return [ "id", "date_created" ]

    @classmethod
    def filter_search(cls, request, queryset, **kwargs):
        for key, value in request.GET.items():
            if key == "id":
                try:
                    id_ = int(value)
                    queryset = queryset.filter(pk=id_)
                except:
                    queryset = cls.objects.none()

            elif key == "date_created":
                try:
                    date1 = value.split('-')[0]
                    date2 = value.split('-')[1]
                    ts1 = Clock.convert_to_utc(date1)
                    ts2 = Clock.convert_to_utc(date2)
                    queryset = queryset.filter(date_created__gte=ts1,date_created__lte=ts2)
                except:
                    queryset = cls.objects.none()

        return queryset

    @classmethod
    def get_searchable_columns(cls):
        return [ ("id",False), ("date_created",True) ]

    @property
    def get_object_inline_button(self):
        return []

    @classmethod
    def search_field_map(cls):
        return {}

    @property
    def render_details_template(self):
        return None

    @property
    def details_config(self):
        d = {}
        return d

    @classmethod
    def filter_search(cls, request, queryset, **kwargs):
        return queryset

    @property
    def tabs_config(self):
        return []

    class Meta:
        abstract=True
