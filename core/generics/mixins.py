from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from crequest.middleware import CrequestMiddleware
from engine.permission_manager import PermissionManager

__author__ = 'codengine'


class CommonMixin(object):
    def __init__(self):
        self.context = {}

class LoginRequiredMixin(object):

    @classmethod
    def as_view(cls, **kwargs):
        view = super(LoginRequiredMixin, cls).as_view(**kwargs)
        return login_required(view,login_url="/login")

class AdminLoginRequired(object):

    def dispatch(self,request, *args, **kwargs):
        dispatched = super(AdminLoginRequired, self).dispatch(request,*args,**kwargs)
        if not request.user.is_staff:
            return HttpResponseRedirect("/admin/login")
        return dispatched

    @classmethod
    def as_view(cls, **kwargs):
        view = super(AdminLoginRequired, cls).as_view(**kwargs)
        return login_required(view,login_url="/admin/login")

class AjaxRequestRequired(object):
    def dispatch(self,request, *args, **kwargs):
        if not request.is_ajax():
            response = {
                "status": "FAILURE",
                "message": "Ajax request required",
                "data": ""
            }
            return self.render_to_json(response)
        return super(AjaxRequestRequired, self).dispatch(request,*args,**kwargs)


class PermissionRequiredMixin(object):
    def dispatch(self,request, *args, **kwargs):
        if not PermissionManager.has_view_permission(request):
            return render(request,"unauthorized_access.html",{})
        return super(PermissionRequiredMixin, self).dispatch(request,*args,**kwargs)

# class NotificationMixin(object):
#     context = {}
#     def __init__(self):
#         current_request = CrequestMiddleware.get_request()
#         self.context = {
#             "notifications": NotificationManager.get_all_notification(target_user=current_request.user)
#         }

class ThumbnailModelMixin(object):
    def create_thumbnail(self):
        # original code for this method came from
        # http://snipt.net/danfreak/generate-thumbnails-in-django-with-pil/

        # If there is no image associated with this.
        # do not create thumbnail
        if not self.attachment:
            return

        if self.attachment.name.endswith("jpg") or self.attachment.name.endswith("jpeg") or self.attachment.name.endswith("png"):
            from PIL import Image
            from cStringIO import StringIO
            from django.core.files.uploadedfile import SimpleUploadedFile
            import os

            # Set our max thumbnail size in a tuple (max width, max height)
            THUMBNAIL_SIZE = (200,200)

            if self.attachment.name.endswith("jpg") or self.attachment.name.endswith("jpeg"):
                DJANGO_TYPE = "image/jpeg"
            elif self.attachment.name.endswith("png"):
                DJANGO_TYPE = "image/png"

            if DJANGO_TYPE == 'image/jpeg':
                PIL_TYPE = 'jpeg'
                FILE_EXTENSION = 'jpg'
            elif DJANGO_TYPE == 'image/png':
                PIL_TYPE = 'png'
                FILE_EXTENSION = 'png'

            # Open original photo which we want to thumbnail using PIL's Image
            image = Image.open(StringIO(self.attachment.read()))

            # Convert to RGB if necessary
            # Thanks to Limodou on DjangoSnippets.org
            # http://www.djangosnippets.org/snippets/20/
            #
            # I commented this part since it messes up my png files
            #
            #if image.mode not in ('L', 'RGB'):
            #    image = image.convert('RGB')

            # We use our PIL Image object to create the thumbnail, which already
            # has a thumbnail() convenience method that contrains proportions.
            # Additionally, we use Image.ANTIALIAS to make the image look better.
            # Without antialiasing the image pattern artifacts may result.
            image.thumbnail(THUMBNAIL_SIZE, Image.ANTIALIAS)

            # Save the thumbnail
            temp_handle = StringIO()
            image.save(temp_handle, PIL_TYPE)
            temp_handle.seek(0)

            # Save image to a SimpleUploadedFile which can be saved into
            # ImageField
            suf = SimpleUploadedFile(os.path.split(self.attachment.name)[-1],
                 temp_handle.read(), content_type=DJANGO_TYPE)
            # Save SimpleUploadedFile into image field
            self.thumbnail = suf

