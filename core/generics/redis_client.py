from core.library.Singleton import Singleton

__author__ = 'Sohel'

import redis
from django.conf import settings

REDIS_DATABASE = 0

@Singleton
class RedisClient(object):

    def __init__(self):
        print("Initializing Redis Client...")
        connection = {
            "host": settings.REDIS_SERVERURL,
            "port": settings.REDIS_SERVER_PORT,
            "db": REDIS_DATABASE
        }
        self.redis = redis.StrictRedis(**connection)
        self.pubsub = self.redis.pubsub()

    def publish_channel(self,channel_name,data):
        print("Publishing data...")
        if self.redis:
            self.redis.publish(channel_name,data)
            print("Done!")

    def __str__(self):
        return "Redis client. Host: %s, Port: %s and database: %s" % (settings.REDIS_SERVERURL, str(settings.REDIS_SERVER_PORT), str(REDIS_DATABASE))

