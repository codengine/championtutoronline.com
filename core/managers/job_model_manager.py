from django.db import models

__author__ = 'Sohel'

class JobModelManager(models.Manager):
    def get_queryset(self):
        return super(JobModelManager, self).get_queryset().exclude(status=4)
