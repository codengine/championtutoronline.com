__author__ = 'Codengine'

from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.core.urlresolvers import reverse

def user_login_required(funct):
    def wrapper(request,*args,**kwargs):
        if not request.session.get("is_login"):
            return HttpResponseRedirect(reverse("user_login"))
        return funct(request,*args,**kwargs)
    return wrapper

def champ_login_required(f):
    def wrapper(request,*args,**kwargs):
        if request.is_ajax():
            if request.user.is_authenticated():
                return f(request,*args,**kwargs)
            else:
                response = {
                    "status": "FAILURE",
                    "message": "User Authentication Required.",
                    "data": []
                }
                return HttpResponse(json.dumps(data))
        else:
            if request.user.is_authenticated():
                return f(request,*args,**kwargs)
            else:
                return HttpResponseRedirect(reverse("user_login"))
    return wrapper


