from django.shortcuts import render_to_response
from django.template import RequestContext


def champ_error_handler400(request):
    response = render_to_response('error_pages/400.html', {},
                                  context_instance=RequestContext(request))
    response.status_code = 400
    return response

def champ_error_handler403(request):
    response = render_to_response('error_pages/403.html', {},
                                  context_instance=RequestContext(request))
    response.status_code = 403
    return response


def champ_error_handler404(request):
    response = render_to_response('error_pages/404.html', {},
                                  context_instance=RequestContext(request))
    response.status_code = 404
    return response


def champ_error_handler500(request):
    response = render_to_response('error_pages/500.html', {},
                                  context_instance=RequestContext(request))
    response.status_code = 500
    return response
