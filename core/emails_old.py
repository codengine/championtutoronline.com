# from core.jobs import send_email_job
from core.models3 import EmailLog
from django.conf import settings
from django.core.mail import EmailMultiAlternatives

__author__ = 'Codengine'

class EmailClient:
    def __init__(self):
        print("Inside Email Sender Class.")

    @classmethod
    def place_to_queue(self,email_object):
        # result = send_email_job.delay(email_object)

        from_email = email_object['from_email']
        if not from_email:
            from_email = settings.EMAIL_HOST_USER
        for receipient in email_object['recipients']:
            email_log = EmailLog()
            email_log.sender = from_email
            email_log.receiver = receipient
            email_log.subject = email_object['subject']
            email_log.html_body = email_object['html_body']
            email_log.text_body = email_object['text_body']
            # email_log.job_id = result.id
            email_log.save()

    @classmethod
    def send_email(cls,email_to,email_sub,html_body,text_body,from_email=None):
        try:
            if not from_email:
                from_email = settings.EMAIL_HOST_USER
            email = EmailMultiAlternatives(subject=email_sub, body=text_body, from_email=from_email,
                                               to=email_to)
            email.attach_alternative(html_body, "text/html")
            return email.send(fail_silently=True)
        except Exception,msg:
            pass


