from core.library.email_client import EmailClient
from django_rq import job
from core.library.job_util import JobUtil


@job
def send_email_job(email_object):
    EmailClient.send_email(email_object["recipients"],email_object["subject"],email_object["html_body"],email_object["text_body"],from_email=None)

@job
def add_job_recommendation_job(job_id):
    JobUtil.add_to_recommendation_list(job_id)

