from django.contrib.auth.models import User
from core.models3 import ChampUser

__author__ = 'Sohel'

class FacebookBackend(object):
    def authenticate(self, email=None, facebook_id=None):
        champ_user_objects = ChampUser.objects.filter(user__email=email,facebook_id=facebook_id)
        if champ_user_objects.exists():
            return champ_user_objects.first().user

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None