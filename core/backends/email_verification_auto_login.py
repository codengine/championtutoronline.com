from django.contrib.auth.models import User
from core.models3 import ChampUser
from django.conf import settings

__author__ = 'Sohel'

class AutoLoginBackend(object):
    def authenticate(self, email=None, server_token=None):
        champ_user_objects = ChampUser.objects.filter(user__email=email)
        if champ_user_objects.exists():
            if server_token and server_token == settings.SERVER_TOKEN:
                return champ_user_objects.first().user

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None