__author__ = 'Sohel'

class EncryptDecryptManager(object):
    def __init__(self, key):

        if len(key) != 16 and len(key) != 24 and len(key) != 32:
            raise Exception("Key must be in 16, 24 or 32 characters long")

        self.key = key

    def generate_key(self, bit): ###128, 192, 256
        import os
        if bit == 128:
            return os.urandom(16)
        elif bit == 192:
            return os.urandom(24)
        elif bit == 256:
            return os.urandom(32)

    def encrypt(self, plaintext):
        import pyaes
        aes = pyaes.AESModeOfOperationCTR(self.key)
        ciphertext = aes.encrypt(plaintext)
        return ciphertext

    def decrypt(self, ciphertext):
        import pyaes
        aes = pyaes.AESModeOfOperationCTR(self.key)
        decrypted = aes.decrypt(ciphertext)
        return decrypted

    @classmethod
    def encode_b64(cls, text):
        import base64
        return base64.b64encode(text)

    @classmethod
    def decode_b64(cls, cipertext):
        import base64
        return base64.b64decode(cipertext)
