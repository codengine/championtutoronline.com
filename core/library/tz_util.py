from datetime import datetime
from django.db.models.loading import get_model
import pytz
from core.library.country_util import CountryUtil

__author__ = 'Sohel'

class TZUtil(object):

    @classmethod
    def get_offset_prefix(cls, offset):
        try:
            offset = int(offset)
            if offset >= 0:
                return "+"
            else:
                return "-"
        except Exception as exp:
            return "+"

    @classmethod
    def get_absolute_offset(cls, offset):
        try:
            offset = int(offset)
            return str(abs(offset))
        except Exception as exp:
            return offset


    @classmethod
    def get_supported_tz(cls):
        TimezoneMapper = get_model("core", "TimezoneMapper")
        tz_objects = TimezoneMapper.objects.filter(country__type="SupportedCountry")
        tz_list = []
        for tzm in tz_objects:
            tz_list += [ (tzm.pk, "( UTC "+ cls.get_offset_prefix(tzm.tz_offset) + " " + cls.get_absolute_offset(tzm.tz_offset) +") "+tzm.tz) ]

        return tz_list


    @classmethod
    def get_timezone_differences(cls):

        TimezoneDifference = get_model('core', 'TimezoneDifference')

        all_tzmd = TimezoneDifference.objects.all()
        if not all_tzmd.exists():
            all_tzmd = []

            tzmd = TimezoneDifference()
            tzmd.min_value = 0
            tzmd.max_value = 3
            tzmd.save()

            all_tzmd += [ tzmd ]

            tzmd = TimezoneDifference()
            tzmd.min_value = 0
            tzmd.max_value = 6
            tzmd.save()

            all_tzmd += [ tzmd ]

            tzmd = TimezoneDifference()
            tzmd.min_value = 0
            tzmd.max_value = 9
            tzmd.save()

            all_tzmd += [ tzmd ]

            tzmd = TimezoneDifference()
            tzmd.min_value = 0
            tzmd.max_value = 12
            tzmd.save()

            all_tzmd += [ tzmd ]

            tzmd = TimezoneDifference()
            tzmd.min_value = 0
            tzmd.max_value = 15
            tzmd.save()

            all_tzmd += [ tzmd ]

        return [ ( "%s" % str(tzmd_object.pk), "%s - %s hours" % ( str(tzmd_object.min_value), str(tzmd_object.max_value))) for tzmd_object in all_tzmd ]


    @classmethod
    def get_tz_for_country(cls,country_name):
        if country_name == "Singapore":
            return ("UTC +8.0", "Asia/Singapore")
        elif country_name == "Malayasia":
            return ("UTC +8.0", "Asia/Kuala_Lumpur")
        else:
            return (None, None)

    @classmethod
    def get_country_from_timezone(cls, all_countries, timezone):
        for country in all_countries:
            try:
                country_code = country.code
                country_timezones = pytz.country_timezones(country_code.lower())
                if timezone in country_timezones:
                    return country
            except Exception as exp:
                pass

    @classmethod
    def update_timezone_country_mapper(cls):
        TimezoneMapper = get_model("core", "TimezoneMapper")
        countries = CountryUtil.get_supported_country_objects()
        all_timezones = pytz.all_timezones
        today = datetime.now()
        for tz in all_timezones:
            pst = pytz.timezone(tz)
            td = pst.utcoffset(today)
            tz_offset = (td.microseconds + (td.seconds + td.days * 24 * 3600) * 10**6) / 10**6
            tz_offset = tz_offset/60/60
            country = cls.get_country_from_timezone(countries, tz)
            if country:
                if not TimezoneMapper.objects.filter(country_id=country.pk, tz=tz, tz_offset=tz_offset):
                    tzm = TimezoneMapper()
                    tzm.country_id = country.pk
                    tzm.tz = tz
                    tzm.tz_offset = tz_offset
                    tzm.save()

    @classmethod
    def get_country_for_timezone(cls, timezone):
        TimezoneMapper = get_model("core", "TimezoneMapper")
        tz_objects = TimezoneMapper.objects.filter(tz=timezone)
        if tz_objects.exists():
            return get_model("core", "Country").objects.get(pk=tz_objects.first().country.pk)