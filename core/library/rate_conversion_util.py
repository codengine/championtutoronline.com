from core.models3 import CurrencyRate

__author__ = 'Sohel'

class RateConversionUtil(object):

    @classmethod
    def convert(cls, base_currency, target_currency, value):
        try:
            if base_currency == target_currency:
                return value

            crates = CurrencyRate.objects.filter(base_currency__code__iexact=base_currency, target_currency__code__iexact=target_currency)
            if crates.exists():
                crate = crates.first()
                converted_value = float(value) * crate.value
                return converted_value

            crates = CurrencyRate.objects.filter(base_currency__code__iexact=target_currency, target_currency__code__iexact=base_currency)
            if crates.exists():
                crate = crates.first()
                converted_value = float(1 / float(crate.value)) * float(value)
                return converted_value

            crates = CurrencyRate.objects.filter(target_currency__code__iexact=base_currency)
            if crates.exists():
                crate = crates.first()
                converted_value = float(1 / float(crate.value)) * float(value)

                crates = CurrencyRate.objects.filter(target_currency__code__iexact=target_currency)
                if crates.exists():
                    crate = crates.first()
                    fcnverted_value =  converted_value * crate.value
                    return fcnverted_value
        except Exception as exp:
            print(exp)
            pass
