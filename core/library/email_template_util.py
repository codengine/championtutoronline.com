from django.core.urlresolvers import reverse
from django.template import loader, Context
from django.template.defaultfilters import join

from core.library.currency_util import CurrencyUtil
from core.library.rate_conversion_util import RateConversionUtil
from core.models3 import Job, JobApplication, JobInvitation, JobOffer, LiveLesson, WrittenLesson, TimezoneMapper
from core.templatetags.questiontagfilters import taglist, replace_last_comma_with_and
from engine.config import servers
from engine.config.API import BASE_CURRENCIES

__author__ = 'Sohel'

logo_url = "%s/static/images/logo.png" % ( servers.APP_SERVER_HOST+":" + str(servers.APP_SERVER_PORT) )
social_icon1 = "%s/static/images/social-icon1.png" % ( servers.APP_SERVER_HOST+":" + str(servers.APP_SERVER_PORT) )
social_icon2 = "%s/static/images/social-icon2.png" % ( servers.APP_SERVER_HOST+":" + str(servers.APP_SERVER_PORT) )
social_icon3 = "%s/static/images/social-icon3.png" % ( servers.APP_SERVER_HOST+":" + str(servers.APP_SERVER_PORT) )
social_icon4 = "%s/static/images/social-icon4.png" % ( servers.APP_SERVER_HOST+":" + str(servers.APP_SERVER_PORT) )

server_url = servers.APP_SERVER_HOST+":"+ str(servers.APP_SERVER_PORT)
if servers.APP_SERVER_PORT == 80:
    server_url = "%s/static/images/logo.png" % (servers.APP_SERVER_HOST)

class EmailTemplateUtil(object):

    @classmethod
    def render_signup_email(cls,name,token):
        template_name = "email_templates/signup_email.html"
        template = loader.get_template(template_name)
        context = {
            "token": token,
            "host": server_url,
            "logo": logo_url,
            "social_icon1": social_icon1,
            "social_icon2": social_icon2,
            "social_icon3": social_icon3,
            "social_icon4": social_icon4,
            "name": name
        }

        cntxt = Context(context)
        rendered = template.render(cntxt)
        return rendered, "Registration Completed Successfully"

    @classmethod
    def render_job_invitation_message_template(cls,receiver_name, subjects):
        template_name = "email_templates/job_invitation_message.html"
        template = loader.get_template(template_name)
        context = {
            "RECEIVER_NAME": receiver_name,
            "subjects": subjects
        }

        cntxt = Context(context)
        rendered = template.render(cntxt)
        return rendered

    @classmethod
    def render_job_offer_message_template(cls,receiver_name):
        template_name = "email_templates/job_offer_message.html"
        template = loader.get_template(template_name)
        context = {
            "RECEIVER_NAME": receiver_name
        }

        cntxt = Context(context)
        rendered = template.render(cntxt)
        return rendered

    @classmethod
    def render_job_invitation_email_template(cls, from_name, to_name, message, job_id):
        template_name = "email_templates/job_invitation_email.html"
        template = loader.get_template(template_name)
        job = Job.objects.get(pk=job_id)
        context = {
            "invitation_from": from_name,
            "invitation_to": to_name,
            "invitation_message": message,
            "job": job,
            "level": job.preference.level.name,
            "subjects": ','.join([ s.name for s in job.preference.subjects.all() ]),
            "logo": logo_url,
            "social_icon1": social_icon1,
            "social_icon2": social_icon2,
            "social_icon3": social_icon3,
            "social_icon4": social_icon4,
            "host": server_url
        }

        cntxt = Context(context)
        rendered = template.render(cntxt)
        return rendered, "Text body"

    @classmethod
    def render_job_applied_email_template(cls, student_name, tutor_name, application_id,messages="", source="marketplace"):
        if source == "marketplace":
            template_name = "email_templates/job_applied_marketplace_email.html"
        else:
            template_name = "email_templates/job_applied_email.html"
        template = loader.get_template(template_name)
        job = Job.objects.get(applications__id=application_id)
        job_application = JobApplication.objects.get(pk=application_id)
        context = {
            "student_name": student_name,
            "tutor_name": tutor_name,
            "job_title": job.title,
            "applied_message": messages,
            "job": job,
            "job_application": job_application,
            "level": job.preference.level.name,
            "subjects": ",".join([ s.name for s in job.preference.subjects.all() ]),
            "logo": logo_url,
            "social_icon1": social_icon1,
            "social_icon2": social_icon2,
            "social_icon3": social_icon3,
            "social_icon4": social_icon4,
            "host": server_url,
            "application_link": reverse("job_application_details_view", kwargs={ "job_id":job.pk,"pk":application_id })
        }

        cntxt = Context(context)
        rendered = template.render(cntxt)
        return rendered, "Text body"

    @classmethod
    def render_job_application_withdrawn_email_template(cls, student_name, tutor_name, job_id, application_id,message=None):
        template_name = "email_templates/job_application_withdraw_email.html"
        template = loader.get_template(template_name)
        job = Job.objects.get(pk=job_id)
        job_application = JobApplication.objects.get(pk=application_id)

        context = {
            "student_name": student_name,
            "tutor_name": tutor_name,
            "job_title": job.title,
            "withdraw_message": message,
            "job": job,
            "job_application": job_application,
            "level": job.preference.level.name,
            "subjects": ",".join([ s.name for s in job.preference.subjects.all() ]),
            "logo": logo_url,
            "social_icon1": social_icon1,
            "social_icon2": social_icon2,
            "social_icon3": social_icon3,
            "social_icon4": social_icon4,
            "host": server_url,
            "application_link": reverse("job_application_details_view", kwargs={ "job_id":job.pk,"pk":application_id })
        }

        cntxt = Context(context)
        rendered = template.render(cntxt)
        return rendered, "Text body"

    @classmethod
    def render_job_application_hired_email_template(cls, student_name, tutor_name, application_id,message=None):
        template_name = "email_templates/job_application_hired.html"
        template = loader.get_template(template_name)
        job = Job.objects.get(applications__id=application_id)
        job_application = JobApplication.objects.get(pk=application_id)

        converted_value, target_currency, conversion_enabled = None, None, False
        if job.lesson:
            champ_tutor = job.lesson.request_user.champuser
            country_name = ''
            tzm = TimezoneMapper.objects.filter(tz=champ_tutor.timezone)
            if tzm.exists():
                tzm = tzm.first()
                country_name = tzm.country.name
            user_currency = CurrencyUtil.get_currency_code_for_country(country_name)
            conversion_enabled = job.preference.currency != user_currency
            base_currency = "USD"
            target_currency = "USD" if not user_currency in BASE_CURRENCIES else user_currency
            converted_value = RateConversionUtil.convert(job.preference.currency, target_currency, job.preference.rate.rate_value)

        context = {
            "student_name": student_name,
            "tutor_name": tutor_name,
            "job_title": job.title,
            "hire_message": message,
            "job": job,
            "job_application": job_application,
            "level": job.preference.level.name,
            "subjects": ",".join([ s.name for s in job.preference.subjects.all() ]),
            "logo": logo_url,
            "social_icon1": social_icon1,
            "social_icon2": social_icon2,
            "social_icon3": social_icon3,
            "social_icon4": social_icon4,
            "host": server_url,
            "application_link": reverse("job_application_details_view", kwargs={ "job_id":job.pk,"pk":application_id }),
            "conversion_enabled": conversion_enabled,
            "converted_value": converted_value,
            "target_currency": target_currency
        }

        cntxt = Context(context)
        rendered = template.render(cntxt)
        return rendered, "Text body"

    @classmethod
    def render_job_invitation_rejected_email_template(cls, student_name, tutor_name, job_invitation_id,reject_message=None):
        template_name = "email_templates/job_invitation_reject_email.html"
        template = loader.get_template(template_name)
        job = JobInvitation.objects.get(pk=job_invitation_id).job

        context = {
            "student_name": student_name,
            "tutor_name": tutor_name,
            "message": reject_message,
            "job": job,
            "level": job.preference.level.name,
            "subjects": ','.join([ s.name for s in job.preference.subjects.all() ]),
            "logo": logo_url,
            "social_icon1": social_icon1,
            "social_icon2": social_icon2,
            "social_icon3": social_icon3,
            "social_icon4": social_icon4,
            "host": server_url,
            "job_link": reverse("job_post_details", kwargs={ "pk":job.pk })
        }

        cntxt = Context(context)
        rendered = template.render(cntxt)
        return rendered, "Text body"

    @classmethod
    def render_job_application_rejected_email_template(cls, student_name, tutor_name, application_id, reject_message=None):
        template_name = "email_templates/job_application_rejected_email.html"
        template = loader.get_template(template_name)
        job = Job.objects.get(applications__id=application_id)
        job_application = JobApplication.objects.get(pk=application_id)

        subjects = ",".join([ s.name for s in job.preference.subjects.all() ])

        context = {
            "student_name": student_name,
            "tutor_name": tutor_name,
            "job": job,
            "job_application": job_application,
            "reject_message": reject_message,
            "level": job.preference.level.name,
            "subjects": subjects,
            "logo": logo_url,
            "social_icon1": social_icon1,
            "social_icon2": social_icon2,
            "social_icon3": social_icon3,
            "social_icon4": social_icon4,
            "host": server_url,
            "application_link": reverse("job_application_details_view", kwargs={ "job_id":job.pk,"pk":application_id })
        }

        cntxt = Context(context)
        rendered = template.render(cntxt)
        return rendered, "Text body"

    @classmethod
    def render_job_offered_email_template(cls, student_name, tutor_id, tutor_name, offer_id):
        template_name = "email_templates/job_offer_email.html"
        template = loader.get_template(template_name)
        job_offer = JobOffer.objects.get(pk=offer_id)
        job = Job.objects.get(applications__joboffer__id=offer_id)

        message = "You have got job offer for the job <b>%s</b>" % job.title

        context = {
            "student_name": student_name,
            "tutor_name": tutor_name,
            "message": message,
            "job": job,
            "job_offer": job_offer,
            "logo": logo_url,
            "social_icon1": social_icon1,
            "social_icon2": social_icon2,
            "social_icon3": social_icon3,
            "social_icon4": social_icon4,
            "host": server_url,
            "offer_link": reverse("job_offer_details", kwargs={ "job_id":job.pk,"user_id":tutor_id })
        }

        cntxt = Context(context)
        rendered = template.render(cntxt)
        return rendered, "Text body"

    @classmethod
    def render_job_offer_accepted_email_template(cls, student_name, tutor_id, tutor_name, offer_id):
        template_name = "email_templates/job_offer_accept_email.html"
        template = loader.get_template(template_name)
        job_offer = JobOffer.objects.get(pk=offer_id)
        job = Job.objects.get(applications__joboffer__id=offer_id)

        message = "%s has accepted the job offer you made <b>%s</b>" % (tutor_name, job.title)

        context = {
            "student_name": student_name,
            "tutor_name": tutor_name,
            "message": message,
            "job": job,
            "job_offer": job_offer,
            "logo": logo_url,
            "social_icon1": social_icon1,
            "social_icon2": social_icon2,
            "social_icon3": social_icon3,
            "social_icon4": social_icon4,
            "host": server_url,
            "offer_link": reverse("job_offer_details", kwargs={ "job_id":job.pk,"user_id":tutor_id })
        }

        cntxt = Context(context)
        rendered = template.render(cntxt)
        return rendered, "Text body"

    @classmethod
    def render_job_offer_rejected_email_template(cls, student_name, tutor_id, tutor_name, offer_id):
        template_name = "email_templates/job_offer_rejected_email.html"
        template = loader.get_template(template_name)
        job_offer = JobOffer.objects.get(pk=offer_id)
        job = Job.objects.get(applications__joboffer__id=offer_id)

        message = "%s has rejected the job offer you made <b>%s</b>" % (tutor_name, job.title)

        context = {
            "student_name": student_name,
            "tutor_name": tutor_name,
            "message": message,
            "job": job,
            "job_offer": job_offer,
            "logo": logo_url,
            "social_icon1": social_icon1,
            "social_icon2": social_icon2,
            "social_icon3": social_icon3,
            "social_icon4": social_icon4,
            "host": server_url,
            "offer_link": reverse("job_offer_details", kwargs={ "job_id":job.pk,"user_id":tutor_id })
        }

        cntxt = Context(context)
        rendered = template.render(cntxt)
        return rendered, "Text body"

    @classmethod
    def render_payment_processed_email(cls,job_id, lesson_id, lesson_type, payer_name, payment_amount, currency):
        template_name = "email_templates/payment_processed.html"
        template = loader.get_template(template_name)
        lesson_object = None
        if lesson_type == 0:
            lesson_object = LiveLesson.objects.get(pk=lesson_id)
        else:
            lesson_object = WrittenLesson.objects.get(pk=lesson_id)
        context = {
            "lesson": lesson_object,
            "payment_amount": payment_amount,
            "payer_name": payer_name,
            "currency": currency,
            "logo": logo_url,
            "social_icon1": social_icon1,
            "social_icon2": social_icon2,
            "social_icon3": social_icon3,
            "social_icon4": social_icon4,
            "host": server_url,
            "job_link": reverse("job_post_details", kwargs={ "pk": job_id })
        }

        cntxt = Context(context)
        rendered = template.render(cntxt)
        return rendered, "Text body"

    @classmethod
    def render_payment_process_failed_email(cls,job_id, lesson_id, lesson_type, payer_name, payment_amount, currency,fail_reason):
        template_name = "email_templates/payment_process_failed_email.html"
        template = loader.get_template(template_name)
        lesson_object = None
        if lesson_type == 0:
            lesson_object = LiveLesson.objects.get(pk=lesson_id)
        else:
            lesson_object = WrittenLesson.objects.get(pk=lesson_id)
        context = {
            "lesson": lesson_object,
            "payment_amount": payment_amount,
            "payer_name": payer_name,
            "currency": currency,
            "fail_reason": fail_reason,
            "logo": logo_url,
            "social_icon1": social_icon1,
            "social_icon2": social_icon2,
            "social_icon3": social_icon3,
            "social_icon4": social_icon4,
            "host": server_url,
            "job_link": reverse("job_post_details", kwargs={ "pk": job_id })
        }

        cntxt = Context(context)
        rendered = template.render(cntxt)
        return rendered, "Text body"

    @classmethod
    def render_lesson_scheduled_email(cls,receiver_name,lesson_id,lesson_type):
        template_name = "email_templates/lesson_scheduled_email.html"
        template = loader.get_template(template_name)
        lesson_object = None
        lesson_url = None
        if lesson_type == 0:
            lesson_object = LiveLesson.objects.get(pk=lesson_id)
            lesson_url = reverse("live_lesson", kwargs={ "pk":lesson_id })
        else:
            lesson_object = WrittenLesson.objects.get(pk=lesson_id)
            lesson_url = reverse("written_lesson_request_view")+"?lesson=%s" % str(lesson_id)
        context = {
            "lesson_title": lesson_object.title,
            "receiver_name": receiver_name,
            "logo": logo_url,
            "social_icon1": social_icon1,
            "social_icon2": social_icon2,
            "social_icon3": social_icon3,
            "social_icon4": social_icon4,
            "host": server_url,
            "lesson_url": lesson_url
        }

        cntxt = Context(context)
        rendered = template.render(cntxt)
        return rendered, "Text body"

    @classmethod
    def render_lesson_reminder_email(cls,receiver_name,lesson_id,lesson_type):
        template_name = "email_templates/lesson_reminder_email.html"
        template = loader.get_template(template_name)
        lesson_object = None
        lesson_url = None
        if lesson_type == 0:
            lesson_object = LiveLesson.objects.get(pk=lesson_id)
            lesson_url = reverse("live_lesson", kwargs={ "pk":lesson_id })
        else:
            lesson_object = WrittenLesson.objects.get(pk=lesson_id)
            lesson_url = reverse("written_lesson_request_view")+"?lesson=%s" % str(lesson_id)
        context = {
            "lesson_title": lesson_object.title,
            "receiver_name": receiver_name,
            "logo": logo_url,
            "social_icon1": social_icon1,
            "social_icon2": social_icon2,
            "social_icon3": social_icon3,
            "social_icon4": social_icon4,
            "lesson": lesson_object,
            "host": server_url,
            "lesson_url": lesson_url
        }

        cntxt = Context(context)
        rendered = template.render(cntxt)
        return rendered, "Text body"

    @classmethod
    def render_internal_server_error_admin_email(cls,exception_details):
        template_name = "email_templates/internal_server_error_admin_email.html"
        template = loader.get_template(template_name)
        context = {
            "stacktrace_details": exception_details,
            "logo": logo_url,
            "social_icon1": social_icon1,
            "social_icon2": social_icon2,
            "social_icon3": social_icon3,
            "social_icon4": social_icon4,
            "host": server_url,
        }

        cntxt = Context(context)
        rendered = template.render(cntxt)
        return rendered, "Text body"

    @classmethod
    def render_digest_recommended_email(cls,receiver_name,job_ids):
        template_name = "email_templates/digest_email_recommended_jobs.html"
        template = loader.get_template(template_name)
        job_objects = Job.objects.filter(pk__in=job_ids)
        context = {
            "receiver_name": receiver_name,
            "job_objects": job_objects,
            "logo": logo_url,
            "social_icon1": social_icon1,
            "social_icon2": social_icon2,
            "social_icon3": social_icon3,
            "social_icon4": social_icon4,
            "host": server_url,
        }

        cntxt = Context(context)
        rendered = template.render(cntxt)
        return rendered, "Text body"

    @classmethod
    def render_password_reset_token_email(cls,requested_at_email,password_reset_token):
        template_name = "email_templates/password_reset_email.html"
        template = loader.get_template(template_name)
        context = {
            "password_reset_token": password_reset_token,
            "requested_at_email": requested_at_email,
            "logo": logo_url,
            "social_icon1": social_icon1,
            "social_icon2": social_icon2,
            "social_icon3": social_icon3,
            "social_icon4": social_icon4,
            "host": server_url,
        }

        cntxt = Context(context)
        rendered = template.render(cntxt)
        return rendered, "Text body"

    @classmethod
    def render_lesson_time_extended_email(cls, lesson_id, lesson_type, receiver_name, lesson_time):
        template_name = "email_templates/lesson_time_extended.html"
        template = loader.get_template(template_name)
        lesson_object = None
        lesson_url = None
        if lesson_type == 0:
            lesson_object = LiveLesson.objects.get(pk=lesson_id)
            lesson_url = reverse("live_lesson", kwargs={ "pk":lesson_id })
        else:
            lesson_object = WrittenLesson.objects.get(pk=lesson_id)
            lesson_url = reverse("written_lesson_request_view")+"?lesson=%s" % str(lesson_id)
        context = {
            "lesson_title": lesson_object.title,
            "receiver_name": receiver_name,
            "lesson_time": lesson_time,
            "logo": logo_url,
            "social_icon1": social_icon1,
            "social_icon2": social_icon2,
            "social_icon3": social_icon3,
            "social_icon4": social_icon4,
            "host": server_url,
            "lesson_url": lesson_url
        }

        cntxt = Context(context)
        rendered = template.render(cntxt)
        return rendered, "Text body"

    @classmethod
    def render_lesson_reschedule_email(cls, lesson_id, lesson_type, receiver_name, lesson_time):
        template_name = "email_templates/lesson_rescheduled.html"
        template = loader.get_template(template_name)
        lesson_object = None
        lesson_url = None
        if lesson_type == 0:
            lesson_object = LiveLesson.objects.get(pk=lesson_id)
            lesson_url = reverse("live_lesson", kwargs={ "pk":lesson_id })
        else:
            lesson_object = WrittenLesson.objects.get(pk=lesson_id)
            lesson_url = reverse("written_lesson_request_view")+"?lesson=%s" % str(lesson_id)
        context = {
            "lesson_title": lesson_object.title,
            "receiver_name": receiver_name,
            "lesson_time": lesson_time,
            "logo": logo_url,
            "social_icon1": social_icon1,
            "social_icon2": social_icon2,
            "social_icon3": social_icon3,
            "social_icon4": social_icon4,
            "host": server_url,
            "lesson_url": lesson_url
        }

        cntxt = Context(context)
        rendered = template.render(cntxt)
        return rendered, "Text body"

    @classmethod
    def render_question_feed_email(cls, recipient, question, comment):
        template_name = "email_templates/question_feed_notification.html"
        template = loader.get_template(template_name)
        context = {
            "recipient": recipient.champuser.fullname,
            "recipient_profile_url": recipient.champuser.profile_url,
            "student": question.created_by.champuser.fullname,
            "student_profile_url": question.created_by.champuser.profile_url,
            "question": question,
            "comment": comment,
            "comment_response_url": "#",  # will add this later when comment display is ready
            "subjects": replace_last_comma_with_and(join(taglist(question.tags.all()), ",", None)),
            "level": question.tags.first().level.name,
            "gender": "her" if question.created_by.champuser.gender == "female" else "him",
            "logo": logo_url,
            "social_icon1": social_icon1,
            "social_icon2": social_icon2,
            "social_icon3": social_icon3,
            "social_icon4": social_icon4,
            "host": server_url,
        }
        cntxt = Context(context)
        rendered = template.render(cntxt)
        return rendered, "Text body"

    @classmethod
    def render_question_thread_email(cls, recipient, question, answer_id, comment_id):
        template_name = "email_templates/question_thread_email.html"
        template = loader.get_template(template_name)
        context = {
            "recipient": recipient.champuser.fullname,
            "recipient_profile_url": recipient.champuser.profile_url,
            "recipient_time_zone": recipient.champuser.timezone,
            "question": question,
            "comment_id": comment_id,
            "answer_id": answer_id,
            "logo": logo_url,
            "social_icon1": social_icon1,
            "social_icon2": social_icon2,
            "social_icon3": social_icon3,
            "social_icon4": social_icon4,
            "host": server_url,
        }
        cntxt = Context(context)
        rendered = template.render(cntxt)
        return rendered, "Text body"


    @classmethod
    def render_contact_email(cls, first_name, email, phone, subject, feedback, recipients):
        template_name = "email_templates/contact_us_email.html"
        template = loader.get_template(template_name)
        context = {
            "first_name": first_name,
            "email": email,
            "subject": subject,
            "feedback": feedback
        }

    @classmethod
    def render_question_thread_email(cls, recipient, question, answer_id, comment_id):
        template_name = "email_templates/question_thread_email.html"
        template = loader.get_template(template_name)
        context = {
            "recipient": recipient.champuser.fullname,
            "recipient_profile_url": recipient.champuser.profile_url,
            "recipient_time_zone": recipient.champuser.timezone,
            "question": question,
            "comment_id": comment_id,
            "answer_id": answer_id,
            "logo": logo_url,
            "social_icon1": social_icon1,
            "social_icon2": social_icon2,
            "social_icon3": social_icon3,
            "social_icon4": social_icon4,
            "host": server_url,
        }
        cntxt = Context(context)
        rendered = template.render(cntxt)
        return rendered

    @classmethod
    def render_contact_email(cls, first_name, email, phone, subject, feedback, recipients):
        template_name = "email_templates/contact_us_email.html"
        template = loader.get_template(template_name)
        context = {
            "first_name": first_name,
            "email": email,
            "subject": subject,
            "feedback": feedback
        }

        cntxt = Context(context)
        rendered = template.render(cntxt)
        return rendered, "Text body"

    @classmethod
    def render_progress_report_email(cls, sender_name, receipient_name, report_date, report_content, **kwargs):
        template_name = "email_templates/progress_report_email.html"
        template = loader.get_template(template_name)
        context = {
            "sender_name": sender_name,
            "receipient_name": receipient_name,
            "report_date": report_date,
            "report_content": report_content
        }

        cntxt = Context(context)
        rendered = template.render(cntxt)
        return rendered, "Text body"
