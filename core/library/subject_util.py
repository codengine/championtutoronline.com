from core.library.country_util import CountryUtil
from core.models3 import SubjectLevel, CountryLevel

__author__ = 'Sohel'

class SubjectUtil(object):

    @classmethod
    def get_all_levels(cls):
        return {
            "Singapore": [
                "Lower Primary", "Upper Primary", "Lower Secondary",
                "GCE N-Level", "GCE O-Level", "GCE A-Level", "International Baccalaureate",
                "Polytechnic", "University (Bachelor)", "University (Masters)", "University (PhD)",
                "Music", "Language", "Computing", "Culinary", "Test Preparation"  # , "Professional Development"
            ],
            "Hong Kong": [
                "Lower Primary", "Upper Primary", "Lower Secondary",
                "GCE N-Level", "GCE O-Level", "GCE A-Level", "International Baccalaureate",
                "Polytechnic", "University (Bachelor)", "University (Masters)", "University (PhD)",
                "Music", "Language", "Computing", "Culinary", "Test Preparation"  # , "Professional Development"
            ],
            "Malaysia": [
                "Lower Primary", "Upper Primary", "Lower Secondary", "Upper Secondary",
                 "Form 6 (STPM)", "International Baccalaureate", "Polytechnic", "University (Bachelor)",
                "University (Masters)", "University (PhD)", "Music", "Language", "Computing",
                "Culinary", "Test Preparation", "Professional Skills", "Personal Development"
            ],
            "Other": [
                "Lower Primary", "Upper Primary", "Lower Secondary",
                "GCE N-Level", "GCE O-Level", "GCE A-Level", "International Baccalaureate",
                "Polytechnic", "University (Bachelor)", "University (Masters)", "University (PhD)",
                "Music", "Language", "Computing", "Culinary", "Test Preparation"  # , "Professional Development"
            ]
        }

    @classmethod
    def get_native_to_new_level_name_mapper(cls, old_level_name):
        level_name_mapping = {
            "GCE N-Level": "Upper Secondary",
            "GCE O-Level": "Upper Secondary",
            "GCE A-Level": "Junior College",
            "University (Bachelor)": "University",
            "University (Masters)": "University",
            "University (PhD)": "University"
        }
        return level_name_mapping.get(old_level_name)

    @classmethod
    def get_new_level_to_native_name_mapper(cls, new_level_name):
        level_name_mapping = {
            "Upper Secondary": [ "GCE N-Level", "GCE O-Level" ],
            "Junior College": [ "GCE A-Level" ],
            "University": [ "University (Bachelor)", "University (Masters)", "University (PhD)" ]
        }
        return level_name_mapping.get(new_level_name)

    @classmethod
    def initialize_subject_country_mapper(cls):
        all_countries = CountryUtil.get_all_countries(exclude_list=CountryLevel.objects.values_list('country_id', flat=True), limit=50)

        for country in all_countries:
            levels = cls.get_levels_for_country(country.name)

            for level_name in levels:
                level_objects = SubjectLevel.objects.filter(name=level_name)
                if not level_objects.exists():
                    level_object = SubjectLevel()
                    level_object.name = level_name
                    level_object.save()
                else:
                    level_object = level_objects.first()

                if not CountryLevel.objects.filter(country_id=country.pk, level_id=level_object.pk).exists():
                    country_level_object = CountryLevel()
                    country_level_object.country_id = country.pk
                    country_level_object.level_id = level_object.pk
                    country_level_object.save()

    @classmethod
    def get_levels_for_country(cls, country_name):
        if country_name != "Singapore" and country_name != "Hong Kong" and country_name != "Malaysia":
            return cls.get_all_levels()["Other"]
        return cls.get_all_levels()[country_name]
