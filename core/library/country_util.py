from django.db.models.loading import get_model
from django.db.models.query_utils import Q

__author__ = 'Sohel'

class CountryUtil(object):
    def __init__(self):
        pass

    @classmethod
    def get_all_countries(cls, exclude_list=[], limit=0):
        all_countries = get_model("core", "LocationEntity").objects.filter(Q(type="Country") | Q(type="SupportedCountry"))
        if exclude_list:
            all_countries = all_countries.exclude(pk__in=exclude_list)

        if limit:
            all_countries = all_countries[:limit]

        return all_countries

    @classmethod
    def get_supported_countries(cls, filter_ids=[], **kwargs):
        supported_country_objects = get_model("core", "SupportedCountry").objects.filter(type="SupportedCountry")
        if filter_ids:
            supported_country_objects = supported_country_objects.filter(pk__in=filter_ids)
        supported_countries = [(country.pk,country.name) for country in supported_country_objects ]
        return supported_countries

    @classmethod
    def get_used_countries(cls, only_ids=True, **kwargs):
        cids = get_model("core", "ChampUser").objects.filter(nationality_id__isnull=False).values("nationality_id").distinct().values_list('nationality_id', flat=True)
        if only_ids:
            return cids
        else:
            return get_model("core", "SupportedCountry").objects.filter(pk__in=cids)

    @classmethod
    def get_supported_country_objects(cls):
        return get_model("core", "SupportedCountry").objects.filter(type="SupportedCountry")
