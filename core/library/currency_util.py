import requests
import json
from core.models3 import CurrencyRate, Currency, AuditLog, ChampUser, TimezoneMapper
from payment.models import SupportedCurrencies

__author__ = 'Sohel'

class CurrencyUtil(object):
    def __init__(self):
        pass

    @classmethod
    def get_supported_currencies(cls):
        return [(obj.code, obj.name) for obj in SupportedCurrencies.objects.all()]

    @classmethod
    def convert_currency_to(cls, from_currency, value=0.0, currency='USD'):
        pass

    @classmethod
    def get_currencies_for_user(cls, user_id):
        try:
            champ_user = ChampUser.objects.get(user_id=user_id)
            country_ids = [ champ_user.nationality.pk ]

            if champ_user.timezone:
                tz_objects = TimezoneMapper.objects.filter(tz=champ_user.timezone)
                if tz_objects.exists():
                    tz_object = tz_objects.first()

                    if not tz_object.country_id in country_ids:
                        country_ids += [ tz_object.country_id ]

            return [(obj.code, obj.name) for obj in Currency.objects.filter(country_id__in=country_ids)]
        except Exception as exp:
            return []

    @classmethod
    def get_currency_list(cls):
        return Currency.objects.filter(country_id__isnull=False)

    @classmethod
    def get_currency_code_for_country(cls, country_name):
        currency_objects = Currency.objects.filter(country__name__iexact=country_name)
        if currency_objects.exists():
            return currency_objects.first().code


    @classmethod
    def fetch_rate_list(cls, base_currency='USD'):
        try:
            print("Fetching currencies...")
            url = "http://api.fixer.io/latest?base=%s" % base_currency
            audit_log = AuditLog()
            audit_log.context = "Currency Rate List"
            audit_log.message = "Fetching rate list from %s" % url
            audit_log.save()
            response = requests.get(url)
            content = response.content
            if response.status_code == 200:
                currency_object = json.loads(content)
                rates = currency_object.get('rates')
                if rates:
                    for c, value in rates.items():
                        crates = CurrencyRate.objects.filter(base_currency__code=base_currency.upper(), target_currency__code=c.upper())
                        if crates.exists():
                            crate = crates.first()
                        else:
                            bcurrencies = Currency.objects.filter(code=base_currency.upper())
                            if bcurrencies.exists():
                                bcurrency = bcurrencies.first()
                            else:
                                bcurrency = Currency()
                                bcurrency.code = base_currency.upper()
                                bcurrency.name = base_currency.upper()
                                bcurrency.save()
                            tcurrencies = Currency.objects.filter(code=c.upper())
                            if tcurrencies.exists():
                                tcurrency = tcurrencies.first()
                            else:
                                tcurrency = Currency()
                                tcurrency.code = c.upper()
                                tcurrency.name = c.upper()
                                tcurrency.save()
                            crate = CurrencyRate()
                            crate.base_currency = bcurrency
                            crate.target_currency = tcurrency
                        crate.value = value
                        crate.save()
                        print(crate.pk)
        except Exception as exp:
            print(exp)

