from django.db.models.query_utils import Q
from core.models3 import ChampUser, LessonRequest, UserMessage, ChattedUser

__author__ = 'Sohel'

class UserUtil(object):

    @classmethod
    def get_chat_users(cls, request):
        chat_users = ChampUser.objects.all()
        if request.user.is_authenticated():
            chatted_user = ChattedUser.objects.filter(user_id=request.user.pk)
            if chatted_user.exists():
                chatted_user = chatted_user.first()
                chat_users = chat_users.filter(user_id__in=chatted_user.chatted_users.values_list('pk', flat=True))
            else:
                chat_users = chat_users.model.objects.none()
        return chat_users

    @classmethod
    def get_chat_users_obsolete(cls, request):
        chat_users = ChampUser.objects.all()
        if request.user.is_authenticated():
            if request.user.champuser.is_tutor: ###The current user is tutor.
                user_ids = []
                lessons = LessonRequest.objects.filter(Q(request_user_id=request.user.pk)|Q(other_users__id__in=[ request.user.pk ]))
                for lesson in lessons:
                    if not lesson.user.pk in user_ids:
                        user_ids += [ lesson.user.pk ]

                p2p_messages = UserMessage.objects.filter(Q(chat_type=0) & (Q(sender_id=request.user.pk) | Q(receiver_id=request.user.pk))).values('sender_id','receiver_id').distinct()

                for umessage in p2p_messages:
                    if umessage['sender_id'] != request.user.pk:
                        if not umessage['sender_id'] in user_ids:
                            user_ids += [ umessage['sender_id'] ]
                    elif umessage['receiver_id'] and not umessage['receiver_id'] in user_ids:
                        user_ids += [ umessage['receiver_id'] ]


            else: ###The current user is student.
                user_ids = []
                lessons = LessonRequest.objects.filter(user_id=request.user.pk).select_related('other_users')
                for lesson in lessons:
                    if not lesson.request_user.pk in user_ids:
                        user_ids += [ lesson.request_user.pk ]
                    for u in lesson.other_users.all():
                        if not u.pk in user_ids:
                            user_ids += [ u.pk ]

                p2p_messages = UserMessage.objects.filter(Q(chat_type=0) & (Q(sender_id=request.user.pk) | Q(receiver_id=request.user.pk))).values('sender_id','receiver_id').distinct()

                for umessage in p2p_messages:
                    if umessage['sender_id'] != request.user.pk:
                        if not umessage['sender_id'] in user_ids:
                            user_ids += [ umessage['sender_id'] ]
                    elif umessage['receiver_id'] and not umessage['receiver_id'] in user_ids:
                        user_ids += [ umessage['receiver_id'] ]


            user_ids = [ int(uid) for uid in user_ids if uid and int(uid) != request.user.pk ]
            user_ids = list(set(user_ids))
            chat_users = chat_users.filter(user_id__in=user_ids)

        return chat_users

    @classmethod
    def get_all_students(cls):
        champ_users = ChampUser.objects.filter(type__name="student").exclude(user__is_staff=True)
        return champ_users

    @classmethod
    def get_all_active_students(cls, tutor_id=None):
        champ_users = cls.get_all_students()
        if tutor_id:
            lesson_users = LessonRequest.objects.filter(Q(request_user_id=tutor_id) | Q(other_users__id__in=[tutor_id])).values_list('user_id', flat=True)
            participated_students = [ int(uid) for uid in lesson_users ]
            participated_students = list(set(participated_students))
            champ_users = champ_users.filter(user_id__in=participated_students)
        champ_users = champ_users.filter(online_status=1)
        return champ_users

    @classmethod
    def get_all_inactive_students(cls,tutor_id=None):
        champ_users = cls.get_all_students()
        if tutor_id:
            lesson_users = LessonRequest.objects.filter(Q(request_user_id=tutor_id) | Q(other_users__id__in=[tutor_id])).values_list('user_id', flat=True)
            participated_students = [ int(uid) for uid in lesson_users ]
            participated_students = list(set(participated_students))
            champ_users = champ_users.filter(user_id__in=participated_students)
        champ_users = champ_users.filter(online_status=0)
        return champ_users

    @classmethod
    def get_all_tutors(cls):
        champ_users = ChampUser.objects.filter(online_status=1,type__name="teacher").exclude(user__is_staff=True)
        return champ_users

    @classmethod
    def get_all_active_tutors(cls, student_id=None):
        champ_users = cls.get_all_tutors()
        if student_id:
            participated_tutors = []
            lessons = LessonRequest.objects.filter(user_id=student_id).select_related('other_users')
            for lesson in lessons:
                participated_tutors += [ lesson.request_user.pk ]
                for user in lesson.other_users.all():
                    participated_tutors += [ user.pk ]
            participated_tutors = list(set(participated_tutors))
            champ_users = champ_users.filter(user_id__in=participated_tutors)
        champ_users = champ_users.filter(online_status=1)
        return champ_users

    @classmethod
    def get_all_inactive_tutors(cls,student_id=None):
        champ_users = cls.get_all_students()
        if student_id:
            participated_tutors = []
            lessons = LessonRequest.objects.filter(user_id=student_id).select_related('other_users')
            for lesson in lessons:
                participated_tutors += [ lesson.request_user.pk ]
                for user in lesson.other_users.all():
                    participated_tutors += [ user.pk ]
            participated_tutors = list(set(participated_tutors))
            champ_users = champ_users.filter(user_id__in=participated_tutors)
        champ_users = champ_users.filter(online_status=0)
        return champ_users
