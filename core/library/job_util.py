from core.enums import JobRecommendationStatus
from core.library.Clock import Clock
from core.library.block_util import BlockListUtil
from core.models3 import Job, ChampUser, UserMajor, JobRecommendation, JobInvitation
from django.db.models import Q

__author__ = 'Sohel'

class JobUtil(object):
    @classmethod
    def recommended_jobs_for_user(cls,user_id,ts=None, exclude_list=[]):
        jobs = Job.recommended_jobs_for_user(user_id, ts=ts)
        if exclude_list:
            jobs = jobs.exclude(pk__in=exclude_list)
        temp_jobs = []
        for job in jobs:
            invited_users = JobInvitation.objects.filter(job_id=job.pk).values_list('user_id', flat=True)
            if not BlockListUtil.check_if_user_blocked(job.posted_by_id, user_id) and not user_id in invited_users:
                temp_jobs += [ job.pk ]
        jobs = Job.objects.filter(pk__in=temp_jobs).order_by('-date_created')
        return jobs

    @classmethod
    def check_if_job_is_recommended_for_user(cls, user_id, job_id):
        jobs = Job.recommended_jobs_for_user(user_id, job_id)
        return jobs.exists()

    @classmethod
    def get_recommended_jobs_digest(cls,user_id, since=24 * 60 * 60, till=Clock.utc_timestamp()):
        twenty_four_hours = since
        now_ts = till
        t4hback = now_ts - twenty_four_hours
        jobs = cls.recommended_jobs_for_user(user_id)
        jobs = jobs.filter(date_created__gte=t4hback,date_created__lte=now_ts)
        jobs = jobs.values_list('pk',flat=True)
        return jobs

    @classmethod
    def get_recommended_users(cls,job_id, exclude_ids=[], limit=10):
        job_object = Job.objects.get(pk=job_id)
        recommended_users = job_object.get_recommended_users(exclude_ids=exclude_ids)
        invited_users = JobInvitation.objects.filter(job_id=job_id).values_list('user_id', flat=True)
        temp_user_ids = []
        for _user in recommended_users:
            if not BlockListUtil.check_if_user_blocked(job_object.posted_by_id, _user.pk) and not _user.pk in invited_users:
                temp_user_ids += [ _user.user.pk ]
        recommended_users = ChampUser.objects.filter(user_id__in=temp_user_ids).order_by('fullname')[:limit]
        return recommended_users

    @classmethod
    def add_to_recommendation_list_obsolete(cls,job_id):
        champ_users = cls.get_recommended_users(job_id)
        JobRecommendation.objects.filter(job_id=job_id,user_id__in=champ_users.values_list('pk',flat=True)).delete()
        for champ_user in champ_users:
            # if not JobRecommendation.objects.filter(job_id=job_id,user_id=champ_user.user.pk).exists():
            if not JobInvitation.objects.filter(job_id=job_id,user_id=champ_user.user.pk):
                job_recommendation = JobRecommendation()
                job_recommendation.job_id = job_id
                job_recommendation.user_id = champ_user.user.pk
                job_recommendation.recommended_by = 0 ###Recommended by the system
                job_recommendation.criteria_matched = 100
                job_recommendation.status = JobRecommendationStatus.active.value
                job_recommendation.save()

#print(JobUtil.get_job_recommendation_users(4))
