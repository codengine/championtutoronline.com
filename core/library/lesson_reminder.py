from datetime import timedelta
from redis.client import Redis
from core.library.email_client import EmailClient
from core.library.email_template_util import EmailTemplateUtil
from core.models3 import LessonRequest, ScheduledJobs
from rq_scheduler.scheduler import Scheduler

__author__ = 'Sohel'

class LessonReminder(object):

    @classmethod
    def send_email(cls, email_object):
        EmailClient.send_email(email_object["recipients"],email_object["subject"],email_object["html_body"],email_object["text_body"],from_email=None)

    @classmethod
    def schedule_reminder(cls, lesson_id, schedule_date):
        job = None
        lesson_objects = LessonRequest.objects.filter(pk=lesson_id)
        if lesson_objects.exists():
            lesson_object = lesson_objects.first()
            scheduler = Scheduler(connection=Redis())
            users = []
            users += [ lesson_object.user ]
            users += [ lesson_object.request_user ]
            for _u in lesson_object.other_users.all():
                users += [ _u ]
            for user in users:
                html, text = EmailTemplateUtil.render_lesson_reminder_email(user.champuser.fullname, lesson_object.pk, lesson_object.type)

                email_object = {
                    "recipients": [ user.email ],
                    'subject': 'Lesson Reminder',
                    'html_body': html,
                    'text_body': text
                }

                job = scheduler.enqueue_at(schedule_date, cls.send_email, email_object)

                scheduled_job = ScheduledJobs()
                scheduled_job.object_class = LessonRequest.__name__
                scheduled_job.object_id = lesson_id
                scheduled_job.job_id = job.id
                scheduled_job.save()

            return job




                
