from core.library.email_scheduler import EmailScheduler
from core.library.email_template_util import EmailTemplateUtil
from core.library.job_util import JobUtil
from core.models3 import ChampUser

__author__ = 'Sohel'

class JobDigestEmailUtil(object):

    @classmethod
    def run_email_digest(cls):
        ###Get all tutor.
        tutors = ChampUser.objects.filter(type__name="teacher")
        for tutor in tutors:
            recommended_jobs =  JobUtil.get_recommended_jobs_digest(tutor.user.pk)
            if recommended_jobs:
                receiver_name = tutor.fullname
                job_ids = recommended_jobs
                html, text = EmailTemplateUtil.render_digest_recommended_email(receiver_name,job_ids)

                email_object = {
                    "recipients": [ tutor.user.email ],
                    'subject': 'Recommended Jobs',
                    'html_body': html,
                    'text_body': text
                }

                EmailScheduler.place_to_queue(email_object)
