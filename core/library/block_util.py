from core.models3 import UserBlockSettings, Conversation

__author__ = 'Sohel'

class BlockListUtil(object):
    @classmethod
    def check_if_user_blocked(cls, user_id1, user_id2):
        blocked1, blocked2 = False, False
        user_blocklist_settings = UserBlockSettings.objects.filter(user_id = user_id1)
        if user_blocklist_settings.exists():
            user_blocklist_settings = user_blocklist_settings.first()
            blocked1 = user_blocklist_settings.blocked_users.filter(user_id=user_id2).exists()
        user_blocklist_settings2 = UserBlockSettings.objects.filter(user_id = user_id2)
        if user_blocklist_settings2.exists():
            user_blocklist_settings2 = user_blocklist_settings2.first()
            blocked2 = user_blocklist_settings2.blocked_users.filter(user_id=user_id1).exists()
        return blocked1 or blocked2

    @classmethod
    def check_if_user_left_this_conversation(cls, conversation_id, user_id):
        conversation_object = Conversation.objects.filter(conversation_id=conversation_id)
        if conversation_object.exists():
            conversation_object = conversation_object.first()
            users = conversation_object.users_left.filter(pk=user_id)
            exists = users.exists()
            return exists
        return False
