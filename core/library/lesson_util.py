import os
import uuid
from datetime import datetime
from django.contrib.auth.models import User
from django.core.files.base import File
from django.core.urlresolvers import reverse
from django.db.models.query_utils import Q
from core.enums import WhiteboardAccess, ApprovalStatus
from core.library.Clock import Clock
from core.library.email_scheduler import EmailScheduler
from core.library.email_template_util import EmailTemplateUtil
from core.models3 import LiveLesson, Whiteboard, WhiteboardUserPermission, TextPadPermission, CodePadPermission, \
    DrawingBoard, DrawingboardLastNid, DrawingboardPermission, WrittenLesson, WrittenLessonAttachment, LessonRequest
from django.db import transaction
from core.permissions import Permission
from django_bootstrap_calendar.models import CalendarEvent
from notification.utils.notification_manager import NotificationManager
from pyetherpad.models import Pad, PadAuthor
from pyetherpad.padutil import PadUtil
from pysharejs.CodePadUtil import CodePadUtil
from django.conf import settings

__author__ = 'Sohel'

class LessonUtil(object):

    @classmethod
    def get_unconfirmed_lessons(cls, user=None):
        now_ts = Clock.utc_timestamp()
        unconfirmed_lessons = LessonRequest.objects.filter((Q(lesson_time__gte=now_ts) & Q(type=0)) | (Q(due_date__gte=now_ts) & Q(type=1)))
        if user:
            if user.champuser.is_tutor:
                unconfirmed_lessons = unconfirmed_lessons.filter(request_user_id = user.pk)
            else:
                unconfirmed_lessons = unconfirmed_lessons.filter(Q(user_id = user.pk)|Q(other_users__id__in=[user.pk]))
        return unconfirmed_lessons.order_by("-id")

    @classmethod
    def get_past_lessons(cls, user=None):
        now_ts = Clock.utc_timestamp()
        past_lessons = LessonRequest.objects.filter((Q(lesson_time__lte=now_ts) | Q(status=4)))
        if user:
            if user.champuser.is_tutor:
                past_lessons = past_lessons.filter(request_user_id = user.pk)
            else:
                past_lessons = past_lessons.filter(Q(user_id = user.pk)|Q(other_users__id__in=[user.pk]))
        return past_lessons.order_by("-id")

    @classmethod
    def get_coming_lessons(cls, user=None):
        now_ts = Clock.utc_timestamp()
        coming_lessons = LessonRequest.objects.filter((Q(lesson_time__gte=now_ts) & Q(type=0)) | (Q(due_date__gte=now_ts) & Q(type=1)) & ~Q(status=4))
        if user:
            if user.champuser.is_tutor:
                coming_lessons = coming_lessons.filter(request_user_id = user.pk)
            else:
                coming_lessons = coming_lessons.filter(Q(user_id = user.pk)|Q(other_users__id__in=[user.pk]))
        return coming_lessons.order_by("-id")

    @classmethod
    def get_missed_lessons(cls, user=None):
        now_ts = Clock.utc_timestamp()
        missed_lessons = LessonRequest.objects.filter((Q(lesson_time__lte=now_ts) & Q(payment_status=0) & Q(type=0)) | (Q(due_date__lte=now_ts) & Q(payment_status=0) & Q(type=1)))
        if user:
            if user.champuser.is_tutor:
                missed_lessons = missed_lessons.filter(request_user_id = user.pk)
            else:
                missed_lessons = missed_lessons.filter(Q(user_id = user.pk)|Q(other_users__id__in=[user.pk]))
        return missed_lessons.order_by("-lesson_time")

    def initialize_whiteboard(self, live_lesson):
        if live_lesson.whiteboard:
            whiteboard = live_lesson.whiteboard
        else:
            whiteboard = Whiteboard()
            whiteboard.access = WhiteboardAccess.protected.value
            whiteboard.created_by_id = live_lesson.user.pk
            whiteboard.name = uuid.uuid4()
            whiteboard.unique_id = uuid.uuid4()
            whiteboard.save()
            whiteboard.users.add(live_lesson.user)
            whiteboard.users.add(live_lesson.request_user)
            for user in live_lesson.other_users.all():
                whiteboard.users.add(user)
            live_lesson.whiteboard_id = whiteboard.pk
            live_lesson.save()

        # ###Create Pad Author and group.
        pad_util = PadUtil()
        lesson_requested_by_author_id = pad_util.create_or_get_padauthor(live_lesson.user.id,live_lesson.user.champuser.fullname)
        #print "Pad Author Created. Author ID: "+lesson_requested_by_author_id

        lesson_requested_to_author_id = pad_util.create_or_get_padauthor(live_lesson.request_user.id,live_lesson.request_user.champuser.fullname)
        #print "Pad Author Created. Author ID: "+lesson_requested_to_author_id

        pad_group_id = pad_util.create_or_get_padgroup(live_lesson.pk,user_id=live_lesson.user.pk)
        #print "Pad Group Created. Pad Group ID: "+pad_group_id

        pad_id = pad_util.create_group_pad(pad_group_id,user_id=live_lesson.user.pk,author_id=lesson_requested_by_author_id)
        pad_object = Pad.objects.get(pad_id=pad_id)
        pad_author = PadAuthor.objects.filter(author_id=lesson_requested_to_author_id)
        if pad_author.exists():
            pad_object.pad_authors.add(pad_author.first())
        whiteboard.text_pads.add(pad_object)

        codepad_util = CodePadUtil()
        code_pad_obj = codepad_util.create_codepad(str(live_lesson.pk),"Javascript",live_lesson.user)

        whiteboard.code_pads.add(code_pad_obj)

        whiteboard_user1_permission = WhiteboardUserPermission()
        whiteboard_user1_permission.user = live_lesson.user
        whiteboard_user1_permission.save()

        whiteboard_user2_permission = WhiteboardUserPermission()
        whiteboard_user2_permission.user = live_lesson.request_user
        whiteboard_user2_permission.save()

        text_pad_permission = TextPadPermission()
        text_pad_permission.text_pad = pad_object
        text_pad_permission.permission = Permission.all.value
        text_pad_permission.save()

        text_pad_permission2 = TextPadPermission()
        text_pad_permission2.text_pad = pad_object
        text_pad_permission2.permission = Permission.all.value
        text_pad_permission2.save()

        code_pad_permission = CodePadPermission()
        code_pad_permission.code_pad = code_pad_obj
        code_pad_permission.permission = Permission.all.value
        code_pad_permission.save()

        code_pad_permission2 = CodePadPermission()
        code_pad_permission2.code_pad = code_pad_obj
        code_pad_permission2.permission = Permission.all.value
        code_pad_permission2.save()

        whiteboard_user1_permission.code_pad_permissions.add(code_pad_permission)
        whiteboard_user2_permission.code_pad_permissions.add(code_pad_permission)

        whiteboard_user1_permission.text_pad_permissions.add(text_pad_permission)
        whiteboard_user2_permission.text_pad_permissions.add(text_pad_permission2)

        drawingboard = DrawingBoard()
        drawingboard.name = "1"
        drawingboard.numeric_id = 1
        drawingboard.save()

        DrawingboardLastNid.objects.filter(group=str(live_lesson.pk)).delete()

        dblastnid = DrawingboardLastNid()
        dblastnid.group = str(live_lesson.pk)
        dblastnid.nid = 1
        dblastnid.save()

        whiteboard.drawingboards.add(drawingboard)


        drawingboard_permission = DrawingboardPermission()
        drawingboard_permission.drawingboard = drawingboard
        drawingboard_permission.permission = Permission.all.value
        drawingboard_permission.save()

        drawingboard_permission2 = DrawingboardPermission()
        drawingboard_permission2.drawingboard = drawingboard
        drawingboard_permission2.permission = Permission.all.value
        drawingboard_permission2.save()

        whiteboard_user1_permission.drawingboard_permissions.add(drawingboard_permission)
        whiteboard_user2_permission.drawingboard_permissions.add(drawingboard_permission2)

        whiteboard.permissions.add(whiteboard_user1_permission)
        whiteboard.permissions.add(whiteboard_user2_permission)

        calendar_event = CalendarEvent()
        calendar_event.title = live_lesson.title + "(Live lesson)"
        calendar_event.css_class = "event-success"
        # calendar_event.start_timestamp = live_lesson.lesson_time
        # calendar_event.end_timestamp = live_lesson.lesson_time + live_lesson.duration
        calendar_event.user_id = live_lesson.user.pk
        calendar_event.url = reverse("live_lesson",kwargs={"pk": live_lesson.pk})
        calendar_event.start = Clock.utc_timestamp_to_local_datetime(live_lesson.lesson_time, live_lesson.user.champuser.timezone)
        calendar_event.end = Clock.utc_timestamp_to_local_datetime(live_lesson.lesson_time + live_lesson.duration, live_lesson.user.champuser.timezone)
        calendar_event.save()

        calendar_event = CalendarEvent()
        calendar_event.title = live_lesson.title + "(Live lesson)"
        calendar_event.css_class = "event-success"
        # calendar_event.start = datetime.fromtimestamp(live_lesson.lesson_time)
        # calendar_event.end = datetime.fromtimestamp(live_lesson.lesson_time + live_lesson.duration)
        calendar_event.user_id = live_lesson.request_user.pk
        calendar_event.url = reverse("live_lesson",kwargs={"pk": live_lesson.pk})
        calendar_event.start = Clock.utc_timestamp_to_local_datetime(live_lesson.lesson_time, live_lesson.request_user.champuser.timezone)
        calendar_event.end = Clock.utc_timestamp_to_local_datetime(live_lesson.lesson_time + live_lesson.duration, live_lesson.request_user.champuser.timezone)
        calendar_event.save()

        for other_user in live_lesson.other_users.all():
            calendar_event = CalendarEvent()
            calendar_event.title = live_lesson.title + "(Live lesson)"
            calendar_event.css_class = "event-success"
            # calendar_event.start = datetime.fromtimestamp(live_lesson.lesson_time)
            # calendar_event.end = datetime.fromtimestamp(live_lesson.lesson_time + live_lesson.duration)
            calendar_event.user_id = other_user.pk
            calendar_event.url = reverse("live_lesson",kwargs={"pk": live_lesson.pk})
            calendar_event.start = Clock.utc_timestamp_to_local_datetime(live_lesson.lesson_time, other_user.champuser.timezone)
            calendar_event.end = Clock.utc_timestamp_to_local_datetime(live_lesson.lesson_time + live_lesson.duration, other_user.champuser.timezone)
            calendar_event.save()

        return live_lesson.pk


    def create_live_lesson(self, job, tutor_id, job_application, lesson_repeat):
        with transaction.atomic():
            live_lesson = LiveLesson()
            live_lesson.title = job.title
            live_lesson.user_id = job.posted_by_id
            live_lesson.lesson_time = job_application.start_time
            live_lesson.request_user_id = tutor_id
            live_lesson.duration = job_application.duration
            live_lesson.currency = job_application.settlement.currency
            live_lesson.rate = job_application.settlement.price
            live_lesson.message = job.description
            live_lesson.repeat = lesson_repeat
            live_lesson.approved = 1
            live_lesson.save()

            live_lesson.subjects.clear()
            for subject in job.preference.subjects.all():
                live_lesson.subjects.add(subject)

            live_lesson.other_users.clear()
            for participant in job.other_students.all():
                live_lesson.other_users.add(participant)

            for j in job.job_attachment.all():
                live_lesson.attachments.add(j)

            self.initialize_whiteboard(live_lesson)

            return True, live_lesson.pk


    def create_written_lesson(self, job, tutor_id, job_application, lesson_repeat):
        written_lesson = WrittenLesson()
        written_lesson.title = job.title
        written_lesson.message = job.description
        written_lesson.user_id = job.posted_by_id
        written_lesson.duration = job_application.duration
        written_lesson.due_date = job_application.start_time
        written_lesson.request_user_id = tutor_id
        written_lesson.repeat = lesson_repeat
        written_lesson.currency = job_application.settlement.currency
        written_lesson.rate = job_application.settlement.price
        written_lesson.approved = 1
        written_lesson.save()

        written_lesson.subjects.clear()
        for subject in job.preference.subjects.all():
            written_lesson.subjects.add(subject)

        written_lesson.other_users.clear()
        for participant in job.other_students.all():
            written_lesson.other_users.add(participant)

        for j in job.job_attachment.all():
            written_lesson.attachments.add(j)

        calendar_event = CalendarEvent()
        calendar_event.title = written_lesson.title + "(Written lesson)"
        calendar_event.css_class = "event-success"
        # calendar_event.start_timestamp = live_lesson.lesson_time
        # calendar_event.end_timestamp = live_lesson.lesson_time + live_lesson.duration
        calendar_event.user_id = written_lesson.user.pk
        calendar_event.url = reverse("live_lesson",kwargs={"pk": written_lesson.pk})
        calendar_event.start = Clock.utc_timestamp_to_local_datetime(written_lesson.due_date, written_lesson.user.champuser.timezone)
        calendar_event.end = Clock.utc_timestamp_to_local_datetime(written_lesson.due_date + written_lesson.duration, written_lesson.user.champuser.timezone)
        calendar_event.save()

        calendar_event = CalendarEvent()
        calendar_event.title = written_lesson.title + "(Written lesson)"
        calendar_event.css_class = "event-success"
        # calendar_event.start = datetime.fromtimestamp(live_lesson.lesson_time)
        # calendar_event.end = datetime.fromtimestamp(live_lesson.lesson_time + live_lesson.duration)
        calendar_event.user_id = written_lesson.request_user.pk
        calendar_event.url = reverse("live_lesson",kwargs={"pk": written_lesson.pk})
        calendar_event.start = Clock.utc_timestamp_to_local_datetime(written_lesson.due_date, written_lesson.request_user.champuser.timezone)
        calendar_event.end = Clock.utc_timestamp_to_local_datetime(written_lesson.due_date + written_lesson.duration, written_lesson.request_user.champuser.timezone)
        calendar_event.save()

        return True, written_lesson.pk


