from core.models3 import SupportedLanguage, Language

__author__ = 'Sohel'

class LanguageUtil(object):
    def __init__(self):
        pass

    @classmethod
    def get_supported_languages(cls):
        return [ (lang.pk, lang.name) for lang in SupportedLanguage.objects.all()]

    @classmethod
    def get_all_languages(cls):
        return [ (lang.pk, lang.name) for lang in Language.objects.all()]
