__author__ = 'codengine'

class TimezoneUtil(object):

    @classmethod
    def get_user_timezone(self,request):
        try:
            tz = request.user.champuser.timezone
            return tz
        except Exception,msg:
            print("Exception Occured. Message: %s" % str(msg))
