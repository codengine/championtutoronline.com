from core.library.email_scheduler import EmailScheduler
from core.library.email_template_util import EmailTemplateUtil

__author__ = 'Sohel'

###Signup Email.
# token = "sdasdasdasdasdas"
# signup_email_html, text = EmailTemplateUtil.render_signup_email("Md Shariful Islam",token)
# # print(signup_email_html)
# f = open('test_mail.html','w')
# f.write(signup_email_html)
# f.close()
# email_object = {
#     "recipients": ["codenginebd@gmail.com", "tanwilson83@yahoo.com.sg"],
#     'subject': 'Registration Confirmation',
#     'html_body': signup_email_html,
#     'text_body': text
# }
# EmailScheduler.place_to_queue(email_object)

###Job Invitation Email.
# html, text = EmailTemplateUtil.render_job_invitation_email_template("Md Shariful Islam","Sam","Please i need help with mathematics.",2)
#
# email_object = {
#     "recipients": [ "codenginebd@gmail.com", "tanwilson83@yahoo.com.sg" ],
#     'subject': 'Job Invitation',
#     'html_body': html,
#     'text_body': text
# }
#
# EmailScheduler.place_to_queue(email_object)

# ###Job Application Email.
# student_name = "Mr Test Student 1"
# tutor_name = "Mr Test Tutor 1"
# application_id = 8
# html, text = EmailTemplateUtil.render_job_applied_email_template(student_name, tutor_name, application_id)
#
# email_object = {
#     "recipients": [ "codenginebd@gmail.com", "tanwilson83@yahoo.com.sg" ],
#     'subject': 'Job Applied',
#     'html_body': html,
#     'text_body': text
# }
#
# EmailScheduler.place_to_queue(email_object)

###Job Invitation Reject Email.
# student_name = "Mr Test Student 1"
# tutor_name = "Mr Test Tutor 1"
# job_invitation_id = 19
# html, text = EmailTemplateUtil.render_job_invitation_rejected_email_template(student_name, tutor_name, job_invitation_id)
#
# email_object = {
#     "recipients": [ "codenginebd@gmail.com", "tanwilson83@yahoo.com.sg" ],
#     'subject': 'Job Invitation Rejected',
#     'html_body': html,
#     'text_body': text
# }
#
# EmailScheduler.place_to_queue(email_object)

###Job Application Reject Email.
# student_name = "Mr Test Student 1"
# tutor_name = "Mr Test Tutor 1"
# job_application_id = 8
# html, text = EmailTemplateUtil.render_job_application_rejected_email_template(student_name, tutor_name, job_application_id)
#
# email_object = {
#     "recipients": [ "codenginebd@gmail.com", "tanwilson83@yahoo.com.sg" ],
#     'subject': 'Job Application Rejected',
#     'html_body': html,
#     'text_body': text
# }
#
# EmailScheduler.place_to_queue(email_object)

###Job Offered Email.
# student_name = "Mr Test Student 1"
# tutor_id = 2
# tutor_name = "Mr Test Tutor 1"
# job_offer_id = 2
# html, text = EmailTemplateUtil.render_job_offered_email_template(student_name, tutor_id, tutor_name, job_offer_id)
#
# email_object = {
#     "recipients": [ "codenginebd@gmail.com", "tanwilson83@yahoo.com.sg" ],
#     'subject': 'Job Offer',
#     'html_body': html,
#     'text_body': text
# }
#
# EmailScheduler.place_to_queue(email_object)

###Job Offer Accepted Email.
# student_name = "Mr Test Student 1"
# tutor_id = 2
# tutor_name = "Mr Test Tutor 1"
# job_offer_id = 2
# html, text = EmailTemplateUtil.render_job_offer_accepted_email_template(student_name, tutor_id, tutor_name, job_offer_id)
#
# email_object = {
#     "recipients": [ "codenginebd@gmail.com", "tanwilson83@yahoo.com.sg"  ],
#     'subject': 'Job Offer Accepted',
#     'html_body': html,
#     'text_body': text
# }
#
# EmailScheduler.place_to_queue(email_object)

###Job Offer Rejected Email.
# student_name = "Mr Test Student 1"
# tutor_id = 2
# tutor_name = "Mr Test Tutor 1"
# job_offer_id = 2
# html, text = EmailTemplateUtil.render_job_offer_rejected_email_template(student_name, tutor_id, tutor_name, job_offer_id)
#
# email_object = {
#     "recipients": [ "codenginebd@gmail.com", "tanwilson83@yahoo.com.sg"  ],
#     'subject': 'Job Offer Rejected',
#     'html_body': html,
#     'text_body': text
# }
#
# EmailScheduler.place_to_queue(email_object)

###Payment Processed Successfully.
# job_id = 2
# lesson_id = 5
# lesson_type = 0
# payer_name = "Md Shariful Islam"
# payment_amount = 100
# currency = 'USD'
# html, text = EmailTemplateUtil.render_payment_processed_email(job_id,lesson_id,lesson_type,payer_name,payment_amount,currency)
#
# email_object = {
#     "recipients": [ "codenginebd@gmail.com", "tanwilson83@yahoo.com.sg"  ],
#     'subject': 'Payment Processed',
#     'html_body': html,
#     'text_body': text
# }
#
# EmailScheduler.place_to_queue(email_object)

###Payment Process Failed.
# job_id = 2
# lesson_id = 5
# lesson_type = 0
# payer_name = "Md Shariful Islam"
# payment_amount = 100
# currency = 'USD'
# fail_reason = "Invalid Credit Card"
# html, text = EmailTemplateUtil.render_payment_process_failed_email(job_id,lesson_id,lesson_type,payer_name,payment_amount,currency,fail_reason)
#
# email_object = {
#     "recipients": [ "codenginebd@gmail.com", "tanwilson83@yahoo.com.sg" ],
#     'subject': 'Payment Failed',
#     'html_body': html,
#     'text_body': text
# }
#
# EmailScheduler.place_to_queue(email_object)

###Lesson Scheduled.
# receiver_name = "Md Shariful Islam"
# lesson_id = 5
# lesson_type = 0
# html, text = EmailTemplateUtil.render_lesson_scheduled_email(receiver_name,lesson_id,lesson_type)
#
# email_object = {
#     "recipients": [ "codenginebd@gmail.com", "tanwilson83@yahoo.com.sg" ],
#     'subject': 'Lesson Started',
#     'html_body': html,
#     'text_body': text
# }
#
# EmailScheduler.place_to_queue(email_object)

###Lesson Reminder.
# receiver_name = "Md Shariful Islam"
# lesson_id = 5
# lesson_type = 0
# html, text = EmailTemplateUtil.render_lesson_reminder_email(receiver_name,lesson_id,lesson_type)
#
# email_object = {
#     "recipients": [ "codenginebd@gmail.com", "tanwilson83@yahoo.com.sg" ],
#     'subject': 'Lesson Reminder',
#     'html_body': html,
#     'text_body': text
# }
#
# EmailScheduler.place_to_queue(email_object)

###Internal Server Error To Admin.
# stacktrace_details = """
#     Traceback (most recent call last):
#     File "/usr/local/lib/python2.7/dist-packages/django/core/handlers/base.py", line 114, in get_response
#     response = wrapped_callback(request, *callback_args, **callback_kwargs)
#     File "/usr/local/lib/python2.7/dist-packages/django/contrib/auth/decorators.py", line 22, in _wrapped_view
#     return view_func(request, *args, **kwargs)
# """
# html, text = EmailTemplateUtil.render_internal_server_error_admin_email(stacktrace_details)
#
# email_object = {
#     "recipients": [ "codenginebd@gmail.com", "tanwilson83@yahoo.com.sg" ],
#     'subject': 'Internal Server Error Occured',
#     'html_body': html,
#     'text_body': text
# }
#
# EmailScheduler.place_to_queue(email_object)

###Digest Email Job Recommended.
# receiver_name = "Md Shariful Islam"
# job_ids = [ 2,3,5,6 ]
# html, text = EmailTemplateUtil.render_digest_recommended_email(receiver_name,job_ids)
#
# email_object = {
#     "recipients": [ "codenginebd@gmail.com", "tanwilson83@yahoo.com.sg" ],
#     'subject': 'Recommended Jobs',
#     'html_body': html,
#     'text_body': text
# }
#
# EmailScheduler.place_to_queue(email_object)

###Password Reset Email.
# requested_at_email = "codenginebd@gmail.com"
# password_reset_token = "asdasd"
# html, text = EmailTemplateUtil.render_password_reset_token_email(requested_at_email,password_reset_token)
#
# email_object = {
#     "recipients": [ "codenginebd@gmail.com", "tanwilson83@yahoo.com.sg" ],
#     'subject': 'Password Reset',
#     'html_body': html,
#     'text_body': text
# }
#
# EmailScheduler.place_to_queue(email_object)

###Lesson time extended email.
# html, text = EmailTemplateUtil.render_lesson_time_extended_email(34, 0, "Md Shariful Islam", 30)
#
# email_object = {
#     "recipients": [ "codenginebd@gmail.com" ],
#     'subject': 'Lesson time extended',
#     'html_body': html,
#     'text_body': text
# }
#
# EmailScheduler.place_to_queue(email_object)

###Lesson rescheduled email.
# html, text = EmailTemplateUtil.render_lesson_reschedule_email(34, 0, "Md Shariful Islam", 30)
#
# email_object = {
#     "recipients": [ "codenginebd@gmail.com" ],
#     'subject': 'Lesson time extended',
#     'html_body': html,
#     'text_body': text
# }
#
# EmailScheduler.place_to_queue(email_object)
