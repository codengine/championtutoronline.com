from core.celery import app
# from celery.task import Task
from core.emails import EmailClient


@app.task
def send_email_task(email_object):
    EmailClient.send_email(email_object["recipients"],email_object["subject"],email_object["html_body"],email_object["text_body"],from_email=None)

