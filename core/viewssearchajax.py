from core.enums import UserTypes
from core.generics.ajaxmixins import JSONResponseMixin
from core.library.block_util import BlockListUtil
from core.models3 import Role

__author__ = 'codengine'

from django.contrib.auth.models import User
from django.views.generic.base import View
from django.http import HttpResponse
import json
from core.models3 import *
from django.db.models import Q
from django.template import loader, Context

class TutorSearchAjaxView(JSONResponseMixin,View):
    def get(self,request,*args,**kwargs):

        json_response = {
            "status": "FAILURE",
            "message": "FAILURE",
            "data": []
        }

        keyword = request.GET.get("term")
        queryset1 = ChampUser.objects.filter(type__name=UserTypes.teacher.value, user__is_active=True).filter(Q(fullname__icontains=keyword) | Q(user__email__iexact=keyword) | Q(major__icontains=keyword) & Q(type=Role.objects.get(name=UserTypes.teacher.value)))[:50]
        user_major_objects = UserMajor.objects.filter(Q(major__name__icontains=keyword) | Q(user__champuser__fullname__icontains=keyword) | Q(user__email__iexact=keyword) | Q(user__champuser__major__icontains=keyword) & Q(user__is_active=True) & Q(user__champuser__type=Role.objects.get(name=UserTypes.teacher.value)))[:50]
        queryset = []
        for user_major_object in user_major_objects:
            if user_major_object.user.pk != request.user.pk:
                if not user_major_object.user.champuser in queryset and not user_major_object.user.champuser in queryset1:
                    if not BlockListUtil.check_if_user_blocked(request.user.pk, user_major_object.user.pk):
                        queryset += [user_major_object.user.champuser]

        for q1 in queryset1:
            if not BlockListUtil.check_if_user_blocked(request.user.pk, q1.user.pk):
                queryset += [ q1 ]
        template = loader.get_template("ajax/tutor-search-result.html")
        cntxt = Context({ 'tutor_list': queryset[:20] ,"request": request,"keyword": keyword})
        rendered = template.render(cntxt)

        json_response["status"] = "SUCCESS"
        json_response["message"] = "Successful"
        json_response["data"] = rendered

        return self.render_to_json(json_response)

class FetchSubjectsLevelAjaxView(JSONResponseMixin, View):
    def get(self, request, *args, **kwargs):
        level_names = request.GET.getlist("level_name[]")
        term = request.GET.get("term")
        param_type = request.GET.get("param_type")
        if param_type == "id":
            level_names = [ int(id) for id in level_names ]
        else:
            temp_level_names = []
            for lname in level_names:
                if lname.startswith('University'):
                    temp_level_names += [ 'University' ]
                else:
                    temp_level_names += [ lname ]
            level_names = temp_level_names
        only_data = request.GET.get("only_data")
        if param_type == "id":
            major_objects = Major.objects.filter(level__pk__in=level_names)
        else:
            major_objects = Major.objects.filter(level__name__in=level_names)
        if term:
            major_objects = major_objects.filter(name__icontains=term)
        major_subjects = []
        for major in major_objects:
            major_subjects += [
                {
                    "id": major.pk,
                    "name": major.name
                }
            ]
        if only_data:
            return self.render_to_json(major_subjects)
        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": {
                "subjects": major_subjects
            }
        }
        return self.render_to_json(response)

class JobsStudentSearchAjaxView(JSONResponseMixin,View):
    def get(self,request,*args,**kwargs):

        json_response = {
            "status": "FAILURE",
            "message": "FAILURE",
            "data": []
        }

        keyword = request.GET.get("term")
        queryset1 = ChampUser.objects.filter(type__name=UserTypes.student.name).filter(Q(fullname__icontains=keyword) | Q(major__icontains=keyword) & Q(type__name=UserTypes.student.value))[:15]
        student_entries = []
        for student in queryset1:

            if not BlockListUtil.check_if_user_blocked(request.user.pk, student.user.pk):
                template = loader.get_template("ajax/top_search_jobs_student_entry.html")
                cntxt = Context({ 'student': student ,"request": request,"keyword": keyword, 'type': 'user'})
                rendered = template.render(cntxt)
                student_entries += [  rendered ]

        job_entries = []
        job_objects = Job.objects.filter(Q(posted_by__champuser__fullname__icontains=keyword) | Q(preference__subjects__name__icontains=keyword) | Q(preference__nationalities__name__icontains=keyword))[:15]
        for job in job_objects:

            if not BlockListUtil.check_if_user_blocked(request.user.pk, job.posted_by.pk):
                template = loader.get_template("ajax/top_search_jobs_student_entry.html")
                cntxt = Context({ 'job': job ,"request": request,"keyword": keyword, 'type': 'job'})
                rendered = template.render(cntxt)
                job_entries += [  rendered ]

        template = loader.get_template("ajax/top_search_jobs_student_list.html")
        cntxt = Context({ 'student_entries': student_entries, "job_entries": job_entries, "request": request,"keyword": keyword})
        rendered = template.render(cntxt)

        json_response["status"] = "SUCCESS"
        json_response["message"] = "Successful"
        json_response["data"] = rendered

        return self.render_to_json(json_response)