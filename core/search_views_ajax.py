from decimal import Decimal
import math
from django.db.models.query_utils import Q
from django.http.response import Http404
from django.template import loader, Context
from django.views.generic.base import View
from django.views.generic.list import ListView
from core.enums import UserTypes, AvailableStatus, OnlineStatus
from core.generics.ajaxmixins import JSONResponseMixin
from core.generics.mixins import LoginRequiredMixin
from core.library.Clock import Clock
from core.library.job_util import JobUtil
from core.loader.template_loader import TemplateLoader
from core.models3 import ChampUser, UserMajor, Rate, Job, Language, ProfileMyVideo, Major, TopSearch, \
    JobApplicationArchive

__author__ = "Sophel"

class SearchTutorListAjaxView(JSONResponseMixin, ListView):
    model = ChampUser
    paginate_by = 10
    queryset = ChampUser.objects.filter(type__name=UserTypes.teacher.value)

    show_pages = 13

    def get_template_names(self):
        return [ "ajax/search_result_item.html", "ajax/search_result_list.html", "ajax/tutor_search_pagination.html" ]

    def get_page_links(self,current_page,total_pages):
        if current_page <= 0:
            return [(i + 1) for i in range(self.show_pages)]

        if current_page > total_pages:
            start_page = total_pages - self.show_pages
            if start_page <= 0:
                start_page = 1

            return [i for i in range(start_page,total_pages)]

        if total_pages <= self.show_pages:
            return [(i + 1) for i in range(total_pages)]

        start_page = current_page - int(math.ceil(self.show_pages / 2))
        if start_page <= 0:
            start_page = 1

        end_page = start_page + self.show_pages

        if end_page > total_pages:
            end_page = total_pages

        return [i for i in range(start_page,end_page)]

    def get_queryset(self):
        queryset = super(SearchTutorListAjaxView,self).get_queryset()

        ###Now apply filters.
        return queryset

    def get_qparam(self,key):
        if self.request.GET.get(key):
            return key, self.request.GET.get(key)
        return key, None

    def dispatch(self, request, *args, **kwargs):
        dispatched = super(SearchTutorListAjaxView, self).dispatch(request, *args, **kwargs)
        return dispatched

    def _filter_search_query(self,**kwargs):
        queryset = self.get_queryset()

        _filter = {}

        ready_to_teach, ready_to_teach_value = self.get_qparam("ready_to_teach")
        if ready_to_teach_value:
            ready_to_teach_value = int(ready_to_teach_value)
            if ready_to_teach_value == 1:
                queryset = queryset.filter(Q(available_to_teach=AvailableStatus.ready_to_teach.value) | Q(available_to_teach=AvailableStatus.ready_for_both.value))

        online_status, online_status_value = self.get_qparam("online")
        if online_status_value:
            online_status_value = int(online_status_value)
            if online_status_value == 1:
                queryset = queryset.filter(online_status=OnlineStatus.online.value)

        video_intro, video_intro_value = self.get_qparam("video-intro")
        if video_intro:
            if video_intro_value == "true":
                my_video_users = ProfileMyVideo.objects.values_list('user_id', flat=True)
                queryset = queryset.filter(user__id__in=my_video_users)

        gender_pref, gender_pref_value = self.get_qparam("gender")
        if gender_pref_value and gender_pref_value != "no_pref":
            queryset = queryset.filter(gender=gender_pref_value)

        levels = self.request.GET.getlist("level[]")
        if levels:
            queryset = queryset.filter(user_id__in=UserMajor.objects.filter(major__level__name__in=levels).values_list('user_id'))

        subjects = self.request.GET.getlist("subject[]")
        if subjects:
            queryset = queryset.filter(user_id__in=UserMajor.objects.filter(major__name__in=subjects).values_list('user_id'))

            ###Update search rank.
            majors = Major.objects.filter(name__in=subjects)
            for major in majors:
                crank = major.rank
                major.rank = crank + 1
                major.save()

        countries = self.request.GET.getlist("country[]")
        if countries:
            queryset = queryset.filter(nationality__name__in=countries)

        budget, budget_value = self.get_qparam("budget")
        if budget_value:

            budgets = [ b for b in budget_value.split(',') if b ]
            rate_user_ids = []
            for budgt in budgets:
                if budgt == "sgd1":
                    low = 0
                    high = 20
                elif budgt == "sgd2":
                    low = 20
                    high = 40
                elif budgt == "sgd3":
                    low = 41
                    high = 60
                elif budgt == "sgd4":
                    low = 61
                    high = 1000000
                rate_user_ids += [ entry['user_id'] for entry in  UserMajor.objects.filter(rate__gte=low, rate__lte=high).values('user_id').distinct() ]
            queryset = queryset.filter(user__id__in=rate_user_ids)

        keyword, keyword_value = self.get_qparam("keyword")
        if keyword_value:
            if keyword_value.endswith('Tutor'):
                keyword_value = keyword_value[:keyword_value.rindex('Tutor')]
            elif keyword_value.endswith('tutor'):
                keyword_value = keyword_value[:keyword_value.rindex('tutor')]
            queryset = queryset.filter(
                Q(user__champuser__fullname__icontains=keyword_value) |
                Q(user__teachingexperiences__experience__icontains=keyword_value) |
                Q(user__usermajor__major__name__icontains=keyword_value) |
                Q(user__usermajor__major__level__name__icontains=keyword_value) |
                Q(user__extracurricularinterest__interest__icontains=keyword_value) |
                Q( user__usertopcourses__subject__name__icontains=keyword_value )).distinct()

        return queryset

    def get_context_data(self, **kwargs):
        queryset = self._filter_search_query(**kwargs)

        total_users = queryset.count()
        online_users = queryset.filter(online_status=OnlineStatus.online.value).count()

        page_size = self.get_paginate_by(queryset)

        current_page = self.request.GET.get("page")
        if not current_page:
            current_page = 1
        try:
            current_page = int(current_page)
        except:
            current_page = 1

        try:
            paginator, page, queryset, is_paginated = self.paginate_queryset(queryset, page_size)
        except Exception as msg:
            self.page_kwarg = 1
            paginator, page, queryset, is_paginated = self.paginate_queryset(self.queryset, page_size)
            current_page = 1

        if current_page > paginator.num_pages:
            current_page = 1

        if current_page <= 0:
            current_page = 1

        page = paginator.page(current_page)

        ###Now update top search
        if self.request.user.is_authenticated():
            search_subjects = self.request.GET.getlist("subject[]")
            for sub in search_subjects:
                top_searches = TopSearch.objects.filter(user_id=self.request.user.pk, subject__name=sub)
                if top_searches.exists():
                    top_search = top_searches.first()
                    top_search.search_frequency += 1
                    top_search.save()

                    major_objects = Major.objects.filter(name=sub)
                    if major_objects.exists():
                        major_object = major_objects.first()
                        major_object.rank += 1
                        major_object.save()
                else:
                    major_objects = Major.objects.filter(name=sub)
                    if major_objects.exists():
                        major_object = major_objects.first()
                        top_search = TopSearch()
                        top_search.user_id = self.request.user.pk
                        top_search.subject_id = major_object.pk
                        top_search.search_frequency = 1
                        top_search.save()

                        major_object.rank += 1
                        major_object.save()


        ###Now render each row and make list.
        rendered_contents = []
        for champ_user in page.object_list:
            template_name = "ajax/search_result_item.html"
            template = loader.get_template(template_name)

            subjects = self.request.GET.getlist("subject[]")

            subject_str = ""
            if subjects:
                user_major_list =  [ entry[0] for entry in UserMajor.objects.filter(user_id=champ_user.user.pk).values_list('major__name') ]
                common_subjects = list(set(subjects) & set(user_major_list))
                if common_subjects:
                    if len(common_subjects) == 1:
                        subject_str = common_subjects[0]
                    elif len(common_subjects) == 2:
                        subject_str = common_subjects[0] + " and " + common_subjects[1]
                    else:
                        s_list = common_subjects[:-1]
                        subject_str = ", ".join(s_list)
                        subject_str += " and " + common_subjects[len(common_subjects) - 1]
            else:
                keyword, keyword_value = self.get_qparam("keyword")
                if keyword and keyword_value:
                    um = UserMajor.objects.filter(user_id=champ_user.user.pk, major__name=keyword_value)
                    if um.exists():
                        subject_str = keyword_value

            keyword, keyword_value = self.get_qparam("keyword")
            keyword_found = keyword_value is not None

            context = {
                "enable_invite_msg": not self.request.user.champuser.is_tutor if self.request.user.is_authenticated() else True,
                "champ_user":champ_user,
                "subject_teach": subject_str,
                "keyword_found": keyword_found,
                "keyword": keyword_value
            }
            cntxt = Context(context)
            rendered = template.render(cntxt)
            rendered_contents += [ rendered ]

        template_name = "ajax/search_result_list.html"
        template = loader.get_template(template_name)

        data_context = {
            "data_items":rendered_contents,
            'paginator': paginator,
            'start_index':page.start_index(),
            'end_index':page.end_index(),
            'page_obj': page,
            'has_next_page': page.has_next(),
            'num_pages': paginator.num_pages,
            'current_page':current_page,
            'show_num_pages': 8,
            "total_users_count": total_users,
            'online_users_count':online_users,
            'page_links': self.get_page_links(current_page,paginator.num_pages),
            #'page_list': [(i + 1) for i in range(current_page,paginator.num_pages)],
            'has_previous_page': page.has_previous(),
            'is_paginated': is_paginated
        }
        build_cntxt = Context(data_context)
        rendered = template.render(build_cntxt)

        context = {
            "current_page": current_page,
            "page_links": self.get_page_links(current_page,paginator.num_pages)
        }
        template_name = "ajax/tutor_search_pagination.html"
        template = loader.get_template(template_name)
        build_cntxt = Context(data_context)
        page_content = template.render(build_cntxt)

        breadcumbs = self.request.session.get('breadcumbs', [])
        level, level_value = self.get_qparam("level")

        ref_b, ref_b_value = self.get_qparam('sref')
        __bid, __bid_value = self.get_qparam('_bid')

        subject, subject_value = self.get_qparam("subject")
        if subject_value and subject_value != "-1":
            if level_value and not ref_b_value and not ref_b_value:
                _bid = str(Clock.utc_timestamp()*1000)
                breadcumbs += [{
                    "type": "level",
                    "bid": _bid,
                    "title": level_value,
                    "url": "level=%s&sref=_b&_bid=%s" % ( level_value, _bid)
                }]
            if not ref_b_value:
                _bid = str(Clock.utc_timestamp()*1000)
                breadcumbs += [{
                    "type": "subject",
                    "bid": _bid,
                    "title": subject_value,
                    "url": "level=%s&subject=%s&sref=_b&_bid=%s" % ( level_value, subject_value, _bid)
                }]

        else:
            if level_value and not ref_b_value:
                _bid = str(Clock.utc_timestamp()*1000)
                breadcumbs += [{
                    "type": "level",
                    "bid": _bid,
                    "title": level_value,
                    "url": "level=%s&sref=_b&_bid=%s" % ( level_value, _bid)
                }]
        if len(breadcumbs) > 10:
            breadcumbs = breadcumbs[-10:]
        self.request.session['breadcumbs'] = breadcumbs

        breadcumb_template_name = "ajax/breadcumb_links.html"
        breadcumb_data = {
            'data_list': breadcumbs
        }

        if ref_b_value:
            breadcumb_data["bid__"] = __bid_value

        breadcumb_template = loader.get_template(breadcumb_template_name)
        breadcumb_data = Context(breadcumb_data)
        breadcumb_content = breadcumb_template.render(breadcumb_data)

        return rendered, current_page, total_users, online_users, page_content, breadcumb_content

    def get(self, request, *args, **kwargs):
        self.object_list = self.get_queryset()
        allow_empty = self.get_allow_empty()

        if not allow_empty:
            # When pagination is enabled and object_list is a queryset,
            # it's better to do a cheap query than to load the unpaginated
            # queryset in memory.
            if (self.get_paginate_by(self.object_list) is not None
                and hasattr(self.object_list, 'exists')):
                is_empty = not self.object_list.exists()
            else:
                is_empty = len(self.object_list) == 0
            if is_empty:
                raise Http404(_("Empty list and '%(class_name)s.allow_empty' is False.")
                        % {'class_name': self.__class__.__name__})
        response, current_page, total_users, online_users, page_content, breadcumb_content = self.get_context_data()
        context = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": response,
            "extra_data": {
                "page_content": page_content,
                "current": current_page,
                "total_count": total_users,
                "online_count": online_users,
                "breadcumb_content": breadcumb_content
            }
        }
        return self.render_to_response(context)

    def render_to_response(self, context, **response_kwargs):
        return self.render_to_json(context)

class AjaxMarketplaceJobsView(JSONResponseMixin, ListView):
    model = Job
    paginate_by = 10
    queryset = Job.objects.all()

    show_pages = 13

    def get_template_names(self):
        return [ "ajax/marketplace_entry.html", "ajax/marketplace_entry_list.html" ]

    def get_page_links(self,current_page,total_pages):
        if current_page <= 0:
            return [(i + 1) for i in range(self.show_pages)]

        if current_page > total_pages:
            start_page = total_pages - self.show_pages
            if start_page <= 0:
                start_page = 1

            return [i for i in range(start_page,total_pages)]

        if total_pages <= self.show_pages:
            return [(i + 1) for i in range(total_pages)]

        start_page = current_page - int(math.ceil(self.show_pages / 2))
        if start_page <= 0:
            start_page = 1

        end_page = start_page + self.show_pages

        if end_page > total_pages:
            end_page = total_pages

        return [i for i in range(start_page,end_page)]

    def get_queryset(self):
        queryset = super(AjaxMarketplaceJobsView,self).get_queryset()

        queryset = queryset.exclude(status=4) #Exclude deleted jobs.

        ###Now apply filters.

        lesson_type = self.get_qparam("lesson_type")
        if lesson_type == "live":
            queryset = queryset.filter(lesson_type=0)
        elif lesson_type == "written":
            queryset = queryset.filter(lesson_type=1)

        gender = self.get_qparam("gender")
        if gender == "male" or gender == "female":
            queryset = queryset.filter(posted_by__champuser__gender=gender)

        subjects = self.get_qparam("subjects")
        if subjects:
            subjects = subjects.split(",")
            queryset = queryset.filter(preference__subjects__value__in=subjects)

        country = self.get_qparam("countries")
        if country and country != "-1":
            queryset = queryset.filter(preference__nationalities__name__icontains=country)

        budget = self.get_qparam("budget")
        if budget:
            try:
                budget = int(budget)
                queryset = queryset.filter(Q(preference__rate__rate_value__gte=(budget-1)) & Q(preference__rate__rate_value__lte=(budget + 1)))
            except:
                queryset = queryset.model.objects.none()

        currency = self.get_qparam("currency")
        if currency and currency != "-1":
            queryset = queryset.filter(preference__currency=currency)

        language = self.get_qparam("language")
        if language and language != "-1":
            try:
                language_object = Language.objects.get(pk=int(language))
                queryset = queryset.filter(preference__languages__value__in=language_object.name)
            except:
                queryset = queryset.model.objects.none()

        keywords = self.get_qparam("keywords")
        if keywords:
            keywords = keywords.split(",")
            for keyword in keywords:
                queryset = queryset.filter(Q(posted_by__champuser__fullname__icontains=keyword) | Q(preference__subjects__value__icontains=keyword) | Q(preference__nationalities__name__icontains=keyword))

        return queryset.order_by('-date_created')

    def get_qparam(self,key):
        if self.request.GET.get(key):
            return self.request.GET.get(key)

    def _filter_search_query(self,**kwargs):
        queryset = self.get_queryset()
        return queryset

    def get_context_data(self, **kwargs):
        queryset = self._filter_search_query(**kwargs)

        total_records = queryset.count()

        page_size = self.get_paginate_by(queryset)

        current_page = self.request.GET.get("page")
        if not current_page:
            current_page = 1
        try:
            current_page = int(current_page)
        except:
            current_page = 1

        try:
            paginator, page, queryset, is_paginated = self.paginate_queryset(queryset, page_size)
        except Exception as msg:
            self.page_kwarg = 1
            paginator, page, queryset, is_paginated = self.paginate_queryset(self.queryset, page_size)
            current_page = 1

        if current_page > paginator.num_pages:
            current_page = 1

        if current_page <= 0:
            current_page = 1

        page = paginator.page(current_page)

        ###Now render each row and make list.
        rendered_contents = []
        for job_entry in page.object_list:
            template_name = "ajax/marketplace_entry.html"
            template = loader.get_template(template_name)
            tutor_applications = job_entry.applications.filter(tutor_id=self.request.user.pk)
            job_application_archives = JobApplicationArchive.objects.filter(application__tutor_id=self.request.user.pk,job_id=job_entry.pk)
            applied = tutor_applications.exists() or job_application_archives.exists()
            applied_status_message = ''
            if applied:
                if tutor_applications.exists():
                    tutor_application = tutor_applications.first()
                    if tutor_application.status == 2 or tutor_application.status == 9:
                        applied_status_message = 'You have applied'
                    elif tutor_application.status == 13:
                        applied_status_message = 'You have withdrawn'
                    elif tutor_application.status == 8:
                        applied_status_message = 'You are hired'
                elif job_application_archives.exists():
                    job_application_archive = job_application_archives.first()
                    if job_application_archive.application.status == 13:
                        applied_status_message = 'You have withdrawn'

            context = {
                "job":job_entry,
                "applied": applied,
                "applied_status_message": applied_status_message
            }
            cntxt = Context(context)
            rendered = template.render(cntxt)
            rendered_contents += [ rendered ]

        template_name = "ajax/marketplace_entry_list.html"
        template = loader.get_template(template_name)

        data_context = {
            "data_items":rendered_contents,
            'paginator': paginator,
            'start_index':page.start_index(),
            'end_index':page.end_index(),
            'page_obj': page,
            'has_next_page': page.has_next(),
            'num_pages': paginator.num_pages,
            'current_page':current_page,
            'show_num_pages': 8,
            'total_records': total_records,
            'page_links': self.get_page_links(current_page,paginator.num_pages),
            #'page_list': [(i + 1) for i in range(current_page,paginator.num_pages)],
            'has_previous_page': page.has_previous(),
            'is_paginated': is_paginated
        }

        build_cntxt = Context(data_context)
        rendered = template.render(build_cntxt)

        return rendered, current_page, total_records

    def render_to_response(self, context, **response_kwargs):
        #response = super(SearchTutorListAjaxView, self).render_to_response(context, response_kwargs)
        response, current_page, total_records = self.get_context_data()
        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": response,
            "extra_data": {
                "current": current_page,
                "total_count": total_records
            }
        }
        return self.render_to_json(response)

class AjaxRecommendedTutorsView(JSONResponseMixin, ListView):
    model = ChampUser
    template_name = "ajax/recommended_tutors.html"
    data_set_size = 5

    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            response = {
                "status": "FAILURE",
                "message": "Unauthorized Access",
                "data": ""
            }
            return self.render_to_json(response)
        return super(AjaxRecommendedTutorsView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        try:
            job_id = int(self.request.GET.get("job_id"))

            exclude_user_ids = []
            try:
                uids = self.request.GET.get('exclude_ids')
                exclude_user_ids = [ int(uid) for uid in uids.split(',') if uid ]
            except Exception as exp:
                pass

            recommended_tutors = JobUtil.get_recommended_users(job_id=job_id, exclude_ids=exclude_user_ids, limit=self.data_set_size)

            user_contents = []
            for tutor in recommended_tutors:
                template_name = "ajax/recommended_tutor_item.html"

                subject_teach = ""
                user_major_objects = UserMajor.objects.filter(user_id=tutor.user.pk).values_list('major__name', flat=True)
                if user_major_objects:
                    subject_teach = ', '.join([ subject.capitalize() for subject in user_major_objects ])

                context = {
                    "champ_user": tutor,
                    "subject_teach": subject_teach,
                    "source_recommendation": True,
                    "job_id": job_id
                }
                content = TemplateLoader.load_template(template_name,context)
                user_contents += [ content ]

            if self.request.GET.get("format") == "json":
                return {
                    "tutor_list": user_contents
                }

            context = {
                "job_id": job_id,
                "tutor_list": user_contents
            }

            dialog_content = TemplateLoader.load_template(self.template_name, context)
            return dialog_content
        except Exception as exp:
            return ""

    def render_to_response(self, context, **response_kwargs):
        #response = super(SearchTutorListAjaxView, self).render_to_response(context, response_kwargs)
        content = self.get_context_data()
        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": content
        }
        return self.render_to_json(response)






