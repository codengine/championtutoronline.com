__author__ = 'codengine'

from enum import Enum

class UserTypes(Enum):
    student = "student"
    teacher = "teacher"
    stuff = "stuff"

class VerificationStatus(Enum):
    not_verified = 0
    verified = 1
    under_review = 2
    rejected = 3
    submitted = 4

class ChatType(Enum):
    p2p = 0
    group = 1
    job_application = 3

class ReadStatus(Enum):
    unread = 0
    read = 1

class OnlineStatus(Enum):
    offline = 0
    online = 1
    idle = 2

class TransactionStatus(Enum):
    approved = "approved"
    declined = "declined"
    not_initiated = "not_initiated"

class TransactionType(Enum):
    sale = "SALE"
    withdraw = "WITHDRAW"
    refund = "REFUND"
    refund2 = "REFUND2"

class ActiveStatus(Enum):
    active = 1
    inactive = 0

class LoationType(Enum):
    country = "country"
    city = "city"
    state = "state"

class Gender(Enum):
    male = "male"
    female = "female"

class AvailableStatus(Enum):
    unavailable=0
    available = 1
    ready_to_teach = 2
    ready_for_written_lesson = 3
    ready_for_both = 4
    any = 5
    profile_deactivated = 6

class OnlineStatus(Enum):
    offline = 0
    online = 1

class LessonTypes(Enum):
    live_lesson = 0
    written = 1

class ApprovalStatus(Enum):
    not_approved = 0
    approved = 1
    rejected = 2
    cancelled = 3

class LessonStatus(Enum):
    started = 0
    approved = 1
    cancelled = 2
    rejected = 3
    completed = 4

class YesNo(Enum):
    no = 0
    yes = 1

class NotificationClass(Enum):
    MESSAGE = "message"
    WRITTEN_LESSON = "written_lesson"
    LIVE_LESSON = "live_lesson"

class WhiteboardAccess(Enum):
    protected = "access_protected" ###Used in lesson
    private = "access_private"   ###Used in a demo lesson when the user logged in
    public = "access_public"   ###Used in a demo lesson when user not logged in

class JobInvitationStatus(Enum):
    active = 1
    applied = 2
    cancelled = 3
    accepted = 4
    rejected = 5
    negotiating = 6
    offered = 7
    hired = 8
    shortlisted = 9
    rejected_application = 10
    rejected_offer = 12
    application_withdrawn = 13
    recommendation_rejected = 14

class JobStatus(Enum):
    job_open = 1
    job_closed = 2
    job_suspended = 3
    job_deleted = 4

class QuestionStatus(Enum):
    open = 1
    close = 2
    answered = 3

class JobRecommendationStatus(Enum):
    active = 1
    applied = 2
    cancelled = 3
    rejected = 5
    negotiating = 6
    offered = 7
    hired = 8
    shortlisted = 9
    rejected_application = 10
    rejected_offer = 12
    application_withdrawn = 13
    recommendation_rejected = 14

class JobApplicationStatus(Enum):
    active = 1
    applied = 2
    cancelled = 3
    rejected = 5
    negotiating = 6
    offered = 7
    hired = 8
    shortlisted = 9
    rejected_application = 10
    rejected_offer = 12
    application_withdrawn = 13
    recommendation_rejected = 14

class JobOfferStatus(Enum):
    active = 1
    hired = 8
    rejected_offer = 12
    application_withdrawn = 13

class PriceSettlementStatus(Enum):
    Created = 0
    Waiting = 1
    Accepted = 2
    AcceptedAndOffered = 3
    Discuss = 4
    Rejected = 5
    OfferAccepted = 6
    OfferRejected = 7
    RequestCancel = 8
    OfferCancel = 9
    WithdrawApplication = 10
    RejectedApplication = 11

class LanguageProficiency:
    below_average = 1
    average = 2
    fluent = 3
