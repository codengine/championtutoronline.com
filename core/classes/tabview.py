__author__ = 'Sohel'

class TabView(object):
    title = ""
    url = ""
    queryset = None

    def __init__(self, title, url, queryset=None):
        super(TabView,self).__init__()
        self.title = title
        self.url = url
        self.queryset = queryset
