from decimal import Decimal
from django.core.paginator import Paginator
from django.db import transaction
from django.db.models.aggregates import Count
from django.http import Http404
from django.http.response import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.utils.safestring import mark_safe
from core.library.country_util import CountryUtil
from core.library.currency_util import CurrencyUtil
from core.library.job_util import JobUtil
from core.library.rate_conversion_util import RateConversionUtil
from core.library.subject_util import SubjectUtil
from core.models3 import ChampUser
from easy_thumbnails.files import get_thumbnailer
from common.channels import CHANNEL
from common.methods import get_buddies_online
from core.generics.mixins import LoginRequiredMixin
from core.generics.redis_client import RedisClient
from core.library.Clock import Clock
from core.library.email_scheduler import EmailScheduler
from core.library.email_template_util import EmailTemplateUtil
from core.library.encryption_manager import EncryptDecryptManager
from core.library.lesson_util import LessonUtil
from core.library.user_util import UserUtil
from django_bootstrap_calendar.models import CalendarEvent
from engine.config.servers import SHAREJS_SERVER_HOST, SHAREJS_SERVER_PORT
from notification.utils.notification_manager import NotificationManager
from payment.models import PaymentMethod
from payment.payment_processor import PaymentProcessor

__author__ = 'Codengine'

from django.views.generic.base import View, TemplateView
from django.shortcuts import render
from django.http import HttpResponse
from forms import SignUpForm,LoginForm,MyAccountForm, StudentPrivateInfoForm, StudentEducationForm, LessonRequestForm, \
    JobPostForm
import otlib
from pyetherpad.padutil import *
from pysharejs.CodePadUtil import *
from django.contrib.auth import authenticate, login
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from core.generics.ajaxmixins import JSONResponseMixin
import os
from django.core.files import File
import json
from core.decorators import champ_login_required
from django.template import loader, Context
import base64
import uuid
from core.models3 import *
import hashlib

class DrawingBoardAjaxView(JSONResponseMixin,View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(DrawingBoardAjaxView, self).dispatch(*args, **kwargs)

    def get(self,request,*args,**kwargs):
        if request.is_ajax():
            lesson_id = int(request.GET.get("lesson_id"))
            live_lessons = LiveLesson.objects.filter(pk=lesson_id)
            whiteboard = None
            live_lesson = None
            if live_lessons.exists():
                live_lesson = live_lessons.first()
                whiteboard = live_lesson.whiteboard
            else:
                if request.user.is_authenticated():
                    whiteboard_objs = DemoWhiteboard.objects.filter(email_or_session_key=request.user.email)
                    if whiteboard_objs.exists():
                        whiteboard = whiteboard_objs[0].whiteboard
                else:
                    whiteboard_objs = DemoWhiteboard.objects.filter(email_or_session_key=request.session.session_key)
                    if whiteboard_objs.exists():
                        whiteboard = whiteboard_objs[0].whiteboard

            context_data = {}
            context_data["drawingboards"] = whiteboard.drawingboards.all()

            sticky_notes = whiteboard.sticky_notes.all()
            if request.user.is_authenticated() and live_lesson:
                sticky_notes = sticky_notes.filter(user_id=request.user.pk)

            context_data["sticky_notes"] = sticky_notes

            context_data["live_lesson"] = live_lesson

            context_data["whiteboard"] = whiteboard

            lesson_payment_status = (live_lesson.payment_status == 1) if live_lesson else False
            context_data["payment_status"] = lesson_payment_status

            if lesson_id == -1:
                context_data["demo"] = True
            else:
                context_data["lesson_status"] = live_lesson.lesson_status

            return render(request,"ajax/drawingboard.html",context_data)
        else:
            return HttpResponse("Invalid Request")
    def post(self,request,*args,**kwargs):
        if request.is_ajax():
            whiteboard_id = request.POST.get("whiteboard_id")
            drawingboard_id = request.POST.get("board_id")
            action = request.POST.get("action", "save_board")
            data = request.POST.get("data")
            if action == "save_board":
                try:
                    whiteboard_id = int(whiteboard_id)
                    drawingboard_id = int(drawingboard_id.replace("drawing_board_",""))
                    whiteboard = Whiteboard.objects.filter(pk=whiteboard_id).first()
                except:
                    whiteboard = None
                    drawingboard_id = None
                if not whiteboard or not drawingboard_id or not data:
                    response = {
                        "status": "FAILURE",
                        "message": "Missing data",
                        "data": []
                    }
                    return self.render_to_json(response)
                else:
                    drawing_board_objects = whiteboard.drawingboards.filter(numeric_id=drawingboard_id)
                    if drawing_board_objects.exists():
                        drawingboard_object = drawing_board_objects.first()
                        drawingboard_object.drawing_data = data

                        _type = request.POST.get("type")
                        if _type == "clear":
                            drawingboard_object.type = "grid"
                            drawingboard_object.background = "graphno"

                        drawingboard_object.save()
                        response = {
                            "status": "SUCCESS",
                            "message": "Successful",
                            "data": []
                        }
                        return self.render_to_json(response)
                    else:
                        response = {
                            "status": "FAILURE",
                            "message": "Invalid drawingboard id",
                            "data": []
                        }
                        return self.render_to_json(response)
            elif action == "save_background":
                _type = request.POST.get("type")
                try:
                    whiteboard_id = int(whiteboard_id)
                    drawingboard_id = int(drawingboard_id.replace("drawing_board_",""))
                    whiteboard = Whiteboard.objects.filter(pk=whiteboard_id).first()
                except:
                    whiteboard = None
                    drawingboard_id = None
                if not whiteboard or not drawingboard_id or not data or not _type:
                    response = {
                        "status": "FAILURE",
                        "message": "Missing data",
                        "data": []
                    }
                    return self.render_to_json(response)
                else:
                    drawing_board_objects = whiteboard.drawingboards.filter(numeric_id=drawingboard_id)
                    if drawing_board_objects.exists():
                        drawingboard_object = drawing_board_objects.first()
                        drawingboard_object.background = data
                        drawingboard_object.type = _type
                        drawingboard_object.save()
                        response = {
                            "status": "SUCCESS",
                            "message": "Successful",
                            "data": []
                        }
                        return self.render_to_json(response)
                    else:
                        response = {
                            "status": "FAILURE",
                            "message": "Invalid drawingboard id",
                            "data": []
                        }
                        return self.render_to_json(response)
        else:
            response = {
                "status": "FAILURE",
                "message": "Invalid request",
                "data": []
            }
            return self.render_to_json(response)

class DrawingBoardDataAjaxView(JSONResponseMixin,View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(DrawingBoardDataAjaxView, self).dispatch(*args, **kwargs)

    def get(self,request,*args,**kwargs):
        if request.is_ajax():
            whiteboard_id = request.GET.get("whiteboard_id")
            board_id = request.GET.get("board_id")
            try:
                whiteboard_id = int(whiteboard_id)
                whiteboard = Whiteboard.objects.get(pk=whiteboard_id)
                board_id = int(board_id.replace("drawing_board_",""))
            except:
                whiteboard = None
                board_id = None

            if whiteboard and board_id:
                drawing_board_objects = whiteboard.drawingboards.filter(numeric_id=board_id)
                if drawing_board_objects.exists():
                    data = drawing_board_objects.first().drawing_data
                    response = {
                        "status": "SUCCESS",
                        "message": "Successful",
                        "data": data
                    }
                    return self.render_to_json(response)
                else:
                    response = {
                        "status": "FAILURE",
                        "message": "Invalid board id",
                        "data": []
                    }
                    return self.render_to_json(response)
            else:
                response = {
                    "status": "FAILURE",
                    "message": "Missing data",
                    "data": []
                }
                return self.render_to_json(response)

        else:
            response = {
                "status": "FAILURE",
                "message": "Invalid request",
                "data": []
            }
            return self.render_to_json(response)


class TextEditorAjaxView(View):
    def get(self,request,*args,**kwargs):
        if request.is_ajax():
            return render(request,"ajax/text_editor.html",{})
        else:
            return HttpResponse("Invalid Request")

class CodeEditorAjaxView(View):
    def get(self,request,*args,**kwargs):
        if request.is_ajax():
            return render(request,"ajax/code_editor.html",{})
        else:
            return HttpResponse("Invalid Request")

class LoginAjaxView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(LoginAjaxView, self).dispatch(*args, **kwargs)

    def get(self,request,*args,**kwargs):
        login_form = LoginForm()
        return render(request,"ajax/login.html",{'form':login_form})
    def post(self,request,*args,**kwargs):
        response = {'status':'unsuccessful','message':''}
        if not request.is_ajax():
            response = {'status':'unsuccessful','message':'Invalid Request.'}
            return HttpResponse(json.dumps(response))
        login_form = LoginForm(request.POST)
        if not login_form.is_valid():
            response = {'status':'unsuccessful','message':'Form contains invalid data.'}
            return HttpResponse(json.dumps(response))
        if login_form.is_valid():
            if not login_form.authenticate(request):
                response = {'status':'unsuccessful','message':'Email or Password Invalid.'}
                return HttpResponse(json.dumps(response))
        ###Passed login. Now set session
        request.session['is_login'] = True
        request.session['user_id'] = request.user.id
        request.session['email'] = request.user.email
        #request.session['utype'] = request.user.type

        response = {'status':'successful','message':'Login Successful.'}
        return HttpResponse(json.dumps(response))

class SignUpAjaxView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(SignUpAjaxView, self).dispatch(*args, **kwargs)

    def get(self,request,*args,**kwargs):
        signup_form = SignUpForm()
        return render(request,"ajax/registration.html",{'form':signup_form})

    def post(self,request,*args,**kwargs):
        response = {'status':'unsuccessful','message':''}
        if not request.is_ajax():
            response = {'status':'unsuccessful','message':'Invalid Request.'}
            return HttpResponse(json.dumps(response))
        try:

            with transaction.atomic():
                form_data = request.POST

                if request.POST.get("social_signup") == "true":
                    form_data = {
                        "signup_role": request.POST.get("social_signup_role"),
                        "signup_name": request.POST.get("social_signup_name"),
                        "signup_surname": request.POST.get("social_signup_surname"),
                        "signup_email": request.POST.get("social_signup_email"),
                        "signup_password": request.POST.get("social_signup_password"),
                        "signup_password_repeat": request.POST.get("social_signup_password_repeat"),
                        "signup_timezones": request.POST.get("social_signup_timezones")

                    }

                signup_form = SignUpForm(form_data)

                if not signup_form.is_valid():
                    response = {'status':'unsuccessful','message':'Form contains invalid data.'}
                    return HttpResponse(json.dumps(response))

                if request.POST.get("social_signup") == "true":
                    name = request.POST.get("social_signup_name")
                    surname = request.POST.get("social_signup_surname")
                    email = request.POST.get("social_signup_email")
                    password = request.POST.get("social_signup_password")
                    pass_repeat = request.POST.get("social_signup_password_repeat")
                    role = request.POST.get("social_signup_role")
                    timezone = request.POST.get("social_signup_timezones")
                    facebook_id = request.POST.get("u_u")
                    import base64
                    facebook_id = base64.b64decode(facebook_id)
                    facebook_id = base64.b64decode(facebook_id)
                else:
                    name = request.POST.get("signup_name")
                    surname = request.POST.get("signup_surname")
                    email = request.POST.get("signup_email")
                    password = request.POST.get("signup_password")
                    pass_repeat = request.POST.get("signup_password_repeat")
                    role = request.POST.get("signup_role")
                    timezone = request.POST.get("signup_timezones")

                ###Check if email is already registered.
                if User.objects.filter(email=email).exists():
                    response['message'] = 'EMAIL_REGISTERED'
                    return HttpResponse(json.dumps(response))
                user_obj = ChampUser()
                user_obj.fullname = name
                user_obj.lastname = surname

                if request.POST.get("social_signup") == "true" and facebook_id:
                    em = EncryptDecryptManager(settings.CIPHER_KEY)
                    user_obj.facebook_id = em.decrypt(facebook_id)

                wallet = Wallet()
                wallet.save()
                user_obj.wallet_id = wallet.pk

                if Role.objects.filter(name=role).exists():
                    role_obj = Role.objects.get(name=role)
                else:
                    role_obj = Role(name=role)
                    role_obj.save()

                auth_user = User.objects.create_user(username=email, email=email, password=password)
                if request.POST.get("social_signup") == "true":
                    auth_user.is_active = True
                else:
                    auth_user.is_active = False
                auth_user.save()
                user_obj.user = auth_user
                user_obj.type = role_obj

                tz_mapper_object = TimezoneMapper.objects.get(pk=int(timezone))

                timezone = tz_mapper_object.tz

                user_obj.timezone = timezone

                user_obj.nationality_id = tz_mapper_object.country_id

                user_obj.activation_code = hashlib.sha512(str(auth_user.pk)).hexdigest()
                user_obj.save()

                response['status'] = 'successful'
                response['message'] = 'Successful.'

                if request.POST.get("social_signup") == "true":
                    user_object = authenticate(email=email,facebook_id=user_obj.facebook_id)
                    if user_object is not None:
                        login(request, user_object)
                        seven_days = 24*60*60*7
                        request.session.set_expiry(seven_days)
                        request.session['is_login'] = True
                        request.session['user_id'] = request.user.pk
                        request.session['email'] = request.user.email
                        response["data"] = {
                            "redirect": "true",
                            "url": request.user.champuser.get_login_success_url()
                        }
                        return HttpResponse(json.dumps(response))




                signup_email_html, text = EmailTemplateUtil.render_signup_email(name,user_obj.activation_code)

                email_object = {
                    "recipients": [email],
                    'subject': 'Registration Confirmation',
                    'html_body': signup_email_html,
                    'text_body': text
                }
                EmailScheduler.place_to_queue(email_object)
                return HttpResponse(json.dumps(response))
        except Exception as msg:
            response['status'] = 'Unsuccessful.'
            response['message'] = 'Couldn\'t sign up. A Server error occured.'
            return HttpResponse(json.dumps(response))

class VideoSessionTokens(JSONResponseMixin,View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(VideoSessionTokens, self).dispatch(*args, **kwargs)

    def post(self,request,*args,**kwargs):
        if request.is_ajax():
            ###Now read the ot session from ot_session table and then request a token id.
            ##printrequest.GET.get("uids[]")
            uid = request.POST.get("uid")
            otsession = request.POST.get("ot_session")
            lesson_id = request.POST.get("lesson_id")
            whiteboard_id = request.POST.get("whiteboard_id")

            live_lessons = LiveLesson.objects.filter(pk=int(lesson_id))

            if not live_lessons.exists():
                response = {
                    "status": "Failure",
                    "message": "Live lesson does not exist",
                    "data": []
                }
                return self.render_to_json(response)

            if not request.user.is_authenticated():
                response = {
                    "status": "Failure",
                    "message": "Unauthenticated user access.",
                    "data": []
                }
                return self.render_to_json(response)

            live_lesson = live_lessons.first()
            if live_lesson.whiteboard_id != int(whiteboard_id):
                response = {
                    "status": "Failure",
                    "message": "Invalid access",
                    "data": []
                }
                return self.render_to_json(response)

            if not live_lesson.user.pk != request.user.pk and not live_lesson.request_user.pk != request.user.pk and not live_lesson.other_users.filter(pk=int(uid)).exists():
                response = {
                    "status": "Failure",
                    "message": "Unauthorized access.",
                    "data": []
                }
                return self.render_to_json(response)

            whiteboard_object = live_lesson.whiteboard
            if not whiteboard_object.video_chat:
                session_id = otlib.create_ot_session()
                video_chat = VideoChat()
                video_chat.ot_session = session_id
                video_chat.save()
                whiteboard_object.video_chat_id = video_chat.pk
                whiteboard_object.save()

            if whiteboard_object.video_chat and not whiteboard_object.video_chat.tokens.filter(email_or_session=request.user.email).exists():
                token = otlib.generate_ot_token(int(uid),otsession=whiteboard_object.video_chat.ot_session)
                video_chat = whiteboard_object.video_chat

                uottoken = UserOTToken()
                uottoken.email_or_session = request.user.email
                uottoken.token = token
                uottoken.save()
                video_chat.tokens.add(uottoken)
            else:
                request_new_token = request.POST.get("request_new_token")
                if request_new_token == "1":
                    if whiteboard_object.video_chat and whiteboard_object.video_chat.tokens.filter(email_or_session=request.user.email).exists():
                        ottoken_ = whiteboard_object.video_chat.tokens.filter(email_or_session=request.user.email).first()
                        whiteboard_object.video_chat.tokens.remove(ottoken_)
                        ottoken_.delete()
                    token = otlib.generate_ot_token(int(uid),otsession=whiteboard_object.video_chat.ot_session)
                    video_chat = whiteboard_object.video_chat

                    uottoken = UserOTToken()
                    uottoken.email_or_session = request.user.email
                    uottoken.token = token
                    uottoken.save()
                    video_chat.tokens.add(uottoken)

            data = {
                "lesson_type": "LIVE_LESSON",
                "otsession": whiteboard_object.video_chat.ot_session,
                "ot_token": whiteboard_object.video_chat.tokens.filter(email_or_session=request.user.email).first().token,
                "ot_api_key": otlib.read_ot_api_key()
            }
            response = {
                "status": "SUCCESS",
                "message": "Successful",
                "data": data
            }

            return self.render_to_json(response)

        else:
            response = {
                "status": "Failure",
                "message": "Invalid request.",
                "data": []
            }
            return self.render_to_json(response)

class VideoSessionStart(View):
    def get(self,request,*args,**kwargs):
        api_key = settings.OT_API_KEY
        otsession = request.GET.get("_ots")
        token = request.GET.get("_token")
        data = {
            "OT_API_KEY": api_key,
            "OT_SESSION": otsession,
            "OT_TOKEN": token
        }
        return render(request,"ajax/videoframe.html",data)

class WhiteboardAjaxView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(WhiteboardAjaxView, self).dispatch(*args, **kwargs)

    def get(self,request,*args,**kwargs):
        response = {
            "STATUS": "",
            "MESSAGE": ""
        }
        if request.is_ajax():
            ###Get current user's whiteboard instance.
            action = request.GET.get("action")
            if action == "ADD_DRAWING_BOARD":
                ###Get the last numeric id.

                lesson_id = request.GET.get('lesson_id')
                whiteboard = None
                group_id = None
                live_lesons = LiveLesson.objects.filter(pk=int(lesson_id))
                target_users = []
                if live_lesons.exists():
                    live_lesson = live_lesons.first()
                    whiteboard = live_lesson.whiteboard
                    group_id = str(live_lesons.first().pk)
                    if live_lesson.user.pk != request.user.pk:
                        target_users += [ live_lesson.user.pk ]
                    if live_lesson.request_user.pk != request.user.pk:
                        target_users += [ live_lesson.request_user.pk ]
                    for _user in live_lesson.other_users.all():
                        if _user.pk != request.user.pk:
                            target_users += [ _user.pk ]
                else:
                    if request.user.is_authenticated():
                        whiteboard = DemoWhiteboard.objects.get(email_or_session_key=request.user.email).whiteboard
                        group_id = request.user.email
                    else:
                        whiteboard = DemoWhiteboard.objects.get(email_or_session_key=request.session.session_key).whiteboard
                        group_id = request.session.session_key

                if whiteboard:
                    dblastnid = DrawingboardLastNid.objects.get(group=group_id)
                    drawingboard = DrawingBoard()
                    drawingboard.name = str(dblastnid.nid + 1)
                    drawingboard.numeric_id = dblastnid.nid + 1
                    drawingboard.save()
                    dblastnid.nid = dblastnid.nid + 1
                    dblastnid.save()
                    whiteboard.drawingboards.add(drawingboard)

                    ###Now publish the status with data in MESSAGE_SENT signal.
                    try:
                        redis_client = RedisClient.Instance()
                        publish_message = {
                            "receivers" : target_users ,
                            "data": {
                                "title":dblastnid.nid,
                                "id": "id_canvas_tab_"+str(dblastnid.nid),
                                "closable": "false"
                            }
                        }
                        publish_message["CHANNEL"] = CHANNEL["notify_drawingboard_added"]
                        redis_client.publish_channel(CHANNEL["notify_drawingboard_added"], json.dumps(publish_message))
                    except Exception as msg:
                        pass

                response["STATUS"] = "SUCCESS"
                response["MESSAGE"] = "Successful."
                response["data"] = {"tab_count": dblastnid.nid }
                return HttpResponse(json.dumps(response))
            elif action == "UPDATE_DRAWING_BOARD":
                ###Get the last numeric id.
                dboard_id = request.GET.get("dbid")
                if not dboard_id:
                    response["STATUS"] = "FAILURE"
                    response["MESSAGE"] = "id must be given."
                    return HttpResponse(json.dumps(response))

                try:
                    dboard_id = int(dboard_id)
                except Exception as msg:
                    response["STATUS"] = "FAILURE"
                    response["MESSAGE"] = "id must be numeric."
                    return HttpResponse(json.dumps(response))

                name = request.GET.get("name")
                if not name:
                    response["STATUS"] = "FAILURE"
                    response["MESSAGE"] = "Name must be given."
                    return HttpResponse(json.dumps(response))

                drawingboard_obj = DrawingBoard.objects.get(id=dboard_id)
                drawingboard_obj.name = name
                drawingboard_obj.save()

                response["STATUS"] = "SUCCESS"
                response["MESSAGE"] = "Successful."
                return HttpResponse(json.dumps(response))
            elif action == "ADD_NEW_TEXT_PAD":
                lesson_id = request.GET.get('lesson_id')
                whiteboard = None
                pad_key = None
                live_lesons = LiveLesson.objects.filter(pk=int(lesson_id))
                demo_pad = True
                user_full_name = ""
                user_id = None
                author_id = None
                target_users = []
                if live_lesons.exists():
                    live_lesson = live_lesons.first()
                    whiteboard = live_lesons.first().whiteboard
                    pad_key = str(live_lesons.first().pk)
                    demo_pad = False
                    user_full_name = request.user.champuser.fullname
                    author_id = request.user.pk
                    user_id = live_lesons.first().user.pk

                    if live_lesson.user.pk != request.user.pk:
                        target_users += [ live_lesson.user.pk ]
                    if live_lesson.request_user.pk != request.user.pk:
                        target_users += [ live_lesson.request_user.pk ]
                    for _user in live_lesson.other_users.all():
                        if _user.pk != request.user.pk:
                            target_users += [ _user.pk ]

                else:
                    if request.user.is_authenticated():
                        whiteboard = DemoWhiteboard.objects.get(email_or_session_key=request.user.email).whiteboard
                        pad_key = request.user.email
                        author_id = request.user.pk
                        user_id = request.user.pk
                    else:
                        whiteboard = DemoWhiteboard.objects.get(email_or_session_key=request.session.session_key).whiteboard
                        pad_key = request.session.session_key
                        author_id = request.session.session_key

                if whiteboard:
                    pad_util = PadUtil()
                    lesson_requested_by_author_id = pad_util.create_or_get_padauthor(author_id,user_full_name)
                    pad_group_id = pad_util.create_or_get_padgroup(pad_key,user_id,demo_pad)
                    pad_id = pad_util.create_group_pad(pad_group_id,pad_name=str(uuid.uuid4()),user_id=user_id,author_id=lesson_requested_by_author_id)
                    pad_object = Pad.objects.get(pad_id=pad_id)
                    pad_util.make_pad_public(pad_id)
                    whiteboard.text_pads.add(pad_object)

                    ###Now publish the status with data in MESSAGE_SENT signal.
                    try:
                        redis_client = RedisClient.Instance()
                        publish_message = {
                            "receivers" : target_users ,
                            "data": {
                                "title":pad_object.pad_nid,
                                "pad_id": str(pad_object.pad_id),
                                "id": "id_pad_tab_"+str(pad_object.pad_nid),
                                "closable": "false"
                            }
                        }
                        publish_message["CHANNEL"] = CHANNEL["notify_text_pad_added"]
                        redis_client.publish_channel(CHANNEL["notify_text_pad_added"], json.dumps(publish_message))
                    except Exception as msg:
                        pass

                response["STATUS"] = "SUCCESS"
                response["MESSAGE"] = "Successful."
                response["data"] = {"pad_nid": pad_object.pad_nid,"pad_id": pad_object.pad_id,"id": pad_object.id}
                return HttpResponse(json.dumps(response))

            elif action == "EDIT_TEXT_PAD":
                pad_name = request.GET.get("name")
                if not pad_name:
                    pad_name = "New Pad"
                pad_id = request.GET.get("pad_id")
                if not pad_id:
                    response["STATUS"] = "FAILURE"
                    response["MESSAGE"] = "Pad ID required."
                    return HttpResponse(json.dumps(response))
                pad_util = PadUtil()
                op_mode = pad_util.rename_group_pad(pad_id,pad_name)
                if op_mode is True:
                    response["STATUS"] = "SUCCESS"
                    response["MESSAGE"] = "Successful."
                    return HttpResponse(json.dumps(response))
                else:
                    response["STATUS"] = "FAILURE"
                    response["MESSAGE"] = "Pad ID doesn't exists."
                    return HttpResponse(json.dumps(response))

            elif action == "CHANGE_PAD_LANGUAGE":
                pad_id = request.GET.get("pad_id")
                language = request.GET.get("language")
                try:

                    target_users = []
                    lesson_id = request.GET.get('lesson_id')
                    live_lesons = LiveLesson.objects.filter(pk=int(lesson_id))
                    if live_lesons.exists():
                        live_lesson = live_lesons.first()
                        if live_lesson.user.pk != request.user.pk:
                            target_users += [ live_lesson.user.pk ]
                        if live_lesson.request_user.pk != request.user.pk:
                            target_users += [ live_lesson.request_user.pk ]
                        for _user in live_lesson.other_users.all():
                            if _user.pk != request.user.pk:
                                target_users += [ _user.pk ]

                    code_pad = CodePad.objects.get(pad_id=pad_id)
                    code_pad.pad_language = language
                    code_pad.save()

                    try:
                        redis_client = RedisClient.Instance()
                        publish_message = {
                            "receivers" : target_users ,
                            "data": {
                                "title":code_pad.pad_nid,
                                "pad_id": str(code_pad.pad_id),
                                "language": code_pad.pad_language,
                                "id": "id_codepad_tab_"+str(code_pad.pad_nid),
                                "closable": "false"
                            }
                        }
                        publish_message["CHANNEL"] = CHANNEL["notify_code_pad_lan_added"]
                        redis_client.publish_channel(CHANNEL["notify_code_pad_lan_added"], json.dumps(publish_message))
                    except Exception as msg:
                        print(msg)

                    response["status"] = "SUCCESS"
                    response["message"] = "Successful."
                    response["data"] = {
                        "url": SHAREJS_SERVER_HOST + ":" + str(SHAREJS_SERVER_PORT) + "/pad/pad.html?id=" + pad_id + "&lan=" + language
                    }
                    return HttpResponse(json.dumps(response))
                except:
                    response["status"] = "FAILURE"
                    response["message"] = "Pad language update failed."
                    return HttpResponse(json.dumps(response))



            elif action == "ADD_NEW_CODE_PAD":
                name = request.GET.get("name")
                lan = request.GET.get("lan")
                creator = request.user
                lesson_id = request.GET.get('lesson_id')
                whiteboard = None
                live_lesons = LiveLesson.objects.filter(pk=int(lesson_id))
                demo_pad = True
                group_id = None
                user_full_name = ""
                creator = None
                target_users = []
                if live_lesons.exists():
                    live_lesson = live_lesons.first()
                    whiteboard = live_lesons.first().whiteboard
                    creator = live_lesons.first().user
                    group_id = str(live_lesons.first().pk)

                    if live_lesson.user.pk != request.user.pk:
                        target_users += [ live_lesson.user.pk ]
                    if live_lesson.request_user.pk != request.user.pk:
                        target_users += [ live_lesson.request_user.pk ]
                    for _user in live_lesson.other_users.all():
                        if _user.pk != request.user.pk:
                            target_users += [ _user.pk ]
                else:
                    if request.user.is_authenticated():
                        whiteboard = DemoWhiteboard.objects.get(email_or_session_key=request.user.email).whiteboard
                        creator = request.user
                        group_id = request.user.email
                    else:
                        whiteboard = DemoWhiteboard.objects.get(email_or_session_key=request.session.session_key).whiteboard
                        group_id = request.session.session_key

                if whiteboard:
                    codepad_util = CodePadUtil()
                    code_pad_obj = codepad_util.create_codepad(group_id,lan,creator=creator)

                    whiteboard.code_pads.add(code_pad_obj)

                    ###Now publish the status with data in MESSAGE_SENT signal.
                    try:
                        redis_client = RedisClient.Instance()
                        publish_message = {
                            "receivers" : target_users ,
                            "data": {
                                "title":code_pad_obj.pad_nid,
                                "pad_id": str(code_pad_obj.pad_id),
                                "language": code_pad_obj.pad_language,
                                "id": "id_codepad_tab_"+str(code_pad_obj.pad_nid),
                                "closable": "false"
                            }
                        }
                        publish_message["CHANNEL"] = CHANNEL["notify_code_pad_added"]
                        redis_client.publish_channel(CHANNEL["notify_code_pad_added"], json.dumps(publish_message))
                    except Exception as msg:
                        pass

                    response["STATUS"] = "SUCCESS"
                    response["MESSAGE"] = "Successful."
                    response["data"] = { "pad_id": str(code_pad_obj.pad_id), "pad_language": code_pad_obj.pad_language, "pad_nid": code_pad_obj.pad_nid, "id": code_pad_obj.id, "pad_name": code_pad_obj.pad_name }
                    return HttpResponse(json.dumps(response))
            elif action == "LOAD_DB_BACKGROUND":
                wb_id = request.GET.get('wb_id')
                db_id = request.GET.get('db_id')
                try:
                    wb_id = int(wb_id)
                    db_id = db_id.replace("drawing_board_","")
                    db_id = int(db_id)
                    whiteboard = Whiteboard.objects.get(pk=wb_id)
                    drawingboard = whiteboard.drawingboards.filter(numeric_id=db_id).first()
                    background = drawingboard.background
                    if drawingboard.type == "image":
                        background = request.META['HTTP_HOST']+background
                        if request.is_secure():
                            protocol = 'https'
                        else:
                            protocol = 'http'
                        background = protocol+'://'+background
                    response = {}
                    response["status"] = "SUCCESS"
                    response["message"] = "Successful"
                    response["data"] = { "type": drawingboard.type, "background": background }
                    return HttpResponse(json.dumps(response))
                except:
                    response = {}
                    response["STATUS"] = "FAILURE"
                    response["MESSAGE"] = "Failed"
                    return HttpResponse(json.dumps(response))
            else:
                response["STATUS"] = "FAILURE"
                response["MESSAGE"] = "Invalid operation specified."
                return HttpResponse(json.dumps(response))

        else:
            response["STATUS"] = "FAILURE"
            response["MESSAGE"] = "Invalid Operation."
            return HttpResponse(json.dumps(response))

    def post(self,request,*args,**kwargs):
        response = {
            "status": "",
            "message": ""
        }
        if request.is_ajax():

            attachment = request.FILES.get("uploaded_file")

            if attachment:
                file_name = str(request.user.id)+"-"+str(uuid.uuid4())+"."+attachment._name[attachment._name.rindex(".")+1:]
                attachment_original_name = attachment
                with open(os.path.join(settings.DRAWINGBOARD_IMAGE_DIR,file_name), 'wb+') as destination:
                    for chunk in attachment.chunks():
                        destination.write(chunk)

            response["status"] = "SUCCESS"
            response["message"] = "Successful"
            response["data"] = {
                "url": os.path.join(settings.DRAWINGBOARD_IMAGE_URL,file_name)
            }
            return HttpResponse(json.dumps(response))
        else:
            response["status"] = "FAILURE"
            response["message"] = "Invalid Operation."
            return HttpResponse(json.dumps(response))

class WhiteboardVideoSetingsAjaxView(JSONResponseMixin,View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(WhiteboardVideoSetingsAjaxView, self).dispatch(*args, **kwargs)

    def update_settings(self,settings_name, settings_value, email_or_session_key_value,whiteboard_id):
        whiteboard_object = Whiteboard.objects.get(pk=int(whiteboard_id))
        user_settings_objects = None
        if whiteboard_object.video_chat:
            user_settings_objects = whiteboard_object.video_chat.user_settings.filter(email_or_session_key=email_or_session_key_value)
        if user_settings_objects and not user_settings_objects.exists():
            video_chat_settings = VideoChatSettings()
            video_chat_settings.audio_enabled = 0
            video_chat_settings.video_enabled = 0
            video_chat_settings.sharing_enabled = 0
            video_chat_settings.save()
            user_video_chat_settings = UserVideoChatSettings()
            user_video_chat_settings.email_or_session_key = email_or_session_key_value
            user_video_chat_settings.settings = video_chat_settings
            user_video_chat_settings.save()
            whiteboard_object.video_chat.user_settings.add(user_video_chat_settings)

        if settings_name == "AUDIO":
            setting_object = whiteboard_object.video_chat.user_settings.get(email_or_session_key=email_or_session_key_value).settings
            setting_object.audio_enabled = int(settings_value)
            setting_object.save()
        elif settings_name == "VIDEO":
            if int(settings_value) == 1:
                setting_object = whiteboard_object.video_chat.user_settings.get(email_or_session_key=email_or_session_key_value).settings
                setting_object.audio_enabled = int(settings_value)
                setting_object.video_enabled = int(settings_value)
                setting_object.save()
            else:
                setting_object = whiteboard_object.video_chat.user_settings.get(email_or_session_key=email_or_session_key_value).settings
                setting_object.video_enabled = int(settings_value)
                setting_object.save()
        elif settings_name == "ALL":
            setting_object = whiteboard_object.video_chat.user_settings.get(email_or_session_key=email_or_session_key_value).settings
            settings_value = int(settings_value)
            if settings_value == 1:
                setting_object.video_enabled = 1
                setting_object.audio_enabled = 1
            else:
                setting_object.video_enabled = 0
                setting_object.audio_enabled = 0
            setting_object.save()


    def post(self,request,*args,**kwargs):
        response = {
            "STATUS": "",
            "MESSAGE": ""
        }
        if request.is_ajax():
            user_id = request.POST.get("user_id")
            whiteboard_id = request.POST.get("wb_id")
            settings_name = request.POST.get("name")
            settings_value = request.POST.get("settings_value")

            if not Whiteboard.objects.filter(pk=int(whiteboard_id)).exists():
                response["STATUS"] = "FAILURE"
                response["MESSAGE"] = "Invalid whiteboard id."
                return self.render_to_json(response)

            if request.user.is_authenticated():
                if request.user.pk == int(user_id):
                    self.update_settings(settings_name, settings_value, request.user.email, whiteboard_id)
                    response["STATUS"] = "SUCCESS"
                    response["MESSAGE"] = "Successful"
                    return self.render_to_json(response)
                else:
                    response["STATUS"] = "FAILURE"
                    response["MESSAGE"] = "Unauthorized access."
                    return self.render_to_json(response)
            else:
                self.update_settings(settings_name, settings_value, request.session.session_key, whiteboard_id)
                response["STATUS"] = "SUCCESS"
                response["MESSAGE"] = "Successful"
                return self.render_to_json(response)
        else:
            response["STATUS"] = "FAILURE"
            response["MESSAGE"] = "Invalid Operation."
            return self.render_to_json(response)

class ProcessPaymentAjaxView(JSONResponseMixin,View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ProcessPaymentAjaxView, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        response = {
            "STATUS": "",
            "MESSAGE": ""
        }
        if request.is_ajax():
            try:
                lesson_id = request.POST.get("lesson_id")
                extra_time = request.POST.get("extra_time")
                payment_method_id = request.POST.get("pm_id")

                if payment_method_id:
                    payment_method_id = int(payment_method_id)
                    payment_methods = PaymentMethod.objects.filter(pk=payment_method_id)

                lesson_object = LiveLesson.objects.get(pk=int(lesson_id))
                job_offers = JobOffer.objects.filter(application__job__lesson__id=lesson_object.pk)
                job_offer = job_offers.first()

                payment_processor = PaymentProcessor()

                transaction_amount = 0

                if extra_time:
                    if lesson_object.base_transaction:
                        transaction_amount = (float(extra_time) / 60.0) * job_offer.rate
                else:
                    if not lesson_object.base_transaction:
                        transaction_amount = (float(job_offer.duration) / 60.0) * job_offer.rate

                if not transaction_amount:
                    response["status"] = "FAILURE"
                    response["message"] = "No amount specified."
                    return self.render_to_json(response)

                lesson_type = "LIVE_LESSON"
                if lesson_object.type == 1:
                    lesson_type = "WRITTEN_LESSON"
                if payment_methods:
                    payment_process_status = payment_processor.process_lesson_payment(lesson_id,lesson_type, int(transaction_amount), job_offer.rate_currency, base_payment=not extra_time and not lesson_object.base_transaction, extra_time=extra_time, payment_methods=payment_methods)
                else:
                    payment_process_status = payment_processor.process_lesson_payment(lesson_id,lesson_type, int(transaction_amount), job_offer.rate_currency, base_payment=not extra_time and not lesson_object.base_transaction, extra_time=extra_time)

                if payment_process_status is True:

                    if extra_time:
                        lesson_object.extra_time = int(extra_time)
                        lesson_object.save()

                    response["status"] = "SUCCESS"
                    response["message"] = "Successful"
                    return self.render_to_json(response)
                else:
                    response["status"] = "FAILURE"
                    response["message"] = "Payment Failed."
                    return self.render_to_json(response)
            except:
                response["status"] = "FAILURE"
                response["message"] = "Payment Failed."
                return self.render_to_json(response)
        else:
            response["status"] = "FAILURE"
            response["message"] = "Invalid Operation."
            return self.render_to_json(response)


class MyAccountAjaxView(View):
    def get(self,request,*args,**kwargs):
        return HttpResponse("Invalid")
    def post(self,request,*args,**kwargs):
        response = {
            "STATUS": "",
            "MESSAGE": ""
        }
        if request.is_ajax():
            form = MyAccountForm(request.POST)
            if form.is_valid():
                email = form.cleaned_data["email"]
                old_password = form.cleaned_data["old_password"]
                new_password = form.cleaned_data["new_password"]
                new_password_again = form.cleaned_data["new_password_again"]
                user = authenticate(username=email, password=old_password)
                if user:
                    user = User.objects.get(email=email)
                    if new_password == new_password_again:
                        #print"Match"
                        user.set_password(new_password)
                        user.save()
                        #print"Lol"
                        response["STATUS"] = "SUCCESS"
                        response["MESSAGE"] = "Account updated successfully."
                        return HttpResponse(json.dumps(response))
                    else:
                        response["STATUS"] = "FAILURE"
                        response["MESSAGE"] = "New password mismatch. Please type your new password carefully."
                        return HttpResponse(json.dumps(response))
                else:
                    response["STATUS"] = "FAILURE"
                    response["MESSAGE"] = "User authentication failed. Please specify your old password correctly."
                    return HttpResponse(json.dumps(response))
            else:
                response["STATUS"] = "FAILURE"
                response["MESSAGE"] = "Please fill up the form correctly."
                return HttpResponse(json.dumps(response))
        else:
            response["STATUS"] = "FAILURE"
            response["MESSAGE"] = "Invalid operation"
            return HttpResponse(json.dumps(response))

class UserListAjaxView(JSONResponseMixin,View):
    def get(self,request,*args,**kwargs):
        keyword = request.GET.get("term")
        exclude = request.GET.get("exclude")
        type = request.GET.get("type")
        exclude_ids = [int(id) for id in exclude.split(',') if id]
        champ_user_objects = ChampUser.objects.filter((Q(fullname__icontains=keyword) & Q(type=Role.objects.get(name=type))) | Q(user__email=keyword)).exclude(user_id__in=exclude_ids)[:10]
        suggestions = []
        for champ_user in champ_user_objects:
            suggestions += [
                {
                    "value": champ_user.fullname,
                    "id": champ_user.user.id
                }
            ]
        return self.render_to_json(suggestions)

class MajorAjaxView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(MajorAjaxView, self).dispatch(*args, **kwargs)

    def get(self,request,*args,**kwargs):
        if request.is_ajax():
            keyword = request.GET.get("term")
            level = request.GET.get("level")
            skip_level_check = request.GET.get('slc')
            user_suggestions = []
            added_suggestions = []
            if keyword:
                major_objects = list(set(Major.objects.filter(name__icontains=keyword)))[:20]
                if major_objects:
                    user_major_objs = UserMajor.objects.filter(user_id=request.user.pk)
                    user_major_ids = []
                    for user_major_obj in user_major_objs:
                        user_major_ids += [user_major_obj.major_id]
                if skip_level_check == '1':
                    major_objects = list(set(Major.objects.filter(name__icontains=keyword).distinct()))[:100]
                    for major_obj in major_objects:
                        if not major_obj.name in added_suggestions and len(user_suggestions) < 20:
                            user_suggestions += [{"value": major_obj.name, "id": major_obj.id}]
                            added_suggestions += [ major_obj.name ]
                else:
                    major_objects = list(set(Major.objects.filter(name__icontains=keyword, level__name=level)))[:20]
                    if major_objects:
                        user_major_objs = UserMajor.objects.filter(user_id=request.user.pk, major__level__name=level)
                        user_major_ids = []
                        for user_major_obj in user_major_objs:
                            user_major_ids += [user_major_obj.major_id]
                        for major_obj in major_objects:
                            if not major_obj.id in user_major_ids:
                                user_suggestions += [{"value": major_obj.name,"id": major_obj.id}]

            k = keyword if keyword else ""

            readonly = True if request.GET.get('readonly') == 'true' else False

            if k and not readonly:
                user_suggestions += [{'value':'Add "<b>'+ k +'</b>"','id':-1}]

            return HttpResponse(json.dumps(user_suggestions), content_type = "application/json")

        else:
            return HttpResponse(json.dumps({"Status":"Error","Message": "Invalid request."}))

    def post(self,request,*args,**kwargs):
        response = {
            "status": "FAILURE",
            "message": "FAILURE",
            "data": []
        }
        if request.is_ajax():
            action = request.POST.get("action")
            tag = request.POST.get("tag")
            tag_id = request.POST.get("tag_id")
            if action == "SAVE":
                if tag and tag_id:
                    tag_id = int(tag_id)
                    if tag_id == -1:
                        major_obj = Major()
                        major_obj.name = tag
                        major_obj.date_created = datetime.now()
                        major_obj.creator = request.user
                        major_obj.save()

                        user_major_obj = UserMajor()
                        user_major_obj.user = request.user
                        user_major_obj.major = major_obj
                        user_major_obj.date_added = datetime.now()
                        user_major_obj.save()

                        response["status"] = "SUCCESS"
                        response["message"] = "SUCCESSFUL"
                        response["data"] = {"id": major_obj.id}

                    else:
                        major_objs = Major.objects.filter(id=tag_id)
                        if major_objs:
                            major_obj = major_objs[0]
                            user_major_obj = UserMajor()
                            user_major_obj.user = request.user
                            user_major_obj.major = major_obj
                            user_major_obj.date_added = datetime.now()
                            user_major_obj.save()

                            response["status"] = "SUCCESS"
                            response["message"] = "SUCCESSFUL"
                            response["data"] = {"id": major_obj.id}

            else:
                if tag_id:
                    tag_id = int(tag_id)
                    UserMajor.objects.filter(user=request.user,major_id=tag_id).delete()
                    response["status"] = "SUCCESS"
                    response["message"] = "SUCCESSFUL"

        else:
            response["status"] = "SUCCESS"
            response["message"] = "Invalid Operation"

        return HttpResponse(json.dumps(response))

class InviteUsersLessonAjaxView(JSONResponseMixin, View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(InviteUsersLessonAjaxView, self).dispatch(*args, **kwargs)

    def post(self,request,*args,**kwargs):
        job_id = request.POST.get("job_list")
        tutor_id = request.POST.get("invite_tutor")
        lesson_note = request.POST.get("invite_lesson_note")

        try:
            job_id = int(job_id)
            job = Job.objects.get(pk=job_id, posted_by_id=request.user.pk)
        except Exception as exp:
            job = None
            response = {
                "status": "FAILURE",
                "message": "Failed",
                "data": {
                    "reason": "Invalid job id."
                }
            }
            return self.render_to_json(response)

        try:
            tutor_id = int(tutor_id)
            user = User.objects.get(pk=tutor_id)
        except:
            response = {
                "status": "FAILURE",
                "message": "Failed",
                "data": {
                    "reason": "Invalid tutor id."
                }
            }
            return self.render_to_json(response)

        if JobInvitation.objects.filter(job_id=job_id,user_id=tutor_id).exists():
            response = {
                "status": "FAILURE",
                "message": "Failed",
                "data": {
                    "reason": "The tutor is already invited to apply to this job."
                }
            }
            return self.render_to_json(response)

        if job.applications.filter(tutor_id=tutor_id).exists():
            response = {
                "status": "FAILURE",
                "message": "Failed",
                "data": {
                    "reason": "Tutor already applied to this job"
                }
            }
            return self.render_to_json(response)

        if lesson_note:
            lesson_note = ''.join([ "<p>%s</p>" % line for line in lesson_note.split("\n") ])

        job_invitation = JobInvitation()
        job_invitation.job_id = job_id
        job_invitation.invited_by_id = request.user.pk
        job_invitation.user_id = tutor_id
        job_invitation.invite_message = lesson_note
        job_invitation.status = 1
        job_invitation.save()

        html, text = EmailTemplateUtil.render_job_invitation_email_template(request.user.champuser.fullname, job_invitation.user.champuser.fullname, lesson_note, job_id)

        job_subjects = ','.join([ s.name for s in job_invitation.job.preference.subjects.all() ])

        email_subject = "You have been invited by %s for %s %s" % ( request.user.champuser.fullname, job_invitation.job.preference.level.name, job_subjects)

        email_object = {
            "recipients": [ job_invitation.user.email ],
            'subject': email_subject,
            'html_body': html,
            'text_body': text
        }

        EmailScheduler.place_to_queue(email_object)

        NotificationManager.push_notification("job_invited",job_id,request.user.pk,tutor_id)

        if lesson_note:
            try:
                content = {}

                user1, user2 = job_invitation.user, request.user
                context = {
                    "message": lesson_note,
                    "sender_me": request.user.pk == user1.pk,
                    "receiver_name": user1.champuser.fullname
                }
                template_name = "ajax/message_detail_row.html"
                template = loader.get_template(template_name)
                cntxt = Context(context)
                rendered_detail_row = template.render(cntxt)

                template_name = "ajax/chatbox_chat_row.html"
                template = loader.get_template(template_name)
                cntxt = Context(context)
                rendered_chat_row = template.render(cntxt)

                contxt_data = {
                    "user_message": lesson_note,
                    "request": request,
                    "STATIC_URL": settings.STATIC_URL + "/",
                    "other_user": user2.pk
                }
                template_name = "ajax/message_list_entry.html"
                template = loader.get_template(template_name)
                cntxt = Context(contxt_data)
                rendered_message_list_entry = template.render(cntxt)

                content[user1.pk] = {
                    "id": user2.pk,
                    "chat_type": 0,
                    "title": user2.champuser.fullname,
                    "chat_popup": rendered_chat_row,
                    "msg_detail_entry": rendered_detail_row,
                    "message_list_entry": rendered_message_list_entry,
                    "status": "SUCCESS"
                }


                added_users = [ job_invitation.user.pk ]

                # Now publish the status with data in MESSAGE_SENT signal.
                try:
                    redis_client = RedisClient.Instance()

                    publish_message = {
                        "content": content,
                        "receivers": added_users
                    }
                    publish_message["CHANNEL"] = CHANNEL["send_chat_message"]
                    redis_client.publish_channel(CHANNEL["send_chat_message"], json.dumps(publish_message))
                except Exception as msg:
                    pass
            except Exception as exp:
                pass

        response = {
            "status": "SUCCESS",
            "message": "SUCCESSFUL",
            "data": []
        }
        return self.render_to_json(response)

class TeachingExperienceAjaxView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(TeachingExperienceAjaxView, self).dispatch(*args, **kwargs)

    def get(self,request,*args,**kwargs):
        data = ''
        teaching_experiences_objs = TeachingExperiences.objects.filter(user=request.user)
        if teaching_experiences_objs:
            data = teaching_experiences_objs[0].experience
        return render(request,"ajax/profile/tutor/teaching_experience.html",{"teaching_exp_data":data})

    def post(self,request,*args,**kwargs):
        exp_html = request.POST.get("data")
        if exp_html:
            exp_html = exp_html.strip()
        else:
            exp_html = ''

        teaching_exp_exists = TeachingExperiences.objects.filter(user=request.user).exists()
        if not teaching_exp_exists:
            teaching_experiences_obj = TeachingExperiences()
        else:
            teaching_experiences_obj = TeachingExperiences.objects.filter(user=request.user).first()
        teaching_experiences_obj.user = request.user
        teaching_experiences_obj.experience = exp_html
        teaching_experiences_obj.last_update_time = datetime.now()
        teaching_experiences_obj.save()
        return HttpResponse(json.dumps({"status":"SUCCESS","message":"SUCCESSFUL","data":[]}))

class ExtracurricularInterestAjaxView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ExtracurricularInterestAjaxView, self).dispatch(*args, **kwargs)

    def get(self,request,*args,**kwargs):
        data = ''
        extra_curricular_interest_objs = ExtraCurricularInterest.objects.filter(user=request.user)
        if extra_curricular_interest_objs:
            data = extra_curricular_interest_objs[0].interest
        return render(request,"ajax/profile/tutor/extra_curricular_interest.html",{ "extra_curricular_interest": data })

    def post(self,request,*args,**kwargs):
        data_html = request.POST.get("data")
        if data_html:
            data_html = data_html.strip()
        else:
            data_html = ''

        data_exists = ExtraCurricularInterest.objects.filter(user=request.user).exists()
        if not data_exists:
            data_obj = ExtraCurricularInterest()
        else:
            data_obj = ExtraCurricularInterest.objects.filter(user=request.user).first()
        data_obj.user = request.user
        data_obj.interest = data_html
        data_obj.last_update_time = datetime.now()
        data_obj.save()
        return HttpResponse(json.dumps({"status":"SUCCESS","message":"SUCCESSFUL","data":[]}))

class TopSubjectsAjaxView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(TopSubjectsAjaxView, self).dispatch(*args, **kwargs)

    def get(self,request,*args,**kwargs):
        id = request.GET.get("id")
        action = request.GET.get("action")
        context = {}
        if id:

            if action == "REMOVE":
                UserTopCourses.objects.filter(pk=int(id)).delete()
                response = {
                    "status": "SUCCESS",
                    "message": "Successful",
                    "data": []
                }
                return HttpResponse(json.dumps(response))
            user_top_course_objs = UserTopCourses.objects.filter(pk=int(id))
            if user_top_course_objs.exists():
                context["user_top_course"] = user_top_course_objs.first()
        return render(request,"ajax/profile/tutor/top_subject.html",context)

    def post(self,request,*args,**kwargs):
        response = {
            "status": "FAILURE",
            "message": "FAILURE",
            "data": []
        }
        if request.is_ajax():
            try:
                top_course_id = int(request.POST.get("top_course_id"))
                subject_val = request.POST.get("subject_val")
                subject_id = int(request.POST.get("subject_id"))
                level_name = request.POST.get("level_name")
                level_id = int(request.POST.get('level_id'))
                description = request.POST.get("description")

                if not level_name:
                    response["status"] = "FAILURE"
                    response["message"] = "Level required."
                    return HttpResponse(json.dumps(response))

                if not subject_val:
                    response["status"] = "FAILURE"
                    response["message"] = "Subject required."
                    return HttpResponse(json.dumps(response))

                if level_id == -1:

                    subject_level = SubjectLevel()
                    subject_level.name = level_name
                    subject_level.save()
                else:
                    subject_level = SubjectLevel.objects.get(pk=level_id)

                if subject_id == -1:
                    subject_object = Major()
                    subject_object.name = subject_val
                    subject_object.description = description
                    subject_object.level_id = subject_level.pk
                    subject_object.creator_id = request.user.pk
                    subject_object.save()
                else:
                    subject_object = Major.objects.get(pk=subject_id)

                if top_course_id == -1:
                    top_course_object = UserTopCourses()
                else:
                    top_course_object = UserTopCourses.objects.get(pk=top_course_id)
                top_course_object.user_id = request.user.pk
                top_course_object.level_id = subject_level.pk
                top_course_object.subject_id = subject_object.pk
                top_course_object.description = description
                top_course_object.save()

                response["status"] = "SUCCESS"
                response["message"] = "Course Added Successfully."
                response["data"] = {

                }
                return HttpResponse(json.dumps(response))
            except Exception as exp:
                response["status"] = "FAILURE"
                response["message"] = "Server Error"

        else:
            response["status"] = "FAILURE"
            response["message"] = "Invalid Operation"

        return HttpResponse(json.dumps(response))

class ProfileRateAjaxView(JSONResponseMixin,View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ProfileRateAjaxView, self).dispatch(*args, **kwargs)

    def get(self,request,*args,**kwargs):
        context = {}
        rate_objs = Rate.objects.filter(user=request.user)
        context["rate_objs"] = rate_objs
        context["currency_objs"] = Currency.objects.all()
        return render(request,"ajax/profile/tutor/profile_rate.html",context)

    def post(self,request,*args,**kwargs):
        response = {
            "status": "FAILURE",
            "message": "FAILURE",
            "data": []
        }
        action = request.POST.get("action")
        if action == "ADD_RATE":
            rate_class = request.POST.get("rate_class")
            rate = request.POST.get("rate")
            # max_rate = request.POST.get("max_rate")
            rate_currency = request.POST.get("rate_currency")
            if not rate_class:
                response["status"] = "FAILURE"
                response["message"] = "Class Required"
                return self.render_to_json(response)

            if not rate:
                response["status"] = "FAILURE"
                response["message"] = "A valid rate is required"
                return self.render_to_json(response)

            try:
                rate = float(rate)
            except Exception as msg:
                response["status"] = "FAILURE"
                response["message"] = "A valid rate is required"
                return self.render_to_json(response)

            # if min_rate > max_rate:
            #     response["status"] = "FAILURE"
            #     response["message"] = "Max rate must be greater than or equal to min rate"
            #     return self.render_to_json(response)

            if rate < 0:
                response["status"] = "FAILURE"
                response["message"] = "Invalid value given in the rate field. The value must be grater than or equal 0"
                return self.render_to_json(response)

            rate_obj = Rate()
            rate_obj.user = request.user
            rate_obj.class_level = rate_class
            rate_obj.rate = rate
            rate_obj.rate_currency = rate_currency
            # rate_obj.create_date = datetime.now()
            # rate_obj.modified_date = datetime.now()
            rate_obj.save()

            response["status"] = "SUCCESS"
            response["message"] = "Profile rate added successfully."

            response["data"] = {
                "id": rate_obj.id,
                "user_id": rate_obj.user_id,
                "class_level": rate_obj.class_level,
                "rate": rate_obj.rate,
                "rate_currency": rate_obj.rate_currency.upper()
            }

            rate_objs = Rate.objects.filter(user=request.user)

            response["data"]["profile_min_rate"] = -1
            response["data"]["profile_max_rate"] = -1

            min_rate_ = rate_objs.aggregate(Min('rate'))['rate__min']
            max_rate_ = rate_objs.aggregate(Max('rate'))['rate__max']

            if min_rate_ is not None and max_rate_ is not None:
                response["data"]["profile_min_rate"] = min_rate_
                response["data"]["profile_max_rate"] = max_rate_
            return self.render_to_json(response)
        elif action == "ADD_BATCH_RATE":
            rates = {}
            for key,value in request.POST.items():
                if key == "action":
                    continue
                if key.startswith("rate_id_"):
                    key_val = int(key.replace("rate_id_",""))
                    if not rates.get(key_val):
                        rates[key_val] = {"id":int(value),"rate":0,"currency":""}
                    else:
                        rates[key_val]["id"] = int(value)
                elif key.startswith("rate_currency_"):
                    key_val = int(key.replace("rate_currency_",""))
                    if not rates.get(key_val):
                        rates[key_val] = {"id":0,"rate":0,"currency": value}
                    else:
                        rates[key_val]["currency"] = value
                else:
                    key_val = int(key.replace("rate_value_",""))
                    if not rates.get(key_val):
                        rates[key_val] = {"id":0,"rate":float(value),"currency":""}
                    else:
                        rates[key_val]["rate"] = float(value)

            for key, pair in rates.items():
                id = pair['id']
                rate = pair['rate']
                currency = pair['currency']
                rate_obj = Rate.objects.get(pk=id)
                rate_obj.rate = rate
                rate_obj.rate_currency = currency
                rate_obj.save()

            response["status"] = "SUCCESS"
            response["message"] = "Profile rate added successfully."
            return self.render_to_json(response)
        elif action == "DELETE_RATE":
            rate_id = request.POST.get("rate_id")
            Rate.objects.filter(id=int(rate_id)).delete()
            response["status"] = "SUCCESS"
            response["message"] = "Profile rate removed successfully."
            return self.render_to_json(response)
        else:
            response["status"] = "FAILURE"
            response["message"] = "Invalid operation requested."
            return self.render_to_json(response)


class SchoolAttendedAjaxView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(SchoolAttendedAjaxView, self).dispatch(*args, **kwargs)

    def get(self,request,*args,**kwargs):
        if request.is_ajax():
            response = {
                "status": "FAILURE",
                "message": "FAILURE",
                "data": []
            }
            try:
                sid = int(request.GET.get("sid"))
                exists = Education.objects.filter(id=sid).exists()
                if exists:
                    Education.objects.filter(id=sid).delete()
                    response["status"] = "SUCCESS"
                    response["message"] = "Successful"
                else:
                    response["status"] = "FAILURE"
                    response["message"] = "This course doesn't exists or has been removed already."

            except Exception as msg:
                response["status"] = "FAILURE"
                response["message"] = "Internal Server Error"+str(msg)
            return HttpResponse(json.dumps(response))

        else:
            context = {}
            education_id = request.GET.get("id")
            if education_id:
                edu_obj = Education.objects.get(id=int(education_id))
                context["education_obj"] = edu_obj
            return render(request,"ajax/profile/tutor/school_attended.html",context)

    def post(self,request,*args,**kwargs):
        response = {
            "status": "FAILURE",
            "message": "FAILURE",
            "data": []
        }
        if request.is_ajax():
            try:
                edu_id = int(request.POST.get("edu_id"))
                level = request.POST.get('level')
                level_id = int(request.POST.get('level_id'))
                sch_val = request.POST.get('sch_val')
                sch_id = int(request.POST.get('sch_id'))
                start_year = request.POST.get('start_year')
                end_year = request.POST.get('end_year')
                cgpa = request.POST.get('cgpa')
                is_current = True if request.POST.get('is_current') == "true" else False
                if level_id == -1:
                    if level:
                        subject_level = SubjectLevel()
                        subject_level.name = level
                        subject_level.save()
                    else:
                        response["status"] = "FAILURE"
                        response["message"] = "Certification required."
                        return HttpResponse(json.dumps(response))
                else:
                    subject_level = SubjectLevel.objects.get(pk=level_id)

                if sch_id == -1:
                    if sch_val:
                        school_obj = Schools()
                        school_obj.name = sch_val
                        school_obj.added_by = request.user
                        school_obj.save()
                    else:
                        response["status"] = "FAILURE"
                        response["message"] = "School required."
                        return HttpResponse(json.dumps(response))
                else:
                    school_objs = Schools.objects.filter(id=sch_id)
                    if school_objs:
                        school_obj = school_objs.first()
                    else:
                        response["status"] = "FAILURE"
                        response["message"] = "School does't exist."
                        return HttpResponse(json.dumps(response))
                education_obj = Education()
                if edu_id and edu_id != -1:
                    education_obj = Education.objects.get(pk=edu_id)
                education_obj.user = request.user
                education_obj.level = subject_level
                education_obj.institution = school_obj
                education_obj.date_attended = int(start_year)
                if not end_year or end_year == 'Present':
                    end_year = 0
                education_obj.end_year = int(end_year)
                education_obj.cgpa = cgpa
                education_obj.is_current = is_current
                education_obj.save()

                response["status"] = "SUCCESS"
                response["message"] = "Successful."
                response["data"] = {
                    "id": education_obj.id
                }

            except Exception as msg:
                response["status"] = "FAILURE"
                response["message"] = "Error occured. Internal Server Error."
        else:
            response["status"] = "FAILURE"
            response["message"] = "Invalid Operation"
        return HttpResponse(json.dumps(response))

class SuggestionsCertificationsAjaxView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(SuggestionsCertificationsAjaxView, self).dispatch(*args, **kwargs)

    def get(self,request,*args,**kwargs):
        if request.is_ajax():
            suggestions = []
            keyword = request.GET.get("term")
            if keyword:
                cert_objs = Certifications.objects.filter(name__icontains=keyword)
                if cert_objs:
                    for cert_obj in cert_objs:
                        suggestions += [{"value": cert_obj.name,"id": cert_obj.id}]
            if keyword:
                suggestions += [{'value':'Add "<b>'+ keyword +'</b>"','id':-1}]

            return HttpResponse(json.dumps(suggestions), content_type = "application/json")
        else:
            return HttpResponse(json.dumps("Invalid request"))

    def post(self,request,*args,**kwargs):
        pass

class SuggestionsSchoolsAjaxView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(SuggestionsSchoolsAjaxView, self).dispatch(*args, **kwargs)

    def get(self,request,*args,**kwargs):
        if request.is_ajax():
            suggestions = []
            keyword = request.GET.get("term")
            if keyword:
                sc_type = request.GET.get('sc_type')
                if sc_type is None:
                    sc_type = ''
                schl_objs = Schools.objects.filter(name__icontains=keyword, type=sc_type)
                if schl_objs:
                    for schl_obj in schl_objs:
                        suggestions += [{"value": schl_obj.name,"id": schl_obj.id}]
            if keyword:
                suggestions += [{'value':'Add "<b>'+ keyword +'</b>"','id':-1}]

            return HttpResponse(json.dumps(suggestions), content_type = "application/json")
        else:
            return HttpResponse(json.dumps("Invalid request"))

    def post(self,request,*args,**kwargs):
        pass

class SuggestionsLocationAjaxView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(SuggestionsLocationAjaxView, self).dispatch(*args, **kwargs)

    def get_suggestions_from_google(self, term):
        import requests
        from engine.config.API import GOOGLE_PLACES_API_KEY, GOOGLE_PLACES_API_ENDPOINT
        import json
        req = requests.get(GOOGLE_PLACES_API_ENDPOINT + "?key=%s&input= %s" % (GOOGLE_PLACES_API_KEY, term))
        json_response = json.loads(req.content)
        suggestions = []
        if json_response['status'] == 'OK':
            predictions = json_response['predictions']
            for prediction in predictions:
                description = prediction['description']
                suggestions += [description]
        return suggestions

    def get(self,request,*args,**kwargs):
        if request.is_ajax():
            suggestions = []
            keyword = request.GET.get("term")
            if keyword:
                google_suggestions = self.get_suggestions_from_google(keyword)
                for suggestion in google_suggestions:
                    suggestions += [{"value": suggestion }]
            return HttpResponse(json.dumps(suggestions), content_type = "application/json")
        else:
            return HttpResponse(json.dumps("Invalid request"))

class SuggestionsLevelAjaxView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(SuggestionsLevelAjaxView, self).dispatch(*args, **kwargs)

    def get(self,request,*args,**kwargs):
        if request.is_ajax():
            suggestions = []
            keyword = request.GET.get("term")
            if keyword:
                levels = CountryLevel.objects.filter(country_id=request.user.champuser.nationality.pk, level__name__icontains=keyword).select_related('level').values_list('level_id', 'level__name')

                exact_found = False
                for id, name in levels:
                    suggestions += [
                        {
                            "id": id,
                            "value": name
                        }
                    ]
                    if name == keyword:
                        exact_found = True
            if keyword and not exact_found:
                suggestions += [{'value':'Add "<b>'+ keyword +'</b>"','id':-1}]

            return HttpResponse(json.dumps(suggestions), content_type = "application/json")
        else:
            return HttpResponse(json.dumps("Invalid request"))

    def post(self,request,*args,**kwargs):
        pass

class SuggestionsSubjectsAjaxView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(SuggestionsSubjectsAjaxView, self).dispatch(*args, **kwargs)

    def get(self,request,*args,**kwargs):
        if request.is_ajax():
            suggestions = []
            try:
                level_id = int(request.GET.get("level"))
                keyword = request.GET.get("term")
                if keyword:
                    course_objs = Major.objects.filter(name__icontains=keyword, level_id=level_id)
                    if course_objs:
                        for obj in course_objs:
                            suggestions += [{"value": obj.name,"id": obj.id}]
                if keyword:
                    suggestions += [{'value':'Add "<b>'+ keyword +'</b>"','id':-1}]
            except Exception as exp:
                return HttpResponse(json.dumps(suggestions), content_type = "application/json")

            return HttpResponse(json.dumps(suggestions), content_type = "application/json")
        else:
            return HttpResponse(json.dumps("Invalid request"))

    def post(self,request,*args,**kwargs):
        pass

class SuggestionsCategoryAjaxView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(SuggestionsCategoryAjaxView, self).dispatch(*args, **kwargs)

    def get(self,request,*args,**kwargs):
        if request.is_ajax():
            suggestions = []
            keyword = request.GET.get("term")
            if keyword:
                objs = Category.objects.filter(name__icontains=keyword)
                if objs:
                    for obj in objs:
                        suggestions += [{"value": obj.name,"id": obj.id}]
            if keyword:
                suggestions += [{'value':'Add "<b>'+ keyword +'</b>"','id':-1}]

            return HttpResponse(json.dumps(suggestions), content_type = "application/json")
        else:
            return HttpResponse(json.dumps("Invalid request"))

    def post(self,request,*args,**kwargs):
        pass

class UpdateProfileInfoAjax(JSONResponseMixin, View):

    def dispatch(self, *args, **kwargs):
        return super(UpdateProfileInfoAjax, self).dispatch(*args, **kwargs)

    @method_decorator(champ_login_required)
    def get(self,request,*args,**kwargs):
        context = {}
        champ_user = ChampUser.objects.get(user=request.user)
        context["champ_user"] = champ_user
        nric_verification_objs = NRICVerification.objects.filter(user=request.user)
        profile_verification_status = 0
        if nric_verification_objs:
            profile_verification_status = nric_verification_objs[0].status

        context["profile_verification_status"] = profile_verification_status

        context["countries"] = CountryUtil.get_supported_country_objects()

        context["nationality"] = context["champ_user"].nationality

        timezones = Clock.get_all_timezones()

        context["timezones"] = timezones

        context["request"] = request

        return render(request,"ajax/profile/tutor/profile_summary.html", context)

    @method_decorator(champ_login_required)
    def post(self,request,*args,**kwargs):
        response = {
            "status": "FAILURE",
            "message": "FAILURE",
            "data": []
        }
        #printrequest.POST
        full_name = request.POST.get("full_name")
        nationality = request.POST.get("nationality")
        gender = request.POST.get("select_gender")
        user_timezone = request.POST.get("user_timezone")
        nid_photo = request.FILES.get("national_id_img")

        if not full_name:
            response["status"] = "FAILURE"
            response["message"] = "Name is required"
            return self.render_to_json(response)

        if not nationality:
            response["status"] = "FAILURE"
            response["message"] = "Nationality is required"
            return self.render_to_json(response)

        try:
            nationality = int(nationality)
        except Exception as msg:
            response["status"] = "FAILURE"
            response["message"] = "Invalid nationality id"
            return self.render_to_json(response)

        if not Country.objects.filter(id=nationality).exists():
            response["status"] = "FAILURE"
            response["message"] = "Invalid nationality id"
            return self.render_to_json(response)

        champ_user_obj = ChampUser.objects.get(user=request.user)
        champ_user_obj.fullname = full_name
        champ_user_obj.nationality = Country.objects.get(id=nationality)
        champ_user_obj.gender = gender.strip() if gender else ""
        champ_user_obj.modified_date = datetime.now()
        champ_user_obj.timezone = user_timezone
        champ_user_obj.save()

        response["status"] = "SUCCESS"
        response["message"] = "Successful."
        response["data"] = {
            "name": champ_user_obj.fullname,
            "nationality": champ_user_obj.nationality.name,
            "nid": champ_user_obj.nationality.id,
            "current_school": champ_user_obj.current_school,
            "session": champ_user_obj.session,
            "major": champ_user_obj.major
        }

        return self.render_to_json(response)

class UpdateProfileAboutAjax(JSONResponseMixin, View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(UpdateProfileAboutAjax, self).dispatch(*args, **kwargs)

    def post(self,request,*args,**kwargs):
        response = {
            "status": "FAILURE",
            "message": "FAILURE",
            "data": []
        }
        about_content = request.POST.get("about_data")

        profile_obj = Profile.objects.filter(user__user=request.user).first()

        profile_obj.about = about_content
        profile_obj.save()

        response["status"] = "SUCCESS"
        response["message"] = "Successful."

        return self.render_to_json(response)

class VideoURLViewAjax(JSONResponseMixin, View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(VideoURLViewAjax, self).dispatch(*args, **kwargs)

    def get_video_duration(self, video_id):
        import requests
        try:
            request_url = "https://www.googleapis.com/youtube/v3/videos?id=%s&part=contentDetails&key=%s" % ( video_id, settings.GOOGLE_YOUTUBE_DATA_API_KEY, )
            req = requests.get(request_url)
            duration_readable = None
            if req.status_code == 200:
                content = req.content
                json_content = json.loads(content)
                if json_content['items']:
                    item_details = json_content['items'][0]
                    content_details = item_details['contentDetails']
                    duration_readable = content_details['duration']
                    duration_readable = duration_readable.replace('PT', '')
                    duration_readable = duration_readable.lower()
                    duration_readable = duration_readable.replace('h', 'h ')
                    duration_readable = duration_readable.replace('m', 'm ')
                    duration_readable = duration_readable.replace('s', 's ')
            return duration_readable
        except Exception as exp:
            pass

    def get_video_thumbnails(self, video_id):
        import requests
        try:
            request_url = "https://www.googleapis.com/youtube/v3/videos?id=%s&part=snippet&key=%s" % ( video_id, settings.GOOGLE_YOUTUBE_DATA_API_KEY, )
            req = requests.get(request_url)
            duration_readable = None
            if req.status_code == 200:
                content = req.content
                json_content = json.loads(content)
                if json_content['items']:
                    item_details = json_content['items'][0]
                    snippet = item_details['snippet']
                    title = snippet['title']
                    description = snippet['description']
                    short_description = snippet['description'][:200] + "..." if len(snippet['description']) > 200 else snippet['description']
                    thumbnails = snippet['thumbnails']
                    return title, description, short_description, thumbnails
            return None, None, None
        except Exception as exp:
            return None, None, None

    def get(self,request,*args,**kwargs):
        if request.is_ajax():

            if request.GET.get('action') == "fetch-youtube-video-details":
                video_id = ""

                video_url = request.GET.get("url")
                if not video_url:
                    response = {
                        "status": "FAILURE",
                        "message": "Invalid url",
                        "data": ""
                    }
                    return self.render_to_json(response)

                video_exists = ProfileMyVideo.objects.filter(user=request.user, url=video_url).exists()
                if video_exists:
                    response = {
                        "status": "FAILURE",
                        "message": "This video already added in your profile.",
                        "data": ""
                    }
                    return self.render_to_json(response)

                if "watch" in video_url:
                    video_id = video_url[video_url.rindex("?v=")+ 3:]

                if "embed" in video_url: #https://www.youtube.com/embed/zWQciUS8qVI
                    video_id = video_url[video_url.rindex("/") + 1:]

                def parse_duration(duration_str):
                    duration = {

                    }
                    duration_str = duration_str.replace("PT", "")
                    if "H" in duration_str:
                        duration['hour'], duration_str = duration_str.split("H")

                    if "M" in duration_str:
                        duration['minute'], duration_str = duration_str.split("M")

                    if "S" in duration_str:
                        duration['seconds'], duration_str = duration_str.split("S")

                    return duration

                video_duration = self.get_video_duration(video_id)
                title, description, short_description, thumbnails = self.get_video_thumbnails(video_id)

                response = {
                    "status": "SUCCESS",
                    "message": "Successful",
                    "data": {
                        "duration": video_duration,
                        "title": title,
                        "description": description,
                        "short_description": short_description,
                        "thumbnails": thumbnails
                    }
                }
                return self.render_to_json(response)

            video_id = request.GET.get("id")

            try:
                video_id = int(video_id)
            except Exception as exp:
                video_id = None

            context = {

            }
            if video_id:
                my_video = ProfileMyVideo.objects.get(pk=video_id)
                context['my_video'] = my_video
                context["duration"] = my_video.video_length
                context["title"] = my_video.title
                context["description"] = my_video.description
                context["short_description"] = my_video.description[:200] + "..." if len(my_video.description) > 200 else my_video.description
                context["thumbnail"] = my_video.thumbnail_url
            template = loader.get_template("ajax/video_url.html")
            cntxt = Context(context)
            rendered = template.render(cntxt)
            response = {
                "status": "SUCCESS",
                "message": "Successful",
                "data": {
                    "dialog_content": rendered
                }
            }
            return self.render_to_json(response)

    def handle_vimeo_upload(self, file_path, video_uri=None, replace=False):
        try:
            import vimeo
            v = vimeo.VimeoClient(
                token = settings.VIMEO_API['ACCESS_TOKEN'],
                key=settings.VIMEO_API['CLIENT_ID'],
                secret=settings.VIMEO_API['CLIENT_SECRET'])
            if not replace:
                video_uri = v.upload(file_path)
            else:
                if video_uri:
                    video_uri = v.replace(
                        video_uri=video_uri,
                        filename=file_path,
                        upgrade_to_1080=False)
                else:
                    video_uri = v.upload(file_path)

            os.remove(file_path)

            return video_uri
        except Exception as exp:
            error_log = ErrorLog()
            error_log.url = ''
            error_log.stacktrace = str(exp)
            error_log.save()
            return ""

    def handle_video_delete(self, video_id):
        try:
            import vimeo
            v = vimeo.VimeoClient(
                token = settings.VIMEO_API['ACCESS_TOKEN'],
                key=settings.VIMEO_API['CLIENT_ID'],
                secret=settings.VIMEO_API['CLIENT_SECRET'])
            v.delete_video(video_id)
        except Exception as exp:
            pass

    def vimeo_video_details(self, video_id):
        try:
            import requests, json
            req = requests.get('http://vimeo.com/api/v2/video/%s.json' % video_id)
            if req.status_code == 200:
                json_content = json.loads(req.content)
                if json_content:
                    json_content = json_content[0]
                    return { 'duration': json_content['duration'], 'thumbnail': json_content['thumbnail_medium'] }
            return {}
        except Exception as exp:
            return {}

    def post(self,request,*args,**kwargs):
        response = {
            "status": "FAILURE",
            "message": "FAILURE",
            "data": []
        }
        if request.is_ajax():
            action = request.POST.get("action")
            if action == "SAVE":
                id = request.POST.get("id")
                try:
                    id = int(id)
                except Exception as exp:
                    id = -1
                title = request.POST.get("title")
                description = request.POST.get("description")
                video_url = request.POST.get("url")
                duration = request.POST.get("duration")
                attachment = request.FILES.get("file")
                thumb = request.POST.get("thumb")
                if not title and not description and not video_url and not attachment:
                    response["status"] = "FAILURE"
                    response["message"] = "Missing data"
                    return self.render_to_json(response)

                if id == -1 and not video_url and not attachment:
                    response["status"] = "FAILURE"
                    response["message"] = "Missing data"
                    return self.render_to_json(response)


                file_path = None

                if attachment:
                    file_name = str(request.user.id)+"-"+str(uuid.uuid4())+"."+attachment._name[attachment._name.rindex(".")+1:]
                    # print file_name
                    with open(os.path.join(settings.TEMP_DIR, file_name), 'wb+') as destination:
                        for chunk in attachment.chunks():
                            destination.write(chunk)

                    file_path = os.path.join(settings.TEMP_DIR, file_name)
                if video_url and attachment and file_path:
                    if id != -1:
                        profile_my_video_obj = ProfileMyVideo.objects.get(pk=id)
                        if profile_my_video_obj.host_type == "VIMEO":
                            vimeo_url = self.handle_vimeo_upload(file_path, profile_my_video_obj.url, replace=True)
                        else:
                            vimeo_url = self.handle_vimeo_upload(file_path)

                        video_details = self.vimeo_video_details(video_url.replace("/", "").replace("videos", ""))

                    else:
                        vimeo_url = self.handle_vimeo_upload(file_path)

                        video_details = self.vimeo_video_details(video_url.replace("/", "").replace("videos", ""))

                        profile_my_videos = ProfileMyVideo.objects.filter(user_id=request.user.pk, url=vimeo_url)
                        if not profile_my_videos.exists():
                            profile_my_video_obj = ProfileMyVideo()
                        else:
                            profile_my_video_obj = profile_my_videos.first()
                    profile_my_video_obj.host_type = "VIMEO"
                    profile_my_video_obj.user = request.user
                    ##https://www.youtube.com/watch?v=ut9ThAbfde0
                    ##https://www.youtube.com/embed/ut9ThAbfde0
                    profile_my_video_obj.title = title
                    profile_my_video_obj.file_name = attachment._name
                    profile_my_video_obj.description = description
                    profile_my_video_obj.url = vimeo_url
                    if video_details.get('duration'):
                        if video_details.get('duration'):
                            hour,minute, seconds = 0, 0, 0
                            duration = int(video_details.get('duration'))
                            hour = duration / 3600
                            remaining_seconds = duration % 3600
                            minute = remaining_seconds / 60
                            seconds = remaining_seconds % 60
                            duration_str = ""
                            if hour:
                                duration_str += "%sh " % hour
                            if minute:
                                duration_str += "%sm " % minute
                            else:
                                duration_str += "0m "
                            if seconds:
                                duration_str += "%ss " % seconds
                            profile_my_video_obj.video_length = duration_str
                    else:
                        profile_my_video_obj.video_length = ''
                    if video_details.get('thumbnail'):
                        profile_my_video_obj.thumbnail_url = video_details['thumbnail']
                    else:
                        profile_my_video_obj.thumbnail_url = ''
                    profile_my_video_obj.save()
                elif video_url:
                    video_id = None
                    if "watch" in video_url:
                        video_id = video_url[video_url.rindex("?v=")+ 3:]

                    if "embed" in video_url: #https://www.youtube.com/embed/zWQciUS8qVI
                        video_id = video_url[video_url.rindex("/") + 1:]
                    if video_id:
                        if id != -1:
                            profile_my_video_obj = ProfileMyVideo.objects.get(pk=id)
                            if profile_my_video_obj.host_type == "VIMEO":
                                self.handle_video_delete(profile_my_video_obj.host_type.replace("/", "").replace("videos", ""))
                        else:
                            profile_my_videos = ProfileMyVideo.objects.filter(user_id=request.user.pk, url=video_id)
                            if not profile_my_videos.exists():
                                profile_my_video_obj = ProfileMyVideo()
                            else:
                                profile_my_video_obj = profile_my_videos.first()
                        profile_my_video_obj.host_type = "YOUTUBE"
                        profile_my_video_obj.user = request.user
                        profile_my_video_obj.url = video_id
                        profile_my_video_obj.title = title
                        profile_my_video_obj.description = description
                        profile_my_video_obj.video_length = duration
                        if thumb:
                            profile_my_video_obj.thumbnail_url = thumb
                        profile_my_video_obj.save()
                elif attachment and file_path:
                    if id != -1:
                        profile_my_video_obj = ProfileMyVideo.objects.get(pk=id)
                        if profile_my_video_obj.host_type == "VIMEO":
                            vimeo_url = self.handle_vimeo_upload(file_path, profile_my_video_obj.url, replace=True)
                        else:
                            vimeo_url = self.handle_vimeo_upload(file_path)

                        video_details = self.vimeo_video_details(video_url.replace("/", "").replace("videos", ""))

                    else:
                        vimeo_url = self.handle_vimeo_upload(file_path)

                        video_details = self.vimeo_video_details(video_url.replace("/", "").replace("videos", ""))

                        profile_my_videos = ProfileMyVideo.objects.filter(user_id=request.user.pk, url=vimeo_url)
                        if not profile_my_videos.exists():
                            profile_my_video_obj = ProfileMyVideo()
                        else:
                            profile_my_video_obj = profile_my_videos.first()
                    profile_my_video_obj.host_type = "VIMEO"
                    profile_my_video_obj.file_name = attachment._name
                    profile_my_video_obj.user = request.user
                    profile_my_video_obj.title = title
                    profile_my_video_obj.description = description
                    profile_my_video_obj.url = vimeo_url
                    if video_details.get('duration'):
                        if video_details.get('duration'):
                            hour,minute, seconds = 0, 0, 0
                            duration = int(video_details.get('duration'))
                            hour = duration / 3600
                            remaining_seconds = duration % 3600
                            minute = remaining_seconds / 60
                            seconds = remaining_seconds % 60
                            duration_str = ""
                            if hour:
                                duration_str += "%sh " % hour
                            if minute:
                                duration_str += "%sm " % minute
                            else:
                                duration_str += "0m "
                            if seconds:
                                duration_str += "%ss " % seconds
                            profile_my_video_obj.video_length = duration_str
                    if video_details.get('thumbnail'):
                        profile_my_video_obj.thumbnail_url = video_details['thumbnail']
                    profile_my_video_obj.save()
                elif id != -1 and not attachment and not file_path and not video_url:
                    profile_my_video_obj = ProfileMyVideo.objects.get(pk=id)
                    profile_my_video_obj.title = title
                    profile_my_video_obj.description = description
                    profile_my_video_obj.save()

                response["status"] = "SUCCESS"
                response["message"] = "Successful"
                response["data"] = {
                }
                return self.render_to_json(response)
            elif action == "DELETE":
                video_id = request.POST.get("video_id")
                if video_id:
                    try:
                        video_id = int(video_id)
                    except Exception as msg:
                        response["status"] = "FAILURE"
                        response["message"] = "Invalid item id provided."
                        return self.render_to_json(response)

                    profile_my_video = ProfileMyVideo.objects.get(pk=video_id)
                    if profile_my_video.host_type == "VIMEO":
                        self.handle_video_delete(profile_my_video.url.replace("/", "").replace("videos", ""))
                    profile_my_video.delete()
                    response["status"] = "SUCCESS"
                    response["message"] = "Successful"
                    return self.render_to_json(response)
                else:
                    response["status"] = "FAILURE"
                    response["message"] = "Uid is needed."
                    return self.render_to_json(response)
            else:
                response["status"] = "FAILURE"
                response["message"] = "Invalid operation mode specified"
                return self.render_to_json(response)

class StickyNoteAjaxView(JSONResponseMixin, View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(StickyNoteAjaxView, self).dispatch(*args, **kwargs)

    def get(self,request,*args,**kwargs):
        pass

    def post(self,request,*args,**kwargs):
        response = {
            "status": "FAILURE",
            "message": "FAILURE",
            "data": []
        }
        if request.is_ajax():
            action = request.POST.get("action")
            if action == "CREATE":
                id = request.POST.get("id")
                lesson_id = request.POST.get("lesson_id")
                top = request.POST.get("top")
                left = request.POST.get("left")
                content = request.POST.get("content")
                try:
                    top = float(top)
                    left = float(left)
                except Exception as msg:
                    response["status"] = "FAILURE"
                    response["message"] = "Co-ordinates must be a valid number"
                    return self.render_to_json(response)
                whiteboard = None
                if lesson_id:
                    live_lessons = LiveLesson.objects.filter(pk=int(lesson_id))
                    if live_lessons.exists():
                        whiteboard = live_lessons.first().whiteboard
                    else:
                        if request.user.is_authenticated():
                            demo_whiteboard_objects = DemoWhiteboard.objects.filter(email_or_session_key=request.user.email)
                            if demo_whiteboard_objects.exists():
                                whiteboard = demo_whiteboard_objects.first().whiteboard
                        else:
                            demo_whiteboard_objects = DemoWhiteboard.objects.filter(email_or_session_key=request.session.session_key)
                            if demo_whiteboard_objects.exists():
                                whiteboard = demo_whiteboard_objects.first().whiteboard
                    if whiteboard:
                        sticky_note = StickyNote()
                        if request.user.is_authenticated():
                            sticky_note.user = request.user
                        sticky_note.serial_id = id
                        sticky_note.content = content if content else ""
                        sticky_note.top = top
                        sticky_note.left = left
                        sticky_note.save()

                        whiteboard.sticky_notes.add(sticky_note)


                response["status"] = "SUCCESS"
                response["message"] = "Successful"
                return self.render_to_json(response)
            elif action == "SAVE":
                id = request.POST.get("id")
                lesson_id = request.POST.get("lesson_id")
                wb_id = request.POST.get("wb_id")
                content = request.POST.get("content")
                top = request.POST.get("top")
                left = request.POST.get("left")
                try:
                    top = float(top)
                    left = float(left)
                except Exception as msg:
                    response["status"] = "FAILURE"
                    response["message"] = "Co-ordinates must be a valid number"
                    return self.render_to_json(response)

                whiteboard = None
                if wb_id:
                    wboards = Whiteboard.objects.filter(pk=int(wb_id))
                    if wboards.exists():
                        whiteboard = wboards.first()
                        sticky_note_objects = whiteboard.sticky_notes.filter(serial_id=id)
                        if sticky_note_objects.exists():
                            sticky_note = sticky_note_objects.first()
                            sticky_note.content = content.strip()
                            sticky_note.top = top
                            sticky_note.left = left
                            sticky_note.modified_date = Clock.utc_now()
                            sticky_note.save()
                            response["status"] = "SUCCESS"
                            response["message"] = "Successful"
                            return self.render_to_json(response)

                else:
                    response["status"] = "FAILURE"
                    response["message"] = "Note cannot be saved"
                    return self.render_to_json(response)
            elif action == "DELETE":
                id = request.POST.get("id")
                lesson_id = request.POST.get("lesson_id")
                wb_id = request.POST.get("wb_id")
                if wb_id:
                    wboards = Whiteboard.objects.filter(pk=int(wb_id))
                    if wboards.exists():
                        whiteboard = wboards.first()
                        sticky_note_objects = whiteboard.sticky_notes.filter(serial_id=id)
                        if sticky_note_objects.exists():
                            #whiteboard.sticky_notes.remove(sticky_note_objects.first())
                            sticky_note_objects.first().delete()
                        response["status"] = "SUCCESS"
                        response["message"] = "Successful"
                        return self.render_to_json(response)
                else:
                    response["status"] = "FAILURE"
                    response["message"] = "Note cannot be deleted"
                    return self.render_to_json(response)

            elif action == "SAVE_LOCK":
                id = request.POST.get("id")
                lesson_id = request.POST.get("lesson_id")
                wb_id = request.POST.get("wb_id")
                locked = request.POST.get("locked")
                if wb_id:
                    wboards = Whiteboard.objects.filter(pk=int(wb_id))
                    if wboards.exists():
                        whiteboard = wboards.first()
                        sticky_note_objects = whiteboard.sticky_notes.filter(serial_id=id)
                        if sticky_note_objects.exists():
                            sticky_note = sticky_note_objects.first()
                            sticky_note.is_locked = True if locked == "1" else False
                            sticky_note.modified_date = Clock.utc_now()
                            sticky_note.save()
                            response["status"] = "SUCCESS"
                            response["message"] = "Successful"
                            return self.render_to_json(response)
                else:
                    response["status"] = "FAILURE"
                    response["message"] = "Note cannot be updated."
                    return self.render_to_json(response)

            elif action == "SAVE_POS":
                id = request.POST.get("id")
                lesson_id = request.POST.get("lesson_id")
                wb_id = request.POST.get("wb_id")
                top = request.POST.get("top")
                left = request.POST.get("left")
                try:
                    top = float(top)
                    left = float(left)
                except Exception as msg:
                    response["status"] = "FAILURE"
                    response["message"] = "Co-ordinates must be a valid number"
                    return self.render_to_json(response)
                whiteboard = None
                if wb_id:
                    wboards = Whiteboard.objects.filter(pk=int(wb_id))
                    if wboards.exists():
                        whiteboard = wboards.first()
                        sticky_note_objects = whiteboard.sticky_notes.filter(serial_id=id)
                        if sticky_note_objects.exists():
                            sticky_note = sticky_note_objects.first()
                            sticky_note.top = top
                            sticky_note.left = left
                            sticky_note.modified_date = Clock.utc_now()
                            sticky_note.save()
                            response["status"] = "SUCCESS"
                            response["message"] = "Successful"
                            return self.render_to_json(response)
                else:
                    response["status"] = "FAILURE"
                    response["message"] = "Note cannot be saved"
                    return self.render_to_json(response)
            else:
                response["status"] = "FAILURE"
                response["message"] = "Invalid action specified"
                return self.render_to_json(response)
        else:
            response["status"] = "FAILURE"
            response["message"] = "Invalid request"
            return self.render_to_json(response)

class VerificationAjaxView(JSONResponseMixin,View):
    # @method_decorator(csrf_exempt)
    # def dispatch(self, *args, **kwargs):
    #     return super(VerificationAjaxView, self).dispatch(*args, **kwargs)

    def get(self,request,*args,**kwargs):
        title = request.GET.get("title")
        if title == "nid":
            title = "National ID Card Verification"
        context = {
            "form_title": title
        }
        return render(request,"ajax/verification.html",context)

    def post(self,request,*args,**kwargs):
        response = {
            "status": "FAILURE",
            "message": "FAILURE",
            "data": []
        }
        # if request.is_ajax():
        try:
            file = request.FILES.get("ver_file")
            nid_name = request.POST.get("nid_name")
            nid_number = request.POST.get("nid_number")
            file_name = str(request.user.id)+"."+file._name[file._name.rindex(".")+1:]
            with open(os.path.join(settings.NID_FILES_DIR, file_name), 'wb+') as destination:
                for chunk in file.chunks():
                    destination.write(chunk)
            img_file = File(open(os.path.join(settings.NID_FILES_DIR, file_name)))
            nric_ver_obj,created = NRICVerification.objects.get_or_create(user=request.user)
            nric_ver_obj.nid_scanned_img = img_file
            nric_ver_obj.status = VerificationStatus.submitted.value
            if nid_name:
                nric_ver_obj.nid_name = nid_name
            if nid_number:
                nric_ver_obj.nid_number = nid_number
            nric_ver_obj.file_name = file._name
            # nric_ver_obj.create_date = datetime.now()
            # nric_ver_obj.modified_date = datetime.now()
            nric_ver_obj.save()
            response["status"] = "SUCCESS"
            response["message"] = "Successful"
            return self.render_to_json(response)
        except Exception as msg:
            response["status"] = "FAILURE"
            response["message"] = "Internal Server Error"
            return self.render_to_json(response)

class CertVerificationAjaxView(JSONResponseMixin,View):

    def get(self,request,*args,**kwargs):
        title = request.GET.get("title")
        if title == "nid":
            title = "Certificates Verification"
        context = {
            "form_title": title
        }
        return render(request,"ajax/certificate_verification_dialog.html",context)

    def post(self,request,*args,**kwargs):
        response = {
            "status": "FAILURE",
            "message": "FAILURE",
            "data": []
        }
        # if request.is_ajax():
        try:
            file = request.FILES.get("ver_file")
            #printfile.__dict__
            #printsettings.MEDIA_ROOT
            file_name = str(request.user.id)+"."+file._name[file._name.rindex(".")+1:]
            #printfile_name
            with open(os.path.join(settings.NID_FILES_DIR, file_name), 'wb+') as destination:
                for chunk in file.chunks():
                    destination.write(chunk)
            img_file = File(open(os.path.join(settings.NID_FILES_DIR, file_name)))
            nric_ver_obj,created = NRICVerification.objects.get_or_create(user=request.user)
            nric_ver_obj.nid_scanned_img = img_file
            nric_ver_obj.status = VerificationStatus.submitted.value
            # nric_ver_obj.create_date = datetime.now()
            # nric_ver_obj.modified_date = datetime.now()
            nric_ver_obj.save()
            response["status"] = "SUCCESS"
            response["message"] = "Successful"
            return self.render_to_json(response)
        except Exception as msg:
            response["status"] = "FAILURE"
            response["message"] = "Internal Server Error"
            return self.render_to_json(response)

class SubjectRateAjaxView(JSONResponseMixin, TemplateView):
    template_name = "ajax/profile/tutor/subject_rates.html"

    def get_context_data(self, **kwargs):
        context = super(SubjectRateAjaxView, self).get_context_data(**kwargs)
        user_level_names = SubjectUtil.get_levels_for_country(country_name=self.request.user.champuser.nationality.name)
        context["levels"] = SubjectLevel.objects.filter(name__in=user_level_names)
        context["currencies"] = CurrencyUtil.get_currencies_for_user(self.request.user.pk)
        user_currency = CurrencyUtil.get_currency_code_for_country(self.request.user.champuser.nationality.name)
        context["user_currency"] = user_currency

        user_major_objects = UserMajor.objects.filter(user_id=self.request.user.pk).select_related('major').select_related('level')
        subject_dict = {}
        level_objects = {}
        for user_major in user_major_objects:
            if not user_major.major.level.pk in subject_dict:
                subject_dict[user_major.major.level.pk] = [ user_major ]
            else:
                subject_dict[user_major.major.level.pk] += [ user_major ]

            if not user_major.major.level.pk in level_objects:
                level_objects[user_major.major.level.pk] = user_major.major.level

        subject_rates = []
        for level_id, user_major_list in subject_dict.items():
            rate_entries = {}
            only_subject_ids = []
            for user_major in user_major_list:
                try:
                    if user_major.currency:
                        if not user_major.currency.code +"_"+ str(user_major.rate) in rate_entries:
                            rate_entries[user_major.currency.code +"_"+ str(user_major.rate)] = [
                                user_major.major.pk
                            ]
                        else:
                            rate_entries[user_major.currency.code +"_"+ str(user_major.rate)] += [
                                user_major.major.pk
                            ]
                    else:
                        only_subject_ids += [ user_major.major.pk ]
                except Exception as exp:
                    pass
            for key, items in rate_entries.items():
                try:
                    subject_rates += [
                        {
                            "level": level_id,
                            "subjects": items,
                            "all_subjects": Major.objects.filter(level_id=level_id),
                            "rate": Decimal(key.split("_")[1]),
                            "currency": key.split("_")[0]
                        }
                    ]
                except Exception as exp:
                    pass

            subject_rates += [
                {
                    "level": level_id,
                    "subjects": only_subject_ids,
                    "all_subjects": Major.objects.filter(level_id=level_id),
                    "rate": Decimal(0),
                    "currency": None
                }
            ]


        context["subject_rates"] = subject_rates

        return context

    def post(self, request, *args, **kwargs):
        with transaction.atomic():
            post_data = request.POST
            data_dict = {}
            for key, value in post_data.items():
                try:
                    index = key[key.index("_") + 1:key.rindex("_")]
                    if "levels" in key:
                        if not data_dict.get(index):
                            data_dict[index] = {
                                "levels": post_data.getlist(key)
                            }
                        else:
                            data_dict[index]["levels"] = post_data.getlist(key)
                    elif "subjects" in key:
                        if not data_dict.get(index):
                            data_dict[index] = {
                                "subjects": post_data.getlist(key)
                            }
                        else:
                            data_dict[index]["subjects"] = post_data.getlist(key)
                    elif "rate" in key:
                        if not data_dict.get(index):
                            data_dict[index] = {
                                "rates": post_data.getlist(key)
                            }
                        else:
                            data_dict[index]["rates"] = post_data.getlist(key)
                    elif "currency" in key:
                        if not data_dict.get(index):
                            data_dict[index] = {
                                "currencies": post_data.getlist(key)
                            }
                        else:
                            data_dict[index]["currencies"] = post_data.getlist(key)
                except Exception as exp:
                    pass

            user_subjects = []
            for index, data_item in data_dict.items():
                if data_item.get('currencies') and data_item.get('rates') and data_item.get('subjects'):
                    currency = data_item['currencies'][0]
                    rate = data_item['rates'][0]
                    subjects = data_item['subjects']
                    for subject in subjects:
                        user_major_objects = UserMajor.objects.filter(user_id=request.user.pk, major_id=int(subject))
                        if user_major_objects.exists():
                            user_major_object = user_major_objects.first()
                        else:
                            user_major_object = UserMajor()
                            user_major_object.user_id = request.user.pk
                            user_major_object.major_id = Major.objects.filter(pk=int(subject)).first().pk
                        user_major_object.currency_id = Currency.objects.filter(code=currency).first().pk
                        user_major_object.rate = Decimal(rate)
                        user_major_object.save()
                        user_subjects += [ int(subject) ]
            UserMajor.objects.filter(user_id=request.user.pk).exclude(major_id__in=user_subjects).delete()
        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": ""
        }
        return self.render_to_json(response)

class StudentPrivateInfo(JSONResponseMixin,View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(StudentPrivateInfo, self).dispatch(*args, **kwargs)

    def get(self,request,*args,**kwargs):
        template = loader.get_template("ajax/student_profile_private_info_edit.html")
        birthdate = datetime.fromtimestamp(request.user.champuser.birthdate).strftime("%d/%m/%Y") if request.user.champuser.birthdate else None
        form_data = {
            "email": request.user.email,
            "gender": request.user.champuser.gender,
            "timezone": request.user.champuser.timezone,
            "birthdate": birthdate,
            "religion": request.user.champuser.religion,
            "nationality": request.user.champuser.nationality.name if request.user.champuser.nationality else None
        }
        languages = request.user.champuser.languages.all()
        langs = [(ul.language.pk,ul.language.name) for ul in languages]
        lang_vals = [str(pk) for pk,lan in langs]
        langs_val = ",".join(lang_vals)
        form = StudentPrivateInfoForm(form_data)
        cntxt = Context({ 'form': form, "langs":langs, "langs_val": langs_val })
        rendered = template.render(cntxt)
        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": rendered
        }
        return self.render_to_json(response)
    def post(self,request,*args,**kwargs):
        response = {
            "status": "FAILURE",
            "message": "FAILURE",
            "data": []
        }

        email = request.POST.get("email")
        old_password = request.POST.get("old_password")
        # new_password = request.POST.get("new_password")
        # new_password_again = request.POST.get("new_password_again")
        gender = request.POST.get("gender")
        birthdate = request.POST.get("birthdate")
        religion = request.POST.get("religion")
        hidden_languages = request.POST.get("hidden_languages")
        nationality = request.POST.get("nationality")
        ethnicity = request.POST.get("ethnicity")
        timezone = request.POST.get("timezone")

        user = authenticate(username=email, password=old_password)
        if not user:
            response["status"] = "FAILURE"
            response["message"] = "Invalid password"
            response["data"] = ""
            return self.render_to_json(response)

        try:
            if birthdate:
                birthdate = datetime.strptime(birthdate,"%m/%d/%Y")
        except Exception as msg:
            response["status"] = "FAILURE"
            response["message"] = "Invalid birthdate"
            response["data"] = ""
            return self.render_to_json(response)

        champ_user_object = ChampUser.objects.get(user=request.user)
        champ_user_object.gender = gender.strip()
        champ_user_object.timezone = timezone.strip()
        champ_user_object.ethnicity = ethnicity.strip()
        champ_user_object.nationality_id = int(nationality)
        champ_user_object.religion = religion
        if birthdate:
            if timezone:
                champ_user_object.birthdate = birthdate.date()
            else:
                champ_user_object.birthdate = birthdate.date()
        else:
            champ_user_object.birthdate = None
        champ_user_object.save()

        langs = hidden_languages.split(",")
        langs = [int(lang) for lang in langs if lang]
        user_languages = []
        for lang in langs:
            languages = request.user.champuser.languages.all()
            if not languages.filter(language__pk=lang):
                lang_prof_obj = LanguageProficiency()
                lang_prof_obj.language_id = lang
                lang_prof_obj.save()
                request.user.champuser.languages.add(lang_prof_obj)
        request.user.champuser.languages.all().exclude(language__pk__in=langs).delete()

        response["status"] = "SUCCESS"
        response["message"] = "Successful"
        response["data"] = {}
        return self.render_to_json(response)

class StudentEducationAjaxView(JSONResponseMixin,View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(StudentEducationAjaxView, self).dispatch(*args, **kwargs)

    def get(self,request,*args,**kwargs):
        edu_id = request.GET.get("edu_id")
        education_object = Education.objects.get(pk=int(edu_id))
        education_form_data = {
            "hidden_pk": education_object.pk,
            "school_type": education_object.institution.type,
            "hidden_school": education_object.institution.pk,
            "school_name": education_object.institution.name,
            "is_current": education_object.is_current,
            "location": education_object.location,
            "date_attended": datetime.strptime(datetime.fromtimestamp(education_object.date_attended).date().strftime("%m/%d/%Y"),"%m/%d/%Y").date(),
            "hidden_major": education_object.major.pk,
            "major": education_object.major.name
        }

        education_form = StudentEducationForm(education_form_data);

        template = loader.get_template("ajax/student_add_education_form.html")
        date_attended = datetime.fromtimestamp(education_object.date_attended).strftime("%m/%d/%Y")
        cntxt = Context({"education_form": education_form, "date_attended": date_attended})
        rendered = template.render(cntxt)

        response = {}
        response["status"] = "SUCCESS"
        response["message"] = "Successful"
        response["data"] = rendered
        return self.render_to_json(response)

    def post(self,request,*args,**kwargs):
        response = {
            "status": "FAILURE",
            "message": "FAILURE",
            "data": []
        }

        edu_form = StudentEducationForm(request.POST)
        if not edu_form.is_valid():
            response["data"] = ""
            return self.render_to_json(response)
        id = int(edu_form.cleaned_data['hidden_pk'])
        school_type = edu_form.cleaned_data["school_type"]
        school_id = int(edu_form.cleaned_data['hidden_school'])
        school_name = edu_form.cleaned_data["school_name"]
        is_current = edu_form.cleaned_data["is_current"]
        location = edu_form.cleaned_data["location"]
        date_attended = edu_form.cleaned_data["date_attended"]
        major_id = int(edu_form.cleaned_data['hidden_major'])
        major = edu_form.cleaned_data["major"]

        if id == -1:
            education_object = Education()
            education_object.user = request.user
        else:
            education_object = Education.objects.get(pk=id)

        if school_id == -1:
            school_object = Schools()
            school_object.name = school_name
            school_object.type = school_type
            school_object.added_by = request.user
            school_object.save()
        else:
            school_object = Schools.objects.get(pk=school_id)

        if major_id == -1:
            major_object = Major()
            major_object.name = major
            major_object.description = ''
            major_object.creator = request.user
            major_object.save()
        else:
            major_object = Major.objects.get(pk=major_id)

        education_object.institution = school_object
        education_object.is_current = is_current
        education_object.location = location
        education_object.major = major_object
        education_object.date_attended = Clock.convert_local_to_utc_timestamp(request.user.champuser.timezone,time.mktime(date_attended.timetuple()))
        education_object.save()

        template = loader.get_template("ajax/student_education_row.html")

        cntxt = Context({"education": education_object})
        rendered = template.render(cntxt)

        response["status"] = "SUCCESS"
        response["message"] = "Successful"
        response["data"] = rendered
        response["append"] = (id == -1)
        return self.render_to_json(response)

class StudentCoverPictureAjaxView(JSONResponseMixin,View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(StudentCoverPictureAjaxView, self).dispatch(*args, **kwargs)

    def get(self,request,*args,**kwargs):
        pass

    def post(self,request,*args,**kwargs):
        image_data = request.POST.get('image-data')
        image_data_split = image_data.split(',')
        image_data = image_data_split[1]
        image_format = image_data_split[0].replace('data:','').replace(';base64','').replace('image/','')
        imgdata = base64.b64decode(image_data)
        file_name = 'c-'+str(request.user.pk)+'-'+str(uuid.uuid4())+'.'+image_format
        file_path = os.path.join(settings.IMAGE_DIR, file_name)
        with open(file_path, 'wb+') as f:
            f.write(imgdata)

        if request.user.champuser.cover_photo:
            cp = request.user.champuser.cover_photo
        else:
            cp = CoverPhoto()

        cp.image_field = file_name
        cp.cropping = '0,17,200,183'
        cp.save()

        request.user.champuser.cover_photo = cp
        request.user.champuser.save()

        response = {}
        response["status"] = "SUCCESS"
        response["message"] = "Successful"
        response["data"] = file_path
        return self.render_to_json(response)

class StudentAboutSectionAjaxView(JSONResponseMixin,View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(StudentAboutSectionAjaxView, self).dispatch(*args, **kwargs)

    def post(self,request,*args,**kwargs):

        response = {
            "status": "FAILURE",
            "message": "FAILURE",
            "data": []
        }

        name = request.POST.get("student_name")
        hometown = request.POST.get("student_hometown")

        if not name:
            response["status"] = "FAILURE"
            response["message"] = "Name missing"
            response["data"] = ""
            return self.render_to_json(response)

        request.user.champuser.fullname = name
        request.user.champuser.hometown = hometown
        request.user.champuser.save()

        response["status"] = "SUCCESS"
        response["message"] = "Successful"
        response["data"] = ""
        return self.render_to_json(response)

class LoadDrawingboardFXAjaxView(JSONResponseMixin,View):
    def get(self,request,*args,**kwargs):
        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": ""
        }
        template = loader.get_template("ajax/drawingboard_fxmenu.html")

        cntxt = Context({"request": request})
        rendered = template.render(cntxt)
        response["data"] = rendered
        #print(rendered)
        return self.render_to_json(response)


class QLikeAjaxView(JSONResponseMixin,View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(QLikeAjaxView, self).dispatch(*args, **kwargs)

    def post(self,request,*args,**kwargs):
        label = request.POST.get("label")
        id = request.POST.get("id")
        qid = request.POST.get("qid")
        action = request.POST.get("action")
        response = {
                "status": "SUCCESS",
                "message": "Successful",
                "data": "",
                "like": 0,
                "dislike": 0
            }
        try:
            if label == "answer":
                qanswer = QuestionAnswer.objects.get(pk=int(id))
                if qanswer.answer_by != request.user:
                    user_qans_like = qanswer.likes.filter(created_by_id=request.user.pk).first()
                    # if not qanswer.likes.filter(created_by_id=request.user.pk).exists():
                    if not user_qans_like:
                        qans_like = QAnswerLike()
                        qans_like.created_by_id = request.user.pk
                        if action == "like":
                            qans_like.value = 1
                            response["like"] = 1
                        elif action == "dislike":
                            qans_like.value = 2
                            response["dislike"] = 1
                        # qans_like.value = 1 if action == "like" else 2
                        qans_like.save()
                        qanswer.likes.add(qans_like)
                        self.send_like_notification(qid, request.user, action, qanswer)
                    else:
                        if action == "like" and user_qans_like.value == 2:
                            user_qans_like.value = 1
                            user_qans_like.save()
                            response.update({"dislike": -1, "like": 1})
                            self.send_like_notification(qid, request.user, action, qanswer)
                        elif action == "dislike" and user_qans_like.value == 1:
                            user_qans_like.value = 2
                            user_qans_like.save()
                            response.update({"dislike": 1, "like": -1})
                            self.send_like_notification(qid, request.user, action, qanswer)

            elif label == "comment":
                qcomment = QuestionComment.objects.get(pk=int(id))
                if not qcomment.likes.filter(created_by_id=request.user.pk).exists():
                    qans_like = QCommentLike()
                    qans_like.created_by_id = request.user.pk
                    qans_like.value.value = 1 if action == "like" else 2
                    qans_like.save()
                    qcomment.likes.add(qans_like)
            # response = {
            #     "status": "SUCCESS",
            #     "message": "Successful",
            #     "data": ""
            # }
            return self.render_to_json(response)
        except Exception as e:
            response.update({"status": "FAILURE", "message": "Failed",})
            # response = {
            #     "status": "FAILURE",
            #     "message": "Failed",
            #     "data": ""
            # }
            return self.render_to_json(response)

    def send_like_notification(self, qid, user, action, answer_object):
        question = Question.objects.get(pk=int(qid))
        target_users = [question.created_by.pk] if question.created_by.pk != user.pk else []
        for answer in question.answers.all():
            if answer.answer_by.pk != user.pk and answer.answer_by.pk not in target_users:
                target_users += [answer.answer_by.pk]
            for like in answer.likes.all():
                if like.created_by.pk != user.pk and like.created_by.pk not in target_users:
                    target_users += [like.created_by.pk]
        for user_id in target_users:
            NotificationManager.push_notification("question_reply_like", question.pk, user, user_id, action=action, answer=answer_object)


class LessonPostAction(JSONResponseMixin,View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(LessonPostAction, self).dispatch(*args, **kwargs)

    def post(self,request,*args,**kwargs):
        action = request.POST.get("action")
        job_id = request.POST.get("job_id")
        job_id = int(job_id)

        job = None
        job_objects = Job.objects.filter(pk=job_id)
        if job_objects.exists():
            job = job_objects.first()
        if not job:
            response = {
                "status": "FAILURE",
                "message": "Job not found",
                "data": ""
            }
            return self.render_to_json(response)

        if action == "ACCEPT":
            lesson_type = job.lesson_type #0 for live lesson and 1 for written lesson
            if lesson_type == LessonTypes.live_lesson.value:
                if not job.lesson:
                    live_lesson = None
                    try:
                        job = job_objects.first()
                        ###Create the live lesson.
                        live_lesson = LiveLesson()
                        live_lesson.user_id = job.posted_by.pk
                        #live_lesson.timezone = lesson_request_timezone
                        live_lesson.request_user_id = request.user.pk
                        live_lesson.lesson_time = job.lesson_time
                        live_lesson.duration = job.lesson_duration
                        live_lesson.course_id = Major.objects.filter(name=job.preference.subjects.all().first().value).first().pk
                        live_lesson.message = job.description
                        live_lesson.save()

                        whiteboard = Whiteboard()
                        whiteboard.access = WhiteboardAccess.protected.value
                        whiteboard.created_by_id = job.posted_by.pk
                        whiteboard.name = uuid.uuid4()
                        whiteboard.unique_id = uuid.uuid4()
                        whiteboard.save()
                        whiteboard.users.add(live_lesson.user)
                        whiteboard.users.add(live_lesson.request_user)

                        live_lesson.approved = ApprovalStatus.approved.value
                        live_lesson.whiteboard = whiteboard
                        live_lesson.save()

                        job.lesson_id = live_lesson.pk
                        job.save()

                        job_invitation_objects = JobInvitation.objects.filter(user_id=request.user.pk,job_id=job.pk)
                        if job_invitation_objects.exists():
                            ji = job_invitation_objects.first()
                            ji.status = JobInvitationStatus.accepted.value
                            ji.save()

                        # ###Create Pad Author and group.
                        pad_util = PadUtil()
                        lesson_requested_by_author_id = pad_util.create_or_get_padauthor(live_lesson.user.id,live_lesson.user.champuser.fullname)
                        #print"Pad Author Created. Author ID: "+lesson_requested_by_author_id

                        lesson_requested_to_author_id = pad_util.create_or_get_padauthor(live_lesson.request_user.id,live_lesson.request_user.champuser.fullname)
                        #print"Pad Author Created. Author ID: "+lesson_requested_to_author_id

                        pad_group_id = pad_util.create_or_get_padgroup(live_lesson.pk,user_id=request.user.pk)
                        #print"Pad Group Created. Pad Group ID: "+pad_group_id

                        pad_id = pad_util.create_group_pad(pad_group_id,user_id=request.user.pk,author_id=lesson_requested_by_author_id)
                        pad_object = Pad.objects.get(pad_id=pad_id)
                        pad_author = PadAuthor.objects.filter(author_id=lesson_requested_to_author_id)
                        if pad_author.exists():
                            pad_object.pad_authors.add(pad_author.first())
                        whiteboard.text_pads.add(pad_object)

                        codepad_util = CodePadUtil()
                        code_pad_obj = codepad_util.create_codepad(str(live_lesson.pk),"Javascript",creator=request.user)

                        whiteboard.code_pads.add(code_pad_obj)

                        whiteboard_user1_permission = WhiteboardUserPermission()
                        whiteboard_user1_permission.user = live_lesson.user
                        whiteboard_user1_permission.save()

                        whiteboard_user2_permission = WhiteboardUserPermission()
                        whiteboard_user2_permission.user = live_lesson.request_user
                        whiteboard_user2_permission.save()

                        text_pad_permission = TextPadPermission()
                        text_pad_permission.text_pad = pad_object
                        text_pad_permission.permission = Permission.all.value
                        text_pad_permission.save()

                        text_pad_permission2 = TextPadPermission()
                        text_pad_permission2.text_pad = pad_object
                        text_pad_permission2.permission = Permission.all.value
                        text_pad_permission2.save()

                        code_pad_permission = CodePadPermission()
                        code_pad_permission.code_pad = code_pad_obj
                        code_pad_permission.permission = Permission.all.value
                        code_pad_permission.save()

                        code_pad_permission2 = CodePadPermission()
                        code_pad_permission2.code_pad = code_pad_obj
                        code_pad_permission2.permission = Permission.all.value
                        code_pad_permission2.save()

                        whiteboard_user1_permission.code_pad_permissions.add(code_pad_permission)
                        whiteboard_user2_permission.code_pad_permissions.add(code_pad_permission)

                        whiteboard_user1_permission.text_pad_permissions.add(text_pad_permission)
                        whiteboard_user2_permission.text_pad_permissions.add(text_pad_permission2)

                        drawingboard = DrawingBoard()
                        drawingboard.name = "1"
                        drawingboard.numeric_id = 1
                        drawingboard.save()

                        DrawingboardLastNid.objects.filter(group=str(live_lesson.pk)).delete()

                        dblastnid = DrawingboardLastNid()
                        dblastnid.group = str(live_lesson.pk)
                        dblastnid.nid = 1
                        dblastnid.save()

                        whiteboard.drawingboards.add(drawingboard)


                        drawingboard_permission = DrawingboardPermission()
                        drawingboard_permission.drawingboard = drawingboard
                        drawingboard_permission.permission = Permission.all.value
                        drawingboard_permission.save()

                        drawingboard_permission2 = DrawingboardPermission()
                        drawingboard_permission2.drawingboard = drawingboard
                        drawingboard_permission2.permission = Permission.all.value
                        drawingboard_permission2.save()

                        whiteboard_user1_permission.drawingboard_permissions.add(drawingboard_permission)
                        whiteboard_user2_permission.drawingboard_permissions.add(drawingboard_permission2)

                        whiteboard.permissions.add(whiteboard_user1_permission)
                        whiteboard.permissions.add(whiteboard_user2_permission)
                        
                        calendar_event = CalendarEvent()
                        calendar_event.title = "Live lesson"
                        calendar_event.css_class = "event-success"
                        calendar_event.start_timestamp = live_lesson.lesson_time
                        calendar_event.end_timestamp = live_lesson.lesson_time + live_lesson.duration
                        calendar_event.user_id = live_lesson.user.pk
                        calendar_event.url = reverse("live_lesson",kwargs={"pk": live_lesson.pk})
                        calendar_event.start = datetime.fromtimestamp(live_lesson.lesson_time)
                        calendar_event.end = datetime.fromtimestamp(live_lesson.lesson_time + live_lesson.duration)
                        calendar_event.save()
        
                        calendar_event = CalendarEvent()
                        calendar_event.title = "Live lesson"
                        calendar_event.css_class = "event-success"
                        calendar_event.start_timestamp = live_lesson.lesson_time
                        calendar_event.end_timestamp = live_lesson.lesson_time + live_lesson.duration
                        calendar_event.user_id = live_lesson.request_user.pk
                        calendar_event.url = reverse("live_lesson",kwargs={"pk": live_lesson.pk})
                        calendar_event.start = datetime.fromtimestamp(live_lesson.lesson_time)
                        calendar_event.end = datetime.fromtimestamp(live_lesson.lesson_time + live_lesson.duration)
                        calendar_event.save()
                        
                        response = {}
                        response["status"] = "SUCCESS"
                        response["message"] = "Successful"
                        response["data"] = { "lesson_type": "live" }
                        return self.render_to_json(response)
                    except Exception as msg:
                        #print(str(msg))
                        if live_lesson:
                            live_lesson.approved = ApprovalStatus.not_approved.value
                            if whiteboard:
                                whiteboard.delete()
                            live_lesson.whiteboard = None
                            live_lesson.save()
                            job.lesson = None
                            job.save()
                        response["status"] = "FAILURE"
                        response["message"] = "Failed to approve"
                        response["data"] = []
                        return self.render_to_json(response)
            elif lesson_type == LessonTypes.written.value:
                if not job.lesson:
                    written_lesson = WrittenLesson()
                    written_lesson.user_id = job.posted_by.pk
                    written_lesson.request_user_id = request.user.pk
                    written_lesson.due_date = job.lesson_time
                    written_lesson.duration = job.lesson_duration
                    written_lesson.course_id = Major.objects.filter(name=job.preference.subjects.all().first().value).first().pk
                    written_lesson.message = job.description
                    written_lesson.save()
                    for a in job.job_attachment.all():
                        wr_attachment = WrittenLessonAttachment()
                        wr_attachment.attachment_name = a.attachment_name
                        wr_attachment.attachment = a.attachment
                        wr_attachment.save()
                        written_lesson.attachments.add(wr_attachment)

                    job.lesson_id = written_lesson.pk
                    job.save()

                    job_invitation_objects = JobInvitation.objects.filter(user_id=request.user.pk,job_id=job.pk)
                    if job_invitation_objects.exists():
                        ji = job_invitation_objects.first()
                        ji.status = JobInvitationStatus.accepted.value
                        ji.save()

                response = {
                    "status": "SUCCESS",
                    "message": "Successful",
                    "data": { "lesson_type": "written" }
                }
                return self.render_to_json(response)

        elif action == "REJECT":
            job.status = LessonStatus.rejected.value
            job.save()
            job_invitation_objects = JobInvitation.objects.filter(user_id=request.user.pk,job_id=job.pk)
            if job_invitation_objects.exists():
                ji = job_invitation_objects.first()
                ji.status = JobInvitationStatus.rejected.value
                ji.save()
            response = {
                "status": "SUCCESS",
                "message": "Successful",
                "data": ""
            }
            return self.render_to_json(response)

class UpdateAvailableStatus(JSONResponseMixin, View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(UpdateAvailableStatus, self).dispatch(*args, **kwargs)

    def post(self,request,*args,**kwargs):
        if request.user.is_authenticated():
            status = request.POST.get("status")
            if status == "ready_to_teach":
                champ_user = request.user.champuser
                champ_user.available_to_teach = AvailableStatus.ready_to_teach.value
                champ_user.save()
            elif status == "ready_for_written_lesson":
                champ_user = request.user.champuser
                champ_user.available_to_teach = AvailableStatus.ready_for_written_lesson.value
                champ_user.save()
            elif status == "ready_for_both":
                champ_user = request.user.champuser
                champ_user.available_to_teach = AvailableStatus.ready_for_both.value
                champ_user.save()
            elif status == "unavailable":
                champ_user = request.user.champuser
                champ_user.available_to_teach = AvailableStatus.unavailable.value
                champ_user.save()
            elif status == "profile_deactivated":
                champ_user = request.user.champuser
                champ_user.available_to_teach = AvailableStatus.profile_deactivated.value
                champ_user.save()
            response = {
                "status": "SUCCESS",
                "message": "Successful",
                "data": ""
            }
            return self.render_to_json(response)
        else:
            response = {
                "status": "FAILURE",
                "message": "Failed",
                "data": ""
            }
            return self.render_to_json(response)

class ProfileSectionAjaxView(JSONResponseMixin,View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ProfileSectionAjaxView, self).dispatch(*args, **kwargs)

    def get_profile_context(self,request, user_id):
        context = {}

        context['thumbnail_url'] = self.object.profile_picture.image_field if self.object.profile_picture else ''
        if Profile.objects.filter(user=self.object.user).exists():
            context['profile'] = Profile.objects.filter(user_id=self.object.user.pk).first()
        else:
             context['profile'] = Profile.objects.create(user_id=self.object.user.pk)

        context["champ_user"] = self.object
        context["object"] = self.object

        nric_verification_objs = NRICVerification.objects.filter(user_id=self.object.user.pk)
        profile_verification_status = 0
        if nric_verification_objs:
            profile_verification_status = nric_verification_objs[0].status

        context["profile_verification_status"] = profile_verification_status

        user_major_objects = UserMajor.objects.filter(user_id=self.object.user.pk).select_related('major').select_related('level')
        subject_dict = {}
        level_objects = {}
        for user_major in user_major_objects:
            if not user_major.major.level.pk in subject_dict:
                subject_dict[user_major.major.level.pk] = [ user_major ]
            else:
                subject_dict[user_major.major.level.pk] += [ user_major ]

            if not user_major.major.level.pk in level_objects:
                level_objects[user_major.major.level.pk] = user_major.major.level

        subject_rates = []
        for level_id, user_major_list in subject_dict.items():
            rate_entries = {}
            only_subject_entries = []
            for user_major in user_major_list:
                if user_major.currency:
                    if not user_major.currency.code +"_"+ str(user_major.rate) in rate_entries:
                        rate_entries[user_major.currency.code +"_"+ str(user_major.rate)] = [
                            user_major.major.name
                        ]
                    else:
                        rate_entries[user_major.currency.code +"_"+ str(user_major.rate)] += [
                            user_major.major.name
                        ]
                else:
                    if not user_major.major.name in only_subject_entries:
                        only_subject_entries += [ user_major.major.name ]
            for key, items in rate_entries.items():
                subject_rates += [
                    {
                        "level": level_objects[level_id].name,
                        "subjects": ", ".join([ ('<a href="%s">%s</a>' % (reverse("tutor_search")+"?subject=%s" % item, item)) for item in items ]),
                        "rate": float(key.split("_")[1]),
                        "currency": key.split("_")[0]
                    }
                ]
            subject_rates += [
                {
                    "level": level_objects[level_id].name,
                    "subjects": ", ".join([ ('<a href="%s">%s</a>' % (reverse("tutor_search")+"?subject=%s" % item, item)) for item in only_subject_entries ]),
                    "rate": None,
                    "currency": None
                }
            ]

        subject_rates1 = []
        subject_rates2 = []

        for index, item in enumerate(subject_rates):
            if index % 2 == 0:
                subject_rates1 += [ item ]
            else:
                subject_rates2 += [ item ]

        context["subject_rates"] = subject_rates1
        context["subject_rates2"] = subject_rates2

        teaching_exp_objs = TeachingExperiences.objects.filter(user_id=self.object.user.pk)

        if teaching_exp_objs:
            context["teaching_experiences"] = teaching_exp_objs[0]
        else:
            context["teaching_experiences"] = None

        extra_curricular_interest_objs = ExtraCurricularInterest.objects.filter(user_id=self.object.user.pk)
        if extra_curricular_interest_objs:
            context["extra_curricular_interest"] = extra_curricular_interest_objs[0]
        else:
            context["extra_curricular_interest"] = None

        educations = Education.objects.filter(user_id=self.object.user.pk)
        context["educations"] = educations

        user_top_courses = UserTopCourses.objects.filter(user_id=self.object.user.pk)
        context["top_courses"] = user_top_courses

        Rate.initialize_rates(self.request,self.object)

        rate_objs = Rate.objects.filter(user_id=self.object.user.pk)

        non_zero_objects = rate_objs.exclude(rate=0)

        context["profile_rates"] = rate_objs

        context["profile_min_rate"] = -1
        context["profile_max_rate"] = -1

        min_rate_ = non_zero_objects.aggregate(Min('rate'))['rate__min']
        max_rate_ = non_zero_objects.aggregate(Max('rate'))['rate__max']

        if min_rate_ is not None:
            context["profile_min_rate"] = min_rate_

        if max_rate_ is not None:
            context["profile_max_rate"] = max_rate_

        context['content_editable'] = True if self.object.user.id == self.request.user.id else False

        context["buddies"] = get_buddies_online(self.request)

        if self.object.type == Role.objects.get(name=UserTypes.student.value):
            context["languages"] = self.object.languages
            context['educations'] = Education.objects.filter(user=self.object.user)
            context["education_form"] = StudentEducationForm({'hidden_pk':'-1','hidden_school':'-1',"school_type":"college/university",'hidden_major':'-1'})
            view_mode = self.request.GET.get("view-mode")
            if view_mode == "profile":
                context["section"] = "profile"
            elif view_mode == "jobs":
                context["section"] = "jobs"
                context["job_post_list"] = Job.objects.filter(posted_by_id=self.object.user.pk)
            elif view_mode == "q_post":
                context["section"] = "q_post"
            elif view_mode == "qs":
                context["section"] = "qs"
                context["q_post_list"] = Question.objects.filter(created_by_id=self.object.user.pk)
            else:
                context["job_post_form"] = JobPostForm()

        else:
            # context["profile_summary"] = self.get_profile_summary()
            context["job_invitation_count"] = JobInvitation.objects.filter(user_id=self.object.user.pk,status=JobInvitationStatus.active.value).count()
            context["job_recommendation_count"] = 0
            view_mode = self.request.GET.get("view-mode")
            if view_mode == "profile":
                context["section"] = "profile"
            else:
                context["section"] = "feed"
                questions = Question.objects.filter(status=QuestionStatus.open.value)
                questions = questions.filter(tags__name__in=[user_major.major.name for user_major in UserMajor.objects.filter(user_id=self.object.user.pk)])
                job_data = []

                if self.request.user.is_authenticated():
                    for question in questions:
                        template = loader.get_template("ajax/job_feed_item.html")
                        cntxt = Context({ 'question': question ,"request": self.request})
                        rendered = template.render(cntxt)
                        job_data += [rendered]

                context["job_data"] = job_data

        view_as = request.GET.get("view_as", "private")
        if view_as == "public":
            context["content_editable"] = False

        return context

    def get(self,request,*args,**kwargs):
        if request.is_ajax():
            section = request.GET.get("section")
            page = request.GET.get("page")
            template_name = None
            paginate_by = 8
            context = {}
            object_list = []
            if not page:
                page = 1
            else:
                try:
                    page = int(page)
                except:
                    page = 1

            rendered_data = ""
            extra_data = {}

            if section == "about":
                template_name = "sections/about_content.html"
                user_id = request.GET.get("user_id")
                try:
                    user_id = int(user_id)
                except:
                    user_id = request.user.pk
                self.object = ChampUser.objects.get(user_id=user_id)
                context = self.get_profile_context(request,user_id)
                context["languages"] = Language.objects.all().exclude(
                    pk__in=[lp.language.pk for lp in self.object.languages.all()])

            elif section == "job_post_form":
                template = loader.get_template("forms/job_post_form.html")
                cntxt = Context({})
                rendered = template.render(cntxt)
                response = {
                    "status": "SUCCESS",
                    "message": "Successful",
                    "data": {
                        "content": rendered
                    }
                }
                return self.render_to_json(response)

            elif section == "my-jobs":
                start_date = request.GET.get("start")
                end_date = request.GET.get("end")
                filter_ = request.GET.get("filter")
                since_ts = request.GET.get("since")
                my_jobs = Job.objects.filter(posted_by_id=request.user.pk).exclude(status=4) #Exclude deleted jobs.

                try:
                    since_ts = int(since_ts)
                except Exception as exp:
                    since_ts = Clock.utc_timestamp()

                my_jobs = my_jobs.filter(date_created__lt=since_ts)

                try:
                    datetime.strptime(start_date, "%Y-%m-%d")
                except Exception as exp:
                    start_date = None

                try:
                    datetime.strptime(end_date, "%Y-%m-%d")
                except Exception as exp:
                    end_date = None

                if start_date:
                    start_date = Clock.local_to_utc_datetime(start_date, "%Y-%m-%d", request.user.champuser.timezone)
                    start_ts = time.mktime(start_date.timetuple())
                    my_jobs = my_jobs.filter(date_created__gte=start_ts)

                if end_date:
                    end_date = Clock.local_to_utc_datetime(end_date, "%Y-%m-%d", request.user.champuser.timezone)
                    end_ts = time.mktime(end_date.timetuple())
                    my_jobs = my_jobs.filter(date_created__lte=end_ts)

                if filter_ == "active":
                    my_jobs = my_jobs.filter(status=1, end_date__gt=Clock.utc_timestamp()).exclude(applications__status=8)
                elif filter_ == "archived":
                    my_jobs = my_jobs.filter(~Q(status=1) | Q(end_date__lte=Clock.utc_timestamp()))
                elif filter_ == "hired":
                    my_jobs = my_jobs.filter(
                        ~Q(status=1) | Q(applications__status=8))

                my_jobs = my_jobs.order_by("-date_created")

                paginator = Paginator(my_jobs,paginate_by)
                try:
                    page = paginator.page(page)
                    object_list = page.object_list
                except Exception as msg:
                    object_list = [] #page.object_list

                template_name = "sections/student_profile_my_jobs.html"
                my_jobs_data = []
                for my_job in object_list:

                    if my_job.status == 1 and my_job.end_date > Clock.utc_timestamp():
                        active_job = True
                    else:
                        active_job = False

                    template = loader.get_template("ajax/student_profile_my_jobs_item.html")
                    data_context = {}
                    data_context['job'] = my_job
                    data_context['active_job'] = active_job
                    data_context["request"] = self.request
                    data_context["pending_count"] = my_job.applications.filter(status=2).count()
                    data_context["invited_count"] = JobInvitation.objects.filter(job_id=my_job.pk,status=1).count()
                    data_context["rejected_count"] = JobInvitation.objects.filter(Q(job_id=my_job.pk) & (Q(status=5) | Q(status=10) | Q(status=12))).count()
                    data_context["shortlisted_count"] = JobInvitation.objects.filter(job_id=my_job.pk, status=9).count()
                    data_context["hired_count"] = JobInvitation.objects.filter(job_id=my_job.pk,status=8).count()

                    if my_job.preference.availability:
                        data_context["availability"] = my_job.preference.availability

                    if my_job.preference.subjects.exists():
                        data_context["subjects"] = ','.join([ s.name for s in my_job.preference.subjects.all() ])

                    if my_job.preference.gender != "male" and my_job.preference.gender != "female":
                        data_context["gender"] = "No Pref"
                    else:
                        data_context["gender"] = my_job.preference.gender.capitalize()

                    if my_job.preference.timezone_diffs.exists():
                        timezones = [ str(s.min_value) + " - " + str(s.max_value) + " hours" for s in my_job.preference.timezone_diffs.all() ]
                        data_context["timezones"] = ', '.join(timezones)

                    if my_job.preference.languages.exists():
                        data_context["languages"] = ','.join([ l.name for l in my_job.preference.languages.all() ])

                    if my_job.preference.currency:
                        currency_objects = Currency.objects.filter(code=my_job.preference.currency)
                        data_context["currency"] = currency_objects.first().name if currency_objects.exists() else None

                    if my_job.preference.rate:
                        data_context["rate1"] = my_job.preference.rate.rate_value
                        data_context["rate2"] = my_job.preference.rate.rate_value2

                    cntxt = Context(data_context)
                    rendered = template.render(cntxt)
                    my_jobs_data += [rendered]

                context["my_jobs"] = my_jobs_data

            elif section == "my_top_tutors":
                template_name = "sections/student_profile_top_tutor_list.html"
                my_top_tutors = []
                lesson_requests = LessonRequest.objects.filter(user_id=request.user.pk).values("request_user_id").annotate(count=Count('id')).order_by('-count').select_related('request_user')[:10]
                for lesson_request in lesson_requests:
                    template = loader.get_template("ajax/student_profile_my_top_tutor_item.html")
                    data_context = {
                        "tutor": ChampUser.objects.get(user_id=lesson_request['request_user_id']),
                        "lesson_count": lesson_request['count']
                    }
                    cntxt = Context(data_context)
                    rendered = template.render(cntxt)
                    my_top_tutors += [rendered]
                context["my_top_tutors"] = my_top_tutors

            elif section == "progress_reports":
                template_name = "sections/progress_report_list.html"
                context['show_progress_report_button'] = request.user.champuser.is_tutor
                
                if request.user.champuser.is_tutor:
                    progress_reports = ProgressReport.objects.filter(sender_id=request.user.pk)
                else:
                    progress_reports = ProgressReport.objects.filter(receiver_id=request.user.pk)

                progress_reports = progress_reports.order_by('-id')

                paginator = Paginator(progress_reports,paginate_by)
                try:
                    page_object = paginator.page(page)
                    object_list = page_object.object_list
                except Exception as msg:
                    page_object = None
                    object_list = [] #page.object_list

                progress_report_items = []

                for progress_report in object_list:
                    template = loader.get_template("ajax/progress_report_list_item.html")
                    data_context = {
                        "request": request,
                        "progress_report": progress_report,
                        "tutor": request.user.champuser.is_tutor
                    }
                    cntxt = Context(data_context)
                    rendered = template.render(cntxt)

                    progress_report_items += [ rendered ]

                context["progress_report_items"] = progress_report_items
                context["is_paginated"] = paginator.num_pages > 1 and page_object
                context["page_obj"] = page_object

                context["meta_data"] = { "section": "progress_reports" }
                context["tab"] = "progress-reports"
                context["container_id"] = "id_progress_report_content"
                

            elif section == "upcoming":
                template_name = "ajax/profile/student_lessons.html"
                my_lessons = {}

                upcoming_lesson_list = []
                past_lesson_list = []
                
                upcoming_lessons = LessonUtil.get_coming_lessons(request.user)
                past_lessons = LessonUtil.get_past_lessons(request.user)
                
                for lesson in upcoming_lessons:
                    template = loader.get_template("ajax/profile/student_profile_lesson_entry.html")
                    data_context = {
                        "lesson": lesson,
                        "status": "upcoming"
                    }
                    cntxt = Context(data_context)
                    rendered = template.render(cntxt)
                    upcoming_lesson_list += [rendered]

                for lesson in past_lessons:
                    template = loader.get_template("ajax/profile/student_profile_lesson_entry.html")
                    data_context = {
                        "lesson": lesson,
                        "status": "past"
                    }
                    cntxt = Context(data_context)
                    rendered = template.render(cntxt)
                    past_lesson_list += [rendered]

                context["my_lessons"] = {
                    "upcoming_lessons": upcoming_lesson_list,
                    "past_lessons": past_lesson_list
                }

            elif section == "top_students":
                template_name = "sections/my_top_student_list.html"
                my_top_students = []
                lesson_requests = LessonRequest.objects.filter(request_user_id=request.user.pk).select_related('user__champuser').prefetch_related('other_users')  #.values("request_user").annotate(count=Count('id')).order_by('-count').select_related('request_user')[:10]
                user_participation_count = {}
                for lr in lesson_requests:
                    if not lr.user.pk in user_participation_count.keys():
                        user_participation_count[lr.user.pk] = {
                            'count': 1,
                            'instance': lr.user.champuser
                        }
                    else:
                        user_participation_count[lr.user.pk]['count'] += 1


                    for o in lr.other_users.all():
                        if not o.pk in user_participation_count.keys():
                            user_participation_count[o.pk] = {
                                'count': 1,
                                'instance': o.champuser
                            }
                        else:
                            user_participation_count[o.pk]['count'] += 1

                top_tutor_count = 0
                for id, p_object in sorted(user_participation_count.items(), key=lambda x: x[1], reverse=True):
                    template = loader.get_template("ajax/top_student_item.html")
                    data_context = {
                        "student": p_object['instance'],
                        "lesson_count": p_object['count']
                    }
                    cntxt = Context(data_context)
                    rendered = template.render(cntxt)
                    my_top_students += [rendered]
                    top_tutor_count += 1
                    if top_tutor_count > 10:
                        break

                context["top_students_items"] = my_top_students

            elif section == "job_feed":
                template_name = "sections/job_feed_list.html"
                question_feed_list = Question.objects.all().order_by("-date_created")
                major_list = [user_major.major.name for user_major in UserMajor.objects.filter(user_id=request.user.pk)]
                question_feed_list = question_feed_list.filter(tags__name__in=major_list).distinct()

                q_all = question_feed_list

                q_all_unread = q_all.filter(read=0)
                for q_ in q_all_unread:
                    q_.read = 1
                    q_.save()

                action_tab = request.GET.get("action_tab")
                answered_qfeed = question_feed_list.filter(answers__answer_by__id=request.user.pk)
                if action_tab == "pending":
                    question_feed_list = question_feed_list.exclude(pk__in=answered_qfeed.values_list('pk',flat=True)).order_by("-date_created")
                elif action_tab == "answered":
                    question_feed_list = answered_qfeed.order_by("-date_created")

                paginator = Paginator(question_feed_list,paginate_by)
                try:
                    page = paginator.page(page)
                    object_list = page.object_list
                except Exception as msg:
                    page = paginator.page(1)
                    object_list = [] #page.object_list

                feed_data = []
                for question in object_list:
                    template = loader.get_template("ajax/job_feed_item.html")
                    cntxt = Context({ 'question': question ,"request": self.request})
                    rendered = template.render(cntxt)
                    feed_data += [rendered]

                context["feed_data"] = feed_data
                context["nf_last_dt"] = q_all.first().date_created if q_all.exists() else 0
                context["action_tab"] = action_tab if action_tab else "pending"
                all_q = Question.objects.all().order_by("-date_created").filter(tags__name__in=[user_major.major.name for user_major in UserMajor.objects.filter(user_id=request.user.pk)]).distinct()
                context["all_count"] = all_q.count()
                context["pending_count"] = all_q.exclude(pk__in=answered_qfeed.values_list('pk',flat=True)).count()
                context["answered_count"] = answered_qfeed.count()
                extra_data["total_pages"] = paginator.num_pages


            elif section == "progress_report":
                template_name = "sections/progress_report_list.html"
                progress_report_list = ProgressReport.objects.filter(sender_id=request.user.pk).order_by("-date_created")
                paginator_object = Paginator(progress_report_list,paginate_by)
                context["object_list"] = paginator_object.page(page)
                extra_data["total_pages"] = paginator_object.num_pages

            elif section == "job_invitation":
                template_name = "sections/job_invitation_list.html"
                job_invitation_list = JobInvitation.objects.filter(user_id=request.user.pk).exclude(job__status=4)

                ji_all = job_invitation_list

                ji_all_unread = ji_all.filter(read=0)
                for ji_ in ji_all_unread:
                    ji_.read = 1
                    ji_.save()

                action_tab = request.GET.get("action_tab")
                if not action_tab:
                    action_tab = "pending"
                if action_tab == "pending":
                    job_invitation_list = job_invitation_list.filter(status=JobInvitationStatus.active.value)
                elif action_tab == "accepted":
                    job_invitation_list = job_invitation_list.filter(Q(status=JobInvitationStatus.applied.value) | Q(status=JobInvitationStatus.shortlisted.value) | Q(status=JobInvitationStatus.offered.value) | Q(status=JobInvitationStatus.hired.value))
                elif action_tab == "rejected":
                    job_invitation_list = job_invitation_list.filter(Q(status=JobInvitationStatus.rejected.value) | Q(status=JobInvitationStatus.rejected_application.value))
                elif action_tab == "applied":
                    job_invitation_list = job_invitation_list.filter(Q(status=2) | Q(status=9))
                elif action_tab == "shortlisted":
                    job_invitation_list = job_invitation_list.filter(user=request.user,status=JobInvitationStatus.shortlisted.value)
                elif action_tab == "offered":
                    job_invitation_list = job_invitation_list.filter(user=request.user,status=JobInvitationStatus.offered.value)
                elif action_tab == "hired":
                    job_invitation_list = job_invitation_list.filter(user=request.user,status=JobInvitationStatus.hired.value)
                job_invitation_list = job_invitation_list.order_by("-date_created")
                paginator_object = Paginator(job_invitation_list,paginate_by)
                job_invitations_data = []
                for job_invitation in paginator_object.page(page):
                    template = loader.get_template("ajax/job_invitation_item.html")
                    cntxt = Context({ 'job_invitation': job_invitation ,"request": request})
                    rendered = template.render(cntxt)
                    job_invitations_data += [rendered]
                context["job_invitation_data"] = job_invitations_data
                context["ji_last_dt"] = ji_all.first().date_created if ji_all.exists() else 0
                context["action_tab"] = action_tab if action_tab else "pending"
                context["all_count"] = JobInvitation.objects.filter(user_id=request.user.pk).exclude(job__status=4).count()
                context["pending_count"] = JobInvitation.objects.filter(user=request.user,status=JobInvitationStatus.active.value).exclude(job__status=4).count()
                context["rejected_count"] = JobInvitation.objects.filter(Q(user=request.user) & (Q(status=JobInvitationStatus.rejected.value) | Q(status=JobInvitationStatus.rejected_application.value) )).exclude(job__status=4).count()
                context["applied_count"] = JobInvitation.objects.filter(Q(user=request.user) & (Q(status=2) | Q(status=9))).count()
                context["accepted_count"] = JobInvitation.objects.filter(Q(user=request.user) & (Q(status=JobInvitationStatus.applied.value) | Q(status=JobInvitationStatus.shortlisted.value) | Q(status=JobInvitationStatus.offered.value) | Q(status=JobInvitationStatus.hired.value))).exclude(job__status=4).count()
                context["shortlisted_count"] = JobInvitation.objects.filter(user=request.user,status=JobInvitationStatus.shortlisted.value).exclude(job__status=4).count()
                context["offered_count"] = JobInvitation.objects.filter(user=request.user,status=JobInvitationStatus.offered.value).exclude(job__status=4).count()
                context["hired_count"] = JobInvitation.objects.filter(user=request.user,status=JobInvitationStatus.hired.value).exclude(job__status=4).count()
                extra_data["total_pages"] = paginator_object.num_pages

            elif section == "job_application":
                template_name = "sections/job_application_list.html"

                job_invitation_ids = JobInvitation.objects.filter(user_id=request.user.pk).exclude(job__status=4).values_list('job_id', flat=True)

                job_application_list = JobApplication.objects.filter(tutor_id=request.user.pk).exclude(job__status=4).exclude(job__id__in=job_invitation_ids).order_by("-date_created")

                action_tab = request.GET.get("action_tab")
                if not action_tab:
                    action_tab = "pending"
                if action_tab == "pending":
                    job_application_list = job_application_list.filter(Q(status=2) | Q(status=9))
                elif action_tab == "accepted":
                    job_application_list = job_application_list.filter(Q(status=JobApplicationStatus.offered.value) | Q(status=JobApplicationStatus.shortlisted.value) | Q(status=JobApplicationStatus.negotiating.value))
                elif action_tab == "rejected":
                    job_application_list = job_application_list.filter(status=JobApplicationStatus.rejected_application.value)
                elif action_tab == "shortlisted":
                    job_application_list = job_application_list.filter(status=JobApplicationStatus.shortlisted.value)
                elif action_tab == "offered":
                    job_application_list = job_application_list.filter(status=JobApplicationStatus.offered.value)
                elif action_tab == "hired":
                    job_application_list = job_application_list.filter(status=JobApplicationStatus.hired.value)

                job_application_list = job_application_list.order_by('-date_created')

                paginator_object = Paginator(job_application_list,paginate_by)
                try:
                    page = paginator_object.page(page)
                    object_list = page.object_list
                except Exception as msg:
                    page = paginator_object.page(1)
                    object_list = [] #page.object_list

                job_application_list = []
                for job_application in object_list:
                    template = loader.get_template("ajax/job_application_item.html")
                    application_details_link = None
                    if job_application.job_set.all().exists():
                        application_details_link = reverse("job_application_details_view",kwargs={ "job_id":job_application.job_set.all().first().pk,"pk":job_application.pk })
                    cntxt = Context({ 'job_application': job_application ,"request": request, "application_details_link": application_details_link})
                    rendered = template.render(cntxt)
                    job_application_list += [rendered]
                context["job_application_list"] = job_application_list
                context["action_tab"] = action_tab if action_tab else "pending"
                context["all_count"] = JobApplication.objects.filter(tutor_id=request.user.pk).exclude(job__id__in=job_invitation_ids).count()
                context["pending_count"] = JobApplication.objects.filter(Q(tutor_id=request.user.pk) & (Q(status=2) | Q(status=9))).exclude(job__id__in=job_invitation_ids).count()
                context["accepted_count"] = JobApplication.objects.filter(Q(tutor_id=request.user.pk) & (Q(status=JobApplicationStatus.offered.value) | Q(status=JobApplicationStatus.shortlisted.value) | Q(status=JobApplicationStatus.negotiating.value))).exclude(job__id__in=job_invitation_ids).count()
                context["rejected_count"] = JobApplication.objects.filter(Q(tutor_id=request.user.pk) & Q(status=JobApplicationStatus.rejected_application.value)).exclude(job__id__in=job_invitation_ids).count()
                context["shortlisted_count"] = JobApplication.objects.filter(tutor_id=request.user.pk,status=JobApplicationStatus.shortlisted.value).exclude(job__id__in=job_invitation_ids).count()
                context["offered_count"] = JobApplication.objects.filter(tutor_id=request.user.pk,status=JobApplicationStatus.offered.value).exclude(job__id__in=job_invitation_ids).count()
                context["hired_count"] = JobApplication.objects.filter(tutor_id=request.user.pk,status=JobApplicationStatus.hired.value).exclude(job__id__in=job_invitation_ids).count()
                extra_data["total_pages"] = paginator_object.num_pages

            elif section == "job_recommendation":
                template_name = "sections/job_recommendation_list.html"
                all_recommended_jobs = JobUtil.recommended_jobs_for_user(request.user.pk)
                action_tab = request.GET.get("action_tab")
                status = 1
                if not action_tab:
                    action_tab = "pending"
                if action_tab == "pending":
                    recommended_jobs = all_recommended_jobs.filter(~Q(applications__tutor__pk=request.user.pk))
                elif action_tab == "rejected":
                    recommended_jobs = all_recommended_jobs.filter(Q(applications__tutor__pk=request.user.pk) & Q(applications__status=10))
                    status = 10
                elif action_tab == "applied":
                    recommended_jobs = all_recommended_jobs.filter(Q(applications__tutor__pk=request.user.pk) & (Q(applications__status=2) | Q(applications__status=9)))
                    status = 9
                elif action_tab == "hired":
                    recommended_jobs = all_recommended_jobs.filter(Q(applications__tutor__pk=request.user.pk) & Q(applications__status=8))
                    status = 8

                recommended_jobs = recommended_jobs.order_by('-date_created')

                last_job_date = recommended_jobs.order_by('-date_created').first().date_created if recommended_jobs.exists() else 0

                paginator_object = Paginator(recommended_jobs,paginate_by)
                job_recommendations_data = []
                paging_objects = paginator_object.page(page)
                for job in paging_objects:
                    template = loader.get_template("ajax/job_recommendation_item.html")
                    cntxt = Context({ 'status': status, 'job': job ,"request": request})
                    rendered = template.render(cntxt)
                    job_recommendations_data += [rendered]
                context["job_recommendation_data"] = job_recommendations_data
                context["jr_last_dt"] = last_job_date
                context["action_tab"] = action_tab if action_tab else "pending"
                context["all_count"] = all_recommended_jobs.count()
                context["pending_count"] = all_recommended_jobs.filter(~Q(applications__tutor__pk=request.user.pk)).count()
                context["rejected_count"] = all_recommended_jobs.filter(Q(applications__tutor__pk=request.user.pk) & Q(applications__status=10)).count()
                context["applied_count"] = all_recommended_jobs.filter(Q(applications__tutor__pk=request.user.pk) & (Q(applications__status=2) | Q(applications__status=9))).count()
                context["hired_count"] = all_recommended_jobs.filter(Q(applications__tutor__pk=request.user.pk) & Q(applications__status=8)).count()
                extra_data["total_pages"] = paginator_object.num_pages

            elif section == "my_participation":
                template_name = "sections/my_participation_list.html"
                question_feed_list = Question.objects.all().order_by("-date_created")
                # major_list = [user_major.major.name for user_major in UserMajor.objects.filter(user_id=request.user.pk)]
                # question_feed_list = question_feed_list.filter(tags__name__in=major_list).distinct()
                major_list = [user_major.major.id for user_major in UserMajor.objects.filter(user_id=request.user.pk)]
                question_feed_list = question_feed_list.filter(tags__id__in=major_list).distinct()\
                    .exclude(Q(answers__answer_by_id=request.user.pk) | Q(answers__comments__created_by_id=request.user.pk))
                # paginator = Paginator(question_feed_list,paginate_by)
                # try:
                #     page_object = paginator.page(page)
                #     object_list = page_object.object_list
                # except Exception as msg:
                #     page_object = paginator.page(1)
                #     object_list = [] #page.object_list

                object_list = question_feed_list

                feed_data = []
                for question in object_list:
                    template = loader.get_template("ajax/job_feed_item.html")
                    cntxt = Context({ 'question': question ,"request": request})
                    rendered = template.render(cntxt)
                    feed_data += [rendered]
                context['feed_data'] = feed_data

                participation_list = Question.objects.filter(Q(answers__answer_by_id=request.user.pk)
                                                             | Q(answers__comments__created_by_id=request.user.pk)).order_by("-date_created").distinct()
                # paginator_object = Paginator(participation_list,paginate_by)
                # try:
                #     page_object = paginator_object.page(page)
                #     object_list = page_object.object_list
                # except Exception as msg:
                #     page_object = paginator_object.page(1)
                #     object_list = [] #page.object_list
                object_list = participation_list
                participations = []
                for question in object_list:
                    template = loader.get_template("ajax/job_feed_item.html")
                    cntxt = Context({ 'question': question ,"request": request})
                    rendered = template.render(cntxt)
                    participations += [rendered]
                context['participations'] = participations
                context['champuser'] = request.user.champuser

                # extra_data["total_pages"] = paginator_object.num_pages

            elif section == "my_recorded_session":
                template_name = "sections/my_recorded_session_list.html"
                lessons_list = LessonUtil.get_past_lessons(user=request.user)
                paginator_object = Paginator(lessons_list,paginate_by)
                my_recorded_session_data = []
                for lesson in paginator_object.page(page):
                    template = loader.get_template("ajax/my_recorded_session_item.html")
                    cntxt = Context({ 'lesson': lesson ,"request": request})
                    rendered = template.render(cntxt)
                    my_recorded_session_data += [rendered]
                context["my_recorded_session_data"] = my_recorded_session_data
                extra_data["total_pages"] = paginator_object.num_pages

            elif section == "my_reviews":
                template_name = "sections/lesson_review_list.html"
                my_lessons = LessonRequest.objects.filter(request_user_id=request.user.pk,review__isnull=False)
                paginator_object = Paginator(my_lessons,paginate_by)
                my_review_data = []
                for lesson in paginator_object.page(page):
                    template = loader.get_template("ajax/my_review_data_item.html")
                    cntxt = Context({ 'lesson': lesson ,"request": request})
                    rendered = template.render(cntxt)
                    my_review_data += [rendered]
                context["champuser"] = request.user.champuser
                context["my_reviews"] = my_review_data
                extra_data["total_pages"] = paginator_object.num_pages
            elif section == "students_active":
                template_name = "sections/student_list.html"
                active_lessons = LessonUtil.get_coming_lessons(request.user)
                paginator_object = Paginator(active_lessons,paginate_by)
                active_lessons_list = []
                for lesson in paginator_object.page(page):
                    template = loader.get_template("ajax/student_active_entry.html")
                    cntxt = Context({ 'lesson': lesson, 'champuser': lesson.user.champuser ,"request": request, "STATIC_URL": settings.STATIC_URL })
                    rendered = template.render(cntxt)
                    active_lessons_list += [rendered]
                context["entry_list"] = active_lessons_list
                context['header'] = 'Upcoming Lessons'
                extra_data["total_pages"] = paginator_object.num_pages

            elif section == "students_inactive":
                template_name = "sections/student_list.html"
                inactive_lessons = LessonUtil.get_past_lessons(request.user)
                paginator_object = Paginator(inactive_lessons,paginate_by)
                active_lessons_list = []
                for lesson in paginator_object.page(page):
                    template = loader.get_template("ajax/student_active_entry.html")
                    cntxt = Context({ 'lesson': lesson, 'champuser': lesson.user.champuser ,"request": request, "STATIC_URL": settings.STATIC_URL })
                    rendered = template.render(cntxt)
                    active_lessons_list += [rendered]
                context["entry_list"] = active_lessons_list
                context['header'] = 'Past Lessons'
                extra_data["total_pages"] = paginator_object.num_pages

            if template_name:
                response = {
                    "status": "SUCCESS",
                    "message": "Successful",
                    "data": ""
                }
                template = loader.get_template(template_name)

                cntxt = Context(context)
                rendered = template.render(cntxt)
                response["data"] = {
                    "content": rendered,
                    "extra": extra_data
                }
                return self.render_to_json(response)
        response = {
            "status": "FAILURE",
            "message": "Failed to process the request.",
            "data": []
        }
        return self.render_to_json(response)

class FetchProfileSectionAjaxView(JSONResponseMixin, View):
    def get(self,request,*args,**kwargs):
        if request.is_ajax():
            ###First fetch News Feed item.
            data = {

            }
            nf_last_item_dt = request.GET.get("nf_last_dt")
            try:
                nf_last_item_dt = int(nf_last_item_dt)
                # question_feed_list = Question.objects.filter(read=0).order_by("-date_created")
                # major_list = [user_major.major.name for user_major in UserMajor.objects.filter(user_id=request.user.pk)]
                # question_feed_list = question_feed_list.filter(tags__value__in=major_list,date_created__gt=nf_last_item_dt).distinct()
                data['qfeed_new_item_count'] = 0
            except:
                data['qfeed_new_item_count'] = 0

            ji_last_dt = request.GET.get("ji_last_dt")
            try:
                ji_last_dt = int(ji_last_dt)
                job_invitation_list = JobInvitation.objects.filter(user_id=request.user.pk,status=1,date_created__gt=ji_last_dt).exclude(job__status=4)
                data['ji_new_item_count'] = job_invitation_list.count()
            except:
                data['ji_new_item_count'] = 0

            jr_last_dt = request.GET.get("jr_last_dt")
            try:
                jr_last_dt = int(jr_last_dt)
                job_recommendation_list = JobUtil.recommended_jobs_for_user(request.user.pk, ts=jr_last_dt)
                job_recommendation_list = job_recommendation_list.exclude(applications__tutor__id=request.user.pk)
                data['jr_new_item_count'] = job_recommendation_list.count()
            except:
                data['jr_new_item_count'] = 0

            data['ji_total_item_count'] = JobInvitation.objects.filter(user_id=request.user.pk, status=1).exclude(job__status=4).count()

            recommended_jobs = JobUtil.recommended_jobs_for_user(request.user.pk).exclude(applications__tutor__id=request.user.pk)
            data['jr_total_item_count'] = recommended_jobs.count()
            data['jr_last_item_date'] = recommended_jobs.first().date_created if recommended_jobs.exists() else 0

            response = {
                    "status": "SUCCESS",
                    "message": "Successful"
                }
            response["data"] = {
                'new_items': data
            }
            return self.render_to_json(response)

        response = {
            "status": "FAILURE",
            "message": "Failed to process the request.",
            "data": []
        }
        return self.render_to_json(response)


class ProfileLanguageUpdate(JSONResponseMixin,View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ProfileLanguageUpdate, self).dispatch(*args, **kwargs)

    def post(self,request,*args,**kwargs):
        lan_code = request.POST.get("lan-code")
        lan_level = request.POST.get("lan-level")
        if lan_code and lan_level:
            lan_objects = Language.objects.filter(code=lan_code)
            if lan_objects.exists():
                if not request.user.champuser.languages.filter(language_id=lan_objects.first().pk).exists():
                    lan_proficiency = LanguageProficiency()
                    lan_proficiency.language_id = lan_objects.first().pk
                    lan_proficiency.proficiency = int(lan_level)
                    lan_proficiency.save()
                    champ_user = request.user.champuser
                    champ_user.languages.add(lan_proficiency)
                    prof_level = "Below Average"
                    lan_level = int(lan_level)
                    if lan_level == 1:
                        prof_level = "Below Average"
                    elif lan_level == 2:
                        prof_level = "Average"
                    elif lan_level == 3:
                        prof_level = "Fluent"
                    response = {
                        "status": "SUCCESS",
                        "message": "Successful",
                        "data": {
                            "lp_id": lan_proficiency.pk,
                            "lang_id": lan_proficiency.language_id,
                            "lang_code": lan_objects.first().code,
                            "lang_name": lan_objects.first().name,
                            "prof_level": prof_level
                        }
                    }
                    return self.render_to_json(response)
                else:
                    response = {
                        "status": "FAILURE",
                        "message": "Already added.",
                        "data": []
                    }
                    return self.render_to_json(response)
        response = {
            "status": "FAILURE",
            "message": "Failed to process the request.",
            "data": []
        }
        return self.render_to_json(response)

class ProfileLanguageDelete(JSONResponseMixin,View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ProfileLanguageDelete, self).dispatch(*args, **kwargs)

    def post(self,request,*args,**kwargs):
        lang_id = request.POST.get("lp_id")
        try:
            lang_id = int(lang_id)
            ulang_prof = request.user.champuser.languages.all()
            lang_prof = LanguageProficiency.objects.get(pk=lang_id)
            request.user.champuser.languages.remove(lang_prof)
            response = {
                "status": "SUCCESS",
                "message": "Successful",
                "data": ""
            }
            return self.render_to_json(response)
        except Exception as msg:
            response = {
                "status": "FAILURE",
                "message": "Failed to process the request.",
                "data": []
            }
            return self.render_to_json(response)


class UserCheckAjaxView(JSONResponseMixin,View):
    def get(self,request,*args,**kwargs):
        email = request.GET.get("email")
        exists = False
        if email:
            user_objects = User.objects.filter(email=email)
            if user_objects.exists():
                exists = True
        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": exists
        }
        return self.render_to_json(response)

class FetchTutorListView(JSONResponseMixin,View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(FetchTutorListView, self).dispatch(*args, **kwargs)

    def get_chat_users(self,request):
        chat_users = ChampUser.objects.all()
        return chat_users

    def post(self,request,*args,**kwargs):
        data = {}
        major_objects = Major.objects.all().order_by('-rank')
        if request.POST.get("major"):
            major_objects = major_objects.filter(name=request.POST["major"])

        major_objects = major_objects[:5]

        excludes = []
        try:
            excludes = request.POST.get("excludes")
            if excludes:
                excludes = [int(id) for id in excludes.split(",") if id]
            else:
                excludes = []
        except:
            excludes = []

        tutors = []
        try:
            tutors = request.POST.get("tutors")
            tutors = [int(id) for id in tutors.split(",") if id]
        except:
            tutors = []

        #print(tutors)

        champ_users = self.get_chat_users(request)

        if request.POST.get("q"):
            champ_users = champ_users.filter(fullname__icontains=request.POST['q'])

        type_name = "teacher"
        if request.user.is_authenticated():
            if request.user.champuser.type.name == "teacher":
                type_name = "student"
            else:
                type_name = "teacher"

        champ_users = champ_users.filter(type__name=type_name)

        major_ranks = {}

        for major in major_objects:
            major_ranks[major.name] = major.rank
            if not major.name in data.keys():
                total_length = 0
                chat_users = champ_users.filter(major=major.name)
                chat_users = chat_users.exclude(available_to_teach=AvailableStatus.profile_deactivated.value)
                if tutors:
                    chat_users = chat_users.filter(user_id__in=tutors)
                chat_users = chat_users.exclude(user_id__in=excludes)
                if not tutors:
                    total_length = chat_users.count()
                    chat_users = chat_users[:5]
                users = []
                for chat_user in chat_users:
                    user = {}
                    user["id"] = chat_user.user.pk
                    user["fullname"] = chat_user.fullname[:20]+".." if len(chat_user.fullname) > 20 else chat_user.fullname
                    user['name'] = chat_user.fullname
                    user["img"] = chat_user.render_profile_picture
                    user["rating"] = chat_user.calculate_rating()
                    user["online_status"] = "online" if chat_user.online_status == 1 else "offline"
                    user["teaching_status"] = chat_user.render_teaching_available_status_verbal
                    user["current_school"] = chat_user.current_school[:20]+".." if len(chat_user.current_school) > 20 else chat_user.current_school
                    positive_feedback_count = chat_user.positive_feedback_count
                    user["positive_rating_count"] = str(positive_feedback_count) if positive_feedback_count else "-"
                    users += [user]
                if users:
                    data[major.name] = {
                        "users": users,
                        "show_more": 1 if (total_length > 5) else 0
                    }

        import operator
        temp_items = []
        sorted_majors = reversed(sorted(major_ranks.items(), key=operator.itemgetter(1)))
        for item in sorted_majors:
            if data.get(item[0]):
                temp_items += [ item[0] ]

        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": data,
            "ordered_items": temp_items
        }
        return self.render_to_json(response)

class ProfilePictureAjaxView(JSONResponseMixin, TemplateView):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ProfilePictureAjaxView, self).dispatch(*args, **kwargs)

    template_name = "ajax/profile_picture_dialog.html"

    def render_to_response(self, context, **response_kwargs):
        template = loader.get_template(self.template_name)
        context = Context({ "champuser": self.request.user.champuser })
        rendered = template.render(context)
        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": {
                "dialog_content": rendered
            }
        }
        return self.render_to_json(response)

    def post(self,request,*args,**kwargs):
        image_data = request.POST.get('image-data')
        image_data_split = image_data.split(',')
        image_data = image_data_split[1]
        image_format = image_data_split[0].replace('data:','').replace(';base64','').replace('image/','')
        imgdata = base64.b64decode(image_data)
        file_name = str(uuid.uuid4())+'.'+image_format
        file_path = os.path.join(settings.IMAGE_DIR, file_name)
        with open(file_path, 'wb+') as f:
            f.write(imgdata)

        if request.user.champuser.profile_picture:
            pp = request.user.champuser.profile_picture
        else:
            pp = ProfilePicture()

        file = File(open(file_path))

        #print(file)

        pp.image_field = settings.MEDIA_URL+"image/"+file_name
        pp.cropping = '0,17,200,183'
        pp.save()

        #print(pp.image_field)

        request.user.champuser.profile_picture = pp
        request.user.champuser.save()

        response = {}
        response["status"] = "SUCCESS"
        response["message"] = "Successful"
        response["data"] = file_path
        return self.render_to_json(response)

class FetchBuddyListView(JSONResponseMixin,View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(FetchBuddyListView, self).dispatch(*args, **kwargs)

    def get_blocked_users(self,request):
        blocked_users = []
        user_blocklist_settings = UserBlockSettings.objects.filter(user_id = request.user.pk)
        if user_blocklist_settings.exists():
            user_blocklist_settings = user_blocklist_settings.first()
            blocked_users = [ user.pk for user in user_blocklist_settings.blocked_users.all() ]
        return blocked_users

    def post(self,request,*args,**kwargs):

        block_size = 10

        data = {}
        format = request.POST.get("format")
        kind = request.POST.get("kind")
        excludes = []
        try:
            excludes = request.POST.get("excludes")
            if excludes:
                excludes = [int(id) for id in excludes.split(",") if id]
            else:
                excludes = []
        except:
            excludes = []

        tutors = []
        try:
            tutors = request.POST.get("tutors")
            tutors = [int(id) for id in tutors.split(",") if id]
        except:
            tutors = []

        if kind == "check_status" and format == "json":
            buddies = []
            champ_users = ChampUser.objects.filter(user_id__in=tutors)
            champ_users = champ_users.exclude(available_to_teach=AvailableStatus.profile_deactivated.value)
            for chat_user in champ_users:
                tutor_chat_status_class = "tutors_chat_img_offline"
                online_status = "offline"
                if chat_user.render_online_status_verbal == "online" and chat_user.render_teaching_available_status_verbal == "ready_to_teach":
                    tutor_chat_status_class = "tutors_chat_img"
                    online_status = "online"
                elif chat_user.render_online_status_verbal == "online" and chat_user.render_teaching_available_status_verbal != "ready_to_teach":
                    tutor_chat_status_class = "tutors_chat_img"
                    online_status = "online"
                elif chat_user.render_online_status_verbal != "online" and chat_user.render_teaching_available_status_verbal == "ready_to_teach":
                    tutor_chat_status_class = "tutors_chat_img_teach_available"
                    online_status = "offline"
                user = {}
                user["id"] = chat_user.user.pk
                user["fullname"] = chat_user.fullname[:17]+".." if len(chat_user.fullname) > 17 else chat_user.fullname
                user['name'] = chat_user.fullname
                user["img"] = chat_user.render_profile_picture
                user["rating"] = chat_user.calculate_rating()
                user["online_status"] = online_status
                user["tutor_chat_status_class"] = tutor_chat_status_class
                current_school = ""
                if chat_user.current_school:
                    if len(chat_user.current_school) > 15:
                        current_school = chat_user.current_school[:15]
                    else:
                        current_school = chat_user.current_school
                user["current_school"] = current_school
                buddies += [ user ]
            response = {
                "status": "SUCCESS",
                "message": "Successful",
                "data": {
                    "buddies": buddies
                }
            }
            return self.render_to_json(response)

        elif kind == "show_more":
            section = request.POST.get("section")
            show_more_contents = {}
            if section == "chatted_users":
                champ_users = UserUtil.get_chat_users(request)
                champ_users = champ_users.exclude(user_id__in=excludes)
                champ_users = champ_users.exclude(user_id__in=self.get_blocked_users(request))
                champ_users = champ_users.exclude(available_to_teach=AvailableStatus.profile_deactivated.value)
                type_name = "teacher"
                if request.user.is_authenticated():
                    if request.user.champuser.type.name == "teacher":
                        type_name = "student"
                    else:
                        type_name = "teacher"

                champ_users = champ_users.filter(type__name=type_name)

                total_count = champ_users.count()
                data_items = []
                show_more = False
                for ch in champ_users:
                    template = loader.get_template("ajax/chat_buddy_list_item.html")
                    cntxt = Context({ 'champ_user': ch ,"request": request})
                    rendered = template.render(cntxt)
                    data_items += [ rendered ]
                if total_count >= block_size:
                    show_more = True

                show_more_contents = {
                    "show_more": show_more,
                    "items": data_items
                }
            else:
                champ_users = ChampUser.objects.filter(user__usermajor__major__name=section)
                champ_users = champ_users.exclude(available_to_teach=AvailableStatus.profile_deactivated.value)
                type_name = "teacher"
                if request.user.is_authenticated():
                    if request.user.champuser.type.name == "teacher":
                        type_name = "student"
                    else:
                        type_name = "teacher"

                champ_users = champ_users.filter(type__name=type_name)
                champ_users = champ_users.exclude(user_id__in=excludes)
                champ_users = champ_users.exclude(user_id__in=self.get_blocked_users(request))
                total_count = champ_users.count()
                data_items = []
                show_more = False
                for ch in champ_users:
                    template = loader.get_template("ajax/chat_buddy_list_item.html")
                    cntxt = Context({ 'champ_user': ch ,"request": request})
                    rendered = template.render(cntxt)
                    data_items += [ rendered ]
                if total_count >= block_size:
                    show_more = True

                show_more_contents = {
                    "show_more": show_more,
                    "items": data_items
                }
            response = {
                "status": "SUCCESS",
                "message": "Successful",
                "data": show_more_contents
            }
            return self.render_to_json(response)

        type_name = "teacher"
        if request.user.is_authenticated():
            if request.user.champuser.type.name == "teacher":
                type_name = "student"
            else:
                type_name = "teacher"

        champ_users = UserUtil.get_chat_users(request)

        if request.POST.get("q"):
            champ_users = champ_users.filter(fullname__icontains=request.POST['q'])

        champ_users = champ_users.filter(type__name=type_name)

        if tutors:
            champ_users = champ_users.filter(user_id__in=tutors)
        chat_users = champ_users.exclude(user_id__in=excludes)

        chat_users = chat_users.exclude(user_id__in=self.get_blocked_users(request))
        chat_users = chat_users.exclude(available_to_teach=AvailableStatus.profile_deactivated.value)
        total_length = chat_users.count()
        if not tutors:
            chat_users = chat_users[:block_size]
        users = []
        chatted_users = []
        chatted_user_ids = []
        chatted_show_more = False
        for chat_user in chat_users:
            if not chat_user.pk in chatted_user_ids:
                chatted_user_ids += [ chat_user.pk ]
            template = loader.get_template("ajax/chat_buddy_list_item.html")
            cntxt = Context({ 'champ_user': chat_user ,"request": request})
            rendered = template.render(cntxt)
            users += [ rendered ]
            chatted_users += [ rendered ]
        if users:
            chatted_show_more = True if (total_length > block_size) else False

        subject_entries = {}

        if type_name == "teacher":
            #Get top 5 search for this user
            top_five_searches = TopSearch.objects.filter(user_id=request.user.pk).order_by('-search_frequency').values_list('subject__name', flat=True)[:5]
            for subject in top_five_searches:
                subjects = []
                subject_users = ChampUser.objects.filter(user__usermajor__major__name=subject)
                subject_users = subject_users.exclude(available_to_teach=AvailableStatus.profile_deactivated.value)
                if request.POST.get("q"):
                    subject_users = subject_users.filter(fullname__icontains=request.POST['q'])
                subject_users = subject_users.exclude(user_id__in=self.get_blocked_users(request))

                total_count = subject_users.count()

                subject_users = subject_users[:block_size]

                for su in subject_users:
                    template = loader.get_template("ajax/chat_buddy_list_item.html")
                    cntxt = Context({ 'champ_user': su ,"request": request})
                    rendered = template.render(cntxt)
                    subjects += [ rendered ]

                show_more = False
                if total_count >= block_size:
                    show_more = True

                subject_entries[subject] = {
                    "show_more": show_more,
                    "subjects": subjects
                }

        template = loader.get_template("ajax/tutor_chat_list.html")
        cntxt = Context({ 'subject_entries': subject_entries ,"chatted_users": chatted_users, "chatted_show_more": chatted_show_more, "type_name": type_name})
        chat_list = template.render(cntxt)

        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": {
                "chat_list": chat_list
            }
        }
        return self.render_to_json(response)

class GuideVideoIntro(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        settings_obj, created = GlobalSettings.objects.get_or_create(pk=1)
        return render(request, 'ajax/two_min_video_intro.html', { 'settings': settings_obj })

class JobTutorListAjaxView(LoginRequiredMixin,JSONResponseMixin, View):
    def get(self, request, *args, **kwargs):
        status_type = request.GET.get("status", "pending")
        search_enable = request.GET.get("search")
        keyword = request.GET.get("q")
        job_id = request.GET.get("job")
        view_invited = request.GET.get("view_invited")
        view_recommended = request.GET.get("view_recommended")
        job = None

        try:
            job_id = int(job_id)
            job = Job.objects.get(pk=job_id)
        except:
            pass

        record_count = 0

        rendered_contents = []

        if status_type == "invited":
            job_invitations = JobInvitation.objects.filter(job_id=job_id, status=1).exclude(job__status=4)
            for job_invitation in job_invitations:
                te = TeachingExperiences.objects.filter(user_id=job_invitation.user.pk)
                context_data = {
                    "job": job,
                    "tutor": job_invitation.user,
                    "champuser": job_invitation.user.champuser,
                    "initiated_as_label": "JobInvitation",
                    "exp_details": te.first().experience if te.exists() else "",
                    "status": status_type,
                    "currency": job.preference.currency,
                    "price": job.preference.rate.rate_value,
                    "price2": job.preference.rate.rate_value2,
                    # "status": job_application.status,
                    "application_link": None,
                    "record_count": job_invitations.count()
                }

                template_name = "ajax/job_details_tutor_row.html"
                template = loader.get_template(template_name)

                cntxt = Context(context_data)
                rendered = template.render(cntxt)
                rendered_contents += [ rendered ]
            record_count = job_invitations.count()
        elif status_type == "rejected":
            job_invitations = JobInvitation.objects.filter(job_id=job_id, status=5).exclude(job__status=4)
            if job_invitations.exists():
                for job_invitation in job_invitations:
                    te = TeachingExperiences.objects.filter(user_id=job_invitation.user.pk)
                    context_data = {
                        "job": job,
                        "tutor": job_invitation.user,
                        "champuser": job_invitation.user.champuser,
                        "initiated_as_label": "JobInvitation",
                        "exp_details": te.first().experience if te.exists() else "",
                        "status": status_type,
                        "currency": job.preference.currency,
                        "price": job.preference.rate.rate_value,
                        "price2": job.preference.rate.rate_value2,
                        # "status": job_application.status,
                        "application_link": None,
                        "record_count": job_invitations.count()
                    }

                    template_name = "ajax/job_details_tutor_row.html"
                    template = loader.get_template(template_name)

                    cntxt = Context(context_data)
                    rendered = template.render(cntxt)
                    rendered_contents += [ rendered ]
                record_count = job_invitations.count()
            job_applications = job.applications.filter(Q(status=5) | Q(status=10) | Q(status=12)).exclude(pk__in=job.shortlist.values_list('application_id', flat=True)).exclude(job__status=4) ###Rejected
            if job_applications.exists():
                for job_application in job_applications:
                    te = TeachingExperiences.objects.filter(user_id=job_application.tutor.pk)
                    context_data = {
                        "job": job,
                        "tutor": job_application.tutor,
                        "champuser": job_application.tutor.champuser,
                        "tutor": job_application.tutor,
                        "application": job_application,
                        "currency": job_application.settlement.currency,
                        "price": job_application.settlement.price,
                        "price2": job_application.settlement.price2,
                        "exp_details": te.first().experience if te.exists() else "",
                        "status": status_type,
                        # "status": job_application.status,
                        "application_link": reverse("job_application_details_view", kwargs={ "job_id": job_id, "pk": job_application.pk }),
                    }

                    template_name = "ajax/job_details_tutor_row.html"
                    template = loader.get_template(template_name)

                    cntxt = Context(context_data)
                    rendered = template.render(cntxt)
                    rendered_contents += [ rendered ]
                record_count += job_applications.count()
        else:
            if job:
                job_applications = job.applications.all().exclude(job__status=4)
            else:
                job_applications = JobApplication.objects.none()

            if status_type == "pending":
                job_applications = job_applications.filter(status=2) ###Active
            elif status_type == "shortlisted":
                job_applications = job_applications.filter(status=9)
            elif status_type == "hired":
                job_applications = job_applications.filter(status=8)

            for job_application in job_applications:
                initiated_as = None
                if JobInvitation.objects.filter(job_id=job.pk, user_id=job_application.tutor.pk).exists():
                    initiated_as = 'JobInvitation'
                te = TeachingExperiences.objects.filter(user_id=job_application.tutor.pk)
                context_data = {
                    "job": job,
                    "tutor": job_application.tutor,
                    "champuser": job_application.tutor.champuser,
                    "tutor": job_application.tutor,
                    "application": job_application,
                    "applied_message": job_application.apply_message.message.msg if job_application.apply_message else '',
                    "start_time": job_application.start_time,
                    "currency": job_application.settlement.currency,
                    "price": job_application.settlement.price,
                    "price2": job_application.settlement.price2,
                    "exp_details": te.first().experience if te.exists() else "",
                    "status": status_type,
                    "initiated_as_label": initiated_as,
                    # "status": job_application.status,
                    "application_link": reverse("job_application_details_view", kwargs={ "job_id": job_id, "pk": job_application.pk }),
                }

                template_name = "ajax/job_details_tutor_row.html"
                template = loader.get_template(template_name)

                cntxt = Context(context_data)
                rendered = template.render(cntxt)
                rendered_contents += [ rendered ]
            record_count = job_applications.count()

        template_name = "ajax/job_details_tutor_list.html"
        template = loader.get_template(template_name)
        ctx = { "data_rows": rendered_contents, "job": job }
        cntxt = Context(ctx)
        rendered_data = template.render(cntxt)

        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": rendered_data,
            "record_count": record_count,
            "tab": status_type
        }
        return self.render_to_json(response)


class JobTutorListAjaxViewOld(LoginRequiredMixin,JSONResponseMixin, View):
    def get(self, request, *args, **kwargs):
        status_type = request.GET.get("status")  ###all, pending, negotiating, rejected, applied, shortlisted
        search_enable = request.GET.get("search")
        keyword = request.GET.get("q")
        job_id = request.GET.get("job")
        view_invited = request.GET.get("view_invited")
        view_recommended = request.GET.get("view_recommended")

        job = None

        try:
            job_id = int(job_id)
            job = Job.objects.get(pk=job_id)
        except:
            pass

        fltrd_tutor_list = []
        ###If search is enabled
        if search_enable == "1":
            ###First find the exact matches.
            if keyword:
                exacts = User.objects.filter(champuser__fullname__iexact=keyword)
                others =  User.objects.filter(champuser__fullname__icontains=keyword).exclude(pk__in=exacts.values_list('pk',flat=True))
                # results = itertools.chain(exacts, others)
                fltrd_tutor_list = exacts.values_list('pk',flat=True)
                fltrd_tutor_list = list(fltrd_tutor_list) + list(others.values_list('pk',flat=True))

        context_data = []
        ###Get All job invitation and recommendation.
        job_invitation_status = JobInvitationStatus.active.value
        if status_type == "shortlisted":
            job_application_qset = job.applications.filter(pk__in=job.shortlist.values_list('application_id', flat=True), status=9)

            if search_enable == "1":
                ###First find the exact matches.
                if keyword and fltrd_tutor_list:
                    job_application_qset = job_application_qset.filter(tutor_id__in=fltrd_tutor_list)

            if view_invited == "1":
                job_application_qset = job_application_qset.filter(initiated_as_label=JobInvitation.__class__.__name__)

            if view_recommended == "1":
                job_application_qset = job_application_qset.filter(initiated_as_label=JobRecommendation.__class__.__name__)


            for job_application in job_application_qset:
                te = TeachingExperiences.objects.filter(user_id=job_application.tutor.pk)
                context_data += [\
                        {
                            "job": job,
                            "tutor": job_application.tutor,
                            "rate": job_application.settlement.price,
                            "currency": job_application.settlement.currency,
                            "start_time": job_application.start_time,
                            "duration": job_application.duration,
                            "exp_details": te.first().experience if te.exists() else "",
                            "status_circle": status_type,
                            "status": job_application.status,
                            "application_link": reverse("job_application_details_view", kwargs={ "job_id": job_id, "pk": job_application.pk }),
                            "initiated_as_label": job_application.initiated_as_label
                        }
                    ]
        else:
            include_invitation = True
            if search_enable == "1" and view_invited == "0":
                include_invitation = False
            if include_invitation:
                job_invitation_qset = JobInvitation.objects.filter(job_id=job_id)
                if status_type == "all":
                    pass
                elif status_type == "pending":
                    job_invitation_status = JobInvitationStatus.active.value
                    job_invitation_qset = job_invitation_qset.filter(status = job_invitation_status)
                elif status_type == "negotiating":
                    job_invitation_status = JobInvitationStatus.negotiating.value
                    job_invitation_qset = job_invitation_qset.filter(status = job_invitation_status)
                elif status_type == "rejected":
                    job_invitation_status = JobInvitationStatus.rejected.value
                    job_invitation_qset = job_invitation_qset.filter(Q(status=JobInvitationStatus.rejected.value)|Q(status=JobInvitationStatus.rejected_application.value)|Q(status=JobInvitationStatus.rejected_offer.value))
                elif status_type == "applied":
                    job_invitation_status = JobInvitationStatus.applied.value
                    job_invitation_qset = job_invitation_qset.filter(Q(status = job_invitation_status) | Q(status = JobInvitationStatus.offered.value) | Q(status = JobInvitationStatus.hired.value))

                ###If search is enabled
                if search_enable == "1":
                    if keyword and fltrd_tutor_list:
                        job_invitation_qset = job_invitation_qset.filter(user_id__in=fltrd_tutor_list)

                for job_invitation in job_invitation_qset:
                    te = TeachingExperiences.objects.filter(user_id=job_invitation.user.pk)
                    application_link = None
                    j_a = job.applications.filter(tutor_id=job_invitation.user.pk)
                    if j_a.exists():
                        application_link = reverse("job_application_details_view", kwargs={ "job_id": job_id, "pk": j_a.first().pk })
                    context_data += [\
                            {
                                "job": job,
                                "tutor": job_invitation.user,
                                "rate": job.preference.rate.rate_value,
                                "currency": job.preference.rate.currency,
                                "start_time": job.lesson_time,
                                "duration": job.lesson_duration,
                                "exp_details": te.first().experience if te.exists() else "",
                                "status_circle": status_type,
                                "status": job_invitation.status,
                                "application_link": application_link,
                                "initiated_as_label": JobInvitation.__name__
                            }
                        ]

            include_recommendation = True
            if search_enable == "1" and view_recommended == "0":
                include_recommendation = False
            if include_recommendation:
                job_recommendation_qset = JobRecommendation.objects.filter(job_id=job_id)
                if status_type == "all":
                    pass
                elif status_type == "pending":
                    job_recommendation_status = JobRecommendationStatus.active.value
                    job_recommendation_qset = job_recommendation_qset.filter(status = job_recommendation_status)
                elif status_type == "negotiating":
                    job_recommendation_status = JobRecommendationStatus.negotiating.value
                    job_recommendation_qset = job_recommendation_qset.filter(status = job_recommendation_status)
                elif status_type == "rejected":
                    job_recommendation_status = JobRecommendationStatus.rejected.value
                    job_recommendation_qset = job_recommendation_qset.filter(Q(status=JobRecommendationStatus.rejected.value) | Q(status=JobRecommendationStatus.rejected_application.value) | Q(status=JobRecommendationStatus.rejected_offer.value))
                elif status_type == "applied":
                    job_recommendation_status = JobRecommendationStatus.applied.value
                    job_recommendation_qset = job_recommendation_qset.filter(Q(status = JobRecommendationStatus.applied.value) | Q(status = JobRecommendationStatus.offered.value) | Q(status = JobRecommendationStatus.hired.value))

                ###If search is enabled
                if search_enable == "1":
                    if keyword and fltrd_tutor_list:
                        job_recommendation_qset = job_recommendation_qset.filter(user_id__in=fltrd_tutor_list)

                for job_recommendation in job_recommendation_qset:
                    te = TeachingExperiences.objects.filter(user_id=job_recommendation.user.pk)
                    application_link = None
                    j__a = job.applications.filter(tutor_id=job_recommendation.user.pk)
                    if j__a.exists():
                        application_link = reverse("job_application_details_view", kwargs={ "job_id": job_id, "pk": j__a.first().pk })
                    context_data += [\
                            {
                                "job": job,
                                "tutor": job_recommendation.user,
                                "rate": job.preference.rate.rate_value,
                                "currency": job.preference.rate.currency,
                                "start_time": job.lesson_time,
                                "duration": job.lesson_duration,
                                "exp_details": te.first().experience if te.exists() else "",
                                "status_circle": status_type,
                                "status": job_recommendation.status,
                                "application_link": application_link,
                                "initiated_as_label": JobRecommendation.__name__
                            }
                        ]


        rendered_contents = []
        for ctx_data in context_data:
            template_name = "ajax/job_details_tutor_row.html"
            template = loader.get_template(template_name)

            cntxt = Context(ctx_data)
            rendered = template.render(cntxt)
            rendered_contents += [ rendered ]

        template_name = "ajax/job_details_tutor_list.html"
        template = loader.get_template(template_name)
        ctx = { "data_rows": rendered_contents, "job": job }
        cntxt = Context(ctx)
        rendered_data = template.render(cntxt)

        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": rendered_data
        }
        return self.render_to_json(response)

class RateConversionView(LoginRequiredMixin,JSONResponseMixin, View):
    def get(self, request, *args, **kwargs):
        base_currency = request.GET.get("basecurrency")
        target_currency = request.GET.get("targetcurrency")
        value = request.GET.get("value")

        if target_currency == "-1":
            country_name = ''
            tzm = TimezoneMapper.objects.filter(tz=request.user.champuser.timezone)
            if tzm.exists():
                tzm = tzm.first()
                country_name = tzm.country.name
            user_currency = CurrencyUtil.get_currency_code_for_country(country_name)
            if base_currency == user_currency:
                response = {
                    "status": "SUCCESS",
                    "message": "Successful",
                    "data": {
                        "decision": "NO_CONVERSION"
                    }
                }
                return self.render_to_json(response)
            else:
                target_currency = "USD"
                if user_currency in settings.BASE_CURRENCIES:
                    target_currency = user_currency
                converted_value = RateConversionUtil.convert(base_currency, target_currency, value)
                if converted_value:
                    response = {
                        "status": "SUCCESS",
                        "message": "Successful",
                        "data": {
                            "decision": "CONVERTED",
                            "base": base_currency.upper(),
                            "target": target_currency.upper(),
                            "value": value,
                            "converted": "%.2f" % float(converted_value)
                        }
                    }
                    return self.render_to_json(response)
                else:
                    response = {
                        "status": "SUCCESS",
                        "message": "Successful",
                        "data": {
                            "decision": "NO_CONVERSION_FOUND"
                        }
                    }
                    return self.render_to_json(response)

        converted_value = RateConversionUtil.convert(base_currency, target_currency, value)
        if converted_value:
            response = {
                "status": "SUCCESS",
                "message": "Successful",
                "data": {
                    "base": base_currency.upper(),
                    "target": target_currency.upper(),
                    "value": value,
                    "converted": "%.2f" % float(converted_value)
                }
            }
            return self.render_to_json(response)
        else:
            response = {
                "status": "FAILURE",
                "message": "No conversion found",
                "data": {
                }
            }
            return self.render_to_json(response)


class UserUpdateNotificationView(LoginRequiredMixin, JSONResponseMixin, View):

    def get_object(self):
        return get_object_or_404(ChampUser, user=self.request.user)

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            champuser = self.get_object()
            # toggle email notification flag
            champuser.toggle_email_notification()
            response = {
                "status": "SUCCESS"
            }
            return self.render_to_json(response)
        raise Http404

class JobRepostPreviewAjaxView(LoginRequiredMixin, JSONResponseMixin, View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(JobRepostPreviewAjaxView, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        response = {
            "status": "FAILURE",
            "message": "Failed",
            "data": ""
        }

        job_id = request.POST.get("object_id")
        try:
            job_object = Job.objects.filter(pk=int(job_id)).exclude(status=4).first()

            job_expire_date = datetime.strptime(request.POST.get('expire_date'), '%m/%d/%Y')

            job_start_date = datetime.strptime(request.POST.get('start_date'), '%m/%d/%Y')

            job_start_time = datetime.strptime(request.POST.get('start_time'), '%H:%M')

            job_start_datetime = datetime(day=job_start_date.day, month=job_start_date.month, year=job_start_date.year,
                                          hour=job_start_time.hour, minute=job_start_time.minute, second=job_start_time.second)

            lesson_timestamp = Clock.convert_local_to_utc_timestamp(request.user.champuser.timezone,
                                                                    time.mktime(job_start_datetime.timetuple()))

            job_object.lesson_time = lesson_timestamp

            end_date = Clock.convert_local_to_utc_timestamp(request.user.champuser.timezone,
                                                                    time.mktime(job_expire_date.timetuple()))

            job_object.end_date = end_date

            job_object.save()

            response['status'] = 'SUCCESS'
            response['message'] = 'Successful'
            response['data'] = {
                'redirect_url': reverse('job_post_details', kwargs={ 'pk': job_object.pk })
            }
            return self.render_to_json(response)

        except Exception as exp:
            response['status'] = 'FAILURE'
            response['message'] = 'Failed'
            return self.render_to_json(response)

class FetchJobUpdate(LoginRequiredMixin, JSONResponseMixin, View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(FetchJobUpdate, self).dispatch(*args, **kwargs)


    def get(self, request, *args, **kwargs):
        job_ids = request.GET.get("job_ids")
        detail = request.GET.get("detail")
        list_output = request.GET.get("list", 1)

        response = {
            "status": "FAILURE",
            "message": "Failed",
            "data": ""
        }

        try:
            job_ids = [ int(jid) for jid in job_ids.split(",") if jid ]
            job_objects = Job.objects.filter(pk__in=job_ids)
        except Exception as exp:
            job_ids = []
            job_objects = Job.objects.none()
            response['message'] = 'Invalid job id'
            return self.render_to_json(response)

        if not job_objects:
            response['message'] = 'Job id required'
            return self.render_to_json(response)

        if not list_output:
            job_ids = [ job_ids[0] ]

        job_records = {}

        for job_object in job_objects:
            job_data = {}
            job_data["pending_count"] = job_object.applications.filter(status=2).count()
            job_data["invitation_count"] = JobInvitation.objects.filter(job_id=job_object.pk,status=1).count()
            job_data["rejected_count"] = JobInvitation.objects.filter(Q(job_id=job_object.pk) & (Q(status=5) | Q(status=10) | Q(status=12))).count()
            job_data["shortlisted_count"] = JobInvitation.objects.filter(job_id=job_object.pk, status=9).count()
            job_data["hired_count"] = JobInvitation.objects.filter(job_id=job_object.pk,status=8).count()
            
            job_records[job_object.pk] = job_data

        response['status'] = 'SUCCESS'
        response['message'] = 'Successful'
        response['data'] = job_records
        return self.render_to_json(response)





















