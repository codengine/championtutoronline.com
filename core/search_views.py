from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from core.library.country_util import CountryUtil
from core.library.currency_util import CurrencyUtil
from core.library.job_util import JobUtil
from core.library.lang_util import LanguageUtil
from core.library.subject_util import SubjectUtil
from core.models3 import Role

__author__ = 'codengine'

from django.views.generic import View,ListView
from django.shortcuts import render
from easy_thumbnails.files import get_thumbnailer
from core.models3 import ChampUser,Profile,ProfilePicture,UserMajor
import math
from core.enums import AvailableStatus,OnlineStatus,UserTypes
from django.db.models import Q
from core.models3 import *

class TutorSearchView(ListView):
    model = ChampUser
    template_name = "tutor_search.html"
    # paginate_by = 5
    # queryset = ChampUser.objects.filter(type=Role.objects.get(name=UserTypes.teacher.value))
    # show_pages = 13

    def get_context_data(self, **kwargs):
        """
        Get the context for this view.
        """
        context = super(TutorSearchView,self).get_context_data(**kwargs)

        country_ids = CountryUtil.get_used_countries()

        country_options = CountryUtil.get_supported_countries(filter_ids=country_ids)

        context["countries"] = country_options
        context["currencies"] = CurrencyUtil.get_supported_currencies()

        country_name = None

        try:
            country_name = self.request.session["user_info"]["country"]
        except:
            country_name = "Other"

        context["academic_levels"] = academic_levels = SubjectLevel.objects.filter(name__in=SubjectUtil.get_levels_for_country(country_name))

        if self.request.GET.get("keyword"):
            context["keyword"] = self.request.GET.get("keyword")
        if self.request.GET.get("subjects"):
            context["subjects"] = self.request.GET.get("subjects").split(',')

        levels = self.request.GET.get("level").split(',') if self.request.GET.get("level") else []
        if levels:
            context["initial_levels"] = levels

            context["subjects"] = Major.objects.filter(level__name__in=levels)

        subjects = self.request.GET.get("subject").split(',') if self.request.GET.get("subject") else []
        if subjects:
            context["initial_subjects"] = subjects
            if not self.request.GET.get("level"):
                context["initial_levels"] = initial_levels = [ subject.level.name for subject in Major.objects.filter(name__in=subjects) ]

                context["subjects"] = Major.objects.filter(level__name__in=initial_levels)

        ready_to_teach = self.request.GET.get("ready_to_teach")
        if ready_to_teach:
            context["ready_to_teach"] = ready_to_teach

        online = self.request.GET.get("online")
        if online:
            context["online"] = online

        gender = self.request.GET.get("gender")
        if gender:
            context["gender"] = gender
        else:
            context["gender"] = "no_pref"


        budget = self.request.GET.get("budget")
        if budget:
            budgets = [ b for b in budget.split(",") if b ]
            if "sgd1" in budgets:
                context["sgd1"] = True
            if "sgd2" in budgets:
                context["sgd2"] = True
            if "sgd3" in budgets:
                context["sgd3"] = True
            if "sgd4" in budgets:
                context["sgd4"] = True


        keywords = self.request.GET.get("keyword")
        if keywords:
            context["keywords"] = keywords

        video_intro = self.request.GET.get("video-intro")
        if video_intro:
            context["video_intro"] = video_intro

        countries = self.request.GET.get("country").split(',') if self.request.GET.get("country") else []
        if countries:
            context["initial_countries"] = countries

        page = self.request.GET.get("page")
        if page:
            context["page"] = page

        context["search"] = False

        context["top_keyword "] = None
        if keywords:
            context["top_keyword "] = keywords
        elif self.request.GET.get("subject"):
            context["top_keyword"] = self.request.GET.get("subject")
        elif self.request.GET.get("level"):
            context["top_keyword"] = self.request.GET.get("level")

        return context

class MarketplaceView(ListView):
    model = Job
    template_name = "marketplace.html"

    def get_context_data(self, **kwargs):
        context = super(MarketplaceView, self).get_context_data(**kwargs)
        countries = CountryUtil.get_supported_countries()
        languages = LanguageUtil.get_supported_languages()
        currencies = CurrencyUtil.get_supported_currencies()
        context["languages"] = languages
        context["currencies"] = currencies
        context["countries"] = countries
        return context

class MarketplaceDetailsView(DetailView):
    model = Job
    template_name = "marketplace_details.html"

    def get_template_names(self):
        if self.object.is_deleted:
            return [
                "404.html"
            ]
        return [
            "marketplace_details.html"
        ]

    def get_context_data(self, **kwargs):
        context = super(MarketplaceDetailsView, self).get_context_data(**kwargs)
        context["applied"] = self.object.applications.filter(tutor_id=self.request.user.pk).exists()
        ###Check if he is invited or recommended.
        invite_type = None
        show_action_bar = False
        status = None
        application_url = None
        show_public_apply = True
        if self.object.posted_by.pk != self.request.user.pk:
            ji_objects = JobInvitation.objects.filter(job_id=self.object.pk,user_id=self.request.user.pk)
            jr_objects = JobRecommendation.objects.filter(job_id=self.object.pk,user_id=self.request.user.pk)
            if ji_objects.exists():
                invite_type = "INVITED"
                show_action_bar = True
                ji_object = ji_objects.first()
                status = ji_object.status
            else:
                job_recommended = JobUtil.check_if_job_is_recommended_for_user(self.request.user.pk, kwargs.get("pk"))
                if job_recommended:
                    invite_type = "RECOMMENDED"
                    status = 1 ###Pending
                    show_action_bar = True
                    applied = Job.objects.filter(pk=self.object.pk, applications__tutor__pk=self.request.user.pk).exists()
                    if applied:
                        job_application = Job.objects.get(pk=self.object.pk).applications.filter(tutor_id=self.request.user.pk).first()
                        status = job_application.status

        # if JobApplication.objects.filter(job__id=self.object.pk, tutor_id=self.request.user.pk, status=JobRecommendationStatus.rejected_application.value).exists():
        #     show_public_apply = False

        if invite_type != "INVITED" and invite_type != "RECOMMENDED":
            applications = self.object.applications.filter(tutor_id=self.request.user.pk)
            if applications.exists():
                application = applications.first()
                status = application.status

        context["invite_type"] = invite_type
        context["show_action_bar"] = show_action_bar
        context["show_public_apply"] = show_public_apply
        context["status"] = status
        context["pending_count"] = self.object.applications.filter(status=2).count()
        context["invited_count"] = JobInvitation.objects.filter(job_id=self.object.pk,status=1).count()
        context["rejected_count"] = JobInvitation.objects.filter(Q(job_id=self.object.pk) & (Q(status=5) | Q(status=10) | Q(status=12))).count()
        context["shortlisted_count"] = JobInvitation.objects.filter(job_id=self.object.pk, status=9).count()
        context["hired_count"] = JobInvitation.objects.filter(job_id=self.object.pk,status=8).count()
        context["application_url"] = application_url

        price = self.object.preference.rate.rate_value
        price2 = self.object.preference.rate.rate_value2
        currency = self.object.preference.currency
        lesson_duration = self.object.lesson_duration
        start_date = self.object.lesson_time

        job_applications = JobApplication.objects.filter(job__id=self.object.pk, tutor_id=self.request.user.pk)
        if job_applications.exists():
            job_application = job_applications.first()
            currency = job_application.settlement.currency
            price = job_application.settlement.price
            price2 = job_application.settlement.price2
            lesson_duration = job_application.duration
            start_date = job_application.start_time

        context['currency'] = currency
        context['price'] = price
        context['price2'] = price2
        context['lesson_duration'] = lesson_duration
        context['start_date'] = start_date

        return context
