from django import template

register = template.Library()

@register.filter(name='highlight_search_keyword')
def highlight_search_keyword(text, keyword):
    try:
        if keyword:
            return text.replace(keyword, "<b class='highlight_node'>"+keyword+"</b>")
    except:
        pass
    return text