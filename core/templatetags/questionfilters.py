import random
import pytz
from core.library.Clock import Clock

__author__ = 'codengine'

from django import template

register = template.Library()


@register.filter(name='likes_count')
def likes_count(objects):
    return objects.filter(value=1).count()


@register.filter(name='dislikes_count')
def dislikes_count(objects):
    return objects.filter(value=2).count()


@register.filter(name='name_initials')
def name_initials(name):
    values = name.split()
    if len(values) >= 2:
        return "%s%s" % (values[0][:1], values[1][:1])
    elif len(values) == 1:
        return name[:2]
    return name


@register.filter(name='random_hex')
def random_hex(value):
    r = lambda: random.randint(0, 255)
    return '#%02X%02X%02X' % (r(), r(), r())