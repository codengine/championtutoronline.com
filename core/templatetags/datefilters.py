import time
import pytz
from core.library.Clock import Clock

__author__ = 'codengine'

from django import template
from datetime import datetime
from common.library import get_date_diff

register = template.Library()

@register.filter(name='date_diff')
def date_diff(value,format_string):
    return get_date_diff(value,format_string)

@register.filter(name='date_diff_timestamp')
def date_diff_timestamp(timestamp):
    datetime_obj = datetime.fromtimestamp(int(timestamp))
    datetime_now = datetime.now()
    diff = datetime_now - datetime_obj
    diff_days = diff.days
    if diff_days > 30:
        return datetime_obj.strftime("%b %d, %Y %H:%M%p")
    diff_seconds = diff.seconds
    diff_str = ""
    if diff_days > 0:
        diff_str = str(diff_days)+" days ago"
        return diff_str
    hours = diff_seconds / 3600
    if hours > 0:
        diff_str = str(hours)+" hours ago"
        return diff_str
    remaining_seconds = diff_seconds % 3600
    mins = remaining_seconds / 60
    if mins > 0:
        diff_str = str(mins)+" minutes ago"
        return diff_str
    remaining_seconds = remaining_seconds % 60
    if remaining_seconds >= 0:
        diff_str += str(remaining_seconds)+" seconds "
    if diff_str:
        diff_str += " ago"
    return diff_str

@register.filter(name="render_short_datestring_from_timestamp")
def render_short_datestring_from_timestamp(value):
    datetime_obj = datetime.fromtimestamp(int(value))
    if datetime_obj.day == datetime.now().day:
        return datetime_obj.strftime("%H:%M%p")
    else:
        datetime_obj.strftime("%b, %d %Y at %H:%M%p")


@register.filter(name='datetime_from_timestamp')
def datetime_from_timestamp(timestamp):
    datetime_obj = datetime.fromtimestamp(int(timestamp))
    return datetime_obj.strftime("%b, %d %Y at %H:%M%p")

@register.filter(name='date_from_timestamp')
def date_from_timestamp(timestamp):
    datetime_obj = datetime.fromtimestamp(int(timestamp))
    return datetime_obj.strftime("%b, %d %Y")

@register.filter(name='format_time_from_min')
def format_time_from_min(min):
    min = int(min)
    if min < 60:
        return str(min)+" min"
    return str(min/60)+" hour "+(str(min%60)+" min" if (min%60) > 0 else '')

@register.filter(name='convert_utc_timestamp_to_local')
def convert_utc_timestamp_to_local(timestamp,request):
    import time
    #timestamp = int(time.mktime(datetime.utcnow().timetuple()))
    utc_dt = pytz.utc.localize(datetime.fromtimestamp(int(timestamp)))
    local_dt = Clock.utc_to_local_datetime(datetime.fromtimestamp(int(timestamp)),request.user.champuser.timezone)
    print(local_dt)
    #local_dt.replace(tzinfo=None)
    print(int(local_dt.strftime("%s")))
    print(int(time.mktime(local_dt.timetuple())))
    return int(time.mktime(local_dt.timetuple()))
    # print(local_dt)
    # local_timestamp = int(local_dt.strftime("%s"))
    # print(datetime.fromtimestamp(local_timestamp))
    # return local_timestamp

@register.filter(name='convert_utc_timestamp_to_local_email_thread')
def convert_utc_timestamp_to_local_email_thread(timestamp, user_timezone):
    import time
    # utc_dt = pytz.utc.localize(datetime.fromtimestamp(int(timestamp)))
    local_dt = Clock.utc_to_local_datetime(datetime.fromtimestamp(int(timestamp)), user_timezone)
    return int(time.mktime(local_dt.timetuple()))


@register.filter(name='convert_utc_timestamp_to_local_datetime')
def convert_utc_timestamp_to_local_datetime(timestamp,request):
    return Clock.utc_to_local_datetime(datetime.fromtimestamp(int(timestamp)),request.user.champuser.timezone) #datetime.fromtimestamp(local_timestamp).strftime("%b %d, %Y at %I:%M%p")

@register.filter(name='fromunix')
def fromunix(value):
    return datetime.fromtimestamp(int(value))

@register.filter(name='format_datetime')
def format_datetime(dt):
    return dt.strftime("%d %b %Y @ %I:%M %p")

@register.filter(name='format_datetime2')
def format_datetime2(dt,format):
    return dt.strftime(format)

@register.filter(name='format_datediff')
def format_date_difference(dt):
    now_time = pytz.utc.localize(Clock.utc_now())
    return (now_time - dt).days/365

@register.filter(name='current_date')
def current_date(request):
    return datetime.fromtimestamp(int(Clock.utc_timestamp()))

@register.filter(name='localize_user_timezone')
def localize_user_timezone(user):
    local_tzinfo = user.champuser.timezone
    ltz = pytz.timezone(local_tzinfo)
    now_time = datetime.now(tz=pytz.utc)
    local_time = now_time.astimezone(ltz)
    return local_time