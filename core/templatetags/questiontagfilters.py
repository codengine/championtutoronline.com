__author__ = 'Sohel'

from django import template

register = template.Library()

@register.filter(name='check_if_user_already_commented')
def check_if_user_already_commented(question,user): ###this is a User instance
    if question.answers.filter(answer_by_id=user.pk).exists():
        return True
    return False


@register.filter
def taglist(tags):
    """
    Returns list of tag names
    """
    return [tag.name for tag in tags]


@register.filter
def get_answer_of_user(question, user):
    """
    Returns :model:'core.QAnswer' of the 'user' parameter based
    on the given 'question'. Returns None if does not exist.
    """
    return question.answers.filter(answer_by=user).first()

@register.filter
def replace_last_comma_with_and(data):
    """
    Replaces the last comma from a string with 'and'
    """
    return ' and '.join(data.rsplit(',', 1))
