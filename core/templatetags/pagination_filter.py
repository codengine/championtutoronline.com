
__author__ = 'Sohel'

from django import template

register = template.Library()

@register.filter(name='calculate_page_range')
def calculate_page_range1(page_obj,current_page=1,show_page=10): ###this is a User instance
    if page_obj.paginator.num_pages <= 0:
        return []
    print(current_page)
    start_page = ((current_page/show_page) + 1) * show_page -  (((current_page/show_page) + 1) * show_page - current_page)
    print(start_page)
    if start_page <= 0:
        start_page = 1
    end_page = start_page + show_page
    if end_page > page_obj.paginator.num_pages:
        end_page = page_obj.paginator.num_pages
    return [ i for i in range(start_page, end_page) ]

@register.filter(name='calculate_page_range')
def calculate_page_range(page_obj,page):
    pages_wanted = set([1,2,page-2, page-1,page,page+1, page+2,page_obj.num_pages-1, page_obj.num_pages])
    pages_to_show = set(page_obj.page_range).intersection(pages_wanted)
    pages_to_show = sorted(pages_to_show)
    skip_pages = [ x[1] for x in zip(pages_to_show[:-1],
                                     pages_to_show[1:])
                   if (x[1] - x[0] != 1) ]
    for i in skip_pages:
        pages_to_show.insert(pages_to_show.index(i), -1)

    return pages_to_show