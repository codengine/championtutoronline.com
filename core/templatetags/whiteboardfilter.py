__author__ = 'Sohel'

from django import template

register = template.Library()

@register.filter(name='get_user_messages')
def get_user_messages(whiteboard_instance,request): ###this is a User instance
    tutor_chat_objects = whiteboard_instance.tutor_chat.all().order_by("date_created")
    return tutor_chat_objects
