from django.core.urlresolvers import reverse
from core.enums import JobRecommendationStatus
from core.models3 import JobApplication

__author__ = 'Sohel'

from django import template

register = template.Library()

@register.filter(name='check_if_user_invited')
def check_if_user_invited(job,request): ###this is a User instance
    return request.user.champuser.check_if_invited_to_job(job)

@register.filter(name='check_if_apply_button_should_render')
def check_if_apply_button_should_render(job,request):
    return not job.applications.filter(tutor_id=request.user.pk).exists() and request.user.champuser.type.name != "student"

@register.filter(name='check_if_already_applied')
def check_if_already_applied(job,request):
    return not job.posted_by.pk == request.user.pk and job.applications.filter(tutor_id=request.user.pk).exists()

@register.filter(name='render_job_application_url')
def render_job_application_url(job,request):
    current_user = request.user
    job_application = JobApplication.objects.filter(job__id=job.pk, tutor_id=current_user.pk)
    if job_application.exists():
        job_application = job_application.first()
        return reverse("job_application_details_view", kwargs={ "job_id": job.pk, "pk": job_application.pk  })