from django.core.urlresolvers import resolve

__author__ = 'Sohel'

from django import template

register = template.Library()

@register.filter(name='current_url_name')
def current_url_name(request):
    try:
        return resolve(request.path).url_name
    except:
        pass


@register.filter(name='reverse_list')
def reverse_list(llist):
    rl = list(llist)
    return rl

@register.filter(name='replace_tags')
def replace_tags(html):
    return html.replace("<p>", "").replace("</p>", "<br/>")