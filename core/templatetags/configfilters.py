from django import template
from engine.config.servers import APP_SERVER_HOST,APP_SERVER_PORT,SOCKETIO_HOST,SOCKETIO_PORT,ETHERPAD_SERVER_HOST,ETHERPAD_SERVER_PORT, \
    APP_SERVER_IP
from engine.config.servers import SHAREJS_SERVER_HOST,SHAREJS_SERVER_PORT
from engine.iputils import get_client_ip

__author__ = 'codengine'

register = template.Library()

@register.filter(name='get_application_server_url')
def get_application_server_url(request):
    return APP_SERVER_HOST+':'+str(APP_SERVER_PORT)+'/'

@register.filter(name='get_application_server_ip')
def get_application_server_ip(request):
    return APP_SERVER_IP+':'+str(APP_SERVER_PORT)+'/'

@register.filter(name='get_socket_server_url')
def get_socket_server_url(request):
    return SOCKETIO_HOST+':'+str(SOCKETIO_PORT)+'/'

@register.filter(name='get_socket_server_ip')
def get_socket_server_ip(request):
    return APP_SERVER_IP+':'+str(SOCKETIO_PORT)+'/'

@register.filter(name='get_etherpad_server_url')
def get_etherpad_server_url(request):
    return ETHERPAD_SERVER_HOST+':'+str(ETHERPAD_SERVER_PORT)+'/'

@register.filter(name='get_sharejs_server_url')
def get_sharejs_server_url(request):
    return SHAREJS_SERVER_HOST+':'+str(SHAREJS_SERVER_PORT)+'/'
