from django import template
from django.core.urlresolvers import reverse
from django.db.models.fields import BigIntegerField

__author__ = 'codengine'

register = template.Library()

@register.filter(name='render_property')
def render_property(object, property):
    fields = object.__class__._meta.fields
    for field in fields:
        try:
            if ":" in property:
                property = property[:property.index(":")]
        except:
            pass
        field_name = field.name
        if field_name == property:
            if isinstance(field, BigIntegerField):
                return getattr(object,property), "__RENDER_TS"
    return getattr(object,property), None

@register.filter(name='split_search_property')
def split_search_property(name):
    if ":" in name:
        search_name = name[:name.index(":")]
        label = name[name.index(":")+1:]
        return {
            "name": search_name,
            "label": label
        }
    return {
        "name": name,
        "label": name
    }

@register.filter(name='get_search_name')
def get_search_name(name):
    return split_search_property(name)['name']

@register.filter(name='get_search_label')
def get_search_label(name):
    return split_search_property(name)['label']

@register.filter(name='render_detail_url')
def render_property(url_name, pk):
    return reverse(url_name,kwargs={ "pk": pk })

@register.filter(name='capitalize_sentence')
def capitalize_sentence(object):
    return (' '.join([ s.capitalize() for s in object.split('_') ])).capitalize()

@register.filter(name='render_details_url')
def render_details_url(url_name,id):
    try:
        return reverse(url_name,kwargs={ "pk": id })
    except:
        return "#"