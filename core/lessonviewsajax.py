from decimal import Decimal
from core.enums import JobStatus
from core.generics.mixins import LoginRequiredMixin
from core.library.Clock import Clock
from core.library.currency_util import CurrencyUtil
from core.library.email_scheduler import EmailScheduler
from core.library.email_template_util import EmailTemplateUtil
from core.library.lesson_util import LessonUtil
from notification.utils.notification_manager import NotificationManager
from django_bootstrap_calendar.models import CalendarEvent
from engine.libutil.TimeUtil import TimeUtil
from engine.libutil.paymentutil import PaymentCurrencyUtil
from notification.models import Notification
from payment.payment_processor import PaymentProcessor
from pyetherpad.models import PadAuthor
from pyetherpad.padutil import PadUtil
from pysharejs.CodePadUtil import CodePadUtil

__author__ = 'codengine'

from django.views.generic import View
from django.template import loader, Context
from core.generics.ajaxmixins import JSONResponseMixin
import os
from datetime import datetime
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.db import transaction
from django.core.files import File
import uuid
from datetime import datetime
import time
from core.models3 import *
import pytz

class LessonRequestAjaxView(JSONResponseMixin,View):

    # @method_decorator(csrf_exempt)
    # def dispatch(self, *args, **kwargs):
    #     return super(MessageAjaxView, self).dispatch(*args, **kwargs)

    def post(self,request,*args,**kwargs):
        response = {
            "status": "FAILURE",
            "message": "FAILURE",
            "data": []
        }
        recipients = request.POST.get("hidden_ids")
        msg_txt = request.POST.get("msg_text")
        #raw_data = request.raw_post_data
        attachment = request.FILES.get("msg_attachment")
        lesson_request_enabled = request.POST.get("lesson-request-enabled")
        if recipients and msg_txt:
            with transaction.atomic():
                for recipient in recipients.split(','):
                    try:
                        recipient = int(recipient)
                        msg = Message()
                        msg.chat_type = ChatType.p2p.value
                        msg.is_read = ReadStatus.unread.value
                        msg.msg_date = Clock.utc_timestamp()
                        msg.msg = msg_txt
                        # msg.create_date = datetime.now()
                        # msg.modified_date = datetime.now()
                        msg.save()

                        user_msg = UserMessage()
                        user_msg.sender = request.user
                        user_msg.receiver = User.objects.get(pk=recipient)
                        user_msg.message = msg
                        user_msg.save()

                        if attachment:
                            file_name = str(request.user.id)+"."+attachment._name[attachment._name.rindex(".")+1:]
                            print file_name
                            with open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name), 'wb+') as destination:
                                for chunk in file.chunks():
                                    destination.write(chunk)
                            attached_file = File(open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name)))

                            msg_attachment = MessageAttachment()
                            msg_attachment.message = msg
                            msg_attachment.attachment = attached_file
                            msg_attachment.save()

                        if lesson_request_enabled == "1":
                            lesson_date = request.POST.get("lesson_date")
                            lesson_time = request.POST.get("lesson_time")
                            lesson_repeat = request.POST.get("lesson-repeat")
                            duration = request.POST.get("duration")
                            course = request.POST.get("course")

                            lesson_date = datetime.strptime(lesson_date,"%d/%m/%Y")
                            lesson_time = datetime.strptime(lesson_time,"%I:%M %p")

                            lesson_datetime = datetime.combine(lesson_date.date(),lesson_time.time())

                            lesson_timestamp = Clock.convert_local_to_utc_timestamp(request.user.champuser.timezone,time.mktime(lesson_datetime.timetuple()))

                            duration = int(duration)

                            live_lesson = LiveLesson()
                            live_lesson.user = request.user
                            #live_lesson.lesson_date = time.mktime(lesson_datetime.timetuple())
                            live_lesson.lesson_time = lesson_timestamp
                            live_lesson.request_user = User.objects.get(pk=recipient)
                            #live_lesson.lesson_timestamp = lesson_timestamp #time.mktime(lesson_datetime.timetuple())
                            live_lesson.duration = int(duration)
                            live_lesson.course = Major.objects.get(pk=int(course))
                            live_lesson.message = user_msg
                            live_lesson.repeat = lesson_repeat
                            live_lesson.approved = ApprovalStatus.not_approved.value
                            live_lesson.save()

                    except Exception,msg:
                        print "Exception occured."
                        print msg
        else:
            response["status"] = "FAILURE"
            response["message"] = "Recipients or message missing."
            return self.render_to_json(response)



        response["status"] = "SUCCESS"
        response["message"] = "Successful"
        return self.render_to_json(response)

class LessonStatusUpdateAjaxView(JSONResponseMixin,View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(LessonStatusUpdateAjaxView, self).dispatch(*args, **kwargs)

    def post(self,request,*args,**kwargs):
        response = {
            "status": "FAILURE",
            "message": "FAILURE",
            "data": []
        }
        action = request.POST.get("action")
        lesson_id = request.POST.get("lesson_id")
        if action == "update_access_time":
            now_time = Clock.utc_timestamp()
            live_lesson = LiveLesson.objects.get(pk=int(lesson_id))
            total_lesson_time = live_lesson.lesson_time + live_lesson.duration * 1000 + live_lesson.extra_time * 1000
            if live_lesson.lesson_time and live_lesson.lesson_time <= now_time and now_time <= total_lesson_time:
                
                user_activities = live_lesson.user_activities.filter(user_id=request.user.pk)
                if not user_activities:
                    user_activity = LessonUserActivity()
                    user_activity.user = request.user
                    user_activity.last_access_time = now_time
                    user_activity.online_status = 1
                    user_activity.save()
                    live_lesson.user_activities.add(user_activity)
                else:
                    user_activity = user_activities.first()
                    user_activity.last_access_time = now_time
                    user_activity.online_status = 1
                    user_activity.save()
                
                live_lesson.save()

                remaining_time = total_lesson_time - now_time
                if remaining_time < 0:
                    remaining_time = 0
                dialog_content = ""
                dialog_title = ""
                show_alert = 0
                contxt_data = {
                    "title": dialog_title,
                    "remaining_time": int(remaining_time / 1000),
                    "lesson": live_lesson,
                    "notify_time": True,
                    "request": request,
                    "STATIC_URL": settings.STATIC_URL+"/"
                }
                if remaining_time <= 0:
                    contxt_data["title"] = "Time is Over"
                    contxt_data["notify_time"] = False
                    show_alert = 1
                elif remaining_time > 0 and remaining_time < 5000:
                    contxt_data["title"] = "Time Alert"
                    contxt_data["notify_time"] = True
                    show_alert = 1

                template = loader.get_template("ajax/reminder_dialog_content.html")
                cntxt = Context(contxt_data)
                dialog_content = template.render(cntxt)
                response["status"] = "SUCCESS"
                response["message"] = "Successful"
                response["data"] = {
                    "remaining_time": int(remaining_time / 1000),
                    "dialog_content": dialog_content,
                    "show_alert": show_alert
                }
            else:
                if live_lesson.lesson_time > now_time:
                    response["status"] = "SUCCESS"
                    response["message"] = "Successful"
                    response["data"] = {
                        "time_lap": int((live_lesson.lesson_time - now_time)/60)
                    }
        return self.render_to_json(response)

class WrittenLessonSubjectAjaxView(JSONResponseMixin,View):
    def get(self,request,*args,**kwargs):
        response = {
            "status": "FAILURE",
            "message": "FAILURE",
            "data": []
        }
        tutor_id = request.GET.get("tid")
        user_majors_objs = UserMajor.objects.filter(user_id=int(tutor_id))
        response["status"] = "SUCCESS"
        response["message"] = "Successful"
        for user_major in user_majors_objs:
            response['data'] += [{"id":user_major.major.pk,"name":user_major.major.name}]

        return self.render_to_json(response)

class LessonRescheduleAjaxView(JSONResponseMixin,View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(LessonRescheduleAjaxView, self).dispatch(*args, **kwargs)

    def get(self,request,*args,**kwargs):
        if request.is_ajax():
            response = {
                "status": "FAILURE",
                "message": "FAILURE",
                "data": []
            }
            lesson_id = int(request.GET.get("lesson_id"))
            lesson = LessonRequest.objects.get(pk=lesson_id)
            job = lesson.lesson_request.first()
            job_applications = JobApplication.objects.filter(job__lesson_id=lesson_id).exclude(job__status=4)
            rate = None
            currency = None
            if job_applications.exists():
                job_application = job_applications.first()
                rate = job_application.settlement.price
                currency = job_application.settlement.currency
            else:
                response["status"] = "FAILURE"
                response["message"] = "Job application or job does not exist."
                response["data"] = {}
                return self.render_to_json(response)
            template_name = "ajax/lesson_reschedule_dialog.html"
            template = loader.get_template(template_name)

            lesson_time_local = Clock.utc_timestamp_to_local_datetime(lesson.lesson_time, request.user.champuser.timezone)

            context = {
                "lesson": lesson,
                "lesson_date": lesson_time_local.strftime("%d/%m/%Y %I:%M"),
                "job": job,
                "rate": rate,
                "currency": currency
            }

            cntxt = Context(context)
            rendered = template.render(cntxt)

            response["status"] = "SUCCESS"
            response["message"] = "Successful"
            response["data"] = {
                "dialog_content": rendered
            }
            return self.render_to_json(response)

    def post(self,request,*args,**kwargs):
        response = {
            "status": "FAILURE",
            "message": "FAILURE",
            "data": []
        }
        try:
            lesson_id = int(request.POST.get("lesson_id"))
            lesson = LessonRequest.objects.get(pk=lesson_id)
            reschedule_time = request.POST.get("reschedule_time")

            now_time = Clock.utc_timestamp()
            if lesson.lesson_time > now_time:

                dt_stamp = datetime.strptime(reschedule_time, "%d/%m/%Y %I:%M")

                localtimezone = pytz.timezone(request.user.champuser.timezone)
                localmoment = localtimezone.localize(dt_stamp)

                utcmoment = localmoment.astimezone(pytz.utc)


                dt_stamp = Clock.convert_datetime_to_utc_timestamp(utcmoment)

                previous_time = lesson.lesson_time

                lesson.lesson_time = dt_stamp
                lesson.save()

                previous_start_time = datetime.fromtimestamp(previous_time)
                previous_end_time = datetime.fromtimestamp(previous_time + lesson.duration)

                new_start_time = datetime.fromtimestamp(dt_stamp)
                new_end_time = datetime.fromtimestamp(dt_stamp + lesson.duration)

                CalendarEvent.update_event_time(lesson.user.pk, previous_start_time, previous_end_time, new_start_time, new_end_time)
                CalendarEvent.update_event_time(lesson.request_user.pk, previous_start_time, previous_end_time, new_start_time, new_end_time)
                for _user in lesson.other_users.all():
                    CalendarEvent.update_event_time(_user.pk, previous_start_time, previous_end_time, new_start_time, new_end_time)


                response["status"] = "SUCCESS"
                response["message"] = "Successful"

                lesson_users = []
                lesson_users += [ lesson.user ]
                lesson_users += [ lesson.request_user ]
                for _u in lesson.other_users.all():
                    lesson_users += [ _u ]

                for user in lesson_users:
                    html, text = EmailTemplateUtil.render_lesson_reschedule_email(lesson_id, 0, user.champuser.fullname, dt_stamp)

                    email_object = {
                        "recipients": [ user.email ],
                        'subject': 'Lesson Rescheduled',
                        'html_body': html,
                        'text_body': text
                    }

                    EmailScheduler.place_to_queue(email_object)

                    NotificationManager.push_notification('lesson_rescheduled',lesson_id,request.user.pk,user.pk, lesson_type=0, reschedule_time=reschedule_time)
        except Exception as msg:
            pass
        return self.render_to_json(response)

class WrittenLessonAjaxView(JSONResponseMixin,View):
    def post(self,request,*args,**kwargs):
        response = {
            "status": "FAILURE",
            "message": "FAILURE",
            "data": []
        }
        tutor_id = request.POST.get("select_tutor")
        subject_id = request.POST.get("select_subject")
        msg = request.POST.get("written_lesson_text")
        due_date = request.POST.get("due-date")
        duration = request.POST.get('duration')

        if not tutor_id or not subject_id or not msg:
            response["message"] = "Missing Parameter"
            return self.render_to_json(response)

        try:
            d_date = datetime.strptime(due_date,"%d/%m/%Y %H:%M %p")
            duration = int(duration)
        except Exception,msg:
            print("Exception Ocured. Exception Message: " + str(msg))
            response["status"] = "FAILURE"
            response["message"] = "Invalid parameter given."
            response["data"] = []
            return self.render_to_json(response)

        if True:
            msg = Message()
            msg.msg_date = datetime.now()
            msg.msg = msg
            msg.is_read = ReadStatus.unread.value
            msg.chat_type = ChatType.p2p.value
            # msg.create_date = datetime.now()
            # msg.modified_date = datetime.now()
            msg.save()

            for file_name,content in request.FILES.items():
                if file_name.startswith("written_file") and len(file_name) > len("written_file"):
                    file = content
                    file_name = str(request.user.id)+"-"+str(uuid.uuid4())+"."+file._name[file._name.rindex(".")+1:]
                    with open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name), 'wb+') as destination:
                        for chunk in file.chunks():
                            destination.write(chunk)
                    file = File(open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name)))
                    msg_attachment = MessageAttachment()
                    msg_attachment.message = msg
                    msg_attachment.attachment = file
                    # msg_attachment.create_date = datetime.now()
                    # msg_attachment.modified_date = datetime.now()
                    msg_attachment.save()

            user_msg = UserMessage()
            user_msg.sender = request.user
            user_msg.receiver = User.objects.get(pk=int(tutor_id))
            user_msg.message = msg
            # user_msg.create_date = datetime.now()
            # user_msg.modified_date = datetime.now()
            user_msg.save()

            written_lesson = WrittenLesson()
            written_lesson.user = request.user
            written_lesson.duration = duration
            written_lesson.due_date = time.mktime(d_date.timetuple())
            written_lesson.request_user = User.objects.get(pk=int(tutor_id))
            written_lesson.course = Major.objects.get(pk=int(subject_id))
            written_lesson.message = user_msg
            # written_lesson.create_date = datetime.now()
            # written_lesson.modified_date = datetime.now()
            written_lesson.save()

        response["status"] = "SUCCESS"
        response["message"] = "Successful"
        response["data"] = []
        return self.render_to_json(response)

class WrittenLessonCommentView(JSONResponseMixin,View):
    def post(self,request,*args,**kwargs):
        response = {
            "status": "FAILURE",
            "message": "FAILURE",
            "data": []
        }
        lesson_id = request.POST.get("lesson_id")
        comment_text = request.POST.get("comment_text")
        attachment = request.FILES.get("attached_file")

        written_lessons = WrittenLesson.objects.filter(pk=int(lesson_id))
        if written_lessons:
            if comment_text:
                wr_comment = WrittenLessonComment()
                wr_comment.comment_text = comment_text
                wr_comment.lesson = written_lessons[0]
                # wr_comment.create_date = datetime.now()
                # wr_comment.modified_date = datetime.now()
                wr_comment.save()
            else:
                pass
            if attachment:
                file_name = str(lesson_id)+"."+attachment._name[attachment._name.rindex(".")+1:]
                print file_name
                with open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name), 'wb+') as destination:
                    for chunk in file.chunks():
                        destination.write(chunk)
                attached_file = File(open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name)))

                wr_comment_attachment = WrittenLessonCommentAttachment()
                wr_comment_attachment.attachment_name = attachment._name
                wr_comment_attachment.attachment = attached_file
                wr_comment_attachment.comment = wr_comment
                wr_comment_attachment.save()

        response["status"] = "SUCCESS"
        response["message"] = "Successful"
        response["data"] = []
        return self.render_to_json(response)

class LiveLessonAjaxView(JSONResponseMixin,View):
    def post(self,request,*args,**kwargs):
        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": []
        }

        tutor = request.POST.get("live_lesson_request_tutor")
        lesson_start_date = request.POST.get("live_lesson_request_start_date")
        lesson_start_time = request.POST.get("live_lesson_request_start_time")
        #lesson_request_timezone = request.POST.get("live_lesson_request_timezone")
        lesson_request_duration = request.POST.get("live_lesson_request_duration")
        lesson_repeat = request.POST.get("live_lesson_request_repeats")
        lesson_request_subject = request.POST.get("live_lesson_request_subject")
        lesson_request_note = request.POST.get("live_lesson_request_note")

        tutor = int(tutor)

        lesson_date = datetime.strptime(lesson_start_date,"%d/%m/%Y")
        lesson_time = datetime.strptime(lesson_start_time,"%I:%M %p")

        lesson_datetime = datetime.combine(lesson_date.date(),lesson_time.time())

        lesson_timestamp = Clock.convert_local_to_utc_timestamp(request.user.champuser.timezone,time.mktime(lesson_datetime.timetuple()))

        #start_datetime = datetime.strptime(lesson_start_date+" "+lesson_start_time,"%d/%m/%Y %H:%M %p")
        #timezone = int(lesson_request_timezone)
        duration = int(lesson_request_duration)
        subject = int(lesson_request_subject)

        live_lesson = LiveLesson()
        live_lesson.user = request.user
        #live_lesson.timezone = lesson_request_timezone
        live_lesson.request_user = User.objects.get(pk=tutor)
        live_lesson.lesson_time = lesson_timestamp #time.mktime(start_datetime.timetuple())
        live_lesson.duration = duration
        live_lesson.course = Major.objects.get(pk=subject)
        live_lesson.message = lesson_request_note
        live_lesson.repeat = lesson_repeat
        live_lesson.save()

        return self.render_to_json(response)

class LiveLessonRequestMutate(JSONResponseMixin,View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(LiveLessonRequestMutate, self).dispatch(*args, **kwargs)

    def get(self,request,*args,**kwargs):
        response = {
            "status": "FAILURE",
            "message": "FAILURE",
            "data": []
        }
        return self.render_to_json(response)

    def post(self,request,*args,**kwargs):
        response = {
            "status": "FAILURE",
            "message": "FAILURE",
            "data": []
        }
        action = request.POST.get("action")
        lesson = request.POST.get("lesson_id")
        if action == "ACCEPT":
            try:
                live_lesson = LiveLesson.objects.get(pk=int(lesson))

                whiteboard = Whiteboard()
                whiteboard.access = WhiteboardAccess.protected.value
                whiteboard.created_by = request.user
                whiteboard.name = uuid.uuid4()
                whiteboard.unique_id = uuid.uuid4()
                whiteboard.save()
                whiteboard.users.add(live_lesson.user)
                whiteboard.users.add(live_lesson.request_user)

                live_lesson.approved = ApprovalStatus.approved.value
                live_lesson.whiteboard = whiteboard
                live_lesson.save()

                # ###Create Pad Author and group.
                pad_util = PadUtil()
                lesson_requested_by_author_id = pad_util.create_or_get_padauthor(live_lesson.user.id,live_lesson.user.champuser.fullname)
                print "Pad Author Created. Author ID: "+lesson_requested_by_author_id

                lesson_requested_to_author_id = pad_util.create_or_get_padauthor(live_lesson.request_user.id,live_lesson.request_user.champuser.fullname)
                print "Pad Author Created. Author ID: "+lesson_requested_to_author_id

                pad_group_id = pad_util.create_or_get_padgroup(live_lesson.pk,user_id=request.user.pk)
                print "Pad Group Created. Pad Group ID: "+pad_group_id

                pad_id = pad_util.create_group_pad(pad_group_id,user_id=request.user.pk,author_id=lesson_requested_by_author_id)
                pad_object = Pad.objects.get(pad_id=pad_id)
                pad_author = PadAuthor.objects.filter(author_id=lesson_requested_to_author_id)
                if pad_author.exists():
                    pad_object.pad_authors.add(pad_author.first())
                whiteboard.text_pads.add(pad_object)

                codepad_util = CodePadUtil()
                code_pad_obj = codepad_util.create_codepad(str(live_lesson.pk),"Javascript",creator=request.user)

                whiteboard.code_pads.add(code_pad_obj)

                whiteboard_user1_permission = WhiteboardUserPermission()
                whiteboard_user1_permission.user = live_lesson.user
                whiteboard_user1_permission.save()

                whiteboard_user2_permission = WhiteboardUserPermission()
                whiteboard_user2_permission.user = live_lesson.request_user
                whiteboard_user2_permission.save()

                text_pad_permission = TextPadPermission()
                text_pad_permission.text_pad = pad_object
                text_pad_permission.permission = Permission.all.value
                text_pad_permission.save()

                text_pad_permission2 = TextPadPermission()
                text_pad_permission2.text_pad = pad_object
                text_pad_permission2.permission = Permission.all.value
                text_pad_permission2.save()

                code_pad_permission = CodePadPermission()
                code_pad_permission.code_pad = code_pad_obj
                code_pad_permission.permission = Permission.all.value
                code_pad_permission.save()

                code_pad_permission2 = CodePadPermission()
                code_pad_permission2.code_pad = code_pad_obj
                code_pad_permission2.permission = Permission.all.value
                code_pad_permission2.save()

                whiteboard_user1_permission.code_pad_permissions.add(code_pad_permission)
                whiteboard_user2_permission.code_pad_permissions.add(code_pad_permission)

                whiteboard_user1_permission.text_pad_permissions.add(text_pad_permission)
                whiteboard_user2_permission.text_pad_permissions.add(text_pad_permission2)

                drawingboard = DrawingBoard()
                drawingboard.name = "1"
                drawingboard.numeric_id = 1
                drawingboard.save()

                DrawingboardLastNid.objects.filter(group=str(live_lesson.pk)).delete()

                dblastnid = DrawingboardLastNid()
                dblastnid.group = str(live_lesson.pk)
                dblastnid.nid = 1
                dblastnid.save()

                whiteboard.drawingboards.add(drawingboard)


                drawingboard_permission = DrawingboardPermission()
                drawingboard_permission.drawingboard = drawingboard
                drawingboard_permission.permission = Permission.all.value
                drawingboard_permission.save()

                drawingboard_permission2 = DrawingboardPermission()
                drawingboard_permission2.drawingboard = drawingboard
                drawingboard_permission2.permission = Permission.all.value
                drawingboard_permission2.save()

                whiteboard_user1_permission.drawingboard_permissions.add(drawingboard_permission)
                whiteboard_user2_permission.drawingboard_permissions.add(drawingboard_permission2)

                whiteboard.permissions.add(whiteboard_user1_permission)
                whiteboard.permissions.add(whiteboard_user2_permission)

                calendar_event = CalendarEvent()
                calendar_event.title = "Live lesson"
                calendar_event.css_class = "event-success"
                # calendar_event.start_timestamp = live_lesson.lesson_time
                # calendar_event.end_timestamp = live_lesson.lesson_time + live_lesson.duration
                calendar_event.user_id = live_lesson.user.pk
                calendar_event.url = reverse("live_lesson",kwargs={"pk": live_lesson.pk})
                calendar_event.start = datetime.fromtimestamp(live_lesson.lesson_time)
                calendar_event.end = datetime.fromtimestamp(live_lesson.lesson_time + live_lesson.duration)
                calendar_event.save()

                calendar_event = CalendarEvent()
                calendar_event.title = "Live lesson"
                calendar_event.css_class = "event-success"
                # calendar_event.start = datetime.fromtimestamp(live_lesson.lesson_time)
                # calendar_event.end = datetime.fromtimestamp(live_lesson.lesson_time + live_lesson.duration)
                calendar_event.user_id = live_lesson.request_user.pk
                calendar_event.url = reverse("live_lesson",kwargs={"pk": live_lesson.pk})
                calendar_event.start = datetime.fromtimestamp(live_lesson.lesson_time)
                calendar_event.end = datetime.fromtimestamp(live_lesson.lesson_time + live_lesson.duration)
                calendar_event.save()

                response["status"] = "SUCCESS"
                response["message"] = "Successful"
                response["data"] = []
                return self.render_to_json(response)
            except Exception,msg:
                #print(str(msg))
                live_lesson.approved = ApprovalStatus.not_approved.value
                if whiteboard:
                    whiteboard.delete()
                live_lesson.whiteboard = None
                live_lesson.save()
                response["status"] = "FAILURE"
                response["message"] = "Failed to approve"
                response["data"] = []
                return self.render_to_json(response)

        elif action == "REJECT":
            live_lesson = LiveLesson.objects.get(pk=int(lesson))
            live_lesson.approved = ApprovalStatus.rejected.value
            live_lesson.save()
            response["status"] = "SUCCESS"
            response["message"] = "Successful"
            response["data"] = []
        elif action == "CANCEL":
            live_lesson = LiveLesson.objects.get(pk=int(lesson))
            live_lesson.approved = ApprovalStatus.cancelled.value
            live_lesson.save()
            response["status"] = "SUCCESS"
            response["message"] = "Successful"
            response["data"] = []
        else:
            response["message"] = "Invalid operation."
        return self.render_to_json(response)

class JobApplicationUpdateAjaxView(JSONResponseMixin,View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(JobApplicationUpdateAjaxView, self).dispatch(*args, **kwargs)

    def post(self,request,*args,**kwargs):
        action = request.POST.get("action")
        ja_id = request.POST.get("ja")
        if not JobApplication.objects.filter(pk=int(ja_id)).exists():
            pass
        else:
            if action == "cancel":
                ja = JobApplication.objects.get(pk=int(ja_id))
                ja.status = JobApplicationStatus.cancelled.value
                ja.save()
                response = {
                    "status": "SUCCESS",
                    "message": "Successful",
                    "data": []
                }
                return self.render_to_json(response)
            elif action == "4" or action == "3": ###Award
                ja = JobApplication.objects.get(pk=int(ja_id))
                if action == "4":
                    ja.status = JobApplicationStatus.awarded.value
                elif action == "3":
                    ja.status = JobApplicationStatus.rejected.value
                ja.save()
                response = {
                    "status": "SUCCESS",
                    "message": "Successful",
                    "data": []
                }
                return self.render_to_json(response)
        response = {
            "status": "FAILURE",
            "message": "FAILURE",
            "data": []
        }
        return self.render_to_json(response)

class OpenHireDialogAjaxView(LoginRequiredMixin, JSONResponseMixin, View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(OpenHireDialogAjaxView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        job_id = request.GET.get("job_id")
        tutor_id = request.GET.get('tutor_id')
        job = Job.objects.get(pk=int(job_id))
        template_name = "ajax/job_applicant_hire_dialog.html"
        template = loader.get_template(template_name)
        tutor = None
        job_application = None
        try:
            tutor_id = int(tutor_id)
            job_id = int(job_id)
            tutor = ChampUser.objects.get(user_id=tutor_id)
            job_application = JobApplication.objects.filter(job__id=job_id, tutor_id=tutor_id).first()
        except:
            pass

        country_name = ''
        tzm = TimezoneMapper.objects.filter(tz=request.user.champuser.timezone)
        if tzm.exists():
            tzm = tzm.first()
            country_name = tzm.country.name
        user_currency = CurrencyUtil.get_currency_code_for_country(country_name)

        conversion_enabled = job.preference.currency != user_currency
        base_currency = "USD"
        context = {
            "job": job,
            "tutor": tutor,
            "application": job_application,
            "currency": job_application.settlement.currency,
            "price": job_application.settlement.price,
            "price2": job_application.settlement.price2,
            "duration": job_application.duration,
            "start_time": job_application.start_time,
            "duration_list": TimeUtil.get_all_duration_list(),
            "currency_list": PaymentCurrencyUtil.get_all_currencies(),
            "conversion_enabled": conversion_enabled,
            "target_currency": user_currency
        }
        cntxt = Context(context)
        rendered = template.render(cntxt)
        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": rendered
        }
        return self.render_to_json(response)

    def validate_params(self, request):
        job_id = request.POST.get("job_id")
        tutor_id = request.POST.get("tutor_id")
        start_date_default = request.POST.get("start_date_default")
        start_date = request.POST.get("start_date")
        duration_default = request.POST.get("duration_default")
        duration = request.POST.get("duration")
        currency = request.POST.get("currency")
        rate_default = request.POST.get("rate_default")
        rate = request.POST.get("rate")
        message = request.POST.get("message")
        repeat = request.POST.get("apply_repeat")

        date_changed = not(start_date_default.strip() == start_date.strip())

        try:
            utc_dt = Clock.local_to_utc_datetime(start_date, '%m/%d/%Y %H:%M', request.user.champuser.timezone)
            start_date = time.mktime(utc_dt.timetuple())
        except:
            try:
                utc_dt = Clock.local_to_utc_datetime(start_date, '%m/%d/%Y %H:%M %p', request.user.champuser.timezone)
                start_date = time.mktime(utc_dt.timetuple())
            except:
                return None, None, None, None, None, None, None, None, None, None, None, None, None

        try:
            job_id = int(job_id)
            job = Job.objects.get(pk=job_id)
            tutor_id = int(tutor_id)
            tutor = User.objects.get(pk=tutor_id)
            duration_default = int(duration_default)
            duration = int(duration)
            rate_default = Decimal(rate_default)
            rate = Decimal(rate)
            repeat = int(repeat)
            return job_id, job, tutor_id, tutor, start_date, duration, currency, rate, message,repeat, date_changed, not(duration_default == duration), not(rate_default==rate)
        except Exception as msg:
            return None, None, None, None, None, None, None, None, None, None, None, None, None

    def post(self, request, *args, **kwargs):
        job_id, job, tutor_id, tutor, start_date, duration, currency, rate, hire_message, repeat, date_changed, duration_changed, rate_changed = self.validate_params(request)

        if any([ not job_id, not job, not start_date, not duration, not rate, not repeat ]):
            response = {
                "status": "FAILURE",
                "message": "Missing parameter",
                "data": ""
            }
            return self.render_to_json(response)

        if not request.user.is_authenticated():
            response = {
                "status": "FAILURE",
                "message": "Unauthorized access",
                "data": ""
            }
            return self.render_to_json(response)

        with transaction.atomic():
            applications = job.applications.filter(tutor_id=tutor_id)
            if applications.exists():

                job_invites = JobInvitation.objects.filter(job_id=job_id, user_id=tutor_id)
                if job_invites.exists():
                    job_invitation = job_invites.first()
                    job_invitation.status = 8 ###Hired.
                    job_invitation.save()

                job_application = applications.first()
                job_application.status = 8 ###Hired.

                if hire_message:
                    msg = Message()
                    msg.chat_type = ChatType.job_application.value
                    msg.is_read = ReadStatus.unread.value
                    msg.msg = hire_message
                    msg.save()

                    ja_message = JobApplicationMessage()
                    ja_message.sender_id = request.user.pk
                    ja_message.receiver_id = tutor_id
                    ja_message.message_id = msg.pk
                    ja_message.save()

                    job_application.hire_message_id = ja_message.pk

                price_settlement = job_application.settlement
                price_settlement.price = rate
                price_settlement.save()

                job_application.start_time = start_date
                job_application.duration = duration

                if date_changed:
                    job_application.time_edited_by = request.user
                if duration_changed:
                    job_application.duration_edited_by = request.user
                if rate_changed:
                    job_application.price_edited_by = request.user

                job_application.save()

                ###For live lesson create lesson and payment later and for written lesson payment immediately.
                ###Now create live lesson or written lesson
                job_type = job.lesson_type  ###0 for live lesson and 1 for written lesson.
                if job_type == 0: ###Live Lesson
                    lesson_util = LessonUtil()
                    repeat = "NO_REPEAT"
                    if job.lesson_repeat == 1:
                        repeat = "NO_REPEAT"
                    elif job.lesson_repeat == 2:
                        repeat = "REPEAT_DAILY"
                    elif job.lesson_repeat == 3:
                        repeat = "REPEAT_WEEKLY"
                    success, lesson_id = lesson_util.create_live_lesson(job, tutor_id, job_application, repeat)
                    job.lesson_id = lesson_id
                    job.save()
                else:
                    lesson_util = LessonUtil()
                    repeat = "NO_REPEAT"
                    if job.lesson_repeat == 1:
                        repeat = "NO_REPEAT"
                    elif job.lesson_repeat == 2:
                        repeat = "REPEAT_DAILY"
                    elif job.lesson_repeat == 3:
                        repeat = "REPEAT_WEEKLY"
                    success, lesson_id = lesson_util.create_written_lesson(job, tutor_id, job_application, repeat)
                    job.lesson_id = lesson_id
                    job.save()
                    ###Now schedule a payment.
                    # payment_processor = PaymentProcessor()
                    # transaction_amount = (float(job_application.duration) / 60.0) * job_application.settlement.price
                    # payment_processor.process_lesson_payment(lesson_id,'WRITTEN_LESSON', int(transaction_amount), job_application.settlement.currency)

                ###Send email and notification
                student_name = request.user.champuser.fullname
                tutor_id = job_application.tutor.pk
                tutor_name = job_application.tutor.champuser.fullname
                html, text = EmailTemplateUtil.render_job_application_hired_email_template(student_name, tutor_name, job_application.pk, hire_message)

                email_object = {
                    "recipients": [ job_application.tutor.email ],
                    'subject': 'You have been hired by %s' % ( student_name, ),
                    'html_body': html,
                    'text_body': text
                }

                EmailScheduler.place_to_queue(email_object)

                NotificationManager.push_notification("job_hired", job.pk, tutor_id, job_application.tutor.pk)

                response = {
                    "status": "SUCCESS",
                    "message": "Successful",
                    "data": ""
                }
                return self.render_to_json(response)

            else:
                response = {
                    "status": "FAILURE",
                    "message": "Invalid access",
                    "data": ""
                }
                return self.render_to_json(response)
        response = {
            "status": "FAILURE",
            "message": "Failed to hire",
            "data": ""
        }
        return self.render_to_json(response)

class InviteTutorAjaxView(JSONResponseMixin, View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(InviteTutorAjaxView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        tutor_id = request.GET.get("tutor_id")
        job_id = request.GET.get("job_id")
        dialog_template_name = "ajax/job_invite_tutor_dialog.html"
        dialog_template = loader.get_template(dialog_template_name)
        context = {}

        if not request.user.is_authenticated():
            response = {
                "status": "FAILURE",
                "code": 4001,
                "message": "Successful"
            }
            return self.render_to_json(response)

        context["logged_in"] = True

        if job_id:
            job = Job.objects.filter(pk=int(job_id), posted_by_id=request.user.pk,status=JobStatus.job_open.value, end_date__gt=Clock.utc_timestamp()).exclude(status=4) #.exclude(applications__status=8)
            if not job.exists():
                context["margin_top"] = True
                context["job_not_opened"] = True
                context["job_id_given"] = True
                cntxt = Context(context)
                rendered = dialog_template.render(cntxt)
                response = {
                    "status": "SUCCESS",
                    "message": "Successful",
                    "data": { "dialog_content": rendered }
                }
                return self.render_to_json(response)

            job_id = int(job_id)
            job = Job.objects.get(pk=job_id)
            template_name = "ajax/job_summary_details.html"
            template = loader.get_template(template_name)
            ###Check if student has any open jobs.
            context['job'] = job

            cntxt = Context(context)
            rendered = template.render(cntxt)
            context["job_summary"] = rendered
            context["job_id_given"] = True
            context["logged_in"] = True

            champ_user = None

            try:
                tutor_id = int(tutor_id)
                champ_user = ChampUser.objects.get(user_id=tutor_id)
            except Exception as exp:
                pass

            context["tutor"] = champ_user

            if champ_user:
                subjects = ','.join([ s.name for s in job.preference.subjects.all() ])
                message = EmailTemplateUtil.render_job_invitation_message_template(champ_user.fullname, subjects)
                context["email_template"] = message

            cntxt = Context(context)
            rendered = dialog_template.render(cntxt)
            response = {
                "status": "SUCCESS",
                "message": "Successful",
                "data": { "dialog_content": rendered }
            }
            return self.render_to_json(response)

        ###Check if student has any open jobs.
        open_jobs = Job.objects.filter(posted_by_id=request.user.pk,status=JobStatus.job_open.value, end_date__gt=Clock.utc_timestamp()).exclude(status=4).exclude(applications__status=8)

        context['open_jobs'] = open_jobs

        if open_jobs.exists():
            if tutor_id:
                tutor_id = int(tutor_id)
                champ_user = ChampUser.objects.get(user_id=tutor_id)
                context["tutor"] = champ_user
                context["email_template"] = None #EmailTemplateUtil.render_job_invitation_message_template(champ_user.fullname, "")

            # if job_id:
            #     job_id = int(job_id)
            #     job = Job.objects.get(pk=job_id)
            #     template_name = "ajax/job_summary_details.html"
            #     template = loader.get_template(template_name)
            #     ###Check if student has any open jobs.
            #     context['job'] = job
            #
            #     cntxt = Context(context)
            #     rendered = template.render(cntxt)
            #     context["job_summary"] = rendered

        cntxt = Context(context)
        rendered = dialog_template.render(cntxt)
        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": { "dialog_content": rendered }
        }
        return self.render_to_json(response)

class GetEmailTemplateAjax(LoginRequiredMixin, JSONResponseMixin, View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(GetEmailTemplateAjax, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        context = request.GET.get("context")
        if context == "JOB_INVITATION":
            receiver = request.GET.get('rid')
            job_id = request.GET.get('job_id')
            job = Job.objects.get(pk=int(job_id))
            subjects = ','.join([ s.name for s in job.preference.subjects.all() ])
            receiver = int(receiver)
            champ_user = ChampUser.objects.get(user_id=receiver)
            template_data = EmailTemplateUtil.render_job_invitation_message_template(champ_user.fullname, subjects)
            response = {
                "status": "SUCCESS",
                "message": "Successful",
                "data": { "email_template": template_data }
            }
            return self.render_to_json(response)
        elif context == "JOB_OFFER":
            receiver = request.GET.get('rid')
            receiver = int(receiver)
            champ_user = ChampUser.objects.get(user_id=receiver)
            template_data = EmailTemplateUtil.render_job_offer_message_template(champ_user.fullname)
            response = {
                "status": "SUCCESS",
                "message": "Successful",
                "data": { "email_template": template_data }
            }
            return self.render_to_json(response)

class NegotiateJobAjaxView(LoginRequiredMixin, JSONResponseMixin, View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(NegotiateJobAjaxView, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        job_id = request.POST.get("job_id")
        tutor_id = request.POST.get("tutor_id")
        update_to_negotiate = request.POST.get("update_to_negotiate")
        if not update_to_negotiate or update_to_negotiate == "1":
            job_id = int(job_id)
            try:
                tutor_id = int(tutor_id)
            except:
                tutor_id = request.user.pk
            ji_objects = JobInvitation.objects.filter(job_id=job_id,user_id=tutor_id)
            jr_objects = JobRecommendation.objects.filter(job_id=job_id,user_id=tutor_id)
            if ji_objects.exists():
                ji_object = ji_objects.first()
                ji_object.status = JobInvitationStatus.negotiating.value
                ji_object.save()

            elif jr_objects.exists():
                jr_object = jr_objects.first()
                jr_object.status = JobRecommendationStatus.negotiating.value
                jr_object.save()

            job = Job.objects.get(pk=job_id)
            applications = job.applications.filter(tutor_id=tutor_id)
            if applications.exists():
                application = applications.first()
                application.status = JobApplicationStatus.negotiating.value
                application.save()

        champ_user = ChampUser.objects.get(user_id=tutor_id)

        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": {
                "user_info": {
                    "id": tutor_id,
                    "name": champ_user.fullname,
                    "profile_picture": champ_user.render_profile_picture(),
                    "online_status": champ_user.render_online_status_verbal
                }
            }
        }
        return self.render_to_json(response)

class ShortlistJobAjaxView(LoginRequiredMixin, JSONResponseMixin, View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ShortlistJobAjaxView, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        job_id = request.POST.get("job_id")
        tutor_id = request.POST.get("tutor_id")
        job_id = int(job_id)
        try:
            tutor_id = int(tutor_id)
        except:
            tutor_id = request.user.pk
        ji_objects = JobInvitation.objects.filter(job_id=job_id,user_id=tutor_id)
        jr_objects = JobRecommendation.objects.filter(job_id=job_id,user_id=tutor_id)
        if ji_objects.exists():
            ji_object = ji_objects.first()
            ji_object.status = JobInvitationStatus.shortlisted.value
            ji_object.save()

        elif jr_objects.exists():
            jr_object = jr_objects.first()
            jr_object.status = JobRecommendationStatus.shortlisted.value
            jr_object.save()

        job = Job.objects.get(pk=job_id)
        applications = job.applications.filter(tutor_id=tutor_id)
        if applications.exists():
            application = applications.first()
            application.status = JobApplicationStatus.shortlisted.value
            application.save()

        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": ""
        }
        return self.render_to_json(response)
