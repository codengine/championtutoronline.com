from django.contrib.auth.models import User
from django.db import models
from datetime import datetime
from image_cropping import ImageRatioField
from image_cropping.fields import ImageCropField
from core.enums import UserTypes,VerificationStatus,ChatType,ReadStatus,OnlineStatus,TransactionStatus
from core.generics.models import GenericModel
from payment.models import *

class Address(GenericModel):
    street_address1 = models.TextField(blank=True, null=True, max_length=200)
    street_address2 = models.TextField(blank=True, null=True, max_length=200)
    city = models.TextField(blank=True, null=True, max_length=40)
    state = models.TextField(blank=True, null=True, max_length=40)
    zip = models.TextField(blank=True, null=True, max_length=40)
    country = models.TextField(blank=True, null=True, max_length=40)

    class Meta:
        db_table = 'address'


class ProfilePicture(GenericModel):
    image_field = ImageCropField(upload_to='image/')
    cropping = ImageRatioField('image_field', size='120x100', allow_fullsize=True)

    class Meta:
        db_table = 'profile_picture'

class Role(GenericModel):
    type_choices = (
        (UserTypes.student,UserTypes.student),
        (UserTypes.teacher,UserTypes.teacher)
    )
    name=models.CharField(blank=False,null=False,max_length=10,choices=type_choices)

    class Meta:
        db_table='role'

class Certifications(GenericModel):
    name = models.CharField(max_length=255)
    added_by = models.ForeignKey(User)

    class Meta:
        db_table =u'certifications'

class Schools(GenericModel):
    name = models.CharField(max_length=255)
    added_by = models.ForeignKey(User)

    class Meta:
        db_table = u'schools'

class ChampUser(GenericModel):

    type_choices = (
        (UserTypes.student,UserTypes.student),
        (UserTypes.teacher,UserTypes.teacher)
    )

    user = models.OneToOneField(User)
    fullname = models.TextField(blank=False, null=False, max_length=100)
    phone = models.TextField(blank=False, null=False, max_length=20)
    address = models.ForeignKey(Address, null=True, default=None)
    profile_picture = models.ForeignKey(ProfilePicture, null=True, default=None)
    type = models.ForeignKey(Role)
    nationality = models.CharField(max_length=255)
    current_school = models.CharField(max_length=255)
    session = models.CharField(max_length=255)
    major = models.CharField(max_length=255)
    class Meta:
        db_table='champ_user'

class EducationVerification(GenericModel):
    scanned_certificate = models.ImageField(upload_to='image/')
    status = models.IntegerField(default=VerificationStatus.not_verified) ###0 means not verified and 1 means verified.
    verified_by = models.BigIntegerField()
    submit_date = models.DateTimeField(auto_now_add=True,blank=True,null=True)
    verification_date = models.DateTimeField(auto_now_add=True,blank=True,null=True)

    class Meta:
        db_table=u'education_verification'

class NRICVerification(GenericModel):
    name = models.CharField(max_length=500, blank=True, default='', null=True)
    nid_number = models.CharField(max_length=50, blank=True, default='', null=True)
    file_name = models.CharField(max_length=500, blank=True, default='', null=True)
    nid_scanned_img = models.ImageField(upload_to='image/',null=True)
    status = models.IntegerField(default=0) ###0 means not verified ,1 means verified,2 means under review,3 means rejected..
    verified_by = models.BigIntegerField()
    submit_date = models.DateTimeField(auto_now_add=True,blank=True,null=True)
    verification_date = models.DateTimeField(auto_now_add=True,blank=True,null=True)

    class Meta:
        db_table=u'nric_verification'

class Education(GenericModel):
    degree = models.ForeignKey(Certifications)
    institution = models.ForeignKey(Schools)
    session = models.CharField(max_length=255, null=True, verbose_name='class')
    cgpa = models.CharField(max_length=255, default='0.0')
    is_current = models.BooleanField(default=False)
    verification_status = models.ForeignKey(EducationVerification) #0 means not verified and 1 means verified.

    class Meta:
        db_table = u'education'

class Category(GenericModel):
    name = models.CharField(max_length=255)
    description = models.TextField()
    added_by = models.ForeignKey(User)

    class Meta:
        db_table = u'category'

class Courses(GenericModel):
    name = models.CharField(max_length=255)
    level = models.CharField(max_length=255)
    description = models.TextField()
    category = models.ForeignKey(Category)

    class Meta:
        db_table = u'course'

class Rate(GenericModel):
    class_level = models.CharField(max_length=255)
    rate = models.FloatField(default=0)
    rate_currency = models.CharField(max_length=10)

    class Meta:
        db_table=u'profile_rate'

class Currency(GenericModel):
    name = models.CharField(max_length=255)
    symbol = models.CharField(max_length=7)

    class Meta:
        db_table=u'currency'

class ProfileAbout(GenericModel):
    about = models.TextField()

    class Meta:
        db_table='profile_about'

class TimezoneSettings(GenericModel):
    # user_id = models.ForeignKey(ChampUser)
    # timezone = models.CharField(max_length=6) ##This field will contain timezone information in +=360 format. Say timezone is UTC+6 then it will store -360
    # last_updated = models.DateField(auto_now_add=True)
    pass

class OnlineStatus(GenericModel):
    status = models.IntegerField(default=OnlineStatus.offline) ## 1 for online and 0 for offline

    class Meta:
        db_table='online_status'


class Messages(GenericModel):
    id = models.AutoField(primary_key=True)
    msg_date = models.DateTimeField(auto_now=True,null=True,blank=True)
    read_date = models.DateTimeField(null=True,blank=True)
    msg = models.TextField()
    is_read = models.IntegerField(default=ReadStatus.unread)  ###0 for unread and 1 for read. Default is unread.
    chat_type = models.IntegerField(default=ChatType.p2p) ###0 for p2p chat and 1 for group chat. Default is p2p chat.

    class Meta:
        db_table='champ_chat_messages'

class OTSessionTable(GenericModel):
    sessionid = models.CharField(blank=False,null=False,max_length=40)
    otsessionId = models.CharField(blank=False,null=False,max_length=60)
    ottoken = models.CharField(blank=True,null=True,max_length=400)

    class Meta:
        db_table='ot_session'

class Whiteboard(GenericModel):
    name = models.CharField(max_length=255)
    active = models.IntegerField() #1 means active and 0 means inactive.

    class Meta:
        db_table=u'whiteboard'

class DrawingBoard(GenericModel):
    whiteboard = models.ManyToManyField(Whiteboard)
    numeric_id = models.IntegerField()
    name = models.CharField(max_length=255)

    class Meta:
        db_table=u'drawing_board'

class Major(GenericModel):
    name = models.CharField(max_length=512)
    description = models.TextField()
    creator = models.ForeignKey(User)

    class Meta:
        db_table=u'major'

class TeachingExperiences(GenericModel):
    experience = models.TextField()

    class Meta:
        db_table=u'teaching_experiences'

class ExtraCurricularInterest(GenericModel):
    interest = models.TextField()

    class Meta:
        db_table = u'extra_curricular_interest'

class Currency(GenericModel):
    name = models.CharField(max_length=255)
    symbol = models.CharField(max_length=7)

    class Meta:
        db_table=u'currency'

class Country(GenericModel):
    name = models.CharField(max_length=255)
    code = models.CharField(max_length=13)

    class Meta:
        db_table=u'country'

class ProfileMyVideo(GenericModel):
    url = models.CharField(max_length=500,null=True,blank=True)

    class Meta:
        db_table=u'profile_my_video'

class TeachingSessionMembers(GenericModel):
    member = models.ForeignKey(User)
    creator = models.BooleanField(default=False)

    class Meta:
        db_table='teaching_session_members'

class TeachingSession(GenericModel):
    members = models.ForeignKey(TeachingSessionMembers)
    end_date = models.DateTimeField(null=True,blank=True)
    duration = models.IntegerField(default=0)

    class Meta:
        db_table='teaching_session'

class Reviews(GenericModel):
    teaching_session = models.ForeignKey(TeachingSession)
    review_by = models.ForeignKey(User)
    rating = models.IntegerField(default=0)
    comment = models.TextField()

    class Meta:
        db_table='reviews'

class AcademicCertificates(GenericModel):
    cert_type = models.CharField(max_length=255)
    cert_name = models.CharField(max_length=255)
    cert_content = models.ImageField(upload_to="images/")

    class Meta:
        db_table = 'academic_certificates'

class Profile(GenericModel):
    user = models.ForeignKey(User)
    champ_user = models.ForeignKey(ChampUser)
    hourly_rate = models.FloatField(default=float(0.0))
    title = models.CharField(max_length=255, null=True)
    description = models.TextField(null=True)
    nationality = models.CharField(max_length=255, null=True)
    rating = models.FloatField(default=float(0.0))
    nric_verification_status = models.ForeignKey(NRICVerification,null=True)
    education = models.ManyToManyField(Education,null=True)
    major_subject = models.ManyToManyField(Major,null=True)
    teaching_exp = models.ForeignKey(TeachingExperiences,null=True)
    extra_curricular_interests = models.ForeignKey(ExtraCurricularInterest,null=True)
    schools = models.ManyToManyField(Schools,null=True)
    top_subjects = models.ManyToManyField(Courses,null=True)
    about = models.ForeignKey(ProfileAbout,null=True)
    rate = models.ForeignKey(Rate,null=True)
    currency = models.ForeignKey(Currency,null=True)
    timezone = models.ForeignKey(TimezoneSettings,null=True)
    messages = models.ManyToManyField(Messages,null=True)
    online_status = models.ForeignKey(OnlineStatus,null=True)
    ot_session = models.ForeignKey(OTSessionTable,null=True)
    whiteboard = models.ForeignKey(Whiteboard,null=True)
    my_video = models.ForeignKey(ProfileMyVideo,null=True)
    teaching_sessions = models.ManyToManyField(TeachingSession,null=True)
    academic_certificates = models.ForeignKey(AcademicCertificates,null=True)

    class Meta:
        db_table=u'champ_profile'

class ResetPasswordToken(GenericModel):
    user = models.ForeignKey(User)
    token = models.CharField(max_length=255)