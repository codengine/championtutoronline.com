from decimal import Decimal
import uuid
from django.db import transaction
from django.http.response import HttpResponse, HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.detail import DetailView
from common.methods import get_buddies_online
from core.generics.ajaxmixins import JSONResponseMixin
from core.generics.mixins import LoginRequiredMixin
from core.library.Clock import Clock
from core.library.currency_util import CurrencyUtil
from core.library.email_scheduler import EmailScheduler
from core.library.email_template_util import EmailTemplateUtil
from core.library.job_util import JobUtil
from core.library.lesson_util import LessonUtil
from core.library.rate_conversion_util import RateConversionUtil
from engine.config.API import SUPPORTED_CURRENCIES, BASE_CURRENCIES
from engine.libutil.TimeUtil import TimeUtil
from engine.libutil.paymentutil import PaymentCurrencyUtil
from notification.utils.notification_manager import NotificationManager

__author__ = 'codengine'

from django.template import loader, Context
from django.shortcuts import render, redirect
from core.models3 import ChampUser,Role, Message, WrittenLesson, Major, WrittenLessonCommentAttachment, WrittenLessonComment, LessonAttachment, LessonAnswer, WrittenLessonAttachment, LessonAnswerAttachment, LiveLesson, ProgressReport, ErrorLog, Job, \
    Country, JobPreference, HourlyRateUnit, CharUnit, Language, Question
from django.db.models import Q
from core.enums import UserTypes, ReadStatus, ChatType, LessonStatus, ApprovalStatus, LessonTypes, JobStatus, \
    AvailableStatus, JobRecommendationStatus, JobOfferStatus
from datetime import datetime
from django.views.generic import View
from core.models3 import UserMessage
from django.contrib.auth.models import User
import os
from core.models3 import MessageAttachment
from django.core.files import File
import time
from django.views.generic.list import ListView
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from core.models3 import *

class WrittenLessonRequestView(LoginRequiredMixin,View):
    def get(self,request,*args,**kwargs):
        context = {}
        if request.GET.get("lesson"):
            written_lesson = WrittenLesson.objects.get(pk=int(request.GET["lesson"]))
            context["written_lesson"] = written_lesson
            return render(request,"written_lesson_request.html",context)
        # _this_user_id = request.user.id
        # user_objs = ChampUser.objects.filter(user__id=_this_user_id)
        # if user_objs:
        #     user_obj = user_objs[0]
        #     #data["utype"] = user_obj.type
        #
        #     query = None
        #
        #     if user_obj.type.name == UserTypes.student.value:
        #         #query to get teachers with recent chat and also teachers who initiated a chat with this student.
        #         context["buddies"] = ChampUser.objects.filter(~Q(user=request.user),type=Role.objects.get(name=UserTypes.teacher.value))
        #         #query = "select * from champ_user where type_id=1 and user_id != %s" % _this_user_id
        #     else:
        #         #query to get students with recent chat and also students who initiated a chat with this teacher.
        #         #query = "select * from champ_user where type_id=2 and user_id != %s" % _this_user_id
        #         context["buddies"] = ChampUser.objects.filter(~Q(user=request.user),type=Role.objects.get(name=UserTypes.student.value))
        context["champ_user"] = ChampUser.objects.get(user=request.user)
        #context["tutors"] = ChampUser.objects.filter(~Q(user=request.user),type=Role.objects.get(name=UserTypes.teacher.value))
        # lesson_time_choices = []
        #
        # format_n = lambda n: "0"+str(n) if n >= 0 and n <=9 else str(n)
        #
        # for i in range(24):
        #     for j in range(0,60,5):
        #         lesson_time_choices += [(i*60 + j,format_n(i)+":"+format_n(j))]

        CHOICES=[

        ]
        for i in range(15,181,5):
            time_str = str(i)+" min"
            if i >= 60:
                if i % 60 > 0:
                    time_str = str(i/60)+" hr "+str(i%60)+" min"
                else:
                    time_str = str(i/60)+" hour "
            CHOICES += [(i,time_str)]

        context["lesson_time_choices"] = CHOICES

        #context["buddies"] = get_buddies_online(request)

        return render(request,"written_lesson_request.html",context)
    def post(self,request,*args,**kwargs):

        action = request.POST.get("action")
        if action and action == "written-lesson-comment":
            lesson_id = request.POST.get("lesson_id")
            comment_text = request.POST.get("comment_text")
            attachment = request.FILES.get("attached_file")
    
            written_lessons = WrittenLesson.objects.filter(pk=int(lesson_id))
            if written_lessons.exists():
                wr_comment = None
                if comment_text:
                    wr_comment = WrittenLessonComment()
                    wr_comment.created_by = request.user
                    wr_comment.comment_text = comment_text
                    wr_comment.lesson_id = written_lessons.first().pk
                    # wr_comment.create_date = datetime.now()
                    # wr_comment.modified_date = datetime.now()
                    wr_comment.save()
                else:
                    pass
                if attachment:
                    file_name = str(lesson_id)+"-"+str(uuid.uuid4())+"."+attachment._name[attachment._name.rindex(".")+1:]
                    print file_name
                    with open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name), 'wb+') as destination:
                        for chunk in attachment.chunks():
                            destination.write(chunk)
                    attached_file = File(open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name)))
    
                    wr_comment_attachment = WrittenLessonCommentAttachment()
                    wr_comment_attachment.attachment_name = attachment._name
                    wr_comment_attachment.attachment = attached_file
                    if wr_comment:
                        wr_comment_attachment.comment = wr_comment
                    else:
                        wr_comment = WrittenLessonComment()
                        wr_comment.created_by = request.user
                        wr_comment.lesson_id = written_lessons.first().pk
                        wr_comment.save()
                        wr_comment_attachment.comment = wr_comment
                    wr_comment_attachment.save()

            return redirect("/written-lesson/?lesson="+str(lesson_id))

        if action and action == "written-lesson-answer":
            lesson_id = request.POST.get("lesson_id")
            answer_text = request.POST.get("written_lesson_text")

            written_lesson = WrittenLesson.objects.get(pk=int(lesson_id))

            if answer_text:
                lesson_answer = LessonAnswer()
                lesson_answer.user = request.user
                lesson_answer.response_user = written_lesson.user
                lesson_answer.text = answer_text
                # lesson_answer.create_date = datetime.now()
                # lesson_answer.modified_date = datetime.now()
                lesson_answer.save()

            for file_name,content in request.FILES.items():
                if file_name.startswith("written_file") and len(file_name) > len("written_file"):
                    file = content
                    original_file_name = file._name
                    file_name = str(request.user.id)+"-"+str(uuid.uuid4())+"."+file._name[file._name.rindex(".")+1:]
                    with open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name), 'wb+') as destination:
                        for chunk in file.chunks():
                            destination.write(chunk)
                    file = File(open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name)))
                    lesson_answer_attachment = LessonAnswerAttachment()
                    lesson_answer_attachment.lesson_answer = lesson_answer
                    lesson_answer_attachment.attachment_name = original_file_name
                    lesson_answer_attachment.attachment = file
                    lesson_answer_attachment.save()
            written_lesson.answer = lesson_answer
            written_lesson.status = LessonStatus.completed.value
            written_lesson.save()


            ###Now creeate a sale transaction.


            return redirect("/written-lesson/?lesson="+str(lesson_id))


        tutor_id = request.POST.get("select_tutor")
        subject_id = request.POST.get("select_subject")
        msg_text = request.POST.get("written_lesson_text")
        due_date = request.POST.get("due-date")
        due_time = request.POST.get("due-time")
        duration = request.POST.get('duration')

        if not tutor_id or not subject_id or not msg_text:
            return redirect("/written-lesson/")

        try:
            d_date = datetime.strptime(due_date,"%d/%m/%Y")
            d_time = datetime.strptime(due_time,"%H:%M %p")
            lesson_datetime = datetime.combine(d_date.date(),d_time.time())
            lesson_timestamp = Clock.convert_local_to_utc_timestamp(request.user.champuser.timezone,time.mktime(lesson_datetime.timetuple()))
            duration = int(duration)
        except Exception,msg:
            #print(msg)
            return redirect("/written-lesson/")

        if True:

            written_lesson = WrittenLesson()
            written_lesson.user = request.user
            written_lesson.duration = duration
            written_lesson.due_date = lesson_timestamp #time.mktime(d_date.timetuple())
            written_lesson.request_user = User.objects.get(pk=int(tutor_id))
            written_lesson.course = Major.objects.get(pk=int(subject_id))
            written_lesson.message = msg_text
            # written_lesson.create_date = datetime.now()
            # written_lesson.modified_date = datetime.now()
            written_lesson.save()
            for file_name,content in request.FILES.items():
                if file_name.startswith("written_file") and len(file_name) > len("written_file"):
                    file = content
                    original_file_name = file._name
                    file_name = str(request.user.id)+"-"+str(uuid.uuid4())+"."+file._name[file._name.rindex(".")+1:]
                    with open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name), 'wb+') as destination:
                        for chunk in file.chunks():
                            destination.write(chunk)
                    file = File(open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name)))
                    lesson_attachment = WrittenLessonAttachment()
                    # lesson_attachment.lesson = written_lesson
                    lesson_attachment.attachment_name = original_file_name
                    lesson_attachment.attachment = file
                    lesson_attachment.save()
                    written_lesson.attachments.add(lesson_attachment)

        return redirect("/written-lesson/?lesson="+str(written_lesson.pk))

class LessonsView(LoginRequiredMixin,View):
    def get(self,request,*args,**kwargs):
        context = {}
        # context["unconfirmed_lessons"] = LessonUtil.get_unconfirmed_lessons(request.user)
        context["coming_lessons"] = LessonUtil.get_coming_lessons(request.user)
        context["past_lessons"] = LessonUtil.get_past_lessons(request.user)
        # context["missed_lessons"] = LessonUtil.get_missed_lessons(request.user)
        context["include_jquery_1_10"] = True
        return render(request,"lessons.html",context)

class StudentProgressReportView(LoginRequiredMixin,JSONResponseMixin,View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(StudentProgressReportView, self).dispatch(*args, **kwargs)

    def get(self,request,*args,**kwargs):

        if not request.user.champuser.is_tutor:
            return HttpResponseRedirect(reverse("access_violation"))

        context = {}

        student_ids = LessonRequest.objects.filter(request_user_id=request.user.pk).values_list('user_id', flat=True)
        context["students"] = ChampUser.objects.filter(user_id__in=student_ids)

        return render(request,"student_progress_report.html",context)

    def post(self,request,*args,**kwargs):

        if not request.user.champuser.is_tutor:
            return HttpResponseRedirect(reverse("access_violation"))

        user_id = request.POST.get("user-id")
        content = request.POST.get("report-content")
        sender = request.user
        receiver = User.objects.get(pk=int(user_id))
        progress_report = ProgressReport()
        progress_report.sender = sender
        progress_report.receiver = receiver
        progress_report.sender_email = sender.email
        progress_report.receiver_email = receiver.email
        progress_report.text = content
        progress_report.save()

        try:
            html, text = EmailTemplateUtil.render_progress_report_email(sender.champuser.fullname, receiver.champuser.fullname, progress_report.date_created, content, request=request)
            email_object = {
                "recipients": [ receiver.email ],
                'subject': "Progress report for %s" % receiver.champuser.fullname,
                'html_body': html,
                'text_body': text
            }

            EmailScheduler.place_to_queue(email_object)
        except Exception as exp:
            error_log = ErrorLog()
            error_log.url = ''
            error_log.stacktrace = "Progress report save failed. Sender: %s, Receiver: %s, Content: %s" % ( sender.email, receiver.email,content )
            error_log.save()


        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": {
                "redirect_url": reverse("user_profile", kwargs={ "pk": request.user.pk })+"?tab=progress-reports"
            }
        }
        return self.render_to_json(response)

class ProgressReportListView(ListView):
    template_name = "progress_report_list.html"
    model = ProgressReport
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(ProgressReportListView, self).get_context_data(**kwargs)
        return context

class ProgressReportDetailsView(DetailView):
    template_name = "progress_report_details.html"
    model = ProgressReport

    def get_context_data(self, **kwargs):
        context = super(ProgressReportDetailsView, self).get_context_data(**kwargs)
        return context

class CurrencyRateListView(ListView):
    template_name = "currency-conversion-list.html"
    model = CurrencyRate

class CurrencyRateDetailsView(DetailView):
    template_name = "currency_rate_details.html"
    model = CurrencyRate

class CreatePostView(LoginRequiredMixin,View):
    def post(self,request,*args,**kwargs):
        lesson_post_type = request.POST.get("job_post_lesson_type")
        title = request.POST.get("title")
        description = request.POST.get("description")
        nationality_list = request.POST.getlist("nationality-list[]")
        gender = request.POST.get("gender")
        rate = request.POST.get("rate")
        currency_list = request.POST.getlist("currency-list[]")
        timezone_list = request.POST.getlist("timezone-list[]")
        language_list = request.POST.getlist("languages-list[]")
        subject_list = request.POST.getlist("subject-list[]")
        availability = request.POST.get("availability")
        end_date_month = request.POST.get("end_date_month")
        end_date_day = request.POST.get("end_date_day")
        end_date_year = request.POST.get("end_date_year")

        job = Job()
        if lesson_post_type == "online":
            job.lesson_type = LessonTypes.live_lesson.value
        else:
            job.lesson_type = LessonTypes.written.value

        if not title:
            return

        if not description:
            return

        job.title = title
        job.description = description
        job.posted_by = request.user
        job.status = JobStatus.job_open.value

        end_date = datetime.strptime(end_date_month+'-'+end_date_day+'-'+end_date_year,'%m-%d-%Y')
        utc_end_date = Clock.convert_local_to_utc(request.user.champuser.timezone,end_date)
        utc_end_timestamp = Clock.convert_datetime_to_timestamp(utc_end_date)
        job.end_date = utc_end_timestamp

        job.save()

        ###Now add job preference.
        job_preference = JobPreference()
        if availability == "ready_to_teach":
            availability = AvailableStatus.ready_to_teach.value
        elif availability == "available":
            availability = AvailableStatus.available.value
        elif availability == "any":
            availability = AvailableStatus.any.value
        else:
            raise Exception("Invalid available status.")

        job_preference.availability = availability
        job_preference.save()

        nationality_list = [int(nid) for nid in nationality_list]
        country_objects = Country.objects.filter(pk__in=nationality_list)
        job_preference.nationalities.add(*country_objects)

        hourly_rate_unit = HourlyRateUnit()
        hourly_rate_unit.currency = "USD"
        hourly_rate_unit.min_value = int(rate.strip().split('-')[0])
        hourly_rate_unit.max_value = int(rate.strip().split('-')[1])
        hourly_rate_unit.save()

        job_preference.rate = hourly_rate_unit

        job_preference.gender = gender.strip()
        
        for tz in timezone_list:
            char_unit = CharUnit()
            char_unit.value = tz
            char_unit.save()
            job_preference.timezones.add(char_unit)

        for currency in currency_list:
            char_unit = CharUnit()
            char_unit.value = currency
            char_unit.save()
            job_preference.currencies.add(char_unit)

        for lang in language_list:
            char_unit = CharUnit()
            char_unit.value = Language.objects.get(pk=int(lang)).name
            char_unit.save()
            job_preference.languages.add(char_unit)

        for subject_id in subject_list:
            char_unit = CharUnit()
            char_unit.value = Major.objects.get(pk=int(subject_id)).name
            char_unit.save()
            job_preference.subjects.add(char_unit)

        job_preference.save()

        job.preference = job_preference


        ###Now add job criteria



        job.save()

        url = reverse('user_profile', kwargs={'pk': request.user.pk})
        return HttpResponseRedirect(url+"?view-mode=jobs")

class CreateQPostView(LoginRequiredMixin,View):
    def post(self,request,*args,**kwargs):

        url = reverse('user_profile', kwargs={'pk': request.user.pk})

        q_title = request.POST.get("q_title")
        q_description = request.POST.get("q_description")
        q_tags = request.POST.getlist("qtag-list[]")

        if not q_title:
            messages.error(request, 'Question title missing')
            HttpResponseRedirect(url+"?view-mode=q_post")

        if not q_description:
            messages.error(request, 'Question description missing')
            HttpResponseRedirect(url+"?view-mode=q_post")

        question = Question()
        question.created_by = request.user
        question.title = q_title
        question.description = q_description
        question.save()

        for q_tag in q_tags:
            major_object = Major.objects.filter(pk=int(q_tag))
            if major_object.exists():
                char_unit = CharUnit()
                char_unit.value = major_object.first().name
                char_unit.save()
                question.tags.add(char_unit)
        messages.success(request, 'Question posted successfully')
        return HttpResponseRedirect(url+"?view-mode=qs")

class LessonFeedbackView(LoginRequiredMixin, JSONResponseMixin, View):
    def get(self,request,*args,**kwargs):
        lesson_id = kwargs.get("pk")
        context = {
            "lesson_id": lesson_id
        }

        lesson = LessonRequest.objects.get(pk=lesson_id)
        if lesson.review:
            positive_count = lesson.review.positive_feedback_count
            negative_count = lesson.review.negative_feedback_count
            context["positive_count"] = positive_count
            context["negative_count"] = negative_count
            context["feed_taken_by_me"] = lesson.review.reviews.filter(user_id=request.user.pk).exists()
            context["feedbacks"] = lesson.review.reviews.all()
        return render(request, 'lesson_feedback.html', context)

    def post(self,request,*args,**kwargs):
        feedback = request.POST.get("feedback")
        comment_pos = request.POST.get("feed_comment_pos")
        comment_neg = request.POST.get("feed_comment_neg")
        lesson_id = request.POST.get("lesson_id")

        if not feedback:
            response = {
                "status": "FAILURE",
                "message": "FEEDBACK_REQUIRED",
                "data": ""
            }
            return self.render_to_json(response)

        if feedback == "negative" and not comment_neg:
            response = {
                "status": "FAILURE",
                "message": "COMMENT_REQUIRED",
                "data": ""
            }
            return self.render_to_json(response)
        lesson = LessonRequest.objects.filter(pk=int(lesson_id))
        if lesson.exists():
            lesson = lesson.first()
            if request.user.champuser.type.name == "student":
                valid_users = [lesson.user.pk]
                valid_users += [lesson.request_user.pk]
                valid_users += lesson.other_users.all().values_list('pk',flat=True)
                if request.user.pk in valid_users:
                    if lesson.review:
                        lesson_review = lesson.review
                    else:
                        lesson_review = LessonReview()
                        lesson_review.save()
                        lesson.review_id = lesson_review.pk
                        lesson.save()

                    if feedback == "positive":
                        if not lesson_review.reviews.filter(user_id=request.user.pk).exists():
                            feedback_like = FeedbackLikes()
                            feedback_like.feedback = True
                            feedback_like.user_id = request.user.pk
                            if comment_pos:
                                feedback_like.comment = comment_pos
                            feedback_like.save()
                            lesson_review.reviews.add(feedback_like)

                    elif feedback == "negative":
                        if not lesson_review.reviews.filter(user_id=request.user.pk).exists():
                            feedback_like = FeedbackLikes()
                            feedback_like.feedback = False
                            feedback_like.user_id = request.user.pk
                            feedback_like.comment = comment_neg
                            feedback_like.save()
                            lesson_review.reviews.add(feedback_like)

                    response = {
                        "status": "SUCCESS",
                        "message": "Successful",
                        "data": ""
                    }
                    return self.render_to_json(response)

        response = {
            "status": "FAILURE",
            "message": "Failed",
            "data": ""
        }
        return self.render_to_json(response)

class ApplyJobAjaxView(LoginRequiredMixin, JSONResponseMixin, View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ApplyJobAjaxView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        job_id = request.GET.get("job_id")
        job = Job.objects.get(pk=int(job_id))
        template_name = "ajax/job_apply_dialog.html"
        template = loader.get_template(template_name)
        country_name = ''
        tzm = TimezoneMapper.objects.filter(tz=request.user.champuser.timezone)
        if tzm.exists():
            tzm = tzm.first()
            country_name = tzm.country.name
        user_currency = CurrencyUtil.get_currency_code_for_country(country_name)
        conversion_enabled = job.preference.currency != user_currency
        base_currency = "USD"
        target_currency = "USD" if not user_currency in BASE_CURRENCIES else user_currency
        context = {
            "job": job,
            "duration_list": TimeUtil.get_all_duration_list(),
            "currency_list": PaymentCurrencyUtil.get_all_currencies(),
            "conversion_enabled": conversion_enabled,
            "target_currency": target_currency,
            "converted_value": RateConversionUtil.convert(job.preference.currency, target_currency, job.preference.rate.rate_value)
        }
        cntxt = Context(context)
        rendered = template.render(cntxt)
        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": rendered
        }
        return self.render_to_json(response)

    def validate_params(self, request):
        job_id = request.POST.get("job_id")
        start_date_default = request.POST.get("start_date_default")
        start_date = request.POST.get("start_date")
        duration_default = request.POST.get("duration_default")
        duration = request.POST.get("duration")
        currency = request.POST.get("currency")
        rate_default = request.POST.get("rate_default")
        rate = request.POST.get("rate")
        message = request.POST.get("message")
        repeat = request.POST.get("apply_repeat")

        date_changed = not(start_date_default.strip() == start_date.strip())

        try:
            utc_dt = Clock.local_to_utc_datetime(start_date, '%m/%d/%Y %H:%M', request.user.champuser.timezone)
            start_date = time.mktime(utc_dt.timetuple())
        except:
            try:
                utc_dt = Clock.local_to_utc_datetime(start_date, '%m/%d/%Y %H:%M %p', request.user.champuser.timezone)
                start_date = time.mktime(utc_dt.timetuple())
            except:
                return None, None, None, None, None, None, None, None, None, None, None

        try:
            job_id = int(job_id)
            job = Job.objects.get(pk=job_id)
            duration_default = int(duration_default)
            duration = int(duration)
            rate_default = Decimal(rate_default)
            rate = Decimal(rate)
            repeat = int(repeat)
            return job_id, job, start_date, duration, currency, rate, message,repeat, date_changed, not(duration_default == duration), not(rate_default==rate)
        except Exception as msg:
            return None, None, None, None, None, None, None, None, None, None, None

    def post(self,request,*args,**kwargs):
        job_id, job, start_date, duration, currency, rate, messages, repeat, date_changed, duration_changed, rate_changed = self.validate_params(request)

        if any([ not job_id, not job, not start_date, not duration, not rate, not repeat ]):
            response = {
                "status": "FAILURE",
                "message": "Missing parameter",
                "data": ""
            }
            return self.render_to_json(response)

        if not request.user.is_authenticated():
            response = {
                "status": "FAILURE",
                "message": "You must login to apply to the job",
                "data": ""
            }
            return self.render_to_json(response)

        if not job.applications.filter(tutor_id=request.user.pk).exists():
            price_settlement = PriceSettlement()
            price_settlement.price = rate
            price_settlement.price2 = 0
            price_settlement.currency = job.preference.currency
            price_settlement.created_by_id = request.user.pk
            price_settlement.status = PriceSettlementStatus.Created.value
            price_settlement.save()

            initiated_as_label = 'AppliedByTutor'
            initiated_as_id = -1
            job_invites = JobInvitation.objects.filter(job_id=job_id, user_id=request.user.pk)
            if job_invites.exists():
                initiated_as_label = 'JobInvitation'
                initiated_as_id = job_invites.first().pk
            else:
                if JobUtil.check_if_job_is_recommended_for_user(request.user.pk, job_id):
                    initiated_as_label = 'JobRecommendation'

            job_app = JobApplication()
            job_app.tutor_id = request.user.pk
            job_app.start_time = start_date
            job_app.duration = duration
            job_app.settlement_id = price_settlement.pk
            job_app.initiated_as_label = initiated_as_label
            job_app.repeat = repeat
            job_app.initiated_as_id = initiated_as_id if initiated_as_id != -1 else 0
            job_app.status = 2 ###Applied

            if date_changed:
                job_app.time_edited_by = request.user
            if duration_changed:
                job_app.duration_edited_by = request.user
            if rate_changed:
                job_app.price_edited_by = request.user

            job_app.save()

            if messages:
                msg = Message()
                msg.chat_type = ChatType.job_application.value
                msg.is_read = ReadStatus.unread.value
                msg.msg = messages
                msg.save()

                ja_message = JobApplicationMessage()
                ja_message.sender_id = request.user.pk
                ja_message.receiver_id = job.posted_by_id
                ja_message.message_id = msg.pk
                ja_message.save()

                job_app.apply_message_id = ja_message.pk
                job_app.save()

            # html_msg = "<div>Job Application</div><div>================================</div>"+job_apply_message+"<div><a href=\""+reverse("job_application_details_view", kwargs={ "job_id": int(job_id), "pk": job_app.pk })+"\">To Reply Go to job application</a></div>"
            # msg.msg = html_msg
            # msg.save()

            apply_source = "marketplace"

            ###Now update JobInvitation or JobRecommendation.
            ji_objects = JobInvitation.objects.filter(job_id=job_id,user_id=request.user.pk)
            if ji_objects.exists():
                ji_object = ji_objects.first()
                ji_object.status = 2 ###Applied
                ji_object.save()
                apply_source = "invite"

            job.applications.add(job_app)

            student_name = job.posted_by.champuser.fullname
            tutor_name = request.user.champuser.fullname
            application_id = job_app.pk
            html, text = EmailTemplateUtil.render_job_applied_email_template(student_name, tutor_name, application_id,messages, source=apply_source)

            subjects = ','.join([ s.name for s in job.preference.subjects.all() ])

            email_subject = "%s has accepted your invitation for %s %s" % ( tutor_name, job.preference.level.name, subjects )
            if apply_source == "marketplace":
                email_subject = "%s has applied for %s %s" % ( tutor_name, job.preference.level.name, subjects )

            email_object = {
                "recipients": [ job.posted_by.email ],
                'subject': email_subject,
                'html_body': html,
                'text_body': text
            }

            EmailScheduler.place_to_queue(email_object)

            if apply_source == "marketplace":
                NotificationManager.push_notification("recommend_applied",job.pk, request.user.pk, job.posted_by.pk)
            else:
                NotificationManager.push_notification("invite_applied",job.pk, request.user.pk, job.posted_by.pk)

            response = {
                "status": "SUCCESS",
                "message": "Successful",
                "data": ""
            }
            return self.render_to_json(response)
        else:
            response = {
                "status": "FAILURE",
                "message": "Already Applied",
                "data": ""
            }
            return self.render_to_json(response)
        return HttpResponseRedirect(reverse("user_profile",kwargs={"pk": request.user.pk}))

class JobApplicationWithdrawView(LoginRequiredMixin, JSONResponseMixin, View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(JobApplicationWithdrawView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        job_id = request.GET.get("job_id")
        job = Job.objects.get(pk=int(job_id))
        user_id = request.GET.get("user_id")
        application = None
        applications = job.applications.filter(tutor_id=int(user_id))
        if applications.exists():
            application = applications.first()
        template_name = "ajax/job_application_withdraw_dialog.html"
        template = loader.get_template(template_name)
        context = {
            "job": job,
            "application": application
        }
        price = job.preference.rate.rate_value
        price2 = job.preference.rate.rate_value2
        currency = job.preference.currency
        lesson_duration = job.lesson_duration
        start_date = job.lesson_time

        job_applications = JobApplication.objects.filter(job__id=job.pk, tutor_id=request.user.pk)
        if job_applications.exists():
            job_application = job_applications.first()
            currency = job_application.settlement.currency
            price = job_application.settlement.price
            price2 = job_application.settlement.price2
            lesson_duration = job_application.duration
            start_date = job_application.start_time

        context['currency'] = currency
        context['price'] = price
        context['price2'] = price2
        context['lesson_duration'] = lesson_duration
        context['start_date'] = start_date
        cntxt = Context(context)
        rendered = template.render(cntxt)
        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": rendered
        }
        return self.render_to_json(response)
    def post(self, request, *args, **kwargs):

        if not request.user.is_authenticated():
            return HttpResponseRedirect(reverse("user_login"))

        job_id = request.POST.get("job_id")
        application_id = request.POST.get("application_id")
        state_reason = request.POST.get("state_reason")
        withdraw_message = request.POST.get("withdraw_message")

        job_id = int(job_id)
        application_id = int(application_id)
        state_reason = int(state_reason)

        with transaction.atomic():
            job_invites = JobInvitation.objects.filter(job_id=job_id, user_id=request.user.pk)
            if job_invites.exists():
                job_invite = job_invites.first()
                job_invite.status = 13 ###Application Withdrawn
                job_invite.save()

            job_app = JobApplication.objects.filter(pk=application_id)
            if job_app.exists():
                job_app = job_app.first()
                job = Job.objects.filter(pk=job_id)
                if job.exists():
                    job = job.first()
                    if job.applications.filter(pk=application_id).exists():
                        job_app.status = 13
                        if state_reason == 1 and withdraw_message:
                            msg = Message()
                            msg.msg = withdraw_message
                            msg.save()

                            jam = JobApplicationMessage()
                            jam.message_id = msg.pk
                            jam.sender_id = request.user.pk
                            jam.receiver_id = job.posted_by.pk
                            jam.save()

                            job_app.withdraw_message_id = jam.pk

                        job_app.save()

                        job.applications.remove(job_app)

                        job_application_archives = JobApplicationArchive.objects.filter(application_id=job_app.pk)
                        if not job_application_archives.exists():
                            job_application_archive = JobApplicationArchive()
                            job_application_archive.application_id = job_app.pk
                            job_application_archive.job_id = job.pk
                            job_application_archive.save()

                    student_name = job.posted_by.champuser.fullname
                    tutor_name = request.user.champuser.fullname
                    application_id = job_app.pk
                    html, text = EmailTemplateUtil.render_job_application_withdrawn_email_template(student_name, tutor_name, job_id, application_id,withdraw_message)

                    email_object = {
                        "recipients": [ job.posted_by.email ],
                        'subject': '%s has withdrawn %s application' % ( tutor_name, "his" if request.user.champuser.gender == "male" else "her" ),
                        'html_body': html,
                        'text_body': text
                    }

                    EmailScheduler.place_to_queue(email_object)

                    NotificationManager.push_notification("application_withdrawn",job.pk, request.user.pk, job.posted_by.pk)

        return HttpResponseRedirect(reverse("job_post_details", kwargs={ "pk": int(job_id) }))

class RejectJobAjaxView(LoginRequiredMixin, JSONResponseMixin, View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(RejectJobAjaxView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        job_id = request.GET.get("job_id")
        job = Job.objects.get(pk=int(job_id))
        template_name = "ajax/job_reject_dialog.html"
        template = loader.get_template(template_name)
        context = {
            "job": job
        }
        cntxt = Context(context)
        rendered = template.render(cntxt)
        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": rendered
        }
        return self.render_to_json(response)

    def post(self, request, *args, **kwargs):
        job_id = request.POST.get("job_id")
        reason = request.POST.get("message")

        job_id = int(job_id)

        job_invites = JobInvitation.objects.filter(job_id=job_id, user_id=request.user.pk)
        if job_invites.exists():
            job_invite = job_invites.first()
            job_invite.status = 5 ###Rejected JobInvitationStatus.rejected

            if reason.strip():
                job_invite.reject_message = reason.strip()
            job_invite.save()

            ###Send Email and Notification.
            job = Job.objects.get(pk=job_id)
            student_name = job.posted_by.champuser.fullname
            tutor_name = request.user.champuser.fullname
            job_invitation_id = job_invite.pk
            html, text = EmailTemplateUtil.render_job_invitation_rejected_email_template(student_name, tutor_name, job_invitation_id,reason)

            subjects = ','.join([ s.name for s in job.preference.subjects.all() ])

            email_object = {
                "recipients": [ job.posted_by.email ],
                'subject': '%s has declined your invitation for %s %s' % ( tutor_name, job.preference.level.name, subjects ),
                'html_body': html,
                'text_body': text
            }

            EmailScheduler.place_to_queue(email_object)

            NotificationManager.push_notification("invite_rejected",job_id,request.user.pk,job.posted_by.pk)

        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": ""
        }
        return self.render_to_json(response)

class RejectJobRecommendationAjaxView(LoginRequiredMixin, JSONResponseMixin, View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(RejectJobRecommendationAjaxView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        job_id = request.GET.get("job_id")
        job = Job.objects.get(pk=int(job_id))
        template_name = "ajax/job_recommendation_reject_dialog.html"
        template = loader.get_template(template_name)
        context = {
            "job": job
        }
        cntxt = Context(context)
        rendered = template.render(cntxt)
        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": rendered
        }
        return self.render_to_json(response)

    def post(self, request, *args, **kwargs):
        job_id = request.POST.get("job_id")
        reason = request.POST.get("message")

        job_id = int(job_id)

        job_recommendations = JobRecommendation.objects.filter(job_id=job_id, user_id=request.user.pk)
        if not job_recommendations.exists():
            job_recommendation = JobRecommendation()
            job_recommendation.user_id = request.user.pk
            job_recommendation.job_id = job_id
            job_recommendation.recommended_by = 0
            job_recommendation.criteria_matched = 100
            job_recommendation.status = JobRecommendationStatus.recommendation_rejected.value
            if reason:
                job_recommendation.reject_message = reason
            job_recommendation.save()

        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": ""
        }
        return self.render_to_json(response)

class RejectJobApplicationAjaxView(LoginRequiredMixin, JSONResponseMixin, View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(RejectJobApplicationAjaxView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        job_id = request.GET.get("job_id")
        tutor_id = request.GET.get("tutor_id")
        job = Job.objects.get(pk=int(job_id))
        template_name = "ajax/job_application_reject_dialog.html"
        template = loader.get_template(template_name)
        context = {
            "job": job,
            "tutor_id": tutor_id,
        }
        cntxt = Context(context)
        rendered = template.render(cntxt)
        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": rendered
        }
        return self.render_to_json(response)

    def post(self, request, *args, **kwargs):
        job_id = request.POST.get("job_id")
        tutor_id = request.POST.get("tutor_id")
        state_reason = request.POST.get("state_reason")
        reason = request.POST.get("reason")
        job_id = int(job_id)
        state_reason = int(state_reason)
        tutor_id = int(tutor_id)
        with transaction.atomic():
            ji_objects = JobInvitation.objects.filter(job_id=job_id,user_id=tutor_id)
            if ji_objects.exists():
                ji_object = ji_objects.first()
                ji_object.status = 10 ###Rejected Application
                ji_object.save()

            job = Job.objects.get(pk=job_id)
            applications = job.applications.filter(tutor_id=tutor_id)
            if applications.exists():
                application = applications.first()
                application.status = 10 ###Rejected Application
                if state_reason:

                    message = Message()
                    message.msg = reason
                    message.save()

                    jam = JobApplicationMessage()
                    jam.message_id = message.pk
                    jam.sender_id = request.user.pk
                    jam.receiver_id = tutor_id
                    jam.save()

                    application.reject_message_id = jam.pk

                application.save()

                student_name = request.user.champuser.fullname
                tutor = ChampUser.objects.get(user_id=tutor_id)
                tutor_name = tutor.fullname
                job_application_id = application.pk
                reject_message = None
                if state_reason:
                    reject_message = reason
                html, text = EmailTemplateUtil.render_job_application_rejected_email_template(student_name, tutor_name, job_application_id, reject_message)

                email_object = {
                    "recipients": [ tutor.user.email ],
                    'subject': 'We are sorry that your application has been declined by %s' % student_name,
                    'html_body': html,
                    'text_body': text
                }

                EmailScheduler.place_to_queue(email_object)

                NotificationManager.push_notification("job_application_rejected", job.pk, request.user.pk, tutor_id)

        return HttpResponseRedirect(reverse("job_post_details", kwargs={ "pk": job_id }))

class RejectJobOfferAjaxView(LoginRequiredMixin, JSONResponseMixin, View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(RejectJobOfferAjaxView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        job_id = request.GET.get("job_id")
        user_id = request.GET.get("user_id")
        offer_id = request.GET.get("offer_id")
        job = Job.objects.get(pk=int(job_id))
        template_name = "ajax/job_offer_reject_dialog.html"
        template = loader.get_template(template_name)
        reject_reason = JobApplicationRejectReason.objects.filter(context='OFFER')
        context = {
            "job": job,
            "user_id": user_id,
            "offer_id": offer_id,
            "reject_reason": reject_reason
        }
        cntxt = Context(context)
        rendered = template.render(cntxt)
        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": rendered
        }
        return self.render_to_json(response)

    def post(self, request, *args, **kwargs):
        job_id = request.POST.get("job_id")
        user_id = request.POST.get("user_id")
        offer_id = request.POST.get("offer_id")
        reason_id = request.POST.get("reason")
        job_id = int(job_id)
        reason_id = int(reason_id)
        offer_id = int(offer_id)
        try:
            user_id = int(user_id)
        except:
            user_id = request.user.pk
        ji_objects = JobInvitation.objects.filter(job_id=job_id,user_id=user_id)
        jr_objects = JobRecommendation.objects.filter(job_id=job_id,user_id=user_id)
        if ji_objects.exists():
            ji_object = ji_objects.first()
            ji_object.status = JobInvitationStatus.rejected_offer.value
            ji_object.save()

        elif jr_objects.exists():
            jr_object = jr_objects.first()
            jr_object.status = JobRecommendationStatus.rejected_offer.value
            jr_object.save()

        job = Job.objects.get(pk=job_id)
        applications = job.applications.filter(tutor_id=user_id)
        if applications.exists():
            application = applications.first()
            application.status = JobApplicationStatus.rejected_offer.value
            application.save()

            ja_reject_feedback = JobOfferRejectFeedback()
            ja_reject_feedback.offer_id = offer_id
            ja_reject_feedback.reason_id = reason_id
            ja_reject_feedback.created_by_id = request.user.pk
            ja_reject_feedback.save()

            job_offer = JobOffer.objects.filter(pk=offer_id)
            if job_offer.exists():
                job_offer = job_offer.first()
                job_offer.status = JobOfferStatus.rejected_offer.value
                job_offer.save()

                student_name = job.posted_by.champuser.fullname
                tutor_id = request.user.pk
                tutor_name = request.user.champuser.fullname
                job_offer_id = job_offer.pk
                html, text = EmailTemplateUtil.render_job_offer_rejected_email_template(student_name, tutor_id, tutor_name, job_offer_id)

                email_object = {
                    "recipients": [ job.posted_by.email  ],
                    'subject': 'Job Offer Rejected',
                    'html_body': html,
                    'text_body': text
                }
                EmailScheduler.place_to_queue(email_object)

                NotificationManager.push_notification("offer_rejected",job.pk,request.user.pk,job.posted_by.pk)

        return HttpResponseRedirect(reverse("job_offer_details", kwargs={ "job_id": job_id, "user_id": user_id  }))


class JobApplicationListView(LoginRequiredMixin,ListView):
    template_name = "job_application_list.html"
    model = ProgressReport
    paginate_by = 10

    def get_queryset(self):
        return JobApplication.objects.filter(tutor_id=self.request.user.pk).order_by("-date_created")

    def get_context_data(self, **kwargs):
        context = super(JobApplicationListView, self).get_context_data(**kwargs)
        return context

class JobApplicationDetailsView(LoginRequiredMixin, DetailView):
    template_name = "job_application_details.html"
    model = JobApplication

    def get_object(self, queryset=None):
        return JobApplication.objects.get(id=int(self.kwargs.get("pk")))

    def get_context_data(self, **kwargs):
        context = super(JobApplicationDetailsView, self).get_context_data(**kwargs)
        # context[""]
        context["job"] = Job.objects.get(pk=int(self.kwargs.get("job_id")))
        return context

class JobApplicationReplyView(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        # ja_id = request.POST.get("job_application_id")
        msg_text = request.POST.get("job_application_message")
        job_id = kwargs.get("job_id")
        application_id = kwargs.get("pk")

        job_app = JobApplication.objects.get(pk=application_id)

        msg = Message()
        msg.chat_type = ChatType.job_application.value
        msg.is_read = ReadStatus.unread.value
        msg.msg = msg_text
        msg.save()

        user_message = UserMessage()
        user_message.sender_id = request.user.pk
        # user_message.receiver_id = job.posted_by_id
        user_message.message_id = msg.pk
        user_message.save()

        job_app.message.add(user_message)

        for file_name,content in request.FILES.items():
            if file_name.startswith("written_file") and len(file_name) > len("written_file"):
                file = content
                original_file_name = file._name
                file_name = str(request.user.id)+"-"+str(uuid.uuid4())+"."+file._name[file._name.rindex(".")+1:]
                with open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name), 'wb+') as destination:
                    for chunk in file.chunks():
                        destination.write(chunk)
                file = File(open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name)))
                msg_attachment = MessageAttachment()
                msg_attachment.message_id = msg.pk
                msg_attachment.attachment_name = original_file_name
                msg_attachment.attachment = file
                msg_attachment.save()

        return HttpResponseRedirect(reverse("job_application_details_view", kwargs= { "job_id": job_id, "pk": application_id  }))

class JobApplicationAgreementActionView(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        job_id = int(kwargs.get("job_id"))
        application_id = int(kwargs.get("pk"))
        action = int(request.POST.get("action"))

        get_param = lambda x: request.POST.get(x)

        if action == PriceSettlementStatus.WithdrawApplication.value:
            reason = get_param("withdraw_reason")
            details = get_param("withdraw_reason_message")

            job_application = JobApplication.objects.get(pk=application_id)
            job_application.status = JobApplicationStatus.cancelled.value
            job_application.save()
            
            withdraw_feedback = JobApplicationWithdrawFeedback()
            withdraw_feedback.user_id = request.user.pk
            withdraw_feedback.application_id = application_id
            withdraw_feedback.reason = reason
            if details:
                withdraw_feedback.details = details
            withdraw_feedback.save()

            for file_name,content in request.FILES.items():
                if file_name.startswith("attachment") and len(file_name) > len("attachment"):
                    file = content
                    original_file_name = file._name
                    file_name = str(request.user.id)+"-"+str(uuid.uuid4())+"."+file._name[file._name.rindex(".")+1:]
                    with open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name), 'wb+') as destination:
                        for chunk in file.chunks():
                            destination.write(chunk)
                    file = File(open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name)))
                    job_attachment = JobAttachment()
                    job_attachment.attachment_name = original_file_name
                    job_attachment.attachment = file
                    job_attachment.save()

                    withdraw_feedback.attachment.add(job_attachment)

        elif action == PriceSettlementStatus.RejectedApplication.value:
            reason = get_param("decline_reason")
            details = get_param("decline_reason_message")

            job_application = JobApplication.objects.get(pk=application_id)
            job_application.status = JobApplicationStatus.rejected.value
            job_application.save()

            withdraw_feedback = JobApplicationWithdrawFeedback()
            withdraw_feedback.user_id = request.user.pk
            withdraw_feedback.application_id = application_id
            withdraw_feedback.reason = reason
            if details:
                withdraw_feedback.details = details
            withdraw_feedback.save()

            for file_name,content in request.FILES.items():
                if file_name.startswith("attachment") and len(file_name) > len("attachment"):
                    file = content
                    original_file_name = file._name
                    file_name = str(request.user.id)+"-"+str(uuid.uuid4())+"."+file._name[file._name.rindex(".")+1:]
                    with open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name), 'wb+') as destination:
                        for chunk in file.chunks():
                            destination.write(chunk)
                    file = File(open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name)))
                    job_attachment = JobAttachment()
                    job_attachment.attachment_name = original_file_name
                    job_attachment.attachment = file
                    job_attachment.save()

                    withdraw_feedback.attachment.add(job_attachment)


        elif action == PriceSettlementStatus.AcceptedAndOffered.value:
            msg = request.POST.get("message")
            job_application = JobApplication.objects.get(pk=application_id)
            last_settlement = job_application.price_settlement
            price_settlement = PriceSettlement()
            price_settlement.price = last_settlement.price
            price_settlement.currency = last_settlement.currency
            price_settlement.created_by_id = request.user.pk
            price_settlement.status = PriceSettlementStatus.AcceptedAndOffered.value
            price_settlement.save()

            job_application.settlement.add(price_settlement)

            message = Message()
            message.msg_date = Clock.utc_timestamp()
            message.is_read = ReadStatus.unread.value
            message.chat_type = ChatType.job_application.value
            message.msg = msg
            message.save()

            user_message = UserMessage()
            user_message.sender_id = request.user.pk
            user_message.receiver_id = job_application.tutor.pk
            user_message.message_id = message.pk
            user_message.save()

            job_application.message.add(user_message)


        elif action == PriceSettlementStatus.Discuss.value:
            adjust_hourly_rate = request.POST.get("adjust_hourly_rate")
            msg = request.POST.get("message")
            job_application = JobApplication.objects.get(pk=application_id)
            last_settlement = job_application.price_settlement
            price_settlement = PriceSettlement()
            price_settlement.price = Decimal(adjust_hourly_rate)
            price_settlement.currency = last_settlement.currency
            price_settlement.created_by_id = request.user.pk
            price_settlement.status = PriceSettlementStatus.Discuss.value
            price_settlement.save()

            job_application.settlement.add(price_settlement)

            message = Message()
            message.msg_date = Clock.utc_timestamp()
            message.is_read = ReadStatus.unread.value
            message.chat_type = ChatType.job_application.value
            message.msg = request.user.champuser.fullname+" has requested rate adjustment. Message: "+msg
            message.save()

            user_message = UserMessage()
            user_message.sender_id = request.user.pk
            user_message.receiver_id = job_application.tutor.pk
            user_message.message_id = message.pk
            user_message.save()

            job_application.message.add(user_message)

        elif action == PriceSettlementStatus.OfferRejected.value:
            job_application = JobApplication.objects.get(pk=application_id)
            last_settlement = job_application.price_settlement
            price_settlement = PriceSettlement()
            price_settlement.price = last_settlement.price
            price_settlement.currency = last_settlement.currency
            price_settlement.created_by_id = request.user.pk
            price_settlement.status = PriceSettlementStatus.OfferRejected.value
            price_settlement.save()

            job_application.settlement.add(price_settlement)

            message = Message()
            message.msg_date = Clock.utc_timestamp()
            message.is_read = ReadStatus.unread.value
            message.chat_type = ChatType.job_application.value
            message.msg = request.user.champuser.fullname+" has rejected the job offer"
            message.save()

            user_message = UserMessage()
            user_message.sender_id = request.user.pk
            user_message.receiver_id = job_application.tutor.pk
            user_message.message_id = message.pk
            user_message.save()

            job_application.message.add(user_message)

        elif action == PriceSettlementStatus.Rejected.value:
            job_application = JobApplication.objects.get(pk=application_id)
            job_application.status = JobApplicationStatus.rejected.value
            job_application.save()
        elif action == PriceSettlementStatus.OfferAccepted.value:
            job = Job.objects.get(pk=job_id)
            lesson_type = job.lesson_type
            if lesson_type == 0: ###Live lesson
                lesson_util = LessonUtil()
                course_name = job.preference.subjects.all().first().value
                course_objects = Major.objects.filter(name=course_name)
                if course_objects.exists():
                    course_id = course_objects.first().pk
                    lesson_util.create_live_lesson(job.posted_by.pk, request.user.pk, job.other_students.all(), job.lesson_time, job.lesson_duration, course_id, job.description, "no_repeat", ApprovalStatus.approved.value)
            else:
                lesson_util = LessonUtil()
                course_name = job.preference.subjects.all().first().value
                course_objects = Major.objects.filter(name=course_name)
                if course_objects.exists():
                    course_id = course_objects.first().pk
                    lesson_util.create_written_lesson(job.posted_by.pk, request.user.pk, job.lesson_time, job.lesson_duration, course_id, job.description, request=None, job = job)

            job_application = JobApplication.objects.get(pk=application_id)
            job_application.status = JobApplicationStatus.awarded.value
            job_application.save()

        return HttpResponseRedirect(reverse("job_application_details_view", kwargs={ "job_id": job_id, "pk": application_id }))





