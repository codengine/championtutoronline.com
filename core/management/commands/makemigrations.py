__author__ = 'codengine'

import os
from django.core.management.base import BaseCommand
from optparse import make_option
from apps import APPS


class Command(BaseCommand):

    option_list = BaseCommand.option_list + (
        make_option('--initial',
                    dest='initial',
                    action='store_true',
                    help='Initial Migrations'),
        make_option('--auto',
                    dest='auto',
                    action='store_true',
                    help='Auto Migrations'),
    )

    def init_migrations(self,*args,**options):
        option = None
        if options.get("initial"):
            option = "--initial"
        elif options.get("auto"):
            option = "--auto"
        for app in APPS:
            cmd = "python manage.py schemamigration "+ app +" "+(option if option else "")
            print("Executing...")
            print(cmd)
            os.system(cmd)

    def handle(self, *args, **options):
        self.init_migrations(*args,**options)

