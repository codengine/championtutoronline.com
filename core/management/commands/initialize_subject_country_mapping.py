from django.core.management.base import BaseCommand
from core.library.subject_util import SubjectUtil

__author__ = 'Sohel'

class Command(BaseCommand):

    def handle(self, *args, **options):
        print("Initializing subject country mapper")
        SubjectUtil.initialize_subject_country_mapper()
        print("Done.")
