import os
import shutil
from os import listdir
from datetime import datetime
from os.path import isfile, join
import simplejson as json
from django.core.management.base import BaseCommand

__author__ = 'codengine'

def read_all_files_with_mtime(directory):
    onlyfiles = [f for f in listdir(directory) if isfile(join(directory, f))]
    return onlyfiles


def read_modified_datetime(directory, filelist):
    file_dict = {}
    for f in filelist:
        try:
            mtime = os.path.getmtime(join(directory, f))
        except OSError:
            mtime = 0
        last_modified_date = datetime.fromtimestamp(mtime)
        id_part = f.split('_')[0] if len(f.split('_')) >= 2 else None
        if id_part:
            try:
                id_part = str(int(id_part))
                if file_dict.get(id_part):
                    last_time = file_dict[id_part]['time']
                    if last_modified_date > last_time:
                        file_dict[id_part] = {
                            'file_name': f,
                            'time': last_modified_date
                        }
                else:
                    file_dict[id_part] = {
                        'file_name': f,
                        'time': last_modified_date
                    }
            except Exception as exp:
                pass
    return file_dict

def collect_images(image_dir):
    all_files = read_all_files_with_mtime(image_dir)

    file_dict = read_modified_datetime(image_dir, all_files)

    return file_dict

def write_image_files(file_dir, destination_dir, file_dict):
    extension_mapping = {}
    for tutor_id, file_info in file_dict.items():
        file_path = os.path.join(file_dir, file_info['file_name'])

        file_name = file_info['file_name']

        try:

            file_extension = file_name[file_name.rindex('.'):]

            extension_mapping[tutor_id] = file_extension

            shutil.copy2(file_path, destination_dir + '/' + tutor_id + file_extension)

        except Exception as exp:
            print(file_name)
            print(str(exp))

    return extension_mapping

class Command(BaseCommand):

    def handle(self, *args, **options):
        base_file_path = os.path.dirname(os.path.realpath(__file__))
        base_file_path = os.path.join(base_file_path, 'tutor_migrations')
        file_name = os.path.join(base_file_path, 'tutor.json')

        image_dir = '/home/codengine/Desktop/tutor_img'

        file_dict = collect_images(image_dir)

        thumbnail_dir = image_dir + "/thumb"

        thumb_dict = collect_images(thumbnail_dir)

        common__ids = set(file_dict.keys()) & set(thumb_dict.keys())

        original_files = {x:file_dict[x] for x in file_dict if x in common__ids}

        profile_image_extensions = write_image_files(image_dir, base_file_path + '/profile_img', original_files)

        thumbnail_files = {x: thumb_dict[x] for x in thumb_dict if x in common__ids}

        image_thumbnail_extension = write_image_files(thumbnail_dir, base_file_path + '/profile_img/thumb', thumbnail_files)

        new_file_name = os.path.join(base_file_path, 'image_extension.json')
        with open(new_file_name, 'wb+') as f:
            data_to_write = json.dumps(profile_image_extensions, ensure_ascii=False, encoding='utf8')
            f.write(data_to_write.encode('utf-8'))

        new_file_name = os.path.join(base_file_path, 'thumbnail_extension.json')
        with open(new_file_name, 'wb+') as f:
            data_to_write = json.dumps(image_thumbnail_extension, ensure_ascii=False, encoding='utf8')
            f.write(data_to_write.encode('utf-8'))








