__author__ = 'codengine'

from django.core.management.base import BaseCommand
user_counter = 1
class Command(BaseCommand):

    def handle(self, *args, **options):
        import os
        from core.models3 import SubjectLevel, Major
        file_path = os.path.dirname(os.path.realpath(__file__))
        file_path = os.path.join(file_path, 'tutor_migrations')
        file_path = os.path.join(file_path, 'subjects')
        file_dir1 = os.path.join(file_path, 'Malaysia')
        file_dir2 = os.path.join(file_path, 'Other')

        dir1 = os.listdir(file_dir1)
        for f in dir1:
            fpath = os.path.join(file_dir1, f)
            print(fpath)
            with open(fpath, 'r') as ff:
                content = ff.read()
                lines = content.split('\n')
                subjects = [ l.split(',')[1].strip() for l in lines ]
                level_name = f.replace('.csv', '')
                print(level_name)
                print(subjects)

                for s in subjects:
                    slevels = SubjectLevel.objects.filter(name=level_name)
                    if slevels.exists():
                        slevel = slevels.first()
                    else:
                        slevel = SubjectLevel()
                        slevel.name = s
                        slevel.save()

                    majors = Major.objects.filter(name=s)
                    if not majors.exists():
                        major = Major()
                        major.name = s
                        major.level_id = slevel.pk
                        major.creator_id = 1
                        major.save()

        dir2 = os.listdir(file_dir2)
        for f in dir2:
            fpath = os.path.join(file_dir2, f)
            print(fpath)
            with open(fpath, 'r') as ff:
                content = ff.read()
                lines = content.split('\n')
                subjects = [ l.split(',')[1].strip() for l in lines ]
                level_name = f.replace('.csv', '')
                print(level_name)
                print(subjects)

                for s in subjects:
                    slevels = SubjectLevel.objects.filter(name=level_name)
                    if slevels.exists():
                        slevel = slevels.first()
                    else:
                        slevel = SubjectLevel()
                        slevel.name = s
                        slevel.save()

                    majors = Major.objects.filter(name=s)
                    if not majors.exists():
                        major = Major()
                        major.name = s
                        major.level_id = slevel.pk
                        major.creator_id = 1
                        major.save()

