import os
import simplejson as json
import re
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

__author__ = 'Sohel'

class Command(BaseCommand):

    def handle(self, *args, **options):
        file_path = os.path.dirname(os.path.realpath(__file__))
        file_path = os.path.join(file_path, 'tutor_migrations')
        
        tutor_ids = {}

        emails = []

        email_id_mapping = {}

        duplicate_emails = []

        base_path = os.path.dirname(os.path.realpath(__file__))
        tutor_list_file_path = os.path.join(base_path, 'tutor_migrations')
        tutor_list_file_path = os.path.join(file_path, 'tutor_list.csv')

        tutor_list_contents = []
        with open(tutor_list_file_path, 'r') as f:
            tutor_list_contents = f.readlines()

        tutor_list_ids = [ int(line.split(',')[0]) for line in tutor_list_contents[ 1: ] ]

        for i in range(1, 7):
            tutor_file_name='tutor_%s.json' % i

            base_path = os.path.dirname(os.path.realpath(__file__))
            file_path = os.path.join(base_path, 'tutor_migrations')
            file_name = os.path.join(file_path, tutor_file_name)
            tutor_file = open(file_name,"r")
            content = tutor_file.read()
            content = content.replace("'", '"')
            content = content.replace('NULL', '""')
            content = content.replace(r'12020_""', r'12020_')
            content = re.sub("\s+", " ", content)
            json_data = json.loads(content)
            tutor_file.close()

            for data_record in json_data:
                if data_record['email'] in emails:
                    duplicate_emails += [ data_record['email'] ]
                else:
                    emails += [ data_record['email'] ]
                    email_id_mapping[data_record['email']] = data_record['tutor_id']

        print(len(emails))
        user_instances = User.objects.filter(username__in=emails)
        print(user_instances)
        for user_instance in user_instances:
            db_id = user_instance.pk
            tutor_id = email_id_mapping[user_instance.username]
            if tutor_id:
                tutor_ids[int(tutor_id)] = db_id

        write_contents = []

        for key, value in tutor_ids.items():
            write_contents += [
                {
                    "tutor_id": key,
                    "db_id": value
                }
            ]


        base_path = os.path.dirname(os.path.realpath(__file__))
        tutor_id_mapping_file_path = os.path.join(base_path, 'tutor_migrations')

        new_file_name = os.path.join(tutor_id_mapping_file_path, 'tutor_id_mapping.json')
        with open(new_file_name, 'wb+') as f:
            data_to_write = json.dumps(write_contents, ensure_ascii=False, encoding='utf8')
            f.write(data_to_write.encode('utf-8'))

        print("Done!")
