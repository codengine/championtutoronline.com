import os
import re
import simplejson as json
from django.core.management.base import BaseCommand
from core.library.Clock import Clock

from core.models3 import Major, UserMajor


class Command(BaseCommand):

    def handle(self, *args, **options):

        self.tutor_preferred_subjects = {}
        self.common_subjects = {}
        self.tutor_id_mapping = {}

        base_file_path = os.path.dirname(os.path.realpath(__file__))
        base_file_path = os.path.join(base_file_path, 'tutor_migrations')

        tutor_level_subjects_file_name = os.path.join(base_file_path, "tutor_preferred_level_subjects.json")

        common_subjects_file_name = os.path.join(base_file_path, "common_subjects.json")

        tutor_id_mapping_file_name = os.path.join(base_file_path, "tutor_id_mapping.json")

        tutor_preferred_subject_file = open(tutor_level_subjects_file_name, "r")
        content = tutor_preferred_subject_file.read()
        tutor_preferred_subject_file.close()

        common_subject_file = open(common_subjects_file_name, "r")
        common_subject_content = common_subject_file.read()
        common_subject_file.close()

        common_subject_json = json.loads(common_subject_content)

        for r in common_subject_json:
            self.common_subjects[r['subject_id']] = r['subject_name']

        tutor_id_mapping_file = open(tutor_id_mapping_file_name, "r")
        tutor_id_mapping_content = tutor_id_mapping_file.read()
        tutor_id_mapping_file.close()

        tutor_id_mapping_json = json.loads(tutor_id_mapping_content)

        for r in tutor_id_mapping_json:
            self.tutor_id_mapping[r['tutor_id']] = r['db_id']

        all_subjects = []

        json_data = json.loads(content)
        for record in json_data:
            if self.tutor_preferred_subjects.get(record["tutor_id"]):
                subject_name = self.common_subjects.get(record["subject_id_fk"])
                if subject_name:
                    self.tutor_preferred_subjects[record["tutor_id"]] += [ subject_name ]
                    if not subject_name in all_subjects:
                        all_subjects += [ subject_name ]
            else:
                subject_name = self.common_subjects.get(record["subject_id_fk"])
                if subject_name:
                    self.tutor_preferred_subjects[record["tutor_id"]] = [subject_name]
                    if not subject_name in all_subjects:
                        all_subjects += [subject_name]

        all_subject_objects = Major.objects.filter(name__in=all_subjects).select_related('level')

        self.subject_to_object = {}

        for major in all_subject_objects:
            self.subject_to_object[major.name] = major

        user_major_object_list = []

        now_time = Clock.utc_timestamp()
        for tutor_id, subject_list in self.tutor_preferred_subjects.items():
            db_id = self.tutor_id_mapping.get(tutor_id)
            if db_id:
                for subject_name in subject_list:
                    subject_object = self.subject_to_object.get(subject_name)
                    if subject_object:
                        if not UserMajor.objects.filter(major_id=subject_object.pk, user_id=db_id).exists():
                            user_major_object = UserMajor()
                            user_major_object.user_id = db_id
                            user_major_object.major_id = subject_object.pks
                            user_major_object.date_created = now_time
                            user_major_object.date_modified = now_time

                            user_major_object_list += [ user_major_object ]

                            print("Done!")

        UserMajor.objects.bulk_create(user_major_object_list)

        print("Done.")
