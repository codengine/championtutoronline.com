import os
import re
from django.conf import settings
import simplejson as json
import shutil
from os import listdir
from datetime import datetime
from os.path import isfile, join
import uuid

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from core.models3 import ChampUser, ProfilePicture

__author__ = 'codengine'

def email_to_id_mapper(tutor_file_name):
    base_path = os.path.dirname(os.path.realpath(__file__))
    file_path = os.path.join(base_path, 'tutor_migrations')
    file_name = os.path.join(file_path, tutor_file_name)
    tutor_file = open(file_name, "r")
    content = tutor_file.read()
    content = content.replace("'", '"')
    content = content.replace('NULL', '""')
    content = content.replace(r'12020_""', r'12020_')
    content = re.sub("\s+", " ", content)
    json_data = json.loads(content)
    tutor_file.close()

    tutor_ids = {}

    for data_record in json_data:
        tutor_ids[data_record['email']] = data_record['tutor_id']

    return tutor_ids

class Command(BaseCommand):

    def handle(self, *args, **options):
        base_file_path = os.path.dirname(os.path.realpath(__file__))
        base_file_path = os.path.join(base_file_path, 'tutor_migrations')

        champ_users = ChampUser.objects.all()

        emails = User.objects.exclude(username='codenginebd@gmail.com').values_list('username', flat=True)

        tutor_extension = base_file_path + '/image_extension.json'

        thumbnail_extension = base_file_path + '/thumbnail_extension.json'

        tutor_extension_mapping = {}

        with open(tutor_extension, 'r') as f:
            tutor_extension_mapping = f.read()

        thumbnail_extension_mapping = {}

        with open(thumbnail_extension, 'r') as f:
            thumbnail_extension_mapping = f.read()

        tutor_extension_mapping = json.loads(tutor_extension_mapping)

        thumbnail_extension_mapping = json.loads(thumbnail_extension_mapping)

        tutor_email_ids = {}

        for i in range(1, 7):
            temp_tutor_email_ids = email_to_id_mapper(tutor_file_name='tutor_%s.json' % i)
            tutor_email_ids = dict(tutor_email_ids.items() + temp_tutor_email_ids.items())

        print(tutor_extension_mapping)

        for champ_user in champ_users:
            base_file_path = os.path.dirname(os.path.realpath(__file__))
            base_file_path = os.path.join(base_file_path, 'tutor_migrations')

            tutor_profile_img_path = os.path.join(base_file_path, 'profile_img')

            tutor_id = tutor_email_ids.get(champ_user.user.username)

            if tutor_id:

                image_extension = tutor_extension_mapping.get(str(tutor_id))

                print(image_extension)

                if image_extension:

                    destination_file_name = str(uuid.uuid4()) + image_extension

                    destination_image_path = os.path.join(settings.IMAGE_DIR, destination_file_name)

                    shutil.copy2(tutor_profile_img_path + '/' + str(tutor_id) + image_extension, destination_image_path)

                    print(tutor_profile_img_path + '/' + str(tutor_id) + image_extension)
                    print(destination_image_path)
                    print("===============================================================================")

                    if champ_user.profile_picture:
                        pp = champ_user.profile_picture
                    else:
                        pp = ProfilePicture()

                    pp.image_field = settings.MEDIA_URL+"image/" + destination_file_name
                    pp.cropping = '0,17,200,183'
                    pp.save()

                    champ_user.profile_picture = pp
                    champ_user.save()
                    print(champ_user.user.username)








