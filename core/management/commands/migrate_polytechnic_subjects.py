import os
from django.core.management.base import BaseCommand
from core.models3 import SubjectLevel, Major

__author__ = 'Sohel'

class Command(BaseCommand):

    def handle(self, *args, **options):

        base_file_path = os.path.dirname(os.path.realpath(__file__))
        base_file_path = os.path.join(base_file_path, 'tutor_migrations')
        base_file_path = os.path.join(base_file_path, 'subjects')
        polytechnic_file_path = os.path.join(base_file_path, 'polytechnic_subjects.txt')

        polytechnic_level, created = SubjectLevel.objects.get_or_create(name="Polytechnic")

        polytechnic_subjects = []

        lines = []

        with open(polytechnic_file_path, 'r') as f:
            lines = f.readlines()

        for line in lines:
            line = line.strip()
            if not line in polytechnic_subjects:
                polytechnic_subjects += [ line ]

        creation_count = 0

        for psubject in polytechnic_subjects:
            print("Checking out subject %s" % ( psubject ))
            subjects = Major.objects.filter(name=psubject, level_id=polytechnic_level.pk)
            if not subjects.exists():
                print("Creating subject %s" % ( psubject ))
                polytechnic_subject = Major()
                polytechnic_subject.name = psubject
                polytechnic_subject.level_id = polytechnic_level.pk
                polytechnic_subject.creator_id = 1
                polytechnic_subject.save()
                creation_count += 1

        print("Total %s objects created" % ( str(creation_count) ))
        print("Done!")
