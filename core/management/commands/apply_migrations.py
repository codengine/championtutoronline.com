__author__ = 'codengine'

import os
from django.core.management.base import BaseCommand
from optparse import make_option
from apps import APPS


class Command(BaseCommand):

    def handle_migration(self,*args,**options):
        for app in APPS:
            cmd = "python manage.py migrate "+app
            print("Executing...")
            print(cmd)
            os.system(cmd)

    def handle(self, *args, **options):
        self.handle_migration(*args,**options)
