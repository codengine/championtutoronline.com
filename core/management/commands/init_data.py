from decimal import Decimal
from redis.client import Redis
from core.library.currency_util import CurrencyUtil
from core.library.subject_util import SubjectUtil
from core.library.tz_util import TZUtil
from payment.models import MerchantAccount, SupportedCurrencies
from rq_scheduler.scheduler import Scheduler

__author__ = 'codengine'

from django.core.management.base import BaseCommand
from random import randint
from core.models3 import *
from core.enums import UserTypes,Gender
from loremipsum import generate_paragraph
from django.conf import settings
from datetime import datetime

from django.contrib.auth.models import User
import simplejson as json
from django.core.management.base import BaseCommand
from core.enums import UserTypes
from core.models3 import Role, Country, SupportedCountry, ChampUser, TeachingExperiences, Major, SubjectLevel, UserMajor
from payment.models import Wallet
from countryinfo import countries as countryzone_info
import os

user_counter = 1

nationalities = []

schools = [
    "Bangladesh University Of Engineering and Technology",
    "Bangladesh University Of Texttiles",
    "University of Texas",
    "Harvard University"
]

majors = [
	"Mathematics","Maths A","Maths E/D","Further Math","Maths C","Math","Biology","Chemistry","Physics","Science","Geography","History",
 	"English Literature","Gmat","SAT","Hanyu Pinyin","Economics","Accounting","Art & Design","Caligraphy","Management", "Arabic",
 	"Dutch","French","German","Greek","Hindi","Italian","Japanese","Korean","Portuguese","Russian","Spanish","Thai","Vietnamese",
	"Higher Chinese","English","Chinese","Tamil","Malay","General Paper","Bio/Chem","Phy/Chem","Chinese Literature","Tamil Literature",
	"Malay Literature","Phonics","Creative Writing","Business","Information Tech","Philosophy","Psychology","Anthropology",
	"Design Tech","Environmental Sci","Music","Theatre","Visual Arts","Extended Essay","Theory of Knowledge","Information Systems",
	"Computer Sci ""Electrical Engin","Chemical Engin","Mechanical Engin", "Architecture","Marketing","Operations Mgmt","Human Resource",
	"Communications","Medicine","Biological Sci", "Social Sciences", "Law", "ASP", "C++","C#","Java","PHP","Python","VB",
	"MsSql","MySql","Oracle","Photoshop","Illustrator","AutoCAD","GIS","3D Design","Flash", "Web Design","Linus", "Mac","Solaris",
	"Windows","Office","Math","Finance","Drums", "Guitar","Music Theory","Organ","Other Instruments","Piano","Saxophone","Trumpet",
	"Voilin","Vocal Lessons","H1 Math","H3 Math","H1 Physics","H3 Physics","H1 Chemistry","H3 Chemistry","H1 Biology","H3 Biology",
	"H1 Economics","H3 Economics","Knowledge & Inquiry","AEIS ", "MCAT","Mathematics HL","Biology HL","Physics HL","Chemistry HL",
	"Phy/Bio","Social Studies","Dyslexia","Autism","Attention Deficit (ADHD)","English"
]

levels = {

}

SUPER_ADMIN_EMAIL = "codenginebd@gmail.com"
SUPER_ADMIN_USER_NAME = "codenginebd@gmail.com"
SUPER_ADMIN_PASSWORD = "awesome"
SUPER_ADMIN_FULLNAME = "Super Admin"

data = []

class Command(BaseCommand):

    subjects = {}
    subject_levels = []
    peferred_level_subjects = {}
    countries = {}
    common_levels = {}
    common_education = {}
    country_timezones = {}
    overriden_subjects = []

    def read_country_timezones(self):
        for ctz in countryzone_info:
            self.country_timezones[ctz['name']] = ctz['timezones']

    def init_missing_subjects(self):
        import os
        from core.models3 import SubjectLevel, Major
        file_path = os.path.dirname(os.path.realpath(__file__))
        file_path = os.path.join(file_path, 'tutor_migrations')
        file_path = os.path.join(file_path, 'subjects')
        file_dir1 = os.path.join(file_path, 'Malaysia')
        file_dir2 = os.path.join(file_path, 'Other')

        dir1 = os.listdir(file_dir1)
        for f in dir1:
            fpath = os.path.join(file_dir1, f)
            with open(fpath, 'r') as ff:
                content = ff.read()
                lines = content.split('\n')
                subjects = [ l.split(',')[1].strip() for l in lines ]
                level_name = f.replace('.csv', '')

                for s in subjects:
                    slevels = SubjectLevel.objects.filter(name=level_name)
                    if slevels.exists():
                        slevel = slevels.first()
                    else:
                        slevel = SubjectLevel()
                        slevel.name = s
                        slevel.save()

                    majors = Major.objects.filter(name=s)
                    if not majors.exists():
                        major = Major()
                        major.name = s
                        major.level_id = slevel.pk
                        major.creator_id = 1
                        major.save()

        dir2 = os.listdir(file_dir2)
        for f in dir2:
            fpath = os.path.join(file_dir2, f)
            with open(fpath, 'r') as ff:
                content = ff.read()
                lines = content.split('\n')
                subjects = [ l.split(',')[1].strip() for l in lines ]
                level_name = f.replace('.csv', '')

                for s in subjects:
                    slevels = SubjectLevel.objects.filter(name=level_name)
                    if slevels.exists():
                        slevel = slevels.first()
                    else:
                        slevel = SubjectLevel()
                        slevel.name = s
                        slevel.save()

                    majors = Major.objects.filter(name=s)
                    if not majors.exists():
                        major = Major()
                        major.name = s
                        major.level_id = slevel.pk
                        major.creator_id = 1
                        major.save()

    def read_subjects(self):
        file_path = os.path.dirname(os.path.realpath(__file__))
        file_path = os.path.join(file_path, 'tutor_migrations')
        file_name = os.path.join(file_path, 'common_subjects.json')
        common_subject_file = open(file_name,"r")
        content = common_subject_file.read()
        json_data = json.loads(content)
        for record in json_data:
            self.subjects[record["subject_id"]] = record["subject_name"]

    def read_country_currency_mapping(self):
        file_path = os.path.dirname(os.path.realpath(__file__))
        file_name = os.path.join(file_path, 'country-currency-codes.json')
        file_object = open(file_name,"r")
        content = file_object.read()
        json_data = json.loads(content)
        file_object.close()

        for entry in json_data:
            try:
                cname = entry["name"]
                cname2 = entry["ISO3166-1-Alpha-2"]
                currency_code = entry["currency_alphabetic_code"]
                currency_name = entry["currency_name"]

                if not SupportedCountry.objects.filter(type=SupportedCountry.__name__,name=cname,code=cname2).exists():
                    sc_ = SupportedCountry()
                    sc_.name = cname
                    sc_.code = cname2
                    sc_.type = SupportedCountry.__name__
                    sc_.save()
                else:
                    sc_ = SupportedCountry.objects.filter(type=SupportedCountry.__name__,name=cname,code=cname2).first()

                currency_object = Currency.objects.filter(name=currency_name, code=currency_code)
                if currency_object.exists():
                    currency_object = currency_object.first()
                    currency_object.country_id = sc_.pk
                    currency_object.save()
                else:
                    currency_object = Currency()
                    currency_object.name = currency_name
                    currency_object.code = currency_code
                    currency_object.country_id = sc_.pk
                    currency_object.save()
            except Exception as exp:
                pass

    def read_common_levels(self):
        file_path = os.path.dirname(os.path.realpath(__file__))
        file_path = os.path.join(file_path, 'tutor_migrations')
        file_name = os.path.join(file_path, 'common_level.json')
        common_subject_file = open(file_name,"r")
        content = common_subject_file.read()
        json_data = json.loads(content)
        for record in json_data:
            self.common_levels[record["level_id"]] = record["level_name"]

    def read_subject_levels(self):
        file_path = os.path.dirname(os.path.realpath(__file__))
        file_path = os.path.join(file_path, 'tutor_migrations')
        file_name = os.path.join(file_path, 'common_level_subject.json')
        common_subject_file = open(file_name,"r")
        content = common_subject_file.read()
        json_data = json.loads(content)
        for record in json_data:
            self.subject_levels += [ { "subject": record["subject_id_fk"], "level": record["level_id_fk"] } ]


    def read_preferred_level_subjects(self):
        file_path = os.path.dirname(os.path.realpath(__file__))
        file_path = os.path.join(file_path, 'tutor_migrations')
        file_name = os.path.join(file_path, 'preferred_level_subject.json')
        common_subject_file = open(file_name,"r")
        content = common_subject_file.read()
        json_data = json.loads(content)
        for record in json_data:
            if not self.peferred_level_subjects.get(record["tutor_id"]):
                self.peferred_level_subjects[record["tutor_id"]] = [{
                    "level_id": record["level_id_fk"],
                    "subject_id": record["subject_id_fk"]
                }]
            else:
                self.peferred_level_subjects[record["tutor_id"]] += [{
                    "level_id": record["level_id_fk"],
                    "subject_id": record["subject_id_fk"]
                }]

    def read_common_education(self):
        file_path = os.path.dirname(os.path.realpath(__file__))
        file_path = os.path.join(file_path, 'tutor_migrations')
        file_name = os.path.join(file_path, 'common_education.json')
        common_subject_file = open(file_name,"r")
        content = common_subject_file.read()
        json_data = json.loads(content)
        for record in json_data:
            self.common_education[record["educational_id"]] = record["educational_name"]

    def read_countries(self):
        file_path = os.path.dirname(os.path.realpath(__file__))
        file_path = os.path.join(file_path, 'tutor_migrations')
        file_name = os.path.join(file_path, 'countries.json')
        common_subject_file = open(file_name,"r")
        content = common_subject_file.read()
        json_data = json.loads(content)
        for record in json_data:
            cid = record["id"]
            name = record["name"]
            alpha2 = record["alpha_2"]
            alpha3 = record["alpha_3"]
            if not Country.objects.filter(name=name,code=alpha2).exists():
                country_object = Country()
                country_object.name = name
                country_object.code = alpha2
                country_object.save()

            if not SupportedCountry.objects.filter(type=SupportedCountry.__name__,name=name,code=alpha2).exists():
                sc_ = SupportedCountry()
                sc_.name = name
                sc_.code = alpha2
                sc_.type = SupportedCountry.__name__
                sc_.save()
                country_object = sc_
            else:
                country_object = SupportedCountry.objects.filter(type=SupportedCountry.__name__,name=name,code=alpha2).first()
            self.countries[cid] = country_object

    def write_all_levels(self):
        for country_name, levels in SubjectUtil.get_all_levels().items():
            for level_name in levels:
                level_object = SubjectLevel.objects.filter(name=level_name)
                if not level_object.exists():
                    level_object = SubjectLevel()
                    level_object.name = level_name
                    level_object.save()

    def write_subject_level_names(self):
        self.subjects_names = []
        for entry in self.subject_levels:
            subject_id = entry['subject']
            level_id = entry['level']
            subject_name = self.subjects[subject_id]
            level_name = self.common_levels[level_id]
            self.subjects_names += [
                {
                    "subject": subject_name,
                    "level": level_name
                }
            ]
            level_object = SubjectLevel.objects.filter(name=level_name)
            if not level_object.exists():
                level_object = SubjectLevel()
                level_object.name = level_name
                level_object.save()
            else:
                level_object = level_object.first()

            if not Major.objects.filter(name=subject_name,level_id=level_object.pk).exists():
                major_object = Major()
                major_object.name = subject_name
                major_object.level = level_object
                major_object.creator_id = 1
                major_object.save()


    def create_tutor(self, email, username, nationality,name,last_name,current_school,major,gender,timezone,experience,subject_levels):
        ###create teacher
        try:
            tutor = User.objects.create_user(email=email, username=email, password="123456")
        except Exception as msg:
            tutor = User.objects.get(username=email)
        #nationality = nationalities[randint(0,len(nationalities) - 1)]
        print("Creating tutor...")
        if ChampUser.objects.filter(user_id=tutor.pk).exists():
            wallet = ChampUser.objects.filter(user_id=tutor.pk).first().wallet
        else:
            wallet = Wallet()
            wallet.save()
        champ_users = ChampUser.objects.filter(user=tutor,fullname=name, type=self.teacher_role,wallet_id=wallet.pk)
        if champ_users.exists():
            champ_user = champ_users.first()
        else:
            champ_user = ChampUser()
            champ_user.user = tutor
        champ_user.fullname = name
        champ_user.lastname = last_name
        champ_user.type = self.teacher_role
        champ_user.wallet_id = wallet.pk
        champ_user.current_school = current_school
        champ_user.major = major
        champ_user.gender = gender.lower()
        champ_user.nationality_id = nationality.pk
        champ_user.timezone = timezone
        champ_user.save()
        print("Tutor created.")

        print("Adding Majors")

        for subject_level in subject_levels:
            subject = subject_level["subject"]
            level = subject_level["level"]

            level_object = SubjectLevel.objects.filter(name=level)
            if level_object.exists():
                level_object = level_object.first()
            else:
                level_object = SubjectLevel()
                level_object.name = level
                level_object.save()

            if not Major.objects.filter(name=subject,level=level_object).exists():
                major_object = Major()
                major_object.name = subject
                major_object.level = level_object
                major_object.creator = tutor
                major_object.save()
            else:
                major_object = Major.objects.filter(name=subject, level=level_object).first()


            # if not UserMajor.objects.filter(user=tutor,major=major_object).exists():
            #     user_major_object = UserMajor()
            #     user_major_object.user = tutor
            #     user_major_object.major = major_object
            #     user_major_object.save()


        if TeachingExperiences.objects.filter(user=tutor).exists():
            teaching_experiences = TeachingExperiences.objects.filter(user=tutor).first()
        else:
            teaching_experiences = TeachingExperiences()
        teaching_experiences.experience = experience
        teaching_experiences.user = tutor
        teaching_experiences.save()

    def get_timezone_from_country_name(self, country_name, include_class=False):
        try:
            return self.country_timezones[country_name][0]
        except:
            return ""

    def migrate_tutor(self):
        file_path = os.path.dirname(os.path.realpath(__file__))
        file_path = os.path.join(file_path, 'tutor_migrations')
        file_name = os.path.join(file_path, 'tutor.json')
        tutor_file = open(file_name,"r")
        content = tutor_file.read()
        json_data = json.loads(content)
        tutor_file.close()

        for record in json_data:
            email = record["email"]
            password = "123456"
            name = record["name"]
            last_name = record["lname"]
            dob = record["dob"]
            address = record["address"]
            tutor_experience = record["tutor_experience"]
            university = record["university"]
            country = record["nationality_id_fk"]
            education_id = record["educational_id_fk"]
            gender = record["gender"]
            university_course = record["university_course"]
            if country:
                country_object = self.countries.get(country)
                timezone = ""
            else:
                country_name = "Singapore"
                country_code = "sg"
                if not Country.objects.filter(name=country_name,code=country_code).exists():
                    country_object = Country()
                    country_object.name = country_name
                    country_object.code = country_code
                    country_object.save()

                if not SupportedCountry.objects.filter(type=SupportedCountry.__name__,name=country_name,code=country_code).exists():
                    sc_ = SupportedCountry()
                    sc_.name = country_name
                    sc_.code = country_code
                    sc_.type = SupportedCountry.__name__
                    sc_.save()
                    country_object = sc_
                else:
                    country_object = SupportedCountry.objects.filter(type=SupportedCountry.__name__,name=country_name,code=country_code).first()

            current_school = self.common_education.get(education_id)
            if not current_school:
                current_school = ""

            s_l_list = []
            psubject_levels = self.peferred_level_subjects.get(record["tutor_id"])
            if psubject_levels:
                for s_l in psubject_levels:
                    subject_name = self.subjects.get(s_l["subject_id"])
                    level_name = self.common_levels.get(s_l["level_id"])
                    s_l_list += [
                        {
                            "subject": subject_name,
                            "level": level_name
                        }
                    ]

            try:
                self.create_tutor(email, email, country_object,name,last_name,current_school,university_course,gender,self.get_timezone_from_country_name(country_object.name),tutor_experience,s_l_list)
            except:
                print("Exception Found! Skipping...")

    def handle(self, *args, **options):
        print("Load all migration data of tutors")
        self.read_subjects()
        self.read_common_levels()
        self.read_subject_levels()
        self.read_preferred_level_subjects()
        self.read_countries()
        self.read_common_education()
        self.read_country_timezones()
        self.write_all_levels()
        self.write_subject_level_names()
        self.init_missing_subjects()

        role_teacher,created = Role.objects.get_or_create(name=UserTypes.teacher.value)
        self.teacher_role = role_teacher
        print("Data loaded.")
        
        print("Creating roles...")
        role_teacher,created = Role.objects.get_or_create(name=UserTypes.teacher.value)
        role_student,created = Role.objects.get_or_create(name=UserTypes.student.value)
        role_stuff,created = Role.objects.get_or_create(name=UserTypes.stuff.value)
        print("Roles created.")
        roles = [
            role_teacher,
            role_student
        ]

        print("Creating merchant accounts")
        for currency, item in settings.BRAINTREE_MERCHANTS.items():
            if currency != "DEFAULT":
                if MerchantAccount.objects.filter(braintree_user_id=item["ID"],mode="SANDBOX").exists():
                    merchant_account = MerchantAccount.objects.filter(braintree_user_id=item["ID"],mode="SANDBOX").first()
                else:
                    merchant_account = MerchantAccount()
                merchant_account.braintree_user_id = item["ID"]
                merchant_account.balance = Decimal(0)
                merchant_account.currency = currency
                merchant_account.mode = "SANDBOX"
                merchant_account.merchant_id = item["SANDBOX"]["MERCHANT_ID"]
                merchant_account.public_key = item["SANDBOX"]["PUBLIC_KEY"]
                merchant_account.private_key = item["SANDBOX"]["PRIVATE_KEY"]
                merchant_account.save()

                if MerchantAccount.objects.filter(braintree_user_id=item["ID"],mode="LIVE").exists():
                    merchant_account = MerchantAccount.objects.filter(braintree_user_id=item["ID"],mode="LIVE").first()
                else:
                    merchant_account = MerchantAccount()
                merchant_account.braintree_user_id = item["ID"]
                merchant_account.balance = Decimal(0)
                merchant_account.currency = currency
                merchant_account.mode = "LIVE"
                merchant_account.merchant_id = item["LIVE"]["MERCHANT_ID"]
                merchant_account.public_key = item["LIVE"]["PUBLIC_KEY"]
                merchant_account.private_key = item["LIVE"]["PRIVATE_KEY"]
                merchant_account.save()

        print("Merchant accounts created.")

        print("Creating Super User")
        ###create teacher
        try:
            admin_user = User.objects.create_user(SUPER_ADMIN_USER_NAME, SUPER_ADMIN_EMAIL,SUPER_ADMIN_PASSWORD)
            admin_user.is_staff = True
            admin_user.save()
        except Exception as msg:
            admin_user = User.objects.get(username=SUPER_ADMIN_USER_NAME)
        if ChampUser.objects.filter(user_id=admin_user.pk).exists():
            wallet = ChampUser.objects.filter(user_id=admin_user.pk).first().wallet
        else:
            wallet = Wallet()
            wallet.save()
        champ_user,created = ChampUser.objects.get_or_create(user=admin_user,fullname=SUPER_ADMIN_FULLNAME, type= role_stuff, wallet_id=wallet.pk)
        champ_user.save()
        print("Super User Created Successfully")

        countries = []
        for k, v in self.countries.items():
            countries += [ v ]

        # file = open("country_codes.txt","r")
        # lines = file.readlines()
        # file.close()

        ###Find the timezone to country mapper and update
        TZUtil.update_timezone_country_mapper()
                
        genders = [
            Gender.male.value,
            Gender.female.value
        ]

        print("Populate languages...")
        file = open("language_list.txt","r")
        lines = file.readlines()
        for line in lines:
            l_split = line.split(',')
            #print(l_split)
            name = l_split[0].strip()
            code = l_split[1].strip()
            Language.objects.get_or_create(name=name,code=code)
        file.close()

        print("Initializing currencies...")
        file = open("currency_list.txt","r")
        clines = file.readlines()
        for line in clines:
            line_split = line.strip().split(',')
            c_val = line_split[0].strip()
            c_label = line_split[1].strip()
            if not Currency.objects.filter(name=c_label,code=c_val).exists():
                currency_object = Currency()
                currency_object.name = c_label
                currency_object.code = c_val
                currency_object.save()
        file.close()

        print("Add Supported Currencies")
        for currency,label in settings.SUPPORTED_CURRENCIES:
            SupportedCurrencies.objects.get_or_create(code=currency,name=label)
        print("Added Supported Currencies")

        print("Adding Supported Languages")
        supported_languages = [ ("EN","English"),("MS","Malay"), ("HI","Hindi"),("TA","Tamil")]
        for code, name in supported_languages:
            if not SupportedLanguage.objects.filter(code=code, name=name).exists():
                s_l = SupportedLanguage()
                s_l.code = code
                s_l.name = name
                s_l.save()
        print("Added Supported Languages")
        scheduler = Scheduler(connection=Redis())
        scheduler.schedule(
            scheduled_time=datetime.utcnow(), # Time for first execution, in UTC timezone
            func=CurrencyUtil.fetch_rate_list,                     # Function to be queued
            args=[],             # Arguments passed into function when executed
            kwargs={},         # Keyword arguments passed into function when executed
            interval=2*60*60,                   # Time before the function is called again, in seconds
            repeat=None                      # Repeat this number of times (None means repeat forever)
        )

        print("Add Security questions")
        questions = [
            "What is the first and last name of your first boyfriend or girlfriend?",
            "Which phone number do you remember most from your childhood?",
            "What was your favourite place to visit as a child?",
            "Who is your favorite actor, musician, or artist?",
            "What is the name of your favourite pet?",
            "In which city were you born in?",
            "What is the name of your attended high school?",
            "What is the name of your first school?",
            "What is your favourite movie?",
            "What is your mother's maiden name?",
            "What was the make of your first car?",
            "When is your anniversary?",
            "What is your favourite color?",
            "What is your father's middle name?",
            "What is the name of your first grade teacher?",
            "What was your high school mascot?",
            "Which is your favourite web browser?"
        ]

        for question in questions:
            if not SecurityQuestion.objects.filter(question=question).exists():
                sq = SecurityQuestion()
                sq.question = question
                sq.created_by_id= 1
                sq.save()

        timezones = [ (obj.tz, obj.tz_offset) for obj in TimezoneMapper.objects.all() ]
        countries = [ c for c in SupportedCountry.objects.filter(type=SupportedCountry.__name__) ]

        print("Adding currency code mapper for country")
        self.read_country_currency_mapping()
        print("Currency code mapper added for countries")

        print("Start tutor migration")
        self.migrate_tutor()
        print("Ended tutor migration")

        for i in range(5000):
            ###create teacher
            try:
                tutor = User.objects.create_user("t"+str(i + 1)+"@gmail.com", "t"+str(i + 1)+"@gmail.com","123")
            except Exception as msg:
                tutor = User.objects.get(username="t"+str(i + 1)+"@gmail.com")
            #nationality = nationalities[randint(0,len(nationalities) - 1)]
            role = role_teacher
            cindex = randint(0,len(countries))
            if cindex == len(countries):
                cindex = len(countries) - 1
            nationality = countries[cindex]

            print("Creating tutor...")
            if ChampUser.objects.filter(user_id=tutor.pk).exists():
                wallet = ChampUser.objects.filter(user_id=tutor.pk).first().wallet
            else:
                wallet = Wallet()
                wallet.save()
            champ_user,created = ChampUser.objects.get_or_create(user=tutor,fullname="Mr Test Tutor "+str(i + 1), lastname="Tutor "+str(i + 1), type=role,wallet_id=wallet.pk)
            champ_user.current_school = schools[randint(0,len(schools) - 1)]
            champ_user.major = majors[randint(0,len(majors) - 1)]
            champ_user.gender = genders[randint(0,len(genders) - 1)]
            champ_user.nationality_id = nationality.pk
            champ_user.timezone = timezones[randint(0,len(timezones) - 1)][0]
            champ_user.save()
            print("Tutor created.")
            
            print("Adding Majors")
            major_list = []
            major_size = randint(5,len(majors) - 1)
            while len(major_list) < major_size:
                random_major = majors[randint(0,len(majors) - 1)]
                if not random_major in major_list:
                    major_list += [random_major]
                else:
                    random_major = majors[randint(0,len(majors) - 1)]

            # for major in major_list:
            #     if not Major.objects.filter(name=major).exists():
            #         major_object = Major()
            #         major_object.name = major
            #         major_object.creator = tutor
            #         major_object.save()
            #
            #     user_major_object = UserMajor()
            #     user_major_object.user = tutor
            #     user_major_object.major = Major.objects.filter(name=major).first()
            #     user_major_object.save()

            sentences_count, words_count, paragraph = generate_paragraph()
            teaching_experiences = TeachingExperiences()
            teaching_experiences.experience = paragraph
            teaching_experiences.user = tutor
            teaching_experiences.save()

            sentences_count, words_count, paragraph = generate_paragraph()
            extracurricular_interest = ExtraCurricularInterest()
            extracurricular_interest.user = tutor
            extracurricular_interest.interest = paragraph
            extracurricular_interest.save()

            ###create teacher
            try:
                student = User.objects.create_user("s"+str(i + 1)+"@gmail.com", "s"+str(i + 1)+"@gmail.com","123")
            except Exception as msg:
                student = User.objects.get(username="s"+str(i + 1)+"@gmail.com")
            # nationality = nationalities[randint(0,len(nationalities) - 1)]
            role = role_student
            print("Creating student...")
            if ChampUser.objects.filter(user_id=student.pk).exists():
                wallet = ChampUser.objects.filter(user_id=student.pk).first().wallet
            else:
                wallet = Wallet()
                wallet.save()

            cindex = randint(0,len(countries))
            if cindex == len(countries):
                cindex = len(countries) - 1
            nationality = countries[cindex]

            champ_user,created = ChampUser.objects.get_or_create(user=student,fullname="Mr Test Student "+str(i + 1), lastname="Student "+str(i + 1), type= role, wallet_id=wallet.pk)
            champ_user.current_school = schools[randint(0,len(schools) - 1)]
            champ_user.major = majors[randint(0,len(majors) - 1)]
            champ_user.gender = genders[randint(0,len(genders) - 1)]
            champ_user.nationality_id = nationality.pk
            champ_user.timezone = timezones[randint(0,len(timezones) - 1)][0]

            champ_user.save()
            print("Student created.")

        print("Init done...")





