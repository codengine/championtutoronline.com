import os
import json
import re
import uuid
import shutil
from django.conf import settings
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from core.models3 import TutorCertificates

__author__ = 'Sohel'

class Command(BaseCommand):

    def extract_tutor_id(self, file_name):
        n_chars = [ str(i) for i in range(10) ]
        if file_name and file_name[0] in n_chars:
            proceed = True
            temp_id = ''
            while proceed:
                if file_name[0] in n_chars:
                    temp_id += file_name[0]
                    file_name = file_name[1:]
                else:
                    proceed = False

            if temp_id:
                return int(temp_id)

    def copy_certificate_file(self, source_file_path, file_extension):
        try:
            destination_file_name = str(uuid.uuid4()) + file_extension if file_extension.startswith(".") else "." + file_extension

            destination_image_path = os.path.join(settings.CERTIFICATES_DIR, destination_file_name)

            shutil.copy2(source_file_path, destination_image_path)

            return destination_file_name
        except Exception as exp:
            pass

    def find_file_extension(self, file_name):
        try:
            file_extension = file_name[file_name.rindex('.'):]
            return file_extension
        except Exception as exp:
            pass

    def generate_tutor_id_mapping(self):
        file_path = os.path.dirname(os.path.realpath(__file__))
        file_path = os.path.join(file_path, 'tutor_migrations')

        tutor_ids = {}

        emails = []

        email_id_mapping = {}

        duplicate_emails = []

        base_path = os.path.dirname(os.path.realpath(__file__))
        tutor_list_file_path = os.path.join(base_path, 'tutor_migrations')
        tutor_list_file_path = os.path.join(file_path, 'tutor_list.csv')

        tutor_list_contents = []
        with open(tutor_list_file_path, 'r') as f:
            tutor_list_contents = f.readlines()

        tutor_list_ids = [ int(line.split(',')[0]) for line in tutor_list_contents[ 1: ] ]

        for i in range(1, 7):
            tutor_file_name='tutor_%s.json' % i

            base_path = os.path.dirname(os.path.realpath(__file__))
            file_path = os.path.join(base_path, 'tutor_migrations')
            file_name = os.path.join(file_path, tutor_file_name)
            tutor_file = open(file_name,"r")
            content = tutor_file.read()
            content = content.replace("'", '"')
            content = content.replace('NULL', '""')
            content = content.replace(r'12020_""', r'12020_')
            content = re.sub("\s+", " ", content)
            json_data = json.loads(content)
            tutor_file.close()

            for data_record in json_data:
                if data_record['email'] in emails:
                    duplicate_emails += [ data_record['email'] ]
                else:
                    emails += [ data_record['email'] ]
                    email_id_mapping[data_record['email']] = data_record['tutor_id']

        print(len(emails))
        user_instances = User.objects.filter(username__in=emails)
        print(user_instances)
        for user_instance in user_instances:
            db_id = user_instance.pk
            tutor_id = email_id_mapping[user_instance.username]
            if tutor_id:
                tutor_ids[int(tutor_id)] = db_id

        return tutor_ids

    def read_tutor_certificates(self, tutor_id_mapping):

        file_path = os.path.dirname(os.path.realpath(__file__))
        file_path = os.path.join(file_path, 'tutor_migrations')
        file_name = os.path.join(file_path, 'tutor_certificates.csv')

        contents = []
        with open(file_name, 'r') as f:
            contents = f.readlines()

        # "tutor_certificate_id","tutor_id","type","year","school","certificate","upload_date","show_cert"

        tutor_certificates = {}

        for index, line in enumerate(contents):
            if index == 0:
                continue
            line_split = line.split(",")
            tutor_certificate_id = line_split[0].replace('"', "")
            tutor_id = line_split[1].replace('"', "")
            type = line_split[2].replace('"', "")
            year = line_split[3].replace('"', "")
            school = line_split[4].replace('"', "")
            certificate = line_split[5].replace('"', "")
            upload_date = line_split[6].replace('"', "")
            show_cert = line_split[7].replace('"', "")



            tutor_db_id = tutor_id_mapping.get(int(tutor_id))
            if tutor_db_id:
                if not tutor_certificates.get(tutor_db_id):
                    tutor_certificates[tutor_db_id] = [
                        {
                            "tutor_certificate_id" : line_split[0].replace('"', ""),
                            "type" : line_split[2].replace('"', ""),
                            "year" : line_split[3].replace('"', ""),
                            "school" : line_split[4].replace('"', ""),
                            "certificate" : line_split[5].replace('"', ""),
                            "upload_date" : line_split[6].replace('"', ""),
                            "show_cert" : line_split[7].replace('"', "")
                        }
                    ]
                else:
                    tutor_certificates[tutor_db_id] += [
                        {
                            "tutor_certificate_id" : line_split[0].replace('"', ""),
                            "type" : line_split[2].replace('"', ""),
                            "year" : line_split[3].replace('"', ""),
                            "school" : line_split[4].replace('"', ""),
                            "certificate" : line_split[5].replace('"', ""),
                            "upload_date" : line_split[6].replace('"', ""),
                            "show_cert" : line_split[7].replace('"', "")
                        }
                    ]

        return tutor_certificates
                

    def handle(self, *args, **options):

        certificates_mapping = {}

        certificates_dir = '/media/sohel/UBUNTU 16_0/Projects/certs_tutor'

        all_files = os.listdir(certificates_dir)

        for file_name in all_files:
            tutor_id = self.extract_tutor_id(file_name)
            if tutor_id:
                if not tutor_id in certificates_mapping.keys():
                    certificates_mapping[tutor_id] = [
                        file_name
                    ]
                else:
                    certificates_mapping[tutor_id] += [
                        file_name
                    ]

        tutor_id_mapping = self.generate_tutor_id_mapping()

        tutor_certificates_entries = self.read_tutor_certificates(tutor_id_mapping)


        for tutor_db_id, entries in tutor_certificates_entries.items():
            print("Executing tutor id: %s" % tutor_db_id)
            print("Certificates record found: %s" % len(entries))
            for record in entries:

                cert_file = record['certificate']

                file_extension = self.find_file_extension(cert_file)

                if file_extension:

                    source_file_path = os.path.join(certificates_dir, cert_file)

                    destination_file_name = self.copy_certificate_file(source_file_path, file_extension)

                    if destination_file_name:

                        tutor_cert_instances = TutorCertificates.objects.filter(type=record['type'],year=int(record['year']),
                                                                                school=record['school'],original_name=cert_file)

                        if tutor_cert_instances.exists():
                            tutor_cert_instance = tutor_cert_instances.first()
                        else:
                            tutor_cert_instance = TutorCertificates()
                        tutor_cert_instance.type = record['type']
                        tutor_cert_instance.year = int(record['year'])
                        tutor_cert_instance.school = record['school']
                        tutor_cert_instance.name = destination_file_name
                        tutor_cert_instance.original_name = cert_file
                        tutor_cert_instance.user_id = tutor_db_id
                        tutor_cert_instance.save()
        print("Done!")
