import os
import re
import simplejson as json
from django.core.management.base import BaseCommand
from core.models3 import Education

__author__ = 'Sohel'

class TutorSchoolMigrator(object):

    @classmethod
    def handle_university_info(cls, record):
        university = record['university']
        university_course = record['university_course']
        university_edu_result = record['university_edu_result']
        univ_start_year = record['univ_start_year']
        univ_end_year = record['univ_end_year']
        univ_current = record['univ_current']
        degree_type = record['degree_type']

    @classmethod
    def handle_university2_info(cls, record):
        university = record['university2']
        university_course = record['university_course2']
        university_edu_result = record['university_edu_result2']
        univ_start_year = record['univ_start_year2']
        univ_end_year = record['univ_end_year2']
        univ_current = record['univ_current2']
        degree_type = record['degree_type2']

    @classmethod
    def handle_university3_info(cls, record):
        university = record['university3']
        university_course = record['university_course3']
        university_edu_result = record['university_edu_result3']
        univ_start_year = record['univ_start_year3']
        univ_end_year = record['univ_end_year3']
        univ_current = record['univ_current3']
        degree_type = record['degree_type3']

    @classmethod
    def handle_polytechnic_info(cls, record):
        polytechnic = record['polytechnic']
        polytechnic_course = record['polytechnic_course']
        polytechnic_edu_result = record['polytechnic_edu_result']
        poly_start_year = record['poly_start_year']
        poly_end_year = record['poly_end_year']
        poly_current = record['poly_current']

    @classmethod
    def handle_ib_school_info(cls, record):
        ib_school_id_fk = record['ib_school_id_fk']
        ib_start_year = record['ib_start_year']
        ib_end_year = record['ib_end_year']
        ib_current = record['ib_current']
        ib_subject_grade = record['ib_subject_grade']
        ib_other_subject_grade = record['ib_other_subject_grade']


    @classmethod
    def handle_aschool_info(cls, record):
        a_school_id_fk = record['a_school_id_fk']
        a_start_year = record['a_start_year']
        a_end_year = record['a_end_year']
        a_current = record['a_current']
        a_subject_grade = record['a_subject_grade']
        a_spaper_h3paper = record['a_spaper_h3paper']


    @classmethod
    def handle_oschool_info(cls, record):
        o_school_id_fk = record['o_school_id_fk']
        o_start_year = record['o_start_year']
        o_end_year = record['o_end_year']
        o_current = record['o_current']
        o_subject_grade = record['o_subject_grade']
        o_other_subject_grade = record['o_other_subject_grade']

    @classmethod
    def migrate_schools(cls):

        cls.school_objects = []

        base_path = os.path.dirname(os.path.realpath(__file__))
        file_path = os.path.join(base_path, 'tutor_migrations')

        tutor_list_file_path = os.path.join(base_path, 'tutor_migrations')
        tutor_list_file_path = os.path.join(file_path, 'tutor_list.csv')

        tutor_list_contents = []
        with open(tutor_list_file_path, 'r') as f:
            tutor_list_contents = f.readlines()

        tutor_list_ids = [ int(line.split(',')[0]) for line in tutor_list_contents[ 1: ] ]

        for i in range(1, 7):
            file_name = os.path.join(file_path, 'tutor_%s.json' % i)
            tutor_file = open(file_name,"r")
            content = tutor_file.read()
            content = content.replace("'", '"')
            content = content.replace('NULL', '""')
            content = content.replace(r'12020_""', r'12020_')
            content = re.sub("\s+", " ", content)
            json_data = json.loads(content)
            tutor_file.close()

            for record in json_data:
                if int(record['tutor_id']) in tutor_list_ids:
                    cls.handle_university_info()
                    cls.handle_university2_info()
                    cls.handle_university3_info()
                    cls.handle_polytechnic_info()
                    cls.handle_ib_school_info()
                    cls.handle_aschool_info()
                    cls.handle_oschool_info()

        try:
            Education.objects.bulk_create(cls.school_objects)
            print("Done!")
        except Exception as exp:
            pass

class Command(BaseCommand):

    def handle(self, *args, **options):
        TutorSchoolMigrator.migrate_schools()