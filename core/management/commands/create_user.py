from core.library.Clock import Clock

__author__ = 'codengine'

from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from random import randint
from core.models3 import *
from core.enums import UserTypes,VerificationStatus,ChatType,ReadStatus,OnlineStatus,TransactionStatus,ActiveStatus,LoationType,Gender
from loremipsum import generate_paragraph

user_counter = 1

nationalities = []

schools = [
    "Bangladesh University Of Engineering and Technology",
    "Bangladesh University Of Texttiles",
    "University of Texas",
    "Harvard University"
]

majors = [
    "Mathematics","Computer Science", "Management", "Finance", "Economics", "English","Bangla","Geography",
    "Accounting","Political Science","Sociology","Discrete Mathematics"
]

data = []

class Command(BaseCommand):
    def handle(self, *args, **options):
        print "Creating roles..."
        role_teacher,created = Role.objects.get_or_create(name=UserTypes.teacher.value)
        role_student,created = Role.objects.get_or_create(name=UserTypes.student.value)
        print "Roles created."
        roles = [
            role_teacher,
            role_student
        ]

        countries = []

        file = open("country_codes.txt","r")
        lines = file.readlines()
        file.close()

        nationalities = [[data.strip() for data in line.strip().split(',')] for line in lines]

        for c in nationalities:
            if not Country.objects.filter(name=c[1],code=[0]).exists():
                country_object = Country()
                country_object.name = c[1]
                country_object.code = c[0]
                country_object.save()
                countries += [country_object]
            else:
                countries += [Country.objects.get(name=c)]
        genders = [
            Gender.male.value,
            Gender.female.value
        ]

        print("Populate languages...")
        file = open("language_list.txt","r")
        lines = file.readlines()
        for line in lines:
            Language.objects.get_or_create(name=line.strip())
        file.close()

        print("Initializing currencies...")
        file = open("currency_list.txt","r")
        clines = file.readlines()
        for line in clines:
            line_split = line.strip().split(',')
            c_val = line_split[0].strip()
            c_label = line_split[1].strip()
            if not Currency.objects.filter(name=c_label,code=c_val).exists():
                currency_object = Currency()
                currency_object.name = c_label
                currency_object.code = c_val
                currency_object.save()
        file.close()

        timezones = Clock.get_all_timezones()

        for i in range(10):
            ###create teacher
            try:
                tutor = User.objects.create_user("t"+str(i + 1)+"@gmail.com", "t"+str(i + 1)+"@gmail.com","123")
            except Exception,msg:
                tutor = User.objects.get(username="t"+str(i + 1)+"@gmail.com")
            #nationality = nationalities[randint(0,len(nationalities) - 1)]
            role = role_teacher
            nationality = countries[randint(0,len(countries) - 1)]

            print "Creating tutor..."
            champ_user,created = ChampUser.objects.get_or_create(user=tutor,fullname="Mr Test Tutor "+str(i + 1), type=role)
            champ_user.current_school = schools[randint(0,len(schools) - 1)]
            champ_user.major = majors[randint(0,len(majors) - 1)]
            champ_user.gender = genders[randint(0,len(genders) - 1)]
            champ_user.nationality = nationality
            champ_user.timezone = timezones[randint(0,len(timezones) - 1)][0]
            champ_user.save()
            print "Tutor created."

            print("Adding Majors")
            major_list = []
            major_size = randint(5,len(majors) - 1)
            while len(major_list) < major_size:
                random_major = majors[randint(0,len(majors) - 1)]
                if not random_major in major_list:
                    major_list += [random_major]
                else:
                    random_major = majors[randint(0,len(majors) - 1)]

            for major in major_list:
                major_object = Major()
                major_object.name = major
                major_object.creator = tutor
                major_object.save()

                user_major_object = UserMajor()
                user_major_object.user = tutor
                user_major_object.major = major_object
                user_major_object.save()

            sentences_count, words_count, paragraph = generate_paragraph()
            teaching_experiences = TeachingExperiences()
            teaching_experiences.experience = paragraph
            teaching_experiences.user = tutor
            teaching_experiences.save()

            sentences_count, words_count, paragraph = generate_paragraph()
            extracurricular_interest = ExtraCurricularInterest()
            extracurricular_interest.user = tutor
            extracurricular_interest.interest = paragraph
            extracurricular_interest.save()

            ###create teacher
            try:
                student = User.objects.create_user("s"+str(i + 1)+"@gmail.com", "s"+str(i + 1)+"@gmail.com","123")
            except Exception,msg:
                student = User.objects.get(username="s"+str(i + 1)+"@gmail.com")
            nationality = nationalities[randint(0,len(nationalities) - 1)]
            role = role_student
            print "Creating student..."
            champ_user,created = ChampUser.objects.get_or_create(user=student,fullname="Mr Test Student "+str(i + 1), type= role)
            champ_user.current_school = schools[randint(0,len(schools) - 1)]
            champ_user.major = majors[randint(0,len(majors) - 1)]
            champ_user.gender = genders[randint(0,len(genders) - 1)]
            champ_user.timezone = timezones[randint(0,len(timezones) - 1)][0]
            champ_user.save()
            print "Student created."

        print "Init done..."





