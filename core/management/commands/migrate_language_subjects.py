import os
from django.core.management.base import BaseCommand
from core.models3 import SubjectLevel, Major

__author__ = 'Sohel'

class Command(BaseCommand):

    def handle(self, *args, **options):

        base_file_path = os.path.dirname(os.path.realpath(__file__))
        base_file_path = os.path.join(base_file_path, 'tutor_migrations')
        base_file_path = os.path.join(base_file_path, 'subjects')
        language_file_path = os.path.join(base_file_path, 'language_subjects.txt')

        language_level, created = SubjectLevel.objects.get_or_create(name="Language")

        language_subjects = []

        lines = []

        with open(language_file_path, 'r') as f:
            lines = f.readlines()

        for line in lines:
            line = line.strip()
            if not line in language_subjects:
                language_subjects += [ line ]

        creation_count = 0

        for psubject in language_subjects:
            print("Checking out subject %s" % ( psubject ))
            subjects = Major.objects.filter(name=psubject, level_id=language_level.pk)
            if not subjects.exists():
                print("Creating subject %s" % ( psubject ))
                polytechnic_subject = Major()
                polytechnic_subject.name = psubject
                polytechnic_subject.level_id = language_level.pk
                polytechnic_subject.creator_id = 1
                polytechnic_subject.save()
                creation_count += 1

        print("Total %s objects created" % ( str(creation_count) ))
        print("Done!")
