from core.models3 import *


class Wishlist(GenericModel):
    type = models.CharField(max_length=200)
    comments = models.TextField()
    created_by = models.ForeignKey(User, related_name='wishlist_created_by')

    class Meta:
        db_table = 'wishlist'