$(document).ready(function(){
    //$('.btn').button();
    $("#id_post_lesson_type_online_label").click(function(e){
        //e.preventDefault();
        $(this).removeClass("active");
        $("#id_post_lesson_type_written_label").addClass("active");
        $("#post_lesson_type_online").prop("checked",true);
        return false;
    });

    $(document).on("click", ".champ_cross", function(e) {
        $(this).parent().remove();
    });

    $("#id_post_lesson_type_written_label").click(function(e){
        e.preventDefault();
        $(this).removeClass("active");
        $("#id_post_lesson_type_online_label").addClass("active");
        $("#post_lesson_type_written").prop("checked",true);
        return false;
    });

    $("#id_subject").select2({ width: "430px" }).on("change", function (e) {
        var value = $(this).val();
        var text = $(this).select2('data')[0].text
        var value_found = false;
        $(this).parent().find(".block_tags").find("input").each(function(e){
            if($(this).val() == value){
                value_found = true;
                return;
            }
        });

        if(!value_found){
            var _value = text;
            //console.log(value);
            if($(this).hasClass("nationality")) {
                _value = $(this).val();
            }
//            var tag_element = "<a href=\"javascript:void(0);\">"+ text +"<span class=\"cross tag_close\"><input type=\"hidden\" name=\""+ $(this).prop("name")+"-list[]\" value=\""+ _value +"\"></span></a>";

            var tag_element = "<li><input type=\"hidden\" name=\""+ $(this).prop("name")+"-list[]\" value=\""+ _value +"\"><p>"+ text +"</p> <a class=\"champ_cross\" href=\"javascript:void(0);\"><img width=\"8\" height=\"8\" alt=\"cross\" src=\"/static/images/champ_cross.png\"></a></li>";

            $(this).parent().parent().find(".block_tags").append(tag_element);
        }

    });

    $("#id_level").select2({ width: "430px" }).on("change", function (e) {
        var text = $(this).select2('data')[0].text
        $.ajax({
            url: "/ajax/fetch-subjects/",
            data: {
                level_name: [ text ]
            },
            success: function(data) {
                var data = jQuery.parseJSON(data);
                if(data.status == "SUCCESS"){
                    $("#id_subject").html("");
                    options = "<option>Select Subject</option>";
                    for(var i = 0; i < data.data.subjects.length; i++) {
                        options += "<option value=\""+ data.data.subjects[i].name +"\">"+ data.data.subjects[i].name +"</option>";
                    }
                    $("#id_subject").html(options);
                    $("#id_subject").parent().find(".select2-container").remove();
                    $("#id_subject").select2();
                    if(edit_mode && !level_triggered_first_time) {

                    }
                    else {
                        $("#id_subject").parent().find(".block_tags").html("");
                    }
                    level_triggered_first_time = true;

                    }

            }
        });
    });

    $(".select2").select2({ "width": "220px" });

    $(document).on("change", ".add_multiple", function (e) {

            var _thisid = $(this).prop("id");
            var value = $("#"+_thisid+" option:selected").val();
            var text = $("#"+_thisid+" option:selected").text();
            var value_found = false;
            var $this = $(this);
            $(this).parent().find(".block_tags").find("input").each(function(e){
                if($(this).val() == value){
                    value_found = true;
                    return;
                }

            });
            //alert(value_found);
            if(value != "-1" && !value_found){
                //var input_element = "<input type='hidden' name='"+ $(this).prop("name")+"-list[]" +"' value='"+ value +"' >";
                //var tag_element = "<span class=\"dfrnt_tag radius3\"><a class=\"tag_close\" href=\"#\"></a>"+ input_element + text +"</span>";

                var tag_element = "<li><input type=\"hidden\" name=\""+ $(this).prop("name")+"-list[]\" value=\""+ value +"\"><p>"+ text +"</p> <a class=\"champ_cross\" href=\"javascript:void(0);\"><img width=\"8\" height=\"8\" alt=\"cross\" src=\"/static/images/champ_cross.png\"></a></li>";

                $(this).parent().find(".block_tags").append(tag_element);
            }

    });

    $(".currency-select2").select2();


    $(document).on("click",".tag_close",function(e){
        e.preventDefault();
        $(this).parent().remove();
        if($(this).hasClass("tag_study_partners")) {
            var id = $(this).find("input").val();
            $("#id_lesson_payer").find("option[value="+ id +"]").remove();
        }
    });

    function validate(element,type,rules,focus) {
        var elem = $(type+"[name="+element+"]");

        if(element == "rate") {
            if($.trim($("input[name=rate]").val()) == "" || parseInt($("input[name=rate]").val()) == 0) {
                elem.addClass("error");
                if(focus) {
                    elem.focus();
                }
                elem.parent().find(".error_status").text("This field is required.").show();
                return false;
            }
            else {
                elem.removeClass("error");
                elem.parent().find(".error_status").text("").hide();
            }
        }
        else if(element == "rate2") {
            if($.trim($("input[name=rate2]").val()) == "" || parseInt($("input[name=rate2]").val()) == 0 ) {
                elem.addClass("error");
                if(focus) {
                    elem.focus();
                }
                elem.parent().find(".error_status").text("This field is required.").show();
                return false;
            }
            else {
                elem.removeClass("error");
                elem.parent().find(".error_status").text("").hide();
            }
        }

        if($.trim(elem.val()) == "") {
            elem.addClass("error");
            if(focus) {
                elem.focus();
            }
            elem.parent().find(".error_status").text("This field is required.").show();
            return false;
        }
        else {
            elem.removeClass("error");
            elem.parent().find(".error_status").text("").hide();
        }
        return true;
    }

    function validate_tags(element,type,rules) {
        var elem = $(type+"[name="+element+"]");
        var value_found = false;
        elem.parent().find(".block_tags").find("input").each(function(e){
            value_found = true;
        });
        if(!value_found) {
            elem.addClass("error");
            elem.focus();
            elem.parent().find(".error_status").text("This field is required.").show();
            return false;
        }
        else {
            elem.removeClass("error");
            elem.parent().find(".error_status").text("").hide();
        }
        return true;
    }


    $("input[name=job_post_button]").click(function(e){
        e.preventDefault();
        var valid = validate("title","input","empty");
        valid &= validate("description","textarea","empty");
        valid &= validate("job_end_date","input","empty",false);
        valid &= validate("lesson_time","input","empty",false);
        valid &= validate("lesson_start_date","input","empty",false);
        valid &= validate_tags("subject","select","empty");
        //valid &= validate_tags("timezone","select","empty");
        valid &= validate("rate","input","empty");
        //valid &= validate("rate2","input","empty");

        var total_file_size = get_total_file_size();
        if(total_file_size > 20971520) {
            $("#id_max_size_hint").show();
            $("input[name=job_post_button]").prop("disabled", true);
            valid = false;
        }
        else {
            $("#id_max_size_hint").hide();
            $("input[name=job_post_button]").prop("disabled", false);
        }
        if(valid) {
            $("input[name=job_post_button]").prop("disabled", true);
            $("form[name=lesson-post-form]").prop("action","/job/create/");
            $("form[name=lesson-post-form]").submit();
        }
        return false;
    });

    function readURL(input) {
        console.log(input);
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                console.log(e.target);
                $('#__files').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).on("change", "#temp_file", function(e) {
        readURL(this);
    });

    $(document).on("click","#id_preview_post", function(e) {
        e.preventDefault();
        var valid = validate("title","input","empty");
        valid &= validate("description","textarea","empty");
        valid &= validate("job_end_date","input","empty",false);
        valid &= validate("lesson_start_date","input","empty",false);
        valid &= validate_tags("subject","input","empty");
        valid &= validate("rate","input","empty");
        valid &= validate("rate2","input","empty");
        if(valid) {
            $("form[name=lesson-post-form]").prop("action","/job/create/");
            $("form[name=lesson-post-form]").submit();
        }
        $("#myModal").modal("hide");
        return false;
    });

    $("input[name=job_post_preview_button]").click(function(e){
        e.preventDefault();
        var valid = validate("title","input","empty");
        valid &= validate("description","textarea","empty");
        valid &= validate("job_end_date","input","empty",false);
        valid &= validate("lesson_start_date","input","empty",false);
        valid &= validate_tags("subject","input","empty");
        valid &= validate("rate","input","empty");
        if(valid) {
            $("#id_overlay_loading").show();
            var serialized_data = $("form[name=lesson-post-form]").serialize();
            $.ajax({
                type: "POST",
                url: "/ajax/job/post-preview/",
                data: serialized_data,
                enctype: 'multipart/form-data',
                success: function(data)
                {
                    $("#temp_file").change();
                    var data = jQuery.parseJSON(data);
                    $("#id_preview_content").html(data.data.dialog_content);
                    $("#myModal").modal("show");
                },
                error: function(jqxhr, status, error)
                {

                }
            })
            .done(function( msg ) {
                $("#id_overlay_loading").hide();
            });
        }
        return false;
    });


    $( "#id_subject" ).autocomplete({
        minLength: 0,
        source: function(request, response) {
        $.ajax({
            url: "/ajax/major",
            data: {
                term: request.term,
                readonly: 'true'
            },
            success: function(data) {
                response(data);
            }
        });
        },
        //source: '/ajax/major?readonly=true',
        data: { 'readonly': 'true' },
        focus: function( event, ui ) {
            if(ui.item.id == -1){
                $( ".job_post_job_subject" ).val( ui.item.value.replace('"','').replace("<b>","").replace("</b>","").replace("Add ","").replace('"','') );
            }
            else{
                $( ".job_post_job_subject" ).val( ui.item.value );
            }
            return false;
        },
        select: function( event, ui ) {
            var tag_val = ui.item.value;
            if(ui.item.id == -1){
                tag_val = ui.item.value.replace('"','').replace("<b>","").replace("</b>","").replace("Add ","").replace('"','');
            }

            var value = ui.item.id;
            var text = ui.item.value;
            var value_found = false;
            $(this).parent().find(".tags").find("input").each(function(e){
                if($(this).val() == value){
                    value_found = true;
                    return;
                }
            });

            if(!value_found){

                var tag_element = "<a href=\"javascript:void(0);\">"+ text +"<span class=\"cross tag_close\"><input type=\"hidden\" name=\""+ $(this).prop("name")+"-list[]\" value=\""+ text +"\"></span></a>";

                //var input_element = "<input type='hidden' name='"+ $(this).prop("name")+"-list[]" +"' value='"+ value +"' >";
                //var tag_element = "<span class=\"dfrnt_tag radius3\"><a class=\"tag_close\" href=\"#\"></a>"+ input_element + text +"</span>";

                $(this).parent().find(".tags").append(tag_element);
            }

            $( "#id_subject" ).val("");
            $( "#id_subject" ).focus();

            return false;
        }
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li style='cursor:pointer;'>" )
                .append( "<div style='padding: 3px;'>"+ item.value +"</div>" )
                .append( "<input type='hidden' class='major_id' value='"+ item.id +"'/>" )
                .appendTo( ul );
        }
    /*$("#id_lesson_start_time").datetimepicker({
        //sideBySide: true
        format: 'LT',
        minDate: new Date()
    });*/

    var file_ = $("#id_attached_file").clone();
var file_index = window.file_index;
var max_files_count = 5;
var files_count = window.files_count;

$("#id_job_post_attach_file").click(function(e){
    e.preventDefault();
    $("#id_job_post_attached_file").click();
});

function get_total_file_size() {
    var total_file_size = 0;
    $("#id_job_post_attached_files").find("input[type=file]").each(function(i) {
        total_file_size += this.files[0].size;
    });
    return total_file_size;
}

$(document).on("change","#id_job_post_attached_file",function(e){
    var _this = $(this);
    var files = $(this).prop("files");
    var clone = $(this).clone();
    //var clone2 = jQuery.extend({}, this);
    _this.removeProp("id");
    //console.log($(clone).prop("files").length);
    //console.log(files.length);
    //clone2.prop("id","temp_file");
    //$("#id_files").append(clone2);
    var pval = $("input[name=fcount]").val();
    pval = parseInt(pval);
    if(pval == undefined || pval == "" || isNaN(pval)) {
        pval = 0;
    }
    pval += 1;
    $("input[name=fcount]").val(pval);
    for(var j = 0 ; j < files.length ; j++){
        _this.prop("name", _this.prop("name") + file_index);
        var html = "<div style='width: 100%; margin-top: 4px; padding: 10px; float: left; clear: both;'><span style='width: 70%; ' class='pull-left'>"+ files[j].name +"</span><i class='glyphicon glyphicon-remove pull-right cancel-file' data-index='"+ file_index +"' style='cursor: pointer;'></i></div>";
        _this.css("display","none");
        $("#id_job_post_attached_files").append(html);
        $("#id_job_post_attached_files").append(_this);
        files_count ++;
        if(files_count >= max_files_count){
            $("#id_file_input_container_hint").show();
            $("#id_file_input_container").hide();
        }
        else{
            $("#id_file_input_container_hint").hide();
            $("#id_file_input_container").show();
        }
//        file_data += "file: "+files[j].name+";";
//        $("input[name=files]").val(file_data);
    }
//    console.log();
    $("#id_file_input_container").append(clone);
    file_index ++;
    var total_file_size = get_total_file_size();
    if(total_file_size > 20971520) {
        $("#id_max_size_hint").show();
        $("input[name=job_post_button]").prop("disabled", true);
    }
    else {
        $("#id_max_size_hint").hide();
        $("input[name=job_post_button]").prop("disabled", false);
    }
});

$(document).on("click",".cancel-file", function(e){
    var _this_index = $(this).data("index");
    var target_name = file_.prop("name")+_this_index;
    $("input[name="+target_name+"]").remove();
    $(this).parent().remove();
    files_count --;
    if(files_count < max_files_count){
        $("#id_file_input_container_hint").hide();
        $("#id_file_input_container").show();
    }
    var total_file_size = get_total_file_size();
    if(total_file_size > 20971520) {
        $("#id_max_size_hint").show();
        $("input[name=job_post_button]").prop("disabled", true);
    }
    else {
        $("#id_max_size_hint").hide();
        $("input[name=job_post_button]").prop("disabled", false);
    }
});

$("#add_study_partners").select2({
  height: 90,
  placeholder: "Type here to add other students",
  ajax: {
    url: "/ajax/tutor-list/",
    dataType: 'json',
    delay: 250,
    data: function (params) {
      return {
        q: params.term, // search term
        utype: "student"
      };
    },
    processResults: function (data, page) {
      return {
            results: $.map(data.items, function(obj) {
                return { id: obj.id, text: obj.name };
            })
        };
    },
    cache: true
  },
  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
  minimumInputLength: 1,
}).on("change",function(e){
    var value = $(this).val();
    var text = $(this).text();

    var value_found = false;

    $(this).parent().find(".tags").find("input").each(function(e){
        if($(this).val() == value){
            value_found = true;
            return;
        }
    });

    if(!value_found) {
        var tag_element = "<a href=\"javascript:void(0);\">"+ text +"<span class=\"cross tag_close tag_study_partners\"><input type=\"hidden\" name=\""+ $(this).prop("name")+"-list[]\" value=\""+ value +"\"></span></a>";

        $(this).parent().find(".tags").append(tag_element);

        $("#id_lesson_payer").append("<option value=\""+ value +"\">"+ text +"</option>");

        $(this).val("");
        $(this).text("");
    }

});

$("select[name=job_post_lesson_type]").change(function(e) {
    if($(this).val() == "online") {
        $("#add_study_partners").prop("disabled",false);
    }
    else {
        $("#add_study_partners").prop("disabled",true);
    }
});

$("#id_subject").parent().find(".select2-selection__rendered").html("Select Subject");

});