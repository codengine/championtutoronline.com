var ajax_call = function(type,url,data,callback,error_back,complete_back){
    $.ajax({
        type: type,
        url: url,
        data: data,
        enctype: 'multipart/form-data',
        success: function(data)
        {
            callback(jQuery.parseJSON(data));
        },
        error: function(jqxhr, status, error)
        {
            error_back(jqxhr, status, error);
        }
    })
    .done(function( msg ) {
        complete_back(msg);
    });
};

function collect_my_job_ids() {
    var ids = "";
    $("#id_section_my_jobs").find(".my-job-item").each(function(i) {
        ids += $(this).data("job-id") + ",";
    });
    return ids;
}

function place_my_jobs_item_counter(data) {
    $("#id_section_my_jobs").find(".my-job-item").each(function(i) {
        var job_id = parseInt($(this).data("job-id"));
        if(typeof data[job_id] != "undefined") {
            var count_data = data[job_id];
            $(this).find(".pending_count").text(count_data.pending_count);
            $(this).find(".invitation_count").text(count_data.invitation_count);
            $(this).find(".reject_count").text(count_data.rejected_count);
            $(this).find(".shortlist_count").text(count_data.shortlisted_count);
            $(this).find(".hired_count").text(count_data.hired_count);
        }
    });
}

function check_job_item_count_update() {
    var job_ids = collect_my_job_ids();
    var request_data = {
      job_ids: job_ids
    };
    var type = "GET";
    var url = $("#champ-active-requests").data("count-update-url");
    //console.log(url);
    ajax_call(type, url, request_data, function(data) {
        if(data.status == "SUCCESS") {
            place_my_jobs_item_counter(data.data);
        }
    },
    function(jqxhr, status, error) {

    },
    function(msg) {

    });
}

function place_job_details_item_counter(job_id, data) {
    var job_id = parseInt(job_id);
    if(typeof data[job_id] != "undefined") {
        var count_data = data[job_id];

        var current_tab = $("input[name=status_class]").val();

        var trigger_class = "";

        if(current_tab == "pending") {
            var current_pending_count = parseInt($("#id_pending_count").text());
            var new_pending_count = count_data.pending_count;
            if(current_pending_count != new_pending_count) {
                trigger_class = ".job-status-list li."+current_tab;
            }
        }
        else if(current_tab == "invited") {
            var current_invited_count = parseInt($("#id_invited_count").text());
            var new_invited_count = count_data.invitation_count;
            if(current_invited_count != new_invited_count) {
                trigger_class = ".job-status-list li."+current_tab;
            }
        }
        else if(current_tab == "rejected") {
            var current_rejected_count = parseInt($("#id_rejected_count").text());
            var new_rejected_count = count_data.rejected_count;
            if(current_rejected_count != new_rejected_count) {
                trigger_class = ".job-status-list li."+current_tab;
            }
        }
        else if(current_tab == "shortlisted") {
            var current_shortlisted_count = parseInt($("#id_shortlisted_count").text());
            var new_shortlisted_count = count_data.shortlisted_count;
            if(current_shortlisted_count != new_shortlisted_count) {
                trigger_class = ".job-status-list li."+current_tab;
            }
        }
        else if(current_tab == "hired") {
            var current_hired_count = parseInt($("#id_hired_count").text());
            var new_hired_count = count_data.hired_count;
            if(current_hired_count != new_hired_count) {
                trigger_class = ".job-status-list li."+current_tab;
            }
        }

        console.log(trigger_class);

        $("#id_pending_count").text(count_data.pending_count);
        $("#id_invited_count").text(count_data.invitation_count);
        $("#id_rejected_count").text(count_data.rejected_count);
        $("#id_shortlisted_count").text(count_data.shortlisted_count);
        $("#id_hired_count").text(count_data.hired_count);

        if(trigger_class != "") {
            $(trigger_class).click();
        }

    }
}

function check_job_details_count_update() {
    var job_ids = $("input[name=job_id]").val();
    var request_data = {
      list: 0,
      job_ids: job_ids
    };
    var type = "GET";
    var url = $("#id_champ_job_details_right").data("count-update-url");
    //console.log(url);
    ajax_call(type, url, request_data, function(data) {
        if(data.status == "SUCCESS") {
            place_job_details_item_counter(job_ids, data.data);
        }
    },
    function(jqxhr, status, error) {

    },
    function(msg) {

    });
}