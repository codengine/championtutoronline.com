$(document).ready(function() {
    function _do_ajax_search(data, callback, errback, completeback) {
        $.ajax({
            type: "GET",
            url: "/ajax/marketplace/jobs/",
            data: data,
            success: function(data)
            {
                var data = jQuery.parseJSON(data);
                callback(data);
            },
            error: function(jqxhr, status, error)
            {
                errback(jqxhr, status, error);
            }
        })
        .done(function( msg ) {
            completeback(msg);
        });
    }

    function prepare_search_data() {
        var data = {};
        data["page"] = $("input[name=page_number]").val();

        var lesson_type = "any";
        if($("#lesson_type_live").is(":checked")) {
            lesson_type = "live";
        }
        else if($("#lesson_type_written").is(":checked")) {
            lesson_type = "written";
        }
        data["lesson_type"] = lesson_type

        var gender = "any";
        if($("#gender_male").is(":checked")) {
            gender = "male";
        }
        else if($("#gender_female").is(":checked")) {
            gender = "female";
        }
        data["gender"] = gender;

        var subjects = $("#id_marketplace_filter_subject").val();
        data["subjects"] = subjects;

        var countries = $("#id_marketplace_filter_country").val();
        data["countries"] = countries;

        var keywords = $("#id_marketplace_filter_keyword").val();
        data["keywords"] = keywords;

        var budget = $("#id_marketplace_filter_budget").val();
        data["budget"] = budget

        var currency = $("#id_marketplace_filter_currency").val();
        data["currency"] = currency

        var language = $("#id_marketplace_filter_language").val();
         data["language"] = language

        return data;
    }

    function _do_initial_call() {
        var data =  prepare_search_data();
        var container_id = "id_marketplace_result_list";

        $("#"+container_id).css("opacity", "0.5");
        $("#"+container_id).css("zoom", "alpha(opacity=50)");
        $("#"+container_id).css("zoom", "1");

        _do_ajax_search(data, function(data) {
            console.log(data);
            $("#"+container_id).html(data.data);
        },
        function(jqxhr, status, error) {
        },
        function(msg) {
            $("#"+container_id).css("opacity", "1");
            $("#"+container_id).css("zoom", "alpha(opacity=100)");
            $("#"+container_id).css("zoom", "0");
        });
    }

    _do_initial_call();

    function _do_search() {
        var data = prepare_search_data();

        //console.log(data);

        var container_id = "id_marketplace_result_list";

        $("#"+container_id).css("opacity", "0.5");
        $("#"+container_id).css("zoom", "alpha(opacity=50)");
        $("#"+container_id).css("zoom", "1");

        _do_ajax_search(data, function(data) {
            $("input[name=page_number]").val(data.extra_data.current);
            $("#"+container_id).html(data.data);
        },
        function(jqxhr, status, error) {
        },
        function(msg) {
            $("#"+container_id).css("opacity", "1");
            $("#"+container_id).css("zoom", "alpha(opacity=100)");
            $("#"+container_id).css("zoom", "0");
        });
    }

    $(document).on("click",".search-next", function(e) {
        e.preventDefault();
        var page_number = $(this).data("page-number");
        $("input[name=page_number]").val(page_number);
        _do_search();
    });

    $(document).on("change","#lesson_type_live", function(e) {
       e.preventDefault();
       _do_search();
    });

    $(document).on("change","#lesson_type_written", function(e) {
       e.preventDefault();
       _do_search();
    });

    $(document).on("change","#lesson_type_any", function(e) {
       e.preventDefault();
       _do_search();
    });

    $(document).on("change","#gender_male", function(e) {
       e.preventDefault();
       _do_search();
    });

    $(document).on("change","#gender_female", function(e) {
       e.preventDefault();
       _do_search();
    });

    $(document).on("change","#gender_any", function(e) {
       e.preventDefault();
       _do_search();
    });

    $(document).on("keyup","#id_marketplace_filter_subject", function(e) {
       e.preventDefault();
       if($.trim($(this).val()) != "" && e.keyCode == 13) {
            _do_search();
       }
    });

    $(document).on("change","#id_marketplace_filter_country", function(e) {
       e.preventDefault();
       if($(this).val() != "-1") {
            _do_search();
       }
    });

    $(document).on("keyup","#id_marketplace_filter_keyword", function(e) {
       e.preventDefault();
       if($.trim($(this).val()) != "" && e.keyCode == 13) {
            _do_search();
       }
    });

    $(document).on("keyup","#id_marketplace_filter_budget", function(e) {
       e.preventDefault();
       if($.trim($(this).val()) != "" && e.keyCode == 13) {
            _do_search();
       }
    });

    $(document).on("change","#id_marketplace_filter_currency", function(e) {
       e.preventDefault();
       if($(this).val() != "-1") {
           _do_search();
       }
    });

    $(document).on("change","#id_marketplace_filter_language", function(e) {
       e.preventDefault();
       if($(this).val() != "-1") {
           _do_search();
       }
    });

    $(document).on("submit","#id_search_marketplace_form", function(e) {
       e.preventDefault();
       return false;
    });

    $(document).on("click","#id_marketplace_search_submit", function(e) {
       e.preventDefault();
       _do_search();
       return false;
    });
});
