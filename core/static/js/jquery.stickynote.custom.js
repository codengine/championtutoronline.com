/**
 * Created with PyCharm.
 * User: codengine
 * Date: 5/15/15
 * Time: 11:15 AM
 * To change this template use File | Settings | File Templates.
 */

(function(){

    //window.sticky_update_timer = null;;

    var generateUUID = function(){
        var d = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = (d + Math.random()*16)%16 | 0;
            d = Math.floor(d/16);
            return (c=='x' ? r : (r&0x3|0x8)).toString(16);
        });
        return uuid;
    };

    //$.fn.stickynote.

    $.extend({
            createNote : function(o){
            var _this_widget = this;
            _this_widget.sticky_update_timer = null;
            if(o.id != ""){
                _this_widget.id = o.id;
            }
            else{
                _this_widget.id = generateUUID();
            }

            var _sticky_container = $(document.createElement("div")).addClass("sticky-note-container").addClass("jSticky-large").prop("id","id_sticky_container_"+_this_widget.id);
            _sticky_container.css({
                "position" : "absolute",
                "top" : o.top+"px",
                "left" : o.left+"px"
            });

            var _sticky_header = $(document.createElement("div")).addClass("sticky-large-header");

            var _sticky_note_content_container = $(document.createElement("div")).addClass("jStickyNote");
            var _sticky_textarea = $(document.createElement("span")).addClass("text-point lesson-note-body");
            _sticky_textarea.css({
                "width": "200px",
                "font-family": "Museo Sans W01 Rounded",
                "font-weight": "200",
                "font-size": "17px",
                "line-height": "24px",
                "background": "#e7f2ff",
                "padding-top": "7px"
            });
            _sticky_textarea.prop("contenteditable",true);
            _sticky_textarea.html("<b>New Note</b>");

            if(o.content != null && o.content != undefined){
                console.log(o.content);
                _sticky_textarea.html(o.content);
            }

            _sticky_note_content_container.append(_sticky_textarea);

            _sticky_textarea.click(function() {
                _sticky_textarea.focus();
            });

            _sticky_textarea.keyup(function(e){
                var _this_textarea = $(this);
                console.log(_this_textarea.html());
                //if(sticky_update_timer != null && sticky_update_timer != undefined)
                {
                //alert("sadasdasdsad");
                clearInterval(_this_widget.sticky_update_timer);  //clear any interval on key up
                _this_widget.sticky_update_timer = setTimeout(function() { //then give it a second to see if the user is finished
                    //do .post ajax request //then do the ajax call
                    if(_this_textarea.val() != "" || _this_textarea.val() != " "){
                        _ajax_call("POST","/ajax/stickynote",{
                                "action": "SAVE",
                                "lesson_id": $("#id_hidden_lesson_id").val(),
                                "wb_id": $("#id_hidden_whiteboard_id").val(),
                                "id": _sticky_container.prop("id").replace("id_sticky_container_",""),
                                "content": _this_textarea.html(),
                                "top": _sticky_container.css("top").replace("px",""),
                                "left": _sticky_container.css("left").replace("px","")
                            },
                            function(data){
                                if(data.status == "SUCCESS"){
                                    //_sticky_container.remove();
                                }
                                else{
                                    console.log("Sticky note save failed.");
                                }
                            },
                            function(jqxhr, status, error){
                                console.log("Sticky note save failed.");
                            },
                            function(msg){

                            });
                    }
                }, 1000);
                };
            });

            var _sticky_delete = $(document.createElement("div")).addClass("jSticky-delete");//.append('<i class="glyphicon glyphicon-remove-sign"></i>')

            _sticky_delete.click(function(e){
                //alert(_sticky_container.prop("id").replace("id_sticky_container_",""));
                _ajax_call("POST","/ajax/stickynote",{
                        "action": "DELETE",
                        "lesson_id": $("#id_hidden_lesson_id").val(),
                        "wb_id": $("#id_hidden_whiteboard_id").val(),
                        "id": _sticky_container.prop("id").replace("id_sticky_container_","")
                    },
                    function(data){
                        if(data.status == "SUCCESS"){
                            _sticky_container.remove();
                        }
                        else{
                            console.log("Sticky note delete failed.");
                        }
                    },
                    function(jqxhr, status, error){
                        console.log("Sticky note delete failed.");
                    },
                    function(msg){

                    });
            });
            var _sticky_lock = $(document.createElement("div")).addClass("jSticky-readonly");

            _sticky_lock.click(function(e){
                if(o.locked == 0 || this.sticky_locked == false) {
                    $(this).parent().find(".text-point").prop("contenteditable",false);
                    this.sticky_locked = true;
                    _sticky_lock.css("background","url(/static/css/sticky/images/lock.png) no-repeat top left");
                }
                else {
                    $(this).parent().find(".text-point").prop("contenteditable",true);
                    this.sticky_locked = false;
                    _sticky_lock.css("background","url(/static/css/sticky/images/unlock.png) no-repeat top left");
                }
                var _is_locked = this.sticky_locked;
                _ajax_call("POST","/ajax/stickynote",{
                    "action": "SAVE_LOCK",
                    "lesson_id": $("#id_hidden_lesson_id").val(),
                    "wb_id": $("#id_hidden_whiteboard_id").val(),
                    "id": _sticky_container.prop("id").replace("id_sticky_container_",""),
                    "locked": _is_locked?1:0
                },
                function(data){
                    if(data.status == "SUCCESS"){
                        //_sticky_container.remove();
                    }
                    else{
                        console.log("Sticky note update failed.");
                    }
                },
                function(jqxhr, status, error){
                    console.log("Sticky note update failed.");
                },
                function(msg){

                });

            });

            _sticky_container.append(_sticky_header).append(_sticky_note_content_container).append(_sticky_delete).append(_sticky_lock);

            if(o.locked == 1) {
                _sticky_lock.parent().find(".text-point").prop("contenteditable",false);
                this.sticky_locked = true;
                _sticky_lock.css("background","url(/static/css/sticky/images/lock.png) no-repeat top left");
            }
            else {
                _sticky_lock.parent().find(".text-point").prop("contenteditable",true);
                this.sticky_locked = false;
                _sticky_lock.css("background","url(/static/css/sticky/images/unlock.png) no-repeat top left");
            }

            _this_widget.sticky_container = _sticky_container;


             this.sticky_hidden = false;

             _sticky_header.dblclick(function() {
                //_sticky_note_content_container.hide();
                if(!this.sticky_hidden) {
                    _sticky_textarea.hide();
                    _sticky_lock.hide();
                    this.sticky_hidden = true;
                }
                else {
                    _sticky_textarea.show();
                    _sticky_lock.show();
                    this.sticky_hidden = false;
                }
            });

            _sticky_textarea.focus();

            //$("#"+ o.container).append(_sticky_container);

            _sticky_container.draggable({
                containment: "id_sticky_content",
                onBeforeDrag: function(e){

                },
                onStartDrag: function(e){
                    console.log("Start");
                },
                onDrag: function(e){

                },
                onStopDrag: function(e){
                    _ajax_call("POST","/ajax/stickynote",{
                            "action": "SAVE_POS",
                            "lesson_id": $("#id_hidden_lesson_id").val(),
                            "wb_id": $("#id_hidden_whiteboard_id").val(),
                            "id": _sticky_container.prop("id").replace("id_sticky_container_",""),
                            "top": _sticky_container.css("top").replace("px",""),
                            "left": _sticky_container.css("left").replace("px","")
                        },
                        function(data){
                            if(data.status == "SUCCESS"){
                                //_sticky_container.remove();
                            }
                            else{
                                console.log("Sticky note update failed.");
                            }
                        },
                        function(jqxhr, status, error){
                            console.log("Sticky note update failed.");
                        },
                        function(msg){

                        });
                },
//            proxy: function(source){
//                var p = $('<div style="border:1px solid #ccc;width:80px"></div>');
//                p.html($(source).html()).appendTo('body');
//                return p;
//            }
                handle: ".sticky-large-header"
            });

            //Save the note to backend.
            if(o.create == true){
                _ajax_call("POST","/ajax/stickynote",{
                        "action": "CREATE",
                        "lesson_id": $("#id_hidden_lesson_id").val(),
                        "wb_id": $("#id_hidden_whiteboard_id").val(),
                        "id": _sticky_container.prop("id").replace("id_sticky_container_",""),
                        "top": _sticky_container.css("top").replace("px",""),
                        "left": _sticky_container.css("left").replace("px",""),
                        "content": _sticky_textarea.html()
                    },
                    function(data){
                        if(data.status == "SUCCESS"){
                            $("#"+ o.container).append(_sticky_container);
                        }
                        else{
                            console.log("Sticky note create failed.");
                        }
                    },
                    function(jqxhr, status, error){
                        console.log("Sticky note create failed.");
                    },
                    function(msg){

                    });
            }
            else{
                $("#"+ o.container).append(_sticky_container);
            }

        }
    });

    $.fn.stickynote = function(options) {
        var opts = $.extend({}, $.fn.stickynote.defaults, options);
        return this.each(function() {
            $this = $(this);
            var o = $.meta ? $.extend({}, opts, $this.data()) : opts;
            switch(o.event){
                case 'click':
                    $this.click(function(e){$.createNote(o);})
                    break;
            }
        });
    };

    $.fn.stickynote.defaults = {
        size 	: 'large',
        event	: 'click',
        color	: '#000000',
        "top": 51,
        "left": 312,
        ajax_call_method: function(type,url,data,callback,error_back,complete_back){

        },
        save_ajax: function(text){

        },
        create_note: function(){

        },
        delete_note: function(){

        },
        container: "id_sticky_content"
    };


})(jQuery);
