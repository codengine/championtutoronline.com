function show_applicant_hire_dialog(job_id, tutor_id) {
    $("#id_overlay_loading").show();
    $("#black_overlay").fadeOut('fast');
    //$("#id_ajax_rendered_content").fadeOut('fast');
    $.ajax({
        type: "GET",
        url: "/ajax/applicant_hire/",
        data: { job_id: job_id, tutor_id: tutor_id },
        enctype: 'multipart/form-data',
        success: function(data)
        {
            var data = jQuery.parseJSON(data);
            //console.log(data.data);
            $("#black_overlay").show();
            $("#id_ajax_rendered_content").html(data.data);
            $("#id_ajax_rendered_content").fadeIn('fast');
        },
        error: function(jqxhr, status, error)
        {

        }
    })
    .done(function( msg ) {
        $("#id_overlay_loading").hide();
        //show_popup();
    });
}

function show_applicant_reject_dialog(job_id, tutor_id) {
    $("#id_overlay_loading").show();
    $("#black_overlay").fadeOut('fast');
    //$("#id_ajax_rendered_content").fadeOut('fast');
    $.ajax({
        type: "GET",
        url: "/ajax/reject_job_application/",
        data: { job_id: job_id, tutor_id: tutor_id },
        enctype: 'multipart/form-data',
        success: function(data)
        {
            var data = jQuery.parseJSON(data);
            //console.log(data.data);
            $("#black_overlay").show(); //.fadeIn('fast');
            $("#id_ajax_rendered_content").html(data.data);
            $("#id_ajax_rendered_content").fadeIn('fast');
        },
        error: function(jqxhr, status, error)
        {

        }
    })
    .done(function( msg ) {
        $("#id_overlay_loading").hide();
        //show_popup();
    });
}

$(document).on("click",".job_applicant_hire_cancel", function(e) {
    $("#black_overlay").fadeOut('fast');
    $("#id_ajax_rendered_invite_content").fadeOut('fast');
});

$(document).on("click",".job_applicant_reject_cancel", function(e) {
    $("#black_overlay").fadeOut('fast');
    $("#id_ajax_rendered_invite_content").fadeOut('fast');
});

$(document).on("click","#id_job_application_reject_submit", function(e) {
    e.preventDefault();
    $("#id_job_application_reject_form").prop("action", "/ajax/reject_job_application/");
    $("#id_job_application_reject_form").submit();
});

$(document).on("click",".job_applicant_hire_submit", function(e) {
    e.preventDefault();
    $(this).prop("disabled", true);
    var $this = $(this);
    var lesson_date = $("input[name=hire_lesson_start_date]").val();
    var lesson_note = $("textarea[name=hire_lesson_note]").val();
    var hire_lesson_rate = $("input[name=hire_lesson_rate]").val();
    if($.trim(lesson_date) == "") {
        $("input[name=hire_lesson_start_date]").focus();
        $(this).prop("disabled", false);
        return false;
    }

    if($.trim(lesson_note) == "") {
        $("textarea[name=hire_lesson_note]").focus();
        $(this).prop("disabled", false);
        return false;
    }

    if(isNaN(parseFloat(hire_lesson_rate))) {
        $("input[name=hire_lesson_rate]").focus();
        $(this).prop("disabled", false);
        return false;
    }

    $.ajax({
        type: "POST",
        url: "/ajax/applicant_hire/",
        data: $("#id_applicant_hire_dialog_form").serialize(),
        enctype: 'multipart/form-data',
        success: function(data)
        {
            var data = jQuery.parseJSON(data);
            if(data.status == "SUCCESS") {
                location.reload();
            }
            else {

                BootstrapDialog.show({
                    title: "Hire Failed",
                    message: "Operation was unsuccessful. Please try again later.",
                    closable: false,
                    type: BootstrapDialog.TYPE_DANGER,
                    size: BootstrapDialog.SIZE_NORMAL,
                    buttons: [
                        {
                            label: "Ok",
                            cssClass: 'btn btn-md btn-primary width120',
                            action: function(dialogRef) {
                                $this.prop("disabled", false);
                                dialogRef.close();
                            }
                        },
                        {
                            label: "Cancel",
                            cssClass: 'btn btn-md btn-warning width120',
                            action: function(dialogRef) {
                                $this.prop("disabled", false);
                                dialogRef.close();
                            }
                        }]
                });


            }
        },
        error: function(jqxhr, status, error)
        {
            BootstrapDialog.show({
                    title: "Hire Failed",
                    message: "Operation was unsuccessful due to an unexoected error in the server. Please try again later.",
                    closable: false,
                    type: BootstrapDialog.TYPE_DANGER,
                    size: BootstrapDialog.SIZE_NORMAL,
                    buttons: [
                        {
                            label: "Ok",
                            cssClass: 'btn btn-md btn-primary width120',
                            action: function(dialogRef) {
                                $this.prop("disabled", false);
                                dialogRef.close();
                            }
                        },
                        {
                            label: "Cancel",
                            cssClass: 'btn btn-md btn-warning width120',
                            action: function(dialogRef) {
                                $this.prop("disabled", false);
                                dialogRef.close();
                            }
                        }]
                });
        }
    })
    .done(function( msg ) {

    });
});

$(document).on("click","#black_overlay", function(e) {
    $("#black_overlay").hide();
    $("#id_ajax_rendered_content").hide();
});

$(document).on("click", "#id_job_apply_cancel", function(e) {
    e.preventDefault();
    $("#black_overlay").hide();
    $("#id_ajax_rendered_content").hide();
})

function show_recommended_tutors_dialog(job_id) {
    //$("#id_overlay_loading").show();
    $("#black_overlay2").show();
    $("#popup_dialog").show();
    $("#id_ajax_rendered_recommended_tutors_content").fadeIn('fast');
    //$("#id_ajax_rendered_content").fadeOut('fast');
//    $.ajax({
//        type: "GET",
//        url: "/ajax/recommendation/tutors/",
//        data: { job_id: job_id },
//        enctype: 'multipart/form-data',
//        success: function(data)
//        {
//            var data = jQuery.parseJSON(data);
//            //console.log(data.data);
//            $("#black_overlay2").show();
//            $("#id_ajax_rendered_recommended_tutors_content").html(data.data);
//            $("#id_ajax_rendered_recommended_tutors_content").fadeIn('fast');
//        },
//        error: function(jqxhr, status, error)
//        {
//
//        }
//    })
//    .done(function( msg ) {
//        $("#id_overlay_loading").hide();
//        //show_popup();
//    });
}

$(document).ready(function() {

    function load_recommended_tutors_dialog() {
        var job_id = $("#id_recommended_tutors").data("job-id");
        $.ajax({
            type: "GET",
            url: "/ajax/recommendation/tutors/",
            data: { job_id: job_id },
            enctype: 'multipart/form-data',
            success: function(data)
            {
                var data = jQuery.parseJSON(data);
                //console.log(data.data);
                $("#id_ajax_rendered_recommended_tutors_content").html(data.data);
            },
            error: function(jqxhr, status, error)
            {

            }
        })
        .done(function( msg ) {
        });
    }

    setTimeout(load_recommended_tutors_dialog, 1000);

    $("#id_recommended_tutors").click(function(e) {
        var job_id = $(this).data("job-id");
        show_recommended_tutors_dialog(job_id);
    });

    $(document).on("click","#id_state_reject_reason",function(e) {
        e.preventDefault();
        $(this).hide();
        $("#id_state_reason").val("1");
        $("#id_apply_info_panel").slideDown('fast');
        $("#id_no_state_reject_reason").show();
    });

    $(document).on("click","#id_no_state_reject_reason",function(e) {
        e.preventDefault();
        $(this).hide();
        $("#id_state_reason").val("0");
        $("#id_apply_info_panel").slideUp('fast');
        $("#id_state_reject_reason").show();
    });

    $(document).on("click","#id_state_apply_message_yes",function(e) {
        e.preventDefault();
        $(this).hide();
        $("#id_state_apply_message_flag").val("1");
        $("#id_apply_info_panel").slideDown('fast');
        $("#id_state_apply_message_no").show();
    });

    $(document).on("click","#id_state_apply_message_no",function(e) {
        e.preventDefault();
        $(this).hide();
        $("#id_state_apply_message_flag").val("0");
        $("#id_apply_info_panel").slideUp('fast');
        $("#id_state_apply_message_yes").show();
    });

    $(document).on("click","#id_job_apply_cancel",function(e) {
        e.preventDefault();
        $("#black_overlay").fadeOut("fast");
        $("#popup_dialog").fadeOut("fast");
    });

    $(document).on("click",".job_reject_cancel",function(e) {
        e.preventDefault();
        $("#black_overlay").fadeOut("fast");
        $("#popup_dialog").fadeOut("fast");
    });

    $(document).on("click",".job_application_action_hire", function(e) {
        e.preventDefault();
        var job_id = $(this).parent().parent().find(".job_id").val();
        var tutor_id = $(this).parent().parent().find(".tutor_id").val();
        show_applicant_hire_dialog(job_id, tutor_id);
    });

    $(document).on("click",".job_application_negotiate", function(e) {
        e.preventDefault();
        var tutor_id = $(this).parent().find(".tutor_id").val();
        var job_id = $(this).parent().find(".job_id").val();

        var online_status = "online";
        var tutor_name = $(this).parent().find("input[name=tutor_name]").val();
        openChatPopup(0, tutor_id, tutor_name, online_status, [ parseInt(tutor_id) ], "", "", 1);
    });

    $(document).on("click",".job_application_message", function(e) {
        e.preventDefault();
        var tutor_id = $(this).parent().find(".tutor_id").val();
        var job_id = $(this).parent().find(".job_id").val();

        var online_status = "online";
        var tutor_name = $(this).parent().find("input[name=tutor_name]").val();
        openChatPopup(0, tutor_id, tutor_name, online_status, [ parseInt(tutor_id) ], "", "", 1);
    });

    $(document).on("click",".job_application_action_shortlist", function(e) {
        e.preventDefault();
        var tutor_id = $(this).parent().parent().find(".tutor_id").val();
        var job_id = $(this).parent().parent().find(".job_id").val();
        $.ajax({
            type: "POST",
            url: "/ajax/update_job_status_shortlisted/",
            data: { job_id: job_id, tutor_id: tutor_id },
            enctype: 'multipart/form-data',
            success: function(data)
            {
                var data = jQuery.parseJSON(data);
                if(data.status == "SUCCESS") {
                    location.reload();
                }
            },
            error: function(jqxhr, status, error)
            {
                BootstrapDialog.show({
                    title: "Failed",
                    message: "Operation couldn't be performed. There was a problem while processing the request. Please retry again or reload the page.",
                    closable: false,
                    type: BootstrapDialog.TYPE_DANGER,
                    size: BootstrapDialog.SIZE_NORMAL,
                    buttons: [
                        {
                            label: "Ok",
                            cssClass: 'btn btn-md btn-primary width120',
                            action: function(dialogRef) {
                                dialogRef.close();
                            }
                        },
                        {
                            label: "Cancel",
                            cssClass: 'btn btn-md btn-warning width120',
                            action: function(dialogRef) {
                                dialogRef.close();
                            }
                        }]
                });
            }
        })
        .done(function( msg ) {

        });
    });
    $(document).on("click",".job_application_action_reject", function(e) {
        e.preventDefault();
        var tutor_id = $(this).parent().parent().find(".tutor_id").val();
        var job_id = $(this).parent().parent().find(".job_id").val();
        show_applicant_reject_dialog(job_id, tutor_id);
    });

    $(document).on("click", "#id_job_hire_submit", function(e) {
        e.preventDefault();
        var job_id = $("#__job_id").val();
        var tutor_id = $("#__tutor_id").val();
        var start_date_default = $("#__id_start_date").data("default");
        var start_date = $("#__id_start_date").val();
        var duration_default = $("#__id_duration").data("default");
        var duration = $("#__id_duration").val();
        var currency = $("#__id_currency").val();
        var rate_default = $("#__id_rate").data("default");
        var rate = $("#__id_rate").val();
        var apply_message_flag = $("#id_state_apply_message_flag").val();
        var message = $("#id_state_apply_message").val();
        var apply_repeat = $("#id_apply_repeat").val();

        if(apply_message_flag == "1") {
            var error = false;
            if($.trim(rate) == "" || $.trim(rate) == "0") {
                $("#__id_rate").addClass("error_border");
                $("#__id_rate").focus();
                error = true;
            }
            else {
                $("#__id_rate").removeClass("error_border");
            }

            if($.trim(start_date) == "") {
                $("#__id_start_date").addClass("error_border");
                $("#__id_start_date").focus();
                error = true;
            }
            else {
                $("#__id_start_date").removeClass("error_border");
            }
            if(error) {
                return false;
            }
        }

        var data = {
            job_id: job_id,
            tutor_id: tutor_id,
            start_date_default: start_date_default,
            start_date: start_date,
            duration_default: duration_default,
            duration: duration,
            currency: currency,
            rate_default: rate_default,
            rate: rate,
            message: message,
            apply_repeat: apply_repeat
        }

        $(this).prop("disabled",true);
        $.ajax({
            type: "POST",
            url: "/ajax/applicant_hire/",
            data: data,
            enctype: 'multipart/form-data',
            success: function(data)
            {
                var data = jQuery.parseJSON(data);
                if(data.status == "SUCCESS") {
                    location.reload();
                }
                else {
                    BootstrapDialog.show({
                        title: "Failed",
                        message: "Failed to hire the tutor",
                        closable: true,
                        type: BootstrapDialog.TYPE_DANGER,
                        size: BootstrapDialog.SIZE_NORMAL,
                        buttons: [
                            {
                                label: "Ok",
                                cssClass: 'btn btn-md btn-primary width120',
                                action: function(dialogRef) {
                                    dialogRef.close();
                                }
                            },
                            {
                                label: "Close",
                                cssClass: 'btn btn-md btn-warning width120',
                                action: function(dialogRef) {
                                    dialogRef.close();
                                }
                            }]
                    });
                }
            },
            error: function(jqxhr, status, error)
            {
                BootstrapDialog.show({
                    title: "Failed",
                    message: "Failed to hire the tutor. Please try again later",
                    closable: true,
                    type: BootstrapDialog.TYPE_DANGER,
                    size: BootstrapDialog.SIZE_NORMAL,
                    buttons: [
                        {
                            label: "Ok",
                            cssClass: 'btn btn-md btn-primary width120 text-center',
                            action: function(dialogRef) {
                                dialogRef.close();
                            }
                        },
                        {
                            label: "Close",
                            cssClass: 'btn btn-md btn-warning width120 text-center',
                            action: function(dialogRef) {
                                dialogRef.close();
                            }
                        }]
                });
                $("#id_job_hire_submit").prop("disabled",false);
            }
        })
        .done(function( msg ) {
            $("#id_job_hire_submit").prop("disabled",false);
        });
    });

    /* Student job details */
    $(document).on("click",".tag_close", function(e){
        $(this).parent().remove();
    });

    function collect_exclude_list() {
        exclude_str = window.champ_user_id+",";;
        $( "#id_invite_tutor_text" ).parent().find(".tags").find("input").each(function(e){
                    exclude_str += $(this).val()+",";
        });
        return exclude_str;
    }

    function format_exclude_list(list) {
                var str = "";
                if(list != undefined && list != null) {
                    for(var i = 0 ; i < list.length ; i++) {
                    str += list[i];
                    if(i < list.length -1) {
                        str += ","
                    }
                }
                console.log(str);
                }
                return str;
            }


            $("#id_invite_tutor_text").select2({
                  width: 550,
                  height: 90,
                  placeholder: "Type Name",
                  ajax: {
                    url: "/ajax/tutor-list/",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                      return {
                        q: params.term, // search term,
                        exclude: format_exclude_list($("#id_invite_tutor_text").val())
                      };
                    },
                    processResults: function (data, page) {
                      return {
                            results: $.map(data.items, function(obj) {
                                return { id: obj.id, text: obj.name };
                            })
                        };
                    },
                    cache: true
                  },
                  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                  minimumInputLength: 1,
                }).on("change",function(e){
                    var list = $(this).val();
                    var str = "";
                    if(list != undefined && list != null) {
                        for(var i = 0 ; i < list.length ; i++) {
                            str += list[i];
                            if(i < list.length -1) {
                                str += ","
                            }
                        }
                    }
                    //console.log(str);
                    $("input[name=tutor_list]").val(str);
                });

    //$("#id_job_delete").prop("disabled", false);
    $("#id_job_delete").click(function(e) {
        e.preventDefault();
        var $this = $(this);
        BootstrapDialog.show({
            size: BootstrapDialog.SIZE_NORMAL,
            message: 'Are you sure you want to delete this job?',
            buttons: [{
                label: 'Confirm',
                cssClass: 'btn-danger',
                action: function(){
                    location.href = $this.data("url");
                }
            }, {
                label: 'Close',
                action: function(dialogItself){
                    dialogItself.close();
                }
            }]
        });
    });

    var _job_tutor_list_ajax_object = null;

    function _do_ajax_search(data, callback, errback, completeback) {
        if(_job_tutor_list_ajax_object != null) {
            _job_tutor_list_ajax_object.abort();
        }
        _job_tutor_list_ajax_object = $.ajax({
            type: "GET",
            url: "/ajax/job_tutor_list/",
            data: data,
            timeout: 10000,
            success: function(data)
            {
                var data = jQuery.parseJSON(data);
                callback(data);
            },
            error: function(jqxhr, status, error)
            {
                errback(jqxhr, status, error);
            }
        })
        .done(function( msg ) {
            completeback(msg);
        });
    }

    function _do_initial_ajax_search(data, callback, errback, completeback) {
        $.ajax({
            type: "GET",
            url: "/ajax/job_tutor_list/",
            data: data,
            timeout: 10000,
            success: function(data)
            {
                var data = jQuery.parseJSON(data);
                callback(data);
            },
            error: function(jqxhr, status, error)
            {
                errback(jqxhr, status, error);
            }
        })
        .done(function( msg ) {
            completeback(msg);
        });
    }

    function prepare_search_data() {
        var data = {};
        data["status"] = $("input[name=status_class]").val();
        data["search"] = $("input[name=search_enabled]").val();
        data["q"] = $("input[name=search_keyword_name]").val();
        data["job"] = $("input[name=job_id]").val();
        var view_invited = "1";
        if($("input[name=invited]").is(":checked")) {
            view_invited = "1";
        }
        else {
            view_invited = "0";
        }
        var view_recommended = "1";
        if($("input[name=recommended]").is(":checked")) {
            view_recommended = "1";
        }
        else {
            view_recommended = "0";
        }

         /*if($("input[name=both]").is(":checked")) {
            view_invited = "1";
            view_recommended = "1";
         }*/

         data["view_invited"] = view_invited;
         data["view_recommended"] = view_recommended;

        return data;
    }

    function reset_all_content_panel() {
        $("#id_container_pending").html("");
        $("#id_container_invited").html("");
        $("#id_container_shortlisted").html("");
        $("#id_container_rejected").html("");
        $("#id_container_hired").html("");
    }

    window.update_job_tutor_list = function(container_id, item_count_container, status) {
        var data =  prepare_search_data();
        if(typeof status != "undefined") {
            data["status"] = status;
        }

        //console.log(data);



        _do_ajax_search(data, function(data) {
            reset_all_content_panel();
            $("#"+container_id).html(data.data);
            $("#"+item_count_container).html(data.record_count);
        },
        function(jqxhr, status, error) {
            if(status == "timeout") {
                $("#"+container_id).html("");
            }
        },
        function(msg) {
            $("#"+container_id).css("opacity", "1");
            $("#"+container_id).css("zoom", "alpha(opacity=100)");
            $("#"+container_id).css("zoom", "0");
        })
    }

    $(".job-status-list li").click(function(e) {
        e.preventDefault();
        var container_id = "id_container_pending";
        var item_count_container = "id_pending_count";
        if($(this).hasClass("pending")) {
            $("input[name=status_class]").val("pending");
            item_count_container = "id_pending_count";
        }
        else if($(this).hasClass("invited")) {
            $("input[name=status_class]").val("invited");
            container_id = "id_container_invited";
            item_count_container = "id_invited_count";
        }
        else if($(this).hasClass("rejected")) {
            $("input[name=status_class]").val("rejected");
            container_id = "id_container_rejected";
            item_count_container = "id_rejected_count";
        }
        else if($(this).hasClass("shortlisted")) {
            $("input[name=status_class]").val("shortlisted");
            container_id = "id_container_shortlisted";
            item_count_container = "id_shortlisted_count";
        }
        else if($(this).hasClass("hired")) {
            $("input[name=status_class]").val("hired");
            container_id = "id_container_hired";
            item_count_container = "id_hired_count";
        }
        $("#"+container_id).html("<p style='padding: 20px;'>Loading...</p>");
        update_job_tutor_list(container_id, item_count_container);

    });


    $("input[class=view_checkbox_flt]").change(function(e) {

        /*if($(this).prop("name") == "both" && $(this).is(":checked")) {
            //$("input[name=invited]").prop("checked", true);
            //$("input[name=recommended]").prop("checked", true);
            $("input[name=invited]").prop("disabled",true);
            $("input[name=recommended]").prop("disabled",true);
        }
        else {
            $("input[name=invited]").prop("disabled",false);
            $("input[name=recommended]").prop("disabled",false);
        }*/

        $("input[name=search_keyword_name]").val("");

        $("input[name=search_enabled]").val("1");

        if(!$("input[name=invited]").is(":checked") && !$("input[name=recommended]").is(":checked")) {
            $("input[name=search_enabled]").val("0");
        }

        var container_id = "id_job_content";

        var data = prepare_search_data();

        //console.log(data);

        $("#"+container_id).css("opacity", "0.5");
        $("#"+container_id).css("zoom", "alpha(opacity=50)");
        $("#"+container_id).css("zoom", "1");

        $(this).parent().find("li").each(function(i) {
            $(this).removeClass("active");
        });
        $(this).addClass("active");

        _do_ajax_search(data, function(data) {
            $("#"+container_id).html(data.data);
        },
        function(jqxhr, status, error) {

        },
        function(msg) {
            $("#"+container_id).css("opacity", "1");
            $("#"+container_id).css("zoom", "alpha(opacity=100)");
            $("#"+container_id).css("zoom", "0");
        })
    });


    function _do_initial_load() {

        var container_id = "id_container_pending";

        var data = prepare_search_data();

        //console.log(data);

        $("#"+container_id).css("opacity", "0.5");
        $("#"+container_id).css("zoom", "alpha(opacity=50)");
        $("#"+container_id).css("zoom", "1");
        $("#"+container_id).html("<p style='padding: 20px;'>Loading...</p>");
        _do_initial_ajax_search(data, function(data) {
            $("#"+container_id).html(data.data);
        },
        function(jqxhr, status, error) {
            if(status == "timeout") {
                $("#"+container_id).html("");
            }
            else if(status == "abort") {
                $("#"+container_id).html("<div class=\"text-center\"><div style=\"margin: 13px;\">No data found.</div></div>");
            }
        },
        function(msg) {
            $("#"+container_id).css("opacity", "1");
            $("#"+container_id).css("zoom", "alpha(opacity=100)");
            $("#"+container_id).css("zoom", "0");
        })
    }

    function place_tab_class() {
        if($(".job-status-list li.resp-tab-active").hasClass("pending")) {
            $("input[name=status_class]").val("pending");
        }
        else if($(".job-status-list li.resp-tab-active").hasClass("invited")) {
            $("input[name=status_class]").val("invited");
        }
        else if($(".job-status-list li.resp-tab-active").hasClass("rejected")) {
            $("input[name=status_class]").val("rejected");
        }
        else if($(".job-status-list li.resp-tab-active").hasClass("shortlisted")) {
            $("input[name=status_class]").val("shortlisted");
        }
        else if($(".job-status-list li.resp-tab-active").hasClass("hired")) {
            $("input[name=status_class]").val("hired");
        }
    }

    //place_tab_class();

    setTimeout(_do_initial_load, 1000);

    $("button[name=job_tutor_search_btn]").click(function(e) {
        e.preventDefault();
        var q = $("input[name=search_keyword_name]").val();
        if($.trim(q) == "") {
            $("input[name=search_keyword_name]").focus();
            return;
        }
        $("input[name=search_enabled]").val("1");

        var container_id = "id_job_content";

        var data = prepare_search_data();

        //console.log(data);

        $("#"+container_id).css("opacity", "0.5");
        $("#"+container_id).css("zoom", "alpha(opacity=50)");
        $("#"+container_id).css("zoom", "1");

        $(this).parent().find("li").each(function(i) {
            $(this).removeClass("active");
        });
        $(this).addClass("active");

        _do_ajax_search(data, function(data) {
            $("#"+container_id).html(data.data);
        },
        function(jqxhr, status, error) {

        },
        function(msg) {
            $("#"+container_id).css("opacity", "1");
            $("#"+container_id).css("zoom", "alpha(opacity=100)");
            $("#"+container_id).css("zoom", "0");
        })
    });

    function _do_initial_call_fetch_tutors() {
        var container_id = "id_job_content";
        var data = prepare_search_data();
        $("#"+container_id).css("opacity", "0.5");
        $("#"+container_id).css("zoom", "alpha(opacity=50)");
        $("#"+container_id).css("zoom", "1");

        $(this).parent().find("li").each(function(i) {
            $(this).removeClass("active");
        });
        $(this).addClass("active");

        _do_ajax_search(data, function(data) {
            $("#"+container_id).html(data.data);
        },
        function(jqxhr, status, error) {

        },
        function(msg) {
            $("#"+container_id).css("opacity", "1");
            $("#"+container_id).css("zoom", "alpha(opacity=100)");
            $("#"+container_id).css("zoom", "0");
        })
    }

    _do_initial_call_fetch_tutors();
});

$(document).ready(function() {
    var current_hash = window.location.hash;
    if(current_hash.indexOf("action-tab1") > -1) {
        var container_id = "id_container_pending";
        var item_count_container = "id_pending_count";

        window.update_job_tutor_list(container_id, item_count_container, "pending");

        $("input[name=status_class]").val("pending");
    }
    else if(current_hash.indexOf("action-tab2") > -1) {
        var container_id = "id_container_invited";
        var item_count_container = "id_invited_count";

        window.update_job_tutor_list(container_id, item_count_container, "invited");

        $("input[name=status_class]").val("invited");
    }
    else if(current_hash.indexOf("action-tab3") > -1) {
        var container_id = "id_container_rejected";
        var item_count_container = "id_rejected_count";

        window.update_job_tutor_list(container_id, item_count_container, "rejected");

        $("input[name=status_class]").val("rejected");
    }
    else if(current_hash.indexOf("action-tab4") > -1) {
        var container_id = "id_container_shortlisted";
        var item_count_container = "id_shortlisted_count";

        window.update_job_tutor_list(container_id, item_count_container, "shortlisted");

        $("input[name=status_class]").val("shortlisted");
    }
    else if(current_hash.indexOf("action-tab5") > -1) {
        var container_id = "id_container_hired";
        var item_count_container = "id_hired_count";

        window.update_job_tutor_list(container_id, item_count_container, "hired");

        $("input[name=status_class]").val("hired");
    }

    $(document).on("click", ".show_comments", function(e) {
        e.preventDefault();
        $(this).hide();
        $(this).parent().find(".show_comments_content").show();
    });

});