
function job_invitation_action_update(data,callback,error_back,complete_back) {
    $.ajax({
        type: "POST",
        url: "/job-invitation/action/",
        data: data,
        enctype: 'multipart/form-data',
        success: function(data)
        {
            callback(jQuery.parseJSON(data));
        },
        error: function(jqxhr, status, error)
        {
            error_back(jqxhr, status, error);
        }
    })
    .done(function( msg ) {
        complete_back(msg);
    });
}

function job_application_action_update(ja_id,action) {
    var _this = $(this);
    $.ajax({
        type: "POST",
        url: "/ajax/update-job-application/",
        data: { action: action, ja: ja_id },
        enctype: 'multipart/form-data',
        success: function(data)
        {
            var data = jQuery.parseJSON(data);
            if(data.status == "SUCCESS") {
                var html = "";
                if(action == "cancel")
                {
                    html = "<div class=\"row\"><div class=\"col-xs-12\">Job Application Deleted</div></div>";
                }
                else if(action == "reject") {
                    html = "<div class=\"row\"><div class=\"col-xs-12\">Job Application Rejected</div></div>";
                }
                else if(action == "award") {
                    html = "<div class=\"row\"><div class=\"col-xs-12\">Job Awarded!</div></div>";
                }
                _this.parent().parent().parent().parent().parent().html(html);
                _this.parent().parent().parent().parent().parent().hide();
            }
        },
        error: function(jqxhr, status, error)
        {

        }
    })
    .done(function( msg ) {

    });
}

$(document).on("click",".ja-action", function(e) {
    var action = $(this).val();
    if(action != "-1") {

        var ja_id = $(this).parent().find(".ja_id").val();
        var msg = "";
        if(action == "4") //
        {
            msg = "Please confirm if you want to award this job";
        }
        else if(action == "3") {
            msg = "Are you sure you want to reject this job application?";
        }

        BootstrapDialog.show({
        title: "Confirm Before this action",
        message: msg,
        closable: true,
        type: BootstrapDialog.TYPE_WARNING,
        size: BootstrapDialog.SIZE_NORMAL,
        buttons: [
            {
                label: "Confirm",
                cssClass: 'btn btn-md btn-primary width120',
                action: function(dialogRef) {
                    job_application_action_update(ja_id,action);
                    dialogRef.close();
                }
            },
            {
                label: "Cancel",
                cssClass: 'btn btn-md btn-warning width120',
                action: function(dialogRef) {
                    dialogRef.close();
                }
            }]
    });


    }
});

$(document).ready(function() {


    $(document).on("#id_job_proposal_submit","click", function(e) {
        e.preventDefault();
        $(this).parent().find(".invite_button").prop("disabled",true);
        var job_id = $(this).parent().parent().find("input[name=job_invitation_job_id]").val();
        var $element = $(this);

        job_invitation_action_update({
            "action": "ACCEPT",
            "job_id": job_id
        },
        function(data) {
            console.log(data);
            if(data.status == "SUCCESS") {
                $element.parent().parent().parent().parent().remove();
                if(data.data.lesson_type == "live") {
                    location.href = "/lesson/"+job_id;
                }
                else {
                    location.href = "/written-lesson/?lesson="+job_id;
                }
            }
        },
        function(jqxhr, status, error) {

        },
        function(msg) {
            $element.parent().find(".invite_button").prop("disabled",false);
        });
    });

    $(document).on("click",".job_invite_reject", function(e) {
        e.preventDefault();
        var job_id = $(this).parent().find("input[name=hidden_job_post_id]").val();

        $(this).parent().find(".invite_button").prop("disabled",true);
        var $element = $(this);

        job_invitation_action_update({
            "action": "REJECT",
            "job_id": job_id
        },
        function(data) {
            if(data.status == "SUCCESS") {
                $element.parent().parent().parent().parent().remove();
            }
        },
        function(jqxhr, status, error) {

        },
        function(msg) {
            $element.parent().find(".invite_button").prop("disabled",false);
        });
    });
});

$(document).on("click",".negotiate", function(e) {
    e.preventDefault();
    if(window.champ_user_id != undefined && window.champ_user_id != -1) {

        //Now update the status as negotiating.
        var job_id = $(this).parent().find("input[name=hidden_job_post_id]").val();
        $.ajax({
            type: "POST",
            url: "/ajax/update_status_negotiating/",
            data: { job_id: job_id },
            enctype: 'multipart/form-data',
            success: function(data)
            {
                var data = jQuery.parseJSON(data);
                if(data.status == "SUCCESS") {

                }
                else {

                }
            },
            error: function(jqxhr, status, error)
            {

            }
        })
        .done(function( msg ) {

        });

        //get the tutor's user_id value.
        var uid = $(this).parent().find("input[name=tutor_id]").val();
        var offline_status = true; //Say user is offline
        var offline_status_found = $(this).parent().find("input[name=online_status]").val();

        if(offline_status_found == "online"){
            offline_status = false;
        }

        uid = parseInt(uid);

        //console.log(window.chat_boxes);

        if(!window.chat_boxes.hasOwnProperty(uid))
        {
            var tutor_name = $(this).parent().find("input[name=tutor_name]").val();
            var img_src = $(this).parent().find("input[name=tutor_pp]").val();
            var chat_object = {user_id:uid,name: tutor_name,img_url: img_src};
            //console.log(chat_object);
            var chat = new Chat();
            var uids = [uid];
            chat.remote_peers = uids;
            chat.addNewChat(chat_object,offline_status);
            //window.chat_boxes[uid] = chat;
        }
        else
        {
            window.chat_boxes[uid].show();
            window.chat_boxes[uid].focusInput();
        }
        //console.log(window.chat_boxes[uid].window_minimized);
    }
    else {
        show_signin();
    }
});


$(document).on("click",".job_apply",function(e) {
   e.preventDefault();
   var job_id = $(this).parent().find("input[name=hidden_job_post_id]").val();
   show_job_apply_dialog(job_id);
});

$(document).on("click",".job_reject",function(e) {
   e.preventDefault();
   var job_id = $(this).parent().find("input[name=hidden_job_post_id]").val();
   show_job_reject_dialog(job_id);
});

//$(document).on("click","#id_job_apply_submit", function(e) {
//    e.preventDefault();
//
//});


function show_popup() {
    $("#black_overlay").show();
    $("#popup_dialog").fadeIn('fast');
}

function show_reject_popup() {
    $("#black_overlay").show();
    $("#rd_popup_dialog").fadeIn('fast');
}

function hide_popup() {
    $("#black_overlay").hide();
    $("#popup_dialog").hide();
}

function hide_reject_popup() {
    $("#black_overlay").hide();
    $("#rd_popup_dialog").hide();
}

function show_job_apply_dialog(job_id) {
    $("#id_overlay_loading").show();
    hide_popup();
    $.ajax({
        type: "GET",
        url: "/ajax/apply_job/",
        data: { job_id: job_id },
        enctype: 'multipart/form-data',
        success: function(data)
        {
            var data = jQuery.parseJSON(data);
            //console.log(data.data);
            $("#id_ajax_rendered_content").html(data.data);
            $("#id_ajax_rendered_content").fadeIn('fast');
        },
        error: function(jqxhr, status, error)
        {

        }
    })
    .done(function( msg ) {
        $("#id_overlay_loading").hide();
        show_popup();
    });
}

function show_job_reject_dialog(job_id) {
    $("#id_overlay_loading").show();
    hide_reject_popup();
    $.ajax({
        type: "GET",
        url: "/ajax/reject_job/",
        data: { job_id: job_id },
        enctype: 'multipart/form-data',
        success: function(data)
        {
            var data = jQuery.parseJSON(data);
            $("#id_ajax_rendered_content").html(data.data);
            $("#id_ajax_rendered_content").fadeIn('fast');
        },
        error: function(jqxhr, status, error)
        {

        }
    })
    .done(function( msg ) {
        $("#id_overlay_loading").hide();
        show_reject_popup();
    });
}

function show_job_recommendation_reject_dialog(job_id) {
    $("#id_overlay_loading").show();
    hide_reject_popup();
    $.ajax({
        type: "GET",
        url: "/ajax/reject_job_recommendation/",
        data: { job_id: job_id },
        enctype: 'multipart/form-data',
        success: function(data)
        {
            var data = jQuery.parseJSON(data);
            $("#id_ajax_rendered_content").html(data.data);
            $("#id_ajax_rendered_content").fadeIn('fast');
        },
        error: function(jqxhr, status, error)
        {

        }
    })
    .done(function( msg ) {
        $("#id_overlay_loading").hide();
        show_reject_popup();
    });
}

function show_application_withdraw_dialog(job_id, user_id) {
    $("#id_overlay_loading").show();
    //hide_reject_popup();
    $.ajax({
        type: "GET",
        url: "/ajax/job-application/withdraw/",
        data: { job_id: job_id, user_id: user_id },
        enctype: 'multipart/form-data',
        success: function(data)
        {
            var data = jQuery.parseJSON(data);
            $("#id_ajax_rendered_content").html(data.data);
            $("#id_ajax_rendered_content").fadeIn('fast');
        },
        error: function(jqxhr, status, error)
        {

        }
    })
    .done(function( msg ) {
        $("#id_overlay_loading").hide();
        show_reject_popup();
    });
}

$(document).on("click",".job_application_withdraw", function(e) {
    e.preventDefault();
    var job_id = $(this).parent().find(".job_id").val();
    var user_id = $(this).data("user-id");
    show_application_withdraw_dialog(job_id, user_id);
});

$(document).on("click","#id_job_withdraw_submit", function(e) {
    e.preventDefault();
    $(this).prop("disabled", true);
    $("#id_application_withdraw_form").prop("action","/ajax/job-application/withdraw/");
    $("#id_application_withdraw_form").submit();
});

//$("#__id_start_date").datetimepicker();

$(document).ready(function() {

    // Configure/customize these variables.
    var showChar = 300; // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "Show more >";
    var lesstext = "Show less";


    $('.more').each(function() {
        var content = $(this).html();

        if(content.length > showChar) {

            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);

            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

            $(this).html(html);
        }

    });

    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });



    /* Job Apply code start here */

    $(document).on("click","#black_overlay",function(e) {
        $("#black_overlay").fadeOut("fast");
        $("#popup_dialog").fadeOut("fast");
        $("#rd_popup_dialog").fadeOut("fast");
    });



    $(document).on("click","#id_change_start_date", function(e) {
        e.preventDefault();
        $("#id_start_date_panel").hide();
        $("#id_start_date_edit_panel").show();
        $("#id_start_date_edit_panel").css("display","inline");
        $(this).hide();

    });

    $(document).on("click","#id_change_duration", function(e) {
        e.preventDefault();
        $("#id_duration_panel").hide();
        $("#id_duration_edit_panel").show();
        $("#id_duration_edit_panel").css("display","inline");
        $(this).hide();

    });

    $(document).on("click","#id_change_rate", function(e) {
        e.preventDefault();
        $("#id_rate_panel").hide();
        $("#id_rate_edit_panel").show();
        $("#id_rate_edit_panel").css("display","inline");
        $(this).hide();

    });

    $(document).on("click","#id_job_apply_submit", function(e) {
        e.preventDefault();
        var job_id = $("#__job_id").val();
        var start_date_default = $("#__id_start_date").data("default");
        var start_date = $("#__id_start_date").val();
        var duration_default = $("#__id_duration").data("default");
        var duration = $("#__id_duration").val();
        var currency = $("#__id_currency").val();
        var rate_default = $("#__id_rate").data("default");
        var rate = $("#__id_rate").val();
        var apply_message_flag = $("#id_state_apply_message_flag").val();
        var message = $("#id_state_apply_message").val();
        var apply_repeat = $("#id_apply_repeat").val();

        if(apply_message_flag == "1") {
            var error = false;
            if($.trim(rate) == "" || $.trim(rate) == "0") {
                $("#__id_rate").addClass("error_border");
                $("#__id_rate").focus();
                error = true;
            }
            else {
                $("#__id_rate").removeClass("error_border");
            }

            if($.trim(start_date) == "") {
                $("#__id_start_date").addClass("error_border");
                $("#__id_start_date").focus();
                error = true;
            }
            else {
                $("#__id_start_date").removeClass("error_border");
            }
            if(error) {
                return false;
            }
        }

        var data = {
            job_id: job_id,
            start_date_default: start_date_default,
            start_date: start_date,
            duration_default: duration_default,
            duration: duration,
            currency: currency,
            rate_default: rate_default,
            rate: rate,
            message: message,
            apply_repeat: apply_repeat
        }

        $(this).prop("disabled",true);
        $.ajax({
            type: "POST",
            url: "/ajax/apply_job/",
            data: data,
            enctype: 'multipart/form-data',
            success: function(data)
            {
                var data = jQuery.parseJSON(data);
                if(data.status == "SUCCESS") {
                    location.reload();
                }
                else {
                    BootstrapDialog.show({
                        title: "Failed",
                        message: "Failed to apply to the job.",
                        closable: true,
                        type: BootstrapDialog.TYPE_DANGER,
                        size: BootstrapDialog.SIZE_NORMAL,
                        buttons: [
                            {
                                label: "Ok",
                                cssClass: 'btn btn-md btn-primary width120',
                                action: function(dialogRef) {
                                    dialogRef.close();
                                }
                            },
                            {
                                label: "Close",
                                cssClass: 'btn btn-md btn-warning width120',
                                action: function(dialogRef) {
                                    dialogRef.close();
                                }
                            }]
                    });
                }
            },
            error: function(jqxhr, status, error)
            {
                BootstrapDialog.show({
                    title: "Failed",
                    message: "Failed to apply to the job. An error occurred. Please try again later.",
                    closable: true,
                    type: BootstrapDialog.TYPE_DANGER,
                    size: BootstrapDialog.SIZE_NORMAL,
                    buttons: [
                        {
                            label: "Ok",
                            cssClass: 'btn btn-md btn-primary width120 text-center',
                            action: function(dialogRef) {
                                dialogRef.close();
                            }
                        },
                        {
                            label: "Close",
                            cssClass: 'btn btn-md btn-warning width120 text-center',
                            action: function(dialogRef) {
                                dialogRef.close();
                            }
                        }]
                });
                $("#id_job_apply_submit").prop("disabled",false);
            }
        })
        .done(function( msg ) {
            $("#id_job_apply_submit").prop("disabled",false);
        });




    });

    $(document).on("click","#id_job_reject_submit", function(e) {
        e.preventDefault();
        var job_id = $("#__rd_job_id").val();
        var state_reason = $("#id_state_reason").val();
        var state_reason_message = $("#id_state_reason_message").val();
        if(state_reason == "1") {
            if($.trim(state_reason_message) == "") {
                $("#id_state_reason_error").text("This field is required.").show();
                $("#id_state_reason_message").focus();
                return;
            }
        }
        $(this).prop("disabled",true);
        $.ajax({
            type: "POST",
            url: "/ajax/reject_job/",
            data: { job_id: job_id, message: state_reason_message },
            enctype: 'multipart/form-data',
            success: function(data)
            {
                var data = jQuery.parseJSON(data);
                if(data.status == "SUCCESS") {
                    location.reload();
                }
                else {
                    BootstrapDialog.show({
                        title: "Failed",
                        message: "Failed to reject the job",
                        closable: true,
                        type: BootstrapDialog.TYPE_DANGER,
                        size: BootstrapDialog.SIZE_NORMAL,
                        buttons: [
                            {
                                label: "Ok",
                                cssClass: 'btn btn-md btn-primary width120',
                                action: function(dialogRef) {
                                    dialogRef.close();
                                }
                            },
                            {
                                label: "Close",
                                cssClass: 'btn btn-md btn-warning width120',
                                action: function(dialogRef) {
                                    dialogRef.close();
                                }
                            }]
                    });
                }
            },
            error: function(jqxhr, status, error)
            {
                BootstrapDialog.show({
                        title: "Failed",
                        message: "Failed to reject the job. An error occurred. Please try again later.",
                        closable: true,
                        type: BootstrapDialog.TYPE_DANGER,
                        size: BootstrapDialog.SIZE_NORMAL,
                        buttons: [
                            {
                                label: "Ok",
                                cssClass: 'btn btn-md btn-primary width120 text-center',
                                action: function(dialogRef) {
                                    dialogRef.close();
                                }
                            },
                            {
                                label: "Close",
                                cssClass: 'btn btn-md btn-warning width120 text-center',
                                action: function(dialogRef) {
                                    dialogRef.close();
                                }
                            }]
                    });
                    $("#id_job_reject_submit").prop("disabled",false);
            }
        })
        .done(function( msg ) {
            $("#id_job_reject_submit").prop("disabled",false);
        });
    });


    $(document).on("click","#id_job_recommendation_reject_submit", function(e) {
        e.preventDefault();
        var job_id = $("#__rd_job_id").val();
        $(this).prop("disabled",true);
        $.ajax({
            type: "POST",
            url: "/ajax/reject_job_recommendation/",
            data: { job_id: job_id },
            enctype: 'multipart/form-data',
            success: function(data)
            {
                var data = jQuery.parseJSON(data);
                if(data.status == "SUCCESS") {
                    location.reload();
                }
                else {
                    BootstrapDialog.show({
                        title: "Failed",
                        message: "Failed to reject the job",
                        closable: true,
                        type: BootstrapDialog.TYPE_DANGER,
                        size: BootstrapDialog.SIZE_NORMAL,
                        buttons: [
                            {
                                label: "Ok",
                                cssClass: 'btn btn-md btn-primary width120',
                                action: function(dialogRef) {
                                    dialogRef.close();
                                }
                            },
                            {
                                label: "Close",
                                cssClass: 'btn btn-md btn-warning width120',
                                action: function(dialogRef) {
                                    dialogRef.close();
                                }
                            }]
                    });
                }
            },
            error: function(jqxhr, status, error)
            {
                BootstrapDialog.show({
                        title: "Failed",
                        message: "Failed to reject the job. An error occurred. Please try again later.",
                        closable: true,
                        type: BootstrapDialog.TYPE_DANGER,
                        size: BootstrapDialog.SIZE_NORMAL,
                        buttons: [
                            {
                                label: "Ok",
                                cssClass: 'btn btn-md btn-primary width120 text-center',
                                action: function(dialogRef) {
                                    dialogRef.close();
                                }
                            },
                            {
                                label: "Close",
                                cssClass: 'btn btn-md btn-warning width120 text-center',
                                action: function(dialogRef) {
                                    dialogRef.close();
                                }
                            }]
                    });
                    $("#id_job_reject_submit").prop("disabled",false);
            }
        })
        .done(function( msg ) {
            $("#id_job_reject_submit").prop("disabled",false);
        });
    });

    /* Job Apply code ended here. */


});

$("#id_written_lesson_tutor").select2().on("change",function(e){
    var tid = $(this).val();
    $("#id_written_lesson_subject").prop("disabled",true);
    $.ajax({
        type: "GET",
        url: "/ajax/written-lesson-subjects/",
        data: { tid: tid },
        success: function(content)
        {
            data = jQuery.parseJSON(content);
            $("#id_written_lesson_subject").html("");
            for(var i = 0 ; i < data["data"].length ; i++){
                $("#id_written_lesson_subject").append("<option value='"+ data["data"][i].id +"'>"+ data["data"][i].name +"</option>");
            }
            $("#id_written_lesson_subject").prop("disabled",false);
        },
        error: function(jqxhr, status, error)
        {

        }
    })
            .done(function( msg ) {

            });
});

//$("#id_due_date").datetimepicker({
//    //sideBySide: true
//    format: 'DD/MM/YYYY'
//});

//$("#id_due_time").datetimepicker({
//    //sideBySide: true
//    format: 'LT'
//});

$("#id_written_lesson_subject").select2().on("change",function(){

});

$(".select2-selection").css("height","35px");
$(".select2-selection").css("border","1px solid #CFCFCF");

//$('b[role="presentation"]').hide();
//$('.select2-selection__arrow').append('<i class="fa fa-shield"></i>');

$("#id_written_lesson_form_submit").click(function(e){
    e.preventDefault();
    var form_data = $("#id_written_lesson_form").serialize();
    console.log(form_data);
    $("#id_written_lesson_form").submit();
});