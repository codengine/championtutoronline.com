function show_applicant_hire_dialog(job_id, tutor_id) {
    $("#id_overlay_loading").show();
    $("#black_overlay").fadeOut('fast');
    $("#id_ajax_rendered_invite_content").fadeOut('fast');
    $.ajax({
        type: "GET",
        url: "/ajax/applicant_hire/",
        data: { job_id: job_id, tutor_id: tutor_id },
        enctype: 'multipart/form-data',
        success: function(data)
        {
            var data = jQuery.parseJSON(data);
            console.log(data.data);
            $("#black_overlay").show();
            $("#id_ajax_rendered_invite_content").html(data.data);
            $("#id_ajax_rendered_invite_content").fadeIn('fast');
        },
        error: function(jqxhr, status, error)
        {

        }
    })
    .done(function( msg ) {
        $("#id_overlay_loading").hide();
        //show_popup();
    });
}

function show_applicant_reject_dialog(job_id, tutor_id) {
    $("#id_overlay_loading").show();
    $("#black_overlay").fadeOut('fast');
    $("#id_ajax_rendered_invite_content").fadeOut('fast');
    $.ajax({
        type: "GET",
        url: "/ajax/reject_job_application/",
        data: { job_id: job_id, tutor_id: tutor_id },
        enctype: 'multipart/form-data',
        success: function(data)
        {
            var data = jQuery.parseJSON(data);
            //console.log(data.data);
            $("#black_overlay").show(); //.fadeIn('fast');
            $("#id_ajax_rendered_invite_content").html(data.data);
            $("#id_ajax_rendered_invite_content").fadeIn('fast');
        },
        error: function(jqxhr, status, error)
        {

        }
    })
    .done(function( msg ) {
        $("#id_overlay_loading").hide();
        //show_popup();
    });
}

$(document).on("click",".job_applicant_hire_cancel", function(e) {
    $("#black_overlay").fadeOut('fast');
    $("#id_ajax_rendered_invite_content").fadeOut('fast');
});

$(document).on("click",".job_applicant_reject_cancel", function(e) {
    $("#black_overlay").fadeOut('fast');
    $("#id_ajax_rendered_invite_content").fadeOut('fast');
});

$(document).on("click",".job_applicant_hire_submit", function(e) {
    e.preventDefault();
    $(this).prop("disabled", true);
    var $this = $(this);
    var lesson_date = $("input[name=hire_lesson_start_date]").val();
    var lesson_note = $("textarea[name=hire_lesson_note]").val();
    var hire_lesson_rate = $("input[name=hire_lesson_rate]").val();

    if($.trim(lesson_date) == "") {
        $("input[name=hire_lesson_start_date]").focus();
        $(this).prop("disabled", false);
        return false;
    }

    if($.trim(lesson_note) == "") {
        $("textarea[name=hire_lesson_note]").focus();
        $(this).prop("disabled", false);
        return false;
    }

    if(isNaN(parseFloat(hire_lesson_rate))) {
        $("input[name=hire_lesson_rate]").focus();
        $(this).prop("disabled", false);
        return false;
    }

    $.ajax({
        type: "POST",
        url: "/ajax/applicant_hire/",
        data: $("#id_applicant_hire_dialog_form").serialize(),
        enctype: 'multipart/form-data',
        success: function(data)
        {
            var data = jQuery.parseJSON(data);
            if(data.status == "SUCCESS") {
                location.reload();
            }
            else {

                BootstrapDialog.show({
                    title: "Hire Failed",
                    message: "Operation was unsuccessful. Please try again later.",
                    closable: false,
                    type: BootstrapDialog.TYPE_DANGER,
                    size: BootstrapDialog.SIZE_NORMAL,
                    buttons: [
                        {
                            label: "Ok",
                            cssClass: 'btn btn-md btn-primary width120',
                            action: function(dialogRef) {
                                $this.prop("disabled", false);
                                dialogRef.close();
                            }
                        },
                        {
                            label: "Cancel",
                            cssClass: 'btn btn-md btn-warning width120',
                            action: function(dialogRef) {
                                $this.prop("disabled", false);
                                dialogRef.close();
                            }
                        }]
                });


            }
        },
        error: function(jqxhr, status, error)
        {
            BootstrapDialog.show({
                    title: "Hire Failed",
                    message: "Operation was unsuccessful due to an unexoected error in the server. Please try again later.",
                    closable: false,
                    type: BootstrapDialog.TYPE_DANGER,
                    size: BootstrapDialog.SIZE_NORMAL,
                    buttons: [
                        {
                            label: "Ok",
                            cssClass: 'btn btn-md btn-primary width120',
                            action: function(dialogRef) {
                                $this.prop("disabled", false);
                                dialogRef.close();
                            }
                        },
                        {
                            label: "Cancel",
                            cssClass: 'btn btn-md btn-warning width120',
                            action: function(dialogRef) {
                                $this.prop("disabled", false);
                                dialogRef.close();
                            }
                        }]
                });
        }
    })
    .done(function( msg ) {

    });
});

$(document).on("click","#black_overlay", function(e) {
    $("#black_overlay").hide();
    $("#id_ajax_rendered_invite_content").hide();
});

$(document).ready(function() {
    $(document).on("click",".job_application_action_hire", function(e) {
        e.preventDefault();
        var job_id = $(this).parent().parent().find(".job_id").val();
        var tutor_id = $(this).parent().parent().find(".tutor_id").val();
        show_applicant_hire_dialog(job_id, tutor_id);
    });

    $(document).on("click",".job_application_negotiate", function(e) {
        e.preventDefault();



        var tutor_id = $(this).parent().find(".tutor_id").val();
        var job_id = $(this).parent().find(".job_id").val();

        var online_status = "online";
        var tutor_name = $(this).parent().find("input[name=tutor_name]").val();
        openChatPopup(0, tutor_id, tutor_name, online_status, [ parseInt(tutor_id) ], "", "", 1);
    });

    $(document).on("click",".job_application_message", function(e) {
        e.preventDefault();
        var tutor_id = $(this).parent().find(".tutor_id").val();
        var job_id = $(this).parent().find(".job_id").val();

        var online_status = "online";
        var tutor_name = $(this).parent().find("input[name=tutor_name]").val();
        openChatPopup(0, tutor_id, tutor_name, online_status, [ parseInt(tutor_id) ], "", "", 1);
    });

    $(document).on("click",".job_application_action_shortlist", function(e) {
        e.preventDefault();
        var tutor_id = $(this).parent().parent().find(".tutor_id").val();
        var job_id = $(this).parent().parent().find(".job_id").val();
        $.ajax({
            type: "POST",
            url: "/ajax/update_job_status_shortlisted/",
            data: { job_id: job_id, tutor_id: tutor_id },
            enctype: 'multipart/form-data',
            success: function(data)
            {
                var data = jQuery.parseJSON(data);
                if(data.status == "SUCCESS") {
                    location.reload();
                }
            },
            error: function(jqxhr, status, error)
            {
                BootstrapDialog.show({
                    title: "Failed",
                    message: "Operation couldn't be performed. There was a problem while processing the request. Please retry again or reload the page.",
                    closable: false,
                    type: BootstrapDialog.TYPE_DANGER,
                    size: BootstrapDialog.SIZE_NORMAL,
                    buttons: [
                        {
                            label: "Ok",
                            cssClass: 'btn btn-md btn-primary width120',
                            action: function(dialogRef) {
                                dialogRef.close();
                            }
                        },
                        {
                            label: "Cancel",
                            cssClass: 'btn btn-md btn-warning width120',
                            action: function(dialogRef) {
                                dialogRef.close();
                            }
                        }]
                });
            }
        })
        .done(function( msg ) {

        });
    });
    $(document).on("click",".job_application_action_reject", function(e) {
        e.preventDefault();
        var tutor_id = $(this).parent().parent().find(".tutor_id").val();
        var job_id = $(this).parent().parent().find(".job_id").val();
        show_applicant_reject_dialog(job_id, tutor_id);
    });
});