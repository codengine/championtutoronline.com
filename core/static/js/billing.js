function validate(element) {
    var elem = $("input[name="+element+"]");
    if($.trim(elem.val()) == "") {
        //elem.focus();
        //elem.parent().find(".error").text("This field is required.");
        return false;
    }
    return true;
}

function validate_creditcardnumber(value)
{
    var re16digit=/^\d{16}$/
    if (value.search(re16digit)==-1) {
        return false;
    }
    else {
        return true
    }
}

function show_message_dialog($title,$message) {
    BootstrapDialog.show({
    title: $title,
    message: $message,
    closable: true,
    type: BootstrapDialog.TYPE_DANGER,
    size: BootstrapDialog.SIZE_NORMAL,
    buttons: [
        {
            label: "Ok",
            cssClass: 'btn btn-md btn-primary width120',
            action: function(dialogRef) {
                dialogRef.close();
            }
        },
        {
            label: "Cancel",
            cssClass: 'btn btn-md btn-warning width120',
            action: function(dialogRef) {
                dialogRef.close();
            }
        }]
});
}

$(document).ready(function() {

    $(document).on("click","#id_action_credit_card_dialog_close", function(e) {
        e.preventDefault();
        $("#black_overlay").hide();
        $("#id_ajax_rendered_content").hide();
        $("#id_overlay_loading").hide();
    });

    $(document).on("click","#black_overlay", function(e) {
        e.preventDefault();
        $("#black_overlay").hide();
        $("#id_ajax_rendered_content").hide();
        $("#id_overlay_loading").hide();
    });

    $(document).on("click","#id_cc_dialog_close", function(e) {
        e.preventDefault();
        $("#black_overlay").hide();
        $("#id_ajax_rendered_content").hide();
        $("#id_overlay_loading").hide();
    });

    $(document).on("click","#id_add_new_credit_card", function(e) {
        e.preventDefault();
        var $this = $(this);
        $("#id_overlay_loading").show();
        $("#black_overlay").hide();
        $("#id_ajax_rendered_content").hide();
        $.ajax({
            type: "GET",
            url: "/ajax/add_cc/",
            data: {},
            timeout: 3000,
            enctype: 'multipart/form-data',
            success: function(data)
            {
                var data = jQuery.parseJSON(data);
                if(data.status == "SUCCESS") {
                    $("#black_overlay").show();
                    $("#id_overlay_loading").hide();
                    $("#id_ajax_rendered_content").show();
                    $("#id_ajax_rendered_content").html(data.data.dialog_content);
                }
                else {

                }
            },
            error: function(jqxhr, status, error)
            {
                if(status === "timeout") {
                    $("#id_overlay_loading").hide();
                }
            }
        })
        .done(function( msg ) {
            $this.prop("disabled", false);
            $("#id_overlay_loading").hide();
        });
    });

    function validate_cc_form() {
        var $card_number = $("input[name=card_number]");
        var valid = true;
        if(!validate("card_number")) {
            $card_number.parent().find(".cc_error").html("Enter a valid credit card number").show();
            $card_number.focus();
            valid &= false;
        }
        else if(!validate_creditcardnumber($.trim($card_number.val()))) {
            $card_number.parent().find(".cc_error").html("Enter a valid credit card number").show();
            $card_number.focus();
            valid &= false;
        }
        else {
            $card_number.parent().find(".cc_error").html("").hide();
        }
        var $card_cvv = $("input[name=card_cvv]");
        if(!validate("card_cvv")) {
            $card_cvv.parent().find(".cc_error").html("This field is required.").show();
            $card_cvv.focus();
            valid &= false;
        }
        else {
            $card_cvv.parent().find(".cc_error").hide();
        }
        var $first_name = $("input[name=first_name]");
        if(!validate("card_cvv")) {
            $first_name.parent().find(".cc_error").html("This field is required.").show();
            $first_name.focus();
            valid &= false;
        }
        else {
            $first_name.parent().find(".cc_error").hide();
        }
        var $last_name = $("input[name=last_name]");
        if(!validate("card_cvv")) {
            $last_name.parent().find(".cc_error").html("This field is required.").show();
            $last_name.focus();
            valid &= false;
        }
        else {
            $last_name.parent().find(".cc_error").hide();
        }
        return valid;
    }

    $(document).on("click","#id_action_credit_card_dialog_submit", function(e) {
        e.preventDefault();
        var $this = $(this);
        $this.prop("disabled", true);
        $("#id_credit_card_form_dialog").css("cursor","wait");

        if(validate_cc_form()) {
            var url = "/ajax/add_cc/";
            //console.log(url);
            //$("form[name=credit_card_create]").prop("action",url);
            //$("form[name=credit_card_create]").submit();

            $.ajax({
                type: "POST",
                url: url,
                data: $("form[name=cc_form]").serialize(),
                enctype: 'multipart/form-data',
                success: function(data)
                {
                    var data = jQuery.parseJSON(data);
                    if(data.status == "SUCCESS") {
                        location.href = "/billing";
                    }
                    else {
                        show_message_dialog("Credit card add failed","Your credit card has not been added. Please check with your card and try again or try a new one.");
                    }
                },
                error: function(jqxhr, status, error)
                {
                    show_message_dialog("Credit card add failed","Your credit card has not been added. Please check with your card and try again or try a new one.");
                    $this.prop("disabled", false);
                }
            })
            .done(function( msg ) {
                $this.prop("disabled", false);
            });
        }
        else {
            $this.prop("disabled", false);
            $("#id_credit_card_form_dialog").css("cursor","pointer");
        }
    });
    $(document).on("click",".remove_payment_method", function(e) {
        e.preventDefault();
        var $this = $(this);
        var pid = $(this).data("pid");
        $.ajax({
            type: "POST",
            url: "/ajax/remove_cc/",
            data: { pid: pid, csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value },
            enctype: 'multipart/form-data',
            success: function(data)
            {
                var data = jQuery.parseJSON(data);
                if(data.status == "SUCCESS") {
                    location.href = "/billing";
                }
                else {
                    show_message_dialog("Credit card remove failed","Your credit card has not been removed. Please try again later.");
                }
            },
            error: function(jqxhr, status, error)
            {
                show_message_dialog("Credit card remove failed","Your credit card has not been removed. Please try again later.");
            }
        })
        .done(function( msg ) {
            $this.prop("disabled", false);
        });
    });
});