$(document).ready(function() {
    function init_tour() {
          //var tour = new Tour({
          //steps: [
          //{
          //  element: "#id_whiteboard_bottom_bar",
          //  title: "Audio/Video sharing",
          //  content: "In live session this tool allows you to share your audio/video to other party of this lesson. Use this tool to talk with other parties in the lesson. You can enable or disable the sharing anytime you want."
          //}
        //]});

        // Initialize the tour
        //tour.init();
        //console.log(tour);
        //return tour;

        var tour = new Tour({storage: false});

          // Add your steps. Not too many, you don't really want to get your users sleepy
          tour.addSteps([

            {
                element: "#id_tour_toolset",
                title: "Drawing Tools",
                content: "Use these tools to draw line, shapes, other geometric shapes in the drawingboard in the live session"
            },
            {
                element: "#id_tab_text_editor",
                title: "Text editor sharing",
                content: "Click this tab to load text editors. You can add as many text editors you want. Use text editor to edit a document interactively"
            },
            {
                element: "#id_tab_code_editor",
                title: "Code editor sharing",
                content: "This tool helps you to enable code sharing. Click here to load code pads that you will use to write and share code with other parties in the live session"
            },
            {
                element: "#id_screensharing_control",
                title: "Screensharing control",
                content: "You can control the sharing of your screen. To enable screen sharing switch on this button. If you want to disable the sharing just toggle the button to switch off"
            },
            {
                element: "#id_video_sharing_control",
                title: "Video control",
                content: "Use this button to enable to disable video sharing from your camera"
            },
            {
                element: "#id_audio_sharing_control",
                title: "Audio control",
                content: "Use this button to enable to disable audio sharing from your mic"
            },
            {
              element: "#id_wb_sharing_tool",
              title: "Audio/Video sharing",
              content: "In live session this tool allows you to share your audio/video to other party of this lesson. Use this tool to talk with other parties in the lesson. You can enable or disable the sharing anytime you want."
            },
            {
                element: "#id_wb_chat_",
                title: "Tutor Chat",
                content: "You can chat with other participants in this lesson. To enable chat click this button"
            },
            {
                element: "#id_canvas_add_button",
                title: "New boards",
                content: "You can add as many new drawingboards you want. Use this tool to add a new drawing canvas."
            },
         ]);

         // Initialize the tour
         tour.init();
         return tour;

    }

    $(document).on("click", "#id_wb_help_tour", function(e) {
        e.preventDefault();
        tour = init_tour();
        tour.start();
    });
})