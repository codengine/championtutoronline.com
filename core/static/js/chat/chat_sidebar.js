var $chat_buddylist_ajax = null;

function fetch_buddy_list_ajax(data,callback,error_back,complete_back){
    $chat_buddylist_ajax = $.ajax({
        type: "POST",
        url: "/ajax/fetch_buddies/",
        data: data,
        enctype: 'multipart/form-data',
        success: function(data)
        {
            callback(jQuery.parseJSON(data));
        },
        error: function(jqxhr, status, error)
        {
            error_back(jqxhr, status, error);
        }
    })
    .done(function( msg ) {
        complete_back(msg);
    });
};

$(document).ready(function(){

    $("#id_nav_how_works").click(function(e) {
          e.preventDefault();
          $('html, body').animate({
            scrollTop: $("#id_section_nav_how_works").offset().top
        }, 1000, function() {

        });
    });

    $("#id_nav_finding_tutor").click(function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $("#id_section_finding_tutor").offset().top - 100
        }, 1000, function() {
           $("#id_home_search_tutor").focus();
        });
    });

    $("#id_nav_help").click(function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $("#p1").offset().top
        }, 1000, function() {

        });
    });

    $("form[name=view_tutor]").submit(function(e) {
        e.preventDefault();
        var keyword = $("#id_home_search_tutor").val();
        console.log(keyword);
        if(keyword == "") {
            $("#id_home_search_tutor").focus();
            return false;
        }
        return true;
    });



    var fetch_buddy_callback = function(data){
//        console.log(data);
        var html = "";
        if(data.data.chat_list) {
            $(".tutors_chat_list").html(data.data.chat_list);
        }
    };

    fetch_buddy_list_ajax({},fetch_buddy_callback,function(jqxhr, status, error){},function(msg){});

    $(document).on("click",".show_more_chat", function(e) {
        e.preventDefault();
        $(this).prop("disabled", true);
        var _this = $(this);
//        var major_name = $(this).find(".major_name").val();

        var excludes = "";
        $(this).parent().find("li").each(function(i) {
            var uid = $(this).find(".tutor_uid").val()
            if(typeof uid != "undefined") {
                excludes += uid+",";
            }
        });

        var section = $(this).find(".major_name").val();

        //console.log(excludes);

        fetch_buddy_list_ajax({ section: section, kind: "show_more", "excludes": excludes},function(data){

            var html = "";
            if(data.data.items) {
                for(var i = 0 ; i < data.data.items.length ; i++) {
                    html += data.data.items[i];
                }
            }
            $(html).insertBefore(_this);
            if(data.data.show_more == 0) {
               _this.hide();
            }
        },function(jqxhr, status, error){},function(msg){
            _this.prop("disabled", false);
        });
    });


    var check_user_status = function(){
        var tutors_list = "";
        $(".tutors_chat_list li").each(function(i) {
            var tutor_id = $(this).find(".tutor_uid").val();
            if(typeof tutor_id != "undefined") {
                tutors_list += tutor_id+",";
            }
        });
        fetch_buddy_list_ajax({ kind: "check_status", format: "json", tutors: tutors_list },function(data){

            if(data.data.buddies) {
                for(var i = 0 ; i < data.data.buddies.length ; i++) {
                    user = data.data.buddies[i];
                    if(window.chat_boxes.hasOwnProperty(user.id)) {
                        if(user.online_status == "online") {
                            window.chat_boxes[user.id].goOnline();
                        }
                        else if(user.online_status == "offline") {
                            window.chat_boxes[user.id].goOffline();
                        }
                    }
                    $(".row_"+user.id).each(function(i) {
                       var _row = $(this);
                        _row.find(".online_status").val(user.online_status);
                        _row.find(".tut_name").val(user.name);
                        _row.find(".tutor_pp").removeClass("tutors_chat_img");
                        _row.find(".tutor_pp").removeClass("tutors_chat_img_teach_available");
                        _row.find(".tutor_pp").removeClass("tutors_chat_img_offline");
                        _row.find(".tutor_pp").addClass(user.tutor_chat_status_class);
                    });
                }
            }
        },function(jqxhr, status, error){},function(msg){
            //_this.prop("disabled", false);
        });
    };

    var delay = (function(){
      var timer = 0;
      return function(callback, ms){
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
      };
    })();

    $(document).on("keyup","#id_chat_buddy_search", function(e) {
        e.preventDefault();
        delay(function(){
            if($chat_buddylist_ajax != null) {
                $chat_buddylist_ajax.abort();
                $chat_buddylist_ajax = null;
            }
            var q = $("#id_chat_buddy_search").val();
            fetch_buddy_list_ajax({ q: q },fetch_buddy_callback,function(jqxhr, status, error){},function(msg){});
        }, 100 );
    });
//    setTimeout(check_user_status,1000);
    if(window.champ_user_id != undefined && window.champ_user_id != -1) {
        setInterval(check_user_status, 5000);
    }
});