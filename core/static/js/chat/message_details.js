$(document).ready(function() {
    if($('#id_message_list').length) {
        $('#id_message_list').scrollTop($('#id_message_list')[0].scrollHeight);
    }

    $(document).on("click", "textarea[name=msg_text]", function(e) {
        e.preventDefault();

        var chat_type = $("#id_page_chat_type").val();
        var __chat_id = $("#id_page_chat_id").val();

        window.mark_messages_read(chat_type, __chat_id, "all", function(data) {

        },
        function(jqxhr, status, message) {

        },
        function(msg) {

        });
    });

    var $ajax_request = null;

    function send_message() {

        if(window.max_file_size_over) {
            return false;
        }

        var fd = new FormData($("form[name=msg_form]")[0]);
        var receivers = "";
        var hidden_tutor_id = $("input[name=id_hidden_tutor]").val();
        //alert(hidden_tutor_id);
        receivers += hidden_tutor_id;
        fd.append('receivers', receivers);
        fd.append('csrfmiddlewaretoken',$("form[name=msg_form]").find('input[name=csrfmiddlewaretoken]').val());
        fd.append('message', $("textarea[name=msg_text]").val());
        fd.append('file', $('input[name=attached_file]')[0].files[0]);
        fd.append('chat_type', 0); //p2p chat.
        fd.append('chat_id', -1);
        $(".submit_msg_detail_action").prop('disabled', true);
        $("#id_fupload_progress_container").hide();
        $(".file_upload_progress > div").css("width","0%");
        $(".file_upload_progress > div").text("0%");

        //console.log(fd);
        $ajax_request = $.ajax({
            type: "POST",
            url: "/ajax/send-message/",
            data: fd,
            contentType: false,
            processData: false,
            xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                if(myXhr.upload){
                    myXhr.upload.addEventListener('progress',progress, false);
                }
                return myXhr;
            },
            cache:false,
            success: function(data)
            {
                var data = jQuery.parseJSON(data);
                if(data.status == "SUCCESS"){
                    $("textarea[name=msg_text]").val("");
                    $("input[name=attached_file]").val("");
                    $("#file").val("");
                    $("textarea[name=msg_text]").focus();
                }
                else{

                }
            },
            error: function(jqxhr, status, error)
            {

            }
        })
        .done(function( msg ) {
            $(".submit_msg_detail_action").prop('disabled', false);
        });
    };

    function send_conversation_message() {

        if(window.max_file_size_over) {
            return false;
        }

        var fd = new FormData($("form[name=msg_form]")[0]);
        var receivers = [];
        var conversation_users = "";
        $(".conv_users").each(function(i) {
            conversation_users += $(this).val()+",";
        });
        //alert(conversation_users);
        fd.append('receivers', conversation_users);
        fd.append('csrfmiddlewaretoken',$("form[name=msg_form]").find('input[name=csrfmiddlewaretoken]').val());
        fd.append('message', $("textarea[name=msg_text]").val());
        fd.append('file', $('input[name=attached_file]')[0].files[0]);
        fd.append('chat_type', 1); //p2p chat.
        fd.append('chat_id', $("form[name=msg_form]").find('input[name=group_id]').val());
        $(".submit_conv_msg_form").prop("disabled", true);
        $("#id_fupload_progress_container").hide();
        $(".file_upload_progress > div").css("width","0%");
        $(".file_upload_progress > div").text("0%");

        //console.log(fd);
        $ajax_request = $.ajax({
            type: "POST",
            url: "/ajax/send-message/",
            data: fd,
            contentType: false,
            processData: false,
            xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                if(myXhr.upload){
                    myXhr.upload.addEventListener('progress',progress, false);
                }
                return myXhr;
            },
            cache:false,
            success: function(data)
            {
                var data = jQuery.parseJSON(data);
                if(data.status == "SUCCESS"){
                    $("textarea[name=msg_text]").val("");
                    $("input[name=attached_file]").val("");
                    $("#file").val("");
                    $("textarea[name=msg_text]").focus();
                }
                else{

                }
            },
            error: function(jqxhr, status, error)
            {

            }
        })
        .done(function( msg ) {
            $(".submit_conv_msg_form").prop("disabled", false);
        });
    }

    function progress(e){

        if(e.lengthComputable){
            console.log();
            console.log(e.loaded);
            console.log(e.total);
            if($('input[name=attached_file]').val() != "") {
                var max = e.total;
                var current = e.loaded;

                var Percentage = parseInt((current * 100)/max);
                $("#id_fupload_progress_container").show();
                $(".file_upload_progress > div").css("width",Percentage+"%");
                $(".file_upload_progress > div").text(Percentage+"%");


                if(Percentage >= 100)
                {
                   $('input[name=attached_file]').val("");
                   $("#id_fupload_progress_container").hide();
                }
            }
        }
     }

    $(document).on("click",".submit_msg_detail_action", function(e) {
        e.preventDefault();
        var msg_text = $("textarea[name=msg_text]").val();
        var attached_file = $('input[name=attached_file]').val();
        if(msg_text == "" && attached_file == "") {
            $("textarea[name=msg_text]").focus();
            return false;
        }
        send_message();
    });

    $(document).on("click",".submit_conv_msg_form", function(e) {
        e.preventDefault();
        var msg_text = $("textarea[name=msg_text]").val();
        var attached_file = $('input[name=attached_file]').val();
        if(msg_text == "" && attached_file == "") {
            $("textarea[name=msg_text]").focus();
            return false;
        }
        send_conversation_message();
    });

    $(document).on("click", "#id_cancel_file_upload" , function(e) {
        e.preventDefault();
        if($ajax_request != null) {
            $ajax_request.abort();
            $ajax_request = null;
            $('input[name=attached_file]').val("");
            $('#file').val("");
            $(".file_upload_progress > div").css("width","0%");
            $(".file_upload_progress > div").text("0%");
            $("#id_fupload_progress_container").hide();
            $(".submit_msg_detail_action").prop("disabled", false);
            $(".submit_conv_msg_form").prop("disabled", false);
        }
    });

    var previous_timestamp = $("#id_message_list").find("li:first").find(".last_message_id").val();
    $(document).on("click",".btn_load_old_msg",function(e){
        $("#id_message_loading").show();
        //alert("sadsad");
        e.preventDefault();
        var last_msg_id = $("#id_message_list").find("li:first").find(".last_message_id").val();
        if(last_msg_id == undefined || last_msg_id == null) {
            last_msg_id = -1;
        }
        $.ajax({
            type: "GET",
            url: "/ajax/load-message/",
            data: {
                "receiver_id": $("#id_message_to").val(),
                "start": last_msg_id,
                "is_delete_enabled": $(".is_delete_enabled").val()
            },
            success: function(data)
            {
                data = jQuery.parseJSON(data);
                //console.log(data.data);
                if(data.status == "SUCCESS" && data.data.contents.length > 0){
                    for(var j = 0 ;  j < data.data.contents.length ; j++) {
                        $("#id_message_list").prepend(data.data.contents[j]);
                    }
                    $('#id_message_list').animate({
                        scrollTop: 0
                    }, 1000);
                    $("#id_message_loading").fadeOut("fast");
                }
                else if(data.status == "SUCCESS" && data.data.contents.length == 0){
                    $(".load_old_msg").prop("disabled",true);
                    $("#id_message_loading").fadeOut("fast");
                }
                else if(data.STATUS == "FAILURE"){
                    $("#id_message_loading").fadeOut("fast");
                }
            },
            error: function(jqxhr, status, error)
            {

            }
        })
        .done(function( msg ) {

        });
    });


    var previous_timestamp = $("#id_message_list").find("li:first").find(".last_message_id").val();
    $(document).on("click",".btn_load_old_conversation_msg",function(e){
        $("#id_message_loading").show();
        //alert("sadsad");
        e.preventDefault();
        var last_msg_id = $("#id_message_list").find("li:first").find(".last_message_id").val();
        if(last_msg_id == undefined || last_msg_id == null) {
            last_msg_id = -1;
        }
        var conversation_id = $(this).data("conv-id");
        $.ajax({
            type: "GET",
            url: "/ajax/load-conversation/"+conversation_id+"/",
            data: {
                "receiver_id": $("#id_message_to").val(),
                "start": last_msg_id,
                "is_delete_enabled": $(".is_delete_enabled").val()
            },
            success: function(data)
            {
                data = jQuery.parseJSON(data);
                //console.log(data.data);
                if(data.status == "SUCCESS" && data.data.contents.length > 0){
                    for(var j = 0 ;  j < data.data.contents.length ; j++) {
                        $("#id_message_list").prepend(data.data.contents[j]);
                    }
                    $('#id_message_list').animate({
                        scrollTop: 0
                    }, 1000);
                    $("#id_message_loading").fadeOut("fast");
                }
                else if(data.status == "SUCCESS" && data.data.contents.length == 0){
                    $(".load_old_msg").prop("disabled",true);
                    $("#id_message_loading").fadeOut("fast");
                }
                else if(data.STATUS == "FAILURE"){
                    $("#id_message_loading").fadeOut("fast");
                }
            },
            error: function(jqxhr, status, error)
            {

            }
        })
        .done(function( msg ) {

        });
    });

    $(document).on("keypress","textarea[name=msg_text]", function(event) {
        if($("input[name=enter_checkbox]").is(":checked")) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if(keycode == '13'){
                var chat_type = $("form[name=msg_form]").find("input[name=chat_type]").val();
                if(chat_type == "0") {
                    send_message()
                }
                else if(chat_type == "1") {
                    send_conversation_message();
                }
                return false;
            }
        }
    });

    $("input[name=enter_checkbox]").change(function(e) {
         var enter_to_send = 0;
         if($(this).is(":checked")) {
            enter_to_send = 1;
         }
         else {
            enter_to_send = 0;
         }

         $.ajax({
            type: "POST",
            url: "/ajax/chat_settings/",
            data: { action: "message_details_enter", status: enter_to_send },
            enctype: 'multipart/form-data',
            success: function(data)
            {
                var data = jQuery.parseJSON(data);
            },
            error: function(jqxhr, status, error)
            {

            }
        })
        .done(function( msg ) {

        });

    });

    $(document).on("click", "#id_open_chatbox", function(e) {
        e.preventDefault();
        var chat_type = $(this).data("chat-type");
        var chat_id = $(this).data("chat-id");
        var chat_title = $(this).data("chat-title");
        var topic_class_name = $(this).data("topic-class-name");
        var topic_object_id = $(this).data("topic-object-id");
        var remote_peers = [];
        if(chat_type == "0") {
            remote_peers = [ parseInt(chat_id) ];
        }
        else {
            $(".conv_users").each(function(i) {
                remote_peers.push(parseInt($(this).val()));
            });
        }
//        console.log(remote_peers);
        window.openChatPopup(chat_type, chat_id, chat_title, "online", remote_peers, topic_class_name, topic_object_id, 1, false);
    });

    var $bootstrap_dialog = null;
    $(document).on("click", "#id_add_buddy_to_message_conversation", function(e) {
        e.preventDefault();
        $("#black_overlay").show();
        $("#add_buddy_to_conversation_form").find("input[name=conversation_id]").val($(this).data("conversation-id"));
        $("#add_buddy_to_conversation").show();
    });

    $(document).on("click", ".close_button", function(e) {
        e.preventDefault();
        if($bootstrap_dialog != null) {
            $bootstrap_dialog.close();
        }
    });

    $(document).on("click", ".add_button", function(e) {
        e.preventDefault();

    });

    $(document).on("click", "#id_leave_conversation", function(e) {
        e.preventDefault();
        var conversation_id = $(this).data("conversation-id");
        var action_url = $(this).data("action-url");
        $bootstrap_dialog = BootstrapDialog.show({
            title: "Confirm the action",
            type: BootstrapDialog.TYPE_DEFAULT,
            message: function(dialog) {
                return "Are you sure you want to leave this conversation?"
            },
            buttons: [
            {
                label: 'Confirm',
                cssClass: 'btn-danger',
                action: function(){
                    $.ajax({
                        type: "POST",
                        url: action_url,
                        data: {
                            conversation_id: conversation_id
                        },
                        success: function(data)
                        {
                            data = jQuery.parseJSON(data);
                            //console.log(data);
                            if(data.status == "SUCCESS"){
                                location.reload();
                            }
                            else if(data.status == "FAILURE"){
                                if($bootstrap_dialog != null) {
                                    $bootstrap_dialog.close();

                                    BootstrapDialog.show({
                                        title: "Operation failed",
                                        type: BootstrapDialog.TYPE_DEFAULT,
                                        message: function(dialog) {
                                            return "Operation could not be performed. Either you have already left the conversation or something wrong happened in the server."
                                        },
                                        buttons: [{
                                            label: 'Close',
                                            action: function(dialogItself){
                                                dialogItself.close();
                                            }
                                        }]
                                        });

                                }
                            }
                        },
                        error: function(jqxhr, status, error)
                        {

                        }
                    })
                    .done(function( msg ) {

                    });
                }
            },
            {
                label: 'Close',
                action: function(dialogItself){
                    dialogItself.close();
                }
            }]
        });
    });

    $(document).on("click", "#id_delete_messages", function(e) {
       e.preventDefault();
       $(".is_delete_enabled").val("1");
       $("#id_delete_control_pane").show();
       $(".submit_msg_detail_action").prop("disabled",true);
       $(".submit_conv_msg_form").prop("disabled",true);
       $(".chat_delete_checkbx").each(function(i) {
           $(this).parent().show();
       });
    });
    $(document).on("click", ".hide_delete_control", function(e) {
       e.preventDefault();
       $(".is_delete_enabled").val("0");
       $("#id_delete_control_pane").hide();
       $(".submit_msg_detail_action").prop("disabled",false);
       $(".submit_conv_msg_form").prop("disabled",false);
       $(".chat_delete_checkbx").each(function(i) {
           $(this).parent().hide();
       });
    });

    $(document).on("change", ".all_select", function(e) {
       e.preventDefault();
       if($(this).is(":checked")) {
           $(".chat_delete_checkbx").each(function(i) {
               $(this).prop("checked", true);
           });
       }
       else {
            $(".chat_delete_checkbx").each(function(i) {
               $(this).prop("checked", false);
           });
       }
    });

    $(document).on("change", ".chat_delete_checkbx", function(e) {
       e.preventDefault();
       var all_checked = true;
       if($(this).is(":checked")) {
           $(".chat_delete_checkbx").each(function(i) {
                all_checked &= $(this).is(":checked");
           });
           if(all_checked) {
                $(".all_select").prop("checked", true);
           }
       }
       else {
           $(".all_select").prop("checked", false);
       }
    });

    $(document).on("click", ".message_delete_selected", function(e) {
       e.preventDefault();
       var message_ids = "";
       var li_objects = [];
       $(".chat_delete_checkbx").each(function(i) {
            if($(this).is(":checked")) {
                message_ids += $(this).data("msg-id")+",";
                li_objects.push($(this).parent().parent());
            }
       });

       if(message_ids != "") {
            $bootstrap_dialog = BootstrapDialog.show({
                title: "Confirm the action",
                type: BootstrapDialog.TYPE_DEFAULT,
                message: function(dialog) {
                    return "Are you sure you want to leave this conversation?"
                },
                buttons: [
                {
                    label: 'Confirm',
                    cssClass: 'btn-danger',
                    action: function(){
                        $.ajax({
                            type: "POST",
                            url: "/ajax/delete_message/",
                            data: {
                                message_ids: message_ids
                            },
                            success: function(data)
                            {
                                data = jQuery.parseJSON(data);
                                //console.log(data);
                                if(data.status == "SUCCESS"){
                                    for(var i = 0 ; i < li_objects.length ; i++) {
                                        li_objects[i].remove();
                                    }
                                    if($bootstrap_dialog != null) {
                                        $bootstrap_dialog.close();
                                    }
                                }
                                else if(data.status == "FAILURE"){
                                    if($bootstrap_dialog != null) {
                                        $bootstrap_dialog.close();

                                        BootstrapDialog.show({
                                            title: "Operation failed",
                                            type: BootstrapDialog.TYPE_DEFAULT,
                                            message: function(dialog) {
                                                return "Operation could not be performed. Something wrong happened in the server."
                                            },
                                            buttons: [{
                                                label: 'Close',
                                                action: function(dialogItself){
                                                    dialogItself.close();
                                                }
                                            }]
                                            });

                                    }
                                }
                            },
                            error: function(jqxhr, status, error)
                            {

                            }
                        })
                        .done(function( msg ) {

                        });
                    }
                },
                {
                    label: 'Close',
                    action: function(dialogItself){
                        dialogItself.close();
                    }
                }]
           });
        }

    });

    $(document).on("click","#id_rename_conversation",function(e){
        e.preventDefault();
        var chat_id = $(this).data("chat-id");
        var chat_title = $("#id_conv_title").text();
        var title = "";
        $bootstrap_dialog = BootstrapDialog.show({
            title: "Change conversation title",
            type: BootstrapDialog.TYPE_DEFAULT,
            message: function(dialog) {
                $content = $("<div style=\"padding: 7px;\"><input class=\"form-control\" type=\"text\" id-\"id_conversation_title_input\" placeholder=\"Enter conversation title here\" value=\""+ chat_title +"\"/></div>");
                $content.find("input").change(function(e) {
                    title = $(this).val();
                });
                return $content;
            },
            buttons: [
            {
                label: 'Confirm',
                cssClass: 'btn btn-default',
                action: function(dialogRef){
                    $(this).prop("disabled", true);
                    var $confirm_button = $(this);
                    $.ajax({
                        type: "POST",
                        url: "/ajax/rename_conversation/",
                        data: {
                            chat_id: chat_id,
                            title: title
                        },
                        success: function(data)
                        {
                            data = jQuery.parseJSON(data);
                            //console.log(data);
                            if(data.status == "SUCCESS"){
                                BootstrapDialog.show({
                                    title: "Changed Title Successfully",
                                    type: BootstrapDialog.TYPE_DEFAULT,
                                    message: function(dialog) {
                                        return "Conversation title changed to "+title
                                    },
                                    buttons: [{
                                        label: 'Close',
                                        action: function(dialogItself){
                                            dialogItself.close();
                                            dialogRef.close();
                                        }
                                    }]
                                    });
                            }
                            else if(data.status == "FAILURE"){
                                BootstrapDialog.show({
                                    title: "Operation failed",
                                    type: BootstrapDialog.TYPE_DEFAULT,
                                    message: function(dialog) {
                                        return "Operation could not be performed. Something wrong happened in the server."
                                    },
                                    buttons: [{
                                        label: 'Close',
                                        action: function(dialogItself){
                                            dialogItself.close();
                                        }
                                    }]
                                    });
                            }
                        },
                        error: function(jqxhr, status, error)
                        {
                            BootstrapDialog.show({
                                title: "Operation failed",
                                type: BootstrapDialog.TYPE_DEFAULT,
                                message: function(dialog) {
                                    return "Operation could not be performed. Something wrong happened in the server."
                                },
                                buttons: [{
                                    label: 'Close',
                                    action: function(dialogItself){
                                        dialogItself.close();
                                    }
                                }]
                            });
                        }
                    })
                    .done(function( msg ) {
                        $confirm_button.prop("disabled", false);
                    });
                }
            },
            {
                label: 'Cancel',
                cssClass: 'btn btn-default',
                action: function(dialogRef){
                 dialogRef.close();
                }
            }]
            });
    });

    $(document).on("click",".tag_close",function(e){
        e.preventDefault();
        $(this).parent().remove();
        if($(this).hasClass("tag_study_partners")) {
            var id = $(this).find("input").val();
            $("#id_lesson_payer").find("option[value="+ id +"]").remove();
        }
    });

    function get_target_user_type() {
        var current_user_type = $("#id_select_tutor").data("user-type");
        if(current_user_type == "teacher") {
            return "student";
        }
        else if(current_user_type == "student") {
            return "teacher"
        }
    }

    $("#id_select_tutor").select2({
      height: 90,
      width: 300,
      placeholder: "Name or email address",
      ajax: {
        url: "/ajax/tutor-list/",
        dataType: 'json',
        delay: 250,
        data: function (params) {
          return {
            q: params.term, // search term
            utype: get_target_user_type()
          };
        },
        processResults: function (data, page) {
          return {
                results: $.map(data.items, function(obj) {
                    return { id: obj.id, text: obj.name };
                })
            };
        },
        cache: true
      },
      escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
      minimumInputLength: 1,
    }).on("change",function(e){
        var value = $(this).val();
        var text = $(this).text();

        var value_found = false;

        $(this).parent().parent().find(".tags").find("input").each(function(e){
            if($(this).val() == value){
                value_found = true;
                return;
            }
        });

        if(!value_found) {
            var tag_element = "<a href=\"javascript:void(0);\">"+ text +"<span class=\"cross tag_close tag_study_partners\"><input type=\"hidden\" class=\"message_receivers\" name=\""+ $(this).prop("name")+"-list[]\" value=\""+ value +"\"></span></a>";

            $(this).parent().parent().find(".tags").append(tag_element);

            $("#id_lesson_payer").append("<option value=\""+ value +"\">"+ text +"</option>");

            $(this).val("");
            $(this).text("");

        }



    });

    $("#id_add_people_conversation_select").select2({
      height: 90,
      width: 300,
      placeholder: "Type here to add person",
      ajax: {
        url: "/ajax/tutor-list/",
        dataType: 'json',
        delay: 250,
        data: function (params) {
          return {
            q: params.term, // search term
            utype: "student"
          };
        },
        processResults: function (data, page) {
          return {
                results: $.map(data.items, function(obj) {
                    return { id: obj.id, text: obj.name };
                })
            };
        },
        cache: true
      },
      escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
      minimumInputLength: 1,
    }).on("change",function(e){
        var value = $(this).val();
        var text = $(this).text();

        var value_found = false;

        $(this).parent().parent().find(".tags").find("input").each(function(e){
            if($(this).val() == value){
                value_found = true;
                return;
            }
        });

        if(!value_found) {
            var tag_element = "<a href=\"javascript:void(0);\">"+ text +"<span class=\"cross tag_close tag_study_partners\"><input type=\"hidden\" class=\"message_receivers\" name=\""+ $(this).prop("name")+"-list[]\" value=\""+ value +"\"></span></a>";

            $(this).parent().parent().find(".tags").append(tag_element);

            $("#id_lesson_payer").append("<option value=\""+ value +"\">"+ text +"</option>");

            $(this).val("");
            $(this).text("");

        }



    });

    $(".snd_msg").css("width","800px");
    $(".snd_msg_bx").css("width","100%");
    $(".snd_msg_bx").css("height","auto");

    $(document).on("click",".add_batch_people_to_conversation_cancel", function(e) {
        e.preventDefault();
        $("#black_overlay").hide();
        $("#add_buddy_to_conversation").hide();
    });

    $(document).on("click",".add_batch_people_to_conversation", function(e) {
//        e.preventDefault();
//        $("#add_buddy_to_conversation_form").prop("action",$("#add_buddy_to_conversation_form").data("action"));
//        $("#add_buddy_to_conversation_form").submit();
    });

    function send_batch_messages() {
        var fd = new FormData($("form[name=msg_form]")[0]);
        var receivers = "";
        $(".message_receivers").each(function(i) {
            receivers += $(this).val() +",";
        });
        fd.append('receivers', receivers);
        fd.append('csrfmiddlewaretoken',$("form[name=msg_form]").find('input[name=csrfmiddlewaretoken]').val());
        fd.append('message', $("textarea[name=msg_text]").val());
        fd.append('file', $('input[name=attached_file]')[0].files[0]);
        fd.append('chat_type', 0); //p2p chat.
        fd.append('chat_id', -1);
        $(".send_batch_message_submit").prop('disabled', true);
        $(".send_batch_message_cancel").prop('disabled', true);

        //console.log(fd);
        $ajax_request = $.ajax({
            type: "POST",
            url: "/ajax/send-batch-message/",
            data: fd,
            contentType: false,
            processData: false,
            xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                if(myXhr.upload){
                    //myXhr.upload.addEventListener('progress',progress, false);
                }
                return myXhr;
            },
            cache:false,
            success: function(data)
            {
                var data = jQuery.parseJSON(data);
                if(data.status == "SUCCESS"){
                    location.reload();
                }
                else{
                    $(".send_batch_message_submit").prop('disabled', false);
                    $(".send_batch_message_cancel").prop('disabled', false);
                    BootstrapDialog.show({
                        title: "Operation failed",
                        type: BootstrapDialog.TYPE_DEFAULT,
                        message: function(dialog) {
                            return "Operation could not be performed. Something wrong happened in the server."
                        },
                        buttons: [{
                            label: 'Close',
                            action: function(dialogItself){
                                dialogItself.close();
                            }
                        }]
                    });
                }
            },
            error: function(jqxhr, status, error)
            {
                $(".send_batch_message_submit").prop('disabled', false);
                $(".send_batch_message_cancel").prop('disabled', false);
                BootstrapDialog.show({
                    title: "Operation failed",
                    type: BootstrapDialog.TYPE_DEFAULT,
                    message: function(dialog) {
                        return "Operation could not be performed. Something wrong happened in the server."
                    },
                    buttons: [{
                        label: 'Close',
                        action: function(dialogItself){
                            dialogItself.close();
                        }
                    }]
                });
            }
        })
        .done(function( msg ) {
            $(".send_batch_message_submit").prop('disabled', false);
            $(".send_batch_message_cancel").prop('disabled', false);
        });
    };

    $(document).on("click",".send_batch_message_submit", function(e) {
        e.preventDefault();
        send_batch_messages();
    });

    $(".select2-selection").css("height","42px");
    $(".select2-dropdown").css("margin-top", "-20px");
    $(".select2-selection").css("border","1px solid #CFCFCF");
    var black_overlay = $('#black_overlay');
    var cross = $('.cross');
    var snd_msg = $('#snd_msg');

    black_overlay.click(function(){
    black_overlay.hide(); snd_msg.hide();$("#add_buddy_to_conversation").hide();});

    $(document).on("click","input[name=cancel]", function(e){
        black_overlay.hide(); snd_msg.hide();
    });

    $( document ).on( 'keydown', function ( e ) {
        if ( e.keyCode === 27 ) { // ESC
            $( black_overlay ).hide();
            $( snd_msg ).hide();
        }
    });

    //window.onload = function(){marscrl.stop()};



});

function showmsg()
    {document.getElementById('black_overlay').style.display="block";
        document.getElementById('snd_msg').style.display="block";}

function toggle_visibility(id) {
        var e = document.getElementById(id);
        if(e.style.display == 'block')
            e.style.display = 'none';
        else
            e.style.display = 'block';
    }