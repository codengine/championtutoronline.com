/**
 * Created with PyCharm.
 * User: codengine
 * Date: 8/21/15
 * Time: 8:11 PM
 * To change this template use File | Settings | File Templates.
 */

token_request_count = 0;
var connection_ids = {};
var stream_ids = {};
var stream_published = false;
var streams = {};
var sharing_mode = "audio"; //video

var request_ajax = function(url,method,data,completeback,callback,errback) //uids are comma separated. like 1,2,3
{
    $.ajax({
        type: method,
        url: url, //"/ajax/initsession",
        data: data,
        success: function(data)
        {
            //console.log(data);
            data = jQuery.parseJSON(data)
            if(callback != undefined && typeof callback == "function")
            {
                callback(data);
            }
        },
        error: function(jqxhr,status,errorthrown)
        {
            if(errback != undefined && typeof errback == "function")
            {
                errback(jqxhr,status,errorthrown);
            }
        }
    })
    .done(function(msg )
    {
        if(completeback != undefined && typeof completeback == "function")
        {
            completeback(msg);
        }
    });
};

var publishStream = function(audio_mode_only) {
    if(ot_session != undefined)
    {
        //console.log("Audio mode set: "+audio_mode_only);

        $("#id_video_section").html("");

        var pubOptions =
        {
//            width:$("#id_video_section").width(),
//            height:$("#id_video_section").height(),
//            buttonDisplayMode:"off"
              width: 150,
              height: 150
        };

        $("#my_video").show();
        $("#my_video_control").show();

        var my_video_id = "id_video_section";
        $("#id_video_section").show();
        $("#my_video_control").hide();
        if(window.live_lesson && window.live_lesson > 0) {
            my_video_id = "my_video";
            //$("#id_video_section").hide();
            $("#my_video_control").show();
        }

        publisher = OT.initPublisher(my_video_id,pubOptions,function(error)
        {
            //console.log("OT.initPublisher error! :(");
        });

        if(audio_mode_only != undefined && audio_mode_only == true){
            //console.log("Voice only session...");
            publisher.publishAudio(true);
            publisher.publishVideo(false);
        }

        var publisher_options = {
            accessAllowed: function (event) {
                // The user has granted access to the camera and mic.
                //console.log("Camera permission granted.");
            },
            accessDenied: function (event) {
                // The user has denied access to the camera and mic.
                //console.log("Camera permission denied.");
            },
            accessDialogOpened: function (event) {
                // The Allow/Deny dialog box is opened.
                //console.log("Access dialog opened.");
            },
            accessDialogClosed: function (event) {
                // The Allow/Deny dialog box is closed.
                //console.log("Access dialog closed.");
            },
            streamCreated:  function (event) {
                //console.log('The publisher started streaming.');
            },
            streamDestroyed : function (event) {
                //console.log("The publisher stopped streaming. Reason: "
                //    + event.reason);
            }
        };

        //console.log(publisher);
        publisher.on(publisher_options);

        ot_session.publish(publisher);
    }
}

var initOTSession = function(OT_api_key,OT_session,OT_token,audio_only_mode)
{
    if(OT_api_key != null && OT_session != null && ot_session == null)
    {
        ot_session = OT.initSession(OT_api_key, OT_session);

        ot_session.on("sessionConnected", function(event) {
            console.log("Session Initialized...");
            //console.log("Now publishing stream to the session...");
            //console.log("Length: "+event.streams.length);
            //Now publish the stream.

            var sharing_passed = false;
            if(sharing_mode == "audio" && audio_only_mode == true) {
                sharing_passed = true;
            }
            else if(sharing_mode == "video" && !audio_only_mode) {
                sharing_passed = false;
            }

            if(!stream_published && sharing_passed)
            {
                publishStream(audio_only_mode);
                stream_published = true;
            }
            var streams = {};
            //Now subscribe to the stream that already in the session and i have just joined the session.
            for (var i = 0; i < event.streams.length; i++) {
                if(!streams.hasOwnProperty(event.streams[i].connection.connectionId)) {
                    streams[event.streams[i].connection.connectionId] = true;
                    var subscriber = ot_session.subscribe(event.streams[i], "id_video_section", {
                        insertMode: "append"
                    });

                    subscriber.on({
                        videoDisabled: function(event)
                        {
                            //console.log("Video disabled.");
                            //console.log(event.reason);
                            subscriber.setStyle('backgroundImageURI',
                                //'http://tokbox.com/img/styleguide/tb-colors-cream.png'
                                'http://'+ window.app_server +'static/images/profile_img.png'
                            );
                        },
                        videoEnabled: function(event)
                        {
                            //console.log("Video enabled.");
                            //console.log(event.reason);
                            var imgData = subscriber.getImgData();
                            subscriber.setStyle('backgroundImageURI', imgData);
                        }
                    });
                    layout();
                }
            }

        });

        ot_session.addEventListener('sessionDisconnected', function(event) {
            console.log("Session Disconnected...");
            if(event.streams == undefined) {
                return;
            }
            connection_count = 0;
            for (var i = 0; i < event.streams.length; i++) {
                if (event.streams[i].connection.connectionId == ot_session.connection.connectionId) {
                    //console.log("Skipping my own session.");
                    return;
                }
                var subscriber = ot_session.subscribe(event.streams[i], "id_video_section", {
                    insertMode: "append"
                });

                subscriber.on({
                    videoDisabled: function(event)
                    {
                        //console.log("Video disabled.");
                        //console.log(event.reason);
                        subscriber.setStyle('backgroundImageURI',
                            //'http://tokbox.com/img/styleguide/tb-colors-cream.png'
                            'http://'+ window.app_server +'static/images/profile_img.png'
                        );
                    },
                    videoEnabled: function(event)
                    {
                        //console.log("Video enabled.");
                        //console.log(event.reason);
                        var imgData = subscriber.getImgData();
                        subscriber.setStyle('backgroundImageURI', imgData);
                    }
                });
                layout();
            }
        });

        ot_session.addEventListener('streamCreated', function(event) {
            //This callback is fired when i am already in the session and new user has just joined the session.
            console.log("Stream created. Stream ID: "+event.stream.streamId);
            $("#id_end_call").prop("disabled",false);

            for (var i = 0; i < event.streams.length; i++) {
                if (event.streams[i].connection.connectionId == ot_session.connection.connectionId) {
                    console.log("Skipping my own session.");
                    return;
                }

                if(!streams.hasOwnProperty(event.streams[i].connection.connectionId)) {
                    streams[event.streams[i].connection.connectionId] = true;
                    var subscriber = ot_session.subscribe(event.streams[i], "id_video_section", {
                        insertMode: "append"
                    });

                    subscriber.on({
                        videoDisabled: function(event)
                        {
                            console.log("Video disabled.");
                            console.log(event.reason);
                            subscriber.setStyle('backgroundImageURI',
                                //'http://tokbox.com/img/styleguide/tb-colors-cream.png'
                                'http://'+ window.app_server +'static/images/profile_img.png'
                            );
                        },
                        videoEnabled: function(event)
                        {
                            console.log("Video enabled.");
                            console.log(event.reason);
                            var imgData = subscriber.getImgData();
                            subscriber.setStyle('backgroundImageURI', imgData);
                        }
                    });
                    layout();
                }
            }
        });

        ot_session.addEventListener('connectionCreated', function(event) {
            var connection_id = event.connection.connectionId;
            if(!connection_ids.hasOwnProperty(connection_id)) {
                connection_count++;
                connection_ids[connection_id] = true;
            }
            console.log("Connection Created...");
            console.log("Count: "+connection_count);
            var container_width_redefined = 300 ;// + (connection_count - 2) * 100;
            var container_height_redefined = 280 ;// + (connection_count - 2) * 100;

            if(connection_count >= 2){
                container_width_redefined = 300 + (connection_count - 2) * 100;
                container_height_redefined = 280 + (connection_count - 2) * 100;
            }

            video_sharing_container.css("width",container_width_redefined+"px");
            video_sharing_container.css("height",container_height_redefined+"px");

            video_section.css("width",container_width_redefined+"px");
            video_section.css("height",container_height_redefined+"px");

            video_section.find(".video-box").css("width",video_box_width+"px");
            video_section.find(".video-box").css("height",video_box_height+"px");

            video_section.find( "[id^='id_video_pane']").css("width",video_box_width+"px");
            video_section.find( "[id^='id_video_pane']").css("height",video_box_height+"px");

            layout();
        });

        ot_session.addEventListener('connectionDestroyed', function(event) {
            connection_count--;
            console.log("Connection destroyed...");
            console.log("Connection Count: "+connection_count);
            if(connection_count == 1){
                disconnectSession();
                calls = {};
                ot_session = null;
                call_active = false;
                ot_session_id = null;
                publisher = null;
                connection_count = 0;
                $(".sharing_video").css("display","none");
            }

            var container_width_redefined = 300 ;// + (connection_count - 2) * 100;
            var container_height_redefined = 280 ;// + (connection_count - 2) * 100;

            if(connection_count >= 2){
                container_width_redefined = 300 + (connection_count - 2) * 100;
                container_height_redefined = 280 + (connection_count - 2) * 100;
            }

            video_sharing_container.css("width",container_width_redefined+"px");
            video_sharing_container.css("height",container_height_redefined+"px");

            video_section.css("width",container_width_redefined+"px");
            video_section.css("height",container_height_redefined+"px");

            video_section.find(".video-box").css("width",video_box_width+"px");
            video_section.find(".video-box").css("height",video_box_height+"px");

            video_section.find( "[id^='id_video_pane']").css("width",video_box_width+"px");
            video_section.find( "[id^='id_video_pane']").css("height",video_box_height+"px");

            layout();

        });

        ot_session.addEventListener('streamDestroyed', function(event) {
            console.log("Stream destroyed...Stream ID: "+event.stream.streamId);
            for (var i = 0; i < event.streams.length; i++) {
                if (event.streams[i].connection.connectionId == ot_session.connection.connectionId) {
                    //console.log("Skipping my own session.");
                    return;
                }
                var subscriber = ot_session.subscribe(event.streams[i], "id_video_section", {
                    insertMode: "append"
                });

                subscriber.on({
                    videoDisabled: function(event)
                    {
                        //console.log("Video disabled.");
                        //console.log(event.reason);
                        subscriber.setStyle('backgroundImageURI',
                            //'http://tokbox.com/img/styleguide/tb-colors-cream.png'
                            'http://'+ window.app_server +'static/images/profile_img.png'
                        );
                    },
                    videoEnabled: function(event)
                    {
                        //console.log("Video enabled.");
                        //console.log(event.reason);
                        var imgData = subscriber.getImgData();
                        subscriber.setStyle('backgroundImageURI', imgData);
                    }
                });
                layout();
            }
        });

        ot_session.connect(OT_token, function(error) {
            console.log("Session Connection Error!");
            console.log(error);
            if(error != null && error.hasOwnProperty("code") && typeof error.code != "undefined" && error.code == 1004) {
                if(token_request_count < 5) {
                    window.start_opentok_session("",audio_only_mode,true);
                }
            }
        });

    }
};


$(document).ready(function(){

    var champ_user_id = window.champ_user_id;

    video_sharing_container = $("#id_video_sharing_container");
    layoutContainer = document.getElementById("id_video_section");
    layout = TB.initLayoutContainer(layoutContainer).layout;
    fresh_layout = layout
    ot_session = null;
    publisher = null;
    video_sharing_container = $("#id_video_sharing_container");
    video_section = $("#id_video_section");
    video_container_max_width = 300;
    video_conatiner_max_height = 280;
    video_box_width = 300;
    video_box_height = 280;
    connection_count = 0;


    window.start_opentok_session = function (ot_session_key,audio_mode,request_new_token){
        token_request_count += 1;
        var lesson_id = $("#id_lesson_id_sharing").val();
        var whiteboard_id = $("#id_wb_id_sharing").val();
        var new_token = 0;
        if(request_new_token != null && request_new_token != "" && request_new_token) {
            new_token = 1;
        }
        var data = { "lesson_id": lesson_id, "whiteboard_id": whiteboard_id, "uid": champ_user_id, "ot_session": ot_session_key, request_new_token: new_token }
        request_ajax("/ajax/initsession","POST",data,function(msg) //Complete Callback.
            {

            },
            function(data) //Success Callback.
            {
                if(data.status == "SUCCESS") {
                    //$("#id_video_loading").hide();
                    initOTSession(data.data.ot_api_key,data.data.otsession,data.data.ot_token,audio_mode);
                }
                else {
                    $("#id_video_loading").hide();
                    $("#id_video_section").show();
                }

            },function(jqxhr,status,errorthrown) //Error Callback.
            {
                $("#id_video_loading").hide();
                $("#id_video_section").show();
            });
    };

    $(document).on("click", "#id_use_audio_button", function(e) {
        e.preventDefault();
        stream_published = false;
        connection_count = 0;
        sharing_mode = "audio";
        if($("#my_video").length) {

        }
        else {
            var html = "<div id=\"my_video\" style=\"z-index: 100012; position: absolute; bottom: 0px; right: 0px; width: 100px; height: 100px; background: black; display: none;\"></div>";
            $("#id_video_sharing_container").append(html);
        }
        enable_audio();
        enable_video();
        $("#my_video_control").show();
        start_opentok_session("",true);

        var data = {
            "user_id": window.champ_user_id,
            "wb_id": $("#id_wb_id_sharing").val(),
            "name": "AUDIO",
            "settings_value": 1
        }

        request_ajax("/ajax/whiteboard_video_settings","POST",data,function(msg) //Complete Callback.
        {

        },
        function(data) //Success Callback.
        {

        },function(jqxhr,status,errorthrown) //Error Callback.
        {

        });
    });

    $(document).on("click", "#id_use_video_button", function(e) {
        e.preventDefault();
        stream_published = false;
        connection_count = 0;
        sharing_mode = "video";
        if($("#my_video").length) {

        }
        else {
            var html = "<div id=\"my_video\" style=\"z-index: 100012; position: absolute; bottom: 0px; right: 0px; width: 100px; height: 100px; background: black; display: none;\"></div>";
            $("#id_video_sharing_container").append(html);
        }
        $("#id_video_loading").show();
        $("#id_video_section").hide();
        $("#my_video_control").show();
        start_opentok_session("",false);

        var data = {
            "user_id": window.champ_user_id,
            "wb_id": $("#id_wb_id_sharing").val(),
            "name": "VIDEO",
            "settings_value": 1
        }

        request_ajax("/ajax/whiteboard_video_settings","POST",data,function(msg) //Complete Callback.
        {

        },
        function(data) //Success Callback.
        {

        },function(jqxhr,status,errorthrown) //Error Callback.
        {

        });

        $("#myonoffswitch3").prop("checked",false); //Video on
        $("#myonoffswitch4").prop("checked",true);  //Audio on
        disable_audio();
        enable_video();

    });

    $("#id_video_sharing_container").draggable().resizable({
        start: function( event, ui ){

        },
        stop: function( event, ui ){

            container_width_redefined = ui.size.width;
            container_height_redefined = ui.size.height;

            video_section.css("width",container_width_redefined+"px");
            video_section.css("height",container_height_redefined+"px");

            video_section.find(".OT_widget-container").css("width",container_width_redefined+"px !important");
            video_section.find(".OT_widget-container").css("height",container_height_redefined+"px !important");

            layout();
        },
        resize: function( event, ui ) {

        }
    });


    $("#myonoffswitch4").change(function (e) {
        var toggle_value = $(this).is(":checked");
        console.log(toggle_value);
        //console.log(publisher);

        var settings_value = 0;

        if(toggle_value == true){
            //Audio on
            if(ot_session == null) {
                start_opentok_session("",true);
            }
            if(publisher != null)
            {
                publisher.publishAudio(true);
                settings_value = 1;
            }
        }
        else{
            //Audio off
            if(publisher != null)
            {
                //publisher.publishAudio(false);
                settings_value = 0;
                if(ot_session != null) {

                    ot_session.disconnect();
                    streams = {};
                    ot_session = null;
                    //$("#id_video_section").html("");
                    var lesson_id = $("#id_lesson_id_sharing").val();
                    var data = {
                        "lesson_id": lesson_id
                    }
                    request_ajax("/ajax/disable_wb_video_sharing/","POST",data,function(msg) //Complete Callback.
                    {

                    },
                    function(data) //Success Callback.
                    {

                    },function(jqxhr,status,errorthrown) //Error Callback.
                    {

                    });


                    var html = "<div style=\"display: table-cell;height: 100% !important;text-align: center;vertical-align: center; background: white; padding: 7px;\">";
                        html +="<p style=\"padding: 13px;\">This tool allows you to share your audio/video to other party of this lesson.</p>";
                        html += "<button id=\"id_use_audio_button\" class=\"btn btn-default btn-sm\"><span class=\"glyphicon glyphicon-volume-up\"></span> Audio</button>";
                        html += "<button id=\"id_use_video_button\" class=\"btn btn-default btn-sm\"><span class=\"glyphicon glyphicon-camera\"></span> Video</button></div>";
                    $("#id_video_section").html(html);


                    connection_count = 0;
                    var container_width_redefined = 300 ;// + (connection_count - 2) * 100;
                    var container_height_redefined = 280 ;// + (connection_count - 2) * 100;

                    video_sharing_container.css("width",container_width_redefined+"px");
                    video_sharing_container.css("height",container_height_redefined+"px");

                    video_section.css("width",container_width_redefined+"px");
                    video_section.css("height",container_height_redefined+"px");

                    video_section.find(".video-box").css("width",video_box_width+"px");
                    video_section.find(".video-box").css("height",video_box_height+"px");

                    video_section.find( "[id^='id_video_pane']").css("width",video_box_width+"px");
                    video_section.find( "[id^='id_video_pane']").css("height",video_box_height+"px");

                    layout();

                    disable_audio();
                    disable_video();

                    $("#my_video_control").hide();

                }
            }
        }

        var data = {
            "user_id": window.champ_user_id,
            "wb_id": $("#id_wb_id_sharing").val(),
            "name": "AUDIO",
            "settings_value": settings_value
        }

        request_ajax("/ajax/whiteboard_video_settings","POST",data,function(msg) //Complete Callback.
        {

        },
        function(data) //Success Callback.
        {

        },function(jqxhr,status,errorthrown) //Error Callback.
        {

        });
    });

    $(document).on("click", ".wb_video_chat_header", function(e) {
         e.preventDefault();
//            var lesson_id = $("#id_lesson_id_sharing").val();
//            var data = {
//                "lesson_id": lesson_id
//            }
//            request_ajax("/ajax/disable_wb_video_sharing/","POST",data,function(msg) //Complete Callback.
//            {
//
//            },
//            function(data) //Success Callback.
//            {
//
//            },function(jqxhr,status,errorthrown) //Error Callback.
//            {
//
//            });
    });

    $("#myonoffswitch3").change(function(e)
    {
        var toggle_value = $(this).is(":checked");
        //console.log(toggle_value);

        var settings_value = 0;

        if(toggle_value == false){
            //video on
            if(ot_session == null) {
                start_opentok_session("",false);
            }
            if(publisher != null)
            {
                publisher.publishVideo(true);
                publisher.publishAudio(true);
                $("#myonoffswitch4").prop("checked",true);
                $("#myonoffswitch4").prop("disabled",true);
                settings_value = 1;
                disable_audio();
            }
        }
        else{
            //video off
            if(publisher != null){
                publisher.publishVideo(false);
                $("#myonoffswitch4").prop("disabled",false);
                settings_value = 0;
                enable_audio();
            }
        }

        var data = {
            "user_id": window.champ_user_id,
            "wb_id": $("#id_wb_id_sharing").val(),
            "name": "VIDEO",
            "settings_value": settings_value
        }

        request_ajax("/ajax/whiteboard_video_settings","POST",data,function(msg) //Complete Callback.
        {

        },
        function(data) //Success Callback.
        {

        },function(jqxhr,status,errorthrown) //Error Callback.
        {

        });

    });

    $("#id_hide_my_video").click(function(e) {
        $("#my_video").hide();
        $(this).hide();
        $("#id_show_my_video").show();
    });

    $("#id_show_my_video").click(function(e) {
        $("#my_video").show();
        $(this).hide();
        $("#id_hide_my_video").show();
    });

    var session_started = $("#id_session_started").val();
    var video_enabled = $("#id_video_enabled").val();
    var audio_enabled = $("#id_audio_enabled").val();
    var sharing_enabled = $("#id_sharing_enabled").val();
//    var ot_session = $("#id_ot_session_key").val();
//    var ot_token = $("#id_ot_token").val();
//    var ot_api_key = $("#id_ot_api_key").val();

    //Auto initialize the audio/video

//    if(session_started == "true"){
//        if(video_enabled == "1"){
//            start_opentok_session("",false);
//            if(publisher != null && publisher != null) {
//                publisher.publishVideo(true);
//                publisher.publishAudio(true);
//            }
//            $("#myonoffswitch4").prop("checked",true);
//            $("#myonoffswitch4").prop("disabled",true);
//        }
//        else{
//            if(audio_enabled == "1"){
//                start_opentok_session("",true);
//                if(publisher != null && publisher != null){
//                    publisher.publishVideo(false);
//                    $("#myonoffswitch4").prop("disabled",false);
//                }
//            }
//            else{
//                start_opentok_session("",false);
//                $("#myonoffswitch4").prop("checked",true);
//                $("#myonoffswitch4").prop("disabled",true);
//            }
//        }
//        $("#id_video_section").show();
//        $(".sharing_video").show();
//    };

    if($("#id_my_video_enabled").length) {
        if($("#id_my_video_enabled").val() == "1") {
            if(video_enabled == "1" && audio_enabled == "1"){
                start_opentok_session("",false);
                if(publisher != null) {
                    publisher.publishVideo(true);
                    publisher.publishAudio(true);
                }
                $("#myonoffswitch3").prop("checked",false); //Video on
                $("#myonoffswitch4").prop("checked",true);  //Audio on
                //$("#myonoffswitch4").prop("disabled",true);
                $("#id_video_section").show();
                $(".sharing_video").show();
                disable_audio();
                enable_video();
            }
            else if(video_enabled == "0" && audio_enabled == "1") {
                start_opentok_session("",true);
                if(publisher != null){
                    publisher.publishVideo(false);
                    $("#myonoffswitch3").prop("checked",true); //Video off
                    $("#myonoffswitch4").prop("checked",true);  //Audio on
                }
                $("#id_video_section").show();
                $(".sharing_video").show();
                enable_audio();
                enable_video();
            }
            else if(video_enabled == "0" && audio_enabled == "0") {
                $("#id_video_section").hide();
                $(".sharing_video").hide();
                disable_audio();
                disable_video();
            }
        }
        else {
            disable_audio();
            disable_video();
        }
    }
    else {
        disable_audio();
        disable_video();
    }

    //console.log("Toggle value here: ");
    //console.log($("#myonoffswitch4").is(":checked"));
    //console.log($("#myonoffswitch3").is(":checked"));
    //console.log("Toggle value here ends");


});
