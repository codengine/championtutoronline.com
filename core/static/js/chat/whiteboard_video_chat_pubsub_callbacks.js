/**
 * Created with PyCharm.
 * User: codengine
 * Date: 8/21/15
 * Time: 10:51 PM
 * To change this template use File | Settings | File Templates.
 */

var publisher_options = {
    accessAllowed: function (event) {
        // The user has granted access to the camera and mic.
        console.log("Camera permission granted.");
    },
    accessDenied: function (event) {
        // The user has denied access to the camera and mic.
        console.log("Camera permission denied.");
    },
    accessDialogOpened: function (event) {
        // The Allow/Deny dialog box is opened.
        console.log("Access dialog opened.");
    },
    accessDialogClosed: function (event) {
        // The Allow/Deny dialog box is closed.
        console.log("Access dialog closed.");
    },
    streamCreated:  function (event) {
        console.log('The publisher started streaming.');
    },
    streamDestroyed : function (event) {
        console.log("The publisher stopped streaming. Reason: "
            + event.reason);
    }
};
