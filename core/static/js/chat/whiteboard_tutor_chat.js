$(document).ready(function() {

    function load_whiteboard_messages(start) {

        var live_lesson = $("#id_wb_chat_content_container").data("live-lesson");
        var whiteboard_id = $("#id_wb_chat_content_container").data("whiteboard");

        $.ajax({
                type: "GET",
                url: "/ajax/fetch_wb_messages/"+whiteboard_id+"/",
                data: { live_lesson: live_lesson, start: start },
                success: function(data)
                {
                    var data = jQuery.parseJSON(data);
                    if(data.status == "SUCCESS"){
                        for(var i = 0 ; i < data.data.wb_messages.length; i++) {
                            $("#id_wb_chat_content_container").prepend(data.data.wb_messages[i]);
                        }
                    }
                    else{

                    }
                },
                error: function(jqxhr, status, error)
                {

                }
            })
            .done(function( msg ) {

            });
    }

    load_whiteboard_messages(-1);

    function wb_file_upload_progress(e){

        if(e.lengthComputable){
            //console.log();
            //console.log(e.loaded);
            //console.log(e.total);
            if($('input[name=attached_file]').val() != "") {
                var max = e.total;
                var current = e.loaded;

                var Percentage = parseInt((current * 100)/max);
                $("#id_file_upload_progress_container").show();
                $(".file_upload_progress > div").css("width",Percentage+"%");
                $(".file_upload_progress > div").text(Percentage+"%");
                $("#id_wb_chat_content_container").css("height","270px");


                if(Percentage >= 100)
                {
                   $('input[name=attached_file]').val("");
                   $("#id_file_upload_progress_container").hide();
                }
            }
        }
     }

     function send_message() {
            var fd = new FormData();
            var chat_text = $("#id_wb_chat_text_input").val();
            var file = $("#id_wb_chat_file")[0].files[0];
            var lesson_id = $("#id_wb_chat_text_input").data("lesson-id");
            var csrf_token = $("#id_tutor_chat_form").find("input[name=csrfmiddlewaretoken]").val();

            var whiteboard_id = $("#id_wb_chat_content_container").data("whiteboard");

            fd.append("whiteboard_id", whiteboard_id);
            fd.append("tutor_chat_text",chat_text);
            fd.append("upload_file",file);
            fd.append("csrfmiddlewaretoken",csrf_token);

            $ajax_request = $.ajax({
                type: "POST",
                url: "/ajax/tutor-chat-submit/",
                data: fd,
                contentType: false,
                processData: false,
                xhr: function() {
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){
                        myXhr.upload.addEventListener('progress',wb_file_upload_progress, false);
                    }
                    return myXhr;
                },
                cache:false,
                success: function(data)
                {
                    var data = jQuery.parseJSON(data);
                    if(data.status == "SUCCESS"){
                        $("#id_wb_chat_content_container").scrollTop($("#id_wb_chat_content_container")[0].scrollHeight);
                    }
                    else{

                    }
                },
                error: function(jqxhr, status, error)
                {

                }
            })
            .done(function( msg ) {
                $("#id_wb_chat_content_container").css("height","290px");
                $("#id_file_upload_progress_container").hide();
            });
     }
     var lastScrollTop = 0;
     $("#id_wb_chat_content_container").on('scroll', function(e) {
            st = $(this).scrollTop();

            if(st < lastScrollTop) {
                  if(st == 0) { //Reach's the top of the div.
                        if($(".tutor_chat_entry").length) {
                            var last_chat_entry = $(".tutor_chat_entry").first();
                            var pk_val = last_chat_entry.data("tc-id");
                            load_whiteboard_messages(pk_val);
                            lastScrollTop = last_chat_entry.scrollTop();
                        }
                  }
              }
          });

    $("#id_wb_chat_text_input").keypress(function(event){
        if(event.keyCode == 13){
            send_message();
            $("#id_wb_chat_text_input").val("");
            $("#id_wb_chat_text_input").focus();
        }
    });

    $("input[name=upload_file]").change(function(e) {
        e.preventDefault();
        if($(this).val() != "") {
            send_message();
        }
    });

    $("#id_upload_file_btn").click(function(e){
        e.preventDefault();
        $("input[name=upload_file]").click();
    });

    $("#id_wb_chat_extend").click(function(e) {
        e.preventDefault();
        //$(".wb-chat").css("height","450px");
        //$("#id_wb_chat_content_container").css("height","390px");
    });

    var is_visible = parseInt($(".wb-chat").data("is-visible"));

    $(".wb_tutor").click(function(e) {
        e.preventDefault();
        initial_call = false;
        toggle_wb_chatbox_status();
        $(".wb_tutor").removeClass("active-head");
        if(chat_active_interval) {
            clearInterval(chat_active_interval);
        }
    });

    var initial_call = true;

    function toggle_wb_chatbox_status() {
        //alert(is_visible);
       if(is_visible == 0) {
            $(".wb-chat").show();
            is_visible = 1;
            if(!initial_call) {
                update_wb_chatbox_status(1);
            }
            initial_call = false;
       }
       else {
            $(".wb-chat").hide();
            is_visible = 0;
            if(!initial_call) {
                update_wb_chatbox_status(0);
            }
            initial_call = false;
       }
    }

    if(is_visible == 1) {
        $(".wb-chat").show();
   }
   else {
        $(".wb-chat").hide();
   }

    $("#hide").click(function(e) {
        e.preventDefault();
        initial_call = false;
        toggle_wb_chatbox_status();
    });

    function update_wb_chatbox_status(status) {
        var whiteboard_id = $("#id_wb_chat_content_container").data("whiteboard");
        $.ajax({
            type: "POST",
            url: "/ajax/update_wb_chatbox_status/",
            data: { whiteboard_id: whiteboard_id, status: status },
            success: function(data)
            {
                var data = jQuery.parseJSON(data);
                if(data.status == "SUCCESS"){

                }
                else{

                }
            },
            error: function(jqxhr, status, error)
            {

            }
        })
        .done(function( msg ) {

        });
    }

});

window.chat_active_interval = false;

var wb_chat_scroll_to_bottom = function() {
    $('#id_wb_chat_content_container').scrollTop($('#id_wb_chat_content_container').scrollHeight);
}

if(window.socket != undefined){
    window.socket.on("WHITEBOARD_MESSAGE", function(data)
    {
        var data = jQuery.parseJSON(data);
        //console.log(data);
        //var dta = render_tutor_chat_row(data.id,data.url,data.name,data.img,data.msg,data.msg_date,data.attachment,data.attachment_name,false);

        $("#id_wb_chat_content_container").append(data);

        $("#id_wb_chat_content_container").scrollTop($("#id_wb_chat_content_container")[0].scrollHeight);


        var is_visible = $(".wb-chat").is(":visible");
        if(!is_visible) {
            chat_active_interval = setInterval(function(){
                $(".wb_tutor").toggleClass("active-head");
             },800);
        }
        else {
            $(".wb_tutor").removeClass("active-head");
        }


    });

    //setTimeout(wb_chat_scroll_to_bottom, 5000);

};


//function render_tutor_chat_row(user_id,url,user_name,user_img,msg,msg_date,attachment,attachment_original_name,sender){
//
//    var chat_row = $("#id_hidden_wb_chat_row").html();
//    chat_row = chat_row.replace("USER_ID",user_id);
//    chat_row = chat_row.replace("USER_PROFILE_URL",url);
//    chat_row = chat_row.replace("USER_FULL_NAME",user_name);
//    chat_row = chat_row.replace("USER_PROFILE_PICTURE",user_img);
//    //chat_row = chat_row.replace("USER_MESSAGE","<p class=\"msg\" style=\"color: black;\">"+ msg +"</p>");
//    if(msg != undefined && msg != ""){
//        chat_row = chat_row.replace("USER_MESSAGE","<p class=\"msg\" style=\"color: black;\">"+ msg +"</p>");
//    }
//    chat_row = chat_row.replace("MSG_DATE_TITLE",msg_date);
//
//    //chat_row = chat_row.replace("MESSAGE_ATTACHMENT",);
//    if(attachment != undefined && attachment != ""){
//        chat_row = chat_row.replace("MESSAGE_ATTACHMENT","<p class=\"attachment\"><a href='/media/"+ attachment +"' style='color: black;'><img src=\"/static/images/attachment.png\" width=20 height=20/>"+attachment_original_name+"</a></p>");
//        //chat_row.find(".attachment").css("display","none");
//    }
//    chat_row = chat_row.replace("USER_MESSAGE","");
//    chat_row = chat_row.replace("MESSAGE_ATTACHMENT","");
//    if(sender == true){
//
//    }
//    return chat_row;
//}
//
//$('#id_wb_chat_content_container').animate({scrollTop: 1000000}, 0);
//
//$(document).ready(function(){
//
//    console.log($('#id_wb_chat_content_container')[0].scrollHeight);
//
//    $('#id_wb_chat_content_container').scroll(function() {
//        if($('#id_wb_chat_content_container').scrollTop() == $('#id_wb_chat_content_container').height()) {
//               // ajax call get data from server and append to the div
//               alert("");
//        }
//    });
//
//    $('#id_wb_chat_content_container').animate({scrollTop: $('#id_wb_chat_content_container').height()}, 0);
//
//    function clear_tutor_message_form(){
//        $("input[name=tutor_chat_text]").val("");
//        $("input[name=upload_file]").val("");
//        $("#id_wb_chat_text_input").val("");
//    }
//
//    var options = {
//        beforeSend: function()
//        {
//            if($("input[name=upload_file]").val() != ""){
//                $("#progress").show();
//                //clear everything
//                $("#bar").width('0%');
//                $("#percent").html("0%");
//            }
//        },
//        uploadProgress: function(event, position, total, percentComplete)
//        {
//            $("#bar").width(percentComplete+'%');
//            $("#percent").html(percentComplete+'%');
//
//        },
//        success: function(data)
//        {
//            $("#bar").width('100%');
//            $("#percent").html('100%');
//
//            var data = jQuery.parseJSON(data);
//            console.log(data);
//            var sender = data.data.sender;
//            var dta = render_tutor_chat_row(sender.id,sender.url,sender.name,sender.img,data.data.msg,sender.msg_date,data.data.attachment,data.data.attachment_name,true);
//
//            $("#id_wb_chat_content_container").append(dta);
//
//            $('#id_wb_chat_content_container').scrollTop($('#id_wb_chat_content_container').scrollHeight);
//
//        },
//        complete: function(response)
//        {
//            $("#progress").hide();
//            clear_tutor_message_form();
//        },
//        error: function()
//        {
//
//        }
//
//    };
//
//    $("#id_tutor_chat_form").ajaxForm(options);
//
//    $("#id_upload_file_btn").click(function(e){
//        e.preventDefault();
//        $("input[name=upload_file]").click();
//    });
//
//
//    $("#id_wb_chat_text_input").keypress(function(event){
//        if(event.keyCode == 13){
//            $("input[name=tutor_chat_submit]").click();
//        }
//    });
//
//    $("#id_wb_chat_text_input").keyup(function(event){
//        $("input[name=tutor_chat_text]").val($("#id_wb_chat_text_input").val());
//    });
//
//    $("input[name=upload_file]").change(function(e){
//        $("input[name=tutor_chat_submit]").click();
//    });
//});
//
//
//if(window.socket != undefined){
//    window.socket.on("wboard_tchat_rcvd", function(data)
//    {
//        var data = jQuery.parseJSON(data);
//        var dta = render_tutor_chat_row(data.id,data.url,data.name,data.img,data.msg,data.msg_date,data.attachment,data.attachment_name,false);
//
//        $("#id_wb_chat_content_container").append(dta);
//
//        $('#id_wb_chat_content_container').scrollTop($('#id_wb_chat_content_container').scrollHeight);
//
//    });
//};

//$('#id_wb_chat_content_container').scrollTop($('#id_wb_chat_content_container')[0].scrollHeight);