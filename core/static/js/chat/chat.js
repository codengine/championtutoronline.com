/**
 * Created with PyCharm.
 * User: Sohel
 * Date: 9/28/14
 * Time: 5:45 PM
 * To change this template use File | Settings | File Templates.
 */

 //Declare global variables.
 //This will contain all chatbox created

jQuery(document).ready(function($){


function generateUUID(){
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
};

var UTCDate = function(){
    var now = new Date(); 
    var now_utc = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
    return now_utc;
};

var UTCDateMillis = function(){
    return UTCDate().getTime();
};

var generateChatUUID = function(){
    var uuid = generateUUID();
    var utcTimeMillis = UTCDateMillis();
    return uuid+utcTimeMillis;
};

//    $("#id_test_tagit").tagit();
    (function()
    {
        window.chat_boxes = {}; //{user_id: new Chat()}

        var position_right = 40;
        var max_visible_chatbox_count = 3;

        var redraw_chat_boxes = function()
        {
            var chat_objs_array = [];

            for( var key in window.chat_boxes ) {
                if(window.chat_boxes.hasOwnProperty(key))
                {
                    chat_objs_array.push(window.chat_boxes[key]);
                }
            }
            var position_right = 20;
            var i = 1;
            for(var indx = chat_objs_array.length - 1 ; indx >= 0 ; indx--)
            {
                var chat_box_wndw = chat_objs_array[indx];
                chat_box_wndw.margin_right = position_right;
                //Draw the chat box.
                if(i <= max_visible_chatbox_count)
                {
                    chat_box_wndw.showChatBox();
                }
                else
                {
                    chat_box_wndw.hideChatBox();
                }
                position_right += 245;
                i += 1;
            }
        }

        redraw_chat_boxes();

        var count_chat_boxes = function()
        {
            return Object.keys(window.chat_boxes).length;
        }

        var Chat = function()
        {
            this.id = generateChatUUID();
            this.chat_contnt = false;
            this.chat_cntnt_height = 100;
            this.window_minimized = false;
            this.remote_peers = [];
            this.chat_type = 0; //0 for p2p and 1 for group chat.
            this.active = true;
            this.order = 1;
            this.margin_right = 245;
            this.last_chat_entry = false;
            this.conversation_id = false;
            this.topic_class_name = false;
            this.topic_object_id = false;
            this.current_ajax_request = false;
            this.is_blocked = false;
        };

        window.Chat = Chat;


        Chat.prototype.prepare_message = function(remote_peers,msg,chat_time,tz,chat_type,conv_id){
            packet = {
                "publisher": {
                    "uid": window.champ_user_id
                },
                "subscribers": {
                    "uids": remote_peers
                },
                "message": msg,
                "chat_time": chat_time,
                "tz": tz,
                "chat_id": conv_id,
                "chat_type": chat_type
            };
            console.log(packet);
            return packet
        };

        Chat.prototype.send_message = function(remote_peers,msg,chat_type,conv_id)
        {

        };

        Chat.prototype.hideChatBox = function()
        {
            this.chat_contnt.parent().hide();
        };

        Chat.prototype.showChatBox = function()
        {
            this.chat_contnt.parent().css("right",this.margin_right);
            this.chat_contnt.parent().show();
        };

        Chat.prototype.hideAddBuddyButton = function() {
            this.chat_contnt.parent().find(".chat_add_buddy").hide();
        };

        Chat.prototype.showAddBuddyButton = function() {
            this.chat_contnt.parent().find(".chat_add_buddy").show();
        };

        Chat.prototype.addNewChat = function(user_obj,offline) //user_obj = {user_id:1233,name: "Sohel",img_url: ""}
        {
            var chat_html = "<div class=\"chat radius3_top\" style=\"margin-right:"+ this.margin_right +"px;display:none;\">";
            chat_html +=   "<div class=\"chat_header\">";
            chat_html +=   "<input type=\"hidden\" class=\"chat_widget_name\" value=\"\"/>"
            if(offline == undefined || offline == false)
            {
                chat_html +=       "<span class=\"online\"><img src=\"/static/images/online.png\" width=\"12\" height=\"12\" alt=\"img\"></span>";
            }
            else
            {
                chat_html +=       "<span class=\"offline hidden\"><img src=\"/static/images/chat_offline.png\" width=\"12\" height=\"12\" alt=\"img\"></span>";
            }
            chat_html +=       "<span class=\"chat_title padding-left-4\">" + user_obj.name + "</span>";
            chat_html +=       "<a href=\"#\" class=\"chat_close scot_tutor_close\" style=\"width: 15px; margin-right: 7px; margin-top: -2px;\"> <img src=\"/static/images/close.png\" width=\"10\" height=\"9\" alt=\"img\" style=\"width: 10px; height: 9px;\"> </a>";
            chat_html +=       "<a href=\"#\" class=\"chat_add_buddy\" style=\"width: 15px; margin-top: -2px;\"> <img src=\"/static/images/blue_pls.png\" width=\"9\" height=\"9\" alt=\"img\" style=\"width: 9px; height: 9px;\"> </a>";
            chat_html +=       "<a href=\"#\" class=\"chat_min_btn\" style=\"width: 15px; margin-top: -2px;\"> <img src=\"/static/images/min.png\" width=\"14\" height=\"8\" alt=\"img\" style=\"width: 14px; height: 8px;\"> </a>";
//            chat_html +=       "<a style=\"float: left;\" href=\"#\"><span class=\"notification-number-card\">3</span></a>";
//            chat_html += "<a href=\"#\"><div class=\"dropdown pull-right\"><span style=\"cursor: pointer;\" class=\"btn btn-default dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\"><span class=\"glyphicon glyphicon-cog\"></span></span>";
//            chat_html += "<ul class=\"dropdown-menu\"><li><a href=\"#\">Open in Chat</a></li></ul></div></a>";
            chat_html +=   "</div>";
            chat_html +=   "<div class='add_chat_buddy_area' style='display:none;background:#dedede;z-index:100;position:absolute;top:40px;left:0px;width:100%; padding: 3px;'><input type='hidden' class='hidden-selected-val'/><input type='text' placeholder='Type name here' class='add_grp_buddy_txt form-control' autocomplete='on' style='width:120px;float:left;'/><button style='float:right;margin_right:3px;margin-top:-10px; position: relative; top: -23px;' id='id_button_add_chat_buddy' class='btn btn-sm btn-primary add_grp_buddy_btn'>Add</button></div>";
            chat_html +=   "<div class=\"chat_contnt\">";
            chat_html +=   "</div>";
//            chat_html +=   "<div id=\"id_chat_file_progress_container\" class=\"chat_txt text-center\" style=\"position: absolute; right: 0px; bottom: 20px; height: 40px; background: white; width: 100%;\">";
//            chat_html +=   "<div style=\"width: 190px;\">";
//            chat_html +=   "<div class=\"progress file_upload_progress\">";
//            chat_html +=   "<div class=\"progress-bar progress-bar-striped active\" role=\"progressbar\"";
//            chat_html +=   "aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:0%\">0%</div></div>";
//            chat_html +=   "</div><div style=\"width: 50px; float: left;\">";
//            chat_html +=   "<span class=\"glyphicon glyphicon-remove cancel_file_upload_progress\" style=\"float: left; color: red; cursor: pointer;\"><img src=\"/static/images/cross.png\" width=\"20\" height=\"20\"/></span></div>";
//            chat_html +=   "</div>";

            chat_html +=   "<div class=\"progress_bar_container\" style=\"display: none; width: 100%; position: absolute; right: 0px; bottom: 20px; height: 40px;\">";
             chat_html +=   "<div style=\"width: 210px;\">";
            chat_html +=   "<div class=\"progress file_upload_progress\">";
            chat_html +=   "<div class=\"progress-bar progress-bar-striped active\" role=\"progressbar\"";
            chat_html +=   "aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:0%\">0%</div></div>";
            chat_html +=   "</div>";
            chat_html +=   "<span class=\"progress_cancel\" style=\"position: absolute; right: 17px; bottom: 0px; height: 40px; cursor: pointer;\"><img src=\"/static/images/cross.png\" width=\"10\" height=\"10\"/></span></div>";

            chat_html +=   "<div class=\"chat_txt\">";
            chat_html +=   "<input class=\"chat_input_textbox\" autofocus type=\"text\" placeholder=\"Enter a Message\">";
            chat_html +=   "<a class=\"click_chat_attachment\" href=\"#\"></a><input type=\"file\" name=\"chat_attached_file\" class=\"chat_attached_file\" style=\"display: none;\"/>";
            chat_html +=  "</div>";

            chat_html +=   "<div class=\"chat_bottom_status\" style=\"padding: 3px; display: none !important;\">";
            chat_html +=   "<span>You can't reply to this conversation</span>";
            chat_html +=   "</div>";

            chat_html +=  "</div>";

            //position_right += 310;

            $(chat_html).appendTo("#id_chat_window_container");
            var _this_obj = this;
            this.chat_contnt = $("#id_chat_window_container").find(".chat_contnt").last();
            this.chat_contnt.parent().find(".chat_input_textbox").focus();

            this.chat_contnt.parent().find(".click_chat_attachment").click(function(event) {
                event.preventDefault();
                $(this).parent().find(".chat_attached_file").click();
            });

            this.chat_contnt.parent().find(".click_chat_attachment").parent().find(".chat_attached_file").change(function(e) {
               var remote_peers = _this_obj.remote_peers;
                var msg = "";
                var chat_type = _this_obj.chat_type; //p2p chat.
                _this_obj.chat_contnt.parent().find("#id_chat_file_progress_container").hide();
                _this_obj.chat_contnt.parent().find(".progress-bar").css("width","0%");
                _this_obj.chat_contnt.parent().find(".progress-bar").text("0%");
                if(chat_type == 1) //group chat
                {
                    var conv_id = _this_obj.conversation_id;
                    var receivers = "";
                    for(var l = 0 ; l < remote_peers.length; l++) {
                        receivers += remote_peers[l]+",";
                    }
                    var fd = new FormData();
                    fd.append('publisher', window.champ_user_id);
                    fd.append('receivers', receivers);
                    fd.append('message', msg);
                    fd.append('chat_id', conv_id);
                    fd.append('chat_type', 1);
                    fd.append('file', _this_obj.chat_contnt.parent().find(".chat_attached_file")[0].files[0]);

                    _this_obj.post_message(fd,function(data) {
                    _this_obj.chat_contnt.append(data.data.chat_popup);
                    $(_this_obj.chat_contnt).scrollTop($(_this_obj.chat_contnt)[0].scrollHeight + 7);
                    },
                    function(jqxhr, status, error) {

                    },
                    function(msg) {

                    });
                }
                else
                {
                    var fd = new FormData();
                    fd.append('publisher', window.champ_user_id);
                    fd.append('receivers', remote_peers[0]);
                    fd.append('message', msg);
                    fd.append('chat_id',-1);
                    fd.append('chat_type', 0);
                    fd.append('file', _this_obj.chat_contnt.parent().find(".chat_attached_file")[0].files[0]);
                    //alert(chat_type);
                    _this_obj.post_message(fd,function(data) {
                        _this_obj.chat_contnt.append(data.data.chat_popup);
                    $(_this_obj.chat_contnt).scrollTop($(_this_obj.chat_contnt)[0].scrollHeight + 7);
                    },
                    function(jqxhr, status, error) {
                        console.log(jqxhr);
                    },
                    function(msg) {

                    });

                }
            });

            this.chat_contnt.parent().find(".chat_input_textbox").on("click", function(e) {
                var remote_peers = _this_obj.remote_peers;
                var chat_type = _this_obj.chat_type; //p2p chat.
                var __chat_id = null;

                if(chat_type == 0) {
                    __chat_id = remote_peers[0];
                }
                else if(chat_type == 1) {
                    __chat_id = _this_obj.conversation_id;
                }

                if(__chat_id != null) {
                    _this_obj.mark_messages_read(chat_type, __chat_id, "all", function(data) {
                        //alert("lol!'");
                    },
                    function(jqxhr, status, message) {

                    },
                    function(msg) {

                    });
                }
            });

            this.chat_contnt.parent().find(".progress_cancel").on("click", function(e) {
                if(_this_obj.current_ajax_request != undefined && _this_obj.current_ajax_request != false) {
                    _this_obj.current_ajax_request.abort();
                    _this_obj.chat_contnt.parent().find(".chat_attached_file").val("");
                    _this_obj.chat_contnt.parent().find(".progress_bar_container").hide();
                }
            });

            this.chat_contnt.parent().find(".chat_input_textbox").keypress(function(event)
            {

                //var chat_object = {user_id:1233,name: "Sohel",img_url: ""};
                if(event.keyCode == 13)
                {
                    var remote_peers = _this_obj.remote_peers;
                    //var name = _this_obj.chat_contnt.parent().find(".chat_widget_name").text();
                    //var pimg_url = "";
                    var msg = $(this).val();
                    var chat_type = _this_obj.chat_type; //p2p chat.
                    var __chat_id = null;
                    //alert(msg);
                    if(msg != "")
                    {
//                        console.log("Meu!")
//                        console.log(_this_obj.conversation_id);
                        if(chat_type == 1) //group chat
                        {
                            var conv_id = _this_obj.conversation_id;
                            __chat_id = conv_id;
                            var receivers = "";
                            for(var l = 0 ; l < remote_peers.length; l++) {
                                receivers += remote_peers[l]+",";
                            }
                            var fd = new FormData();
                            fd.append('publisher', window.champ_user_id);
                            fd.append('receivers', receivers);
                            fd.append('message', msg);
                            fd.append('chat_id',conv_id);
                            fd.append('chat_type', 1);
                            _this_obj.post_message(fd,function(data) {
                            _this_obj.chat_contnt.append(data.data.chat_popup);
                            $(_this_obj.chat_contnt).scrollTop($(_this_obj.chat_contnt)[0].scrollHeight + 7);
                            },
                            function(jqxhr, status, error) {

                            },
                            function(msg) {

                            });
                        }
                        else
                        {
                            __chat_id = remote_peers[0];
                            var fd = new FormData();
                            fd.append('publisher', window.champ_user_id);
                            fd.append('receivers', remote_peers[0]);
                            fd.append('message', msg);
                            fd.append('chat_id',-1);
                            fd.append('chat_type', 0);
                            _this_obj.post_message(fd,function(data) {
                            _this_obj.chat_contnt.append(data.data.chat_popup);
                            $(_this_obj.chat_contnt).scrollTop($(_this_obj.chat_contnt)[0].scrollHeight + 7);
                            },
                            function(jqxhr, status, error) {

                            },
                            function(msg) {

                            });

                        };

                        $(this).val("");
                        $(this).focus();
                    }
                }
            });

            Chat.prototype.file_upload_progress_observer = function(e){
                if(e.lengthComputable){
                    //console.log();
                    //console.log(e.loaded);
                    //console.log(e.total);
                    if(_this_obj.chat_contnt.parent().find(".click_chat_attachment").parent().find(".chat_attached_file").val() != "") {
                        var max = e.total;
                        var current = e.loaded;

                        var Percentage = parseInt((current * 100)/max);

                        _this_obj.chat_contnt.parent().find(".progress_bar_container").show();
                        _this_obj.chat_contnt.parent().find(".progress-bar").css("width",Percentage+"%");
                        _this_obj.chat_contnt.parent().find(".progress-bar").text(Percentage+"%");


                        if(Percentage >= 100)
                        {
                            _this_obj.chat_contnt.parent().find(".chat_attached_file").val("");
                           _this_obj.chat_contnt.parent().find(".progress_bar_container").hide();
                        }
                    }
                }
             };

            Chat.prototype.post_message = function(data, success_callback, error_back, complete_back) {
                _this_obj.current_ajax_request = $.ajax({
                    type: "POST",
                    url: "/ajax/send-chat-message/",
                    data: data,
                    timeout: 3000,
                    enctype: 'multipart/form-data',
                    contentType: false,
                    processData: false,
                    xhr: function() {
                        var myXhr = $.ajaxSettings.xhr();
                        if(myXhr.upload){
                            myXhr.upload.addEventListener('progress',_this_obj.file_upload_progress_observer, false);
                        }
                        return myXhr;
                    },
                    cache:false,
                    success: function(data)
                    {
                        success_callback(jQuery.parseJSON(data));
                    },
                    error: function(jqxhr, status, error)
                    {
                        error_back(jqxhr, status, error);
                    }
                })
                .done(function( msg ) {
                    complete_back(msg);
                });
            }

            this.chat_contnt.click(function(e)
            {
                $(this).parent().find(".chat_input_textbox").focus();
                $(this).parent().find(".add_chat_buddy_area").hide();
            });

//            var _this_obj = this;
            this.load_chat_messages(-1, function(data) {
                //console.log(data);
                if(data.status == "SUCCESS") {
                    for(var index = 0 ; index < data.data.rows.length ; index++) {
                        _this_obj.chat_contnt.prepend(data.data.rows[index]);
                    }
                    $(_this_obj.chat_contnt).scrollTop($(_this_obj.chat_contnt)[0].scrollHeight + 7);
                    if(data.data.hide_textbox == 1) {
                        _this_obj.showUserBlocked("You are no longer in this conversation");

//                        if(_this_obj.is_blocked) {
//                            _this_obj.chat_contnt.parent().find(".chat_bottom_status").hide();
//                        }
                          if(_this_obj.window_minimized) {
                                _this_obj.chat_contnt.parent().find(".chat_bottom_status").hide();
                          }
                          else {
                               //_this_obj.chat_contnt.parent().find(".chat_bottom_status").hide();
                          }
                        _this_obj.hideAddBuddyButton();
                    }
                }
                else {
                    if(data.message == "LOGIN_REQUIRED") {
//                        alert("Login Required");
//                        console.log(_this_obj.parent().find(".chat_txt"));
                       show_signin();
                       _this_obj.close();
                    }
                    else if(data.message == "USER_BLOCKED") {
                        _this_obj.showUserBlocked("You can't reply to this conversation");
                        _this_obj.hideAddBuddyButton();
                    }
                }

            }, function(xhr, status, error) {

            },
            function(msg) {

            });

            var lastScrollTop = 0;

            $(_this_obj.chat_contnt).on('scroll', function(e) {
                st = $(this).scrollTop();
                console.log(e);
                if(st < lastScrollTop) {
                      if(st == 0) { //Reach's the top of the div.
                            var chat_rows = $(_this_obj.chat_contnt).find(".chat_box_row");
                            if(chat_rows.length) {
                                var first_entry = chat_rows.first();
                                lastScrollTop = first_entry.scrollTop();
                                var since_pk = first_entry.data("msg-pk");
                                _this_obj.load_chat_messages(since_pk, function(data) {

                                    //console.log(data);

                                    if(data.status == "SUCCESS") {
                                        for(var index = 0 ; index < data.data.rows.length ; index++) {
                                            _this_obj.chat_contnt.prepend(data.data.rows[index]);
                                        }
                                        if(data.data.rows.length > 0) {
                                            $(_this_obj.chat_contnt).scrollTop($(_this_obj.chat_contnt)[0].scrollHeight + 7);
                                        }
                                    }
                                    else {
                                        if(data.message == "LOGIN_REQUIRED") {
//                                            alert("Login Required");
//                                            _this_obj.parent().find(".chat_txt").hide();
//                                            _this_obj.parent().find(".chat_bottom_status").show();
                                        }
                                    }

                                }, function(xhr, status, error) {

                                },
                                function(msg) {

                                });
                            }
                      }
                }
                else {
//                    console.log('down 1');
                }
                lastScrollTop = st;
            });

            _this_obj.chat_contnt.parent().find(".add_grp_buddy_txt").tagit({
                autocomplete: {
                    autoSelectFirst: true,
                    delay: 0,
                    minLength: 2,
                    source: function(request, response) {
                        var buddy_ids = [];
                        $(".tagit-hidden-ids").each(function(i){
                            buddy_ids.push(parseInt($(this).val()));
                        });

                        for(var i = 0 ; i < _this_obj.remote_peers.length ; i++){
                            buddy_ids.push(parseInt(_this_obj.remote_peers[i]));
                        }

                        var buddy_ids_str = "";

                        for(var i = 0 ; i < buddy_ids.length ; i++){
                            if(i < buddy_ids.length - 1){
                                buddy_ids_str += buddy_ids[i]+",";
                            }
                            else{
                                buddy_ids_str += buddy_ids[i];
                            }
                        }

                        $.ajax({
                            url: "/ajax/search_user",
                            dataType: "json",
                            data: {
                                term: request.term,
                                exclude: buddy_ids_str
                            },
                            success: function(data) {
                                response(data);
                            }
                        });
                    },
                    fieldName : "user_id",
                    focus: function (event, ui) {
                        _this_obj.chat_contnt.parent().find(".hidden-selected-val").val(ui.item.value);
                        //console.log(_this_obj.chat_contnt.parent().find(".hidden-selected-val").val());
                        ui.item.value = ui.item.label;
                    },
                    select: function( event, ui ) {

                    },
                    create: function() {
                        $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
                            return $("<li></li>")
                                .data("item.autocomplete", item)
                                .append("<a><div class='ui-menu-item-div' style='background:white;margin-top:1px;padding:4px;'><img src='"+ item.pimage +"' width='20' height='20' style='margin-right:10px;'/>" + item.label + "</div></a>")
                                .appendTo(ul);
                        };
                    }
                },
                showAutocompleteOnFocus : true,
                allowDuplicates : true,
                animate: true,
                allowSpaces: true,
                tagLimit : 10,
                placeholderText : "Type Name Here",
                // Event callbacks.
                beforeTagAdded : function(event,ui){
                    console.log("Before Tag Added");
                    console.log(ui);
                    console.log("Before Tag Added done.");
                },
                afterTagAdded   : function(event,ui){
                    _this_obj.chat_contnt.parent().find(".hidden-selected-val").val("");
                },
                afterTagRemoved : function(event,ui){

                },
                onTagClicked: function(event,ui){
                    //alert("Clicked!");
                }
            });

            this.chat_contnt.parent().find(".add_grp_buddy_btn").click(function(e){
                var buddy_ids = [];
                $(".tagit-hidden-ids").each(function(i){
                    buddy_ids.push(parseInt($(this).val()));
                });
                for(var i = 0 ; i < _this_obj.remote_peers.length ; i++){
                    buddy_ids.push(parseInt(_this_obj.remote_peers[i]));
                }
                var chat_text_value = _this_obj.chat_contnt.parent().find(".add_grp_buddy_txt").val();
                if($.trim(chat_text_value) == ""){
                    _this_obj.chat_contnt.parent().find(".add_chat_buddy_area").slideUp(200);
                }
                else{
                    if(_this_obj.chat_type == 0){
                        var chat = new Chat();
                        chat.remote_peers = buddy_ids;
                        chat.chat_type = 1;
                        chat.conversation_id = chat.id;
//                        console.log(chat.conversation_id);
                        var chat_object = {user_id:-1,name: "Group Chat"};
                        chat.addNewChat(chat_object);
                        window.chat_boxes[chat.id] = chat;
                        _this_obj.chat_contnt.parent().find(".add_chat_buddy_area").slideUp(200);
                        _this_obj.chat_contnt.parent().find(".add_grp_buddy_txt").tagit("removeAll");
                    }
                    else{
                        //alert("This is group chat.");
                        $.ajax({
                            type: "POST",
                            url: "/ajax/add_buddy_to_conversation/",
                            data: { "buddy-list[]": buddy_ids, "conversation_id": _this_obj.conversation_id, "format": "json" },
                            success: function(data)
                            {
                                var data = jQuery.parseJSON(data);
                                if(data.status == "SUCCESS"){
                                    _this_obj.remote_peers = buddy_ids;
                                    _this_obj.chat_contnt.parent().find(".add_chat_buddy_area").slideUp(200);
                                    _this_obj.chat_contnt.parent().find(".add_grp_buddy_txt").tagit("removeAll");
                                }
                                else{

                                }
                            },
                            error: function(jqxhr, status, error)
                            {

                            }
                        })
                        .done(function( msg ) {

                        });
                    }
                }
            });

            this.chat_contnt.parent().find(".chat_header").click(function(e)
            {
                e.preventDefault();
                if(!_this_obj.window_minimized)
                {
                    _this_obj.hide();
                    _this_obj.updateChatboxSettings({status: "hide" });
                    if(_this_obj.is_blocked) {
                        _this_obj.chat_contnt.parent().find(".chat_bottom_status").hide();
                    }
                    _this_obj.window_minimized = true;

                    $(this).find(".chat_min_btn > img").prop("src", "/static/images/expend.png");
                }
                else
                {
                    _this_obj.show();
                    _this_obj.updateChatboxSettings({status: "show" });
                    if(_this_obj.is_blocked) {
                        _this_obj.chat_contnt.parent().find(".chat_bottom_status").show();
                    }
                    _this_obj.window_minimized = false;

                    $(this).find(".chat_min_btn > img").prop("src", "/static/images/min.png");

                }
                _this_obj.markDeativeChatHeader();
                return false;
            });

            // this.chat_contnt.parent().find(".add_chat_buddy_icon").click(function(e){
            //   //alert("Clicked!");
            //     $(this).parent().parent().find(".add_chat_buddy_area").show();
            // });

            this.chat_contnt.parent().find(".chat_close").click(function(e)
            {
                e.preventDefault();
                _this_obj.close();

            });

            this.chat_contnt.parent().find(".chat_add_buddy").click(function(e){
                _this_obj.chat_contnt.parent().find(".add_chat_buddy_area").slideDown(200);
                return false;
            });

            this.chat_contnt.parent().find(".chat_min_btn").click(function(e){
                e.preventDefault();
                if(!_this_obj.window_minimized)
                {
                    _this_obj.hide();
                    _this_obj.updateChatboxSettings({status: "hide" });
                    if(_this_obj.is_blocked) {
                        _this_obj.chat_contnt.parent().find(".chat_bottom_status").hide();
                    }
                    _this_obj.window_minimized = true;

                    $(this).parent().find(".chat_min_btn > img").prop("src", "/static/images/expend.png");

                }
                else
                {
                    _this_obj.show();
                    _this_obj.updateChatboxSettings({status: "show" });
                    if(_this_obj.is_blocked) {
                        _this_obj.chat_contnt.parent().find(".chat_bottom_status").show();
                    }
                    _this_obj.window_minimized = false;

                    $(this).parent().find(".chat_min_btn > img").prop("src", "/static/images/min.png");

                }
                _this_obj.markDeativeChatHeader();
                return false;
            });

            if(this.chat_type == 0) //p2p chat
            {
                window.chat_boxes[_this_obj.remote_peers[0]] = this;
            }
            else{
                window.chat_boxes[_this_obj.id] = this;
            }

            redraw_chat_boxes();

        };

        Chat.prototype.updateChatBottomStatusMessage = function(msg) {
            this.chat_contnt.parent().find(".chat_bottom_status > span").text(msg);
        };

        Chat.prototype.goOffline = function() {
            this.chat_contnt.parent().find(".chat_header").find("span.online").find("img").prop("src", "/static/images/chat_offline.png");
            this.chat_contnt.parent().find(".chat_header").find("span.online").removeClass("online").addClass("offline").addClass("hidden");
        };

        Chat.prototype.goOnline = function() {
            this.chat_contnt.parent().find(".chat_header").find("span.offline").find("img").prop("src", "/static/images/online.png");
            this.chat_contnt.parent().find(".chat_header").find("span.offline").removeClass("offline").removeClass("hidden").addClass("online");
        };

        Chat.prototype.showUserBlocked = function(msg) {
             this.updateChatBottomStatusMessage(msg);
             this.chat_contnt.parent().find(".chat_txt").hide();
             this.chat_contnt.parent().find(".chat_bottom_status").show();
             this.is_blocked = true;
        };

        Chat.prototype.showUserUnBlocked = function() {
             this.updateChatBottomStatusMessage("");
             this.chat_contnt.parent().find(".chat_txt").show();
             this.chat_contnt.parent().find(".chat_bottom_status").hide();
             this.is_blocked = false;
        };

        Chat.prototype.close = function(dont_update_chat_settings) {
            this.chat_contnt.parent().remove();

            if(this.chat_type == 0)
            {
                if(this.remote_peers && window.chat_boxes[this.remote_peers[0]])
                {
                    delete window.chat_boxes[this.remote_peers[0]];
                }
            }
            else
            {
                if(window.chat_boxes[this.id])
                {
                    delete window.chat_boxes[this.id];
                }
            }
            redraw_chat_boxes();
            if(dont_update_chat_settings != undefined || !dont_update_chat_settings) {
                this.updateChatboxSettings({status: "close" });
            }
        }

        Chat.prototype.updateChatboxSettings = function(settings) {
            var $this_object = this;

            var chat_id = -1;
            if($this_object.chat_type == 0) {
                chat_id = $this_object.remote_peers[0];
            }
            else if($this_object.chat_type == 1) {
                chat_id = $this_object.conversation_id;
            }

            var default_settings = { action: "chatbox", chat_type: $this_object.chat_type, chat_id: chat_id };
            $.extend(true, default_settings, settings);

            $.ajax({
                type: "POST",
                url: "/ajax/chat_settings/",
                data: default_settings,
                enctype: 'multipart/form-data',
                success: function(data)
                {
                    var data = jQuery.parseJSON(data);
                },
                error: function(jqxhr, status, error)
                {

                }
            })
            .done(function( msg ) {

            });
        }

        Chat.prototype.hide = function()
        {
            var container_element = this.chat_contnt.parent();
            container_element.find(".chat_contnt").hide();
            container_element.find(".chat_txt").hide();
            container_element.css("height","40px");

            this.window_minimized = true;

            if(this.is_blocked) {
                container_element.find(".chat_bottom_status").hide();
            }

        };

        Chat.prototype.updateChatTitle = function(title) {
            this.chat_contnt.parent().find(".chat_title").text(title);
        };

        Chat.prototype.updateRemotePeers = function(remote_peers) {
            this.remote_peers = remote_peers;
        }

        Chat.prototype.show = function()
        {
            var container_element = this.chat_contnt.parent();
            container_element.find(".chat_contnt").show();
            container_element.find(".chat_txt").show();
            container_element.css("height","262px");
            this.window_minimized = false;
        };

        Chat.prototype.focusInput = function()
        {
            this.chat_contnt.parent().find(".chat_input_textbox").focus();
        };

        Chat.prototype.scrollToBottom = function()
        {
            var _this_obj = this;
            $(_this_obj.chat_contnt).scrollTop($(_this_obj.chat_contnt)[0].scrollHeight + 7);
        };

        Chat.prototype.load_chat_messages = function(since_pk, success_callback, error_back, complete_back) {
            if(window.champ_user_id != undefined && window.champ_user_id != -1) {
                var _this_obj = this;
                $.ajax({
                    type: "GET",
                    url: "/ajax/fetch_chat_messages/",
                    data: { chat_type: _this_obj.chat_type, chat_id: _this_obj.conversation_id, user_id: _this_obj.remote_peers[0], since_pk: since_pk },
                    enctype: 'multipart/form-data',
                    success: function(data)
                    {
                        success_callback(jQuery.parseJSON(data));
                    },
                    error: function(jqxhr, status, error)
                    {
                        error_back(jqxhr, status, error);
                    }
                })
                .done(function( msg ) {
                    complete_back(msg);
                });
            }
        }

        Chat.prototype.mark_messages_read = window.mark_messages_read = function(chat_type, chat_id, message_ids, success_callback, error_back, complete_back) {
            $.ajax({
                type: "POST",
                url: "/ajax/mark_message_mutate/",
                data: { chat_type: chat_type, chat_id: chat_id, message_ids: message_ids },
                enctype: 'multipart/form-data',
                success: function(data)
                {
                    success_callback(jQuery.parseJSON(data));
                },
                error: function(jqxhr, status, error)
                {
                    error_back(jqxhr, status, error);
                }
            })
            .done(function( msg ) {
                complete_back(msg);
            });
        };

        Chat.prototype.addNewIncomingChat = function(incomingMsg)
        {
            this.chat_contnt.append(incomingMsg);
            this.scrollToBottom();

        };

        Chat.prototype.addNewOutgoingChat = function(user_id,name,pimg_url,msg)
        {

        };

        Chat.prototype.markActiveChatHeader = function() {
             this.chat_contnt.parent().find(".chat_header").addClass("chat_header_active");
        };

        Chat.prototype.markDeativeChatHeader = function() {
             this.chat_contnt.parent().find(".chat_header").removeClass("chat_header_active");
        };

        Chat.prototype.onIncomingMessage = function(data)
        {
            //console.log("Incoming message received.");
            //console.log(data);
            this.addNewIncomingChat(data);
        };

        var register_global_socket_events = function()
        {
            if(window.socket != undefined){

                window.socket.on("CHAT_SETTINGS_UPDATED", function(data) {
                    if(data.action == "chatbox") {
                        var chat_id = data.chat_id;
                        if(data.chat_type == 0) {
                            chat_id = parseInt(chat_id);
                        }
                        var remote_peers = data.remote_peers;
                        if(data.status == "show")
                        {
                            if(!window.chat_boxes.hasOwnProperty(chat_id)) {
                                var chat_object = {
                                    user_id:chat_id,
                                    name: data.title,//data.local_peer.name,
                                };
                                var chat = new Chat();
                                chat.remote_peers =  remote_peers; //data.local_peer.uid;
                                chat.addNewChat(chat_object);
                                window.chat_boxes[chat_id] = chat;
                            }
                            window.chat_boxes[chat_id].show();
                            window.chat_boxes[chat_id].focusInput();
                            window.chat_boxes[chat_id].markDeativeChatHeader();
                        }
                        else if(data.status == "hide") {
                            if(window.chat_boxes.hasOwnProperty(chat_id)) {
                                window.chat_boxes[chat_id].hide();
                            }
                        }
                        else if(data.status == "close") {
                            if(window.chat_boxes.hasOwnProperty(chat_id)) {
                                window.chat_boxes[chat_id].close();
                            }
                        }
                    }
                    else if(data.action == "online_status") {
                        if(data.status == "online") {
                            window.expand_chat_sidebar(true);
                        }
                        else if(data.status == "offline") {
                            window.expand_chat_sidebar(false);
                        }
                    }
                    else if(data.action == "message_details_enter") {
                        if($("input[name=enter_checkbox]").length) {
                            if(data.status == 1) {
                                $("input[name=enter_checkbox]").prop("checked", true);
                            }
                            else {
                                $("input[name=enter_checkbox]").prop("checked", false);
                            }
                        }
                    }
                });

                window.socket.on("MESSAGE_UNREAD_COUNT_RECEIVED", function(data) {
                    //place unread notification count.
                    if($("#id_message_notification").length) {
                        $("#id_message_notification").text(data.unread_message_count);
                        if(data.unread_message_count == 0) {
                            $("#id_message_notification").hide();
                        }
                        else {
                            $("#id_message_notification").hide();
                        }
                    }
                });

                window.socket.on("USER_BLOCKED", function(data) {
                    //place unread notification count.
                    var blocked_user_id = data.blocked_user;
                    if(window.chat_boxes.hasOwnProperty(blocked_user_id)) {
                        window.chat_boxes[blocked_user_id].showUserBlocked("You can't reply to this conversation");
                    }
                    if($(".chat_reply").length) {
                        $(".chat_reply").remove();
                    }
                    window.chat_boxes[blocked_user_id].hideAddBuddyButton();
                });

                window.socket.on("USER_UNBLOCKED", function(data) {
                    //place unread notification count.
                    var blocked_user_id = data.blocked_user;
                    if(window.chat_boxes.hasOwnProperty(blocked_user_id)) {
                        window.chat_boxes[blocked_user_id].showUserUnBlocked();
                    }
                    window.chat_boxes[blocked_user_id].showAddBuddyButton();
                });

                window.socket.on("CONVERSATION_TITLE_CHANGED", function(data) {
                    if(window.chat_boxes.hasOwnProperty(data.chat_id)) {
                        window.chat_boxes[data.chat_id].updateChatTitle(data.title);
                    }
                    if($("#id_conv_title").length) {
                        $("#id_conv_title").text(data.title);
                    }
                });

                window.socket.on("CONVERSATION_BUDDY_ADDED", function(data) {
                    console.log(data);
                    if(window.chat_boxes.hasOwnProperty(data.chat_id)) {
                        window.chat_boxes[data.chat_id].updateRemotePeers(data.receivers);
                    }
                });

                window.socket.on("CONVERSATION_BUDDY_LEFT", function(data) {
                    if(window.chat_boxes.hasOwnProperty(data.chat_id)) {
                        window.chat_boxes[data.chat_id].updateRemotePeers(data.receivers);
                        window.chat_boxes.hasOwnProperty(data.chat_id);
                        window.chat_boxes[data.chat_id].showUserBlocked("You are no longer in this conversation");
                        window.chat_boxes[data.chat_id].hideAddBuddyButton();
                    }
                });

                window.socket.on("CHAT_MESSAGE_RECEIVED", function(data)
                {
                    console.log(data);
                    if(data.chat_type == 0) //p2p chat
                    {
                        var added_new = false;
                        var entry_exists = false;
                        if(!window.chat_boxes.hasOwnProperty(data.id))
                        {
                            var chat_object = {
                                user_id:data.id,
                                name: data.title,//data.local_peer.name,
                            };
                            var chat = new Chat();
                            chat.remote_peers =  [data.id]; //data.local_peer.uid;
                            var offline = true;
                            if(data.online_status == "online") {
                                offline = false;
                            }
                            chat.addNewChat(chat_object,offline);
                            window.chat_boxes[data.id] = chat;
                            added_new = true;
                            entry_exists = true;

                        }
                        if(!window.chat_boxes[data.id].window_minimized) {
                            window.chat_boxes[data.id].show();
                            window.chat_boxes[data.id].focusInput();
                            if(added_new) {
                                window.chat_boxes[data.id].updateChatboxSettings({status: "show" });
                            }
                            window.chat_boxes[data.id].markDeativeChatHeader();
                        }
                        else {
                            window.chat_boxes[data.id].markActiveChatHeader();
                            if(added_new) {
                                window.chat_boxes[data.id].updateChatboxSettings({status: "hide" });
                            }
                        }
                        if(!entry_exists) {
                            window.chat_boxes[data.id].addNewIncomingChat(data.chat_popup);
                        }

                        //Add in the details page if opened.
                        if($("#id_message_list").length) {
                            var page_chat_type = $("#id_page_chat_type").val();
                            var page_chat_id = $("#id_page_chat_id").val();
                            if(page_chat_type == "0" && page_chat_id == data.id) {
                                var html_content = data.msg_detail_entry;
                                $("#id_message_list").append(html_content);
                                    //$('#id_message_list').scrollTop();
                                $('#id_message_list').animate({
                                    scrollTop: $('#id_message_list')[0].scrollHeight
                                }, 1000);
                                $("form[class=chat_reply]").find("input[name=id_hidden_tutor]").val(data.id);
                            }
                        }

                        //Place entry to the message list view
                        if($("#id_message_entry_list").length) {
                            //console.log(data);
                            var $expected_li = null;
                            $(".message_list_entry").each(function(i) {
                                var chat_type = parseInt($(this).find(".chat_type").val());
                                var chat_id = parseInt($(this).find(".chat_id").val());
                                var user_message_id = parseInt($(this).find(".user_message_id").val());
                                if(chat_type == 0 && chat_id == data.id) {
                                    if(data.object_id != user_message_id) {
                                        $expected_li = $(this);
                                    }
                                }
                            });
                            //console.log($expected_li);
                            if($expected_li != null) {
                                $expected_li.remove();
                                $("#id_message_entry_list").prepend($(data.message_list_entry));
                            }
                        }

                        //place unread notification count.
//                        if($("#id_message_notification").length && data.unread_message_count) {
//                            $("#id_message_notification").text(data.unread_message_count);
//                        }

                    }
                    else if(data.chat_type == 1) //Group chat
                    {
                        var added_new = false;
                        var entry_exists = false;
                        if(!window.chat_boxes.hasOwnProperty(data.id))
                        {
                            console.log(data.remote_peers);
                            var chat_object = {
                                user_id:data.id,
                                name: data.title,//data.local_peer.name,
                            };
                            var chat = new Chat();
                            chat.id = data.id;
                            chat.chat_type = 1; //Group chat.
                            chat.conversation_id = data.id;
                            chat.remote_peers =  data.remote_peers, //data.local_peer.uid;
                            chat.addNewChat(chat_object);
                            window.chat_boxes[data.id] = chat;
                            added_new = true;

//                            var entry_id = $(data.chat_popup).find(".chat_box_row").data("msg-pk");
//                            console.log($(data.chat_popup).find(".chat_box_row").data("msg-pk"));
//                            $(".chat_box_row").each(function(i) {
//                                 entry_exists = true;
//                            });
                              entry_exists = true;
                        }

                        if(!window.chat_boxes[data.id].window_minimized) {
                            window.chat_boxes[data.id].show();
                            window.chat_boxes[data.id].focusInput();
                            if(added_new) {
                                window.chat_boxes[data.id].updateChatboxSettings({status: "show" });
                            }
                            window.chat_boxes[data.id].markDeativeChatHeader();
                        }
                        else {
                            window.chat_boxes[data.id].markActiveChatHeader();
                            if(added_new) {
                                window.chat_boxes[data.id].updateChatboxSettings({status: "hide" });
                            }
                        }
                        if(!entry_exists) {
                            window.chat_boxes[data.id].addNewIncomingChat(data.chat_popup);
                        }

//                        window.chat_boxes[data.id].show();
//                        window.chat_boxes[data.id].focusInput();
//                        window.chat_boxes[data.id].addNewIncomingChat(data.chat_popup);

                        //Add in the details page if opened.
                        if($("#id_message_list").length) {
                            var page_chat_type = $("#id_page_chat_type").val();
                            var page_chat_id = $("#id_page_chat_id").val();
                            if(page_chat_type == "1" && page_chat_id == data.id) {
                                var html_content = data.msg_detail_entry;
                                $("#id_message_list").append(html_content);
                                    //$('#id_message_list').scrollTop();
                                $('#id_message_list').animate({
                                    scrollTop: $('#id_message_list')[0].scrollHeight
                                }, 1000);

                                var rp_html = "";
                                for(var l = 0 ; l < data.remote_peers.length; l++) {
                                    rp_html += "<input type=\"hidden\" name=\"conversation_users[]\" class=\"conv_users\" value=\""+ data.remote_peers[l] +"\"/>";
                                }
                                $("#id_conversation_users").html(rp_html);
                            }
                        }
                        //Place entry to the message list view
                        if($("#id_message_entry_list").length) {
                            //console.log(data);
                            var $expected_li = null;
                            $(".message_list_entry").each(function(i) {
                                var chat_type = parseInt($(this).find(".chat_type").val());
                                var chat_id = $(this).find(".chat_id").val();
                                var user_message_id = parseInt($(this).find(".user_message_id").val());
                                if(chat_type == 1 && chat_id == data.id) {
                                    if(data.object_id != user_message_id) {
                                        $expected_li = $(this);
                                    }
                                }
                            });
                            //console.log($expected_li);
                            if($expected_li != null) {
                                $expected_li.remove();
                                $("#id_message_entry_list").prepend($(data.message_list_entry));
                            }
                        }
                        //place unread notification count.
                        if($("#id_message_notification").length) {
                            $("#id_message_notification").text(data.unread_message_count);
                        }
                    }
                });
            }

        };

        var attach_event_handlers = function()
        {
            $(document).on("click",".chat_sidebar_header",function(e)
            {
                $(this).parent().hide();
            });

            $(document).on("click",".champ_text_chat",function(e)
            {
                e.preventDefault();
                if(window.champ_user_id != undefined && window.champ_user_id != -1) {
                    //get the tutor's user_id value.
                    var uid = $(this).parent().find(".tutor_uid").val();
                    var offline_status = true; //Say user is offline
                    var offline_status_found = $(this).parent().find(".online_status").val();

                    if(offline_status_found == "online"){
                        offline_status = false;
                    }

                    uid = parseInt(uid);

                    //console.log(window.chat_boxes);

                    if(!window.chat_boxes.hasOwnProperty(uid))
                    {
                        var tutor_name = $(this).parent().find(".tut_name").val();
                        var img_src = $(this).parent().find("img").attr("src");
                        var chat_object = {user_id:uid,name: tutor_name,img_url: img_src};

                        var chat = new Chat();
                        var uids = [uid];
                        chat.remote_peers = uids;
                        chat.addNewChat(chat_object,offline_status);
                        //window.chat_boxes[uid] = chat;
                    }
                    window.chat_boxes[uid].show();
                    window.chat_boxes[uid].focusInput();

                    window.chat_boxes[uid].updateChatboxSettings({status: "show" });

                    //console.log(window.chat_boxes[uid].window_minimized);
                }
                else {
                    show_signin();
                }
            });

        };

        $(document).ready(function()
        {
            register_global_socket_events();
            attach_event_handlers();
        });
    })();

window.openChatbox = function(tutor_id, online_status, tutor_name, tutor_img) {
    if(window.champ_user_id != undefined && window.champ_user_id != -1) {
        //get the tutor's user_id value.
        var uid = tutor_id;
        var offline_status = true; //Say user is offline
        var offline_status_found = online_status;

        if(offline_status_found == "online"){
            offline_status = false;
        }

        uid = parseInt(uid);

        //console.log(window.chat_boxes);

        if(!window.chat_boxes.hasOwnProperty(uid))
        {
            var tutor_name = tutor_name;
            var img_src = tutor_img;
            var chat_object = {user_id:uid,name: tutor_name,img_url: img_src};
            //console.log(chat_object);
            var chat = new Chat();
            var uids = [uid];
            chat.remote_peers = uids;
            chat.addNewChat(chat_object,offline_status);
            //window.chat_boxes[uid] = chat;
        }
        else
        {
            window.chat_boxes[uid].show();
            window.chat_boxes[uid].focusInput();
        }
        //console.log(window.chat_boxes[uid].window_minimized);
    }
    else {
        show_signin();
    }
}

window.openChatPopup = function(chat_type, chat_id, chat_title, online_status, remote_peers, topic_class_name, topic_object_id, show, dont_update_chat_settings, blocked) {
    if(window.champ_user_id != undefined && window.champ_user_id != -1) {

        chat_type = parseInt(chat_type);
        if(chat_type == 0) {
            chat_id = parseInt(chat_id);
        }

        var offline_status = true; //Say user is offline
        var offline_status_found = online_status;

        if(offline_status_found == "online"){
            offline_status = false;
        }

        //console.log(window.chat_boxes);

        if(!window.chat_boxes.hasOwnProperty(chat_id))
        {
            var chat_object = {
                user_id:chat_id,
                name: chat_title,//data.local_peer.name,
            };
            var chat = new Chat();
            chat.remote_peers =  remote_peers;
            chat.id = chat_id;
            chat.conversation_id = chat_id;
            chat.chat_type = chat_type;
//            console.log(chat);
            chat.addNewChat(chat_object, offline_status);
            window.chat_boxes[chat_id] = chat;
            if(topic_class_name != undefined && topic_object_id != undefined) {
                window.chat_boxes[chat_id].topic_class_name = topic_class_name;
                window.chat_boxes[chat_id].topic_object_id = topic_object_id;
            }
        }
//        alert(show);
        if(show != undefined && show == 1) {
            window.chat_boxes[chat_id].show();
            window.chat_boxes[chat_id].focusInput();
            window.chat_boxes[chat_id].window_minimized = false;
            if(dont_update_chat_settings == undefined || dont_update_chat_settings == false) {
                window.chat_boxes[chat_id].updateChatboxSettings({status: "show" });
            }
            if(blocked != undefined && blocked == 1) {
                window.chat_boxes[chat_id].showUserBlocked("You can't reply to this conversation");
                window.chat_boxes[chat_id].hideAddBuddyButton();
            }
        }
        else {
            if(blocked != undefined && blocked == 1) {
                window.chat_boxes[chat_id].showUserBlocked("You can't reply to this conversation");
                window.chat_boxes[chat_id].hideAddBuddyButton();
            }
            window.chat_boxes[chat_id].hide();
            window.chat_boxes[chat_id].window_minimized = true;
            if(dont_update_chat_settings == undefined || dont_update_chat_settings == false) {
                window.chat_boxes[chat_id].updateChatboxSettings({status: "hide" });
            }

        }
    }
    else {
        show_signin();
    }
}

});


