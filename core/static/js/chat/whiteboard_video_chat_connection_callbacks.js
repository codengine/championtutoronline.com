/**
 * Created with PyCharm.
 * User: codengine
 * Date: 8/21/15
 * Time: 10:43 PM
 * To change this template use File | Settings | File Templates.
 */

var session_connected_callback = function(event) {
    console.log("Session Initialized...");
    console.log("Now publishing stream to the session...");
    //Now publish the stream.
    publishStream(audio_only_mode);

    //Now subscribe to the stream that already in the session and i have just joined the session.
    for (var i = 0; i < event.streams.length; i++) {
        if (event.streams[i].connection.connectionId == ot_session.connection.connectionId) {
            console.log("Skipping my own session.");
            return;
        }
        var subscriber = ot_session.subscribe(event.streams[i], "id_video_section", {
            insertMode: "append"
        });

        subscriber.on({
            videoDisabled: function(event)
            {
                console.log("Video disabled.");
                console.log(event.reason);
                subscriber.setStyle('backgroundImageURI',
                    //'http://tokbox.com/img/styleguide/tb-colors-cream.png'
                    'http://'+ window.app_server +'static/images/profile_img.png'
                );
            },
            videoEnabled: function(event)
            {
                console.log("Video enabled.");
                console.log(event.reason);
                var imgData = subscriber.getImgData();
                subscriber.setStyle('backgroundImageURI', imgData);
            }
        });
        layout();
    }

};


var session_disconnected_callback = function(event) {
    console.log("Session Disconnected...");
    for (var i = 0; i < event.streams.length; i++) {
        if (event.streams[i].connection.connectionId == ot_session.connection.connectionId) {
            console.log("Skipping my own session.");
            return;
        }
        var subscriber = ot_session.subscribe(event.streams[i], "id_video_section", {
            insertMode: "append"
        });

        subscriber.on({
            videoDisabled: function(event)
            {
                console.log("Video disabled.");
                console.log(event.reason);
                subscriber.setStyle('backgroundImageURI',
                    //'http://tokbox.com/img/styleguide/tb-colors-cream.png'
                    'http://'+ window.app_server +'static/images/profile_img.png'
                );
            },
            videoEnabled: function(event)
            {
                console.log("Video enabled.");
                console.log(event.reason);
                var imgData = subscriber.getImgData();
                subscriber.setStyle('backgroundImageURI', imgData);
            }
        });
        layout();
    }
};

var stream_created_callback = function(event) {
    //This callback is fired when i am already in the session and new user has just joined the session.
    console.log("Stream created. Stream ID: "+event.stream.streamId);
    $("#id_end_call").prop("disabled",false);
    for (var i = 0; i < event.streams.length; i++) {
        if (event.streams[i].connection.connectionId == ot_session.connection.connectionId) {
            console.log("Skipping my own session.");
            return;
        }
        var subscriber = ot_session.subscribe(event.streams[i], "id_video_section", {
            insertMode: "append"
        });

        subscriber.on({
            videoDisabled: function(event)
            {
                console.log("Video disabled.");
                console.log(event.reason);
                subscriber.setStyle('backgroundImageURI',
                    //'http://tokbox.com/img/styleguide/tb-colors-cream.png'
                    'http://'+ window.app_server +'static/images/profile_img.png'
                );
            },
            videoEnabled: function(event)
            {
                console.log("Video enabled.");
                console.log(event.reason);
                var imgData = subscriber.getImgData();
                subscriber.setStyle('backgroundImageURI', imgData);
            }
        });
        layout();
    }
};

var connection_created_callback = function(event) {
    connection_count++;
    console.log("Connection Created...");
    var container_width_redefined = 300 ;// + (connection_count - 2) * 100;
    var container_height_redefined = 280 ;// + (connection_count - 2) * 100;

    if(connection_count >= 2){
        container_width_redefined = 300 + (connection_count - 2) * 100;
        container_height_redefined = 280 + (connection_count - 2) * 100;
    }

    console.log("Width and height redefined: "+container_width_redefined+", "+container_height_redefined);

    video_sharing_container.css("width",container_width_redefined+"px");
    video_sharing_container.css("height",container_height_redefined+"px");

    video_section.css("width",container_width_redefined+"px");
    video_section.css("height",container_height_redefined+"px");

    video_section.find(".video-box").css("width",video_box_width+"px");
    video_section.find(".video-box").css("height",video_box_height+"px");

    video_section.find( "[id^='id_video_pane']").css("width",video_box_width+"px");
    video_section.find( "[id^='id_video_pane']").css("height",video_box_height+"px");

    layout();
};

var connection_destroyed_callback = function(event) {
    connection_count--;
    console.log("Connection destroyed...");
    console.log("Connection Count: "+connection_count);
    if(connection_count == 1){
        disconnectSession();
        calls = {};
        ot_session = null;
        call_active = false;
        ot_session_id = null;
        publisher = null;
        connection_count = 0;
        $(".sharing_video").css("display","none");
    }

    var container_width_redefined = 300 ;// + (connection_count - 2) * 100;
    var container_height_redefined = 280 ;// + (connection_count - 2) * 100;

    if(connection_count >= 2){
        container_width_redefined = 300 + (connection_count - 2) * 100;
        container_height_redefined = 280 + (connection_count - 2) * 100;
    }

    video_sharing_container.css("width",container_width_redefined+"px");
    video_sharing_container.css("height",container_height_redefined+"px");

    video_section.css("width",container_width_redefined+"px");
    video_section.css("height",container_height_redefined+"px");

    video_section.find(".video-box").css("width",video_box_width+"px");
    video_section.find(".video-box").css("height",video_box_height+"px");

    video_section.find( "[id^='id_video_pane']").css("width",video_box_width+"px");
    video_section.find( "[id^='id_video_pane']").css("height",video_box_height+"px");

    layout();

};

var stream_destroyed_callback = function(event) {
    console.log("Stream destroyed...Stream ID: "+event.stream.streamId);
    for (var i = 0; i < event.streams.length; i++) {
        if (event.streams[i].connection.connectionId == ot_session.connection.connectionId) {
            console.log("Skipping my own session.");
            return;
        }
        var subscriber = ot_session.subscribe(event.streams[i], "id_video_section", {
            insertMode: "append"
        });

        subscriber.on({
            videoDisabled: function(event)
            {
                console.log("Video disabled.");
                console.log(event.reason);
                subscriber.setStyle('backgroundImageURI',
                    //'http://tokbox.com/img/styleguide/tb-colors-cream.png'
                    'http://'+ window.app_server +'static/images/profile_img.png'
                );
            },
            videoEnabled: function(event)
            {
                console.log("Video enabled.");
                console.log(event.reason);
                var imgData = subscriber.getImgData();
                subscriber.setStyle('backgroundImageURI', imgData);
            }
        });
        layout();
    }
};

var ot_error_callback = function(error) {
    console.log("Session Connection Error!");
    console.log(error);
};
