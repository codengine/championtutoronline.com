function fetch_buddy_list_ajax(data,callback,error_back,complete_back){
    $.ajax({
        type: "POST",
        url: "/ajax/fetch_chat_tutors/",
        data: data,
        enctype: 'multipart/form-data',
        success: function(data)
        {
            callback(jQuery.parseJSON(data));
        },
        error: function(jqxhr, status, error)
        {
            error_back(jqxhr, status, error);
        }
    })
    .done(function( msg ) {
        complete_back(msg);
    });
};

$(document).ready(function(){

    $("#id_nav_how_works").click(function(e) {
          e.preventDefault();
          $('html, body').animate({
            scrollTop: $("#id_section_nav_how_works").offset().top
        }, 1000, function() {

        });
    });

    $("#id_nav_finding_tutor").click(function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $("#id_section_finding_tutor").offset().top - 100
        }, 1000, function() {
           $("#id_home_search_tutor").focus();
        });
    });

    $("#id_nav_help").click(function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $("#p1").offset().top
        }, 1000, function() {

        });
    });

    $("form[name=view_tutor]").submit(function(e) {
        e.preventDefault();
        var keyword = $("#id_home_search_tutor").val();
        console.log(keyword);
        if(keyword == "") {
            $("#id_home_search_tutor").focus();
            return false;
        }
        return true;
    });



    var chat_expanded = false;
    var master_container_expanded_width = $("#id_master_container").css("width");
    var menu_scrol_expanded_width = $(".menu_scrol").css("width");
    var hdng_inner_expanded_width = $(".hdng_inner").css("width");
    var outer_tutors_chat_min_height = $(".outer_tutors_chat").css("height");
    var container_expanded_width = $(".container").css("width");

    $(".tutors_chat").click(function(e){
        if(!chat_expanded){

            var chat_list_height = $(window).height() - 100;
            $(".tutors_chat_list").css("height",chat_list_height+"px");

            $(".tutors_chat_detail").show();
            chat_expanded = true;



            var viewportWidth = $(window).width();
            var viewportHeight = $(window).height();

            $("#id_master_container").css("width", (viewportWidth - 260)+"px");
            $(".menu_scrol").css("width", (viewportWidth - 260)+"px");
            $(".hdng_inner").css("width", (viewportWidth - 260)+"px");
            //$(this).css("width", (viewportWidth - 260)+"px");
            $(".outer_tutors_chat").css("height", (viewportHeight - 0 )+"px");
            $(".container").css("width", (viewportWidth - 260)+"px");


        }
        else{
            $(".tutors_chat_detail").hide();
            chat_expanded = false;

            var viewportWidth = $(window).width();
            var viewportHeight = $(window).height();
            $("#id_master_container").css("width", master_container_expanded_width);
            $(".menu_scrol").css("width", menu_scrol_expanded_width);
            $(".hdng_inner").css("width", hdng_inner_expanded_width);
            $(".outer_tutors_chat").css("height", outer_tutors_chat_min_height);
            $(".container").css("width", container_expanded_width);
        }
    });

    fetch_buddy_list_ajax({},function(data){

        var html = "";

        for(var m = 0; m < data.ordered_items.length; m++) {
            var major = data.ordered_items[m];
            //console.log(major);
            //console.log("============");
            html += "<h3>"+ major +" Tutor </h3>";
            //jQuery.each(data.data[major], function()
            for(var i = 0 ; i < data.data[major].users.length ; i++)
            {
                var user = data.data[major].users[i];
                html += "<li id=\"id_row_"+ user.id +"\">";
                html += "<input type=\"hidden\" class=\"tutor_uid\" value=\""+ user.id +"\">";
                html += "<input type=\"hidden\" class=\"online_status\" value=\""+ user.online_status +"\">";
                html += "<input type=\"hidden\" class=\"major_class\" value=\""+ major +"\">";
                html += "<input type=\"hidden\" class=\"tut_name\" value=\""+ user.fullname +"\">";
                if(user.online_status == "online") {
                    html += "<div class=\"user_pp tutors_chat_img\" style=\"height: 50px;\"> ";
                    html += "<img src=\""+ user.img +"\" width=\"31\" height=\"31\" alt=\"img\"> ";
                    html += "</div>";
                }
                else {
                    html += "<div class=\"user_pp tutors_chat_img_offline\" style=\"height: 50px;\"> ";
                    html += "<img src=\""+ user.img +"\" width=\"31\" height=\"31\" alt=\"img\"> ";
                    html += "</div>";

                }
                html += "<div class=\"tutors_chat_txt\">";
                html += "<h3><a target=\"_blank\" href=\"/profile/"+ user.id +"/\">"+ user.fullname +"</a></h3>";
                html += "<p>"+ user.current_school +"</p>";
                html += "<ul><li><img src=\"/static/images/grade_active.png\" width=\"14\" height=\"15\" alt=\"img\"> <span style=\"margin-top: 2px;\">"+ user.positive_rating_count +"</span></li></ul>";
                html += "</div>";
                html += "<div class=\"notification champ_text_chat\"><img src=\"/static/images/chat-icon.png\" width=\"20\" height=\"15\"/></div>";
                html += "</li>";
            }
            html += "<a href=\"#\" class=\"show_more_chat\"><input type=\"hidden\" class=\"major_name\" value=\""+ major +"\"/>Show More</a>";
        }
        $(".tutors_chat_list").html(html);
    },function(jqxhr, status, error){},function(msg){});

    // Speed up calls to hasOwnProperty
    var hasOwnProperty = Object.prototype.hasOwnProperty;

    function isEmpty(obj) {

        // null and undefined are "empty"
        if (obj == null) return true;

        // Assume if it has a length property with a non-zero value
        // that that property is correct.
        if (obj.length > 0)    return false;
        if (obj.length === 0)  return true;

        // Otherwise, does it have any properties of its own?
        // Note that this doesn't handle
        // toString and valueOf enumeration bugs in IE < 9
        for (var key in obj) {
            if (hasOwnProperty.call(obj, key)) return false;
        }

        return true;
    }


    $(document).on("click",".show_more_chat", function(e) {
        e.preventDefault();
        $(this).prop("disabled", true);
        var _this = $(this);
        var major_name = $(this).find(".major_name").val();

        var excludes = "";
        $(".tutors_chat_list li").each(function(i) {
            if($(this).find(".major_class").val() == major_name) {
                excludes += $(this).find(".tutor_uid").val()+",";
            }
        });

        //console.log(excludes);

        fetch_buddy_list_ajax({"major": major_name,"excludes": excludes},function(data){

            var html = "";
            if(isEmpty(data.data)) {
                _this.hide();
            }
            for(var major in data.data) {
                //html += "<h3>"+ major +" Tutor </h3>";
                //jQuery.each(data.data[major], function()
                for(var i = 0 ; i < data.data[major].users.length ; i++)
                {
                    var user = data.data[major].users[i];
                    html += "<li id=\"id_row_"+ user.id +"\">";
                    html += "<input type=\"hidden\" class=\"tutor_uid\" value=\""+ user.id +"\">";
                    html += "<input type=\"hidden\" class=\"online_status\" value=\""+ user.online_status +"\">";
                    html += "<input type=\"hidden\" class=\"major_class\" value=\""+ major +"\">";
                    html += "<input type=\"hidden\" class=\"tut_name\" value=\""+ user.fullname +"\">";
                    if(user.online_status == "online") {
                        html += "<div class=\"user_pp tutors_chat_img\" style=\"height: 50px;\"> ";
                        html += "<img src=\""+ user.img +"\" width=\"31\" height=\"31\" alt=\"img\"> ";
                        html += "</div>";
                    }
                    else {
                        html += "<div class=\"user_pp tutors_chat_img_offline\" style=\"height: 50px;\"> ";
                        html += "<img src=\""+ user.img +"\" width=\"31\" height=\"31\" alt=\"img\"> ";
                        html += "</div>";

                    }
                    html += "<div class=\"tutors_chat_txt\">";
                    html += "<h3><a target=\"_blank\" href=\"/profile/"+ user.id +"/\">"+ user.fullname +"</a></h3>";
                    html += "<p>"+ user.current_school +"</p>";
                    html += "<ul><li><img src=\"/static/images/grade_active.png\" width=\"14\" height=\"15\" alt=\"img\"> <span style=\"margin-top: 2px;\">"+ user.positive_rating_count +"</span></li></ul>";
                    html += "</div>";
                    html += "<div class=\"notification champ_text_chat\"><img src=\"/static/images/chat-icon.png\" width=\"20\" height=\"15\"/></div>";
                    html += "</li>";
                }
            }
            $(html).insertBefore(_this);
        },function(jqxhr, status, error){},function(msg){
            _this.prop("disabled", false);
        });
    });


    var check_user_status = function(){
        var tutors_list = "";
        $(".tutors_chat_list li").each(function(i) {
            var tutor_id = $(this).find(".tutor_uid").val();
            tutors_list += tutor_id+",";
        });
        //console.log(tutors_list.length);
        fetch_buddy_list_ajax({ tutors: tutors_list },function(data){

            var html = "";

            for(var major in data.data) {
                //html += "<h3>"+ major +" Tutor </h3>";
                jQuery.each(data.data[major], function() {
                    for(var i = 0 ; i < data.data[major].users.length ; i++ ) {
                        var user = data.data[major].users[i];
                        var _row = $("#id_row_"+user.id);
                        _row.find(".online_status").val(user.online_status);
                        _row.find(".tut_name").val(user.fullname);
                        if(user.online_status == "offline") {
                            if(user.teaching_status == "ready_to_teach") {
                                _row.find(".user_pp").removeClass("tutors_chat_img");
                                 _row.find(".user_pp").removeClass("tutors_chat_img_offline");
                                _row.find(".user_pp").addClass("tutors_chat_img_teach_available");
                                 $(_row.find(".user_pp").find("img")).prop("src",user.img);
                            }
                            else {
                                _row.find(".user_pp").removeClass("tutors_chat_img");
                                _row.find(".user_pp").removeClass("tutors_chat_img_teach_available");
                                _row.find(".user_pp").addClass("tutors_chat_img_offline");
                                 $(_row.find(".user_pp").find("img")).prop("src",user.img);
                            }
                        }
                        else if(user.online_status == "online") {
                            _row.find(".user_pp").removeClass("tutors_chat_img_offline");
                            _row.find(".user_pp").removeClass("tutors_chat_img_teach_available");
                            _row.find(".user_pp").addClass("tutors_chat_img");
                        }
                    }
                });
            }
            //$(html).insertBefore(_this);
        },function(jqxhr, status, error){},function(msg){
            //_this.prop("disabled", false);
        });
    };

    //setTimeout(check_user_status,1000);
    setInterval(check_user_status, 5000);


});