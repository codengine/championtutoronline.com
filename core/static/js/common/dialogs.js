__author__ = 'codengine'

$(document).ready(function(){
    window.show_colorbox_dialog = function(html,options){
        var default_options =  {
            inline:true,
            opacity: 0.1,
            width:"700px",
            height:"500px",
            scrolling: false,
            photo: true,
            escKey: false,
            fixed: true,
            html: html,
            overlayClose: false,
            onOpen:function(){ },
            onLoad:function(){ },
            onComplete:function(){ },
            onCleanup:function(){ },
            onClosed:function(){ }
        };
        options = $.extend(default_options,options);
        $.colorbox(options);
    }

//    window.show_colorbox_dialog = function(html,options){
//        var default_options =  {
//            inline:true,
//            opacity: 0.1,
//            width:"700px",
//            height:"500px",
//            scrolling: false,
//            photo: true,
//            escKey: false,
//            fixed: true,
//            html: html,
//            overlayClose: false,
//            onOpen:function(){ },
//            onLoad:function(){ },
//            onComplete:function(){ },
//            onCleanup:function(){ },
//            onClosed:function(){ }
//        };
//        options = $.extend(default_options,options);
//        $.colorbox(options);
//    }

});
