/**
 * Created with PyCharm.
 * User: codengine
 * Date: 5/8/15
 * Time: 9:00 AM
 * To change this template use File | Settings | File Templates.
 */

window._ajax_call = function(type,url,data,callback,error_back,complete_back){
    $.ajax({
        type: type,
        url: url,
        data: data,
        enctype: 'multipart/form-data',
        success: function(data)
        {
            callback(jQuery.parseJSON(data));
        },
        error: function(jqxhr, status, error)
        {
            error_back(jqxhr, status, error);
        }
    })
    .done(function( msg ) {
        complete_back(msg);
    });
};


window.show_popup_dialog = function(response_data,$title,$message,$closable,dialog_type,$colorbox_ref,$success_label,$cancel_label,success_callback,cancel_callback,close_colorbox_on_ok_click){

    var dialogtype = BootstrapDialog.TYPE_DEFAULT;
    if(dialog_type == "warning"){
        dialogtype = BootstrapDialog.TYPE_WARNING;
    }
    else if(dialog_type == "info"){
        dialogtype = BootstrapDialog.TYPE_INFO;
    }
    else if(dialog_type == "error"){
        dialogtype = BootstrapDialog.TYPE_DANGER;
    }
    else if(dialog_type == "success"){
        dialogtype = BootstrapDialog.TYPE_PRIMARY;
    }
    else if(dialog_type == "primary"){
        dialogtype = BootstrapDialog.TYPE_PRIMARY;
    }

    BootstrapDialog.show({
        title: $title,
        message: $message,
        closable: $closable,
        type: dialogtype,
        size: BootstrapDialog.SIZE_NORMAL,
        buttons: [
            {
                label: $success_label,
                cssClass: 'btn btn-md btn-primary width120',
                action: function(dialogRef) {

                    //$('body').removeClass("modal-open");

                    success_callback(response_data);

                    dialogRef.close();
                    console.log(close_colorbox_on_ok_click);
                    console.log($colorbox_ref);
                    if(close_colorbox_on_ok_click == true){
                        if($colorbox_ref != undefined || $colorbox_ref != null){
                            $colorbox_ref.close();
                        }
                    }
                }
            },
            {
                label: $cancel_label,
                cssClass: 'btn btn-md btn-warning width120',
                action: function(dialogRef) {
                    //$('body').removeClass("modal-open");
                    cancel_callback(response_data);
                    dialogRef.close();
                }
            }]
    });
}
