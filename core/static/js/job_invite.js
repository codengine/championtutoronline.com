function load_invite_tutor_dialog(data) {
    $("#id_overlay_loading").show();
    $("#black_overlay").hide();
    $.ajax({
        type: "GET",
        url: "/ajax/invite_tutor/",
        data: data,
        enctype: 'multipart/form-data',
        success: function(data)
        {
            var data = jQuery.parseJSON(data);
            if(data.status == "SUCCESS") {
                //console.log(data.data);
                $("#black_overlay").show();
                $("#id_ajax_rendered_content").html(data.data.dialog_content);
                $("#id_ajax_rendered_content").fadeIn('fast');
            }
            else {
                if(data.code == 4001) {
                    show_signin();
                }
            }
        },
        error: function(jqxhr, status, error)
        {

        }
    })
    .done(function( msg ) {
        $("#id_overlay_loading").hide();
        //show_popup();
    });
}

$(document).on("click",".tutor_invite_btn", function(e) {
    e.preventDefault();
    if(window.champ_user_id != undefined && window.champ_user_id != -1) {
        var tutor_id = $(this).parent().find("input[name=tutor_id]").val();
        load_invite_tutor_dialog({ tutor_id: tutor_id });
    }
    else {
        show_signin();
    }
});

$(document).on("click",".recommended_tutor_invite_btn", function(e) {
    e.preventDefault();
    if(window.champ_user_id != undefined && window.champ_user_id != -1) {
        var tutor_id = $(this).data("tutor-id");
        var job_id = $(this).data("job-id");
        load_invite_tutor_dialog({ tutor_id: tutor_id, job_id: job_id });
    }
    else {
        show_signin();
    }
});

$(document).on("click",".job_invite_cancel", function(e) {
    e.preventDefault();
    $("#black_overlay").fadeOut('fast');
    $("#id_ajax_rendered_content").fadeOut('fast');
    $("#id_overlay_loading").hide();
});

$(document).on("click", "#black_overlay", function(e) {
    e.preventDefault();
    $("#black_overlay").fadeOut('fast');
    $("#id_ajax_rendered_invite_content").fadeOut('fast');
    $("#id_overlay_loading").hide();
});

function load_job_summary(job_id) {
    //$("#id_job_summary").fadeOut('fast');
    $.ajax({
        type: "GET",
        url: "/job/details/"+ job_id +"/",
        data: { format: "lt" },
        enctype: 'multipart/form-data',
        success: function(data)
        {
            var data = jQuery.parseJSON(data);
            console.log(data.data);
            if(data.status == "SUCCESS") {
                $("#id_job_summary").html(data.data.job_summary_content);
                $("#id_job_summary").show();
            }
        },
        error: function(jqxhr, status, error)
        {
            $("#id_job_summary").html("<p style='color: red;'>Job details could not be loaded. There was a problem while processing the request.</p>");
            $("#id_job_summary").show();
        }
    })
    .done(function( msg ) {

    });
}

$(document).on("change",".job_invite_job", function(e) {
   e.preventDefault();
   var job_id = $(this).val();
   if(job_id != "-1") {
        load_job_summary(job_id);
   }
   else {
        $("#id_job_summary").hide();
   }
});

function load_job_invite_email_template(user_id,job_id) {
    //$("#id_job_summary").fadeOut('fast');
    $.ajax({
        type: "GET",
        url: "/ajax/get/email/",
        data: { context: "JOB_INVITATION", rid: user_id, job_id: job_id },
        enctype: 'multipart/form-data',
        success: function(data)
        {
            var data = jQuery.parseJSON(data);
            if(data.status == "SUCCESS") {
                $("#id_invite_lesson_note").html(data.data.email_template);
            }
        },
        error: function(jqxhr, status, error)
        {

        }
    })
    .done(function( msg ) {

    });
}

//$(document).on("change",".invite_tutor", function(e) {
//   e.preventDefault();
//   var user_id = $(this).val();
//   var job_id = $(this).data("job-id");
//   if(user_id != "-1") {
//        load_job_invite_email_template(user_id,job_id);
//   }
//   else {
//        $("#id_invite_lesson_note").html("");
//   }
//});

$(document).on("click","#id_invite_tutor", function(e) {
    e.preventDefault();
    load_invite_tutor_dialog({});
});

$(document).on("click",".invite_tutors", function(e) {
    e.preventDefault();
    var job_id = $(this).data("job-id");
    load_invite_tutor_dialog({ job_id: job_id });
});

function validate_invite_form() {
    var valid = true;
    var tutor_id = $("select[name=invite_tutor]").val();
    if(tutor_id == undefined || tutor_id == "-1") {
        $("select[name=invite_tutor]").parent().find(".invite_error").html("This field is required.");
        $("select[name=invite_tutor]").parent().find(".invite_error").show();
        valid &= false;
    }
    var job_id = $("select[name=job_list]").val();
    if(job_id == undefined || job_id == "-1") {
        $("select[name=job_list]").parent().find(".invite_error").html("This field is required.");
        $("select[name=job_list]").parent().find(".invite_error").show();
        valid &= false;
    }
    var lesson_note = $("#id_invite_lesson_note").val();
    if(lesson_note == undefined || $.trim(lesson_note) == "") {
        $("#id_invite_lesson_note").parent().find(".invite_error").html("This field is required.");
        $("#id_invite_lesson_note").parent().find(".invite_error").show();
        valid &= false;
    }
    return valid;
}

$(document).on("click",".job_invite_submit", function(e) {
    e.preventDefault();
    var $this = $(this);
    $this.prop("disabled",true);
    if(validate_invite_form()) {

        var request_data = $("#id_invite_tutor_dialog_form").serialize();
        $.ajax({
            type: "POST",
            url: "/ajax/invite_user/",
            data: request_data,
            enctype: 'multipart/form-data',
            success: function(data)
            {
                var data = jQuery.parseJSON(data);
                if(data.status == "SUCCESS") {
                    $("#id_post_dialog").show();
                    $("#id_job_invite_form_content").hide();

                    var container_id = "id_container_invited";
                    var item_count_container = "id_invited_count";

                    window.update_job_tutor_list(container_id, item_count_container, "invited");

                    $(".job-status-list").find("li.invited").click();

                }
                else {
                    BootstrapDialog.show({
                        title: "Invitation failed",
                        message: data.data.reason,
                        closable: true,
                        type: BootstrapDialog.TYPE_DANGER,
                        size: BootstrapDialog.SIZE_NORMAL,
                        buttons: [
                            {
                                label: "Ok",
                                cssClass: 'btn btn-md btn-primary width120',
                                action: function(dialogRef) {
                                    $this.prop("disabled",false);
                                    dialogRef.close();
                                }
                            },
                            {
                                label: "Close",
                                cssClass: 'btn btn-md btn-warning width120',
                                action: function(dialogRef) {
                                    $this.prop("disabled",false);
                                    dialogRef.close();
                                }
                            }]
                    });
                }
            },
            error: function(jqxhr, status, error)
            {
                BootstrapDialog.show({
                    title: "Invitation failed",
                    message: "Invitation couldn't be sent.",
                    closable: true,
                    type: BootstrapDialog.TYPE_DANGER,
                    size: BootstrapDialog.SIZE_NORMAL,
                    buttons: [
                        {
                            label: "Ok",
                            cssClass: 'btn btn-md btn-primary width120',
                            action: function(dialogRef) {
                                $this.prop("disabled",false);
                                dialogRef.close();
                            }
                        },
                        {
                            label: "Close",
                            cssClass: 'btn btn-md btn-warning width120',
                            action: function(dialogRef) {
                                $this.prop("disabled",false);
                                dialogRef.close();
                            }
                        }]
                });
            }
        })
        .done(function( msg ) {
            $this.prop("disabled",false);
        });
    }
});

$(document).on("click",".feed_job_delete", function(e) {
    e.preventDefault();
    var $this = $(this);
    BootstrapDialog.show({
        size: BootstrapDialog.SIZE_NORMAL,
        message: 'Are you sure you want to delete this job?',
        buttons: [{
            label: 'Confirm',
            cssClass: 'btn-danger',
            action: function(){
                location.href = $this.data("url");
            }
        }, {
            label: 'Close',
            action: function(dialogItself){
                dialogItself.close();
            }
        }]
    });
});