function show_offer_reject_dialog(job_id, user_id, offer_id) {
    $("#id_overlay_loading").show();
    $("#black_overlay").fadeOut('fast');
    $("#id_ajax_rendered_content").fadeOut('fast');
    $.ajax({
        type: "GET",
        url: "/ajax/reject_job_offer/",
        data: { job_id: job_id, user_id: user_id, offer_id: offer_id },
        enctype: 'multipart/form-data',
        success: function(data)
        {
            var data = jQuery.parseJSON(data);
            //console.log(data.data);
            $("#black_overlay").fadeIn('fast');
            $("#id_ajax_rendered_content").html(data.data);
            $("#id_ajax_rendered_content").show();
        },
        error: function(jqxhr, status, error)
        {

        }
    })
    .done(function( msg ) {
        $("#id_overlay_loading").hide();
        //show_popup();
    });
}

$(document).ready(function() {
    $(document).on("click",".job_offer_accept", function(e) {
        e.preventDefault();
        $("form[name=job_offer_accept_form]").submit();
    });
    $(document).on("click",".job_offer_reject", function(e) {
        e.preventDefault();
        var job_id = $(this).parent().parent().parent().find(".job_id").val();
        var user_id = $(this).parent().parent().parent().find(".user_id").val();
        var offer_id = $(this).parent().parent().parent().find(".offer_id").val();
        show_offer_reject_dialog(job_id, user_id, offer_id);
    });
    $(document).on("click",".job_offer_reject_cancel", function(e) {
        e.preventDefault();
        $("#black_overlay").fadeOut('fast');
        $("#id_ajax_rendered_content").fadeOut('fast');
        $("#id_overlay_loading").hide();
    });
    $(document).on("click","#black_overlay", function(e) {
        $("#black_overlay").fadeOut('fast');
        $("#id_ajax_rendered_content").fadeOut('fast');
    });
});