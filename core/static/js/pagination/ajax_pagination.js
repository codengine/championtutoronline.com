function pagination_ajax_search(url, type, data, callback, error_back, complete_back) {
    $.ajax({
            url: url,
            data: data,
            success: function(data) {
                data = jQuery.parseJSON(data);
                callback(data);
            },
            error: function(jqxhr, status, error)
            {
                error_back(jqxhr, status, error);
            }
        }).done(function( msg ) {
            complete_back(msg);
        });
}

function update_history() {
    var hu = $("#id_ajax_search_pagination").data("update-history");
    var url = $("#id_ajax_search_pagination").data("url");
    var current_page = $("#id_ajax_search_pagination").data("current-page");
    if(hu == "1") {
        History.replaceState({}, null, "?"+url+"?page="+current_page);
    }
}

function handle_pagination_ajax_search(next_page, tab, container_id) {
    var meta_data = {};

    $.each($("#id_ajax_search_pagination").data(), function(key,value){
        if(key.startsWith("meta")) {
            meta_data[key.replace("meta", "").toLowerCase()] = value;
        }
    });

    var data = {
        "page": next_page
    };

    var data = $.extend({}, data, meta_data);

    var url = $("#id_ajax_search_pagination").data("url");
    var type = "GET";

    load_tutor_section_contents(data,function(data) {
        $("#"+container_id).html(data.data.content);
        content_cache[container_id] = true;
        History.replaceState({}, null, "?tab="+ tab +"&page="+next_page);
    },
    function(jqxhr, status, error) {

    },
    function(msg) {

    });

//    pagination_ajax_search(url, type, data, function(data) {
//
//        if(data.status == "SUCCESS") {
//            update_history();
//        }
//    },
//    function(jqxhr, status, error) {
//
//    },
//    function(msg) {
//
//    });

}

$(document).ready(function() {
    $(".ajax_pagination_list_item").click(function(e) {
        e.preventDefault();

        var next_page = $(this).data("page");

        handle_pagination_ajax_search(next_page, $("#id_ajax_search_pagination").data("tab"), $("#id_ajax_search_pagination").data("container"));

    });
});