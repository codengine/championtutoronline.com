function disable_audio() {
    $("#myonoffswitch4").prop("disabled", true);
    $("#myonoffswitch4").parent().parent().css("opacity", "0.3");
}

function disable_video() {
    $("#myonoffswitch3").prop("disabled", true);
    $("#myonoffswitch3").parent().parent().css("opacity", "0.3");
}

function disable_screensharing() {
    $("#myonoffswitch6").prop("disabled", true);
    $("#myonoffswitch6").parent().parent().css("opacity", "0.3");
    window.screensharing = false;
}

function enable_audio() {
    $("#myonoffswitch4").prop("disabled", false);
    $("#myonoffswitch4").parent().parent().css("opacity", "1.0");
}

function enable_video() {
    $("#myonoffswitch3").prop("disabled", false);
    $("#myonoffswitch3").parent().parent().css("opacity", "1.0");
}

function enable_screensharing() {
    $("#myonoffswitch6").prop("disabled", false);
    $("#myonoffswitch6").parent().parent().css("opacity", "1.0");
    window.screensharing = true;
}

function disable_toolbar_bottom() {
    disable_audio();
    disable_video();
    disable_screensharing();
}

function disable_whiteboard_menu() {
    $(".canvas_menu").find(".canvas_menu_inner").each(function(i) {
        $(this).prop("disabled", true);
        $(this).css("opacity", "0.3");
    });
}

function disable_drawing() {
    window.drawing_enabled = false;
}

function enable_drawing() {
    window.drawing_enabled = true;
}

function disable_wb_chat() {
    $(".wb-chat_txt").hide();
    $(".wb-chat").css("top","-330px");
}

function enable_wb_chat() {
    $(".wb-chat_txt").show();
    $(".wb-chat").css("top","-350px");
}

function disable_audio_video_sharing() {
    $("#id_use_audio_button").prop("disabled", true);
    $("#id_use_audio_button").css("opacity", "0.3");
    $("#id_use_video_button").prop("disabled", true);
    $("#id_use_video_button").css("opacity", "0.3");
}

function enable_audio_video_sharing() {
    $("#id_use_audio_button").prop("disabled", false);
    $("#id_use_audio_button").css("opacity", "1.0");
    $("#id_use_video_button").prop("disabled", false);
    $("#id_use_video_button").css("opacity", "1.0");
}

function disable_whiteboard() {
    disable_toolbar_bottom();
    disable_whiteboard_menu();
    disable_wb_chat();
    disable_audio_video_sharing();
}

function enable_toolbar_bottom() {
    enable_audio();
    enable_video();
    enable_screensharing();
}

function enable_whiteboard() {
    enable_toolbar_bottom();
}

$(document).ready(function() {
    //alert("Hi!");
    //disable_whiteboard();
});