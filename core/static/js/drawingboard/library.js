function euclidian_distance(start_point,end_point)
{
    return Math.sqrt(((start_point.x-end_point.x)*(start_point.x-end_point.x)) + ((start_point.y-end_point.y)*(start_point.y-end_point.y)));
}

window.uuid_generator = function() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
    });
}

var drawGrid = function (cellSize) {

    this.cellSize = cellSize;
    this.gridColor = '#D0D0D0';

    var self = this;
    this.gridGroup;

    var boundingRect = view.bounds;
    var rectanglesX = view.bounds.width / this.cellSize;
    var rectanglesY = view.bounds.height / this.cellSize;

    this.createGrid = function() {

        var background_layer = new Layer();
        background_layer.name = "background_layer";

        gridGroup = new paper_scope.Group();
        gridGroup.name = "background_grid"

        //Vertical Lines
        for (var i = 0; i <= rectanglesX; i++) {
            var correctedLeftBounds = Math.ceil(boundingRect.left / self.cellSize) * self.cellSize;
            var xPos = correctedLeftBounds + i * self.cellSize;
            var topPoint = new Point(xPos, boundingRect.top);
            var bottomPoint = new Point(xPos, boundingRect.bottom);
            var gridLine = new Path.Line(topPoint, bottomPoint);
            gridLine.name = "grid_unit"
            gridLine.strokeColor = self.gridColor;
            gridLine.strokeWidth = 1 / view.zoom;

            self.gridGroup.addChild(gridLine);

        }

        //Horizontal Lines
        for (var i = 0; i <= rectanglesY; i++) {
            var correctedTopBounds = Math.ceil(boundingRect.top / self.cellSize) * self.cellSize;
            var yPos = correctedTopBounds + i * self.cellSize;
            var leftPoint = new Point(boundingRect.left, yPos);
            var rightPoint = new Point(boundingRect.right, yPos);
            var gridLine = new Path.Line(leftPoint, rightPoint);
            gridLine.name = "grid_unit"
            gridLine.strokeColor = self.gridColor;
            gridLine.strokeWidth = 1 / view.zoom;

            self.gridGroup.addChild(gridLine);
        }

        gridGroup.sendToBack();

        background_layer.addChild(gridGroup);
        background_layer.sendToBack();

        view.update();
    }

    //Removes the children of the gridGroup and discards the gridGroup itself
    this.removeGrid = function() {
        for (var i = 0; i<= gridGroup.children.length-1; i++) {
          gridGroup.children[i].remove();
        }
        gridGroup.remove();
    }

    //Initialization
    if(typeof gridGroup === 'undefined') {
        this.createGrid();
    } else {
        //this.removeGrid();
        this.createGrid();
    }
    if(drawing_board_background[active_board_id]) {
        drawing_board_background[active_board_id].remove();
    }
    drawing_board_background[active_board_id] = this.gridGroup;
    return this.gridGroup;
}

function initSelectionRectangle(path) {
    if(selectionRectangle!=null)
        selectionRectangle.remove();
    var reset =  true; //path.rotation==0 && path.scaling.x==1 && path.scaling.y==1;
    var bounds;
    console.log(reset);
    if(reset)
    {
        console.log('reset');
        bounds = path.bounds;
        path.pInitialBounds = path.bounds;
    }
    else
    {
        console.log('no reset');
        bounds = path.bounds;
    }
    console.log('bounds: ' + bounds);
    b = bounds.clone().expand(10,10);

    selectionRectangle = new Path.Rectangle(b);
    selectionRectangle.pivot = selectionRectangle.position;
    selectionRectangle.insert(2, new Point(b.center.x, b.top));
    selectionRectangle.insert(2, new Point(b.center.x, b.top-25));
    selectionRectangle.insert(2, new Point(b.center.x, b.top));
    if(!reset)
    {
        selectionRectangle.position = path.bounds.center;
        selectionRectangle.rotation = path.rotation;
        selectionRectangle.scaling = path.scaling;
    }

    selectionRectangle.strokeWidth = 1;
    selectionRectangle.strokeColor = 'blue';
    selectionRectangle.name = "selection rectangle";
    selectionRectangle.selected = true;
    selectionRectangle.ppath = path;
    selectionRectangle.ppath.pivot = selectionRectangle.pivot;
}

function drawArrowLine(sx, sy, ex, ey, isDouble) {
    function calcArrow(px0, py0, px, py) {
        var points = [];
        var l = Math.sqrt(Math.pow((px - px0), 2) + Math.pow((py - py0), 2));
        points[0] = (px - ((px - px0) * Math.cos(0.5) - (py - py0) * Math.sin(0.5)) * 10 / l);
        points[1] = (py - ((py - py0) * Math.cos(0.5) + (px - px0) * Math.sin(0.5)) * 10 / l);
        points[2] = (px - ((px - px0) * Math.cos(0.5) + (py - py0) * Math.sin(0.5)) * 10 / l);
        points[3] = (py - ((py - py0) * Math.cos(0.5) - (px - px0) * Math.sin(0.5)) * 10 / l);
        return points;
    }

    var endPoints = calcArrow(sx, sy, ex, ey);
    var startPoints = calcArrow(ex, ey, sx, sy);

    var e0 = endPoints[0],
        e1 = endPoints[1],
        e2 = endPoints[2],
        e3 = endPoints[3],
        s0 = startPoints[0],
        s1 = startPoints[1],
        s2 = startPoints[2],
        s3 = startPoints[3];
    var line = new paper_scope.Path({
        name: "grid_unit",
        segments: [
            new paper_scope.Point(sx, sy),
            new paper_scope.Point(ex, ey)
        ],
        strokeWidth: 1
    });
    var arrow1 = new paper_scope.Path({
        name: "grid_unit",
        segments: [
            new paper_scope.Point(e0, e1),
            new paper_scope.Point(ex, ey),
            new paper_scope.Point(e2, e3)
        ]
    });

    var compoundPath = new paper_scope.CompoundPath([line, arrow1]);

    if (isDouble === true) {
        var arrow2 = new paper_scope.Path({
            name: "grid_unit",
            segments: [
                new paper_scope.Point(s0, s1),
                new paper_scope.Point(sx, sy),
                new paper_scope.Point(s2, s3)
            ]
        });

        compoundPath.addChild(arrow2);
        //compoundPath.selected = true;
    }

    return compoundPath;
}

function drawRightTriangle(sx,sy,dx,dy) {
    var p1 = [sx,sy];
    var p3 = [dx,dy];
    var p2 = [sx,dy];
    var from = new Point(sx,sy);
    var to = new Point(dx,dy);
    var line1 = new paper_scope.Path.Line(from,to);
    line1.name = "grid_unit";
    from = new Point(dx,dy);
    to = new Point(sx,dy);
    var line2 = new paper_scope.Path.Line(from,to);
    line2.name = "grid_unit";
    from = new Point(sx,dy);
    to = new Point(sx,sy);
    var line3 = new paper_scope.Path.Line(from,to);
    line3.name = "grid_unit";

    return new paper_scope.CompoundPath({
        children: [
            line1,line2,line3
        ]
    });

}

function draw_cartesian(sx,sy,cx,cy,left,marked,is_background) {

        //        /*
        //         * p1--p12--p2
        //         * |        |
        //         * p14     p23
        //         * |        |
        //         * p4--p43--p3
        //         * */

        var p1 = [],p2 = [],p3 = [],p4 = [];

        if(sx < cx && sy < cy)
        {
            p1 = new Point(sx, sy);
            p2 = new Point(cx, sy);
            p3 = new Point(cx, cy);
            p4 = new Point(sx, cy);
        }
        else if(sx > cx && sy < cy)
        {
            p1 = new Point(cx, sy);
            p2 = new Point(sx, sy);
            p3 = new Point(sx, cy);
            p4 = new Point(cx, cy);
        }
        else if(sx > cx && sy > cy)
        {
            p1 = new Point(cx, cy);
            p2 = new Point(sx, cy);
            p3 = new Point(sx, sy);
            p4 = new Point(cx, sy);
        }
        else if(sx < cx && sy > cy)
        {
            p1 = new Point(sx, cy);
            p2 = new Point(cx, cy);
            p3 = new Point(cx, sy);
            p4 = new Point(sx, sy);
        }

        var p12 = new Point((p1.x + (p2.x - p1.x)/2),p1.y);
        var p23 = new Point(p2.x, p2.y + (p3.y - p2.y)/2);
        var p43 = new Point(p4.x + (p3.x - p4.x)/2, p4.y);
        var p14 = new Point(p1.x, p1.y + (p4.y - p1.y)/2);

        var p23_3 = new Point(p23.x,p23.y+(p3.y - p23.y)/2);
        var p14_4 = new Point(p14.x,p14.y+(p4.y - p14.y)/2);

        var p43_3 = new Point(p43.x+(p3.x - p43.x)/2 , p43.y);
        var p12_2 = new Point(p12.x+(p2.x - p12.x)/2 , p12.y);

        var p4_43 = new Point(p4.x+(p43.x - p4.x)/2 , p4.y);
        var p1_12 = new Point(p1.x+(p12.x - p1.x)/2 , p1.y);


        this.gridGroup;

        this.createGrid = function(lines,send_to_back) {

            if(!send_to_back) {
                this.gridGroup = new CompoundPath();
            }
            else {
                this.gridGroup = new Group();
            }

            if(send_to_back) {
                this.gridGroup.name = "background_grid";
            }
            else {
                if(left && !marked) {
                    this.gridGroup.name = "cartesian_axis_no_arrow";
                }
                else if(left && marked){
                    this.gridGroup.name = "cartesian_axis_arrow";
                }
            }

            //Vertical Lines
            for (var i = 0; i <= lines.length; i++) {
                this.gridGroup.addChild(lines[i]);

            }
            if(send_to_back) {
                this.gridGroup.sendToBack();
            }

            //view.update();
            return this.gridGroup;
        }

        //Removes the children of the gridGroup and discards the gridGroup itself
//        this.remove = function() {
//            for (var i = 0; i<= gridGroup.children.length-1; i++) {
//              gridGroup.children[i].remove();
//            }
//            gridGroup.remove();
//        }

        if(is_background) {
            var line1 = new paper_scope.Path({
                name: "grid_unit",
                segments: [
                    p12,p43
                ],
                strokeWidth: 2,
                strokeColor: "#000000"
            });
            var line2 = new paper_scope.Path({
                name: "grid_unit",
                segments: [
                    p14,p23
                ],
                strokeWidth: 2,
                strokeColor: "#000000"
            });

            var mark_lines = [];

            for(var j = p12.y; j <= ((p43.y - p12.y)/2) - 40; j+=40 ) {
                var mline = new paper_scope.Path({
                    name: "grid_unit",
                    segments: [
                        new Point(p12.x,j),new Point(p12.x + 10,j)
                    ],
                    strokeWidth: 1,
                    strokeColor: "#000000"
                });
                mark_lines.push(mline)
            }

            for(var j = ((p43.y - p12.y)/2) + 40; j < p43.y; j+=40 ) {
                var mline = new paper_scope.Path({
                    name: "grid_unit",
                    segments: [
                        new Point(p12.x,j),new Point(p12.x + 10,j)
                    ],
                    strokeWidth: 1,
                    strokeColor: "#000000"
                });
                mark_lines.push(mline)
            }

            for(var j = p14.x; j < ((p23.x-p14.x)/2) - 40; j+=40 ) {
                var mline = new paper_scope.Path({
                    name: "grid_unit",
                    segments: [
                        new Point(j,p14.y),new Point(j,p14.y - 10)
                    ],
                    strokeWidth: 1,
                    strokeColor: "#000000"
                });
                mark_lines.push(mline)
            }

            for(var j = ((p23.x-p14.x)/2) + 40; j < p23.x; j+=40 ) {
                var mline = new paper_scope.Path({
                    name: "grid_unit",
                    segments: [
                        new Point(j,p14.y),new Point(j,p14.y - 10)
                    ],
                    strokeWidth: 1,
                    strokeColor: "#000000"
                });
                mark_lines.push(mline)
            }

            mark_lines.push(line1);
            mark_lines.push(line2);

            var grid = this.createGrid(mark_lines,true);

            view.update();

            if(drawing_board_background[active_board_id]) {
                drawing_board_background[active_board_id].remove();
            }
            drawing_board_background[active_board_id] = this.gridGroup;
            return grid;
        }
        else {
                var line1 = new paper_scope.Path({
                name: "grid_unit",
                segments: [
                    p4_43,p1_12
                ],
                strokeWidth: 2,
                strokeColor: "#000000"
                });
                var line2 = new paper_scope.Path({
                    name: "grid_unit",
                    segments: [
                        p14_4,p23_3
                    ],
                    strokeWidth: 2,
                    strokeColor: "#000000"
                });

                var mark_lines = [];

                if(marked) {

                    //        /*
                    //         * p1--p12--p2
                    //         * |        |
                    //         * p14     p23
                    //         * |        |
                    //         * p4--p43--p3
                    //         * */

                    //Draw vertical mark
                    for(var j = p1_12.y; j <= p14_4.y - 8; j+=8 ) {
                        var mline = new paper_scope.Path({
                            name: "grid_unit",
                            segments: [
                                new Point(p1_12.x - 5,j),new Point(p1_12.x + 5,j)
                            ],
                            strokeWidth: 1,
                            strokeColor: "#000000"
                        });
                        mark_lines.push(mline)
                    }

                    for(var j = p14_4.y + 8; j <= p43.y; j+=8 ) {
                        var mline = new paper_scope.Path({
                            name: "grid_unit",
                            segments: [
                                new Point(p1_12.x - 5,j),new Point(p1_12.x + 5,j)
                            ],
                            strokeWidth: 1,
                            strokeColor: "#000000"
                        });
                        mark_lines.push(mline)
                    }

                    //Draw horizontal mark.
                    for(var j = p14_4.x; j <= p4_43.x - 8; j+=8 ) {
                        var mline = new paper_scope.Path({
                            name: "grid_unit",
                            segments: [
                                new Point(j,p14_4.y + 5),new Point(j,p14_4.y - 5)
                            ],
                            strokeWidth: 1,
                            strokeColor: "#000000"
                        });
                        mark_lines.push(mline)
                    }

                    for(var j = p4_43.x + 8; j < p23.x; j+=8 ) {
                        var mline = new paper_scope.Path({
                            name: "grid_unit",
                            segments: [
                                new Point(j,p14_4.y + 5),new Point(j,p14_4.y - 5)
                            ],
                            strokeWidth: 1,
                            strokeColor: "#000000"
                        });
                        mark_lines.push(mline)
                    }
                }

                mark_lines.push(line1);
                mark_lines.push(line2);

                var grid = this.createGrid(mark_lines,false);
                return grid;
            }

}

function draw_wordwrap(txt,max){
    var lines=[];
    var space=-1;
    times=0;
    function cut(){
        for(var i=0;i<txt.length;i++){
            (txt[i]==' ')&&(space=i);
            if(i>=max){
                (space==-1||txt[i]==' ')&&(space=i);
                if(space>0){lines.push(txt.slice((txt[0]==' '?1:0),space));}
                txt=txt.slice(txt[0]==' '?(space+1):space);
                space=-1;
                break;
                }}check();}
    function check(){if(txt.length<=max){lines.push(txt[0]==' '?txt.slice(1):txt);txt='';}else if(txt.length){cut();}return;}
    check();
    return this.content=lines.join('\n');
    }