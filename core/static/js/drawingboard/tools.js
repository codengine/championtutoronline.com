function attachEventHandler(tool){
    tool.onMouseDown = onMouseDown;

    tool.onMouseDrag = onMouseDrag;

    tool.onMouseMove = onMouseMove;

    tool.onMouseUp = onMouseUp;

    tool.onKeyDown = onKeyDown;

    tool.onKeyUp = onKeyUp;
}

drawing_tools = {};


//Pencil tool.

var pencil_tool_size_5 = new Tool();
pencil_tool_size_5.name = "pencil_tool_size_5";

var pencil_tool_size_10 = new Tool();
pencil_tool_size_10.name = "pencil_tool_size_10";

var pencil_tool_size_15 = new Tool();
pencil_tool_size_15.name = "pencil_tool_size_15";

var pencil_tool_size_20 = new Tool();
pencil_tool_size_20.name = "pencil_tool_size_20";

attachEventHandler(pencil_tool_size_5);
attachEventHandler(pencil_tool_size_10);
attachEventHandler(pencil_tool_size_15);
attachEventHandler(pencil_tool_size_20);

drawing_tools.pencil_tool_size_5 = pencil_tool_size_5;
drawing_tools.pencil_tool_size_10 = pencil_tool_size_10;
drawing_tools.pencil_tool_size_15 = pencil_tool_size_15;
drawing_tools.pencil_tool_size_20 = pencil_tool_size_20;

//Eraser tool.
var eraser_tool_size_5 = new Tool();
eraser_tool_size_5.name = "eraser_tool_size_5";

var eraser_tool_size_10 = new Tool();
eraser_tool_size_10.name = "eraser_tool_size_10";

var eraser_tool_size_15 = new Tool();
eraser_tool_size_15.name = "eraser_tool_size_15";

var eraser_tool_size_20 = new Tool();
eraser_tool_size_20.name = "eraser_tool_size_20";

attachEventHandler(eraser_tool_size_5);
attachEventHandler(eraser_tool_size_10);
attachEventHandler(eraser_tool_size_15);
attachEventHandler(eraser_tool_size_20);

drawing_tools.eraser_tool_size_5 = eraser_tool_size_5;
drawing_tools.eraser_tool_size_10 = eraser_tool_size_10;
drawing_tools.eraser_tool_size_15 = eraser_tool_size_15;
drawing_tools.eraser_tool_size_20 = eraser_tool_size_20;

//Brush tool.
var brush_tool_size_5 = new Tool();
brush_tool_size_5.name = "brush_tool_size_5";

var brush_tool_size_10 = new Tool();
brush_tool_size_10.name = "brush_tool_size_10";

var brush_tool_size_15 = new Tool();
brush_tool_size_15.name = "brush_tool_size_15";

var brush_tool_size_20 = new Tool();
brush_tool_size_20.name = "brush_tool_size_20";

attachEventHandler(brush_tool_size_5);
attachEventHandler(brush_tool_size_10);
attachEventHandler(brush_tool_size_15);
attachEventHandler(brush_tool_size_20);

drawing_tools.brush_tool_size_5 = brush_tool_size_5;
drawing_tools.brush_tool_size_10 = brush_tool_size_10;
drawing_tools.brush_tool_size_15 = brush_tool_size_15;
drawing_tools.brush_tool_size_20 = brush_tool_size_20;

//Shape circle.
var drawing_tool_circle_5 = new Tool();
drawing_tool_circle_5.name = "drawing_tool_circle_5";

attachEventHandler(drawing_tool_circle_5);
drawing_tools.drawing_tool_circle_5 = drawing_tool_circle_5;

//Shape rectangle.
var drawing_tool_rectangle_5 = new Tool();
drawing_tool_rectangle_5.name = "drawing_tool_rectangle_5";

attachEventHandler(drawing_tool_rectangle_5);
drawing_tools.drawing_tool_rectangle_5 = drawing_tool_rectangle_5;

//Shape line.
var drawing_tool_line_5 = new Tool();
drawing_tool_line_5.name = "drawing_tool_line_5";

attachEventHandler(drawing_tool_line_5);
drawing_tools.drawing_tool_line_5 = drawing_tool_line_5;

//Shape poly_6_size.
var drawing_tool_poly_6_size_5 = new Tool();
drawing_tool_poly_6_size_5.name = "drawing_tool_poly_6_size_5";

attachEventHandler(drawing_tool_poly_6_size_5);
drawing_tools.drawing_tool_poly_6_size_5 = drawing_tool_poly_6_size_5;

//Shape poly_5_size.
var drawing_tool_poly_5_size_5 = new Tool();
drawing_tool_poly_5_size_5.name = "drawing_tool_poly_5_size_5";

attachEventHandler(drawing_tool_poly_5_size_5);
drawing_tools.drawing_tool_poly_5_size_5 = drawing_tool_poly_5_size_5;

//Shape triangle.
var drawing_tool_triangle_5 = new Tool();
drawing_tool_triangle_5.name = "drawing_tool_triangle_5";

attachEventHandler(drawing_tool_triangle_5);
drawing_tools.drawing_tool_triangle_5 = drawing_tool_triangle_5;

//Shape selection.
var drawing_tool_selection = new Tool();
drawing_tool_selection.name = "drawing_tool_selection";

attachEventHandler(drawing_tool_selection);
drawing_tools.drawing_tool_selection = drawing_tool_selection;

//Shape arrow single.
var drawing_tool_arrow_single = new Tool();
drawing_tool_arrow_single.name = "drawing_tool_arrow_single";

attachEventHandler(drawing_tool_arrow_single);
drawing_tools.drawing_tool_arrow_single = drawing_tool_arrow_single;

//Shape arrow double.
var drawing_tool_arrow_double = new Tool();
drawing_tool_arrow_double.name = "drawing_tool_arrow_double";

attachEventHandler(drawing_tool_arrow_double);
drawing_tools.drawing_tool_arrow_double = drawing_tool_arrow_double;

//Shape right triangle
var drawing_tool_right_triangle = new Tool();
drawing_tool_right_triangle.name = "drawing_tool_right_triangle";

attachEventHandler(drawing_tool_right_triangle);
drawing_tools.drawing_tool_right_triangle = drawing_tool_right_triangle;

//Shape right cartesian
var drawing_tool_cartesian = new Tool();
drawing_tool_cartesian.name = "drawing_tool_cartesian";

attachEventHandler(drawing_tool_cartesian);
drawing_tools.drawing_tool_cartesian = drawing_tool_cartesian;

//Shape right cartesian marked
var drawing_tool_cartesian_marked = new Tool();
drawing_tool_cartesian_marked.name = "drawing_tool_cartesian_marked";

attachEventHandler(drawing_tool_cartesian_marked);
drawing_tools.drawing_tool_cartesian_marked = drawing_tool_cartesian_marked;

//Shape text
var drawing_tool_text = new Tool();
drawing_tool_text.name = "drawing_tool_text";

attachEventHandler(drawing_tool_text);
drawing_tools.drawing_tool_text = drawing_tool_text;

window.drawing_tools = drawing_tools