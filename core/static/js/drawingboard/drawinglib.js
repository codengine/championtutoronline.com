
//item = {
//    tool_name: '',
//    data: [],
//    stroke_color: '',
//    stroke_size: ''
//}

function draw_offline_items(items) {

    if(items == undefined) {
        return;
    }
    //console.log(items);
    //console.log(items);
    for(var k = 0 ; k < items.length ; k++) {
        var tool_name = items[k].tool_name;
        if(tool_name == drawing_tools.pencil_tool_size_5.name) {
            //console.log(items[k].data);
            var path_ = new paper_scope.Path();
            path_._id = items[k]._id;
            path_.strokeWidth = items[k].stroke_size;
            path_.strokeColor = items[k].stroke_color;
            for(var m = 0 ; m < items[k].data.length ; m++) {
                var _t = items[k].data[m];
                console.log(_t);
                path_.add(new paper_scope.Point(_t.x,_t.y));
            }
            path_ = null;
            view.draw();
        }
        else if(tool_name == drawing_tools.pencil_tool_size_10.name) {
            var path_ = new Path();
            path_._id = items[k]._id;
            path_.strokeWidth = items[k].stroke_size;
            path_.strokeColor = items[k].stroke_color;
            for(var m = 0 ; m < items[k].data.length ; m++) {
                var _t = items[k].data[m];
                path_.add(new Point(_t.x,_t.y));
            }
            path_ = null;
        }
        else if(tool_name == drawing_tools.pencil_tool_size_15.name) {
            var path_ = new Path();
            path_._id = items[k]._id;
            path_.strokeWidth = items[k].stroke_size;
            path_.strokeColor = items[k].stroke_color;
            for(var m = 0 ; m < items[k].data.length ; m++) {
                var _t = items[k].data[m];
                path_.add(new Point(_t.x,_t.y));
            }
            path_ = null;
        }
        else if(tool_name == drawing_tools.pencil_tool_size_20.name) {
            var path_ = new Path();
            path_._id = items[k]._id;
            path_.strokeWidth = items[k].stroke_size;
            path_.strokeColor = items[k].stroke_color;
            for(var m = 0 ; m < items[k].data.length ; m++) {
                var _t = items[k].data[m];
                path_.add(new Point(_t.x,_t.y));
            }
            path_ = null;
        }
        //Draw Brush
        else if(tool_name == drawing_tools.brush_tool_size_5.name) {
            //console.log(items[k].data);
            var path_ = new paper_scope.Path();
            path_._id = items[k]._id;
            path_.strokeWidth = items[k].stroke_size;
            path_.strokeColor = items[k].stroke_color;
            for(var m = 0 ; m < items[k].data.length ; m++) {
                var _t = items[k].data[m];
                console.log(_t);
                path_.add(new paper_scope.Point(_t.x,_t.y));
            }
            path_ = null;
            view.draw();
        }
        else if(tool_name == drawing_tools.brush_tool_size_10.name) {
            var path_ = new Path();
            path_._id = items[k]._id;
            path_.strokeWidth = items[k].stroke_size;
            path_.strokeColor = items[k].stroke_color;
            for(var m = 0 ; m < items[k].data.length ; m++) {
                var _t = items[k].data[m];
                path_.add(new Point(_t.x,_t.y));
            }
            path_ = null;
        }
        else if(tool_name == drawing_tools.brush_tool_size_15.name) {
            var path_ = new Path();
            path_._id = items[k]._id;
            path_.strokeWidth = items[k].stroke_size;
            path_.strokeColor = items[k].stroke_color;
            for(var m = 0 ; m < items[k].data.length ; m++) {
                var _t = items[k].data[m];
                path_.add(new Point(_t.x,_t.y));
            }
            path_ = null;
            view.draw();
        }
        else if(tool_name == drawing_tools.brush_tool_size_20.name) {
            var path_ = new Path();
            path_._id = items[k]._id;
            path_.strokeWidth = items[k].stroke_size;
            path_.strokeColor = items[k].stroke_color;
            for(var m = 0 ; m < items[k].data.length ; m++) {
                var _t = items[k].data[m];
                path_.add(new Point(_t.x,_t.y));
            }
            path_ = null;
        }
        else if(tool_name == drawing_tools.drawing_tool_circle_5.name) {
            if(items[k].data.length >= 2) {
                var radius = euclidian_distance(items[k].data[0],items[k].data[items[k].data.length - 1]);
                path_ = new Path.Circle(new Point(items[k].data[0].x, items[k].data[0].y), radius);
                path_._id = items[k]._id;
                path_.strokeWidth = items[k].stroke_size;
                path_.strokeColor = items[k].stroke_color;
                path_ = null;
            }
        }
        else if(tool_name == drawing_tools.drawing_tool_rectangle_5.name) {
            if(items[k].data.length >= 2) {
                var from = new Point(items[k].data[0].x,items[k].data[0].y);
                var to = new Point(items[k].data[items[k].data.length - 1].x, items[k].data[items[k].data.length - 1].y);
                path_ = new Path.Rectangle(from,to);
                path_._id = items[k]._id;
                path_.strokeWidth = items[k].stroke_size;
                path_.strokeColor = items[k].stroke_color;
                path_ = null;
            }
        }
        else if(tool_name == drawing_tools.drawing_tool_line_5.name) {
            if(items[k].data.length >= 2) {
                var from = new Point(items[k].data[0].x,items[k].data[0].y);
                var to = new Point(items[k].data[items[k].data.length - 1].x, items[k].data[items[k].data.length - 1].y);
                path_ = new Path.Line(from,to);
                path_._id = items[k]._id;
                path_.strokeWidth = items[k].stroke_size;
                path_.strokeColor = items[k].stroke_color;
                path_ = null;
            }
        }
        else if(tool_name == drawing_tools.drawing_tool_arrow_single.name) {
            if(items[k].data.length >= 2) {
                path_ = new paper_scope.Shape.ArrowLine(items[k].data[0].x,items[k].data[0].y, items[k].data[items[k].data.length - 1].x, items[k].data[items[k].data.length - 1].y,false);
                path_._id = items[k]._id;
                path_.strokeWidth = items[k].stroke_size;
                path_.strokeColor = items[k].stroke_color;
                path_ = null;
            }
        }
        else if(tool_name == drawing_tools.drawing_tool_arrow_double.name) {
            if(items[k].data.length >= 2) {
                path_ = new paper_scope.Shape.ArrowLine(items[k].data[0].x,items[k].data[0].y, items[k].data[items[k].data.length - 1].x, items[k].data[items[k].data.length - 1].y,true);
                path_._id = items[k]._id;
                path_.strokeWidth = items[k].stroke_size;
                path_.strokeColor = items[k].stroke_color;
                path_ = null;
            }
        }
        else if(tool_name == drawing_tools.drawing_tool_right_triangle.name) {
            if(items[k].data.length >= 2) {
                path_ = new paper_scope.Shape.drawRightTriangle(items[k].data[0].x,items[k].data[0].y, items[k].data[items[k].data.length - 1].x, items[k].data[items[k].data.length - 1].y);
                path_._id = items[k]._id;
                path_.strokeWidth = items[k].stroke_size;
                path_.strokeColor = items[k].stroke_color;
                path_ = null;
            }
        }
        else if(tool_name == drawing_tools.drawing_tool_poly_6_size_5.name) {
            if(items[k].data.length >= 2) {
                var radius = euclidian_distance(items[k].data[0],items[k].data[items[k].data.length - 1]);
                path_ = new Path.RegularPolygon(new Point(items[k].data[0].x,items[k].data[0].y), 6, radius);
                path_._id = items[k]._id;
                path_.strokeWidth = items[k].stroke_size;
                path_.strokeColor = items[k].stroke_color;
                path_ = null;
            }
        }
        else if(tool_name == drawing_tools.drawing_tool_poly_5_size_5.name) {
            if(items[k].data.length >= 2) {
                var radius = euclidian_distance(items[k].data[0],items[k].data[items[k].data.length - 1]);
                path_ = new Path.RegularPolygon(new Point(items[k].data[0].x,items[k].data[0].y), 5, radius);
                path_._id = items[k]._id;
                path_.strokeWidth = items[k].stroke_size;
                path_.strokeColor = items[k].stroke_color;
                path_ = null;
            }
        }
        else if(tool_name == drawing_tools.drawing_tool_triangle_5.name) {
            if(items[k].data.length >= 2) {
                var radius = euclidian_distance(items[k].data[0],items[k].data[items[k].data.length - 1]);
                path_ = new Path.RegularPolygon(new Point(items[k].data[0].x,items[k].data[0].y), 3, radius);
                path_._id = items[k]._id;
                path_.strokeWidth = items[k].stroke_size;
                path_.strokeColor = items[k].stroke_color;
                path_ = null;
            }
        }
        else if(tool_name == drawing_tools.drawing_tool_cartesian.name) {
            if(items[k].data.length >= 2) {
                var spoint = items[k].data[0];
                var lpoint = items[k].data[items[k].data.length - 1];
                path_ = new paper_scope.Shape.draw_cartesian(spoint.x,spoint.y,lpoint.x,lpoint.y,true,false,false);
                path_._id = items[k]._id;
                path_ = null;
            }
        }
        else if(tool_name == drawing_tools.drawing_tool_cartesian_marked.name) {
            if(items[k].data.length >= 2) {
                var spoint = items[k].data[0];
                var lpoint = items[k].data[items[k].data.length - 1];
                path_ = new paper_scope.Shape.draw_cartesian(spoint.x,spoint.y,lpoint.x,lpoint.y,true,true,false);
                path_._id = items[k]._id;
                path_ = null;

            }
        }
    }
}