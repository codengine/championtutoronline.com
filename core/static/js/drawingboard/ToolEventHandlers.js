paper_scope.Shape.ArrowLine = drawArrowLine;
paper_scope.Shape.drawRightTriangle = drawRightTriangle;
window.euclidian_distance = euclidian_distance;
window.initSelectionRectangle = initSelectionRectangle;
paper_scope.Shape.draw_cartesian = draw_cartesian;
paper_scope.PointText.prototype.wordwrap = draw_wordwrap;

var current_tsync_id = uuid_generator();

function onMouseDown(event) {

    if(!window.drawing_enabled) {
        return;
    }

    if(drawing_board_loading[active_board_id]) {
        return;
    }

    if(event.tool.name == drawing_tools.drawing_tool_selection.name){
        segment = path = null;
        var hitResult = false;
        hitResult = paper_scope.project.hitTest(event.point, background_image_hit_options);
        if(hitResult) {
            return;
        }
        hitResult = paper_scope.project.hitTest(event.point, background_grid_hit_options);
        if(hitResult) {
            return;
        }
        hitResult = paper_scope.project.hitTest(event.point, hitOptions);
//        if (!hitResult)
//            return;

        //console.log(hitResult);

        if(hitResult){
            console.log("Inside hitResult");
            console.log(hitResult.item.name);
            console.log(hitResult);
            if(hitResult.item.name == "grid_unit") {
                return;
            }
            path = hitResult.item;
            if (hitResult.type == 'segment') {
                if(selectionRectangle!=null && path.name == "selection rectangle")
                {
                    if(hitResult.segment.index >= 2 && hitResult.segment.index <= 4)
                    {
                        selectionRectangleRotation = 0;
                    }
                    else
                    {
                        selection_rect_scale_length = event.point.subtract(selectionRectangle.bounds.center).length;
                        selectionRectangleScale = event.point.subtract(selectionRectangle.bounds.center).length/path.scaling.x;
                    }
                }
                else {
                    segment = hitResult.segment;
                }
            } else if (hitResult.type == 'stroke' && path!=selectionRectangle) {
                //var location = hitResult.location;
                //segment = path.insert(location.index + 1, event.point);
                //path.smooth();

            }
            if((selectionRectangle==null || selectionRectangle.ppath!=path) && selectionRectangle!=path)
            {
                initSelectionRectangle(path);
            }
        }
        else {

            console.log("Inside childrens");

            var hit_found = false;
            //var all_items = paper_scope.project.activeLayer.children;
            for(var i = paths.length - 1 ; i >= 0 ; i--){

                console.log(paths[i]);
                if(typeof paths[i] != "undefined" && paths[i] != null && paths[i] == selectionRectangle || paths[i].name == "background_grid" || paths[i].name == "background_image") {
                    continue;
                }

                var bounding_rectangle = paths[i].bounds;

                console.log(bounding_rectangle);

                result = bounding_rectangle.contains(event.point);
                console.log("Result: "+result);
                if(result){
                    if(!hit_found){
                        paths[i].selected = true;
                        //all_items[i].position = new Point(event.point.x,event.point.y);
                        path = paths[i];
                        hit_found = true;
                    }
                }
                else{
                    paths[i].selected = false;
                }
            }
            if(hit_found){
                initSelectionRectangle(path);
            }
            else {
                if(selectionRectangle!=null) {
                    selectionRectangle.remove();
                    selectionRectangle = null;
                }
            }

            dragging = true;
        }

        return;
    }

    if(selectionRectangle!=null) {
        selectionRectangle.remove();
        selectionRectangle = null;
    }

    //By default path will be segment.
    path = new Path();
    current_tsync_id = uuid_generator();
    path._id = current_tsync_id;

    //Check pencil tool.

    if(event.tool.name == drawing_tools.pencil_tool_size_5.name){
        drawing_stroke_size = 1;
        path.strokeWidth = 1;
    }
    else if(event.tool.name == drawing_tools.pencil_tool_size_10.name){
        drawing_stroke_size = 3;
        path.strokeWidth = 3;
    }
    else if(event.tool.name == drawing_tools.pencil_tool_size_15.name){
        drawing_stroke_size = 5;
        path.strokeWidth = 5;
    }
    else if(event.tool.name == drawing_tools.pencil_tool_size_20.name){
        drawing_stroke_size = 10;
        path.strokeWidth = 10;
    }
    //Check eraser tool.
    else if(event.tool.name == drawing_tools.eraser_tool_size_5.name){
        path.strokeWidth = 3;
        drawing_stroke_size = 3;
        path.globalCompositeOperation = 'destination-out';
    }
    else if(event.tool.name == drawing_tools.eraser_tool_size_10.name){
        path.strokeWidth = 5;
        drawing_stroke_size = 5;
        path.globalCompositeOperation = 'destination-out';
    }
    else if(event.tool.name == drawing_tools.eraser_tool_size_15.name){
        path.strokeWidth = 10;
        drawing_stroke_size = 10;
        path.globalCompositeOperation = 'destination-out';
    }
    else if(event.tool.name == drawing_tools.eraser_tool_size_20.name){
        path.strokeWidth = 15;
        drawing_stroke_size = 15;
        path.globalCompositeOperation = 'destination-out';
    }

    //Check brush tool
    else if(event.tool.name == drawing_tools.brush_tool_size_5.name){
        path.strokeWidth = 5;
        drawing_stroke_size = 5;
    }
    else if(event.tool.name == drawing_tools.brush_tool_size_10.name){
        path.strokeWidth = 10;
        drawing_stroke_size = 10;
    }
    else if(event.tool.name == drawing_tools.brush_tool_size_15.name){
        path.strokeWidth = 13;
        drawing_stroke_size = 13;
    }
    else if(event.tool.name == drawing_tools.brush_tool_size_20.name){
        path.strokeWidth = 17;
        drawing_stroke_size = 17;
    }

    //Check circle tool.
    else if(event.tool.name == drawing_tools.drawing_tool_circle_5.name){
        drawing_stroke_size = 1;
        path = new Path.Circle(new Point(event.point.x, event.point.y),0);
    }
    //Check rectangle tool.
    else if(event.tool.name == drawing_tools.drawing_tool_rectangle_5.name){
        var from = new Point(event.point.x, event.point.y);
        var to = new Point(event.point.x, event.point.y);
        path = new Path.Rectangle(from,to);
        drawing_stroke_size = 1;
    }
    //Check Text tool.
    else if(event.tool.name == drawing_tools.drawing_tool_text.name){
        var from = new Point(event.point.x, event.point.y);
        var to = new Point(event.point.x, event.point.y);
        path = new Path.Rectangle(from,to);
        mouse_down_point = { "x": event.pageX, "y": event.pageY }
    }
    //Check line tool.
    else if(event.tool.name == drawing_tools.drawing_tool_line_5.name){
        var from = new Point(event.point.x, event.point.y);
        var to = new Point(event.point.x, event.point.y);
        path = new Path.Line(from,to);
        drawing_stroke_size = 1;
    }
    //Check if Hexagon.
    else if(event.tool.name == drawing_tools.drawing_tool_poly_6_size_5.name){
        path = new Path.RegularPolygon(new Point(event.point.x, event.point.y), 0, 0);
        drawing_stroke_size = 1;
    }
    //Check if Pentagon..
    else if(event.tool.name == drawing_tools.drawing_tool_poly_5_size_5.name){
        path = new Path.RegularPolygon(new Point(event.point.x, event.point.y), 0, 0);
        drawing_stroke_size = 1;
    }
    //Check if Triangle.
    else if(event.tool.name == drawing_tools.drawing_tool_triangle_5.name){
        path = new Path.RegularPolygon(new Point(event.point.x, event.point.y), 0, 0);
        drawing_stroke_size = 1;
    }
    //Check if line with single arrow.
    else if(event.tool.name == drawing_tools.drawing_tool_arrow_single.name){
        path = new paper_scope.Shape.ArrowLine(event.downPoint.x, event.downPoint.y, event.point.x, event.point.y,false);
        drawing_stroke_size = 1;
    }
    //Check if line with double arrow.
    else if(event.tool.name == drawing_tools.drawing_tool_arrow_double.name){
        path = new paper_scope.Shape.ArrowLine(event.downPoint.x, event.downPoint.y, event.point.x, event.point.y,true);
        drawing_stroke_size = 1;
    }
    //Check if right triangle.
    else if(event.tool.name == drawing_tools.drawing_tool_right_triangle.name){
        path = new paper_scope.Shape.drawRightTriangle(event.downPoint.x, event.downPoint.y, event.point.x, event.point.y);
        drawing_stroke_size = 1;
    }

    if(drawing_color_selected != undefined)
    {
        path.strokeColor = drawing_color_selected;
    }
    else {
        path.strokeColor = 'black';
    }

    if(event.tool.name.indexOf("eraser_tool_size_") > -1){
        path.strokeColor = 'white';
    }

    if(event.tool.name == drawing_tools.drawing_tool_text.name){
        path.style = {
            strokeColor: 'black',
            dashArray: [4, 10],
            strokeWidth: 4,
            strokeCap: 'round'
        };
    }

    start_point = event.point;
    previous_point = event.point;
    path.add(event.point);
    path.strokeCap = "round";

    if(typeof path != "undefined") {
        send_data_packet(event, path._id);
    }
    else {
        send_data_packet(event);
    }
}

function onMouseDrag(event) {

    if(!window.drawing_enabled) {
        return;
    }

    if(drawing_board_loading[active_board_id]) {
        return;
    }

    if(event.tool.name == drawing_tools.drawing_tool_selection.name){

        if (selectionRectangleScale!=null)
        {
            ratio = event.point.subtract(selectionRectangle.bounds.center).length/selectionRectangleScale;
            console.log("Ration...");
            console.log(ratio);
            console.log("Ration......");
            scaling = new Point(ratio, ratio);
            selectionRectangle.scaling = scaling;
            selectionRectangle.ppath.scaling = scaling;
            console.log('scaling: '+selectionRectangle.ppath);
            send_item_scaling_packet(event, selectionRectangle.ppath._id, ratio);
            return;
        }
        else if(selectionRectangleRotation!=null)
        {
            console.log('rotation: '+selectionRectangle.ppath);
            rotation = event.point.subtract(selectionRectangle.pivot).angle + 90;
            selectionRectangle.ppath.rotation = rotation;
            selectionRectangle.rotation = rotation;
            send_item_rotate_packet(event, selectionRectangle.ppath._id, selectionRectangle.pivot, rotation);
            return;
        }
        else {
            console.log("Delta: "+event.delta);
            console.log("Position: "+event.point);
            console.log("Calculated: "+(new Point(event.point.x,event.point.y)));
            if (path!=selectionRectangle)
            {
                console.log("First");
                if(typeof path != "undefined" && path != null && typeof selectionRectangle != "undefined" && selectionRectangle != null) {
                    console.log(path);
                    path.position = new Point(event.point.x,event.point.y);
                    selectionRectangle.position = new Point(event.point.x,event.point.y);
                }
            }
            else
            {
                console.log("Second");
                if(typeof path != "undefined" && path != null && typeof selectionRectangle != "undefined" && selectionRectangle != null) {
                    if(path.name == "point_text") {
                        console.log(path);
                        path.position = new Point(event.point.x,event.point.y);
                        selectionRectangle.position = new Point(event.point.x,event.point.y);
                        selectionRectangle.ppath.position = new Point(event.point.x,event.point.y);
                    }
                    else {
                        path.position = new Point(event.point.x,event.point.y);
                        selectionRectangle.position = new Point(event.point.x,event.point.y);
                        selectionRectangle.ppath.position = new Point(event.point.x,event.point.y);
                    }
                }
            }
        }
        console.log("Segment: "+segment);
    }
    else{
        if(event.tool.name == drawing_tools.drawing_tool_circle_5.name){
            path.removeSegments();
            previous_point = event.point;

            //console.log(previous_point);

            path.remove();
            var radius = euclidian_distance(start_point,event.point);
            console.log(parseInt(radius));
            console.log(start_point);
            path = new Path.Circle(new Point(start_point.x, start_point.y), radius);

            //path.add(start_point);
            //path.add(event.point);
        }
        else if(event.tool.name == drawing_tools.drawing_tool_rectangle_5.name){
            path.remove();
            var from = new Point(start_point.x,start_point.y);
            var to = new Point(event.point.x, event.point.y);
            path = new Path.Rectangle(from,to);
        }
        //Check Text tool.
        else if(event.tool.name == drawing_tools.drawing_tool_text.name){
            path.remove();
            var from = new Point(start_point.x,start_point.y);
            var to = new Point(event.point.x, event.point.y);
            path = new Path.Rectangle(from,to);
            path.dashArray = [10, 4];
        }
        else if(event.tool.name == drawing_tools.drawing_tool_line_5.name){
            path.remove();
            var from = new Point(start_point.x,start_point.y);
            var to = new Point(event.point.x, event.point.y);
            path = new Path.Line(from,to);
        }
        else if(event.tool.name == drawing_tools.drawing_tool_triangle_5.name){
            path.remove();
            var radius = euclidian_distance(new Point(start_point.x, start_point.y),event.point);
            path = new Path.RegularPolygon(start_point, 3, radius);
        }
        //Check if Hexagon.
        else if(event.tool.name == drawing_tools.drawing_tool_poly_6_size_5.name){
            path.remove();
            var radius = euclidian_distance(new Point(start_point.x, start_point.y),event.point);
            path = new Path.RegularPolygon(start_point, 6, radius);
        }
        //Check if Pentagon..
        else if(event.tool.name == drawing_tools.drawing_tool_poly_5_size_5.name){
            path.remove();
            var radius = euclidian_distance(new Point(start_point.x, start_point.y),event.point);
            path = new Path.RegularPolygon(start_point, 5, radius);
        }
        //Check if line with single arrow.
        else if(event.tool.name == drawing_tools.drawing_tool_arrow_single.name){
            path.remove();
            path = new paper_scope.Shape.ArrowLine(event.downPoint.x, event.downPoint.y, event.point.x, event.point.y,false);
        }
        //Check if line with double arrow.
        else if(event.tool.name == drawing_tools.drawing_tool_arrow_double.name){
            path.remove();
            path = new paper_scope.Shape.ArrowLine(event.downPoint.x, event.downPoint.y, event.point.x, event.point.y,true);
        }
        //Check if right triangle.
        else if(event.tool.name == drawing_tools.drawing_tool_right_triangle.name){
            path.remove();
            path = new paper_scope.Shape.drawRightTriangle(event.downPoint.x, event.downPoint.y, event.point.x, event.point.y);
        }
        else if(event.tool.name == drawing_tools.drawing_tool_cartesian.name) {
            path.remove();
            path = new paper_scope.Shape.draw_cartesian(start_point.x,start_point.y,event.point.x,event.point.y,true,false,false);
            drawing_stroke_size = 1;
        }
        else if(event.tool.name == drawing_tools.drawing_tool_cartesian_marked.name) {
            path.remove();
            path = new paper_scope.Shape.draw_cartesian(start_point.x,start_point.y,event.point.x,event.point.y,true,true,false);
            drawing_stroke_size = 1;
        }
        //Check brush tool
        else if(event.tool.name == drawing_tools.brush_tool_size_5.name){
            path.strokeJoin = "round";
            path.add(event.point);
            drawing_stroke_size = 5;
        }
        else if(event.tool.name == drawing_tools.brush_tool_size_10.name){
            path.strokeJoin = "round";
            path.add(event.point);
            drawing_stroke_size = 10;
        }
        else if(event.tool.name == drawing_tools.brush_tool_size_15.name){
            path.strokeJoin = "round";
            path.add(event.point);
            drawing_stroke_size = 13;
        }
        else if(event.tool.name == drawing_tools.brush_tool_size_20.name){
            path.strokeJoin = "round";
            path.add(event.point);
            drawing_stroke_size = 17;
        }
        else if(event.tool.name == drawing_tools.pencil_tool_size_5.name){
            path.strokeJoin = "round";
            drawing_stroke_size = 1;
            path.add(event.point);
        }
        else if(event.tool.name == drawing_tools.pencil_tool_size_10.name){
            path.strokeJoin = "round";
            drawing_stroke_size = 3;
            path.add(event.point);
        }
        else if(event.tool.name == drawing_tools.pencil_tool_size_15.name){
            path.strokeJoin = "round";
            drawing_stroke_size = 5;
            path.add(event.point);
        }
        else if(event.tool.name == drawing_tools.pencil_tool_size_20.name){
            path.strokeJoin = "round";
            drawing_stroke_size = 10;
            path.add(event.point);
        }
        else if(event.tool.name == drawing_tools.eraser_tool_size_5.name){
            path.strokeJoin = "round";
            drawing_stroke_size = 1;
            path.add(event.point);
        }
        else if(event.tool.name == drawing_tools.eraser_tool_size_10.name){
            path.strokeJoin = "round";
            drawing_stroke_size = 3;
            path.add(event.point);
        }
        else if(event.tool.name == drawing_tools.eraser_tool_size_15.name){
            path.strokeJoin = "round";
            drawing_stroke_size = 5;
            path.add(event.point);
        }
        else if(event.tool.name == drawing_tools.eraser_tool_size_20.name){
            path.strokeJoin = "round";
            drawing_stroke_size = 10;
            path.add(event.point);
        }
        else {
            path.strokeJoin = "round";
            drawing_stroke_size = 1;
            path.add(event.point);

        }

        if(drawing_color_selected != undefined && path != undefined && path != null)
        {
            path.strokeColor = drawing_color_selected;
        }
        else {
            if(path != undefined && path != null) {
                path.strokeColor = 'black';
            }
        }

        if(event.tool.name.indexOf("eraser_tool_size_") > -1 && path != undefined && path != null){
            path.strokeColor = 'white';
        }
        if(typeof path != "undefined" && path != null) {
            path._id = current_tsync_id;
        }

    }
    //console.log("Sending stream...");
    //console.log(paperboard_streamer);


    if(typeof path != "undefined" && path != null) {
        send_data_packet(event, path._id);
    }
    else {
        send_data_packet(event);
    }
}

function onMouseMove(event) {

    if(!window.drawing_enabled) {
        return;
    }

    if(drawing_board_loading[active_board_id]) {
        return;
    }



    if(event.tool.name == drawing_tools.drawing_tool_selection.name) {
            var hit_found = false;
            for(var i = paths.length - 1 ; i >= 0 ; i--){
                if(paths[i] != undefined) {
                    var bounding_rectangle = paths[i].bounds;

                    result = bounding_rectangle.contains(event.point);
                    if(result){
                        if(!hit_found){
                            paths[i].selected = true;
                            //initSelectionRectangle(paths[i]);
                            hit_found = true;
                        }
                    }
                    else{
                        paths[i].selected = false;
                    }
                }
            }
    }
}

function onMouseUp(event) {

    if(!window.drawing_enabled) {
        return;
    }

    if(drawing_board_loading[active_board_id]) {
        return;
    }

    if(path != null) {

        path.onClick = function(event) {
            this.selected = true;
        }

        if(event.tool.name == drawing_tools.drawing_tool_text.name){

            var text_bounds = path.bounds;
            path.remove();

            var text_start_point = event.downPoint;
            var text_end_point = event.point;

            //console.log("Original: "+event+", and now: "+text_end_point.y);

            var drag_outside = false;
            var same_point = false;

            if(text_end_point.x < 0 || text_end_point.y < 0) {
                drag_outside = true;
            }

            if(text_start_point == text_end_point) {
                same_point = true;
            }

            if(!drag_outside && !same_point) {
                if(text_start_point.x >= text_end_point.x &&
                text_start_point.y >= text_end_point.y) {
                        var temp_point = text_start_point;
                        text_start_point = text_end_point;
                        text_end_point = temp_point;
                    }
                    else if(text_start_point.x < text_end_point.x && text_start_point.y > text_end_point.y) {
                        temp_text_start_point = {
                            "x": text_start_point.x,
                            "y": text_end_point.y
                        }

                        temp_text_end_point = {
                            "x": text_end_point.x,
                            "y": text_start_point.y
                        }
                        text_start_point = temp_text_start_point;
                        text_end_point = temp_text_end_point;
                    }
                    else if(text_start_point.x > text_end_point.x && text_start_point.y < text_end_point.y) {
                        temp_text_start_point = {
                            "x": text_end_point.x,
                            "y": text_start_point.y
                        }

                        temp_text_end_point = {
                            "x": text_start_point.x,
                            "y": text_end_point.y
                        }
                        text_start_point = temp_text_start_point;
                        text_end_point = temp_text_end_point;
                    }

                    var left = text_start_point.x;
                    var top = text_start_point.y + parseInt($("#"+active_board_id).offset().top);
                    var width = Math.abs(text_end_point.x - left);
                    var height = Math.abs(text_end_point.y - text_start_point.y);

                    if(last_text_element != null) {
                        var text_val = $(document).find("#id_text_inputbox").val();
                        var position = new Point(last_text_element.left, last_text_element.top + 25);
                        var text = new PointText(position);
                        text.position = position;
                        text.name = "point_text";
                        text.justification = 'left';
                        text.fontFamily = 'Arial';
                        text.fontSize ='20pt';
                        text.fillColor = 'black';
                        //text.bounds = last_text_element.bounds;
                        if(text_val != null && text_val != "") {
                            text.wordwrap(text_val,last_text_element.width/15);
                            text._id = current_tsync_id;
                            send_text_point_packet(event, "draw_text", { left: last_text_element.left, top: last_text_element.top + 25 }, { width: last_text_element.width }, text_val, text._id);
                            text.onDoubleClick = function(evt) {
                                alert("asdsd");
                            }
                        }

                        $(document).find('#id_text_inputbox').remove();
                    }

                    var textarea_element = $("<textarea type='text' id='id_text_inputbox' style='z-index: 901;position:absolute;line-height: 20px; resize: both; overflow: hidden; border: 2px dashed #343434; background: #F5F6CE; font:20pt Arial;width:"+ width +"px;height: "+ height +"px; z-index: 999; left:"+ left +"px; top:"+ top +"px;'></textarea>")
                        .focusout(function()
                        {
                            if(last_text_element != null) {

                                var text_val = $(document).find("#id_text_inputbox").val();
                                var position = new Point(last_text_element.left, last_text_element.top + 25);
                                var text = new PointText(position);
                                text.position = position;
                                text.justification = 'left';
                                text.fontFamily = 'Arial';
                                text.fontSize ='20pt';
                                text.fillColor = 'black';
                                //text.bounds = last_text_element.bounds;
                                if(text_val != null && text_val != "") {
                                    text.wordwrap(text_val,last_text_element.width/15);
                                    //send_text_point_packet("draw_text", new Point(last_text_element.left, last_text_element.top + 25));
                                    text.onDoubleClick = function(evt) {
                                        alert("asdsd");
                                    }
                                }

                                $(document).find('#id_text_inputbox').remove();
                            }
                        })
                        .keyup(function(){
                            //$(this).css("height","auto");
                            //$(this).css("height",$(this).prop('scrollHeight')+"px");
                            //console.log($(document).find('#id_text_inputbox').val());
                        })
                        .draggable()
                        .resizable({
                            stop: function( event, ui ){
                                alert(ui.size.width);
                            }
                        })
                        .css("z-index",0)
                        .css("font-size", "20pt")
                        .css("font-family","Arial")
                        .appendTo('body');
                    setTimeout(function() {
                        $(document).find('#id_text_inputbox').focus();
                    }, 0);

                    //console.log(textarea_element);

                    last_text_element = {
                        "left": left,
                        "top": text_start_point.y,
                        "width": width,
                        "height": height,
                        "bounds": text_bounds,
                        "dom": textarea_element
                    };
                    path = text;
            }

        }

    }

    if(path != selectionRectangle) {
        paths.push(path);
        if(!drawingboards.hasOwnProperty(active_board_id)) {
            drawingboards[active_board_id] = [];
        }
        drawingboards[active_board_id].push(path);
        if(!window.undo_stack.hasOwnProperty(active_board_id)) {
            window.undo_stack[active_board_id] = [ path ]
        }
        else {
            window.undo_stack[active_board_id].push(path);
        }
    }
    axis_object = null;
    axis_object_marked = null;

    dragging = false;

    //path = null;
    //segment = null;
    selectionRectangleScale = null;
    selectionRectangleRotation = null;

    if(typeof path != "undefined" && path != null) {
        send_data_packet(event, path._id);
    }
    else {
        send_data_packet(event);
    }

    //Saving drawing data.
    var active_canvas = paper_scope.View._viewsById[active_board_id];
    var drawing_data = active_canvas._project.exportJSON();
    savePaperboard(active_board_id, "save_board", drawing_data);

}

function onKeyDown(event) {

    if(!window.drawing_enabled) {
        return;
    }

    if(drawing_board_loading[active_board_id]) {
        return;
    }
}

function onKeyUp(event) {

    if(!window.drawing_enabled) {
        return;
    }

    if(drawing_board_loading[active_board_id]) {
        return;
    }

    if(event.key == "delete") {



        var temp_list = [];
        for(var k = 0 ; k < paths.length ; k ++) {
            if(paths[k] != path) {
               temp_list.push(paths[k]);
            }
        }
        paths = temp_list;

        send_item_delete_packet(event, "item_delete", path._id);

        if(path != null) {
            path.remove();
        }
        if(segment != null) {
            segment.remove();
        }
        if(selectionRectangle != null) {
            selectionRectangle.remove();
        }

        //Saving drawing data.
        var active_canvas = paper_scope.View._viewsById[active_board_id];
        var drawing_data = active_canvas._project.exportJSON();
        savePaperboard(active_board_id, "save_board", drawing_data);

    }
}