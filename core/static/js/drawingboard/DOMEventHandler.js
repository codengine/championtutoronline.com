//Attach color click event listener.

function set_default_color_menu_border()
{
    $("#black").css({"border":"2px solid #1c1f21"});
    $("#orange").css({"border":"2px solid #ff9875"});
    $("#sky_blue").css({"border":"2px solid #67b5e6"});
    $("#green").css({"border":"2px solid #6eb711"});
    $("#yellow").css({"border":"2px solid #fcac33"});
    $("#peach").css({"border":"2px solid #ef665c"});
    $("#gray").css({"border":"2px solid #6a6b7e"});
}

window.drawing_color_selected = "#1c1f21";

color_menu_click_handler = function(event)
{
    set_default_color_menu_border();
    //alert($(this).attr('border'));
    $(this).css({"border":"2px solid #fff"});

    var element_id = event.target.id;

    var color = "#1c1f21";

    if(element_id == "black")
    {
        color = "#1c1f21";
    }
    else if(element_id == "orange")
    {
        color = "#ff9875";
    }
    else if(element_id == "sky_blue")
    {
        color = "#67b5e6";
    }
    else if(element_id == "green")
    {
        color = "#6eb711";
    }
    else if(element_id == "yellow")
    {
        color = "#fcac33";
    }
    else if(element_id == "peach")
    {
        color = "#ef665c";
    }
    else if(element_id == "gray")
    {
        color = "#6a6b7e";
    }

    drawing_color_selected = color;

    //console.log("Tool color selected: "+whiteboard.selected_tool.color);

    return false;
};

$(document).on("click","#black",color_menu_click_handler);
$(document).on("click","#orange",color_menu_click_handler);
$(document).on("click","#sky_blue",color_menu_click_handler);
$(document).on("click","#green",color_menu_click_handler);
$(document).on("click","#yellow",color_menu_click_handler);
$(document).on("click","#peach",color_menu_click_handler);
$(document).on("click","#gray",color_menu_click_handler);


window.drawing_stroke_size = 5;

function resetStrokeSizeSelection() {
    for(var i = 1 ; i < 6; i++) {
        $("#size"+i).css({"border":"2px solid #1c1f21"});
    }
}

function strokeSizeClickHandler(e) {
    e.preventDefault();
    var id = $(this).prop("id");
    if(id == "size1") {
        drawing_stroke_size = 5;
    }
    else if(id == "size2") {
        drawing_stroke_size = 7;
    }
    else if(id == "size3") {
        drawing_stroke_size = 9;
    }
    else if(id == "size4") {
        drawing_stroke_size = 11;
    }
    else if(id == "size5") {
        drawing_stroke_size = 13;
    }
    return false;
}



$(document).on("click","a[id^=size]",strokeSizeClickHandler);

var deselect_tools = function()
{
    $("#id_canvas_menu_icon_redo").removeClass('menu_active');
    $("#id_canvas_menu_icon_undo").removeClass('menu_active');
    $("#id_canvas_menu_icon_select").removeClass('menu_active');
    $("#id_canvas_menu_icon_pen_menu").removeClass('menu_active');
    $("#id_canvas_menu_icon_eraser").removeClass('menu_active');
    $("#id_canvas_menu_icon_shape").removeClass('menu_active');
    $("#id_canvas_menu_icon_text").removeClass('menu_active');
    $("#id_canvas_menu_icon_fx").removeClass('menu_active');
    $("#id_canvas_menu_icon_line").removeClass('menu_active');
    $("#id_canvas_menu_icon_circle").removeClass('menu_active');
    $("#id_canvas_menu_icon_triangle").removeClass('menu_active');
    $("#id_canvas_menu_icon_rectangle").removeClass('menu_active');
};

$(document).on("click","#id_canvas_menu_icon_pen_size5",function(e)
{
    e.preventDefault();

    $("#drawing_board").css('cursor','url(/static/images/wb_menu_icon4_cursor.png) 0 19, auto');

    //console.log(window.drawing_tools.pencil_tool);
    drawing_tools.pencil_tool_size_5.activate();

    return false;
});

$(document).on("click","#id_canvas_menu_icon_pen_size10",function(e)
{
    e.preventDefault();

    $("#drawing_board").css('cursor','url(/static/images/wb_menu_icon4_cursor.png) 0 19, auto');

    drawing_tools.pencil_tool_size_10.activate();

    return false;
});

$(document).on("click","#id_canvas_menu_icon_pen_size15",function(e)
{
    e.preventDefault();

    $("#drawing_board").css('cursor','url(/static/images/wb_menu_icon4_cursor.png) 0 19, auto');

    drawing_tools.pencil_tool_size_15.activate();

    return false;
});

$(document).on("click","#id_canvas_menu_icon_pen_size20",function(e)
{
    e.preventDefault();

    $("#drawing_board").css('cursor','url(/static/images/wb_menu_icon4_cursor.png) 0 19, auto');

    drawing_tools.pencil_tool_size_20.activate();

    return false;
});

$(document).on("click","#id_canvas_menu_icon_eraser_size5",function(e)
{
    e.preventDefault();

    drawing_tools.eraser_tool_size_5.activate();

    return false;
});

$(document).on("click","#id_canvas_menu_icon_eraser_size10",function(e)
{
    e.preventDefault();

    drawing_tools.eraser_tool_size_10.activate();

    return false;
});

$(document).on("click","#id_canvas_menu_icon_eraser_size15",function(e)
{
    e.preventDefault();

    drawing_tools.eraser_tool_size_15.activate();

    return false;
});

$(document).on("click","#id_canvas_menu_icon_eraser_size20",function(e)
{
    e.preventDefault();

    drawing_tools.eraser_tool_size_20.activate();

    return false;
});


$(document).on("click","#id_canvas_menu_icon_brush_size5",function(e)
{
    e.preventDefault();

    drawing_tools.brush_tool_size_5.activate();

    return false;
});

$(document).on("click","#id_canvas_menu_icon_brush_size10",function(e)
{
    e.preventDefault();

    drawing_tools.brush_tool_size_10.activate();

    return false;
});

$(document).on("click","#id_canvas_menu_icon_brush_size15",function(e)
{
    e.preventDefault();

    drawing_tools.brush_tool_size_15.activate();

    return false;
});

$(document).on("click","#id_canvas_menu_icon_brush_size20",function(e)
{
    e.preventDefault();

    drawing_tools.brush_tool_size_20.activate();

    return false;
});

$(document).on("click","#id_canvas_menu_icon_shape_circle",function(e)
{
    e.preventDefault();

    drawing_tools.drawing_tool_circle_5.activate();

    return false;
});

$(document).on("click","#id_canvas_menu_icon_shape_hexa",function(e)
{
    e.preventDefault();

    drawing_tools.drawing_tool_poly_6_size_5.activate();

    return false;
});

$(document).on("click","#id_canvas_menu_icon_shape_penta",function(e)
{
    e.preventDefault();

    drawing_tools.drawing_tool_poly_5_size_5.activate();

    return false;
});

$(document).on("click","#id_canvas_menu_icon_shape_rect",function(e)
{
    e.preventDefault();

    drawing_tools.drawing_tool_rectangle_5.activate();

    return false;
});

$(document).on("click","#id_canvas_menu_icon_shape_triangle",function(e)
{
    e.preventDefault();

    drawing_tools.drawing_tool_triangle_5.activate();

    return false;
});

$(document).on("click","#id_canvas_menu_icon_shape_line",function(e)
{
    e.preventDefault();

    drawing_tools.drawing_tool_line_5.activate();

    return false;
});

$(document).on("click","#id_canvas_menu_icon_shape_righttriangle",function(e)
{
    e.preventDefault();

    drawing_tools.drawing_tool_right_triangle.activate();

    return false;
});

$(document).on("click","#id_canvas_menu_icon_shape_arrow1",function(e)
{
    e.preventDefault();

    drawing_tools.drawing_tool_arrow_single.activate();

    return false;
});

$(document).on("click","#id_canvas_menu_icon_shape_arrow2",function(e)
{
    e.preventDefault();

    drawing_tools.drawing_tool_arrow_double.activate();

    return false;
});

$(document).on("click","#id_canvas_menu_icon_axis1",function(e)
{
    e.preventDefault();

    drawing_tools.drawing_tool_cartesian.activate();

    return false;
});

$(document).on("click","#id_canvas_menu_icon_axismarked",function(e)
{
    e.preventDefault();

    drawing_tools.drawing_tool_cartesian_marked.activate();

    return false;
});



$(document).on("click","#id_canvas_menu_icon_graphno",function(e){
    e.preventDefault();

//    if(drawing_board_background[active_board_id]) {
//        drawing_board_background[active_board_id].remove();
//    }

    clear_background(active_board_id);
    if(drawing_board_background.hasOwnProperty(active_board_id)) {
        if(typeof drawing_board_background[active_board_id] != "undefined" && drawing_board_background[active_board_id] != null) {
            paper_scope.project.addLayer(drawing_board_background[active_board_id]);
        }
    }
    savePaperboard(active_board_id, "save_background", "graphno", "grid", function(data) {
        send_update_background_data_packet();
    });

    return false;
});

$(document).on("click","#id_canvas_menu_icon_graphpage",function(e){
   e.preventDefault();

    //window.draw_grid_graph(paper_scope,1900,2000,10,'black',20,5);
    //alert("asd");
    clear_background(active_board_id);
    var grid = drawGrid(15);
    savePaperboard(active_board_id, "save_background", "graphpage", "grid", function(data) {
        send_update_background_data_packet();
    });
    return false;
});

$(document).on("click","#id_canvas_menu_icon_graph4_1",function(e){
    e.preventDefault();
    clear_background(active_board_id);
    var grid = drawGrid(30);
    savePaperboard(active_board_id, "save_background", "4_1", "grid", function(data) {
        send_update_background_data_packet();
    });
    return false;
});

$(document).on("click","#id_canvas_menu_icon_graph4_2",function(e){
    e.preventDefault();
    clear_background(active_board_id);
    var grid = drawGrid(20);
    savePaperboard(active_board_id, "save_background", "4_2", "grid", function(data) {
        send_update_background_data_packet();
    });
    return false;
});

$(document).on("click","#id_canvas_menu_icon_graph2",function(e){
    e.preventDefault();
    clear_background(active_board_id);
    var grid = new draw_cartesian(0,0,window.innerWidth,1600,false,true,true);
    savePaperboard(active_board_id, "save_background", "graph2", "cartesian", function(data) {
        send_update_background_data_packet();
    });
    return false;
});

// $(document).on("click","#id_canvas_menu_icon_clearboard", function(e) {
//     alert("Item will be cleared away...");
// });