//Author Sohel.


//Install the paperScole to global.
//PaperScope is the main container or scope of paper.js
//It contains other elements like projects,view etc etc
//A scope can have multiple projects and one project activated at a time.
//View is not project specific.
var paper_scope = new paper.PaperScope();
paper_scope.install(window);

var path;
var start_point;
var previous_point;
var paths = [];
var dragging = false;
var selected_items = [];
var active_board_id;
var drawing_boards = {};
var drawing_board_background = {};
var clear_boards = {};
var axis_object,axis_object_marked;
var selection_rect_scale_length;

var mouse_down_point;

var drawingboards = {};
var undo_stacks_parties = {};


var hitOptions = {
    segments: true,
    stroke: true,
    fill: true,
    tolerance: 5
};

var background_image_hit_options = {
    name: "background_image"
};

var background_grid_hit_options = {
    name: "grid_unit"
};

var selectionRectangle = null;
var selectionRectangleScale=null;
var selectionRectangleScaleNormalized=null;
var selectionRectangleRotation=null;
var segment, selectionRectangleSegment;
var movePath = false;

var paperjs_init_done = false;
var paperjs_init_timer = false;

var last_text_element = null;


var current_drawing_paths = {};
var offline_paths = {}; //{ tool_name: '', data: [], stroke_color: '', stroke_size: '' }
var offline_backgrounds = {};


var wb_participants = [];

var initial_data_loaded = {}; //{ board_id: true } e.g. { 1: true, 2: true, 3: false }
var drawing_board_loading = {}; //{ 1: true } //true means loading and false means loaded or not tried till now.

window.undo_stack = {};
window.redo_stack = {};

window.cursor_position = {};
window.cursor_text_color = {};

window.screensharing = true;
window.drawing_enabled = true;

window.whiteboard_id = -1;
window.lesson_id = -1;

$(document).ready(function() {
    $("input[class=wb_participants]").each(function(i) {
        wb_participants.push(parseInt($(this).val()));
    });

    window.whiteboard_id = $("#id_hidden_whiteboard_id").val();
    window.lesson_id = $("#id_hidden_lesson_id").val();

    function notify_user_presence() {
        var presence_data = {
            sender_id: window.champ_user_id,
            other_participants: wb_participants
        };
        window.socket.emit("ask_whiteboard_presence",presence_data);
    }

    setTimeout(notify_user_presence, 2000);

    var data = {
        sender_id: window.champ_user_id,
        other_participants: wb_participants
    };
    window.onbeforeunload = function(){
         window.socket.emit("notify_whiteboard_exit",data);
    }

});

function send_data_packet(mouse_event, object_id) {
    var drawing_object = {
        stroke_color: drawing_color_selected,
        stroke_size: drawing_stroke_size,
        target_users:wb_participants,
        event_name: mouse_event.type,
        down_point: { x: mouse_event.downPoint.x, y: mouse_event.downPoint.y },
        current_point: { x: mouse_event.point.x, y: mouse_event.point.y },
        selected_tool: mouse_event.tool.name,
        sender: window.champ_user_id,
        active_board_id: $(paper_scope.view.element).prop("id"),
        object_id: object_id
    };

    paperboard_streamer.send_stream(drawing_object);
}

function send_text_point_packet(mouse_event, event_type, draw_point, point_size, text, object_id) {
   var drawing_object = {
        stroke_color: drawing_color_selected,
        stroke_size: drawing_stroke_size,
        target_users:wb_participants,
        event_name: event_type,
        down_point: draw_point,
        size: point_size,
        text: text,
        selected_tool: mouse_event.tool.name,
        sender: window.champ_user_id,
        active_board_id: $(paper_scope.view.element).prop("id"),
        object_id: object_id
    };

    paperboard_streamer.send_stream(drawing_object);
}

function send_item_delete_packet(mouse_event, event_type, object_id) {
    var drawing_object = {
        target_users:wb_participants,
        event_name: event_type,
        sender: window.champ_user_id,
        active_board_id: $(paper_scope.view.element).prop("id"),
        object_id: object_id
    };

    paperboard_streamer.send_stream(drawing_object);
}

function send_item_scaling_packet(mouse_event, item_id, ratio) {
    var drawing_object = {
        target_users:wb_participants,
        event_name: "item_scaling",
        sender: window.champ_user_id,
        active_board_id: $(paper_scope.view.element).prop("id"),
        object_id: item_id,
        ratio: ratio,
        down_point: { x: mouse_event.downPoint.x, y: mouse_event.downPoint.y },
        current_point: { x: mouse_event.point.x, y: mouse_event.point.y },
    };

    paperboard_streamer.send_stream(drawing_object);
}

function send_undo_redo_packet(action, item_id, item) {
    var drawing_object = {
        target_users:wb_participants,
        event_name: "undo_redo",
        action: action,
        sender: window.champ_user_id,
        active_board_id: $(paper_scope.view.element).prop("id"),
        object_id: item_id,
        item: item
    };

    paperboard_streamer.send_stream(drawing_object);
}

function send_item_rotate_packet(mouse_event, item_id, pivot, rotation) {
    var drawing_object = {
        target_users:wb_participants,
        event_name: "item_rotate",
        sender: window.champ_user_id,
        active_board_id: $(paper_scope.view.element).prop("id"),
        object_id: item_id,
        rotation: rotation,
        pivot: pivot,
        down_point: { x: mouse_event.downPoint.x, y: mouse_event.downPoint.y },
        current_point: { x: mouse_event.point.x, y: mouse_event.point.y },
    };

    paperboard_streamer.send_stream(drawing_object);
}

function send_clear_board_data_packet() {
    var data_packet = {
        event_name: "clear_board",
        target_users:wb_participants,
        sender: window.champ_user_id,
        active_board_id: $(paper_scope.view.element).prop("id")
    };
    paperboard_streamer.send_stream(data_packet);
}

function send_update_background_data_packet() {
    var data_packet = {
        event_name: "update_background",
        target_users:wb_participants,
        sender: window.champ_user_id,
        active_board_id: $(paper_scope.view.element).prop("id")
    };
    paperboard_streamer.send_stream(data_packet);
}

function clear_background(board_id) {

    var canvas = paper_scope.View._viewsById[board_id];

    var current_backgrounds = canvas._project.getItems({
       "name": "background_grid"
    });
    for(var i = 0 ; i < current_backgrounds.length ; i++) {
        current_backgrounds[i].remove();
    }

    var current_backgrounds = canvas._project.getItems({
       "name": "background_image"
    });
    for(var i = 0 ; i < current_backgrounds.length ; i++) {
        current_backgrounds[i].remove();
    }
    paper_scope.view.draw();
}

function load_drawingboard_background(board_id) {
    $.ajax({
        type: "GET",
        data: { "action": "LOAD_DB_BACKGROUND", "wb_id": $("#id_hidden_whiteboard_id").val(), "db_id": board_id },
        url: "/ajax/action_whiteboard_tab",
        success: function(data)
        {
            var data = jQuery.parseJSON(data);
            if(data.status == "SUCCESS"){

                var type = data.data.type;
                var background = data.data.background;
                if(type == "image") {
                    clear_background(board_id);

                    var url = background;
                    var raster = new paper_scope.Raster({
                        source: url,
                        position: new paper_scope.Point(view.center.x,parseInt($("#"+board_id).offset().top + 135))
                    });

                    raster.name = "background_image"

                    // If you create a Raster using a url, you can use the onLoad
                    // handler to do something once it is loaded:
                    raster.onLoad = function() {

                    };
                    //raster.selected = true;
                    raster.sendToBack();

                    drawing_board_background[active_board_id] = raster;

                }
                else if(type == "cartesian") {
                    if(background == "graph2") {
                        clear_background(board_id);
                        var grid = draw_cartesian(0,0,window.innerWidth,1600,false,true,true);
                        drawing_board_background[active_board_id] = grid;
                    }
                }
                else if(type == "grid") {
                    if(background == "graphno") {
                        clear_background(board_id);
                    }
                    else if(background == "graphpage") {
                        clear_background(board_id);
                        var grid = drawGrid(15);
                        drawing_board_background[active_board_id] = grid;
                    }
                    else if(background == "4_1") {
                        console.log("Lol!");
                        clear_background(board_id);
                        var grid = drawGrid(30);
                        drawing_board_background[active_board_id] = grid;
                    }
                    else if(background == "4_2") {
                        clear_background(board_id);
                        var grid = drawGrid(20);
                        drawing_board_background[active_board_id] = grid;
                    }
                }
            }
        },
        error: function(jqxhr, status, error)
        {

        }
    })
    .done(function( msg ) {

    });
}

function savePaperboard(board_id, action, data, type, callback, errorback, completeback) {
    if(!window.screensharing) {
        return;
    }
    $.ajax({
        type: "POST",
        url: "/ajax/drawing_board",
        data: { whiteboard_id: $("#id_hidden_whiteboard_id").val(), board_id: board_id, action: action, data: data, type: type },
        success: function(data)
        {
            data = jQuery.parseJSON(data)
            if(typeof callback != "undefined" && typeof callback === "function") {
                callback(data);
            }
        },
        error: function(jqxhr,status,errorthrown)
        {
            if(typeof errorback != "undefined" && typeof errorback === "function") {
                errorback(data);
            }
        }
    })
    .done(function(msg )
    {
        if(typeof completeback != "undefined" && typeof completeback === "function") {
            completeback(data);
        }
    });
}

function load_drawingboard_data(board_id) {
    $('#drawing_board_'+board_id).css("opacity","0.5");
    $('#drawing_board_'+board_id).css("cursor","wait");
    $("id_loading_whiteboard").show();
    drawing_board_loading['drawing_board_'+board_id] = true;
    $.ajax({
        type: "GET",
        url: "/ajax/drawing_data",
        data: { whiteboard_id: $("#id_hidden_whiteboard_id").val(), board_id: board_id },
        success: function(data)
        {
            data = jQuery.parseJSON(data)
            //console.log(data);
            if(data.status == "SUCCESS" && data.data != null && data.data != "") {
                var selected_canvas = paper_scope.View._viewsById['drawing_board_'+board_id];
                if(selected_canvas != undefined){
                    selected_canvas._project.clear();
                    selected_canvas._project.importJSON(data.data);
                    paper_scope.view.draw();
                    $('#drawing_board_'+board_id).css("opacity","1.0");
                    $('#drawing_board_'+board_id).css("cursor","default");
                    $("id_loading_whiteboard").hide();

                    clear_background('drawing_board_'+board_id);

                    load_drawingboard_background('drawing_board_'+board_id);

                    var _board_id = 'drawing_board_'+board_id;

                    var items = paper_scope.project.activeLayer.getItems({ name: "selection rectangle" });
                    for(var l = 0 ; l < items.length ; l++) {
                        items[l].remove();
                    }

                    if(paper_scope.project.activeLayer.children.length > 0) {
                       for(var i = 0 ; i < paper_scope.project.activeLayer.children.length ; i++) {
                            var child_element = paper_scope.project.activeLayer.children[i];
                            child_element.selected = false;
                            if(child_element != selectionRectangle) {
                                if(!window.undo_stack.hasOwnProperty(_board_id)) {
                                    window.undo_stack[_board_id] = [ child_element ];
                                }
                                else {
                                    window.undo_stack[_board_id].push(child_element);
                                }
                            }
                            window.paths.push(child_element);
                        }
                    }
                }
            }
            else {
                $('#drawing_board_'+board_id).css("opacity","1.0");
                $('#drawing_board_'+board_id).css("cursor","default");
                $("id_loading_whiteboard").hide();
            }
        },
        error: function(jqxhr,status,errorthrown)
        {

        }
    })
    .done(function(msg )
    {
        drawing_board_loading['drawing_board_'+board_id] = false;
    });
}

function initPaperJS() {
    if(!paperjs_init_done) {
        console.log("Called");
        var first_canvas = null;
        $("canvas[id^=drawing_board]").each(function(i){
            if(first_canvas == null){
                first_canvas = $(this);
            }
            paper_scope.setup($(this).prop("id"));
            initial_data_loaded[parseInt($(this).prop("id").replace("drawing_board_",""))] = false;
            drawing_board_loading[parseInt($(this).prop("id").replace("drawing_board_",""))] = false;
        });

        if(first_canvas != null){
            active_board_id = first_canvas.prop("id");
            var first_canvas = paper_scope.View._viewsById[first_canvas.prop("id")];
            first_canvas._project.activate();

            var b_id = parseInt(active_board_id.replace("drawing_board_",""));
            if(initial_data_loaded.hasOwnProperty(b_id)) {
                if(!initial_data_loaded[b_id]) {
                    load_drawingboard_data(b_id);
                    var items = paper_scope.project.activeLayer.getItems({ name: "selection_rectangle" });
                    initial_data_loaded[b_id] = true;
                }
            }
            else {
                load_drawingboard_data(b_id);
                initial_data_loaded[b_id] = true;
            }

            paperjs_init_done = true;
            if(paperjs_init_timer) {
                clearInterval(paperjs_init_timer);
            }
        }
    }

}


window.onload = function() {
    //console.log("Window loaded!");
    paperjs_init_timer = setInterval(initPaperJS,1000);
}