var stream_events = {
    "mouse_down": "mousedown",
    "mouse_move": "mousemove",
    "mouse_drag": "mousedrag",
    "mouse_up":"mouseup",
    "key_down": "keydown",
    "key_up": "keyup"
}

//stream_object will be in the follwoing format.
/*
* stream_object = {
*     event_name: on_mouse_down,
*     lesson_id: 1,
*     whiteboard_id: 1,
*     target_users: [1,2],
*     drawing_info: {
*         start_point: Point,
*         points: [],
*         tool
*     }
*     canvas_id: 1,
*     drawing_state: start/end
* }
* */

var paperboard_streamer = {
    send_stream : function(stream_object) {  //stream_object will have an member named event_name
        if(window.socket != undefined) {
            if(window.screensharing) {
                window.socket.emit("send_stream",stream_object);
            }
        }
        else {
            console.log("Socket connection required.");
        }
    },
    receive_stream : function(stream_object) {  //stream_object will have an member named event_name
        var event_name = stream_object["event_name"];
        if(event_name == "mousedown") {
            paperboard_streamer.on_mouse_down(stream_object);
        }
        else if(event_name == "mousemove") {
            paperboard_streamer.on_mouse_move(stream_object);
        }
        else if(event_name == "mousedrag") {
            paperboard_streamer.on_mouse_drag(stream_object);
        }
        else if(event_name == "mouseup") {
            paperboard_streamer.on_mouse_up(stream_object);
        }
        else if(event_name == "keydown") {
            paperboard_streamer.on_key_down(stream_object);
        }
        else if(event_name == "keyup") {
            paperboard_streamer.on_key_up(stream_object);
        }
        else if(event_name == "clear_board") {
            paperboard_streamer.on_clear_board(stream_object);
        }
        else if(event_name == "update_background") {
            paperboard_streamer.on_update_background(stream_object);
        }
        else if(event_name == "draw_text") {
            paperboard_streamer.on_text_point_draw(stream_object);
        }
        else if(event_name == "item_delete") {
            paperboard_streamer.on_item_delete(stream_object);
        }
        else if(event_name == "item_scaling") {
            paperboard_streamer.on_scale_item(stream_object);
        }
        else if(event_name == "item_rotate") {
            paperboard_streamer.on_rotate_item(stream_object);
        }
        else if(event_name == "undo_redo") {
            paperboard_streamer.on_undo_redo_item(stream_object);
        }
    },
    on_mouse_down : function(stream_object) {  //stream_object will have an member named event_name
        _onMouseDown(stream_object);
    },
    on_mouse_move : function(stream_object) {  //stream_object will have an member named event_name
        _onMouseMove(stream_object);
    },
    on_mouse_drag : function(stream_object) {  //stream_object will have an member named event_name
        _onMouseDrag(stream_object);
    },
    on_mouse_up : function(stream_object) {  //stream_object will have an member named event_name
        _onMouseUp(stream_object);
    },
    on_key_down : function(stream_object) {  //stream_object will have an member named event_name
        _onKeyDown(stream_object);
    },
    on_key_up: function(stream_object) {  //stream_object will have an member named event_name
        _onKeyUp(stream_object);
    },
    on_clear_board: function(stream_object) {
        _onClearBoard(stream_object);
    },
    on_update_background: function(stream_object) {
        _onUpdateBackground(stream_object);
    },
    on_text_point_draw: function(stream_object) {
        _onTextPointUpdate(stream_object);
    },
    on_item_delete: function(stream_object) {
        _onItemDelete(stream_object);
    },
    on_scale_item: function(stream_object) {
        _onItemScale(stream_object);
    },
    on_rotate_item: function(stream_object) {
        _onItemRotate(stream_object);
    },
    on_undo_redo_item: function(stream_object) {
        _onItemUndoRedo(stream_object);
    }
}

window.paperboard_streamer = paperboard_streamer;

function toggle_user_visibility(user_id, is_visible) {
    if(is_visible == true) {
        $("#user_"+user_id).fadeIn('slow');
    }
    else {
        $("#user_"+user_id).fadeOut('slow');
    }
}

//Register stream events
if(window.socket != undefined){
    window.socket.on("receive_stream", paperboard_streamer.receive_stream);

    window.socket.on("ON_DRAWINGBOARD_ADDED", function(data)
    {
        //console.log(data);
        //var data = jQuery.parseJSON(data);
        var closable = false;
        if(data.closable == "true") {
            closable = true;
        }
        else {
            closable = false;
        }
        $('#id_canvas_tab').tabs('add',{
            title:data.title,
            id: data.id,
            content: render_drawing_board_tab("drawing_board_"+data.title),
            closable:closable
        });

        paper_scope.setup("drawing_board_"+data.title);

    });

    window.socket.on("ON_TEXT_PAD_ADDED", function(data)
    {
        var closable = false;
        if(data.closable == "true") {
            closable = true;
        }
        else {
            closable = false;
        }

        $('#tt').tabs('add',{
              title:data.title,
              id: data.id,
              content:'',
              closable:closable
        });

        AddNewPad(data.title,data.pad_id);
        window.text_tabs_list[data.title] = true;

    });

    window.socket.on("ON_CODE_PAD_ADDED", function(data)
    {
        console.log(data);
        var closable = false;
        if(data.closable == "true") {
            closable = true;
        }
        else {
            closable = false;
        }

        var pad_language = '<input type="hidden" id="id_hidden_codepad_lan_'+ data.title +'" value="'+ data.language +'">';
        $("#id_hidden_code_pad_info").append(pad_language);

        $('#ctt').tabs('add',{
              title:data.title,
              id: data.id,
              content:'',
              closable:closable
        });

        AddNewCodePad(data.title,data.pad_id,data.language, data.pad_id);
        window.code_tabs_list[data.title] = true;

    });

    window.socket.on("ON_CODE_PAD_LAN_CHANGED", function(data)
    {
        $("#id_hidden_codepad_lan_"+data.title).val(data.language);
        var pp = $('#ctt').tabs('getSelected');
        var tab = pp.panel('options').tab;    // the corresponding tab object
        var tab_title = $(tab).find(".tabs-title").text();
        if(tab_title == ""+data.title) {
            $("#id_codepad_language_changer").val(data.language);
            var src = $("#id_codepad_tab_"+data.title).find("iframe").prop("src");
            var frame_id = $("#id_codepad_tab_"+data.title).find("iframe").prop("id");
            $("#"+frame_id)[0].src = src;
        }
    });

    window.socket.on("whiteboard_user_presence_ask_received", function(data)
    {
        toggle_user_visibility(data.user_id, true);
        window.socket.emit("notify_whiteboard_presence",{ receiver: data.user_id, sender_id: window.champ_user_id });
    });

    window.socket.on("whiteboard_user_presence_received", function(data)
    {
        var user_id = data.user_id;
        toggle_user_visibility(data.user_id, true);
    });

    window.socket.on("notify_whiteboard_exit_received", function(data)
    {
        var user_id = data.user_id;
        toggle_user_visibility(data.user_id, false);
    });

    window.socket.on("any_other_whiteboard_tab_open", function(data)
    {
        window.socket.emit("open_whiteboard_found",data);
    });

};