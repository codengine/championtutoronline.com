


function _onMouseDown(stream_object) {
    var current_view_id = $(paper_scope.view.element).prop("id");
    var receive_stream_view_id = stream_object.active_board_id;
    if(receive_stream_view_id == current_view_id) {

        if(stream_object.selected_tool == drawing_tools.drawing_tool_selection.name) {
//            var object_id = stream_object.object_id;
//            var items = paper_scope.project.activeLayer.getItems({ _id: object_id });
//            for(var c = 0 ;c < items.length ; c++) {
//                items[c].selected = true;
//                items[c].position = new Point(stream_object.current_point.x,stream_object.current_point.y);
//            }
        }
        else
        {
            //Draw things.
            var mouse_icon_text_height = 75;
            var mouse_icon_text_width_half = 40;
            var current_view_offset_top = $("#"+active_board_id).offset().top - mouse_icon_text_height;
            $("#user_"+stream_object.sender).css({
                "left": stream_object.current_point.x - mouse_icon_text_width_half,
                "top": stream_object.current_point.y + current_view_offset_top
            });

            //By default path will be segment.
            current_drawing_paths[stream_object.sender] = new Path();
            current_drawing_paths[stream_object.sender]._id = stream_object.object_id;
            _path = current_drawing_paths[stream_object.sender];
            current_drawing_paths[stream_object.sender].strokeCap = "round";
            current_drawing_paths[stream_object.sender].strokeColor = stream_object.stroke_color;
            if(stream_object.selected_tool == drawing_tools.pencil_tool_size_5.name) {
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
                _path.add(new paper_scope.Point(stream_object.current_point.x,stream_object.current_point.y));
            }
            else if(stream_object.selected_tool == drawing_tools.pencil_tool_size_10.name) {
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
                _path.add(new paper_scope.Point(stream_object.current_point.x,stream_object.current_point.y));
            }
            else if(stream_object.selected_tool == drawing_tools.pencil_tool_size_15.name) {
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
                _path.add(new paper_scope.Point(stream_object.current_point.x,stream_object.current_point.y));
            }
            else if(stream_object.selected_tool == drawing_tools.pencil_tool_size_20.name) {
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
                _path.add(new paper_scope.Point(stream_object.current_point.x,stream_object.current_point.y));
            }
            //Draw Brush
            else if(stream_object.selected_tool == drawing_tools.brush_tool_size_5.name) {
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
                _path.add(new paper_scope.Point(stream_object.current_point.x,stream_object.current_point.y));
            }
            else if(stream_object.selected_tool == drawing_tools.brush_tool_size_10.name) {
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
                _path.add(new paper_scope.Point(stream_object.current_point.x,stream_object.current_point.y));
            }
            else if(stream_object.selected_tool == drawing_tools.brush_tool_size_15.name) {
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
                _path.add(new paper_scope.Point(stream_object.current_point.x,stream_object.current_point.y));
            }
            else if(stream_object.selected_tool == drawing_tools.brush_tool_size_20.name) {
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
                _path.add(new paper_scope.Point(stream_object.current_point.x,stream_object.current_point.y));
            }
            //Draw Eraser
            else if(stream_object.selected_tool == drawing_tools.eraser_tool_size_5.name) {
                _path.strokeColor = 'white';
                _path.strokeWidth = stream_object.stroke_size;
                _path.add(new paper_scope.Point(stream_object.current_point.x,stream_object.current_point.y));
            }
            else if(stream_object.selected_tool == drawing_tools.eraser_tool_size_10.name) {
                _path.strokeColor = 'white';
                _path.strokeWidth = stream_object.stroke_size;
                _path.add(new paper_scope.Point(stream_object.current_point.x,stream_object.current_point.y));
            }
            else if(stream_object.selected_tool == drawing_tools.eraser_tool_size_15.name) {
                _path.strokeColor = 'white';
                _path.strokeWidth = stream_object.stroke_size;
                _path.add(new paper_scope.Point(stream_object.current_point.x,stream_object.current_point.y));
            }
            else if(stream_object.selected_tool == drawing_tools.eraser_tool_size_20.name) {
                _path.strokeColor = 'white';
                _path.strokeWidth = stream_object.stroke_size;
                _path.add(new paper_scope.Point(stream_object.current_point.x,stream_object.current_point.y));
            }
            //Draw Circle.
            else if(stream_object.selected_tool == drawing_tools.drawing_tool_circle_5.name){
                current_drawing_paths[stream_object.sender] = new paper_scope.Path.Circle(new paper_scope.Point(stream_object.current_point.x,stream_object.current_point.y),0);
                _path = current_drawing_paths[stream_object.sender];
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
            }
            //Check rectangle tool.
            else if(stream_object.selected_tool == drawing_tools.drawing_tool_rectangle_5.name){
                var from = new Point(stream_object.current_point.x, stream_object.current_point.y);
                var to = new Point(stream_object.current_point.x, stream_object.current_point.y);
                current_drawing_paths[stream_object.sender] = new paper_scope.Path.Rectangle(from,to);
                _path = current_drawing_paths[stream_object.sender];
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
            }
            //Check line tool.
            else if(stream_object.selected_tool == drawing_tools.drawing_tool_line_5.name){
                var from = new Point(stream_object.current_point.x, stream_object.current_point.y);
                var to = new Point(stream_object.current_point.x, stream_object.current_point.y);
                current_drawing_paths[stream_object.sender] = new Path.Line(from,to);
                _path = current_drawing_paths[stream_object.sender];
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
            }
            //Check if line with single arrow.
            else if(stream_object.selected_tool == drawing_tools.drawing_tool_arrow_single.name){
                current_drawing_paths[stream_object.sender] = new paper_scope.Shape.ArrowLine(stream_object.current_point.x, stream_object.current_point.y, stream_object.current_point.x, stream_object.current_point.y,false);
                _path = current_drawing_paths[stream_object.sender];
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
            }
            //Check if line with double arrow.
            else if(stream_object.selected_tool == drawing_tools.drawing_tool_arrow_double.name){
                current_drawing_paths[stream_object.sender] = new paper_scope.Shape.ArrowLine(stream_object.current_point.x, stream_object.current_point.y, stream_object.current_point.x, stream_object.current_point.y,true);
                _path = current_drawing_paths[stream_object.sender];
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
            }
            //Check if right triangle.
            else if(stream_object.selected_tool == drawing_tools.drawing_tool_right_triangle.name){
                current_drawing_paths[stream_object.sender] = new paper_scope.Shape.drawRightTriangle(stream_object.current_point.x, stream_object.current_point.y, stream_object.current_point.x, stream_object.current_point.y);
                _path = current_drawing_paths[stream_object.sender];
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
            }
            //Check if Hexagon.
            else if(stream_object.selected_tool == drawing_tools.drawing_tool_poly_6_size_5.name){
                current_drawing_paths[stream_object.sender] = new Path.RegularPolygon(new Point(stream_object.current_point.x, stream_object.current_point.y), 0, 0);
                _path = current_drawing_paths[stream_object.sender];
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
            }
            //Check if Pentagon..
            else if(stream_object.selected_tool == drawing_tools.drawing_tool_poly_5_size_5.name){
                current_drawing_paths[stream_object.sender] = new Path.RegularPolygon(new Point(stream_object.current_point.x, stream_object.current_point.y), 0, 0);
                _path = current_drawing_paths[stream_object.sender];
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
            }
            //Check if Triangle.
            else if(stream_object.selected_tool == drawing_tools.drawing_tool_triangle_5.name){
                current_drawing_paths[stream_object.sender] = new Path.RegularPolygon(new Point(stream_object.current_point.x, stream_object.current_point.y), 0, 0);
                _path = current_drawing_paths[stream_object.sender];
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
            }
        }
        paper_scope.view.draw();
    }
    else {
        if(!offline_paths.hasOwnProperty(stream_object.sender)) {
            offline_paths[stream_object.sender] = {
                tool_name: stream_object.selected_tool,
                data: [],
                stroke_color: stream_object.stroke_color,
                stroke_size: stream_object.stroke_size,
                _id: stream_object.object_id
            };
        }

        offline_paths[stream_object.sender].tool_name = stream_object.selected_tool;
        offline_paths[stream_object.sender].stroke_color = stream_object.stroke_color;
        offline_paths[stream_object.sender].stroke_size = stream_object.stroke_size;

        if(stream_object.selected_tool == drawing_tools.pencil_tool_size_5.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        else if(stream_object.selected_tool == drawing_tools.pencil_tool_size_10.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        else if(stream_object.selected_tool == drawing_tools.pencil_tool_size_15.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        else if(stream_object.selected_tool == drawing_tools.pencil_tool_size_20.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Draw Brush
        else if(stream_object.selected_tool == drawing_tools.brush_tool_size_5.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        else if(stream_object.selected_tool == drawing_tools.brush_tool_size_10.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        else if(stream_object.selected_tool == drawing_tools.brush_tool_size_15.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        else if(stream_object.selected_tool == drawing_tools.brush_tool_size_20.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Draw Eraser
        else if(stream_object.selected_tool == drawing_tools.eraser_tool_size_5.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        else if(stream_object.selected_tool == drawing_tools.eraser_tool_size_10.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        else if(stream_object.selected_tool == drawing_tools.eraser_tool_size_15.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        else if(stream_object.selected_tool == drawing_tools.eraser_tool_size_20.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Draw Circle.
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_circle_5.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Draw Rectangle.
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_rectangle_5.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Check line tool.
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_line_5.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Check if line with single arrow.
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_arrow_single.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Check if line with double arrow.
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_arrow_double.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Check if right triangle.
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_right_triangle.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Check if Hexagon.
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_poly_6_size_5.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Check if Pentagon..
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_poly_5_size_5.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Check if Triangle.
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_triangle_5.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Check if Triangle.
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_triangle_5.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_cartesian.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_cartesian_marked.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }

    }
}

function _onMouseMove(stream_object) {

}

function _onMouseDrag(stream_object) {
    var current_view_id = $(paper_scope.view.element).prop("id");
    var receive_stream_view_id = stream_object.active_board_id;
    if(receive_stream_view_id == current_view_id) {

        if(stream_object.selected_tool == drawing_tools.drawing_tool_selection.name) {

            var mouse_icon_text_height = 75;
            var mouse_icon_text_width_half = 40;
            var current_view_offset_top = $("#"+active_board_id).offset().top - mouse_icon_text_height;
            $("#user_"+stream_object.sender).css({
                "left": stream_object.current_point.x - mouse_icon_text_width_half,
                "top": stream_object.current_point.y + current_view_offset_top
            });

            var object_id = stream_object.object_id;
            var items = paper_scope.project.activeLayer.getItems({ _id: object_id });
            for(var c = 0 ;c < items.length ; c++) {
                items[c].selected = true;
                items[c].position = new Point(stream_object.current_point.x,stream_object.current_point.y);
                paper_scope.view.draw();
            }
        }
        else {
            //Draw things.
            var mouse_icon_text_height = 75;
            var mouse_icon_text_width_half = 40;
            var current_view_offset_top = $("#"+active_board_id).offset().top - mouse_icon_text_height;
            $("#user_"+stream_object.sender).css({
                "left": stream_object.current_point.x - mouse_icon_text_width_half,
                "top": stream_object.current_point.y + current_view_offset_top
            });

            var _path = current_drawing_paths[stream_object.sender];

            if(stream_object.selected_tool == drawing_tools.pencil_tool_size_5.name) {
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
                _path.add(new paper_scope.Point(stream_object.current_point.x,stream_object.current_point.y));
            }
            else if(stream_object.selected_tool == drawing_tools.pencil_tool_size_10.name) {
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
                _path.add(new paper_scope.Point(stream_object.current_point.x,stream_object.current_point.y));
            }
            else if(stream_object.selected_tool == drawing_tools.pencil_tool_size_15.name) {
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
                _path.add(new paper_scope.Point(stream_object.current_point.x,stream_object.current_point.y));
            }
            else if(stream_object.selected_tool == drawing_tools.pencil_tool_size_20.name) {
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
                _path.add(new paper_scope.Point(stream_object.current_point.x,stream_object.current_point.y));
            }
            //Draw Brush
            else if(stream_object.selected_tool == drawing_tools.brush_tool_size_5.name) {
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
                _path.add(new paper_scope.Point(stream_object.current_point.x,stream_object.current_point.y));
            }
            else if(stream_object.selected_tool == drawing_tools.brush_tool_size_10.name) {
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
                _path.add(new paper_scope.Point(stream_object.current_point.x,stream_object.current_point.y));
            }
            else if(stream_object.selected_tool == drawing_tools.brush_tool_size_15.name) {
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
                _path.add(new paper_scope.Point(stream_object.current_point.x,stream_object.current_point.y));
            }
            else if(stream_object.selected_tool == drawing_tools.brush_tool_size_20.name) {
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
                _path.add(new paper_scope.Point(stream_object.current_point.x,stream_object.current_point.y));
            }
            //Draw Eraser
            else if(stream_object.selected_tool == drawing_tools.eraser_tool_size_5.name) {
                _path.strokeColor = 'white';
                _path.strokeWidth = stream_object.stroke_size;
                _path.add(new paper_scope.Point(stream_object.current_point.x,stream_object.current_point.y));
            }
            else if(stream_object.selected_tool == drawing_tools.eraser_tool_size_10.name) {
                _path.strokeColor = 'white';
                _path.strokeWidth = stream_object.stroke_size;
                _path.add(new paper_scope.Point(stream_object.current_point.x,stream_object.current_point.y));
            }
            else if(stream_object.selected_tool == drawing_tools.eraser_tool_size_15.name) {
                _path.strokeColor = 'white';
                _path.strokeWidth = stream_object.stroke_size;
                _path.add(new paper_scope.Point(stream_object.current_point.x,stream_object.current_point.y));
            }
            else if(stream_object.selected_tool == drawing_tools.eraser_tool_size_20.name) {
                _path.strokeColor = 'white';
                _path.strokeWidth = stream_object.stroke_size;
                _path.add(new paper_scope.Point(stream_object.current_point.x,stream_object.current_point.y));
            }
            else if(stream_object.selected_tool == drawing_tools.drawing_tool_circle_5.name){
                _path.removeSegments();
                _path.remove();
                var radius = euclidian_distance(stream_object.down_point,stream_object.current_point);
                current_drawing_paths[stream_object.sender] = new paper_scope.Path.Circle(new paper_scope.Point(stream_object.down_point.x, stream_object.down_point.y), radius);
                _path = current_drawing_paths[stream_object.sender];
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
            }
            //Draw Rectangle.
            else if(stream_object.selected_tool == drawing_tools.drawing_tool_rectangle_5.name){
                _path.remove();
                var from = new Point(stream_object.down_point.x, stream_object.down_point.y);
                var to = new Point(stream_object.current_point.x, stream_object.current_point.y);
                current_drawing_paths[stream_object.sender] = new paper_scope.Path.Rectangle(from,to);
                _path = current_drawing_paths[stream_object.sender];
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
            }
            //Check line tool.
            else if(stream_object.selected_tool == drawing_tools.drawing_tool_line_5.name){
                _path.remove();
                var from = new Point(stream_object.down_point.x, stream_object.down_point.y);
                var to = new Point(stream_object.current_point.x, stream_object.current_point.y);
                current_drawing_paths[stream_object.sender] = new Path.Line(from,to);
                _path = current_drawing_paths[stream_object.sender];
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
            }
            //Check if line with single arrow.
            else if(stream_object.selected_tool == drawing_tools.drawing_tool_arrow_single.name){
                //current_drawing_paths[stream_object.sender].remove();
                if(_path != undefined) {
                    _path.remove();
                }
                current_drawing_paths[stream_object.sender] = new paper_scope.Shape.ArrowLine(stream_object.down_point.x, stream_object.down_point.y, stream_object.current_point.x, stream_object.current_point.y,false);
                _path = current_drawing_paths[stream_object.sender];
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
            }
            //Check if line with double arrow.
            else if(stream_object.selected_tool == drawing_tools.drawing_tool_arrow_double.name){
                if(_path != undefined) {
                    _path.remove();
                }
                current_drawing_paths[stream_object.sender] = new paper_scope.Shape.ArrowLine(stream_object.down_point.x, stream_object.down_point.y, stream_object.current_point.x, stream_object.current_point.y,true);
                _path = current_drawing_paths[stream_object.sender];
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
            }
            //Check if right triangle.
            else if(stream_object.selected_tool == drawing_tools.drawing_tool_right_triangle.name){
                if(_path != undefined) {
                    _path.remove();
                }
                current_drawing_paths[stream_object.sender] = new paper_scope.Shape.drawRightTriangle(stream_object.down_point.x, stream_object.down_point.y, stream_object.current_point.x, stream_object.current_point.y);
                _path = current_drawing_paths[stream_object.sender];
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
            }
            //Check if Hexagon.
            else if(stream_object.selected_tool == drawing_tools.drawing_tool_poly_6_size_5.name){
                if(_path != undefined) {
                    _path.remove();
                }
                var radius = euclidian_distance(new Point(stream_object.down_point.x, stream_object.down_point.y),new Point(stream_object.current_point.x, stream_object.current_point.y));
                current_drawing_paths[stream_object.sender] = new Path.RegularPolygon(new Point(stream_object.down_point.x, stream_object.down_point.y), 6, radius);
                _path = current_drawing_paths[stream_object.sender];
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
            }
            //Check if Pentagon..
            else if(stream_object.selected_tool == drawing_tools.drawing_tool_poly_5_size_5.name){
                if(_path != undefined) {
                    _path.remove();
                }
                var radius = euclidian_distance(new Point(stream_object.down_point.x, stream_object.down_point.y),new Point(stream_object.current_point.x, stream_object.current_point.y));
                current_drawing_paths[stream_object.sender] = new Path.RegularPolygon(new Point(stream_object.down_point.x, stream_object.down_point.y), 5, radius);
                _path = current_drawing_paths[stream_object.sender];
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
            }
            //Check if Triangle.
            else if(stream_object.selected_tool == drawing_tools.drawing_tool_triangle_5.name){
                if(_path != undefined) {
                    _path.remove();
                }
                var radius = euclidian_distance(new Point(stream_object.down_point.x, stream_object.down_point.y),new Point(stream_object.current_point.x, stream_object.current_point.y));
                current_drawing_paths[stream_object.sender] = new Path.RegularPolygon(new Point(stream_object.down_point.x, stream_object.down_point.y), 3, radius);
                _path = current_drawing_paths[stream_object.sender];
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
            }
            //Check if Cartesian Axis.
            else if(stream_object.selected_tool == drawing_tools.drawing_tool_cartesian.name){
                if(typeof _path != "undefined") {
                    _path.remove();
                }
                _path = new paper_scope.Shape.draw_cartesian(stream_object.down_point.x, stream_object.down_point.y,stream_object.current_point.x, stream_object.current_point.y,true,false,false);
                current_drawing_paths[stream_object.sender] = _path;
                _path = current_drawing_paths[stream_object.sender];
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;

            }
            else if(stream_object.selected_tool == drawing_tools.drawing_tool_cartesian_marked.name) {
                if(typeof _path != "undefined") {
                    _path.remove();
                }
                _path = new paper_scope.Shape.draw_cartesian(stream_object.down_point.x, stream_object.down_point.y,stream_object.current_point.x, stream_object.current_point.y,true,true,false);
                current_drawing_paths[stream_object.sender] = _path;
                _path = current_drawing_paths[stream_object.sender];
                _path.strokeColor = stream_object.stroke_color;
                _path.strokeWidth = stream_object.stroke_size;
            }
            _path._id = stream_object.object_id;
            paper_scope.view.draw();
        }
    }
    else {
        if(!offline_paths.hasOwnProperty(stream_object.sender)) {
            offline_paths[stream_object.sender] = {
                tool_name: stream_object.selected_tool,
                data: [],
                stroke_color: stream_object.stroke_color,
                stroke_size: stream_object.stroke_size,
                _id: stream_object.object_id
            };
        }

        offline_paths[stream_object.sender].tool_name = stream_object.selected_tool;
        offline_paths[stream_object.sender].stroke_color = stream_object.stroke_color;
        offline_paths[stream_object.sender].stroke_size = stream_object.stroke_size;

        if(stream_object.selected_tool == drawing_tools.pencil_tool_size_5.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        else if(stream_object.selected_tool == drawing_tools.pencil_tool_size_10.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        else if(stream_object.selected_tool == drawing_tools.pencil_tool_size_15.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        else if(stream_object.selected_tool == drawing_tools.pencil_tool_size_20.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Draw Brush
        else if(stream_object.selected_tool == drawing_tools.brush_tool_size_5.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        else if(stream_object.selected_tool == drawing_tools.brush_tool_size_10.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        else if(stream_object.selected_tool == drawing_tools.brush_tool_size_15.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        else if(stream_object.selected_tool == drawing_tools.brush_tool_size_20.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Draw Eraser
        else if(stream_object.selected_tool == drawing_tools.eraser_tool_size_5.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        else if(stream_object.selected_tool == drawing_tools.eraser_tool_size_10.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        else if(stream_object.selected_tool == drawing_tools.eraser_tool_size_15.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        else if(stream_object.selected_tool == drawing_tools.eraser_tool_size_20.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Draw Circle.
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_circle_5.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Draw Rectangle.
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_rectangle_5.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Check line tool.
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_line_5.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Check if line with single arrow.
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_arrow_single.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Check if line with double arrow.
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_arrow_double.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Check if right triangle.
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_right_triangle.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Check if Hexagon.
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_poly_6_size_5.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Check if Pentagon..
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_poly_5_size_5.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Check if Triangle.
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_triangle_5.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Check if Cartesian Axis.
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_cartesian.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);

        }
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_cartesian_marked.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Check if Triangle.
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_triangle_5.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_cartesian_marked.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
    }
}

function _onMouseUp(stream_object) {
    var current_view_id = $(paper_scope.view.element).prop("id");
    var receive_stream_view_id = stream_object.active_board_id;
    if(receive_stream_view_id == current_view_id) {

        if(stream_object.selected_tool == drawing_tools.drawing_tool_selection.name) {

            var mouse_icon_text_height = 75;
            var mouse_icon_text_width_half = 40;
            var current_view_offset_top = $("#"+active_board_id).offset().top - mouse_icon_text_height;
//            $("#user_"+stream_object.sender).css({
//                "left": stream_object.current_point.x - mouse_icon_text_width_half,
//                "top": stream_object.current_point.y + current_view_offset_top
//            });

            var object_id = stream_object.object_id;
            var items = paper_scope.project.activeLayer.getItems({ _id: object_id });
            for(var c = 0 ;c < items.length ; c++) {
                items[c].selected = false;
                //items[c].position = new Point(stream_object.current_point.x,stream_object.current_point.y);
                paper_scope.view.draw();
            }
        }
        else
        {
            //Draw things.
    //        var mouse_icon_text_height = 35;
    //        var mouse_icon_text_width_half = 40;
    //        var current_view_offset_top = $("#"+active_board_id).offset().top - mouse_icon_text_height;
    //        $("#user_"+stream_object.sender).css({
    //            "left": window.cursor_position[stream_object.sender].left,
    //            "top": window.cursor_position[stream_object.sender].top
    //        });

            //console.log(current_drawing_paths[stream_object.sender]._id);

            $("#user_"+stream_object.sender).animate( {'top': window.cursor_position[stream_object.sender].top, 'left':window.cursor_position[stream_object.sender].left}, 'slow', function(){
               $("#user_"+stream_object.sender).css({
                    "left": window.cursor_position[stream_object.sender].left,
                    "top": window.cursor_position[stream_object.sender].top
                });
            });

            if(stream_object.selected_tool == drawing_tools.pencil_tool_size_5.name) {
                current_drawing_paths[stream_object.sender] = null;
            }
            else if(stream_object.selected_tool == drawing_tools.pencil_tool_size_10.name) {
                current_drawing_paths[stream_object.sender] = null;
            }
            else if(stream_object.selected_tool == drawing_tools.pencil_tool_size_15.name) {
                current_drawing_paths[stream_object.sender] = null;
            }
            else if(stream_object.selected_tool == drawing_tools.pencil_tool_size_20.name) {
                current_drawing_paths[stream_object.sender] = null;
            }
            //Draw Brush
            else if(stream_object.selected_tool == drawing_tools.brush_tool_size_5.name) {
                current_drawing_paths[stream_object.sender] = null;
            }
            else if(stream_object.selected_tool == drawing_tools.brush_tool_size_10.name) {
                current_drawing_paths[stream_object.sender] = null;
            }
            else if(stream_object.selected_tool == drawing_tools.brush_tool_size_15.name) {
                current_drawing_paths[stream_object.sender] = null;
            }
            else if(stream_object.selected_tool == drawing_tools.brush_tool_size_20.name) {
                current_drawing_paths[stream_object.sender] = null;
            }
            //Draw Eraser
            else if(stream_object.selected_tool == drawing_tools.eraser_tool_size_5.name) {
                current_drawing_paths[stream_object.sender] = null;
            }
            else if(stream_object.selected_tool == drawing_tools.eraser_tool_size_10.name) {
                current_drawing_paths[stream_object.sender] = null;
            }
            else if(stream_object.selected_tool == drawing_tools.eraser_tool_size_15.name) {
                current_drawing_paths[stream_object.sender] = null;
            }
            else if(stream_object.selected_tool == drawing_tools.eraser_tool_size_20.name) {
                current_drawing_paths[stream_object.sender] = null;
            }
            //Draw Circle.
            else if(stream_object.selected_tool == drawing_tools.drawing_tool_circle_5.name) {
                current_drawing_paths[stream_object.sender] = null;
            }
            //Draw Rectangle.
            else if(stream_object.selected_tool == drawing_tools.drawing_tool_rectangle_5.name){
                current_drawing_paths[stream_object.sender] = null;
            }
            //Check line tool.
            else if(stream_object.selected_tool == drawing_tools.drawing_tool_line_5.name){
                current_drawing_paths[stream_object.sender] = null;
            }
            //Check if line with single arrow.
            else if(stream_object.selected_tool == drawing_tools.drawing_tool_arrow_single.name){
                current_drawing_paths[stream_object.sender] = null;
            }
            //Check if line with double arrow.
            else if(stream_object.selected_tool == drawing_tools.drawing_tool_arrow_double.name){
                current_drawing_paths[stream_object.sender] = null;
            }
            //Check if right triangle.
            else if(stream_object.selected_tool == drawing_tools.drawing_tool_right_triangle.name){
                current_drawing_paths[stream_object.sender] = null;
            }
            //Check if Hexagon.
            else if(stream_object.selected_tool == drawing_tools.drawing_tool_poly_6_size_5.name){
                current_drawing_paths[stream_object.sender] = null;
            }
            //Check if Pentagon..
            else if(stream_object.selected_tool == drawing_tools.drawing_tool_poly_5_size_5.name){
                current_drawing_paths[stream_object.sender] = null;
            }
            //Check if Triangle.
            else if(stream_object.selected_tool == drawing_tools.drawing_tool_triangle_5.name){
                current_drawing_paths[stream_object.sender] = null;
            }
            //Check if Cartesian Axis.
            else if(stream_object.selected_tool == drawing_tools.drawing_tool_cartesian.name){

                axis_object = null;
                axis_object_marked = null;
                current_drawing_paths[stream_object.sender] = null;

            }
            else if(stream_object.selected_tool == drawing_tools.drawing_tool_cartesian_marked.name) {
                axis_object = null;
                axis_object_marked = null;
                current_drawing_paths[stream_object.sender] = null;
            }
        }
        paper_scope.view.draw();
    }
    else {

        if(!offline_paths.hasOwnProperty(stream_object.sender)) {
            offline_paths[stream_object.sender] = {
                tool_name: stream_object.selected_tool,
                data: [],
                stroke_color: stream_object.stroke_color,
                stroke_size: stream_object.stroke_size
            };
        }

        offline_paths[stream_object.sender].tool_name = stream_object.selected_tool;
        offline_paths[stream_object.sender].stroke_color = stream_object.stroke_color;
        offline_paths[stream_object.sender].stroke_size = stream_object.stroke_size;

        if(stream_object.selected_tool == drawing_tools.pencil_tool_size_5.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        else if(stream_object.selected_tool == drawing_tools.pencil_tool_size_10.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        else if(stream_object.selected_tool == drawing_tools.pencil_tool_size_15.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        else if(stream_object.selected_tool == drawing_tools.pencil_tool_size_20.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Draw Brush
        else if(stream_object.selected_tool == drawing_tools.brush_tool_size_5.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        else if(stream_object.selected_tool == drawing_tools.brush_tool_size_10.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        else if(stream_object.selected_tool == drawing_tools.brush_tool_size_15.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        else if(stream_object.selected_tool == drawing_tools.brush_tool_size_20.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Draw Eraser
        else if(stream_object.selected_tool == drawing_tools.eraser_tool_size_5.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        else if(stream_object.selected_tool == drawing_tools.eraser_tool_size_10.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        else if(stream_object.selected_tool == drawing_tools.eraser_tool_size_15.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        else if(stream_object.selected_tool == drawing_tools.eraser_tool_size_20.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Draw Circle.
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_circle_5.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Draw Rectangle.
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_rectangle_5.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Check line tool.
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_line_5.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Check if line with single arrow.
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_arrow_single.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Check if line with double arrow.
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_arrow_double.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Check if right triangle.
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_right_triangle.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Check if Hexagon.
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_poly_6_size_5.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Check if Pentagon..
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_poly_5_size_5.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Check if Triangle.
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_triangle_5.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Check if Cartesian Axis.
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_cartesian.name){

            offline_paths[stream_object.sender].data.push(stream_object.current_point);

        }
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_cartesian_marked.name) {
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        //Check if Triangle.
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_triangle_5.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_cartesian.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }
        else if(stream_object.selected_tool == drawing_tools.drawing_tool_cartesian_marked.name){
            offline_paths[stream_object.sender].data.push(stream_object.current_point);
        }

        if(!drawingboards.hasOwnProperty(stream_object.active_board_id)) {
            drawingboards[stream_object.active_board_id] = [];
        }
        drawingboards[stream_object.active_board_id].push(offline_paths[stream_object.sender]);

        offline_paths[stream_object.sender] = {
            data: []
        };
    }
}

function _onKeyDown(stream_object) {

}

function _onKeyUp(stream_object) {

}

function _onClearBoard(stream_object) {
    var current_view_id = $(paper_scope.view.element).prop("id");
    var receive_stream_view_id = stream_object.active_board_id;
    if(receive_stream_view_id == current_view_id) {
        paper_scope.project.activeLayer.removeChildren();
        paper_scope.view.draw();
    }
    else {
        clear_boards[receive_stream_view_id] = true;
    }
}

function _onUpdateBackground(stream_object) {
    var current_view_id = $(paper_scope.view.element).prop("id");
    var receive_stream_view_id = stream_object.active_board_id;
    if(receive_stream_view_id == current_view_id) {
        load_drawingboard_background(current_view_id);
    }
    else {
        offline_backgrounds[receive_stream_view_id] = true;
    }
}

function _onTextPointUpdate(stream_object) {

    var text_val = stream_object.text;
    var text_size = stream_object.size;
    _path = new Path();
    var text = new PointText(new Point(stream_object.down_point.left, stream_object.down_point.top));
    text.justification = 'left';
    text.fontFamily = 'Arial';
    text.fontSize ='20pt';
    text.fillColor = 'black';
    //text.bounds = last_text_element.bounds;
    if(text_val != null && text_val != "") {
        text.wordwrap(text_val,text_size.width/15);
        _path = text;
        paper_scope.view.draw();
    }
}

function _onItemDelete(stream_object) {
    var object_id = stream_object.object_id;
    var items = paper_scope.project.activeLayer.getItems({ _id: object_id });
    for(var c = 0 ;c < items.length ; c++) {
        items[c].remove();
        paper_scope.view.draw();
    }

    $("#user_"+stream_object.sender).animate( {'top': window.cursor_position[stream_object.sender].top, 'left':window.cursor_position[stream_object.sender].left}, 'slow', function(){
       $("#user_"+stream_object.sender).css({
            "left": window.cursor_position[stream_object.sender].left,
            "top": window.cursor_position[stream_object.sender].top
        });
    });

}

function _onItemScale(stream_object) {

    var object_id = stream_object.object_id;
    var items = paper_scope.project.activeLayer.getItems({ _id: object_id });
    ratio = stream_object.ratio;
    for(var c = 0 ;c < items.length ; c++) {
        items[c].scaling = new Point(ratio, ratio);
        paper_scope.view.draw();
    }

    $("#user_"+stream_object.sender).animate( {'top': window.cursor_position[stream_object.sender].top, 'left':window.cursor_position[stream_object.sender].left}, 'slow', function(){
       $("#user_"+stream_object.sender).css({
            "left": window.cursor_position[stream_object.sender].left,
            "top": window.cursor_position[stream_object.sender].top
        });
    });
}

function _onItemRotate(stream_object) {

    var object_id = stream_object.object_id;
    var items = paper_scope.project.activeLayer.getItems({ _id: object_id });
    var rotation = stream_object.rotation;
    for(var c = 0 ;c < items.length ; c++) {
        console.log("Rotate: "+rotation);
        items[c].rotation = rotation;
        paper_scope.view.draw();
    }

    $("#user_"+stream_object.sender).animate( {'top': window.cursor_position[stream_object.sender].top, 'left':window.cursor_position[stream_object.sender].left}, 'slow', function(){
       $("#user_"+stream_object.sender).css({
            "left": window.cursor_position[stream_object.sender].left,
            "top": window.cursor_position[stream_object.sender].top
        });
    });
}

function _onItemUndoRedo(stream_object) {
    if(stream_object.action == "undo") {
        var object_id = stream_object.object_id;
        var items = paper_scope.project.activeLayer.getItems({ _id: object_id });
        console.log("Undo items: "+items);
        for(var c = 0 ;c < items.length ; c++) {
            if(!undo_stacks_parties.hasOwnProperty(stream_object.sender)) {
                undo_stacks_parties[stream_object.sender] = [ items[c] ];
            }
            else {
                undo_stacks_parties[stream_object.sender].push(items[c]);
            }
            items[c].remove();
            paper_scope.view.draw();
        }
    }
    else if(stream_object.action == "redo") {
        var object_id = stream_object.object_id;
        var item = null;
        if(undo_stacks_parties.hasOwnProperty(stream_object.sender)) {
            for(var m = 0 ; m < undo_stacks_parties[stream_object.sender].length ; m++) {
                var _o = undo_stacks_parties[stream_object.sender][m];
                if(_o._id == object_id) {
                    item = _o;
                    break;
                }
            }
        }
        if(item != null) {
            paper_scope.project.activeLayer.addChild(item);
            paper_scope.view.draw();
        }
        else {
            if(typeof stream_object.item != "undefined" && stream_object.item != null) {
                var item = paper_scope.project.activeLayer.importJSON(stream_object.item);
                item._id = object_id;
                console.log("Imported: "+item._id);
                paper_scope.view.draw();
            }
        }
    }
}

