$(document).ready(function(e)
{

    //$(".form-control").tooltip();
    $(".email").focusin(function()
    {
        $(this).parent().find(".error").hide();
    })
    .focusout(function()
    {
        var email = $(this).val();
        if($.trim(email) == "")
        {
            $(this).parent().find(".error").html("A valid email address is required.");
            $(this).parent().find(".error").show();
        }
        else
        {
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            if( !emailReg.test(email) )
            {
                $(this).parent().find(".error").html("A valid email address is required.");
                $(this).parent().find(".error").show();
                return false;
            } else {
                $(this).parent().find(".error").hide();
                return true;
            }

        }
    });

    $(".password").focusin(function()
    {
        $(this).parent().find(".error").hide();
    })
    .focusout(function()
    {
        var password = $(this).val();
        if($.trim(password) == "")
        {
            $(this).parent().find(".error").html("Enter your password");
            $(this).parent().find(".error").show();
        }
        else
        {
            $(this).parent().find(".error").hide();
        }
    });

    var validate_login_form = function($login_form)
    {
        var error = false;
        var email = $login_form.find(".email").val();
        var password = $login_form.find(".password").val();

        if($.trim(email) == "")
        {
            $login_form.find(".email").parent().find(".error").html("Enter your email address");
            $login_form.find(".email").parent().find(".error").show();
            error = true;
        }
        else
        {
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            if( !emailReg.test(email) )
            {
                $login_form.find(".email").parent().find(".error").html("Enter a valid email address");
                $login_form.find(".email").parent().find(".error").show();
                error = true;
            } else {
                $login_form.find(".email").parent().find(".error").html("Enter a valid email address");
                $login_form.find(".email").parent().find(".error").hide();
            }

        }

        if($.trim(password) == "")
        {
            $login_form.find(".password").parent().find(".error").html("Enter your password");
            $login_form.find(".password").parent().find(".error").show();
            error = true;
        }
        else
        {
            $(this).parent().find(".error").hide();
        }
        return !error;
    };

    var signin_enabled = true;

    $(".sign_in_btn").click(function(e)
    {

        e.preventDefault();

        if(!signin_enabled) {
            return false;
        }
        signin_enabled = false;

        var $login_form = $(this).parent();
        var $this = $(this);
        if(validate_login_form($login_form))
        {
            $.ajax({
                type: "POST",
                url: "/ajax/login",
                data: $login_form.serialize(),
                success: function(data)
                {
                    //console.log(data);
                    var parsed_json = jQuery.parseJSON(data);
                    //console.log(parsed_json.status);
                    if(parsed_json.status == "successful")
                    {
                        location.reload();
                    }
                    else
                    {
                        //console.log(parsed_json.message);
                        $login_form.find(".login_status_message2").text(parsed_json.message);
                        $login_form.find(".login_status_message2_container").css("visibility","visible");
                        signin_enabled = true;
                    }
                },
                error: function(jqXHR, status, errorThrown)
                {
                    $login_form.find(".login_status_message2").text("An error occured while processing your request. Please try again later.");
                    $login_form.find(".login_status_message2_container").css("visibility","visible");
                    signin_enabled = true;
                },
                complete: function(jqXHR, status)
                {
                    signin_enabled = true;
                }
            });

            $("#status_ok").click(function(e)
            {
                parent.location.reload();
                return false;
            });
        }
        else {
            signin_enabled = true;
        }
        return false;
    });

    $("#id_password_reset_link").click(function(e){
        e.preventDefault();
        var url = "/reset_password/";
        parent.location.href = url;
    });


    var email_valid = false;

    $(".signup_name").focusin(function()
    {
        $(this).parent().find(".error").hide();
    })
    .focusout(function()
    {
        var name = $(this).val();
        if($.trim(name) == "")
        {
            $(this).parent().find(".error").html("This field is required");
            $(this).parent().find(".error").show();
        }
        else
        {
            $(this).parent().find(".error").hide();
        }
    });

    $(".signup_surname").focusin(function()
    {
        $(this).parent().find(".error").hide();
    })
    .focusout(function()
    {
        var name = $(this).val();
        if($.trim(name) == "")
        {
            $(this).parent().find(".error").html("This field is required");
            $(this).parent().find(".error").show();
        }
        else
        {
            $(this).parent().find(".error").hide();
        }
    });

    $(".signup_email").focusin(function()
    {
        $(this).parent().find(".error").hide();
    })
    .focusout(function()
    {
        var email = $(this).val();
        if($.trim(email) == "")
        {
            $(this).parent().find(".error").html("This field is required");
            $(this).parent().find(".error").show();
        }
        else
        {
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            var $this = $(this);
            if( !emailReg.test(email) )
            {
                $(this).parent().find(".error").html("Enter a valid email address");
                $(this).parent().find(".error").show();
            } else {

                $.ajax({
                    type: "GET",
                    url: "/ajax/check_user/",
                    data: { "email": email },
                    enctype: 'multipart/form-data',
                    success: function(data)
                    {
                        var data = jQuery.parseJSON(data);
                        if(data.status == "SUCCESS") {
                            if(data.data == false) {
                                email_valid = true;
                                $this.parent().find(".error").hide();
                            }
                            else {
                                $this.parent().find(".error").html("This email is not available");
                                $this.parent().find(".error").show();
                            }
                        }
                    },
                    error: function(jqxhr, status, error)
                    {

                    }
                })
                .done(function( msg ) {

                });
            }

        }
    });

    var password = '';

    $(".signup_password").focusin(function()
    {
        $(this).parent().find(".error").hide();
    })
    .focusout(function()
    {
        password = $(this).val();
        if($.trim(password) == "")
        {
            $(this).parent().find(".error").html("Password is required at least 6 characters long.");
            $(this).parent().find(".error").show();
        }
        else
        {
            $(this).parent().find(".error").hide();
        }
    });

    $(".signup_password_repeat").focusin(function()
    {
        $(this).parent().find(".error").hide();
    })
    .focusout(function()
    {
        var pass_repeat = $(this).val();
        if($.trim(pass_repeat) == "")
        {
            $(this).parent().find(".error").html("Password required. At least 6 characters long");
            $(this).parent().find(".error").show();
        }
        else
        {
            var pass1 = password;
            if(pass1 == pass_repeat)
            {
                $(this).parent().find(".error").hide();
            }
            else
            {
                $(this).parent().find(".error").html("Repeat your password very carefully.");
                $(this).parent().find(".error").show();
            }
        }
    });

    $(".tz").select2({ width: 432 }).on("change", function(e) {
        var selected_tz = $(this).val();
         if(selected_tz == "---") {
            $(this).parent().find(".error").html("Please select your timezone");
            $(this).parent().find(".error").show();
         }
         else {
            $(this).parent().find(".error").hide();
         }
    });

    var validate_fields = function($signup_form)
    {
        var error = false;
        var name = $signup_form.find(".signup_name").val();
        var surname = $signup_form.find(".signup_surname").val();
        var email = $signup_form.find(".signup_email").val();
        var password = $signup_form.find(".signup_password").val();
        var pass_repeat = $signup_form.find(".signup_password_repeat").val();
        var timeone = $signup_form.find("select[name=signup_timezones] :selected").val();
        var email_valid = true;

        var role_selected = $("input[name=signup_role]:checked").val();
        if(typeof role_selected == "undefined") {
            $("#id_signup_role_info").find(".error").show();
            error = true;
        }
        else {
            $("#id_signup_role_info").find(".error").hide();
        }

        if($.trim(name) == "")
        {
            $signup_form.find(".signup_name").parent().find(".error").html("This field is required");
            $signup_form.find(".signup_name").parent().find(".error").show();
            error = true;
        }
        else
        {
            $signup_form.find(".signup_name").parent().find(".error").hide();
        }

        if($.trim(surname) == "")
        {
            $signup_form.find(".signup_surname").parent().find(".error").html("This field is required");
            $signup_form.find(".signup_surname").parent().find(".error").show();
            error = true;
        }
        else
        {
            $signup_form.find(".signup_surname").parent().find(".error").hide();
        }

        if($.trim(email) == "")
        {
            $signup_form.find(".signup_email").parent().find(".error").html("This field is required");
            $signup_form.find(".signup_email").parent().find(".error").show();
            error = true;
        }
        else
        {
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            if( !emailReg.test(email) )
            {
                $signup_form.find(".signup_email").parent().find(".error").html("Enter a valid email address");
                $signup_form.find(".signup_email").parent().find(".error").show();
                error = true;
            } else {
                if(!email_valid) {
                    $signup_form.find(".signup_email").parent().find(".error").html("Enter a valid email address");
                    $signup_form.find(".signup_email").parent().find(".error").show();
                }
                else {
                    $signup_form.find(".signup_email").parent().find(".error").hide();
                }
            }

        }

        if($.trim(password) == "")
        {
            $signup_form.find(".signup_password").parent().find(".error").html("Password required. At least 6 characters long");
            $signup_form.find(".signup_password").parent().find(".error").show();
            error = true;
        }
        else
        {
            $signup_form.find(".signup_password").parent().find(".error").hide();
        }

        if(password.length < 6)
        {
            $signup_form.find(".signup_password").parent().find(".error").html("Password required. At least 6 characters long");
            $signup_form.find(".signup_password").parent().find(".error").show();
            error = true;
        }
        else
        {
            $signup_form.find(".signup_password").parent().find(".error").hide();
        }

        if($.trim(pass_repeat) == "")
        {
            $signup_form.find(".signup_password_repeat").parent().find(".error").html("Password required. At least 6 characters long");
            $signup_form.find(".signup_password_repeat").parent().find(".error").show();
            error = true;
        }
        else
        {
            var pass1 = password;
            if(pass1 == pass_repeat)
            {
                $signup_form.find(".signup_password_repeat").parent().find(".error").hide();
            }
            else
            {
                $signup_form.find(".signup_password_repeat").parent().find(".error").html("Password mismatch.");
                $signup_form.find(".signup_password_repeat").parent().find(".error").show();
                error = true;
            }
        }
        if(timeone == "---"){
            $signup_form.find("select[name=signup_timezones]").parent().find(".error").html("Please select your timezone");
            $signup_form.find("select[name=signup_timezones]").parent().find(".error").show();
            error = true;
        }
        else{
            $signup_form.find("select[name=signup_timezones]").parent().find(".error").hide();
        }
        return !error && email_valid;
    };

    $(".signup_submit").click(function(e)
    {
        e.preventDefault();
        var $signup_form = $(this).parent();

        if(validate_fields($signup_form))
        {
            //$("form[name=registrationform]").submit();
            $(".signup_submit").text("Processing...");
            $(this).prop("disabled",true);
            $signup_form.css("cursor","wait");
            $.ajax({
                type: "POST",
                url: "/ajax/signup",
                data: $signup_form.serialize(),
                success: function(data)
                {
                    var parsed_json = jQuery.parseJSON(data);
                    if(parsed_json.status == "successful")
                    {
                        if(parsed_json.data && parsed_json.data.redirect == "true") {
                            window.location.href = parsed_json.data.url;
                        }
                        else {
                            window.location.href = "/signup-success/"
                        }
                    }
                    else
                    {
                        if(parsed_json.message == "EMAIL_REGISTERED")
                        {
                            $("#id_signup_email").css("border","1px solid red");
                            $("#id_signup_email").attr("data-original-title","Email address is already registered.");
                        }
                        else
                        {
                            $("#id_signup_error").text(parsed_json.message).fadeIn(500).fadeOut(3000);
                            $(".signup_submit").prop("disabled",false);
                            $("form[name=registrationform]").css("cursor","auto");
                            $(".signup_submit").text("Sign Up");

                        }
                    }
                },
                error: function(jqXHR, status, errorThrown)
                {
                    $("#id_signup_error").text("An error occured while processing your request. Please try again later.").show();
                },
                complete: function(jqXHR, status)
                {
                    $(".signup_submit").prop("disabled",false);
                    $("form[name=registrationform]").css("cursor","auto");
                    $(".signup_submit").text("Sign Up");
                }
            });
        }
        return false;
    });


    $("#id_fb_login").click(function(e) {
            FB.login(function(response) {
               if (response.status === 'connected') {
                  if ( response.authResponse ) {
                    try {
                        FB.api('/me', {fields: 'id,email,first_name,middle_name,last_name,gender,timezone,birthday'}, function(response) {
                          $("#id_social_fb_id").val(response.id);
                          $("#id_social_first_name").val(response.first_name);
                          $("#id_social_middle_name").val(response.middle_name);
                          $("#id_social_surname").val(response.last_name);
                          $("#id_social_email").val(response.email);
                          $("#id_social_tz_offset").val(response.timezone);
                          $("#id_social_gender").val(response.gender);
                          $(".social_signupform").prop("action", "/social-signup/");
                          $(".social_signupform").submit();
                        });
                    } catch ( error ) {

                    };
                } else {
                    alert( 'unauthorized' );
                };
                } else if (response.status === 'not_authorized') {
                  // The person is logged into Facebook, but not your app.
                } else {
                  // The person is not logged into Facebook, so we're not sure if
                  // they are logged into this app or not.
                }
             }, {scope: 'public_profile,email'});
        });

    $(".sign_up_tab li label").click(function(e) {
        $(this).parent().parent().find("li").removeClass("current");
        $(this).parent().addClass("current");
    });

    $("select[name=signup_role]").change(function(e) {
        $(".error").hide();
    });

            $("input[name=social_signup_role_radio]").change(function(e) {
            $("input[name=social_signup_role]").val($(this).val());
        });

        $("#id_social_signup").click(function(e) {
            e.preventDefault();
            var social_signup_name = $("input[name=social_signup_name]").val();
            var social_signup_surname = $("input[name=social_signup_surname]").val();
            var social_signup_email = $("input[name=social_signup_email]").val();
            var social_signup_password = $("input[name=social_signup_password]").val();
            var social_signup_password_repeat = $("input[name=social_signup_password_repeat]").val();
            var tz = $("select[name=social_signup_timezones] :selected").val();
            var social_signup_role = $("#id_social_signup_role").val();

            var request_url = "/ajax/signup";

            var error = false;

            if($.trim(social_signup_name) == "") {
                $("input[name=social_signup_name]").addClass("field_error");
                $("input[name=social_signup_name]").focus();
                $("input[name=social_signup_name]").parent().find(".error-hint1").text("This field is required").show();
                error = true;
            }
            else {
                $("input[name=social_signup_name]").removeClass("field_error");
                $("input[name=social_signup_name]").parent().find(".error-hint1").text("").hide();
            }

            if($.trim(social_signup_surname) == "") {
                $("input[name=social_signup_surname]").addClass("field_error");
                $("input[name=social_signup_surname]").focus();
                $("input[name=social_signup_surname]").parent().find(".error-hint1").text("This field is required").show();
                error = true;
            }
            else {
                $("input[name=social_signup_surname]").removeClass("field_error");
                $("input[name=social_signup_surname]").parent().find(".error-hint1").text("").hide();
            }

            if($.trim(social_signup_email) == "") {
                $("input[name=social_signup_email]").addClass("field_error");
                $("input[name=social_signup_email]").focus();
                $("input[name=social_signup_email]").parent().find(".error-hint1").text("This field is required").show();
                error = true;
            }
            else {
                $("input[name=social_signup_email]").removeClass("field_error");
                $("input[name=social_signup_email]").parent().find(".error-hint1").text("").hide();
            }

            if($.trim(social_signup_password) == "") {
                $("input[name=social_signup_password]").addClass("field_error");
                $("input[name=social_signup_password]").focus();
                $("input[name=social_signup_password]").parent().find(".error-hint1").text("This field is required").show();
                error = true;
            }
            else {
                $("input[name=social_signup_password]").removeClass("field_error");
                $("input[name=social_signup_password]").parent().find(".error-hint1").text("").hide();
            }

            if(social_signup_password.length < 6) {
                $("input[name=social_signup_password]").addClass("field_error");
                $("input[name=social_signup_password]").focus();
                $("input[name=social_signup_password]").parent().find(".error-hint1").text("Password required. At least 6 characters long").show();
                error = true;
            }
            else {
                $("input[name=social_signup_password]").removeClass("field_error");
                $("input[name=social_signup_password]").parent().find(".error-hint1").text("").hide();
            }

            if($.trim(social_signup_password_repeat) == "") {
                $("input[name=social_signup_password_repeat]").addClass("field_error");
                $("input[name=social_signup_password_repeat]").focus();
                $("input[name=social_signup_password_repeat]").parent().find(".error-hint1").text("This field is required").show();
                error = true;
            }
            else {
                $("input[name=social_signup_password_repeat]").removeClass("field_error");
                $("input[name=social_signup_password_repeat]").parent().find(".error-hint1").text("").hide();
            }
            if($.trim(social_signup_role) == "") {
                $("#id_social_signup_role").addClass("field_error");
                $("#id_social_signup_role").focus();
                $("#id_social_signup_role").parent().find(".error-hint1").text("Please select a role").show();
                error = true;
            }
            else {
                $("#id_social_signup_role").removeClass("field_error");
                $("#id_social_signup_role").parent().find(".error-hint1").text("").hide();
            }

            if(error) {
                return false;
            }

            $.ajax({
                type: "POST",
                url: "/ajax/signup",
                data: $("form[name=social_signup_form]").serialize(),
                success: function(data)
                {
                    var parsed_json = jQuery.parseJSON(data);
                    if(parsed_json.status == "successful")
                    {
                        window.location.href = "/login?signup=success"
                    }
                    else
                    {
                        if(parsed_json.message == "EMAIL_REGISTERED")
                        {
                            $("input[name=social_signup_email]").parent().find(".error-hint1").text("Email address is already registered").show();
                        }
                        else
                        {
                            //$("#status_message3").text(parsed_json.message).show();
                        }
                    }
                },
                error: function(jqXHR, status, errorThrown)
                {

                },
                complete: function(jqXHR, status)
                {

                }
            });

            return false;
        });



});