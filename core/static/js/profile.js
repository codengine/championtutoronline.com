jQuery(function () {
    jQuery('#id_lesson_date').datetimepicker({ format: 'DD/MM/YYYY' });
    jQuery('#id_lesson_time').datetimepicker({ format: 'LT' });
});
$(document).ready(function(){

    $("#id_tz_visit_profile").select2({
        width: 210
    });

     {% if content_editable %}
        $("#id_signup_timezones").select2({ width: 432 }).on("change", function(e) {
            var selected_tz = $(this).val();
             if(selected_tz == "---") {
                $(this).parent().find(".error").html("Please select your timezone");
                $(this).parent().find(".error").show();
             }
             else {
                $(this).parent().find(".error").hide();
             }
        });
     {% endif %}


{#        $("#id_lesson_date").datepicker({#}
{#            //format: 'DD/MM/YYYY'#}
{#        });#}

{#        $("#id_lesson_length").select2();#}
{##}
{#        $("#id_lesson_subject").select2();#}

    $(".select2-selection").css("height","35px");
    $(".select2-selection").css("border","1px solid #CFCFCF");

    $("#id_add_written_lesson_request").click(function(e){
        e.preventDefault();
        window.location = "/written-lesson/";
    });

    $(document).on("click","#id_add_lesson_request",function(e){
        e.preventDefault();
        $("input[name=lesson-request-enabled]").val("1");
        $(this).hide();
        $("#id_lesson_request_content").show();
        $("#id_btn_send_msg").text("Send Proposal");
    });

    $(document).on("click","#id_hide_lesson_request_content", function(e){
        e.preventDefault();
        $("input[name=lesson-request-enabled]").val("0");
        $("#id_add_lesson_request").show();
        $("#id_lesson_request_content").hide();
        $("#id_btn_send_msg").text("Send Message");
    });

    $(document).on("click","#id_btn_send_msg",function(e){
        e.preventDefault();
        var form_data = $("#id_msg_dialog_form").serialize();

        console.log(form_data);
        console.log($("input[name=msg_attachment]").val());

        _ajax_call("POST","/ajax/lesson-request/",form_data,function(data){
                    var lesson_proposal_enabled = $("input[name=lesson-request-enabled]").val();
                    //console.log(lesson_proposal_enabled);
                    //console.log(lesson_proposal_enabled=="1");
                    if(lesson_proposal_enabled == "1"){
                        //console.log("sadasd");
                        $("#id_msg_sent_status").html("<p style='font-size: 18px;'>Your Lesson request has been sent. You will be notified once approved.</p>").show();
                        $("#id_msg_html").html("<p style='font-weight: bold; font-size: 20px; color: black; padding: 20px;'>Your Lesson request has been sent. You will be notified once approved.</p>");
                    }
                    else{
                        //console.log("here");
                        $("#id_msg_sent_status").html("<p style='font-size: 18px;'>Message sent successfully.</p>").show();
                        $("#id_msg_html").html("<p style='font-weight: bold; font-size: 20px; color: black; padding: 20px;'>Your Message has been sent.</p>");
                    }
                },
                function(jqxhr, status, error){

                },
                function(msg){

                });

        return false;
    });

    var load_ajax_content = function(content_id,callback,errback,complete_back){

        var request_url = "/ajax/profile";

        $.ajax({
            type: "GET",
            url: request_url,
            data: { content_id: content_id },
            success: function(content)
            {
                {#                    $("#feed-content").html(content);#}
                {#                    console.log("Content loaded.");#}
                callback(content);
            },
            error: function(jqxhr, status, error)
            {
                console.log("An error occured while adding canvas tab. Error message: "+error);
                errback(jqxhr, status, error);
            }
        })
        .done(function( msg ) {
            complete_back(msg);
        });
    }

   $("form[name=visit_profile_signup]").submit(function(e) {
        //e.preventDefault();
        var value = $("#id_msg_text").val();
        if($.trim(value) == "") {
            $("#id_msg_text").next().text("This field is required").show();
            $("#id_msg_text").focus();
            return false;
        }
        else {
            $("#id_msg_text").next().hide();
        }
        var value = $("#id_name_visit_profile").val();
        if($.trim(value) == "") {
            $("#id_name_visit_profile").next().text("This field is required").show();
            $("#id_name_visit_profile").focus();
            return false;
        }
        else {
            $("#id_name_visit_profile").next().hide();
        }
        var value = $("#id_email_visit_profile").val();
        if($.trim(value) == "") {
            $("#id_email_visit_profile").next().text("This field is required").show();
            $("#id_email_visit_profile").focus();
            return false;
        }
        else {
            $("#id_email_visit_profile").next().hide();
        }
        var value = $("#id_password_visit_profile").val();
        if($.trim(value) == "") {
            $("#id_password_visit_profile").next().text("This field is required").show();
            $("#id_password_visit_profile").focus();
            return false;
        }
        else {
            $("#id_password_visit_profile").next().hide();
        }
        var value = $("#id_tz_visit_profile").val();
        if($.trim(value) == "") {
            $("#id_tz_visit_profile").next().text("This field is required").show();
            $("#id_tz_visit_profile").focus();
            return false;
        }
        else {
            $("#id_tz_visit_profile").next().hide();
        }

        $("#id_visit_profile_send_message").prop("disabled", true);

        $.ajax({
            type: "POST",
            url: "/ajax/signup",
            data: $("form[name=visit_profile_signup]").serialize(),
            success: function(data)
            {
                var parsed_json = jQuery.parseJSON(data);
                if(parsed_json.status == "successful")
                {
                    window.location.href = "/login?signup=success"
                }
                else
                {
                    if(parsed_json.message == "EMAIL_REGISTERED")
                    {
                        $("#id_email_visit_profile").next().text("This email address is already registered.").show();
                        $("#id_email_visit_profile").focus();
                    }
                    else
                    {

                    }
                }
            },
            error: function(jqXHR, status, errorThrown)
            {

            },
            complete: function(jqXHR, status)
            {
                $("#id_visit_profile_send_message").prop("disabled", false);
            }
        });

        return false;
    });
});