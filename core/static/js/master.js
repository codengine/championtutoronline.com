(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));




    $(window).load(function(){
        //alert("Loaded all contents...");
    });

$(document).ready(function()
{

    //alert($(window).height());
    $("#id_chat_sidebar_floating").find(".chat_sidebar").css("height",($(window).height()) + "px");

    $(".chat_sidebar").find("tbody").css("height",($(window).height()) + "px");

    $("#id_button_tut_online_toggle").click(function(e){
        $(this).parent().find(".chat_sidebar").toggle();
    });

    $( window ).resize(function() {
        $("#id_chat_sidebar_floating").find(".chat_sidebar").css("height",($(window).height()) + "px");

        $(".chat_sidebar").find("tbody").css("height",($(window).height()) + "px");
    });

    $(".buddy-row").hover(function(e){
        $(this).find(".buddy-inline-action").show();
    },
    function(e){
        $(this).find(".buddy-inline-action").hide();
    });

    $(document).mouseup(function (e)
    {
        var container = $("#id_chat_tutor_list");
        var chatbox = $(".chat");
        var board_taskbar = $(".ui-tabs-nav");

        if ((!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) && (!chatbox.is(e.target) && chatbox.has(e.target).length == 0 ) && e.target.className != "show_menu_icon"  && (!board_taskbar.is(e.target) && board_taskbar.has(e.target).length == 0 ) ) // ... nor a descendant of the container
        {
            container.hide();
        }
    });

    $(window).resize(function()
    {
        $('#loading_container').css({
            position: 'absolute'
        });

        $('#loading_container').css({
            left: ($(window).width() - $('#loading_container').outerWidth()) / 2,
            top: ($(window).height() - $('#loading_container').outerHeight()) / 2
        });
    });

    // call `resize` to center elements
    $(window).resize();

});

$(function(){
$('#sidemenu a').on('click', function(e){
});
});

$("body").addClass("modal-open");
$("body").css("overflow-y","auto");

$(document).ready( function () {
    if($("ul.submenu1").hasClass("addactive"))
    {
        $(this).parent().find("span").hide();
        //alert('active');
    }
});

$(document).ready( function () {
    $("ul.after_login_menu li,.profile_detail").click(function (e) {

        //alert("sadasd");

        $("ul.after_login_menu li").removeClass('hidespan');
        $(this).addClass("hidespan");
        $("ul.submenu1",this).toggleClass("addactive");
        if($("ul.submenu1",this).hasClass("off"))
        {
            //       alert("start");
            $("ul.submenu1",this).removeClass("off");
            $("ul.submenu1",this).addClass("addactive");

            //    alert("end");

        }
        if(!$("ul.submenu1",this).hasClass("addactive"))
        {
            $("ul.after_login_menu li").removeClass('hidespan');
        }
        $("ul.submenu1").addClass("off");
        $("ul.submenu1",this).removeClass("off");
        return true;

    });

    $("ul.submenu1").click(function () {
        $("ul.submenu1").toggleClass("addactive");

    });
    $(".contnt").click(function () {
        $("ul.submenu1").removeClass("addactive");
    });

    $("ul.after_login_menu li.hidespan").mouseover(function(){
        //alert("nbdl");
        $("span.hvr").hide();
    });

    var black_overlay = $('#black_overlay');
var sign_up = $('#sign_up');
var sign_in = $('#sign_in');

black_overlay.click(function(){black_overlay.hide(); sign_up.hide();});
black_overlay.click(function(){black_overlay.hide(); sign_in.hide();});


$( document ).on( 'keydown', function ( e ) {
    if ( e.keyCode === 27 ) { // ESC
        $( black_overlay ).hide();
        $( sign_up ).hide();
        $( sign_in ).hide();
    }
});

$("#id_open_signup").click(function(e) {
    $( black_overlay ).hide();
    $( sign_in ).hide();
    show_signup();
});

$("#id_open_signin").click(function(e) {
    $( black_overlay ).hide();
    $( sign_up ).hide();
    show_signin();
});

});

function toggle_visibility(id) {
    var e = document.getElementById(id);
    if(e.style.display == 'block')
        e.style.display = 'none';
    else
        e.style.display = 'block';
}

function show_signup(tab)
{
    if(typeof tab != "undefined") {
        if(tab == "tutor") {
            $("#tutor").parent().parent().find("li").removeClass("current");
            $("#tutor").parent().addClass("current");
        }
        else if(tab == "student") {
            $("#student").parent().parent().find("li").removeClass("current");
            $("#student").parent().addClass("current");
        }
    }
    document.getElementById('black_overlay').style.display="block";
    document.getElementById('sign_up').style.display="block";
}

function show_signin()
{document.getElementById('black_overlay').style.display="block";
    document.getElementById('sign_in').style.display="block";}

$("#id_countdown_timestamp").countdown("{{ lesson_starting.remaining_time }}", function(event) {
    if(event.offset.hours == 0 && event.offset.minutes == 0 && event.offset.seconds == 0){
        window.location.href = $("#id_countdown_timestamp").parent().prop("href");
    }
    else{
        $(this).text(
                event.strftime('%H:%M:%S')
        );
    }
});


$('ul.sign_up_tab > li > label').click(function() {
    $('ul.sign_up_tab > li').eq($(this).parent('li').index()).addClass('current').siblings().removeClass('current');
    tabsss($(this).parent('li').index());
});
function tabsss(val)
{
    $(".tab").hide();
    if(val>0)
    {
        document.getElementsByClassName('tab')[val].style.display="block";
    }
    else
    {
        document.getElementsByClassName('tab')[0].style.display="block";
    }
}

$(document).ready(function(e)
{

    var email_valid = false;

    $("#id_signup_name").focusin(function()
    {
        $(this).parent().find(".error").hide();
    })
    .focusout(function()
    {
        var name = $(this).val();
        if($.trim(name) == "")
        {
            $(this).parent().find(".error").html("Name is required.");
            $(this).parent().find(".error").show();
        }
        else
        {
            $(this).parent().find(".error").hide();
        }
    });

    $("#id_signup_email").focusin(function()
    {
        $(this).parent().find(".error").hide();
    })
    .focusout(function()
    {
        var email = $(this).val();
        if($.trim(email) == "")
        {
            $(this).parent().find(".error").html("A valid email address is required.");
            $(this).parent().find(".error").show();
        }
        else
        {
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            var $this = $(this);
            if( !emailReg.test(email) )
            {
                $(this).parent().find(".error").html("A valid email address is required.");
                $(this).parent().find(".error").show();
            } else {

                $.ajax({
                    type: "GET",
                    url: "/ajax/check_user/",
                    data: { "email": email },
                    enctype: 'multipart/form-data',
                    success: function(data)
                    {
                        var data = jQuery.parseJSON(data);
                        if(data.status == "SUCCESS") {
                            if(data.data == false) {
                                email_valid = true;
                                $this.parent().find(".error").hide();
                            }
                            else {
                                $this.parent().find(".error").html("User already exists with this email.");
                                $this.parent().find(".error").show();
                            }
                        }
                    },
                    error: function(jqxhr, status, error)
                    {

                    }
                })
                .done(function( msg ) {

                });
            }

        }
    });

    $("#id_signup_password").focusin(function()
    {
        $(this).parent().find(".error").hide();
    })
    .focusout(function()
    {
        var password = $(this).val();
        if($.trim(password) == "")
        {
            $(this).parent().find(".error").html("Password is required. Use a strong password at least 6 characters long.");
            $(this).parent().find(".error").show();
        }
        else
        {
            $(this).parent().find(".error").hide();
        }
    });

    $("#id_signup_password_repeat").focusin(function()
    {
        $(this).parent().find(".error").hide();
    })
    .focusout(function()
    {
        var pass_repeat = $(this).val();
        if($.trim(pass_repeat) == "")
        {
            $(this).parent().find(".error").html("Password is required. Use a strong password at least 6 characters long.");
            $(this).parent().find(".error").show();
        }
        else
        {
            var pass1 = $("#id_signup_password").val();
            if(pass1 == pass_repeat)
            {
                $(this).parent().find(".error").hide();
            }
            else
            {
                $(this).parent().find(".error").html("Repeat your password very carefully.");
                $(this).parent().find(".error").show();
            }
        }
    });

    var validate_fields = function()
    {
        var error = false;
        var name = $("#id_signup_name").val();
        var email = $("#id_signup_email").val();
        var password = $("#id_signup_password").val();
        var pass_repeat = $("#id_signup_password_repeat").val();
        var timeone = $("#id_signup_timezones").val();

        if($.trim(name) == "")
        {
            $("#id_signup_name").parent().find(".error").html("Name is required.");
            $("#id_signup_name").parent().find(".error").show();
            error = true;
        }
        else
        {
            $("#id_signup_name").parent().find(".error").hide();
        }

        if($.trim(email) == "")
        {
            $("#id_signup_email").parent().find(".error").html("A valid email address is required.");
            $("#id_signup_email").parent().find(".error").show();
            error = true;
        }
        else
        {
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            if( !emailReg.test(email) )
            {
                $("#id_signup_email").parent().find(".error").html("A valid email address is required.");
                $("#id_signup_email").parent().find(".error").show();
                error = true;
            } else {
                if(!email_valid) {
                    $("#id_signup_email").parent().find(".error").html("A valid email address is required.");
                    $("#id_signup_email").parent().find(".error").show();
                }
                else {
                    $("#id_signup_email").parent().find(".error").hide();
                }
            }

        }

        if($.trim(password) == "")
        {
            $("#id_signup_password").parent().find(".error").html("Password is required. Please use a strong password with at least 6 characters.");
            $("#id_signup_password").parent().find(".error").show();
            error = true;
        }
        else
        {
            $("#id_signup_password").parent().find(".error").hide();
        }

        if($.trim(pass_repeat) == "")
        {
            $("#id_signup_password_repeat").parent().find(".error").html("Password is required. Please use a strong password with at least 6 characters.");
            $("#id_signup_password_repeat").parent().find(".error").show();
            error = true;
        }
        else
        {
            var pass1 = $("#id_signup_password").val();
            if(pass1 == pass_repeat)
            {
                $("#id_signup_password_repeat").parent().find(".error").hide();
            }
            else
            {
                $("#id_signup_password_repeat").parent().find(".error").html("Password mismatch.");
                $("#id_signup_password_repeat").parent().find(".error").show();
                error = true;
            }
        }
        if(timeone == "---"){
            $("#id_signup_timezones").parent().find(".error").html("Please select your timezone.");
            $("#id_signup_timezones").parent().find(".error").show();
            error = true;
        }
        else{
            $("#id_signup_timezones").parent().find(".error").hide();
        }
        return !error && email_valid;
    };

    $("#id_signup_submit_button").click(function(e)
    {
        if(validate_fields())
        {
            //$("form[name=registrationform]").submit();
            $("#id_signup_submit_button").text("Processing...");
            $(this).prop("disabled",true);
            $("form[name=registrationform]").css("cursor","wait");
            $.ajax({
                type: "POST",
                url: "/ajax/signup",
                data: $("form[name=registrationform]").serialize(),
                success: function(data)
                {
                    console.log(data);
                    var parsed_json = jQuery.parseJSON(data);
                    console.log(parsed_json.status);
                    if(parsed_json.status == "successful")
                    {
                        window.location.href = "/login?signup=success"
                    }
                    else
                    {
                        if(parsed_json.message == "EMAIL_REGISTERED")
                        {
                            $("#id_signup_email").css("border","1px solid red");
                            $("#id_signup_email").attr("data-original-title","Email address is already registered.");
                        }
                        else
                        {
                            $("#status_message3").text(parsed_json.message).show();
                        }
                    }
                },
                error: function(jqXHR, status, errorThrown)
                {
                    $("#status_message3").text("An error occured while processing your request. Please try again later.").show();
                },
                complete: function(jqXHR, status)
                {
                    $("#id_signup_submit_button").prop("disabled",false);
                    $("form[name=registrationform]").css("cursor","auto");
                    $("#id_signup_submit_button").text("Sign Up");
                }
            });
        }
        return false;
    });

    //$(".form-control").tooltip();
        $("#id_email").focusin(function()
        {
            $(this).parent().find(".error").hide();
        })
        .focusout(function()
        {
            var email = $(this).val();
            if($.trim(email) == "")
            {
                $(this).parent().find(".error").html("A valid email address is required.");
                $(this).parent().find(".error").show();
            }
            else
            {
                var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                if( !emailReg.test(email) )
                {
                    $(this).parent().find(".error").html("A valid email address is required.");
                    $(this).parent().find(".error").show();
                    return false;
                } else {
                    $(this).parent().find(".error").hide();
                    return true;
                }

            }
        });

        $("#id_password").focusin(function()
        {
            $(this).parent().find(".error").hide();
        })
        .focusout(function()
        {
            var password = $(this).val();
            if($.trim(password) == "")
            {
                $(this).parent().find(".error").html("Enter your password");
                $(this).parent().find(".error").show();
            }
            else
            {
                $(this).parent().find(".error").hide();
            }
        });

        var validate_login_form = function()
        {
            var error = false;
            var email = $("#id_email").val();
            var password = $("#id_password").val();

            if($.trim(email) == "")
            {
                $(this).parent().find(".error").html("Enter your email address");
                $(this).parent().find(".error").show();
                error = true;
            }
            else
            {
                var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                if( !emailReg.test(email) )
                {
                    $(this).parent().find(".error").html("A valid email address is required.");
                    $(this).parent().find(".error").show();
                    error = true;
                } else {
                    $(this).parent().find(".error").html("A valid email address is required.");
                    $(this).parent().find(".error").hide();
                }

            }

            if($.trim(password) == "")
            {
                $(this).parent().find(".error").html("Enter your password");
                $(this).parent().find(".error").show();
                error = true;
            }
            else
            {
                $(this).parent().find(".error").hide();
            }
            return !error;
        };

        $("#id_submit_button").click(function(e)
        {
            $(this).prop("disabled",true);
            e.preventDefault();
            if(validate_login_form())
            {
                $.ajax({
                    type: "POST",
                    url: "/ajax/login",
                    data: $("form[name=loginform]").serialize(),
                    success: function(data)
                    {
                        //console.log(data);
                        var parsed_json = jQuery.parseJSON(data);
                        //console.log(parsed_json.status);
                        if(parsed_json.status == "successful")
                        {
                            location.reload();
                        }
                        else
                        {
                            //console.log(parsed_json.message);
                            $("#login_status_message2").text(parsed_json.message);
                            $("#login_status_message2_container").css("visibility","visible");
                            $("#id_submit_button").prop("disabled",false);
                        }
                    },
                    error: function(jqXHR, status, errorThrown)
                    {
                        $("#login_status_message2").text("An error occured while processing your request. Please try again later.");
                        $("#slogin_tatus_message2_container").css("visibility","visible");
                        $("#id_submit_button").prop("disabled",false);
                    },
                    complete: function(jqXHR, status)
                    {
                        $(this).prop("disabled",false);
                        $(this).css("cursor","default");
                    }
                });

                $("#status_ok").click(function(e)
                {
                    parent.location.reload();
                    return false;
                });
            }
            return false;
        });

//        $("#id_password_reset_link").click(function(e){
//            e.preventDefault();
//            var url = "{% url "reset_password" %}";
//            parent.location.href = url;
//        });

});

$(document).ready(function(){

    $( ".tol" ).hover(function() {
      $(this).siblings('.online_teaching').toggleClass("displayblock");
    });

    function tutor_status_update(data,callback,error_back,complete_back) {
        $.ajax({
            type: "POST",
            url: "/update-status/",
            data: data,
            enctype: 'multipart/form-data',
            success: function(data)
            {
                callback(jQuery.parseJSON(data));
            },
            error: function(jqxhr, status, error)
            {
                error_back(jqxhr, status, error);
            }
        })
        .done(function( msg ) {
            complete_back(msg);
        });
    }

    var success_back = function(data) {

    }

    var error_back = function(jqxhr, status, error) {

    }

    var complete_back = function(msg) {

    }


    function update_status() {
        var ready_for_online = !$("#myonoffswitch100").is(":checked");
        var written_lesson = $("#myonoffswitch00").is(":checked");
        var profile_deactivated = !$("#myonoffswitch102").is(":checked");

        var data = {};

        if(profile_deactivated) {
            data["status"] = "profile_deactivated";
        }
        else {
            if(ready_for_online && written_lesson) {
                data["status"] = "ready_for_both";
            }
            else if(ready_for_online && !written_lesson) {
                data["status"] = "ready_to_teach";
            }
            else if(!ready_for_online && written_lesson) {
                data["status"] = "ready_for_written_lesson";
            }
            else if(!ready_for_online && !written_lesson) {
                data["status"] = "unavailable";
            }
        }
        tutor_status_update(data,success_back,error_back,complete_back);
    }

    var profile_deactivation_action = false;

    $(document).on("change","#myonoffswitch100",function(e) {
        if(!profile_deactivation_action) {
            update_status();
        }
    });

    $(document).on("change","#myonoffswitch00",function(e) {
        if(!profile_deactivation_action) {
            update_status();
        }
    });

    $(document).on("change","#myonoffswitch102",function(e) {
        if(!$(this).is(":checked")) {
            profile_deactivation_action = true;
            $("#myonoffswitch100").prop("checked", true);
            $("#myonoffswitch00").prop("checked", false);
            $("#myonoffswitch100").prop("disabled", true);
            $("#myonoffswitch00").prop("disabled", true);
            update_status();
        }
        else {
            profile_deactivation_action = true;
            $("#myonoffswitch100").prop("disabled", false);
            $("#myonoffswitch00").prop("disabled", false);
            update_status();
        }

    });

     $(document).on("change","#myonoffswitch6",function(e) {
        if(window.screensharing != "undefined") {
            window.screensharing = $(this).is(":checked");
        }
    });

});

function load_tutor_section_contents(data,callback,error_back,complete_back) {
    var url = "/ajax/profile-section";
    if(data["url"] != undefined && data["url"] != "") {
        url = data["url"];
    }
    $.ajax({
        type: "GET",
        url: url,
        data: data,
        timeout: 5000,
        enctype: 'multipart/form-data',
        success: function(data)
        {
            callback(jQuery.parseJSON(data));
        },
        error: function(jqxhr, status, error)
        {
            error_back(jqxhr, status, error);
        }
    })
    .done(function( msg ) {
        complete_back(msg);
    });
}

function collect_my_jobs_request_data(scroll_event) {
    var data = {"section": "my-jobs"};
    var filter = $("#id_my_jobs_filter").val();
    if(filter != "all") {
        data["filter"] = filter;
    }
    var start = $("#date-range200").val();
    var end = $("#date-range201").val();
    if(start != "") {
        data["start"] = start;
    }
    if(end != "") {
        data["end"] = end;
    }
    if(typeof scroll_event != "undefined" && scroll_event) {
        if($("#id_section_my_jobs").find("li.my-job-item").length) {
            data["since"] = $("#id_section_my_jobs").find("li.my-job-item").last().data("date");
        }
    }
    return data
  }

  function build_my_jobs_history_url(scroll_event) {
        var query_params = {"tab": "my-jobs"};
        var filter = $("#id_my_jobs_filter").val();
        if(filter != "all") {
            query_params["filter"] = filter;
        }
        var start = $("#date-range200").val();
        var end = $("#date-range201").val();
        if(start != "") {
            query_params["start"] = start;
        }
        if(end != "") {
            query_params["end"] = end;
        }
        if(typeof scroll_event != "undefined" && scroll_event) {
            if($("#id_section_my_jobs").find("li.my-job-item").length) {
                query_params["since"] = $("#id_section_my_jobs").find("li.my-job-item").last().data("date");
            }
        }
        query_string = "";
        for(var param in query_params) {
            query_string += param + "="+ query_params[param]+"&";
        }
        return query_string;
  }

  function update_address_url_my_jobs() {
        History.replaceState({}, null, "?"+build_my_jobs_history_url());
  }

  function load_my_jobs_contents(scroll_event, append, callback, error_back, complete_back) {

        $("#id_section_my_jobs").css("opacity", "0.7");
        load_tutor_section_contents(collect_my_jobs_request_data(scroll_event),function(data) {
            if(typeof append != "undefined" && append) {
                $("#id_section_my_jobs").append(data.data.content);
            }
            else {
                $("#id_section_my_jobs").html(data.data.content);
            }
            $("#id_section_my_jobs").css("opacity", "1.0");
            content_cache["id_section_my_jobs"] = true;
            if(typeof callback != "undefined" && typeof callback == "function") {
                callback(data);
            }
            //my_pagination("id_student_active_pagination","id_student_active_content","students_active",1,data.data.extra.total_pages,8);
        },
        function(jqxhr, status, error) {
            if(typeof error_back != "undefined" && typeof error_back == "function") {
                error_back(jqxhr, status, error);
            }
        },
        function(msg) {
            if(typeof complete_back != "undefined" && typeof complete_back == "function") {
                complete_back(msg);
            }
        });
  }

  $(document).on("click", "#id_search_my_jobs", function(e) {
    e.preventDefault();
    update_address_url_my_jobs();
    load_my_jobs_contents();
  });

  $(document).on("click", "#id_my_jobs_load_more_button", function(e) {
    e.preventDefault();
    $("#id_my_jobs_load_more").show();
    $("#id_my_jobs_load_more_button").hide();
    load_my_jobs_contents(true, true, function(data) {
        $("#id_my_jobs_load_more").hide();
        $("#id_my_jobs_load_more_button").hide();
    }, function(jqxhr, status, error) {
        $("#id_my_jobs_load_more").hide();
        $("#id_my_jobs_load_more_button").show();
    }, function(msg) {

    });
  });

content_cache = {};

$(function(){
  $('#sidemenu a').on('click', function(e){
    e.preventDefault();

    if($(this).hasClass('open')) {
      // do nothing because the link is already open
    } else {
      var oldcontent = $('#sidemenu a.open').attr('href');
      var newcontent = $(this).attr('href');

      $(oldcontent).fadeOut('fast', function(){
        $(newcontent).fadeIn().removeClass('hidden');
        $(oldcontent).addClass('hidden');
      });


      $('#sidemenu a').removeClass('open');
      $(this).addClass('open');

      var href = $(this).attr('href');
      if(href.match("#about-content$") && (content_cache["about-content"] === undefined || !content_cache["about-content"])) {
            History.replaceState({}, null, "?tab=profile");
            load_tutor_section_contents({"section": "about", "view_as": $("#id_hidden_view_as").val(), "user_id": $("#id_profile_object_pk").val()},function(data) {
                $("#about-content").find(".data_msng").html(data.data.content);
                content_cache["about-content"] = true;
            },
            function(jqxhr, status, error) {

            },
            function(msg) {

            });
      }
      else if(href.match("#about-content-public$") && (content_cache["about-content-public"] === undefined || !content_cache["about-content-public"])) {
            load_tutor_section_contents({"section": "about", "view_as": $("#id_hidden_view_as").val(), "user_id": $("#id_profile_object_pk").val()},function(data) {
                $("#about-content-public").find(".data_msng").html(data.data.content);
                content_cache["about-content-public"] = true;
            },
            function(jqxhr, status, error) {

            },
            function(msg) {

            });
      }
      else if(href.match("#report-content$") && (content_cache["report-content"] == undefined || !content_cache["report-content"])) {
            load_tutor_section_contents({"section": "progress_reports"},function(data) {
                $("#report-content").find(".data_msng").html(data.data.content);
                content_cache["report-content"] = true;
                my_pagination("id_progress_report_content_pagination","id_progress_report_content","progress_reports",1,data.data.extra.total_pages,8);
            },
            function(jqxhr, status, error) {

            },
            function(msg) {

            });
      }
      else if(href.match("#jobs-invitation-content$") && (content_cache["jobs-invitation-content"] == undefined || !content_cache["jobs-invitation-content"])) {
            History.replaceState({}, null, "?tab=invites");
            load_tutor_section_contents({"section": "job_invitation"},function(data) {
                $("#jobs-invitation-content").find(".data_msng").html(data.data.content);
                content_cache["jobs-invitation-content"] = true;
                my_pagination("id_job_invitation_content_pagination","id_job_invitation_content","job_invitation",1,data.data.extra.total_pages,8);
            },
            function(jqxhr, status, error) {

            },
            function(msg) {

            });
      }
      else if(href.match("#jobs-recommendation-content$") && (content_cache["jobs-recommendation-content"] == undefined || !content_cache["jobs-recommendation-content"])) {
            History.replaceState({}, null, "?tab=recommendations");
            load_tutor_section_contents({"section": "job_recommendation"},function(data) {
                $("#jobs-recommendation-content").find(".data_msng").html(data.data.content);
                content_cache["jobs-recommendation-content"] = true;
                my_pagination("id_job_recommendation_content_pagination","id_job_recommendation_content","job_recommendation",1,data.data.extra.total_pages,8);
            },
            function(jqxhr, status, error) {

            },
            function(msg) {

            });
      }
      else if(href.match("#participation-content$") && (content_cache["participation-content"] == undefined || !content_cache["participation-content"])) {
            load_tutor_section_contents({"section": "my_participation"},function(data) {
                $("#participation-content").find(".data_msng").html(data.data.content);
                content_cache["participation-content"] = true;
                my_pagination("id_participation_content_pagination","id_participation_content","my_participation",1,data.data.extra.total_pages,8)
            },
            function(jqxhr, status, error) {

            },
            function(msg) {

            });
      }
      else if(href.match("/calendar-events/$")) {
            location.href = "/calendar-events/";
      }
      else if(href.match("#recorded-sessions-content$") && (content_cache["recorded-sessions-content"] == undefined || !content_cache["recorded-sessions-content"])) {
            load_tutor_section_contents({"section": "my_recorded_session"},function(data) {
                $("#recorded-sessions-content").find(".data_msng").html(data.data.content);
                content_cache["recorded-sessions-content"] = true;
                my_pagination("id_my_recorded_session_content_pagination","id_my_recorded_session_content","my_recorded_session",1,data.data.extra.total_pages,8)
            },
            function(jqxhr, status, error) {

            },
            function(msg) {

            });
      }
      else if(href.match("#review-content$") && (content_cache["review-content"] == undefined || !content_cache["review-content"])) {
            if(window.champ_user_id != -1) {
                load_tutor_section_contents({"section": "my_reviews"},function(data) {
                    $("#review-content").find(".data_msng").html(data.data.content);
                    content_cache["review-content"] = true;
                    my_pagination("id_my_reviews_content_pagination","my_reviews_content","my_reviews",1,data.data.extra.total_pages,8)
                },
                function(jqxhr, status, error) {

                },
                function(msg) {

                });
            }
      }
      else if(href.match("#review-content-public$") && (content_cache["review-content-public"] == undefined || !content_cache["review-content-public"])) {
            if(window.champ_user_id != -1) {
                load_tutor_section_contents({"section": "my_reviews"},function(data) {
                    $("#review-content-public").find(".data_msng").html(data.data.content);
                    content_cache["review-content-public"] = true;
                    my_pagination("id_my_reviews_content_pagination","my_reviews_content","my_reviews",1,data.data.extra.total_pages,8)
                },
                function(jqxhr, status, error) {

                },
                function(msg) {

                });
            }
      }
      else if(href.match("#job-application-content$") && (content_cache["job-application-content"] == undefined || !content_cache["job-application-content"])) {
            History.replaceState({}, null, "?tab=applications");
            load_tutor_section_contents({"section": "job_application"},function(data) {

                $("#job-application-content").find(".data_msng").html(data.data.content);
                content_cache["job-application-content"] = true;
                my_pagination("id_job_application_content_pagination","id_job_application_content","job_application",1,data.data.extra.total_pages,8)
            },
            function(jqxhr, status, error) {

            },
            function(msg) {

            });
      }
      else if(href.match("#students-active-content$") && (content_cache["students-active-content"] == undefined || !content_cache["students-active-content"])) {
            History.replaceState({}, null, "?tab=active-students");
            load_tutor_section_contents({"section": "students_active"},function(data) {
                console.log(data.data.content);
                $("#students-active-content").find(".data_msng").html(data.data.content);
                content_cache["students-active-content"] = true;
                //my_pagination("id_student_active_pagination","id_student_active_content","students_active",1,data.data.extra.total_pages,8);
            },
            function(jqxhr, status, error) {

            },
            function(msg) {

            });
      }
      else if(href.match("#students-inactive-content$") && (content_cache["students-inactive-content"] == undefined || !content_cache["students-inactive-content"])) {
            History.replaceState({}, null, "?tab=inactive-students");
            load_tutor_section_contents({"section": "students_inactive"},function(data) {
                $("#students-inactive-content").find(".data_msng").html(data.data.content);
                content_cache["students-inactive-content"] = true;
                //my_pagination("id_student_active_pagination","id_student_active_content","students_active",1,data.data.extra.total_pages,8);
            },
            function(jqxhr, status, error) {

            },
            function(msg) {

            });
      }

      //Students sections start here.
      else if(href.match("#champ-request-tutor$")) {
            History.replaceState({}, null, "?tab=job-post");
      }

      else if(href.match("#champ-active-requests$")) {
          update_address_url_my_jobs();
          load_my_jobs_contents();
      }

      else if(href.match("#champ-top-tutor$")) {
          History.replaceState({}, null, "?tab=my-tutors");
          load_tutor_section_contents({"section": "my_top_tutors"},function(data) {
                $("#id_my_top_tutors").html(data.data.content);
                content_cache["id_my_top_tutors"] = true;
                //my_pagination("id_student_active_pagination","id_student_active_content","students_active",1,data.data.extra.total_pages,8);
            },
            function(jqxhr, status, error) {

            },
            function(msg) {

            });
      }
      else if(href.match("#champ-post-question$")) {
          History.replaceState({}, null, "?tab=post-question");
      }
      else if(href.match("#champ-questions$")) {
          History.replaceState({}, null, "?tab=my-questions");
      }
      else if(href.endsWith("#video-content")) {
          History.replaceState({}, null, "?tab=my-videos");
      }
      else if(href.endsWith("#top-students")) {
          History.replaceState({}, null, "?tab=top-students");
          load_tutor_section_contents({"section": "top_students"},function(data) {
            $("#id_top_students").html(data.data.content);
            content_cache["id_top_students"] = true;
            //my_pagination("id_student_active_pagination","id_student_active_content","students_active",1,data.data.extra.total_pages,8);
        },
        function(jqxhr, status, error) {

        },
        function(msg) {

        });
      }
      else if(href.endsWith("#champ-upcoming")) {
          History.replaceState({}, null, "?tab=upcoming");
          load_tutor_section_contents({"section": "upcoming"},function(data) {
            $("#id_champ_upcoming_content").html(data.data.content);
            content_cache["id_champ_upcoming_content"] = true;
            //my_pagination("id_student_active_pagination","id_student_active_content","students_active",1,data.data.extra.total_pages,8);
        },
        function(jqxhr, status, error) {

        },
        function(msg) {

        });
      }
      else if(href.endsWith("champ-progress-report")) {
          History.replaceState({}, null, "?tab=progress-reports");
          load_tutor_section_contents({"section": "progress_reports"},function(data) {
                $("#id_progress_report_content").html(data.data.content);
                content_cache["id_progress_report_content"] = true;
                //my_pagination("id_student_active_pagination","id_student_active_content","students_active",1,data.data.extra.total_pages,8);
            },
            function(jqxhr, status, error) {

            },
            function(msg) {

            });
      }
      else if(href.match("#champ-profile$")) {
          History.replaceState({}, null, "?tab=profile");
      }
      else if(href.match("#champ-reviews$")) {
          History.replaceState({}, null, "?tab=reviews");
      }
    }
  });


  $(document).ready(function() {
//        $(".tutors_chat").click(function(){
//            $('.tutors_chat_detail').toggleClass('displayblock');
//            $('.outer_tutors_expend').toggleClass('mini');
//            $('.main').toggleClass('mainedit');
//            $('header > .wrapper').toggleClass('wrapperedit');
//        });

        $(window).scroll(function() {
           if($(window).scrollTop() + $(window).height() == $(document).height()) {
                $("#id_my_jobs_load_more").show();
                $("#id_my_jobs_load_more_button").hide();
                load_my_jobs_contents(true, true, function(data) {
                    $("#id_my_jobs_load_more").hide();
                    $("#id_my_jobs_load_more_button").hide();
                }, function(jqxhr, status, error) {
                    $("#id_my_jobs_load_more").hide();
                    $("#id_my_jobs_load_more_button").show();
                }, function(msg) {

                });
           }
        });


        $('.splitter').click(function() {
            $(this).toggleClass("splitter_active");
            $(".aside_right").toggleClass("aside_right_hide");
            $("#w").toggleClass("full_width");
            $("#content").toggleClass("content_full");
        });

        $('.splitter1').click(function() {
            $(this).toggleClass("splitter_active");
            $("#sidemenu").toggleClass("side_menu_hide");
            $("#content").find(".contentblock").toggleClass("wide_contentblock");
        });


    });

});

function my_pagination(element_id,data_element_id,section,current_page,total_pages,number_of_pages) {
    if(total_pages > 1)
    {
            var options = {
            currentPage: current_page,
            totalPages: total_pages,
            numberOfPages:number_of_pages,
            itemContainerClass: function (type, page, current) {
                return (page === current) ? "active" : "pointer-cursor";
            },
            itemTexts: function (type, page, current) {
                switch (type) {
                case "first":
                    return "First";
                case "prev":
                    return "Previous";
                case "next":
                    return "Next";
                case "last":
                    return "Last";
                case "page":
                    return page;
                }
            },
            tooltipTitles: function (type, page, current) {
                switch (type) {
                case "first":
                    return "Go To First Page";
                case "prev":
                    return "Go To Previous Page";
                case "next":
                    return "Go To Next Page";
                case "last":
                    return "Go To Last Page";
                case "page":
                    return "Go to page " + page;
                }
            },
            bootstrapTooltipOptions: {
                html: true,
                placement: 'bottom'
            },
            itemContainerClass: function (type, page, current) {
                return (page === current) ? "active" : "pointer-cursor";
            },
            pageUrl: function(type, page, current){

                //return "/ajax/profile-section";

            },
            onPageClicked: function(e,originalEvent,type,page){

                var currentTarget = $(e.currentTarget);

                //$('#'+data_element_id).parent().find(".overlay").css("height","100000px");
                $('#'+data_element_id).css("opacity","0.5");
                var action_tab = $("input[name=action_tab]");
                if(action_tab != undefined) {
                    action_tab = action_tab.val();
                }
                else {
                    action_tab = "";
                }
//                $('#'+data_element_id).hide();
                load_tutor_section_contents({"section": section,"page":page, action_tab: action_tab },function(data) {
                    $('#'+data_element_id).html(data.data.content);
//                    $('#'+data_element_id).show();
                    $('#'+data_element_id).css("opacity","1.0");
                },
                function(jqxhr, status, error) {

                },
                function(msg) {

                });
            },
            shouldShowPage:function(type, page, current){
                return true;
            }
        }

        $('#'+element_id).bootstrapPaginator(options);
    }
}

$(document).on("click",".ji_action_tab_item", function(e) {
   e.preventDefault();
   var action_tab = $(this).data("ji-section");
   load_tutor_section_contents({"section": "job_invitation", "action_tab": action_tab},function(data) {
        $("#jobs-invitation-content").find(".data_msng").html(data.data.content);
        my_pagination("id_job_invitation_content_pagination","id_job_invitation_content","job_invitation",1,data.data.extra.total_pages,8);
    },
    function(jqxhr, status, error) {

    },
    function(msg) {

    });
});

$(document).on("click",".jr_action_tab_item", function(e) {
   e.preventDefault();
   var action_tab = $(this).data("jr-section");
   load_tutor_section_contents({"section": "job_recommendation", action_tab: action_tab },function(data) {
        $("#jobs-recommendation-content").find(".data_msng").html(data.data.content);
        my_pagination("id_job_recommendation_content_pagination","id_job_recommendation_content","job_recommendation",1,data.data.extra.total_pages,8);
    },
    function(jqxhr, status, error) {

    },
    function(msg) {

    });
});

$(document).on("click",".ja_action_tab_item", function(e) {
   e.preventDefault();
   var action_tab = $(this).data("ja-section");
   load_tutor_section_contents({"section": "job_application", action_tab: action_tab},function(data) {
        $("#job-application-content").find(".data_msng").html(data.data.content);
        content_cache["job-application-content"] = true;
        my_pagination("id_job_application_content_pagination","id_job_application_content","job_application",1,data.data.extra.total_pages,8)
    },
    function(jqxhr, status, error) {

    },
    function(msg) {

    });
});

$(document).on("click",".nf_action_tab_item", function(e) {
   e.preventDefault();
   var action_tab = $(this).data("nf-section");
   load_tutor_section_contents({"section": "job_feed", action_tab: action_tab},function(data) {
        $("#feed-content").find(".data_msng").html(data.data.content);
        my_pagination("id_feed_content_pagination","id_job_feed_content","job_feed",1,data.data.extra.total_pages,8)
    },
    function(jqxhr, status, error) {

    },
    function(msg) {

    });
});

function collect_request_data_for_new_items() {
    var request_data = {};
//    var nf_last_dt = $(".nf_last_dt_item").val();
//    request_data["nf_last_dt"] = nf_last_dt
    var ji_last_dt = $(".ji_last_dt_item").val();
    request_data["ji_last_dt"] = ji_last_dt
    var jr_last_dt = $(".jr_last_dt_item").val();
    request_data["jr_last_dt"] = jr_last_dt
    return request_data
}

function show_new_item_in_feed(data) {
    //console.log(data)
    var new_items = data.data.new_items;
    //console.log(new_items.qfeed_new_item_count);
    if(new_items.qfeed_new_item_count != undefined && new_items.qfeed_new_item_count != null && new_items.qfeed_new_item_count >= 0) {
        $("#id_nf_new_item_display_container").find(".new_item_available").text(new_items.qfeed_new_item_count+" new items");
        if(new_items.qfeed_new_item_count > 0) {
            $("#id_nf_new_item_display_container").fadeIn(1000);
        }
        else {
            $("#id_nf_new_item_display_container").hide();
        }
    }
    if(new_items.ji_new_item_count != undefined && new_items.ji_new_item_count != null && new_items.ji_new_item_count >= 0) {
        $("#id_ji_new_item_display_container").find(".new_item_available").text(new_items.ji_new_item_count+" new items");
        if(new_items.ji_new_item_count > 0) {
            $("#id_ji_new_item_display_container").fadeIn(1000);
        }
        else {
            $("#id_ji_new_item_display_container").hide();
        }
    }
    if(new_items.jr_new_item_count != undefined && new_items.jr_new_item_count != null && new_items.jr_new_item_count >= 0) {
        $("#id_jr_new_item_display_container").find(".new_item_available").text(new_items.jr_new_item_count+" new items");
        if(new_items.jr_new_item_count > 0) {
            $("#id_jr_new_item_display_container").fadeIn(1000);
        }
        else {
            $("#id_jr_new_item_display_container").hide();
        }
        $(".jr_last_dt_item").val(new_items.jr_last_item_date);
    }
    if(new_items.ji_total_item_count != undefined && new_items.ji_total_item_count != null && new_items.ji_total_item_count >= 0) {
        $("#id_ji_total_count_panel").text(new_items.ji_total_item_count);
        if(new_items.ji_total_item_count > 0) {
            $("#id_ji_total_count_panel").fadeIn(1000);
        }
        else {
            $("#id_ji_total_count_panel").hide();
        }
    }
    if(new_items.jr_total_item_count != undefined && new_items.jr_total_item_count != null && new_items.jr_total_item_count >= 0) {
        $("#id_jr_total_count_panel").text(new_items.jr_total_item_count);
        if(new_items.jr_total_item_count > 0) {
            $("#id_jr_total_count_panel").fadeIn(1000);
        }
        else {
            $("#id_jr_total_count_panel").hide();
        }
    }
}

function retrieve_new_profile_items() {
    $.ajax({
        type: "GET",
        url: "/ajax/fetch-profile-section",
        timeout: 3000,
        data: collect_request_data_for_new_items(),
        enctype: 'multipart/form-data',
        success: function(data)
        {
            var data = jQuery.parseJSON(data);
            show_new_item_in_feed(data);
        },
        error: function(jqxhr, status, error)
        {

        }
    })
    .done(function( msg ) {

    });
}

setInterval( retrieve_new_profile_items, 5000);


$(document).on("click",".new_item_available_link", function(e) {
    e.preventDefault();
    var section = $(this).parent().find(".section").val();
    load_tutor_section_contents({"section": section},function(data) {

        if(section == "job_feed") {
           $("#feed-content").find(".data_msng").html(data.data.content);
            my_pagination("id_feed_content_pagination","id_job_feed_content","job_feed",1,data.data.extra.total_pages,8)
        }
        else if(section == "job_invitation") {
            $("#jobs-invitation-content").find(".data_msng").html(data.data.content);
            my_pagination("id_job_invitation_content_pagination","id_job_invitation_content","job_invitation",1,data.data.extra.total_pages,8);
        }
        else if(section == "job_recommendation") {
            $("#jobs-recommendation-content").find(".data_msng").html(data.data.content);
            my_pagination("id_job_recommendation_content_pagination","id_job_recommendation_content","job_recommendation",1,data.data.extra.total_pages,8);
        }
    },
    function(jqxhr, status, error) {

    },
    function(msg) {

    });
});

$(document).ready(function() {
    var options =  {
        iframe:true,
        opacity: 0.1,
        width:"70%",
        height:"500px",
        scrolling: false,
        photo: true,
        escKey: false,
        fixed: false,
        overlayClose: false
    };

    $("#id_chat_block_manage").colorbox(options);
});