function load_profile_picture_dialog() {
    $("#id_overlay_loading").show();
    $("#black_overlay").fadeOut('fast');
    $.ajax({
        type: "GET",
        url: "/ajax/profile-picture/",
        data: {},
        success: function(data)
        {
            data = jQuery.parseJSON(data);
            if(data.status == "SUCCESS"){
                $("#black_overlay").show();
                $("#id_ajax_rendered_content").html(data.data.dialog_content);
                $("#id_ajax_rendered_content").fadeIn('fast');
            }
            else if(data.STATUS == "FAILURE"){

            }
        },
        error: function(jqxhr, status, error)
        {

        }
    })
    .done(function( msg ) {
        $("#id_overlay_loading").hide();
    });
}

$(document).ready(function() {

    $("#black_overlay").click(function(e) {
        $("#id_ajax_rendered_content").hide();
        $(this).hide();
    });

    $(document).on("click", "#id_job_apply_cancel",function(e) {
        $("#id_ajax_rendered_content").hide();
        $("#black_overlay").hide();
    });

    $("#id_change_pp").click(function(e) {
        e.preventDefault();
        load_profile_picture_dialog();
    });
});