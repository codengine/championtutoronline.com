$(document).ready(function() {

    $(document).on("change", ".job_details_action", function(e) {
        e.preventDefault();
        var selected_value = $(this).val();
        if(selected_value == "hire") {
            var job_id = $(this).parent().parent().find(".job_id").val();
            var tutor_id = $(this).parent().parent().find(".tutor_id").val();
            show_applicant_hire_dialog(job_id, tutor_id);
        }
        else if(selected_value == "negotiate") {
            var tutor_id = $(this).parent().find(".tutor_id").val();
            var job_id = $(this).parent().find(".job_id").val();

            var online_status = "online";
            var tutor_name = $(this).parent().parent().find(".tutor_name").val();
            openChatPopup(0, tutor_id, tutor_name, online_status, [ parseInt(tutor_id) ], "", "", 1);
        }
        else if(selected_value == "shortlist") {
            var tutor_id = $(this).parent().parent().find(".tutor_id").val();
            var job_id = $(this).parent().parent().find(".job_id").val();
            $.ajax({
                type: "POST",
                url: "/ajax/update_job_status_shortlisted/",
                data: { job_id: job_id, tutor_id: tutor_id },
                enctype: 'multipart/form-data',
                success: function(data)
                {
                    var data = jQuery.parseJSON(data);
                    if(data.status == "SUCCESS") {
                        location.reload();
                    }
                },
                error: function(jqxhr, status, error)
                {
                    BootstrapDialog.show({
                        title: "Failed",
                        message: "Operation couldn't be performed. There was a problem while processing the request. Please retry again or reload the page.",
                        closable: false,
                        type: BootstrapDialog.TYPE_DANGER,
                        size: BootstrapDialog.SIZE_NORMAL,
                        buttons: [
                            {
                                label: "Ok",
                                cssClass: 'btn btn-md btn-primary width120',
                                action: function(dialogRef) {
                                    dialogRef.close();
                                }
                            },
                            {
                                label: "Cancel",
                                cssClass: 'btn btn-md btn-warning width120',
                                action: function(dialogRef) {
                                    dialogRef.close();
                                }
                            }]
                    });
                }
            })
            .done(function( msg ) {

            });
        }
        else if(selected_value == "reject") {
            var job_id = $(this).parent().parent().find(".job_id").val();
            show_job_reject_dialog(job_id);
        }
        else if(selected_value == "reject_recommendation") {
            var job_id = $(this).parent().parent().find(".job_id").val();
            show_job_recommendation_reject_dialog(job_id);
        }
        else if(selected_value == "reject-application") {
            var tutor_id = $(this).parent().parent().find(".tutor_id").val();
            var job_id = $(this).parent().parent().find(".job_id").val();
            show_applicant_reject_dialog(job_id, tutor_id);
        }
        else if(selected_value == "apply") {
            var job_id = $(this).parent().parent().find(".job_id").val();
            show_job_apply_dialog(job_id);
        }
        else if(selected_value == "withdraw") {
            var job_id = $(this).parent().parent().find(".job_id").val();
            var user_id = $(this).data("user-id");
            show_application_withdraw_dialog(job_id, user_id);
        }
    });

});