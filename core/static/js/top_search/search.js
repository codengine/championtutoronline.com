$(document).ready(function(){

    var delay = (function(){
        var timer = 0;
        return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
        };
    })();

    //For tutor search in student page
    $("#id_tutor_ajax_search").click(function(e){
        e.preventDefault();
        $(".tutor-search").focus();
        return false;
    });

    function handle_tutor_search(term){
        $(".search-result").css("height","450px");
        $("#id_loading_search_icon_container").show();
        $.ajax({
            url: "/ajax/tutor-search/",
            dataType: "json",
            data: {
                term: term
            },
            success: function(data) {
                //data = jQuery.parseJSON(data);
                console.log(data);
                if(data.status == "SUCCESS"){
                    $(".search-result").show();
                    $("#id_loading_search_icon_container").hide();
                    $("#id_search_result_container").show();
                    $("#id_search_result_container").html(data.data);
                }
                else{
                    console.log("There was a problem with the search. Please try again later or refresh the page.");
                }
            },
            complete: function(){

            }
        });
    };

    function search_tutors(){
        var _this = $(".tutor-search");
        if(_this.val() != ""){
            handle_tutor_search(_this.val());
        }
    };

    $(".tutor-search").keyup(function(e){
        delay(search_tutors,500);
    });

    $(".tutor-search").focus(function(e){
        $(".search-result").show();
        $("#id_loading_search_icon_container").show();
        $("#id_search_result_container").show();
        var _this = $(this);
        if(_this.val() == ""){
            $("#id_search_result_container").text("Type to start search");
            $(".search-result").css("height","53px");
            $("#id_loading_search_icon_container").hide();
        }
        if(_this.val() != ""){
            handle_tutor_search(_this.val());
        }
    });

    //For student search in tutor page
    $("#id_student_ajax_search").click(function(e){
        e.preventDefault();
        $(".student-search").focus();
        return false;
    });

    function handle_student_job_search_top(term){
        $(".search-result").css("height","450px");
        $("#id_loading_search_icon_container").show();
        $.ajax({
            url: "/ajax/jobs-student-search/",
            dataType: "json",
            data: {
                term: term
            },
            success: function(data) {
                //data = jQuery.parseJSON(data);
                //console.log(data);
                if(data.status == "SUCCESS"){
                    $(".search-result").show();
                    $("#id_loading_search_icon_container").hide();
                    $("#id_search_result_container").show();
                    $("#id_search_result_container").html(data.data);
                }
                else{
                    console.log("There was a problem with the search. Please try again later or refresh the page.");
                }
            },
            complete: function(){

            }
        });
    };

    function search_jobs_top(){
        var _this = $(".student-search");
        if(_this.val() != ""){
            handle_student_job_search_top(_this.val());
        }
    };

    $(".student-search").keyup(function(e){
        delay(search_jobs_top,500);
    });

    $(".student-search").focus(function(e){
        $(".search-result").show();
        $("#id_loading_search_icon_container").show();
        $("#id_search_result_container").show();
        var _this = $(this);
        if(_this.val() == ""){
            $("#id_search_result_container").text("Type to start search");
            $(".search-result").css("height","53px");
            $("#id_loading_search_icon_container").hide();
        }
        if(_this.val() != ""){
            handle_student_job_search_top(_this.val());
        }
    });
});