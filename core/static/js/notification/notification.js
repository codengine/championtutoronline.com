$(document).ready(function()
{
    if(window.champ_user_id != undefined && window.champ_user_id != null && window.socket != undefined && window.socket != null) {
        window.socket.on("PN_RECEIVE", function(data)
        {
            var data = jQuery.parseJSON(data);
            console.log(data);
            if(data.action == "new") {
                $(".object_id").each(function(i) {
                    console.log($(this).val());
                    console.log(data.object_id);
                    if($(this).val() == data.object_id) {
                        $(this).parent().parent().remove();
                    }
                });
                if(data.unread_count > 0) {
                    $("#id_notif_counter").text(data.unread_count);
                    $("#id_notif_counter").show();
                    if(data.unread_count > 0) {
                        $("#id_new_notification_container2").hide();
                    }
                    $("#id_new_notification_container").prepend(data.notif_item);
                    if($("#id_notif_page_content").length) {
                        $("#id_notif_page_content").prepend(data.notif_item);
                    }
                }
            }
            else if(data.action == "modify") {
                if(data.unread_count > 0) {
                    $("#id_new_notification_container2").hide();
                    $("#id_notif_counter").text(data.unread_count);
                    $("#id_notif_counter").show();
                }
                else {
                    $("#id_notif_counter").text(0);
                    $("#id_notif_counter").hide();
                }
                $(".object_id").each(function(i) {
                    if(parseInt($(this).val()) == data.object_id) {
                        $(this).parent().find(".is_read").val("1");
                        $(this).parent().find(".mark_notification").removeClass("mark_unread");
                        $(this).parent().find(".mark_notification").addClass("mark_read");
                    }
                });
            }
            else if(data.action == "modify_all") {
                $(".mark_notification").each(function(i) {
                    $(this).removeClass("mark_unread");
                    $(this).addClass("mark_read");
                });
                $("#id_notif_counter").hide();
            }
            else if(data.action == "delete") {
                if(data.unread_count > 0) {
                    $("#id_new_notification_container2").hide();
                    $("#id_notif_counter").text(data.unread_count);
                    $("#id_notif_counter").show();
                }
                else {
                    $("#id_notif_counter").text(0);
                    $("#id_notif_counter").hide();
                }
                $(".object_id").each(function(i) {
                    if(parseInt($(this).val()) == data.object_id) {
                        $(this).parent().parent().remove();
                    }
                });
            }
            //console.log(data);
            if(data.active_new) {
                if(data.active_new.new_invite_count && data.active_new.new_invite_count > 0) {
                    var $new_invite_count_label = $(".new_invite_count");
                    if($new_invite_count_label.length) {
                        $new_invite_count_label.text(data.active_new.new_invite_count);
                        $new_invite_count_label.show();
                    }
                }
                else {
                    var $new_invite_count_label = $(".new_invite_count");
                    $new_invite_count_label.text(0);
                    $new_invite_count_label.hide();
                }
                if(data.active_new.new_recommend_count && data.active_new.new_recommend_count > 0) {
                    var $job_recommend_count_label = $(".new_recommend_count");
                    if($job_recommend_count_label.length) {
                        $job_recommend_count_label.text(data.active_new.new_recommend_count);
                        $job_recommend_count_label.show();
                    }
                }
                else {
                    var $job_recommend_count_label = $(".new_recommend_count");
                    $job_recommend_count_label.text(0);
                    $job_recommend_count_label.hide();
                }
            }
            //console.log(data);
        });
    }

//    $(".notification_item").hover(function() {
//        $(this).find(".notification_item_action").show();
//    },
//    function() {
//        $(this).find(".notification_item_action").hide();
//    });

});

function load_unread_notifications(mark_read,domain) {
    $("#id_no_new_notif").hide();
    $("#id_notification_loading").show();
    $.ajax({
        type: "GET",
        url: "/notification/ajax/notification_action/",
        data: { action: "load", domain: domain, mark_read: mark_read },
        timeout: 3000,
        enctype: 'multipart/form-data',
        success: function(data)
        {
            var data = jQuery.parseJSON(data);
            if(data.status == "SUCCESS") {
                if(data.data.unread_notifications.length > 0) {
                    $("#id_new_notification_container2").hide();
                }
                $("#id_notif_counter").hide();
                $('#id_new_notification_container').html("");
                if(data.data.unread_notifications.length > 0) {
                    $("#id_no_new_notif").show();
                    $("#id_notification_loading").hide();
                }
                else {
                    $("#id_notification_loading").hide();
                }

                $('#id_new_notification_header').parent().find('li:not(:first)').remove();

                for(var i = 0; i < data.data.unread_notifications.length; i++) {
                    var notification_item = data.data.unread_notifications[i];
                    $('#id_new_notification_header').parent().append(notification_item);
                }
                $(".mark_notification").each(function(i) {
                    $(this).removeClass("mark_unread");
                    $(this).addClass("mark_read");
                });
            }
        },
        error: function(jqxhr, status, error)
        {
            if(status === 'timeout') {
                //Timed Out
            }
        }
    })
    .done(function( msg ) {

    });
}

$(document).on("click","#id_notif_icon", function(e) {
    e.preventDefault();
    load_unread_notifications(true, "all");
});

//load_unread_notifications(false, "unread");

function notification_action(action, object_id, value, success_callback, error_callback, complete_callback) {
    $.ajax({
        type: "POST",
        url: "/notification/ajax/notification_action/",
        data: { action: action, object_id: object_id, mark_read: value },
        timeout: 3000,
        enctype: 'multipart/form-data',
        success: function(data)
        {
            var data = jQuery.parseJSON(data);
            success_callback(data);
        },
        error: function(jqxhr, status, error)
        {
            error_callback(jqxhr, status, error);
        }
    })
    .done(function( msg ) {
        complete_callback(msg);
    });
}

$(document).on("click",".mark_unread", function(e) {
    e.preventDefault();
    var action = "update";
    var object_id = $(this).parent().find(".object_id").val();
    var value = $(this).parent().find(".is_read").val();
    if(value == "1") {
        value = "0";
    }
    else {
        value = "1";
    }
    var $this = $(this);
    notification_action(action, object_id, value, function(data) {
        if(data.status == "SUCCESS") {

        }
    },
    function(jqxhr, status, error) {

    },
    function(msg) {

    });
});

$(document).on("click",".delete_notification", function(e) {
    e.preventDefault();
    var action = "delete";
    var object_id = $(this).parent().find(".object_id").val();
    var value = "";
    var $this = $(this);
    notification_action(action, object_id, value, function(data) {
        if(data.status == "SUCCESS") {
            $this.parent().parent().fadeOut(1000);
            $this.parent().parent().remove();
        }
    },
    function(jqxhr, status, error) {

    },
    function(msg) {

    });
});