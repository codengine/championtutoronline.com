var on_mouse_out = function(e)
{
    if(whiteboard.canvas_selected)
    {
        whiteboard.canvas_data_bfr_drawng_start = whiteboard.context.getImageData(0,0,whiteboard.canvas.width,whiteboard.canvas.height);
        whiteboard.canvas_data_array.push(whiteboard.canvas_data_bfr_drawng_start);

        if(whiteboard.selected_tool.name == "Pen")
        {
            ////console.log(whiteboard.selected_tool.points);
            if(whiteboard.selected_tool.points.length > 0)
            {
                whiteboard.selected_tool.calculate_area_points();
                var cloned_obj = deep_copy(whiteboard.selected_tool);
                whiteboard.canvas_data_stack.push(cloned_obj);
            }
            whiteboard.selected_tool.points = [];
        }
        else if(whiteboard.selected_tool.name == "Eraser")
        {
            if(whiteboard.selected_tool.points.length > 0)
            {
                var cloned_obj = deep_copy(whiteboard.selected_tool);
                whiteboard.canvas_data_stack.push(cloned_obj);
            }

            whiteboard.selected_tool.points = [];

            // for(var i =0; i  < whiteboard.canvas_data_stack.length ; i++)
            // {
            //     console.log("Object: "+ i);
            //     console.log(whiteboard.canvas_data_stack[i].points);
            // }
        }
        else if(whiteboard.selected_tool.name == "Brush")
        {
            if(whiteboard.selected_tool.points.length > 0)
            {
                whiteboard.selected_tool.calculate_area_points();
                var cloned_obj = deep_copy(whiteboard.selected_tool);
                whiteboard.canvas_data_stack.push(cloned_obj);
            }

            whiteboard.selected_tool.points = [];

            /*for(var i =0; i  < whiteboard.canvas_data_stack.length ; i++)
            {
                //console.log("Object: "+ i);
                //console.log(whiteboard.canvas_data_stack[i].points);
            }*/

        }
        else if(whiteboard.selected_tool.name == "Line")
        {
            //console.log(whiteboard.selected_tool.points);
            whiteboard.selected_tool.calculate_area_points();

            var cloned_obj = deep_copy(whiteboard.selected_tool);
            whiteboard.canvas_data_stack.push(cloned_obj);

            whiteboard.selected_tool.points = [];

            /*
            for(var i =0; i  < whiteboard.canvas_data_stack.length ; i++)
            {
                //console.log("Object: "+ i);
                //console.log(whiteboard.canvas_data_stack[i].points);
            }
            */

        }
        else if(whiteboard.selected_tool.name == "Text"){

            whiteboard.drawing_action.redraw();

            var text_val = whiteboard.selected_tool.get_textarea_text();
            if(text_val == undefined || text_val == "")
            {
                var cpoint = new Point(whiteboard.draw_starting_point.X,whiteboard.draw_starting_point.Y);
                whiteboard.selected_tool.x = cpoint.X;
                whiteboard.selected_tool.y = cpoint.Y;
                whiteboard.selected_tool.rect_width = Math.abs(whiteboard.current_point.X - whiteboard.draw_starting_point.X);
                whiteboard.selected_tool.rect_height = Math.abs(whiteboard.current_point.Y - whiteboard.draw_starting_point.Y);
                whiteboard.selected_tool.margin_top = parseInt(GetTopLeft("id_canvas_tab").y+30)
                //whiteboard.selected_tool.text = "";

                //whiteboard.selected_tool.draw_rectangle();

                whiteboard.selected_tool.place_textarea(e);

                whiteboard.canvas_data_bfr_drawng_start = whiteboard.context.getImageData(0,0,whiteboard.canvas.width,whiteboard.canvas.height);
            }
            else
            {
                whiteboard.selected_tool.text = text_val;
                whiteboard.selected_tool.update_textarea_size();
                whiteboard.selected_tool.draw_text();
                var cloned_obj = deep_copy(whiteboard.selected_tool);
                whiteboard.canvas_data_stack.push(cloned_obj);
                whiteboard.selected_tool.remove_textarea();
                whiteboard.selected_tool.clean_tool();
            }
            whiteboard.selected_tool.area_points = [];
            whiteboard.canvas_selected = false;
        }
        else if(whiteboard.selected_tool.name == "Arrow")
        {
            whiteboard.selected_tool.points.push(whiteboard.current_point.X);
            whiteboard.selected_tool.points.push(whiteboard.current_point.Y);

            var cloned_obj = deep_copy(whiteboard.selected_tool);
            whiteboard.canvas_data_stack.push(cloned_obj);

            whiteboard.selected_tool.points = [];
        }
        else if(whiteboard.selected_tool.name == "Circle")
        {
            var start_point = new Point(whiteboard.selected_tool.center_point[0],whiteboard.selected_tool.center_point[1]);
            var current_point = new Point(whiteboard.current_point.X,whiteboard.current_point.Y);
            var circle_radius = euclidian_distance(start_point,current_point);
            whiteboard.selected_tool.center_point = [];
            whiteboard.selected_tool.center_point.push(start_point.X);
            whiteboard.selected_tool.center_point.push(start_point.Y);
            whiteboard.selected_tool.radius = circle_radius;
            //whiteboard.selected_tool.offset_x = start_point.X - circle_radius;
            //whiteboard.selected_tool.offset_y = start_point.Y - circle_radius;
            whiteboard.selected_tool.calculate_area_points();
            var cloned_obj = deep_copy(whiteboard.selected_tool);
            whiteboard.canvas_data_stack.push(cloned_obj);
            /*
            for(var i =0; i  < whiteboard.canvas_data_stack.length ; i++)
            {
                //console.log("Object: "+ i);
                //console.log(whiteboard.canvas_data_stack[i]);
            }
            */
            whiteboard.selected_tool.center_point = [];
            whiteboard.selected_tool.radius = 0;
        }
        else if(whiteboard.selected_tool.name == "CircleHexa" || whiteboard.selected_tool.name == "Triangle" || whiteboard.selected_tool.name == "CirclePenta")
        {
            //Put object with current status into the stack.
            var start_point = new Point(whiteboard.selected_tool.center_point[0],whiteboard.selected_tool.center_point[1]);
            var current_point = new Point(whiteboard.current_point.X,whiteboard.current_point.Y);

            var circle_radius = euclidian_distance(start_point,current_point);
            whiteboard.selected_tool.radius = circle_radius;

            whiteboard.selected_tool.points = [];

            whiteboard.selected_tool.points.push(start_point.X);
            whiteboard.selected_tool.points.push(start_point.Y);
            whiteboard.selected_tool.points.push(current_point.X);
            whiteboard.selected_tool.points.push(current_point.Y);

            whiteboard.selected_tool.calculate_area_points();

            var cloned_obj = deep_copy(whiteboard.selected_tool);
            whiteboard.canvas_data_stack.push(cloned_obj);

            //reset values.
            whiteboard.selected_tool.center_point = [];
            whiteboard.selected_tool.points = [];
            whiteboard.selected_tool.radius = 0;
        }
        else if(whiteboard.selected_tool.name == "Rectangle")
        {
            var starting_point = whiteboard.selected_tool.draw_starting_point;

            var offX = $("#drawing_board").offset().left;
            var offY = $("#drawing_board").offset().top;
            var current_point = new Point(e.pageX - offX, e.pageY - offY);

            //console.log("Output starting and current points.");
            //console.log(starting_point);
            //console.log(current_point);

            var p1 = [],p2 = [],p3 = [],p4 = [];

            if(starting_point.X < current_point.X && starting_point.Y < current_point.Y)
            {
                p1 = [starting_point.X, starting_point.Y];
                p2 = [current_point.X, starting_point.Y];
                p3 = [current_point.X, current_point.Y];
                p4 = [starting_point.X, current_point.Y];
            }
            else if(starting_point.X > current_point.X && starting_point.Y < current_point.Y)
            {
                p1 = [current_point.X, starting_point.Y];
                p2 = [starting_point.X, starting_point.Y];
                p3 = [starting_point.X, current_point.Y];
                p4 = [current_point.X, current_point.Y];
            }
            else if(starting_point.X > current_point.X && starting_point.Y > current_point.Y)
            {
                p1 = [current_point.X, current_point.Y];
                p2 = [starting_point.X, current_point.Y];
                p3 = [starting_point.X, starting_point.Y];
                p4 = [current_point.X, starting_point.Y];
            }
            else if(starting_point.X < current_point.X && starting_point.Y > current_point.Y)
            {
                p1 = [starting_point.X, current_point.Y];
                p2 = [current_point.X, current_point.Y];
                p3 = [current_point.X, starting_point.Y];
                p4 = [starting_point.X, starting_point.Y];
            }

            if(p1.length > 0 && p2.length > 0 && p3.length > 0 && p4.length > 0)
            {
                whiteboard.selected_tool.points = [];
                whiteboard.selected_tool.points.push(p1[0]);
                whiteboard.selected_tool.points.push(p1[1]);
                whiteboard.selected_tool.points.push(p2[0]);
                whiteboard.selected_tool.points.push(p2[1]);
                whiteboard.selected_tool.points.push(p3[0]);
                whiteboard.selected_tool.points.push(p3[1]);
                whiteboard.selected_tool.points.push(p4[0]);
                whiteboard.selected_tool.points.push(p4[1]);
            }

            whiteboard.selected_tool.calculate_area_points();

            var cloned_obj = deep_copy(whiteboard.selected_tool);
            whiteboard.canvas_data_stack.push(cloned_obj);

            whiteboard.selected_tool.points = [];

        }
        else if(whiteboard.selected_tool.name == "Axis")
        {
            var starting_point = new Point(whiteboard.selected_tool.start_point[0],whiteboard.selected_tool.start_point[1]);

            var offX = $("#drawing_board").offset().left;
            var offY = $("#drawing_board").offset().top;
            var current_point = new Point(e.pageX - offX, e.pageY - offY);

            var p1 = [],p2 = [],p3 = [],p4 = [];

            if(starting_point.X < current_point.X && starting_point.Y < current_point.Y)
            {
                p1 = [starting_point.X, starting_point.Y];
                p2 = [current_point.X, starting_point.Y];
                p3 = [current_point.X, current_point.Y];
                p4 = [starting_point.X, current_point.Y];
            }
            else if(starting_point.X > current_point.X && starting_point.Y < current_point.Y)
            {
                p1 = [current_point.X, starting_point.Y];
                p2 = [starting_point.X, starting_point.Y];
                p3 = [starting_point.X, current_point.Y];
                p4 = [current_point.X, current_point.Y];
            }
            else if(starting_point.X > current_point.X && starting_point.Y > current_point.Y)
            {
                p1 = [current_point.X, current_point.Y];
                p2 = [starting_point.X, current_point.Y];
                p3 = [starting_point.X, starting_point.Y];
                p4 = [current_point.X, starting_point.Y];
            }
            else if(starting_point.X < current_point.X && starting_point.Y > current_point.Y)
            {
                p1 = [starting_point.X, current_point.Y];
                p2 = [current_point.X, current_point.Y];
                p3 = [current_point.X, starting_point.Y];
                p4 = [starting_point.X, starting_point.Y];
            }

            if(p1.length > 0 && p2.length > 0 && p3.length > 0 && p4.length > 0)
            {

                //whiteboard.selected_tool.update_position(p1[0],p1[1]);

                whiteboard.selected_tool.points = [];
                whiteboard.selected_tool.points.push(p1[0]);
                whiteboard.selected_tool.points.push(p1[1]);
                whiteboard.selected_tool.points.push(p2[0]);
                whiteboard.selected_tool.points.push(p2[1]);
                whiteboard.selected_tool.points.push(p3[0]);
                whiteboard.selected_tool.points.push(p3[1]);
                whiteboard.selected_tool.points.push(p4[0]);
                whiteboard.selected_tool.points.push(p4[1]);

                // console.log(p1);
                // console.log(p2);
                // console.log(whiteboard.selected_tool.points);

                //whiteboard.selected_tool.calculate_resize_options();

            }

            //console.log("Here is the updated object.");
            //console.log(whiteboard.selected_tool.anchors);

            whiteboard.selected_tool.calculate_area_points();

            var cloned_obj = deep_copy(whiteboard.selected_tool);
            whiteboard.canvas_data_stack.push(cloned_obj);

            whiteboard.selected_tool.reset_tool();

        }
        else if(whiteboard.selected_tool.name == "Select") {
            console.log("Mouse Up: Tools is Select...");

            console.log("Points: "+whiteboard.selected_tool.points);

//            var mx = parseInt(e.clientX - whiteboard.offsetX);
//            var my = parseInt(e.clientY - whiteboard.offsetY);
//
//            whiteboard.selected_tool.rect_area_end_point = [mx,my];
//
//            console.log(mx+" "+my);
//
//            var sp = whiteboard.selected_tool.rect_area_start_point;
//            var ep = whiteboard.selected_tool.rect_area_end_point;
//            whiteboard.selected_tool.points = [sp[0],sp[1],ep[0],ep[1]];
            whiteboard.selected_tool.calculate_area_points();


            whiteboard.context.lineWidth = 1;
            whiteboard.context.strokeStyle = "#C0C0C0";
            whiteboard.drawing_action.draw_points([10,10,100,10,100,200,10,200]);

            whiteboard.dragging = false;

            for(var i = whiteboard.canvas_data_stack.length - 1 ; i >= 0 ; i--){
                var cTool = whiteboard.canvas_data_stack[i];
                if(cTool.name == "Pen"){
                    console.log("selected_tool is Pen");
                    //cTool.calculate_area_points();
                    console.log(cTool.area_points);
                    whiteboard.drawing_action.draw_points(cTool.area_points);
                }
                else if(cTool.name == "Brush"){
                    console.log("selected_tool is Brush");
                    //cTool.calculate_area_points();
                    console.log(cTool.area_points);
                }
                else if(cTool.name == "Circle"){
                    console.log("selected_tool is Circle");
                    //cTool.calculate_area_points();
                    console.log(cTool.area_points);
                    if(cTool.check_if_hit(mx,my)){
                        cTool.draw_anchors();
                    }
                }
                else if(cTool.name == "CircleHexa"){
                    console.log("selected_tool is CircleHexa");
                    //cTool.calculate_area_points();
                    console.log(cTool.area_points);
                }
                else if(cTool.name == "CirclePenta"){
                    console.log("selected_tool is CirclePenta");
                    //cTool.calculate_area_points();
                    console.log(cTool.area_points);
                }
                else if(cTool.name == "Rectangle"){
                    console.log("selected_tool is Rectangle");
                    //cTool.calculate_area_points();
                    console.log(cTool.area_points);
                }
                else if(cTool.name == "Triangle"){
                    console.log("selected_tool is Triangle");
                    //cTool.calculate_area_points();
                    console.log(cTool.area_points);
                }
                else if(cTool.name == "Line"){
                    console.log("selected_tool is Line");
                    //cTool.calculate_area_points();
                    console.log(cTool.area_points);
                }
                else if(cTool.name == "Axis"){
                    console.log("selected_tool is Axis");
                    //cTool.calculate_area_points();
                    console.log(cTool.area_points);
                }
            }


        }
        whiteboard.drawing_state = Drawing_States.ended;
        whiteboard.canvas_selected = false;

    }
    return false;
};

var event_mouse_up = function(e)
{
    e.preventDefault();
    return on_mouse_out(e);
};

var event_mouse_leave = function(e)
{
    e.preventDefault();
    if(whiteboard.canvas_selected)
    {
        return on_mouse_out(e);
    }
};