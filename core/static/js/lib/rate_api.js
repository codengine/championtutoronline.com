function call_ajax(url, method, data, callback, errorback, completeback) {
    $.ajax({
        type: method,
        url: url,
        data: data,
        enctype: 'multipart/form-data',
        success: function(data)
        {
            var data = jQuery.parseJSON(data);
            callback(data);
        },
        error: function(jqxhr, status, error)
        {
            errorback(jqxhr, status, error);
        }
    })
    .done(function( msg ) {
        completeback(msg);
    });
}

$(document).ready(function() {
    $("#__id_rate").keyup(function(e) {
        var _this = $(this);
        var value = $(this).val();
        if(value == "") {
             _this.parent().find(".rate_conv_value").hide();
             return false;
        }
        var base_currency = $(this).data("basecurrency");
        var target_currency = $(this).data("targetcurrency");
        var conversion_enabled = $(this).data("conversion-enabled");
        if(conversion_enabled == "True") {
            var url = "/rate-conversion";
            var method = "GET";
            var data = {
                "basecurrency": base_currency,
                "targetcurrency": target_currency,
                "value": value
            }
            call_ajax(url, method, data, function(data) {
                if(data.status == "SUCCESS") {
                    _this.parent().find(".rate_conv_value").text("≈"+ data.data.target + " " +data.data.converted);
                    _this.parent().find(".rate_conv_value").show();
                }
            }, function(jqxhr, status, error) {
                _this.parent().find(".rate_conv_value").hide();
            }, function(msg) {

            });
        }
    });


    $("#id_rate").keyup(function(e) {
        var _this = $(this);
        var value = $(this).val();
        if(value == "") {
             $("#id_ratec_1").hide();
             return false;
        }
        var base_currency = $(this).parent().data("basecurrency");
        var target_currency = $(this).parent().data("targetcurrency");
        var conversion_enabled = $(this).parent().data("conversion-enabled");
        if(conversion_enabled == "True") {
            var url = "/rate-conversion";
            var method = "GET";
            var data = {
                "basecurrency": base_currency,
                "targetcurrency": target_currency,
                "value": value
            }
            call_ajax(url, method, data, function(data) {
                if(data.status == "SUCCESS") {
                    $("#id_ratec_1").text(data.data.converted);
                    $("#id_target_currency").text(data.data.target);
                    $("#id_ratec_1").show();
                    $("#id_cconverter_container").show();
                }
            }, function(jqxhr, status, error) {
                $("#id_ratec_1").hide();
            }, function(msg) {

            });
        }
    });

    $("#id_rate2").keyup(function(e) {
        var _this = $(this);
        var value = $(this).val();
        if(value == "") {
             $("#id_ratec_2").hide();
             return false;
        }
        var base_currency = $(this).parent().data("basecurrency");
        var target_currency = $(this).parent().data("targetcurrency");
        var conversion_enabled = $(this).parent().data("conversion-enabled");
        if(conversion_enabled == "True") {
            var url = "/rate-conversion";
            var method = "GET";
            var data = {
                "basecurrency": base_currency,
                "targetcurrency": target_currency,
                "value": value
            }
            call_ajax(url, method, data, function(data) {
                if(data.status == "SUCCESS") {
                    $("#id_ratec_2").text(data.data.converted);
                    $("#id_target_currency").text(data.data.target);
                    $("#id_ratec_2").show();
                    $("#id_cconverter_container").show();
                }
            }, function(jqxhr, status, error) {
                $("#id_ratec_2").hide();
            }, function(msg) {

            });
        }
    });

    $(document).on("keyup", ".tutoring_rate", function(e) {
                var _this = $(this);
                var value = $(this).val();
                if(value == "") {
                     $(this).parent().parent().parent().find(".rate_conversion_value").hide();
                     return false;
                }
                var base_currency =  $(this).parent().parent().parent().next().find(".tutoring_currency").val();
                var url = "/rate-conversion";
                var method = "GET";
                var data = {
                    "basecurrency": base_currency,
                    "targetcurrency": -1, //To be decided.
                    "value": value
                }

                call_ajax(url, method, data, function(data) {
                    if(data.status == "SUCCESS") {
                        if(data.data.decision == "NO_CONVERSION" || data.data.decision == "NO_CONVERSION_FOUND") {
                            _this.parent().parent().parent().find(".rate_conversion_value").hide();
                        }
                        else {
                            _this.parent().parent().parent().find(".rate_conversion_value").text("Approx. "+data.data.converted+" "+data.data.target);
                            _this.parent().parent().parent().find(".rate_conversion_value").show();
                        }
                    }
                }, function(jqxhr, status, error) {
                    _this.parent().parent().parent().find(".rate_conversion_value").hide();
                }, function(msg) {

                });
    });

});