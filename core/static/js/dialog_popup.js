function show_simple_dialog(title,msg,dialog_type) {
    var dialogtype = BootstrapDialog.TYPE_SUCCESS;
    if(dialog_type == "warning"){
        dialogtype = BootstrapDialog.TYPE_WARNING;
    }
    else if(dialog_type == "info"){
        dialogtype = BootstrapDialog.TYPE_INFO;
    }
    else if(dialog_type == "error"){
        dialogtype = BootstrapDialog.TYPE_DANGER;
    }
    else if(dialog_type == "success"){
        dialogtype = BootstrapDialog.TYPE_PRIMARY;
    }
    else if(dialog_type == "primary"){
        dialogtype = BootstrapDialog.TYPE_PRIMARY;
    }

    BootstrapDialog.show({
        title: title,
        message: msg,
        closable: true,
        type: dialogtype,
        size: BootstrapDialog.SIZE_NORMAL,
        buttons: [
            {
                label: "ok",
                cssClass: 'btn btn-md btn-primary width120',
                action: function(dialogRef) {
                    dialogRef.close();
                }
            },
            {
                label: "Cancel",
                cssClass: 'btn btn-md btn-warning width120',
                action: function(dialogRef) {

                }
            }]
    });
}