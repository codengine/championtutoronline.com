function _do_ajax_search(data, callback, errback, completeback) {
    $.ajax({
        type: "GET",
        url: "/ajax/search-tutor/",
        data: data,
        success: function(data)
        {
            var data = jQuery.parseJSON(data);
            callback(data);
        },
        error: function(jqxhr, status, error)
        {
            errback(jqxhr, status, error);
        }
    })
    .done(function( msg ) {
        completeback(msg);
    });
}

function update_address_url(state_object, url) {
    History.replaceState(state_object, null, url);
}

function prepare_search_data() {
    var data = {};

    if($("#id_filter_ready_to_teach").is(":checked")) {
        data["ready_to_teach"] = "1"
    }
    else {
        data["ready_to_teach"] = "0"
    }

    if($("#id_filter_ready_to_teach_online").is(":checked")) {
        data["online"] = "1"
    }
    else {
        data["online"] = "0";
    }

    data["gender"] = $("input[name=gender]:checked").val();

    var countries = $("#id_country_select").val();



    if(countries != null) {
        data["countries"] = countries;
    }

    var budget = "";
    if($("#sgd1").is(":checked")) {
        budget += "sgd1,"
    }
    if($("#sgd2").is(":checked")) {
        budget += "sgd2,"
    }
    if($("#sgd3").is(":checked")) {
        budget += "sgd3,"
    }
    if($("#sgd4").is(":checked")) {
        budget += "sgd4,"
    }
    if(budget != "") {
        data["budget"] = budget;
    }

    var level = $("select[name=level]").val();
    if(level != null) {
        data["level"] = level;
    }

    var subject = $("select[name=subject]").val();
    if(subject != null) {
        data["subject"] = subject;
    }

    var keywords = $("#id_keywords").val();
    if(keywords != "") {
     data["keyword"] = keywords;
   }

    var video_intro = $("#id_tutor_video_intro").is(":checked");
    if(video_intro) {
        data["video-intro"] = "true"
    }

    var country = $("#id_country_select").val();
    if(country != null) {
        data["country"] = country;
    }

    data["page"] = $("input[name=page_number]").val();

    return data;
}

function get_url_formatted() {
    var query_params = [];

    if($("#id_filter_ready_to_teach").is(":checked")) {
        query_params.push("ready_to_teach=1");
    }

    if($("#id_filter_ready_to_teach_online").is(":checked")) {
        query_params.push("online=1");
    }

    query_params.push("gender="+$("input[name=gender]:checked").val());

    var countries = $("#id_country_select").val();

    if(countries != null) {
        query_params.push("countries="+countries);
    }

    var budget = "";
    if($("#sgd1").is(":checked")) {
        budget += "sgd1,"
    }
    if($("#sgd2").is(":checked")) {
        budget += "sgd2,"
    }
    if($("#sgd3").is(":checked")) {
        budget += "sgd3,"
    }
    if($("#sgd4").is(":checked")) {
        budget += "sgd4,"
    }
    if(budget != "") {
        query_params.push("budget="+budget);
    }

    var level = $("select[name=level]").val();
    if(level != null) {
        query_params.push("level="+level);
    }

    var subject = $("select[name=subject]").val();
    if(subject != null) {
        query_params.push("subject="+subject);
    }

    var keywords = $("#id_keywords").val();
    if(keywords != "") {
        query_params.push("keyword="+keywords);
   }

    var video_intro = $("#id_tutor_video_intro").is(":checked");
    if(video_intro) {
        query_params.push("video-intro=true");
    }

    var country = $("#id_country_select").val();
    if(country != null) {
        query_params.push("country="+country);
    }

    query_params.push("page="+$("input[name=page_number]").val());

    query_string = "";
    for(var i = 0 ; i < query_params.length ; i++) {
        query_string += query_params[i];
        if(i < query_params.length - 1) {
            query_string += "&";
        }
    }
    return query_string;
}

window.initial_search_call_finished = false;

function _do_initial_call() {

    var data =  prepare_search_data();
    var container_id = "id_tutor_search_result_container";

    $("#"+container_id).css("opacity", "0.5");
    $("#"+container_id).css("zoom", "alpha(opacity=50)");
    $("#"+container_id).css("zoom", "1");

    _do_ajax_search(data, function(data) {
        $("input[name=page_number]").val(data.extra_data.current);
        $("#"+container_id).html(data.data);
        $("#id_pagination_links").html(data.extra_data.page_content);
        $("#id_top_pagination_links").html(data.extra_data.page_content);
        $(".breadcrumb").html(data.extra_data.breadcumb_content);

        window.initial_search_call_finished = true;

    },
    function(jqxhr, status, error) {
    },
    function(msg) {
        $("#"+container_id).css("opacity", "1");
        $("#"+container_id).css("zoom", "alpha(opacity=100)");
        $("#"+container_id).css("zoom", "0");
    });
}

function _do_search(search_data) {

    var data = search_data;
    if(typeof search_data == "undefined") {
        data = prepare_search_data();
    }

    //console.log(data);

    var container_id = "id_tutor_search_result_container";

    $("#"+container_id).css("opacity", "0.5");
    $("#"+container_id).css("zoom", "alpha(opacity=50)");
    $("#"+container_id).css("zoom", "1");
    _do_ajax_search(data, function(data) {
        $("input[name=page_number]").val(data.extra_data.current);
        $("#"+container_id).html(data.data);
        $("#id_pagination_links").html(data.extra_data.page_content);
        $("#id_top_pagination_links").html(data.extra_data.page_content);
        $(".breadcrumb").html(data.extra_data.breadcumb_content);

        update_address_url(data, "?"+get_url_formatted());

    },
    function(jqxhr, status, error) {
    },
    function(msg) {
        $("#"+container_id).css("opacity", "1");
        $("#"+container_id).css("zoom", "alpha(opacity=100)");
        $("#"+container_id).css("zoom", "0");
    });
}

$(document).ready(function() {

    /*
    New changes here.
    */
    $("input[name=gender]").change(function(e) {
        _do_search();
    });
    $("#id_keywords").change(function(e) {
        if($.trim($(this).val() != "")) {
           _do_search();
        }
    });
    $("#sgd1").change(function(e) {
        _do_search();
    });
    $("#sgd2").change(function(e) {
        _do_search();
    });
    $("#sgd3").change(function(e) {
        _do_search();
    });
    $("#sgd4").change(function(e) {
       _do_search();
    });

    $(".onoffswitch").change(function(e) {
        _do_search();
    });

    var first_loaded = false;

    $("#id_tutor_video_intro").change(function(e) {
        _do_search();
    });

    $(document).on("click", ".breadcumb-item", function(e) {
        e.preventDefault();
        var url = $(this).data("url");
        _do_search(url);
    });

    (function(window,undefined){

        // Bind to StateChange Event
        History.Adapter.bind(window,'statechange',function(){ // Note: We are using statechange instead of popstate
            /*var State = History.getState(); // Note: We are using History.getState() instead of event.state
            State.data["page"] = $("input[name=page_number]").val();
            _do_search(State.data);*/
        });

        // Change our States


    })(window);
    setTimeout(_do_initial_call, 1000);
});

$(document).on("click",".search-next", function(e) {
    e.preventDefault();
    var page_number = $(this).data("page-number");
    $("input[name=page_number]").val(page_number);
    _do_search();
});

$(document).on("click",".tutor-send-msg-btn", function(e) {
    e.preventDefault();
    if(window.champ_user_id != undefined && window.champ_user_id != -1) {
        //get the tutor's user_id value.
        var uid = $(this).parent().find("input[name=tutor_id]").val();
        var offline_status = true; //Say user is offline
        var offline_status_found = $(this).parent().find("input[name=online_status]").val();

        if(offline_status_found == "online"){
            offline_status = false;
        }

        uid = parseInt(uid);

        //console.log(window.chat_boxes);

        if(!window.chat_boxes.hasOwnProperty(uid))
        {
            var tutor_name = $(this).parent().find("input[name=tutor_name]").val();
            var img_src = $(this).parent().find("input[name=tutor_pp]").val();
            var chat_object = {user_id:uid,name: tutor_name,img_url: img_src};
            //console.log(chat_object);
            var chat = new Chat();
            var uids = [uid];
            chat.remote_peers = uids;
            chat.addNewChat(chat_object,offline_status);
            //window.chat_boxes[uid] = chat;
        }
        else
        {
            window.chat_boxes[uid].show();
            window.chat_boxes[uid].focusInput();
        }
    }
    else {
        show_signin();
    }

});

