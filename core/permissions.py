from enum import Enum

__author__ = 'codengine'

class Permission(Enum):
    read = "perm_read"
    write = "perm_write"
    delete = "perm_delete"
    all = "perm_all"
