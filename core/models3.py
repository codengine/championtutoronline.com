from audioop import reverse
from collections import OrderedDict
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import Min, Max, Count
from django.db.models.aggregates import Sum
from django.db.models.query_utils import Q
from image_cropping import ImageRatioField
from image_cropping.fields import ImageCropField
from core.classes.tabview import TabView
from core.enums import LessonTypes, LessonStatus, WhiteboardAccess, QuestionStatus, TransactionStatus, \
    PriceSettlementStatus, JobApplicationStatus, JobInvitationStatus, JobRecommendationStatus, JobStatus
from core.generics.mixins import ThumbnailModelMixin
from core.generics.models import GenericModel
from core.enums import UserTypes,VerificationStatus,ChatType,ReadStatus,ActiveStatus,AvailableStatus,OnlineStatus,ApprovalStatus
from core.library.Clock import Clock
from core.library.tz_util import TZUtil
from core.managers.job_model_manager import JobModelManager
from core.permissions import Permission
from payment.models import Wallet, PaypalStoredCreditCard, Transaction
from pyetherpad.models import Pad
from pysharejs.models import CodePad
import settings
from django.template import loader, Context
import random


class LocationEntity(GenericModel):
    parent = models.ForeignKey("self",null=True)
    name = models.CharField(max_length=255)
    code = models.CharField(max_length=13)
    type = models.CharField(max_length=20)

    class Meta:
        db_table = 'location_entity'

class Country(LocationEntity):

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.type = Country.__name__
        super(Country,self).save(force_insert,force_update,using,update_fields)

    class Meta:
        proxy = True

class City(LocationEntity):

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.type = City.__name__
        super(Country,self).save(force_insert,force_update,using,update_fields)

    class Meta:
        db_table=u'city'
        proxy = True

class State(LocationEntity):

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.type = State.__name__
        super(Country,self).save(force_insert,force_update,using,update_fields)

    class Meta:
        db_table=u'state'
        proxy = True

class ZipCode(LocationEntity):

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.type = ZipCode.__name__
        super(Country,self).save(force_insert,force_update,using,update_fields)

    class Meta:
        db_table=u'zip_code'
        proxy = True

class Address(GenericModel):
    street_address1 = models.TextField(blank=True, null=True, max_length=200)
    street_address2 = models.TextField(blank=True, null=True, max_length=200)
    city = models.ForeignKey(City,null=True)
    state = models.ForeignKey(State,null=True)
    zip = models.ForeignKey(ZipCode,null=True)
    country = models.ForeignKey(Country,null=True)


class ProfilePicture(GenericModel):
    image_field = ImageCropField(upload_to='image/')
    cropping = ImageRatioField('image_field', size='120x100', allow_fullsize=True)

    class Meta:
        db_table='profile_picture'

class CoverPhoto(GenericModel):
    image_field = models.ImageField(upload_to='image/')

    class Meta:
        db_table = 'cover_photo'

class Role(GenericModel):
    type_choices = (
        (UserTypes.student,UserTypes.student),
        (UserTypes.teacher,UserTypes.teacher),
        (UserTypes.stuff,UserTypes.stuff)
    )
    name=models.CharField(blank=False,null=False,max_length=10,choices=type_choices)

    class Meta:
        db_table='role'

class Language(GenericModel):
    name = models.CharField(max_length=200)
    code = models.CharField(null=True,blank=True,max_length=10)
    #proficiency = models.IntegerField(default=0,null=True,blank=True) ###Max is 5

    class Meta:
        db_table = 'language'

class LanguageProficiency(GenericModel):
    #user = models.ForeignKey(User)
    type = models.IntegerField(default=0) # 0 means spoken and 1 means writtensss
    language = models.ForeignKey(Language)
    proficiency = models.IntegerField(default=0)

    class Meta:
        db_table = 'language_proficiency'


class ChampUser(GenericModel):

    user = models.OneToOneField(User)
    firstname = models.CharField(blank=False, null=False, max_length=100)
    lastname = models.CharField(blank=False, null=False, max_length=100)
    fullname = models.TextField(blank=False, null=False, max_length=100)
    facebook_id = models.CharField(max_length=100, blank=True, default='')
    phone = models.TextField(blank=False, null=False, max_length=20)
    address = models.ForeignKey(Address, null=True, default=None)
    profile_picture = models.ForeignKey(ProfilePicture, null=True, default=None)
    type = models.ForeignKey(Role)
    nationality = models.ForeignKey(Country,null=True)
    current_school = models.CharField(max_length=255,null=True)
    session = models.CharField(max_length=255,null=True)
    major = models.CharField(max_length=255,null=True)
    gender = models.CharField(max_length=100,null=True,blank=True)
    available_to_teach = models.IntegerField(default=AvailableStatus.unavailable.value,null=True,blank=True)
    online_status = models.IntegerField(default=OnlineStatus.offline.value,null=True,blank=True)
    timezone = models.CharField(null=True,blank=True,max_length=200)
    hometown = models.CharField(null=True,blank=True,max_length=200)
    zip = models.CharField(null=True,blank=True,max_length=20)
    grad_year = models.CharField(null=True,blank=True,max_length=200)
    cover_photo = models.ForeignKey(CoverPhoto,null=True)
    birthdate = models.DateField(null=True,blank=True)
    religion = models.CharField(max_length=255,null=True,blank=True)
    #languages = models.ManyToManyField(Language)
    ethnicity = models.CharField(null=True,blank=True,max_length=40)
    languages = models.ManyToManyField(LanguageProficiency)
    activation_code = models.CharField(max_length=600,null=True,blank=True)
    wallet = models.ForeignKey(Wallet)
    credit_cards = models.ManyToManyField(PaypalStoredCreditCard)
    receive_email_notification = models.BooleanField(default=True)
    color_hex = models.CharField(blank=True, null=True, default=None, max_length=7)

    @property
    def render_profile_summary(self):
        return ""

    @property
    def render_last_education(self):
        edu_objects = Education.objects.filter(user_id=self.user_id)
        if edu_objects.exists():
            current_edu_objects = edu_objects.filter(is_current=1)
            if current_edu_objects.exists():
                current_edu_object = current_edu_objects.order_by('-date_attended').first()
                return current_edu_object
            edu_object = edu_objects.order_by('-date_attended').first()
            return edu_object


    @property
    def render_profile_summary(self):
        return ""

    @property
    def render_last_education(self):
        edu_objects = Education.objects.filter(user_id=self.user_id)
        if edu_objects.exists():
            current_edu_objects = edu_objects.filter(is_current=1)
            if current_edu_objects.exists():
                current_edu_object = current_edu_objects.order_by('-date_attended').first()
                return current_edu_object
            edu_object = edu_objects.order_by('-date_attended').first()
            return edu_object

    @property
    def render_online_status_icon(self):
        if self.online_status == 1:
            if self.available_to_teach == 2 or self.available_to_teach == 4:
                return "images/corct_icon.png"
            return "images/online.png"
        return None

    @property
    def render_timezone_country(self):
        country_name = ''
        tzm = TimezoneMapper.objects.filter(tz=self.user.champuser.timezone)
        if tzm.exists():
            tzm = tzm.first()
            country_name = tzm.country.name
        return country_name

    def is_deactivated(self):
        return self.available_to_teach == AvailableStatus.profile_deactivated.value

    def get_queryset(self):
        queryset = super(ChampUser, self).get_queryset()
        queryset = queryset.exclude(status=AvailableStatus.profile_deactivated.value)
        return queryset

    @property
    def positive_feedback_count(self):
        feedback_count = LessonRequest.objects.filter(request_user_id=self.user.pk, review__isnull=False, review__reviews__feedback=True).count()
        return feedback_count

    @property
    def positive_feedback_count_verbal(self):
        feedback_count = self.positive_feedback_count
        if feedback_count:
            return str(feedback_count)
        else:
            return "-"

    @property
    def negative_feedback_count(self):
        feedback_count = LessonRequest.objects.filter(request_user_id=self.user.pk, review__isnull=False, review__reviews__feedback=False).count()
        return feedback_count

    @property
    def negative_feedback_count_verbal(self):
        feedback_count = self.negative_feedback_count
        if feedback_count:
            return str(feedback_count)
        else:
            return "-"

    def get_login_success_url(self):
        return reverse("user_profile", kwargs={ "pk": self.user.pk })

    def toggle_email_notification(self):
        if self.receive_email_notification:
            self.receive_email_notification = False
        else:
            self.receive_email_notification = True
        self.save()

    @property
    def render_age_in_years(self):
        if self.birthdate:
            birth_date = Clock.convert_ts_to_datetime(self.birthdate)
            now = Clock.timenow()
            return now.year - birth_date.year

    @property
    def render_profile_picture(self):
        profile_img = "/static/images/default_avater_m.jpg"
        if self.profile_picture and self.profile_picture.image_field:
            if not str(self.profile_picture.image_field).startswith("/media/"):
                profile_img = "/media/"+str(self.profile_picture.image_field)
            else:
                profile_img = str(self.profile_picture.image_field)
        else:
            if self.gender == "male":
                profile_img = "/static/images/default_avater_m.jpg"
            elif self.gender == "female":
                profile_img = "/static/images/default_avater_f.jpeg"
        return profile_img

    @property
    def render_teaching_available_status_verbal(self):
        status = "unavailable"
        if self.available_to_teach == 2 or self.available_to_teach == 4:
            status = "ready_to_teach"
        return status

    def check_if_invited_to_job(self,job):
        return JobInvitation.objects.filter(job=job,user=self.user).exists()

    @property
    def render_email(self):
        return self.user.email

    @property
    def render_profile_thumbnail(self):
        return """<img src="%s" width="30" height="30"/>""" % self.render_profile_picture
    @classmethod
    def table_columns(cls):
        return [ "fullname:Name", "render_email", "render_profile_thumbnail:Profile Picture", "date_created:Registered On" ]

    @property
    def wallet_balance(self):
        return self.wallet.balance

    @classmethod
    def filter_search(cls, request, queryset, **kwargs):
        type = request.GET.get('type')
        if type:
            queryset = queryset.filter(type__name=type)
        return queryset


    @classmethod
    def get_searchable_columns(cls):
        return [
            ("type", "User Type e.g tutor")
        ]

    @property
    def is_tutor(self):
        return self.type.name == "teacher"

    @property
    def render_online_status_verbal(self):
        return "online" if self.online_status == 1 else "offline"

    def calculate_rating(self):
        my_lessons = LessonRequest.objects.filter(request_user_id=self.user.pk,review__isnull=False)
        rating = 0
        for l in my_lessons:
            rating += l.review.average_rating
        return rating

    @property
    def profile_url(self):
        return reverse("user_profile",kwargs={ "pk": self.user.pk })

    @property
    def render_type(self):
        return self.type.name.capitalize()

    @property
    def render_majors(self):
        majo_subjects = [ umajor.major.name for umajor in UserMajor.objects.filter(user_id=self.user.pk) ]
        verbose_string = ''
        if majo_subjects:
            verbose_string += majo_subjects[0]
            if len(majo_subjects) > 1:
                verbose_string += ' + %s other' % str(len(majo_subjects) -1)
        return verbose_string


    @property
    def render_tutor_profile_currency(self):
        user_major_objects = UserMajor.objects.filter(user_id=self.user.pk)
        if user_major_objects.exists():
            try:
                return user_major_objects.first().currency.code
            except Exception as exp:
                pass

    @property
    def render_min_currency(self):
        min_rate = UserMajor.objects.filter(user_id=self.user.pk).aggregate(Min('rate'))
        min_rate = min_rate["rate__min"]
        return min_rate

    @property
    def render_max_currency(self):
        max_rate = UserMajor.objects.filter(user_id=self.user.pk).aggregate(Max('rate'))
        max_rate = max_rate["rate__max"]
        return max_rate

    @property
    def render_teaching_experiences(self):
        teaching_experiences = TeachingExperiences.objects.filter(user_id=self.user.pk)
        if teaching_experiences.exists():
            return teaching_experiences.first().experience
        return "N/A"

    @property
    def render_status(self):
        if self.user.is_active == 1:
            return "Active"
        elif self.user.is_active == 0:
            return "Inactive"

    def generate_random_hex(self):
            r = lambda: random.randint(0, 255)
            return '#%02X%02X%02X' % (r(), r(), r())

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.color_hex:
            self.color_hex = self.generate_random_hex()
        if not self.pk:
            country = TZUtil.get_country_for_timezone(self.timezone)
            if country:
                self.nationality = country
        super(ChampUser,self).save(force_insert, force_update, using,update_fields)

    class Meta:
        db_table='champ_user'

class Certifications(GenericModel):
    name = models.CharField(max_length=255)
    added_by = models.ForeignKey(User)

    class Meta:
        db_table =u'certifications'

class TutorCertificates(ThumbnailModelMixin, GenericModel):
    title = models.CharField(max_length=500, blank=True)
    description = models.TextField(blank=True)
    name = models.CharField(max_length=500)
    original_name = models.CharField(max_length=500)
    type = models.CharField(max_length=150, blank=True)
    year = models.IntegerField(null=True)
    school = models.CharField(max_length=500, blank=True)
    user = models.ForeignKey(User)
    thumbnail = models.ImageField(upload_to='Certificates/Thumbnails/',max_length=500,blank=True,null=True)
    show_certificates = models.IntegerField(default=1) # 0 means hide and 1 means show.

    class Meta:
        db_table = 'tutor_certificates'

class Schools(GenericModel):
    name = models.CharField(max_length=255)
    type = models.CharField(max_length=300,null=True,blank=True)
    added_by = models.ForeignKey(User)

    class Meta:
        db_table = u'schools'

class NRICVerification(GenericModel):
    user = models.ForeignKey(User)
    nid_name = models.CharField(max_length=500, blank=True, default='', null=True)
    nid_number = models.CharField(max_length=50, blank=True, default='', null=True)
    file_name = models.CharField(max_length=500, blank=True, default='', null=True)
    nid_scanned_img = models.ImageField(upload_to='NRIC_Images/')
    status = models.IntegerField(default=VerificationStatus.not_verified.value) ###0 means not verified ,1 means verified,2 means under review,3 means rejected,4 means submitted..
    verified_by = models.BigIntegerField(null=True,blank=True)
    submit_date = models.DateTimeField(auto_now_add=True,blank=True,null=True)
    verification_date = models.DateTimeField(auto_now_add=True,blank=True,null=True)

    @property
    def attachment_url(self):
        return "/media/"+str(self.nid_scanned_img)

    @property
    def render_status(self):
        if self.status == VerificationStatus.submitted.value:
            return "Submitted"
        elif self.status == VerificationStatus.not_verified.value:
            return "Not Verified"
        elif self.status == VerificationStatus.under_review.value:
            return "Under Review"
        elif self.status == VerificationStatus.rejected.value:
            return "Rejected"
        elif self.status == VerificationStatus.verified.value:
            return "Approved"
    @property
    def actions(self):
        return [
            ("-1","Select Action","","Select Action"),
            (VerificationStatus.submitted.value,"Submitted","", "Change status to Submitted"),
            (VerificationStatus.not_verified.value,"Not Verified","","Change status to Not Verified"),
            (VerificationStatus.under_review.value,"Under Review","","Change status to Under Review"),
            (VerificationStatus.rejected.value,"Rejected","", "Change status to Rejected"),
            (VerificationStatus.verified.value,"Verified","","Change status to Verified"),
        ]

    @property
    def details_link(self):
        return reverse("verification_request_details",kwargs={ "pk": self.pk })

    class Meta:
        db_table=u'nric_verification'

# class TopSubject(GenericModel):
#     degree = models.CharField(max_length=255, null=True)
#     institution = models.CharField(max_length=255, null=True)
#     session = models.CharField(max_length=255, null=True, verbose_name='class')
#     cgpa = models.CharField(max_length=255, default='0.0')
#     is_current = models.BooleanField(default=False)

class Category(GenericModel):
    name = models.CharField(max_length=255)
    description = models.TextField()
    added_by = models.ForeignKey(User)

    class Meta:
        db_table = u'category'

class Courses(GenericModel):
    name = models.CharField(max_length=255)
    level = models.CharField(max_length=255)
    description = models.TextField()
    category = models.ForeignKey(Category,null=True)

    class Meta:
        db_table = u'course'

class Rate(GenericModel):
    user = models.ForeignKey(User)
    class_level = models.CharField(max_length=255)
    rate = models.FloatField(default=0)
    rate_currency = models.CharField(max_length=10)

    @classmethod
    def initialize_rates(cls,request,champuser=None):
        def add_rate(level):
            user = champuser.user if champuser else request.user
            if not Rate.objects.filter(user=user,class_level=level).exists():
                Rate(user=user,class_level=level).save()
        add_rate("Elementary");
        add_rate("HighSchool");
        add_rate("College");
        add_rate("UniversityDegree");
        add_rate("UniversityMasters");
        add_rate("UniversityPhD");
        add_rate("Music");
        add_rate("Language");
        add_rate("Computing");


    class Meta:
        db_table=u'profile_rate'

class Currency(GenericModel):
    code = models.CharField(max_length=7,null=True,blank=True)
    name = models.CharField(max_length=255)
    symbol = models.CharField(max_length=7,null=True,blank=True)
    country = models.ForeignKey(Country, null=True)

    class Meta:
        db_table=u'currency'

class DrawingBoard(GenericModel):
    #whiteboard = models.ForeignKey(Whiteboard)
    numeric_id = models.IntegerField()
    name = models.CharField(max_length=255)
    drawing_data = models.TextField(null=True,blank=True,default='')
    background = models.TextField(null=True,blank=True,default='')
    type = models.CharField(max_length=50,blank=True,default='')

    class Meta:
        db_table=u'drawing_board'

class DrawingboardLastNid(GenericModel):
    group = models.CharField(max_length=300) ###Group can be lesson id,user id or session id.
    nid = models.BigIntegerField()

    class Meta:
        db_table = u'drawingboard_last_nid'

class DrawingboardPermission(GenericModel):
    drawingboard = models.ForeignKey(DrawingBoard)
    permission = models.CharField(max_length=700,null=True,blank=True,default=Permission.all.value)

    class Meta:
        db_table=u'drawingboard_permission'

class TextPadPermission(GenericModel):
    text_pad = models.ForeignKey(Pad)
    permission = models.CharField(max_length=700,null=True,blank=True,default=Permission.all.value)

    class Meta:
        db_table=u'text_pad_permission'

class CodePadPermission(GenericModel):
    code_pad = models.ForeignKey(CodePad)
    permission = models.CharField(max_length=700,null=True,blank=True,default=Permission.all.value)

    class Meta:
        db_table=u'codepad_permission'

class WhiteboardUserPermission(GenericModel):
    user = models.ForeignKey(User)
    drawingboard_permissions = models.ManyToManyField(DrawingboardPermission)
    text_pad_permissions = models.ManyToManyField(TextPadPermission)
    code_pad_permissions = models.ManyToManyField(CodePadPermission)
    chat_permission = models.CharField(max_length=700,null=True,blank=True,default=Permission.all.value)

    class Meta:
        db_table=u'whiteboard_user_permission'

class UserOTToken(GenericModel):
    email_or_session = models.CharField(max_length=300)
    token = models.TextField()

    class Meta:
        db_table=u'user_ot_token'

class VideoChatSettings(GenericModel):
    video_enabled = models.IntegerField() ###1 means yes and 0 means no
    audio_enabled = models.IntegerField()  ###1 means yes and 0 means no
    sharing_enabled = models.IntegerField() ###1 means yes and 0 means no

    class Meta:
        db_table = u'video_chat_settings'

class UserVideoChatSettings(GenericModel):
    email_or_session_key = models.CharField(max_length=500)
    settings = models.ForeignKey(VideoChatSettings)

    class Meta:
        db_table = u'user_video_chat_settings'

class VideoChat(GenericModel):
    ot_session = models.CharField(max_length=500)
    archive_url = models.CharField(max_length=500,null=True,blank=True)
    tokens = models.ManyToManyField(UserOTToken)
    user_settings = models.ManyToManyField(UserVideoChatSettings)

    class Meta:
        db_table=u'video_chat'

class TutorChat(ThumbnailModelMixin, GenericModel):
    #whiteboard = models.ForeignKey(Whiteboard)
    sender = models.ForeignKey(User,related_name="whiteboard_chat_sender",null=True)
    receivers = models.ManyToManyField(User,related_name="whiteboard_chat_receivers")
    text = models.TextField(null=True,blank=True)
    attachment_original_name = models.CharField(max_length=500,null=True,blank=True)
    attachment = models.FileField(upload_to='Msg_Attachment/')
    thumbnail = models.ImageField(upload_to='Msg_Attachment/',max_length=500,blank=True,null=True)

    @property
    def render_attachment_url(self):
        return settings.MEDIA_URL+str(self.attachment)

    @property
    def render_thumbnail_url(self):
        return settings.MEDIA_URL+str(self.thumbnail)

    def save(self):
        try:
            self.create_thumbnail()
            # self.save()
        except Exception as msg:
            print(str(msg))
            print("Thumbnail creation failed.")
        super(TutorChat, self).save()


    class Meta:
        db_table = 'tutor_chat'


class UserDrawingBoard(GenericModel):
    user = models.ForeignKey(User)
    drawingboard = models.ForeignKey(DrawingBoard)
    numeric_name = models.IntegerField()
    label = models.CharField(max_length=255)

    class Meta:
        db_table=u'user_drawing_board'

class StickyNote(GenericModel):
    user = models.ForeignKey(User,null=True)
    #whiteboard = models.ForeignKey(Whiteboard)
    serial_id = models.CharField(max_length=100,null=False,blank=False)
    content = models.TextField(null=True,blank=True)
    top = models.FloatField(null=True,blank=True)
    left = models.FloatField(null=True,blank=True)
    is_locked = models.IntegerField(default=0)

    class Meta:
        db_table='sticky_note'

class WbChatboxStatus(GenericModel):
    status = models.IntegerField(default=0) ###0 if hidden and 1 if show
    user = models.ForeignKey(User)

    class Meta:
        db_table = 'wb_chatbox_status'

class Whiteboard(GenericModel):
    access = models.CharField(max_length=100,default=WhiteboardAccess.protected.value)
    created_by = models.ForeignKey(User,null=True)
    unique_id = models.CharField(max_length=60)
    name = models.CharField(max_length=255)
    active = models.IntegerField(default=ActiveStatus.active.value)
    sticky_notes = models.ManyToManyField(StickyNote)
    drawingboards = models.ManyToManyField(DrawingBoard)
    text_pads = models.ManyToManyField(Pad)
    code_pads = models.ManyToManyField(CodePad)
    tutor_chat = models.ManyToManyField(TutorChat)
    video_chat = models.ForeignKey(VideoChat,null=True)
    users = models.ManyToManyField(User,related_name="whiteboard_users")
    permissions = models.ManyToManyField(WhiteboardUserPermission)
    chatbox_statuses = models.ManyToManyField(WbChatboxStatus)

    class Meta:
        db_table=u'whiteboard'

class DemoWhiteboard(GenericModel):
    email_or_session_key = models.CharField(max_length=500)
    whiteboard = models.ForeignKey(Whiteboard)

    class Meta:
        db_table = u'demo_whiteboard'

class Profile(GenericModel):
    user = models.ForeignKey(ChampUser)
    hourly_rate = models.FloatField(default=float(0.0))
    title = models.CharField(max_length=255, null=True)
    description = models.TextField(null=True)
    nationality = models.CharField(max_length=255, null=True)
    rating = models.FloatField(default=float(0.0))
    nric_verification_status = models.BooleanField(default=False)
    major_subject = models.CharField(max_length=255, null=True)
    #education = models.ManyToManyField(Education)
    #top_subjects = models.ManyToManyField(TopSubject)
    #teaching_exp = models.CharField(max_length=5000, null=True,default=None)
    #ext_interest = models.CharField(max_length=5000, null=True,default=None)
    about = models.TextField()
    currency = models.ForeignKey(Currency,null=True)

    #whiteboard = models.ForeignKey(Whiteboard)

    class Meta:
        db_table=u'champ_profile'

class UserTimezoneSettings(GenericModel):
    # user_id = models.ForeignKey(ChampUser)
    # timezone = models.CharField(max_length=6) ##This field will contain timezone information in +=360 format. Say timezone is UTC+6 then it will store -360
    # last_updated = models.DateField(auto_now_add=True)
    pass


class OnlineStatus(GenericModel):
    user_id = models.BigIntegerField()
    status = models.IntegerField(default=OnlineStatus.offline.value) ## 1 for online and 0 for offline

    class Meta:
        db_table='online_status'

class Message(GenericModel):
    # msg_date = models.BigIntegerField(null=True,blank=True)
    msg = models.TextField()
    is_read = models.IntegerField(default=ReadStatus.unread.value)  ###0 for unread and 1 for read. Default is unread.
    chat_type = models.IntegerField(default=ChatType.p2p.value) ###0 means p2p chat and 1 means group chat.

    @property
    def attachments(self):
        return MessageAttachment.objects.filter(message_id=self.pk)

    class Meta:
        db_table='champ_chat_Message'

class MessageAttachment(GenericModel):
    message = models.ForeignKey(Message)
    attachment = models.FileField(upload_to='Msg_Attachment/')
    attachment_name = models.CharField(max_length=500,null=True,blank=True)
    thumbnail = models.ImageField(upload_to='Msg_Attachment/',max_length=500,blank=True,null=True)

    @property
    def render_attachment_url(self):
        return settings.MEDIA_URL+str(self.attachment)

    @property
    def render_thumbnail_url(self):
        return settings.MEDIA_URL+str(self.thumbnail)

    def create_thumbnail(self):


        # original code for this method came from
        # http://snipt.net/danfreak/generate-thumbnails-in-django-with-pil/

        # If there is no image associated with this.
        # do not create thumbnail
        if not self.attachment:
            return

        if self.attachment.name.endswith("jpg") or self.attachment.name.endswith("jpeg") or self.attachment.name.endswith("png"):
            from PIL import Image
            from cStringIO import StringIO
            from django.core.files.uploadedfile import SimpleUploadedFile
            import os

            # Set our max thumbnail size in a tuple (max width, max height)
            THUMBNAIL_SIZE = (200,200)

            if self.attachment.name.endswith("jpg") or self.attachment.name.endswith("jpeg"):
                DJANGO_TYPE = "image/jpeg"
            elif self.attachment.endswidth("png"):
                DJANGO_TYPE = "image/png"

            if DJANGO_TYPE == 'image/jpeg':
                PIL_TYPE = 'jpeg'
                FILE_EXTENSION = 'jpg'
            elif DJANGO_TYPE == 'image/png':
                PIL_TYPE = 'png'
                FILE_EXTENSION = 'png'

            # Open original photo which we want to thumbnail using PIL's Image
            image = Image.open(StringIO(self.attachment.read()))

            # Convert to RGB if necessary
            # Thanks to Limodou on DjangoSnippets.org
            # http://www.djangosnippets.org/snippets/20/
            #
            # I commented this part since it messes up my png files
            #
            #if image.mode not in ('L', 'RGB'):
            #    image = image.convert('RGB')

            # We use our PIL Image object to create the thumbnail, which already
            # has a thumbnail() convenience method that contrains proportions.
            # Additionally, we use Image.ANTIALIAS to make the image look better.
            # Without antialiasing the image pattern artifacts may result.
            image.thumbnail(THUMBNAIL_SIZE, Image.ANTIALIAS)

            # Save the thumbnail
            temp_handle = StringIO()
            image.save(temp_handle, PIL_TYPE)
            temp_handle.seek(0)

            # Save image to a SimpleUploadedFile which can be saved into
            # ImageField
            suf = SimpleUploadedFile(os.path.split(self.attachment.name)[-1],
                 temp_handle.read(), content_type=DJANGO_TYPE)
            # Save SimpleUploadedFile into image field
            self.thumbnail = suf


    def save(self):
        try:
            self.create_thumbnail()
        except Exception as msg:
            print(str(msg))
            print("Thumbnail creation failed.")
        super(MessageAttachment, self).save()


    class Meta:
        db_table = 'message_attachment'

class ConversationSinceDate(GenericModel):
    user = models.ForeignKey(User)
    since_ts = models.BigIntegerField(default=0)

    class Meta:
        db_table = 'conversation_since_date'

class Conversation(GenericModel):
    title = models.CharField(max_length=500,null=True,blank=True)
    conversation_id = models.CharField(max_length=255,blank=True)
    other_users = models.ManyToManyField(User)
    users_left = models.ManyToManyField(User,related_name="conversation_left_users")
    since_dates = models.ManyToManyField(ConversationSinceDate)

    class Meta:
        db_table="conversation"

class UserMessageResponseTime(GenericModel):
    total_reply_count = models.IntegerField(default=0)
    user_1 = models.ForeignKey(User)
    user_2 = models.ForeignKey(User, related_name="user2")
    total_response_time = models.BigIntegerField(default=0)
    last_count_entry_id = models.IntegerField(default=0)

    @classmethod
    def average_response_time(cls, user_id):
        t_r_t = cls.objects.filter(user_1_id=user_id).aggregate(total_response_time=Sum('total_response_time'))
        t_r_c = cls.objects.filter(user_1_id=user_id).aggregate(total_reply_count=Sum('total_reply_count'))
        if t_r_t['total_response_time']and t_r_c['total_reply_count']:
            return t_r_t['total_response_time']/(t_r_c['total_reply_count'])
        return -1

    class Meta:
        db_table = "user_message_response_time"

class ChattedUser(GenericModel):
    user = models.ForeignKey(User)
    chatted_users = models.ManyToManyField(User, related_name="my_chatted_users")

    class Meta:
        db_table = "chatted_users"

class UserMessage(GenericModel):
    sender = models.ForeignKey(User)
    receiver = models.ForeignKey(User, null=True, related_name="msg_receiver")
    message = models.ForeignKey(Message)
    last_seen = models.DateTimeField(null=True,blank=True)
    chat_type = models.IntegerField(default=0) ###0 means p2p chat and 1 means group chat, 2 means topic discussion, 3 means auto generated event message.
    conversation = models.ForeignKey(Conversation,null=True)
    topic_object_class = models.CharField(max_length=40,blank=True, null=True)
    topic_object_id = models.BigIntegerField(blank=True, null=True)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        super(GenericModel,self).save(force_insert, force_update, using,update_fields)

        if self.sender.champuser.type.name == "teacher":
            try:
                if self.__class__.objects.filter(sender_id=self.receiver.pk, receiver_id=self.sender.pk).exists():
                    response_objects = UserMessageResponseTime.objects.filter(user_1_id=self.sender.pk, user_2_id=self.receiver.pk)
                    if response_objects.exists():
                        response_object = response_objects.first()
                        last_count_id = response_object.last_count_entry_id
                        last_entries = self.__class__.objects.filter(sender_id=self.receiver.pk, receiver_id=self.sender.pk, id__gt=last_count_id).order_by('id')
                        if last_entries.exists():
                            last_entry = last_entries.first()
                            response_time = self.message.date_created - last_entry.message.date_created
                            response_object.total_reply_count += 1
                            response_object.total_response_time += response_time
                            response_object.last_count_entry_id = self.pk
                            response_object.save()
                    else:
                        last_entries = self.__class__.objects.filter(sender_id=self.receiver.pk, receiver_id=self.sender.pk).order_by('id')
                        last_entry = last_entries.first()
                        response_time = self.message.date_created - last_entry.message.date_created
                        urt = UserMessageResponseTime()
                        urt.total_reply_count = 1
                        urt.user_1_id = self.sender.pk
                        urt.user_2_id = self.receiver.pk
                        urt.total_response_time = response_time
                        urt.last_count_entry_id = self.pk
                        urt.save()
            except Exception as exp:
                pass

        sender_chatted_users = ChattedUser.objects.filter(user_id=self.sender.pk)
        if sender_chatted_users.exists():
            sender_chat_user = sender_chatted_users.first()
            if not sender_chat_user.chatted_users.filter(id=self.receiver.pk).exists():
                sender_chat_user.chatted_users.add(self.receiver)
        else:
            sender_chat_user = ChattedUser()
            sender_chat_user.user_id = self.sender.pk
            sender_chat_user.save()

            sender_chat_user.chatted_users.add(self.receiver)

        receiver_chatted_users = ChattedUser.objects.filter(user_id=self.receiver.pk)
        if receiver_chatted_users.exists():
            receiver_chat_user = receiver_chatted_users.first()
            if not receiver_chat_user.chatted_users.filter(id=self.sender.pk).exists():
                receiver_chat_user.chatted_users.add(self.sender)
        else:
            receiver_chat_user = ChattedUser()
            receiver_chat_user.user_id = self.receiver.pk
            receiver_chat_user.save()

            receiver_chat_user.chatted_users.add(self.sender)



    class Meta:
        db_table="user_message"

class Chatbox(GenericModel):
    chat_type = models.IntegerField(default=0) ###0 means p2p and 1 means conversation
    chat_id = models.CharField(max_length=500,null=True,blank=True)
    open_status = models.IntegerField(default=0) ###0 means closed, 1 means opened.
    active_status = models.IntegerField(default=0) ###0 means hide, 1 means show.
    title = models.CharField(max_length=255,null=True, blank=True)
    topic_class_name = models.CharField(max_length=100,null=True,blank=True)
    topic_object_id = models.BigIntegerField(null=True,blank=True)

    class Meta:
        db_table = "chatbox"

class ChatSettings(GenericModel):
    user = models.ForeignKey(User)
    online_status = models.IntegerField(default=1) ###0 means offline, 1 means online
    enter_to_send = models.IntegerField(default=1) ###0 means no and 1 means yes.
    chatboxes = models.ManyToManyField(Chatbox)

    class Meta:
        db_table = "chat_settings"

class BlockUserEntry(GenericModel):
    user = models.ForeignKey(User)

    class Meta:
        db_table = 'block_user_entry'

class UserBlockSettings(GenericModel):
    user = models.ForeignKey(User)
    blocked_users = models.ManyToManyField(BlockUserEntry,related_name="blocked_users")

    class Meta:
        db_table = 'user_blocked_settings'

class OTSessionTable(GenericModel):
    sessionid = models.CharField(blank=False,null=False,max_length=40)
    otsessionId = models.CharField(blank=False,null=False,max_length=60)
    ottoken = models.CharField(blank=True,null=True,max_length=400)

    class Meta:
        db_table='ot_session'



class ResetPasswordToken(GenericModel):
    user = models.ForeignKey(User)
    token = models.CharField(max_length=255)
    used = models.IntegerField(default=0)

    class Meta:
        db_table='reset_password_token'

class SubjectLevel(GenericModel):
    name = models.CharField(max_length=100,blank=True)

    def get_queryset(self):
        queryset = super(SubjectLevel, self).get_queryset()
        queryset = queryset.exclude(name="Professional Development")
        return queryset

    class Meta:
        db_table = "subject_level"

class CountryLevel(GenericModel):
    country = models.ForeignKey(Country, null=True)
    level = models.ForeignKey(SubjectLevel)

    class Meta:
        db_table = 'country_level'

class Major(GenericModel):
    name = models.CharField(max_length=512)
    description = models.TextField(null=True, blank=True,default='')
    level = models.ForeignKey(SubjectLevel, null=True)
    creator = models.ForeignKey(User)
    rank = models.IntegerField(default=0)

    class Meta:
        db_table=u'major'

class TopSearch(GenericModel):
    user = models.ForeignKey(User)
    subject = models.ForeignKey(Major)
    search_frequency = models.IntegerField(default=0)

    class Meta:
        db_table = "top_search"

class UserTopCourses(GenericModel):
    user = models.ForeignKey(User)
    level = models.ForeignKey(SubjectLevel)
    subject = models.ForeignKey(Major)
    description = models.TextField()

    # class Meta:
    #     db_table=u'user_top_courses'

class UserMajor(GenericModel):
    user = models.ForeignKey(User)
    major = models.ForeignKey(Major)
    currency = models.ForeignKey(Currency, null=True)
    rate = models.DecimalField(default=0, decimal_places=2,max_digits=10)

    class Meta:
        db_table=u'user_major'

class Education(GenericModel):
    user = models.ForeignKey(User)
    degree = models.ForeignKey(Certifications,null=True)
    level = models.ForeignKey(SubjectLevel, null=True)
    institution = models.ForeignKey(Schools)
    session = models.CharField(max_length=255, null=True, blank=True, verbose_name='class')
    cgpa = models.CharField(max_length=255, default='0.0')
    is_current = models.BooleanField(default=False)
    verified = models.IntegerField(default=0) #0 means not verified and 1 means verified.
    location = models.CharField(max_length=500,null=True,blank=True)
    date_attended = models.BigIntegerField(default=0, null=True,blank=True)
    end_year = models.BigIntegerField(default=0, null=True, blank=True)
    major = models.ForeignKey(Major,null=True)

    class Meta:
        db_table = u'education'

class EducationVerification(GenericModel):
    education = models.ForeignKey(Education)
    scanned_certificate = models.ImageField(upload_to='image/')
    status = models.IntegerField(default=0) ###0 means not verified and 1 means verified.
    verified_by = models.BigIntegerField()
    submit_date = models.DateTimeField(auto_now_add=True,blank=True,null=True)
    verification_date = models.DateTimeField(auto_now_add=True,blank=True,null=True)

    class Meta:
        db_table=u'education_verification'

class TeachingExperiences(GenericModel):
    user = models.ForeignKey(User)
    experience = models.TextField()

    class Meta:
        db_table=u'teaching_experiences'

class ExtraCurricularInterest(GenericModel):
    user = models.ForeignKey(User)
    interest = models.TextField()

    class Meta:
        db_table = u'extra_curricular_interest'

class ProfileMyVideo(GenericModel):
    user = models.ForeignKey(User)
    host_type = models.CharField(max_length=50, blank=True) ###YOUTUBE/VIMEO
    url = models.CharField(max_length=500,null=True,blank=True)
    title = models.CharField(max_length=100, blank=True, default='')
    video_length = models.CharField(max_length=100, default='') ###In seconds
    description = models.TextField(blank=True)
    file_name = models.CharField(max_length=500,default='')
    thumbnail_url = models.CharField(max_length=500,default='')

    @property
    def vimdeo_video_id(self):
        if self.host_type == "VIMEO":
            return self.url.replace("/videos/", "")

    @property
    def youtube_video_url(self):
        if self.host_type == "YOUTUBE":
            return "https://www.youtube.com/watch?v=%s" % self.url

    @property
    def format_duration(self):
        duration = self.video_length.replace(" ", "").replace("h", ":").replace("m", ":").replace("s", ":")
        if duration.endswith(":"):
            duration = duration[:-1]
        return duration

    @property
    def render_user(self):
        return self.user.champuser.fullname

    @property
    def render_url(self):
        return "<a href='%s'>%s</a>" % (self.url, self.url)

    @property
    def iframe_url(self):
        return """<iframe" width="500" height="345" allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen" src="%s">
        </iframe>""" % self.url

    @property
    def render_details_url(self):
        pass

    @property
    def details_config(self):
        d = OrderedDict()
        d["User"] = "render_user"
        d["video"] = "iframe_url"
        d["Uploaded On"] = "date_created"
        return d

    @property
    def tabs_config(self):
        return [
            TabView(
                title="Transactions",
                queryset=Transaction.objects.all(),
                url=reverse("admin_partial_details_view", kwargs={ "partial_parent_model_name": self.__class__.__name__.lower() ,"partial_parent": self.pk, "partial_model_name": "transactions" })
            ),
            TabView(
                title="Payments",
                queryset=PaymentLog.objects.all(),
                url=reverse("admin_partial_details_view", kwargs={ "partial_parent_model_name": self.__class__.__name__.lower() ,"partial_parent": self.pk, "partial_model_name": "payments" })
            )
        ]

    class Meta:
        db_table=u'profile_my_video'

class AcademicCertificates(GenericModel):
    cert_type = models.CharField(max_length=255)
    cert_name = models.CharField(max_length=255)
    cert_content = models.ImageField(upload_to="images/")

    class Meta:
        db_table = 'academic_certificates'

class TeachingSessionMembers(GenericModel):
    member = models.ForeignKey(User)
    creator = models.BooleanField(default=False)

    class Meta:
        db_table='teaching_session_members'

class TeachingSession(GenericModel):
    members = models.ForeignKey(TeachingSessionMembers)
    end_date = models.DateTimeField(null=True,blank=True)
    duration = models.IntegerField(default=0)

    class Meta:
        db_table='teaching_session'

class Reviews(GenericModel):
    teaching_session = models.ForeignKey(TeachingSession)
    review_by = models.ForeignKey(User)
    rating = models.IntegerField(default=0)
    comment = models.TextField()

    class Meta:
        db_table='reviews'

class LessonAnswer(GenericModel):
    user = models.ForeignKey(User)
    response_user = models.ForeignKey(User,related_name='lesson_response_to')
    text = models.TextField()

    class Meta:
        db_table = 'lesson_answer'

class LessonAnswerAttachment(GenericModel):
    lesson_answer = models.ForeignKey(LessonAnswer)
    attachment = models.FileField(upload_to='Msg_Attachment/')
    attachment_name = models.CharField(max_length=500,null=True,blank=True)
    thumbnail = models.ImageField(upload_to='Msg_Attachment/',max_length=500,blank=True,null=True)

    # def get_attachment_thumbnail_link

    def create_thumbnail(self):


        # original code for this method came from
        # http://snipt.net/danfreak/generate-thumbnails-in-django-with-pil/

        # If there is no image associated with this.
        # do not create thumbnail
        if not self.attachment:
            return

        if self.attachment.name.endswith("jpg") or self.attachment.name.endswith("jpeg") or self.attachment.name.endswith("png"):
            from PIL import Image
            from cStringIO import StringIO
            from django.core.files.uploadedfile import SimpleUploadedFile
            import os

            # Set our max thumbnail size in a tuple (max width, max height)
            THUMBNAIL_SIZE = (200,200)

            if self.attachment.name.endswith("jpg") or self.attachment.name.endswith("jpeg"):
                DJANGO_TYPE = "image/jpeg"
            elif self.attachment.endswidth("png"):
                DJANGO_TYPE = "image/png"

            if DJANGO_TYPE == 'image/jpeg':
                PIL_TYPE = 'jpeg'
                FILE_EXTENSION = 'jpg'
            elif DJANGO_TYPE == 'image/png':
                PIL_TYPE = 'png'
                FILE_EXTENSION = 'png'

            # Open original photo which we want to thumbnail using PIL's Image
            image = Image.open(StringIO(self.attachment.read()))

            # Convert to RGB if necessary
            # Thanks to Limodou on DjangoSnippets.org
            # http://www.djangosnippets.org/snippets/20/
            #
            # I commented this part since it messes up my png files
            #
            #if image.mode not in ('L', 'RGB'):
            #    image = image.convert('RGB')

            # We use our PIL Image object to create the thumbnail, which already
            # has a thumbnail() convenience method that contrains proportions.
            # Additionally, we use Image.ANTIALIAS to make the image look better.
            # Without antialiasing the image pattern artifacts may result.
            image.thumbnail(THUMBNAIL_SIZE, Image.ANTIALIAS)

            # Save the thumbnail
            temp_handle = StringIO()
            image.save(temp_handle, PIL_TYPE)
            temp_handle.seek(0)

            # Save image to a SimpleUploadedFile which can be saved into
            # ImageField
            suf = SimpleUploadedFile(os.path.split(self.attachment.name)[-1],
                 temp_handle.read(), content_type=DJANGO_TYPE)
            # Save SimpleUploadedFile into image field
            self.thumbnail = suf


    def save(self):
        try:
            self.create_thumbnail()
        except Exception as msg:
            print(str(msg))
            print("Thumbnail creation failed.")
        super(LessonAnswerAttachment, self).save()

    class Meta:
        db_table = 'lesson_answer_attachment'

class FeedbackLikes(GenericModel):
    user = models.ForeignKey(User)
    feedback = models.BooleanField(default=False) #True means positive and False means negative.
    comment = models.TextField(null=True,blank=True)

    class Meta:
        db_table = 'feedback_likes'

class LessonReview(GenericModel):
    comment = models.TextField(null=True,blank=True)
    rating1 = models.IntegerField(default=0,null=True,blank=True) ###Teaching skill
    rating2 = models.IntegerField(default=0,null=True,blank=True) ###Responsibility
    rating3 = models.IntegerField(default=0,null=True,blank=True) ###Punctuality
    rating4 = models.IntegerField(default=0,null=True,blank=True) ###Availability
    rating5 = models.IntegerField(default=0,null=True,blank=True) ###Communication
    created_by = models.ForeignKey(User, null=True)
    reviews = models.ManyToManyField(FeedbackLikes)

    @property
    def positive_feedback_count(self):
        return self.reviews.filter(feedback=True).count()

    @property
    def negative_feedback_count(self):
        return self.reviews.filter(feedback=False).count()

    @property
    def average_rating(self):
        try:
            return float(self.rating1 + self.rating2 + self.rating3 + self.rating4 + self.rating5)/5
        except:
            return 0

    class Meta:
        db_table = 'lesson_review'

class LessonAttachment(GenericModel):
    #lesson = models.ForeignKey(LessonRequest)
    attachment = models.FileField(upload_to='Msg_Attachment/')
    attachment_name = models.CharField(max_length=500,null=True,blank=True)
    thumbnail = models.ImageField(upload_to='Msg_Attachment/',max_length=500,blank=True,null=True)

    # def get_attachment_thumbnail_link

    def create_thumbnail(self):


        # original code for this method came from
        # http://snipt.net/danfreak/generate-thumbnails-in-django-with-pil/

        # If there is no image associated with this.
        # do not create thumbnail
        if not self.attachment:
            return

        if self.attachment.name.endswith("jpg") or self.attachment.name.endswith("jpeg") or self.attachment.name.endswith("png"):
            from PIL import Image
            from cStringIO import StringIO
            from django.core.files.uploadedfile import SimpleUploadedFile
            import os

            # Set our max thumbnail size in a tuple (max width, max height)
            THUMBNAIL_SIZE = (200,200)

            if self.attachment.name.endswith("jpg") or self.attachment.name.endswith("jpeg"):
                DJANGO_TYPE = "image/jpeg"
            elif self.attachment.endswidth("png"):
                DJANGO_TYPE = "image/png"

            if DJANGO_TYPE == 'image/jpeg':
                PIL_TYPE = 'jpeg'
                FILE_EXTENSION = 'jpg'
            elif DJANGO_TYPE == 'image/png':
                PIL_TYPE = 'png'
                FILE_EXTENSION = 'png'

            # Open original photo which we want to thumbnail using PIL's Image
            image = Image.open(StringIO(self.attachment.read()))

            # Convert to RGB if necessary
            # Thanks to Limodou on DjangoSnippets.org
            # http://www.djangosnippets.org/snippets/20/
            #
            # I commented this part since it messes up my png files
            #
            #if image.mode not in ('L', 'RGB'):
            #    image = image.convert('RGB')

            # We use our PIL Image object to create the thumbnail, which already
            # has a thumbnail() convenience method that contrains proportions.
            # Additionally, we use Image.ANTIALIAS to make the image look better.
            # Without antialiasing the image pattern artifacts may result.
            image.thumbnail(THUMBNAIL_SIZE, Image.ANTIALIAS)

            # Save the thumbnail
            temp_handle = StringIO()
            image.save(temp_handle, PIL_TYPE)
            temp_handle.seek(0)

            # Save image to a SimpleUploadedFile which can be saved into
            # ImageField
            suf = SimpleUploadedFile(os.path.split(self.attachment.name)[-1],
                 temp_handle.read(), content_type=DJANGO_TYPE)
            # Save SimpleUploadedFile into image field
            self.thumbnail = suf


    def save(self):
        try:
            self.create_thumbnail()
        except Exception as msg:
            print(str(msg))
            print("Thumbnail creation failed.")
        super(LessonAttachment, self).save()

    class Meta:
        db_table = 'lesson_attachment'

class Agreement(GenericModel):
    tutor = models.ForeignKey(User,related_name="agreement_tutor")
    students = models.ManyToManyField(User)
    hourly_rate = models.DecimalField(decimal_places=2,max_digits=10)
    message = models.TextField(null=True,blank=True)

    class Meta:
        db_table = 'lesson_agreement'

class LessonUserActivity(GenericModel):
    user = models.ForeignKey(User)
    last_access_time = models.BigIntegerField(default=0)
    online_status = models.IntegerField(default=0)

    @property
    def is_user_online(self):
        now_time = Clock.utc_timestamp()
        return ( now_time - self.last_access_time ) <= 2000

    class Meta:
        db_table = 'lesson_user_activity'

class ScheduledJobs(GenericModel):
    object_class = models.CharField(max_length=50)
    object_id = models.IntegerField()
    job_id = models.CharField(max_length=500)

    class Meta:
        db_table = 'scheduled_jobs'

class LessonRequest(GenericModel):
    title = models.CharField(max_length=500)
    user = models.ForeignKey(User)
    request_user = models.ForeignKey(User,related_name='lesson_request_to')
    other_users = models.ManyToManyField(User,related_name="lesson_other_users")
    lesson_date = models.BigIntegerField(null=True,blank=True) ###in seconds
    lesson_time = models.BigIntegerField(null=True,blank=True) ###in seconds
    duration = models.BigIntegerField(null=True,blank=True) ###in seconds
    extra_time = models.BigIntegerField(default=0)
    subjects = models.ManyToManyField(Major)
    currency = models.CharField(max_length=100,default='')
    rate = models.DecimalField(decimal_places=2, max_digits=10,default=0)
    message = models.TextField(blank=True)
    due_date = models.BigIntegerField(null=True,blank=True)
    type = models.IntegerField(null=True,blank=True)
    answer = models.ForeignKey(LessonAnswer,null=True)
    status = models.IntegerField(default=LessonStatus.started.value)
    timezone = models.CharField(null=True,blank=True,max_length=200)
    review = models.ForeignKey(LessonReview,null=True)
    approved = models.IntegerField(null=True,blank=True,default=ApprovalStatus.not_approved.value)
    repeat = models.CharField(null=True,blank=True,max_length=100) ###NO_REPEAT, REPEAT_DAILY, REPEAT_WEEKLY
    attachments = models.ManyToManyField(LessonAttachment)
    transactions = models.ManyToManyField(Transaction,null=True)
    agreement = models.ForeignKey(Agreement,null=True)
    base_transaction = models.ForeignKey(Transaction,related_name='base_transaction',null=True)
    payment_status = models.IntegerField(default=0) ###0 means transaction not made, 1 means transaction made and successful, 2 means transaction made and unsuccessful.
    whiteboard = models.ForeignKey(Whiteboard,null=True)
    user_activities = models.ManyToManyField(LessonUserActivity)


    @property
    def render_subjects(self):
        return ','.join(self.subjects.values_list('name', flat=True))


    def is_active(self):
        return self.status == LessonStatus.started.value or self.status == LessonStatus.approved.value

    @classmethod
    def search_field_map(cls):
        return {
            "title": "title",
            "tutor": "request_user"
        }

    @property
    def render_student_name(self):
        return "<a href='%s'>%s</a>" % (self.user.champuser.profile_url, self.user.champuser.fullname)

    @property
    def render_lesson_duration(self):
        return str(self.duration)+" min"

    @property
    def render_tutor(self):
        return "<a href='%s'>%s</a>" % (self.request_user.champuser.profile_url, self.request_user.champuser.fullname)

    @property
    def render_details_template(self):
        template = loader.get_template("lessons/lesson_details.html")
        cntxt = Context({ 'lesson': self})
        rendered = template.render(cntxt)
        return rendered

    @property
    def render_lesson_type(self):
        if self.type == 0:
            return "Written Lesson"
        else:
            return "Live Lesson"

    @classmethod
    def table_columns(cls):
        return [
            "title: Lesson Title", "render_lesson_type", "render_lesson_duration", "render_tutor", "date_created"
        ]

    @property
    def get_object_inline_button(self):
        return []

    @classmethod
    def filter_search(cls, request, queryset, **kwargs):
        title = request.GET.get('title')
        type = request.GET.get('type')
        if title:
            queryset = queryset.filter(title__icontains=title)
        if type:
            queryset = queryset.filter(type__name=type)
        return queryset


    @classmethod
    def get_searchable_columns(cls):
        return [
            ("title", "text", False),
            ("type", "text", False),
            ("date_created", "timestamp", True)
        ]

    @property
    def details_config(self):
        d = OrderedDict()
        d["Lesson Title"] = "title"
        return d

    @property
    def tabs_config(self):
        return [

        ]

    class Meta:
        db_table = 'lesson_request'

class WrittenLessonAttachment(LessonAttachment):
    class Meta:
        proxy = True

class WrittenLessonAnswer(LessonAnswer):
    class Meta:
        proxy = True

class LiveLesson(LessonRequest):
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.type = LessonTypes.live_lesson.value
        super(LiveLesson,self).save()

    @property
    def is_live(self):
        now_time = Clock.utc_timestamp()
        lesson_scheduled_time = self.lesson_time
        lesson_end_time = self.lesson_time + self.duration + self.extra_time
        if now_time >= lesson_scheduled_time and now_time < lesson_end_time:
            return True
        return False

    @property
    def lesson_status(self):
        now_time = Clock.utc_timestamp()
        lesson_scheduled_time = self.lesson_time
        lesson_end_time = self.lesson_time + self.duration + self.extra_time
        if now_time >= lesson_scheduled_time and now_time < lesson_end_time:
            return "LIVE"
        elif now_time >= lesson_end_time:
            return "PAST"
        elif now_time < lesson_scheduled_time:
            return "SCHEDULED"

    class Meta:
        proxy = True

class LiveLessonMembers(GenericModel):
    user = models.ForeignKey(User)
    lesson = models.ForeignKey(LiveLesson)
    active = models.IntegerField()

    class Meta:
        db_table = 'live_lesson_members'

class WrittenLesson(LessonRequest):

    def get_comments(self):
        return WrittenLessonCommentAttachment.objects.filter(lesson_id=self.pk)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.type = LessonTypes.written.value
        super(WrittenLesson,self).save()

    class Meta:
        proxy = True

class Comment(GenericModel):
    comment_text = models.TextField(null=True)

    class Meta:
        db_table='comment'

class CommentAttachment(GenericModel):
    attachment = models.FileField(upload_to='Msg_Attachment/')
    attachment_name = models.CharField(max_length=500,null=True,blank=True)
    thumbnail = models.ImageField(upload_to='Msg_Attachment/',max_length=500,blank=True,null=True)

    # def get_attachment_thumbnail_link

    def create_thumbnail(self):


        # original code for this method came from
        # http://snipt.net/danfreak/generate-thumbnails-in-django-with-pil/

        # If there is no image associated with this.
        # do not create thumbnail
        if not self.attachment:
            return

        if self.attachment.name.endswith("jpg") or self.attachment.name.endswith("jpeg") or self.attachment.name.endswith("png"):
            from PIL import Image
            from cStringIO import StringIO
            from django.core.files.uploadedfile import SimpleUploadedFile
            import os

            # Set our max thumbnail size in a tuple (max width, max height)
            THUMBNAIL_SIZE = (200,200)

            if self.attachment.name.endswith("jpg") or self.attachment.name.endswith("jpeg"):
                DJANGO_TYPE = "image/jpeg"
            elif self.attachment.endswidth("png"):
                DJANGO_TYPE = "image/png"

            if DJANGO_TYPE == 'image/jpeg':
                PIL_TYPE = 'jpeg'
                FILE_EXTENSION = 'jpg'
            elif DJANGO_TYPE == 'image/png':
                PIL_TYPE = 'png'
                FILE_EXTENSION = 'png'

            # Open original photo which we want to thumbnail using PIL's Image
            image = Image.open(StringIO(self.attachment.read()))

            # Convert to RGB if necessary
            # Thanks to Limodou on DjangoSnippets.org
            # http://www.djangosnippets.org/snippets/20/
            #
            # I commented this part since it messes up my png files
            #
            #if image.mode not in ('L', 'RGB'):
            #    image = image.convert('RGB')

            # We use our PIL Image object to create the thumbnail, which already
            # has a thumbnail() convenience method that contrains proportions.
            # Additionally, we use Image.ANTIALIAS to make the image look better.
            # Without antialiasing the image pattern artifacts may result.
            image.thumbnail(THUMBNAIL_SIZE, Image.ANTIALIAS)

            # Save the thumbnail
            temp_handle = StringIO()
            image.save(temp_handle, PIL_TYPE)
            temp_handle.seek(0)

            # Save image to a SimpleUploadedFile which can be saved into
            # ImageField
            suf = SimpleUploadedFile(os.path.split(self.attachment.name)[-1],
                 temp_handle.read(), content_type=DJANGO_TYPE)
            # Save SimpleUploadedFile into image field
            self.thumbnail = suf


    def save(self):
        try:
            self.create_thumbnail()
        except Exception as msg:
            print(str(msg))
            print("Thumbnail creation failed.")
        super(CommentAttachment, self).save()

    class Meta:
        db_table = 'comment_attachment'

class WrittenLessonComment(Comment):
    created_by = models.ForeignKey(User)
    lesson = models.ForeignKey(WrittenLesson)
    class Meta:
        db_table = 'written_lesson_comment'

class WrittenLessonCommentAttachment(CommentAttachment):
    comment = models.ForeignKey(WrittenLessonComment,null=True)

    class Meta:
        db_table = 'written_lesson_comment_attachment'

class EmailLog(GenericModel):
    sender = models.CharField(max_length=500)
    receiver = models.CharField(max_length=500)
    subject = models.CharField(max_length=500)
    html_body = models.TextField()
    text_body = models.TextField()
    job_id = models.CharField(max_length=500)
    status = models.CharField(max_length=20,blank=True) ###SUCCESS, FAILED
    exception_stacktrace = models.TextField(blank=True)

    class Meta:
        db_table = 'email_log'

class ProgressReport(GenericModel):
    sender = models.ForeignKey(User)
    receiver = models.ForeignKey(User,related_name="receiver")
    sender_email = models.CharField(max_length=300,null=True,blank=True)
    receiver_email = models.CharField(max_length=300,null=True,blank=True)
    status = models.CharField(max_length=100,null=True,blank=True)
    text = models.TextField()
    attachment = models.FileField(upload_to='Progress_Report_Attachment/')

    class Meta:
        db_table = 'progress_report'

class IntegerUnit(GenericModel):
    value = models.IntegerField()

    class Meta:
        db_table = 'integer_unit'

class FloatUnit(GenericModel):
    value = models.FloatField()

    class Meta:
        db_table = 'float_unit'

class CharUnit(GenericModel):
    value = models.CharField(max_length=500)

    class Meta:
        db_table = 'char_unit'

class HourlyRateUnit(GenericModel):
    currency = models.CharField(max_length=100)
    rate_value = models.FloatField(default=0)
    rate_value2 = models.FloatField(default=0)

    class Meta:
        db_table = 'hourly_rate_unit'

class TimeRangeUnit(GenericModel):
    start_time = models.BigIntegerField()
    end_time = models.BigIntegerField()

    class Meta:
        db_table = 'time_range_unit'

class TimezoneMapper(GenericModel):
    country = models.ForeignKey(LocationEntity)
    tz = models.CharField(max_length=100)
    tz_offset = models.CharField(max_length=100, blank=True)

    class Meta:
        db_table = 'timezone_mapper'

class TimezoneDifference(GenericModel):
    min_value = models.IntegerField(default=0)
    max_value = models.IntegerField(default=0)

    class Meta:
        db_table = 'timezone_differerence'

class JobPreference(GenericModel):
    nationalities = models.ManyToManyField(Country)
    rate = models.ForeignKey(HourlyRateUnit,null=True)
    gender = models.CharField(max_length=20,null=True,blank=True)
    timezones = models.ManyToManyField(TimezoneMapper,related_name="timezone_preferences")
    # currencies = models.ManyToManyField(CharUnit,related_name="currency_preferences")
    currency = models.CharField(max_length=10,blank=True)
    languages = models.ManyToManyField(Language,related_name="language_preferences")
    availability = models.IntegerField() ###AvailableStatus.available, AvailableStatus.unavailable
    level = models.ForeignKey(SubjectLevel, null=True)
    subjects = models.ManyToManyField(Major)
    timezone_diffs = models.ManyToManyField(TimezoneDifference)

    class Meta:
        db_table = 'job_preference'

class JobCriteria(GenericModel):
    level = models.CharField(max_length=500,null=True,blank=True)
    subjects = models.ManyToManyField(Courses)

    class Meta:
        db_table = 'job_criteria'

class JobAttachment(LessonAttachment):
    class Meta:
        proxy = True

class PriceSettlement(GenericModel):
    price = models.DecimalField(decimal_places=2,max_digits=10)
    price2 = models.DecimalField(decimal_places=2,max_digits=10)
    currency = models.CharField(max_length=10,blank=True)
    created_by = models.ForeignKey(User, related_name="settlement_created_by")
    status = models.IntegerField(default=PriceSettlementStatus.Created.value) ###Created.

    class Meta:
        db_table = 'job_application_price_settlement'

class JobApplicationMessage(GenericModel):
    message = models.ForeignKey(Message)
    sender = models.ForeignKey(User)
    receiver = models.ForeignKey(User, related_name='job_app_message_receiver')

    @property
    def attachments(self):
        return MessageAttachment.objects.filter(message_id=self.message.pk)

    class Meta:
        db_table='application_message'

class JobApplication(GenericModel):
    tutor = models.ForeignKey(User)
    apply_message = models.ForeignKey(JobApplicationMessage, null=True)
    withdraw_message = models.ForeignKey(JobApplicationMessage, related_name="job_application_withdraw_message", null=True)
    reject_message = models.ForeignKey(JobApplicationMessage, related_name="job_application_reject_message", null=True)
    hire_message = models.ForeignKey(JobApplicationMessage, related_name="job_application_hire_message", null=True)
    settlement = models.ForeignKey(PriceSettlement)
    start_time = models.BigIntegerField()
    duration = models.BigIntegerField()
    repeat = models.IntegerField()
    status = models.IntegerField(default=JobApplicationStatus.active.value)
    initiated_as_label = models.CharField(max_length=20, blank=True) ###JobInvitation, JobRecommendation
    initiated_as_id = models.BigIntegerField(default=0) ##If JobInvitation then itss's id else JobRecommendation id
    price_edited_by = models.ForeignKey(User, null=True, related_name="price_edited_by")
    time_edited_by = models.ForeignKey(User, null=True, related_name="time_edited_by")
    duration_edited_by = models.ForeignKey(User, null=True, related_name="duration_edited_by")

    @property
    def render_application_status_verbal(self):
        if self.status == 1:
            return "Active"
        elif self.status == 3:
            return "Cancelled"
        elif self.status == 5:
            return "Rejected"
        elif self.status == 6:
            return "Negotiating"
        elif self.status == 7:
            return "Offered"
        elif self.status == 8:
            return "Hired"
        elif self.status == 9:
            return "Shortlisted"
        elif self.status == 10:
            return "Application Rejected"
        elif self.status == 12:
            return "Offer Rejected"
        elif self.status == 13:
            return "Application Withdrawn"

    @classmethod
    def filter_search(cls, request, queryset, **kwargs):
        for key, value in request.GET.items():
            if key == "id":
                try:
                    queryset = queryset.filter(pk=int(value))
                except:
                    queryset = cls.objects.none()
            elif key == "applied_by":
                queryset = queryset.filter(tutor__champuser__fullname__icontains=value)
            elif key == "applied_on":
                try:
                    date1 = value.split('-')[0]
                    date2 = value.split('-')[1]
                    ts1 = Clock.convert_to_utc(date1)
                    ts2 = Clock.convert_to_utc(date2)
                    queryset = queryset.filter(date_created__gte=ts1,date_created__lte=ts2)
                except:
                    queryset = cls.objects.none()
        return queryset

    @classmethod
    def get_searchable_columns(cls):
        return [
            ("id",False),
            ("applied_by", False),
            ("applied_on",True)
        ]

    @property
    def render_lesson_repeat_verbal(self):
        if self.repeat == 1:
            return "Single"
        elif self.repeat == 2:
            return "Daily"
        elif self.repeat == 3:
            return "Weekly"

    @property
    def price_settlement(self):
        return self.settlement.all().reverse().first()

    @property
    def render_tutor_name(self):
        return "<a href='%s'>%s</a>" % (reverse("user_profile", kwargs={ "pk": self.tutor.pk }),self.tutor.champuser.fullname)

    @property
    def render_job_title(self):
        return self.job_set.first().title if self.job_set.exists() else ''

    @classmethod
    def table_columns(cls):
        return ["id" , "render_job_title", "render_tutor_name", "render_application_status_verbal:Application Status", "date_created"]

    @property
    def get_last_message(self):
        last_message = self.message.all().reverse().first()
        return last_message

    class Meta:
        db_table = 'job_application'

class JobOffer(GenericModel):
    application = models.ForeignKey(JobApplication)
    start_date = models.BigIntegerField()
    duration = models.IntegerField()
    rate = models.FloatField()
    rate_currency = models.CharField(max_length=7)
    subject = models.CharField(max_length=100,blank=True)
    repeat = models.IntegerField() #single = 1, daily = 2, weekly = 3
    message = models.TextField()
    created_by = models.ForeignKey(User)
    status = models.IntegerField(default=1)

    @property
    def render_repeat_verbal(self):
        if self.repeat == 1:
            return "Single"
        elif self.repeat == 2:
            return "Daily"
        elif self.repeat == 3:
            return "Weekly"

    class Meta:
        db_table = 'job_offer'


class JobApplicationWithdrawFeedback(GenericModel):
    user = models.ForeignKey(User)
    application = models.ForeignKey(JobApplication)
    reason = models.CharField(max_length=500)
    details = models.TextField(null=True)
    attachment = models.ManyToManyField(JobAttachment, null=True)

    class Meta:
        db_table = 'job_application_withdraw_feedback'

class JobShortList(GenericModel):
    application = models.ForeignKey(JobApplication)

    class Meta:
        db_table = 'job_short_list'

class Job(GenericModel):
    lesson_type = models.IntegerField()  ###Written or Live lesson. 0 for Live Lesson and 1 for written lesson
    lesson_time = models.BigIntegerField()
    lesson_duration = models.BigIntegerField()
    title = models.CharField(max_length=500)
    description = models.TextField()
    posted_by = models.ForeignKey(User)
    end_date = models.BigIntegerField()
    criteria = models.ForeignKey(JobCriteria,null=True)
    preference = models.ForeignKey(JobPreference,null=True)
    status = models.IntegerField(default=1) ###Status may be opened, closed, suspended
    job_attachment = models.ManyToManyField(JobAttachment)
    lesson = models.ForeignKey(LessonRequest, related_name="lesson_request", null=True)
    applications = models.ManyToManyField(JobApplication)
    shortlist = models.ManyToManyField(JobShortList)
    other_students = models.ManyToManyField(User,related_name="other_students")
    payer = models.ForeignKey(User,related_name="lesson_payer")
    lesson_repeat = models.IntegerField() ###1 for single, 2 for daily and 3 for weekly

    # objects = JobModelManager()

    @property
    def is_deleted(self):
        return self.status == 4

    def get_recommended_users(self, exclude_ids=[]):
        champ_users = ChampUser.objects.filter(type__name="teacher")

        rejected_recommended_users = JobRecommendation.objects.filter(job_id=self.pk,status=JobRecommendationStatus.recommendation_rejected.value).values_list('user_id', flat=True)

        champ_users = champ_users.exclude(user_id__in=rejected_recommended_users)

        applied_users = JobApplication.objects.filter(job__id=self.pk).values_list('tutor_id', flat=True)

        champ_users = champ_users.exclude(user_id__in=applied_users)

        if exclude_ids:
            champ_users = champ_users.exclude(user_id__in=exclude_ids)

        # Match Gender
        if self.preference.gender == "male" or self.preference.gender == "female":
            champ_users = champ_users.filter(gender=self.preference.gender)

        # Match subjects
        if self.preference.subjects.exists():
            job_subjects = self.preference.subjects.all().values_list('pk', flat=True)
            champ_users = champ_users.filter(user__usermajor__major__id__in=job_subjects)

        # Match Conversational Languages.
        if self.preference.languages.exists():
            job_languages = self.preference.languages.all().values_list('pk', flat=True)
            champ_users = champ_users.filter(languages__language__id__in=job_languages)

        # Match timezones
        # if self.preference.timezones.exists():
        #     job_timezones = self.preference.timezones.all().values_list('tz', flat=True)
        #     champ_users = champ_users.filter(timezone__in=job_timezones)

        # Match timezone difference
        if self.preference.timezone_diffs.exists():
            timezone_offsets = self.preference.timezone_diffs.values_list('max_value', flat=True)

            temp_timezone_offsets = []

            for t_o in timezone_offsets:
                temp_timezone_offsets += [str(t_o), "-" + str(t_o)]

            timezone_offsets = temp_timezone_offsets

            job_timezones = TimezoneMapper.objects.filter(tz_offset__in=timezone_offsets)

            champ_users = champ_users.filter(timezone__in=job_timezones)

        # Match Nationality
        if self.preference.nationalities.exists():
            job_nationalities = self.preference.nationalities.all().values_list('name', flat=True)
            champ_users = champ_users.filter(nationality__name__in=job_nationalities)

        return champ_users.distinct()


    @classmethod
    def recommended_jobs_for_user(cls, user_id, job_id=None, ts=None):
        champ_users = ChampUser.objects.filter(user_id=user_id)
        if champ_users.exists():
            champ_user = champ_users.first()
            utc_now_ts = Clock.utc_timestamp()

            rejected_recommended_jobs = JobRecommendation.objects.filter(user_id=champ_user.user.pk,status=JobRecommendationStatus.recommendation_rejected.value).values_list('job_id', flat=True)

            jobs = cls.objects.filter(end_date__gt=utc_now_ts).exclude(pk__in=rejected_recommended_jobs)
            if ts:
                jobs = jobs.filter(date_created__gt=ts).exclude(status=4) #Exclude deleted jobs.

            if job_id:
                jobs = jobs.filter(pk=job_id)

            # Match Gender
            jobs = jobs.filter(preference__gender__in=[champ_user.gender,"any"])

            # Match subjects
            jobs = jobs.filter(preference__subjects__id__in=[user_major.major.pk for user_major in UserMajor.objects.filter(user_id=user_id)])

            # # Match timezones
            # jobs = jobs.filter(Q(preference__timezones=None) | Q(preference__timezones__tz__in=[champ_user.timezone]))

            # Match timezone difference
            tzm = TimezoneMapper.objects.filter(tz=champ_user.timezone)
            offset_values = list(set([abs(int(t.tz_offset)) for t in tzm]))
            jobs = jobs.filter(Q(preference__timezone_diffs=None) | Q(preference__timezone_diffs__max_value__in=offset_values))

            # Match Nationality
            jobs = jobs.filter(Q(preference__nationalities=None) | Q(preference__nationalities__name__in=[champ_user.nationality.name]))

            # Match Conversational Languages
            user_languages = champ_user.languages.all().values_list('language__id', flat=True)
            jobs = jobs.filter(Q(preference__languages=None) | Q(preference__languages__id__in=user_languages))

            return jobs.distinct()
        else:
            return Job.objects.none()


    @property
    def render_job_subjects(self):
        subjects = []
        if self.preference:
            subjects = [ s.value.capitalize() for s in self.preference.subjects.all() ]
            return ','.join(subjects)

    @property
    def render_nationalities(self):
        nationalities = []
        if self.preference:
            nationalities = [ s.value.capitalize() for s in self.preference.nationalities.all() ]
            return ','.join(nationalities)

    @property
    def render_timezones(self):
        if self.preference:
            timezones = [ str(s.min_value) + " - " + str(s.max_value) + " hours" for s in self.preference.timezone_diffs.all() ]
            return ', '.join(timezones)

    @property
    def render_languages(self):
        languages = []
        if self.preference:
            languages = [ s.value.capitalize() for s in self.preference.languages.all() ]
            return ','.join(languages)

    @property
    def render_currency(self):
        if self.preference:
            return self.preference.currency

    @property
    def render_preferred_rate(self):
        if self.preference:
            return self.preference.rate.rate_value


    @property
    def render_gender(self):
        if self.preference:
            return self.preference.gender.capitalize()

    @property
    def render_job_status(self):
        if self.status == 2:
            return "Closed"
        elif self.status == 3:
            return "Suspended"
        elif self.status == 1:
            return "Open"
    
    @property
    def is_job_opened(self):
        return self.end_date > Clock.utc_timestamp() and self.status == JobStatus.job_open.value

    @property
    def render_lesson_type_verbal(self):
        return 'Live Lesson' if self.lesson_type == 0 else 'Written Lesson'

    @property
    def render_lesson_repeat_verbal(self):
        if self.lesson_repeat == 1:
            return "Single"
        elif self.lesson_repeat == 2:
            return "Daily"
        elif self.lesson_repeat == 3:
            return "Weekly"

    @property
    def tabs_config(self):
        return [
            TabView(
                title="All Applications",
                queryset=Transaction.objects.all(),
                url=reverse("admin_tab_list_view", kwargs={ "parent_model_name": self.__class__.__name__.lower() ,"parent_id": self.pk, "tab_model_name": "jobapplication" })
            ),
            TabView(
                title="Invited Tutors",
                queryset=PaymentLog.objects.all(),
                url=reverse("admin_tab_list_view", kwargs={ "parent_model_name": self.__class__.__name__.lower() ,"parent_id": self.pk, "tab_model_name": "jobinvitation" })
            ),
            TabView(
                title="Recommended Tutors",
                queryset=PaymentLog.objects.all(),
                url=reverse("admin_tab_list_view", kwargs={ "parent_model_name": self.__class__.__name__.lower() ,"parent_id": self.pk, "tab_model_name": "jobrecommendation" })
            ),
        ]

    @classmethod
    def filter_search(cls, request, queryset, **kwargs):
        get_param = lambda x: request.GET.get(x)
        for key, value in request.GET.items():
            if key == "id":
                try:
                    id_ = int(value)
                    queryset = queryset.filter(pk=id_)
                except:
                    queryset = cls.objects.none()
            elif key == "title":
                queryset = queryset.filter(title__icontains=value)

            elif key == "posted_by":
                queryset = queryset.filter(posted_by__champuser__fullname__icontains=value)

            elif key == "posted_on":
                try:
                    date1 = value.split('-')[0]
                    date2 = value.split('-')[1]
                    ts1 = Clock.convert_to_utc(date1)
                    ts2 = Clock.convert_to_utc(date2)
                    queryset = queryset.filter(date_created__gte=ts1,date_created__lte=ts2)
                except:
                    queryset = cls.objects.none()

            elif key == "end_date":
                try:
                    date1 = value.split('-')[0]
                    date2 = value.split('-')[1]
                    ts1 = Clock.convert_to_utc(date1)
                    ts2 = Clock.convert_to_utc(date2)
                    queryset = queryset.filter(end_date__gte=ts1,end_date_lte=ts2)
                except:
                    queryset = cls.objects.none()

        return queryset



    @classmethod
    def get_searchable_columns(cls):
        return [
                   ("id", False),
                   ("title", False),
                   ("posted_by", False),
                   ("posted_on", True),
                   ("end_date", True)
        ]

    @property
    def render_details_template(self):
        template = loader.get_template("jobs/job_details.html")
        cntxt = Context({ 'job': self})
        rendered = template.render(cntxt)
        return rendered


    @property
    def render_lesson_duration(self):
        return str(self.lesson_duration)+" min"

    @property
    def render_posted_by(self):
        return "<a href='%s'>%s</a>" % ( reverse("user_profile", kwargs={ "pk": self.posted_by.pk }), self.posted_by.champuser.fullname)

    @classmethod
    def table_columns(cls):
        return [ "title", "render_job_status", "render_posted_by:Posted By", "render_lesson_type_verbal:Lesson Type", "render_lesson_duration:Duration", "date_created", "end_date:Job End Date" ]


    @property
    def get_all_status_count(self):
        all_invitation = JobInvitation.objects.filter(job_id=self.pk).count()
        all_recommendation = JobRecommendation.objects.filter(job_id=self.pk).count()
        return all_invitation + all_recommendation

    @property
    def get_applied_status_count(self):
        return self.applications.filter(~Q(status=5) & ~Q(status=6) & ~Q(status=9) & ~Q(status=10) & ~Q(status=12)).count() ###Get all except rejected, negotiating, shortlisted, rejected_application, offer_rejected

    @property
    def get_rejected_status_count(self):
        rejected_invitation = JobInvitation.objects.filter(Q(job_id=self.pk) & (Q(status=JobInvitationStatus.rejected.value)|Q(status=JobInvitationStatus.rejected_application.value)|Q(status=JobInvitationStatus.rejected_offer.value))).count()
        rejected_recommendation = JobRecommendation.objects.filter(Q(job_id=self.pk) & (Q(status=JobRecommendationStatus.rejected.value) | Q(status=JobRecommendationStatus.rejected_application.value) | Q(status=JobRecommendationStatus.rejected_offer.value))).count()
        return rejected_invitation + rejected_recommendation

    @property
    def get_negotiating_status_count(self):
        negotiating_invitation = JobInvitation.objects.filter(job_id=self.pk,status=JobInvitationStatus.negotiating.value).count()
        negotiating_recommendation = JobRecommendation.objects.filter(job_id=self.pk,status=JobRecommendationStatus.negotiating.value).count()
        return negotiating_invitation + negotiating_recommendation

    @property
    def get_shortlist_count(self):
        return self.shortlist.filter(application_id__in=self.applications.filter(status=9).values_list('pk')).count()

    @property
    def get_pending_status_count(self):
        pending_invitation = JobInvitation.objects.filter(job_id=self.pk,status=JobInvitationStatus.active.value).count()
        pending_recommendation = JobRecommendation.objects.filter(job_id=self.pk,status=JobRecommendationStatus.active.value).count()
        return pending_invitation + pending_recommendation

    @property
    def match_job_criteria(self,user=None):
        return True

    class Meta:
        db_table = 'job'

class JobApplicationArchive(GenericModel):
    application = models.ForeignKey(JobApplication)
    job = models.ForeignKey(Job)

    class Meta:
        db_table='job_application_archive'

class JobApplicationRejectReason(GenericModel):
    context = models.CharField(max_length=20) ###OFFER or APPLICATION
    reason = models.TextField(blank=True)
    created_by = models.ForeignKey(User, related_name="created_by")
    last_updated_by = models.ForeignKey(User, related_name="last_updated_by")

    class Meta:
        db_table = 'job_application_reject_reason'

class JobApplicationRejectFeedback(GenericModel):
    application = models.ForeignKey(JobApplication)
    reason = models.ForeignKey(JobApplicationRejectReason)
    created_by = models.ForeignKey(User)

    class Meta:
        db_table = 'job_application_reject_feedback'

class JobOfferRejectFeedback(GenericModel):
    offer = models.ForeignKey(JobOffer)
    reason = models.ForeignKey(JobApplicationRejectReason)
    created_by = models.ForeignKey(User)

    class Meta:
        db_table = 'job_offer_reject_feedback'


class JobInvitation(GenericModel):
    invited_by = models.ForeignKey(User,related_name="job_invited_by")
    job = models.ForeignKey(Job)
    user = models.ForeignKey(User)
    status = models.IntegerField()
    invite_message = models.TextField(blank=True)
    reject_message = models.TextField(blank=True)
    read = models.IntegerField(default=0)
    response_time = models.BigIntegerField(default=0)

    @property
    def render_status_verbal(self):
        if self.status == 1:
            return "Active"
        elif self.status == 3:
            return "Cancelled"
        elif self.status == 5:
            return "Rejected"
        elif self.status == 6:
            return "Negotiating"
        elif self.status == 7:
            return "Offered"
        elif self.status == 8:
            return "Hired"
        elif self.status == 9:
            return "Shortlisted"
        elif self.status == 10:
            return "Application Rejected"
        elif self.status == 12:
            return "Offer Rejected"
        elif self.status == 13:
            return "Application Withdrawn"

    @property
    def render_tutor(self):
        return "<a href='%s'>%s</a>" % (reverse("user_profile",kwargs={ "pk": self.user.pk }), self.user.champuser.fullname)

    @property
    def render_job_title(self):
        return self.job.title

    @property
    def render_invited_by(self):
        return "<a href='%s'>%s</a>" % (reverse("user_profile",kwargs={ "pk": self.invited_by.pk }), self.invited_by.champuser.fullname)

    @classmethod
    def table_columns(cls):
        return [ "id", "render_job_title", "render_invited_by", "render_tutor", "render_status_verbal:Invitation Status", "date_created:Invited On" ]

    class Meta:
        db_table = 'job_invitation'

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.response_time = Clock.utc_timestamp()
        super(GenericModel,self).save(force_insert, force_update, using,update_fields)

class JobRecommendation(GenericModel):
    job = models.ForeignKey(Job)
    user = models.ForeignKey(User)
    recommended_by = models.IntegerField() #0 means the system and 1 means a user. It stores user_id
    criteria_matched = models.FloatField()  ###In percentage
    status = models.IntegerField()  ###Either active,cancelled, applied
    read = models.IntegerField(default=0)
    reject_message = models.TextField(null=True, default='')

    @property
    def render_status_verbal(self):
        if self.status == 1:
            return "Active"
        elif self.status == 3:
            return "Cancelled"
        elif self.status == 5:
            return "Rejected"
        elif self.status == 6:
            return "Negotiating"
        elif self.status == 7:
            return "Offered"
        elif self.status == 8:
            return "Hired"
        elif self.status == 9:
            return "Shortlisted"
        elif self.status == 10:
            return "Application Rejected"
        elif self.status == 12:
            return "Offer Rejected"
        elif self.status == 13:
            return "Application Withdrawn"

    @property
    def render_tutor(self):
        return "<a href='%s'>%s</a>" % (reverse("user_profile",kwargs={ "pk": self.user.pk }), self.user.champuser.fullname)

    @property
    def render_job_title(self):
        return self.job.title

    @classmethod
    def table_columns(cls):
        return [ "id", "render_job_title", "render_tutor", "render_status_verbal:Status", "date_created:Invited On" ]

    class Meta:
        db_table = 'job_recommendation'

class WithdrawApplication(GenericModel):
    job = models.ForeignKey(Job)
    application = models.ForeignKey(JobApplication)
    reason = models.TextField(blank=True)

    class Meta:
        db_table = 'withdraw_application'

class QuestionAttachment(ThumbnailModelMixin, GenericModel):
    #lesson = models.ForeignKey(LessonRequest)
    attachment = models.FileField(upload_to='QAttachment/')
    attachment_name = models.CharField(max_length=500,null=True,blank=True)
    thumbnail = models.ImageField(upload_to='Msg_Attachment/',max_length=600,blank=True,null=True)

    # def get_attachment_thumbnail_link

    @property
    def render_thumbnail(self):
        if self.thumbnail:
            return self.thumbnail
        return ""

    @property
    def render_thumbnail_url(self):
        return settings.MEDIA_URL+str(self.thumbnail)

    def save(self):
        try:
            self.create_thumbnail()
        except Exception as msg:
            print(str(msg))
            print("Thumbnail creation failed.")
        super(QuestionAttachment, self).save()

    def save(self):
        try:
            self.create_thumbnail()
        except Exception as msg:
            print(str(msg))
            print("Thumbnail creation failed.")
        super(QuestionAttachment, self).save()

    @property
    def attachment_url(self):
        return settings.MEDIA_URL+str(self.attachment)

    class Meta:
        db_table = 'question_attachment'

class QCommentLike(GenericModel):
    value = models.IntegerField()  ###1 means liked, 2 means disliked.
    created_by = models.ForeignKey(User)

    class Meta:
        db_table = u'qcomment_like'

class QuestionComment(GenericModel):
    comment = models.TextField()
    created_by = models.ForeignKey(User)
    attachment = models.ForeignKey(QuestionAttachment,null=True)
    likes = models.ManyToManyField(QCommentLike)

    class Meta:
        db_table = u'question_comment'

class QAnswerLike(GenericModel):
    value = models.IntegerField()  ###1 means liked, 2 means disliked.
    created_by = models.ForeignKey(User)

    class Meta:
        db_table = u'qanswer_like'

class QuestionAnswer(GenericModel):
    answer = models.TextField()
    answer_by = models.ForeignKey(User)
    comments = models.ManyToManyField(QuestionComment)
    likes = models.ManyToManyField(QAnswerLike)
    attachment = models.ManyToManyField(QuestionAttachment)

    @property
    def render_answer_by(self):
        return "<a href='%s'>%s</a>" % (self.answer_by.champuser.profile_url, self.answer_by.champuser.fullname)

    @classmethod
    def table_columns(cls):
        return [ "id", "answer", "render_answer_by","date_created:Answered On" ]

    class Meta:
        db_table=u'question_answer'

class Question(GenericModel):
    created_by = models.ForeignKey(User)
    title = models.CharField(max_length=500)
    description = models.TextField()
    tags = models.ManyToManyField(Major)
    answers = models.ManyToManyField(QuestionAnswer)
    status = models.IntegerField(default=QuestionStatus.open.value)
    attached_file = models.ManyToManyField(QuestionAttachment,related_name="qattachment")
    read = models.IntegerField(default=0)

    def get_best_answer(self):
        answer = self.answers.filter(likes__value=1).annotate(num_likes=Count("likes")).order_by("-num_likes").first()
        return answer

    def render_created_by(self):
        return """
            <div><img src="%s" width="40" height="40"/><br/><a href="%s">%s</a></div>
        """ % ( self.created_by.champuser.render_profile_picture, reverse("user_profile", kwargs={ "pk": self.created_by.pk }), self.created_by.champuser.fullname )

    @property
    def render_description(self):
        return "<p>%s</p>" % self.description

    @classmethod
    def table_columns(cls):
        return [
            "render_created_by", "title:Question Title", "render_description", "date_created"
        ]

    @property
    def render_qtags(self):
        return ",".join([cu.value for cu in self.tags.all()])

    @property
    def details_config(self):
        d = OrderedDict()
        d["Question Title"] = "title"
        d["Posted By"] = "render_created_by"
        d["description"] = "render_description"
        d["Tags"] = "render_qtags"
        d["Posted On"] = "date_created"
        return d

    @property
    def tabs_config(self):
        return [
            TabView(
                title="Answers",
                url=reverse("admin_partial_details_view", kwargs={ "partial_parent_model_name": self.__class__.__name__.lower() ,"partial_parent": self.pk, "partial_model_name": "qanswer" })
            ),
            TabView(
                title="Answer By Me",
                url=reverse("admin_partial_details_view", kwargs={ "partial_parent_model_name": self.__class__.__name__.lower() ,"partial_parent": self.pk, "partial_model_name": "qanswer_me" })
            ),
            TabView(
                title="My Comments",
                url=reverse("admin_partial_details_view", kwargs={ "partial_parent_model_name": self.__class__.__name__.lower() ,"partial_parent": self.pk, "partial_model_name": "qcomment" })
            ),
        ]

    class Meta:
        db_table = 'question'

class ErrorLog(GenericModel):
    url = models.CharField(max_length=500)
    stacktrace = models.TextField()

    class Meta:
        db_table = 'error_log'

class SystemLog(GenericModel):
    url = models.CharField(max_length=500)
    user_id = models.IntegerField(null=True,blank=True)
    event_type = models.CharField(max_length=255,null=True,blank=True)
    message = models.TextField()

    class Meta:
        db_table = 'system_log'

class PaymentLog(GenericModel):
    created_by = models.ForeignKey(User,verbose_name="payment created by")
    url = models.CharField(max_length=500,null=True,blank=True)
    message = models.TextField()
    user_ip = models.CharField(max_length=255,null=True,blank=True)
    status = models.CharField(max_length=100) ###SUCCESS, FAILED
    lesson_id = models.BigIntegerField(null=True,blank=True)
    transaction = models.ForeignKey(Transaction,null=True)

    @property
    def render_url(self):
        return reverse("payment_log_details",kwargs={ "pk": self.pk })

    class Meta:
        db_table = 'payment_log'

class AuditLog(GenericModel):
    created_by = models.ForeignKey(User,null=True)
    context = models.CharField(max_length=255,null=True,blank=True)
    message = models.TextField()

    class Meta:
        db_table = 'audit_log'


class EmailTemplate(GenericModel):
    context_options = (
        ("SIGNUP_EMAIL","SIGN UP EMAIL"),
        ("JOB_INVITATION","JOB INVITATION EMAIL"),
        ("JOB_OFFER","JOB OFFER EMAIL"),
        ("JOB_RECOMMENDATION","JOB RECOMMENDATION EMAIL")
    )
    context = models.CharField(max_length=200)
    html_body = models.TextField(null=True,blank=True,default='')
    text_body = models.TextField(null=True,blank=True,default='')
    created_by = models.ForeignKey(User, related_name='created_by_user')
    modified_by = models.ForeignKey(User, related_name='last_modified_by')

    class Meta:
        db_table = u'email_template'

class CurrencyRate(GenericModel):
    base_currency = models.ForeignKey(Currency,related_name="base_currency")
    target_currency = models.ForeignKey(Currency,related_name="target_currency")
    value = models.FloatField(default=1)

    class Meta:
        db_table = 'currency_rate'

class TempAttachment(GenericModel, ThumbnailModelMixin):
    #lesson = models.ForeignKey(LessonRequest)
    attachment = models.FileField(upload_to='TEMP_DIR/')
    attachment_name = models.CharField(max_length=500,null=True,blank=True)
    thumbnail = models.ImageField(upload_to='TEMP_DIR/',max_length=600,blank=True,null=True)


    def save(self):
        try:
            self.create_thumbnail()
        except Exception as msg:
            print(str(msg))
            print("Thumbnail creation failed.")
        super(TempAttachment, self).save()

    @property
    def attachment_url(self):
        return "/media/"+str(self.attachment)

    class Meta:
        db_table = 'temp_attachment'

class GlobalSettings(GenericModel):
    home_page_video = models.CharField(max_length=500,null=True,blank=True)
    intro_video = models.CharField(max_length=500,null=True,blank=True)

    class Meta:
        db_table = 'global_settings'

class SupportedCountry(LocationEntity):
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.type = SupportedCountry.__name__
        super(SupportedCountry,self).save(force_insert,force_update,using,update_fields)

    class Meta:
        proxy = True

class SupportedLanguage(GenericModel):
    name = models.CharField(max_length=200)
    code = models.CharField(null=True,blank=True,max_length=10)
    #proficiency = models.IntegerField(default=0,null=True,blank=True) ###Max is 5

    class Meta:
        db_table = 'supported_language'

class SecurityQuestion(GenericModel):
    question = models.CharField(max_length=500)
    created_by = models.ForeignKey(User)

    class Meta:
        db_table = "security_question"


class UserSecurityQuestion(GenericModel):
    user = models.ForeignKey(User)
    question = models.ForeignKey(SecurityQuestion)
    answer = models.CharField(max_length=500)

    class Meta:
        db_table = "user_security_question"


class EmailRecoveryRequest(GenericModel):
    gender = models.CharField(max_length=20, blank=True)
    phone = models.CharField(max_length=40, blank=True)
    birthdate = models.CharField(max_length=40, blank=True)
    account_opening_date = models.CharField(max_length=40, blank=True)
    question1 = models.ForeignKey(SecurityQuestion)
    answer1 = models.CharField(max_length=500, blank=True)
    question2 = models.ForeignKey(SecurityQuestion, related_name="security_question2")
    answer2 = models.CharField(max_length=500, blank=True)
    nationality = models.CharField(max_length=200, blank=True)
    nric = models.CharField(max_length=100, blank=True)
    surname = models.CharField(max_length=50, blank=True)
    last_login_date = models.CharField(max_length=50, blank=True)
    password1 = models.CharField(max_length=200, blank=True)
    password2 = models.CharField(max_length=200, blank=True)

    class Meta:
        db_table = "email_recovery_request"

class UserSearchHistory(GenericModel):
    keyword = models.CharField(max_length=200,blank=True)
    user = models.ForeignKey(User)

    class Meta:
        db_table = "user_search_history"

class ContactUs(GenericModel):
    inquiry_type = models.IntegerField(default=0) #0=Technical Issue, 1=General Inquiry
    first_name = models.CharField(max_length=300)
    email = models.CharField(max_length=200, null=True)
    contact_no = models.CharField(max_length=200, null=True)
    subject = models.CharField(max_length=500, null=True)
    feedback = models.TextField(default='')

    class Meta:
        db_table = 'contact_us'



