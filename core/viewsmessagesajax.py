from django.shortcuts import render
from core.library.block_util import BlockListUtil
from notification.models import Notification

__author__ = 'codengine'

import uuid
from django.core.files.base import File
from django.http.response import HttpResponse, HttpResponseRedirect
from core.library.Clock import Clock
from core.templatetags.datefilters import render_short_datestring_from_timestamp
from django.views.generic import View
from core.enums import NotificationClass
from core.generics.ajaxmixins import JSONResponseMixin
from django.db import transaction
import os
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from core.generics.redis_client import RedisClient
from common.channels import CHANNEL
from django.template import loader, Context
from core.models3 import *
from django.utils import timezone
import time
import json

class MessageAjaxView(JSONResponseMixin,View):

    # @method_decorator(csrf_exempt)
    # def dispatch(self, *args, **kwargs):
    #     return super(MessageAjaxView, self).dispatch(*args, **kwargs)

    def post(self,request,*args,**kwargs):
        response = {
            "status": "FAILURE",
            "message": "FAILURE",
            "data": []
        }
        recipients = request.POST.get("hidden_ids")
        msg_txt = request.POST.get("msg_text")
        #raw_data = request.raw_post_data
        attachment = request.FILES.get("msg_attachment")
        if recipients and msg_txt:
            with transaction.atomic():
                for recipient in recipients.split(','):
                    try:
                        recipient = int(recipient)
                        msg = Message()
                        msg.chat_type = ChatType.p2p.value
                        msg.is_read = ReadStatus.unread.value
                        msg.msg_date = Clock.utc_timestamp()
                        msg.msg = msg_txt
                        # msg.create_date = datetime.now()
                        # msg.modified_date = datetime.now()
                        msg.save()

                        user_msg = UserMessage()
                        user_msg.sender = request.user
                        user_msg.receiver = User.objects.get(pk=recipient)
                        user_msg.message = msg
                        user_msg.save()

                        if attachment:
                            file_name = str(request.user.id)+"."+attachment._name[attachment._name.rindex(".")+1:]
                            print file_name
                            with open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name), 'wb+') as destination:
                                for chunk in file.chunks():
                                    destination.write(chunk)
                            attached_file = File(open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name)))

                            msg_attachment = MessageAttachment()
                            msg_attachment.message = msg
                            msg_attachment.attachment = attached_file
                            msg_attachment.save()

                        # notification_object = Notification()
                        # notification_object.notif_class = NotificationClass.MESSAGE.value
                        # notification_object.created_by = request.user
                        # notification_object.read = ReadStatus.unread.value
                        # notification_object.target_user_id = recipient
                        # notification_object.text = "Unread message."
                        # notification_object.url = "s"
                        # notification_object.save()

                    except Exception,msg:
                        print "Exception occured."
                        print msg
        else:
            response["status"] = "FAILURE"
            response["message"] = "Recipients or message missing."
            return self.render_to_json(response)



        response["status"] = "SUCCESS"
        response["message"] = "Successful"
        return self.render_to_json(response)

class ChatboxMessageAjaxView(JSONResponseMixin, View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ChatboxMessageAjaxView, self).dispatch(*args, **kwargs)

    def get(self,request,*args,**kwargs):
        response = {
            "status": "FAILURE",
            "message": "FAILURE",
            "data": []
        }
        if request.is_ajax():
            user_id = request.GET.get("user_id")
            since_pk = request.GET.get('since_pk')
            chat_type = request.GET.get("chat_type")
            chat_id = request.GET.get("chat_id")
            max_since_ts = Clock.utc_timestamp()
            user_left = False
            try:
                user_id = int(user_id)
                since_pk = int(since_pk)
                chat_type = int(chat_type)

                if not request.user.is_authenticated():
                    response["status"] = "FAILURE"
                    response["message"] = "LOGIN_REQUIRED"
                    return self.render_to_json(response)

                if chat_type == 0:
                    if BlockListUtil.check_if_user_blocked(request.user.pk, int(user_id)):
                        response["status"] = "FAILURE"
                        response["message"] = "USER_BLOCKED"
                        return self.render_to_json(response)
                elif chat_type ==1:
                    conversation_object = Conversation.objects.filter(conversation_id=chat_id)
                    if conversation_object.exists():
                        conversation_object = conversation_object.first()
                        if conversation_object.users_left.filter(pk=request.user.pk).exists():
                            since_dates = conversation_object.since_dates.filter(user_id=request.user.pk)
                            if since_dates.exists():
                                max_since_ts = since_dates.first().since_ts
                            user_left = True
                            # response["status"] = "FAILURE"
                            # response["message"] = "USER_LEFT"
                            # return self.render_to_json(response)

            except Exception as msg:
                # print msg
                response["status"] = "FAILURE"
                response["message"] = "Invalid user id."
                return self.render_to_json(response)

            if chat_type == 0: ###p2p chat.
                if not since_pk or since_pk == -1:
                    # user_messages = UserMessage.objects.filter((Q(sender_id=request.user.pk) & Q(receiver_id=user_id)) | (Q(sender_id=user_id) & Q(receiver_id=request.user.pk))).order_by('-date_created')[:10]
                    messages = Message.objects.filter(usermessage__in=UserMessage.objects.filter((Q(sender_id=request.user.pk) & Q(receiver_id=user_id)) | (Q(sender_id=user_id)) & Q(receiver_id=request.user.pk))).order_by("-date_created")[:10]
                else:
                    # user_messages = UserMessage.objects.filter(((Q(sender_id=request.user.pk) & Q(receiver_id=user_id)) | (Q(sender_id=user_id) & Q(receiver_id=request.user.pk))) & Q(date_created__lte=since_date)).order_by('-date_created')[:10]
                    messages = Message.objects.filter(usermessage__in=UserMessage.objects.filter((Q(sender_id=request.user.pk) & Q(receiver_id=user_id)) | (Q(sender_id=user_id)) & Q(receiver_id=request.user.pk))).filter(pk__lt=since_pk).order_by("-date_created")[:10]
            elif chat_type == 1: ###Group chat
                if not since_pk or since_pk == -1:
                    # user_messages = UserMessage.objects.filter((Q(sender_id=request.user.pk) & Q(receiver_id=user_id)) | (Q(sender_id=user_id) & Q(receiver_id=request.user.pk))).order_by('-date_created')[:10]
                    messages = Message.objects.filter(usermessage__in=UserMessage.objects.filter(conversation__conversation_id=chat_id))
                else:
                    # user_messages = UserMessage.objects.filter(((Q(sender_id=request.user.pk) & Q(receiver_id=user_id)) | (Q(sender_id=user_id) & Q(receiver_id=request.user.pk))) & Q(date_created__lte=since_date)).order_by('-date_created')[:10]
                    messages = Message.objects.filter(usermessage__in=UserMessage.objects.filter(conversation__conversation_id=chat_id)).filter(pk__lt=since_pk)

                messages = messages.filter(usermessage__conversation__conversation_id=chat_id,date_created__lte=max_since_ts).order_by("-date_created")[:10]

            message_rows = []
            for message in messages:
                context = {
                    "message": message,
                    "request": request,
                    "sender_me": message.usermessage_set.first().sender_id == request.user.pk
                }
                template_name = "ajax/chatbox_chat_row.html"
                template = loader.get_template(template_name)
                cntxt = Context(context)
                rendered = template.render(cntxt)
                message_rows += [ rendered ]
            response = {
                "status": "SUCCESS",
                "message": "Successful",
                "data": {
                    "hide_textbox": 1 if user_left else 0,
                    "rows": message_rows
                }
            }
            return self.render_to_json(response)

class FetchWbMessages(JSONResponseMixin, View):
    def get(self,request,*args,**kwargs):
        wb_id = int(self.kwargs.get('whiteboard_id'))
        start = request.GET.get('start')
        try:
            wb_id = int(wb_id)
            start = int(start)
        except:
            start = -1
        wb_objects = Whiteboard.objects.filter(pk=wb_id)
        if wb_objects.exists():
            whiteboard = wb_objects.first()
            wb_messages = whiteboard.tutor_chat.all()
            if start != -1:
                wb_messages = wb_messages.filter(pk__lt=start)
            wb_messages = wb_messages.order_by('-id')[:20]
            chat_messages = []
            for wb_tutor_chat in wb_messages:
                contxt_data = {
                    "tutor_chat": wb_tutor_chat,
                    "request": request,
                    "STATIC_URL": settings.STATIC_URL+"/"
                }
                template_name = "ajax/wb_tutor_chat_row.html"
                template = loader.get_template(template_name)
                cntxt = Context(contxt_data)
                message_entry = template.render(cntxt)
                chat_messages += [ message_entry ]
            response = {
                "status": "SUCCESS",
                "message": "Successful",
                "data": {
                    "wb_messages": chat_messages
                }
            }
            return self.render_to_json(response)
        response = {
            "status": "FAILURE",
            "message": "Failed",
            "data": {
                "wb_messages": []
            }
        }
        return self.render_to_json(response)


def get_media_url(user):
    if user.champuser.profile_picture:
        return settings.MEDIA_URL+'image/'+str(user.champuser.profile_picture.image_field)
    return ""

class SendMessageAjaxView(JSONResponseMixin,View):

    def get_unread_message_count(self, user_id):
        query = "select * from user_message where sender_id=%s or receiver_id=%s group by sender_id,receiver_id order by date_created DESC;" % (user_id, user_id)
        user_messages = UserMessage.objects.raw(query) #.filter(Q(sender=self.request.user) | Q(receiver=self.request.user)).group_by('sender_id','receiver_id').order_by("-date_created")
        umessage_ids = []
        uids = []
        for umessage in user_messages:
            if umessage.sender_id != user_id:
                if not umessage.sender_id in uids:
                    # umessage_ids += [ umessage.pk ]
                    uids += [ umessage.sender_id ]
            else:
                if not umessage.receiver_id in uids:
                    # umessage_ids += [ umessage.pk ]
                    uids += [ umessage.receiver_id ]

        for uid in uids:
            umessage = UserMessage.objects.filter((Q(sender_id=user_id) & Q(receiver_id=uid) & Q(message__is_read=0)) | (Q(sender_id=uid) & Q(receiver_id=user_id) & Q(message__is_read=0))).order_by('-date_created')
            if umessage.exists():
                umessage = umessage.first()
                umessage_ids += [ umessage.pk ]

        return len(umessage_ids)


    def post(self,request,*args,**kwargs):
        if request.is_ajax() and request.user.is_authenticated():
            receivers = request.POST.get("receivers")
            msg_text = request.POST.get("message")
            attachment = request.FILES.get("file")
            chat_id = request.POST.get("chat_id")
            chat_type = request.POST.get("chat_type")
            receiver_list = []
            remote_peers = []
            try:
                chat_type = int(chat_type)
                for receiver in receivers.split(','):
                    if receiver:
                        receiver_id = int(receiver)
                        receiver_list += [ User.objects.get(pk=receiver_id) ]
                        remote_peers += [ receiver_id ]
            except Exception,msg:
                response = {
                    "status": "FAILURE",
                    "message": "Invalid recipient.",
                    "data": []
                }
                return self.render_to_json(response)

            if not receiver_list:
                response = {
                    "status": "FAILURE",
                    "message": "Recipient list empty",
                    "data": []
                }
                return self.render_to_json(response)

            sender = request.user
            msg = Message()
            if msg_text:
                msg.msg = msg_text
            else:
                msg.msg = ""
            if chat_type == 0: ###p2p chat.
                msg.chat_type = 0 ###p2p chat
            elif chat_type == 1: ###Group chat.
                msg.chat_type = 1 ###group chat
            msg.is_read = 0 #unread
            msg.save()

            attachment_name = ""
            attatchment_url = ""
            thumbnail_url = ""

            if attachment:
                file_name = str(request.user.id)+"-"+str(uuid.uuid4())+"."+attachment._name[attachment._name.rindex(".")+1:]
                print file_name
                with open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name), 'wb+') as destination:
                    for chunk in attachment.chunks():
                        destination.write(chunk)
                attached_file = File(open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name)))

                msg_attachment = MessageAttachment()
                msg_attachment.message = msg
                msg_attachment.attachment_name = attachment._name
                msg_attachment.attachment = attached_file
                msg_attachment.save()
                attatchment_url = settings.MEDIA_URL+str(msg_attachment.attachment)
                attachment_name = attachment._name
                thumbnail_url = ""
                if msg_attachment.thumbnail:
                    thumbnail_url = settings.MEDIA_URL+str(msg_attachment.thumbnail)

            if chat_type == 0:
                user_msg = UserMessage()
                user_msg.sender_id = sender.pk
                user_msg.receiver_id = receiver_list[0].pk
                if chat_type == 1:
                    for r in receiver_list:
                        if r.pk != user_msg.receiver_id:
                            user_msg.other_users.add(r)
                user_msg.message_id = msg.pk
                user_msg.chat_type = 0 ###p2p chat.
                user_msg.save()
            elif chat_type == 1:

                user_conv = Conversation.objects.filter(conversation_id=chat_id)
                if user_conv.exists():
                    conversation = user_conv.first()

                    temp_receiver_list = []
                    conv_users = conversation.other_users.values_list('pk', flat=True)
                    for rcv in receiver_list:
                        if rcv in conv_users:
                            temp_receiver_list += [ rcv ]

                    receiver_list = temp_receiver_list

                else:
                    conversation = Conversation()
                    conversation.conversation_id = chat_id
                    conversation.save()
                    for rcv in receiver_list:
                        conversation.other_users.add(rcv)
                user_msg = UserMessage()
                user_msg.sender_id = sender.pk
                user_msg.chat_type = 1 ###group chat.
                user_msg.conversation_id = conversation.pk
                user_msg.message_id = msg.pk
                user_msg.save()

            added_users = []
            content = {}
            if chat_type == 0: ###p2p chat

                context = {
                    "message": msg,
                    "sender_me": True,
                    "receiver_name": receiver_list[0].champuser.fullname
                }
                template_name = "ajax/message_detail_row.html"
                template = loader.get_template(template_name)
                cntxt = Context(context)
                rendered_detail_row = template.render(cntxt)

                template_name = "ajax/chatbox_chat_row.html"
                template = loader.get_template(template_name)
                cntxt = Context(context)
                rendered_chat_row = template.render(cntxt)

                contxt_data = {
                    "user_message": user_msg,
                    "request": request,
                    "STATIC_URL": settings.STATIC_URL+"/"
                }
                template_name = "ajax/message_list_entry.html"
                template = loader.get_template(template_name)
                cntxt = Context(contxt_data)
                rendered_message_list_entry = template.render(cntxt)

                unread_message_count = self.get_unread_message_count(request.user.pk)

                content[request.user.pk] = {
                    "id": receiver_list[0].pk,
                    "object_id": user_msg.pk,
                    "chat_type": 0,
                    "title": receiver_list[0].champuser.fullname,
                    "chat_popup": rendered_chat_row,
                    "msg_detail_entry": rendered_detail_row,
                    "message_list_entry": rendered_message_list_entry,
                    "unread_message_count": unread_message_count,
                    "status": "SUCCESS"
                }

                context = {
                    "message": msg,
                    "sender_me": False,
                    "receiver_name": request.user.champuser.fullname
                }
                template_name = "ajax/message_detail_row.html"
                template = loader.get_template(template_name)
                cntxt = Context(context)
                rendered_detail_row = template.render(cntxt)

                template_name = "ajax/chatbox_chat_row.html"
                template = loader.get_template(template_name)
                cntxt = Context(context)
                rendered_chat_row = template.render(cntxt)

                contxt_data = {
                    "user_message": user_msg,
                    "request": request,
                    "STATIC_URL": settings.STATIC_URL+"/"
                }
                template_name = "ajax/message_list_entry.html"
                template = loader.get_template(template_name)
                cntxt = Context(contxt_data)
                rendered_message_list_entry = template.render(cntxt)

                unread_message_count = self.get_unread_message_count(receiver_list[0].pk)

                content[receiver_list[0].pk] = {
                    "id": request.user.pk,
                    "object_id": user_msg.pk,
                    "chat_type": 0,
                    "title": request.user.champuser.fullname,
                    "chat_popup": rendered_chat_row,
                    "msg_detail_entry": rendered_detail_row,
                    "message_list_entry": rendered_message_list_entry,
                    "unread_message_count": unread_message_count,
                    "status": "SUCCESS"
                }

                added_users = [ request.user.pk, receiver_list[0].pk ]

            else:
                all_users = [ request.user ] + receiver_list

                for euser in all_users:
                    if not euser.pk in added_users:
                        context = {
                            "message": msg,
                            "sender_me": euser.pk == request.user.pk
                        }
                        template_name = "ajax/message_detail_row.html"
                        template = loader.get_template(template_name)
                        cntxt = Context(context)
                        rendered_detail_row = template.render(cntxt)

                        template_name = "ajax/chatbox_chat_row.html"
                        template = loader.get_template(template_name)
                        cntxt = Context(context)
                        rendered_chat_row = template.render(cntxt)

                        contxt_data = {
                            "user_message": user_msg,
                            "request": request,
                            "STATIC_URL": settings.STATIC_URL+"/"
                        }
                        template_name = "ajax/message_list_entry.html"
                        template = loader.get_template(template_name)
                        cntxt = Context(contxt_data)
                        rendered_message_list_entry = template.render(cntxt)

                        unread_message_count = self.get_unread_message_count(euser.pk)

                        content[euser.pk] = {
                            "id": chat_id,
                            "object_id": user_msg.pk,
                            "chat_type": 1,
                            "title": conversation.title if conversation.title else "Group Chat",
                            "chat_popup": rendered_chat_row,
                            "msg_detail_entry": rendered_detail_row,
                            "message_list_entry": rendered_message_list_entry,
                            "unread_message_count": unread_message_count,
                            "remote_peers": remote_peers + ([ request.user.pk ] if not request.user.pk in remote_peers else []),
                            "status": "SUCCESS"
                        }
                        added_users += [ euser.pk ]

            ###Now publish the status with data in MESSAGE_SENT signal.
            try:
                redis_client = RedisClient.Instance()
                publish_message = {
                    "content": content,
                    "receivers": added_users
                }
                publish_message["CHANNEL"] = CHANNEL["send_chat_message"]
                redis_client.publish_channel(CHANNEL["send_chat_message"], json.dumps(publish_message))
            except Exception as msg:
                pass

            response = {
                "status": "SUCCESS",
                "message": "Successful",
                "data": ""
            }
            return self.render_to_json(response)

class SendChatMessageAjaxView(JSONResponseMixin, View):

    def get_unread_message_count(self, user_id):
        query = "select * from user_message where sender_id=%s or receiver_id=%s group by sender_id,receiver_id order by date_created DESC;" % (user_id, user_id)
        user_messages = UserMessage.objects.raw(query) #.filter(Q(sender=self.request.user) | Q(receiver=self.request.user)).group_by('sender_id','receiver_id').order_by("-date_created")
        umessage_ids = []
        uids = []
        for umessage in user_messages:
            if umessage.sender_id != user_id:
                if not umessage.sender_id in uids:
                    # umessage_ids += [ umessage.pk ]
                    uids += [ umessage.sender_id ]
            else:
                if not umessage.receiver_id in uids:
                    # umessage_ids += [ umessage.pk ]
                    uids += [ umessage.receiver_id ]

        for uid in uids:
            umessage = UserMessage.objects.filter((Q(sender_id=user_id) & Q(receiver_id=uid) & Q(message__is_read=0)) | (Q(sender_id=uid) & Q(receiver_id=user_id) & Q(message__is_read=0))).order_by('-date_created')
            if umessage.exists():
                umessage = umessage.first()
                umessage_ids += [ umessage.pk ]

        return len(umessage_ids)

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(SendChatMessageAjaxView, self).dispatch(*args, **kwargs)

    def post(self,request, *args, **kwargs):
        if request.is_ajax() and request.user.is_authenticated():
            receivers = request.POST.get("receivers")
            msg_text = request.POST.get("message")
            attachment = request.FILES.get("file")
            chat_id = request.POST.get("chat_id")
            chat_type = request.POST.get("chat_type")
            receiver_list = []
            remote_peers = []
            try:
                chat_type = int(chat_type)
                for receiver in receivers.split(','):
                    if receiver:
                        receiver_id = int(receiver)
                        receiver_list += [ User.objects.get(pk=receiver_id) ]
                        remote_peers += [ receiver_id ]
                if not request.user.pk in remote_peers:
                    remote_peers += [ request.user.pk ]

                if not request.user in receiver_list:
                    receiver_list += [ request.user ]

            except Exception as msg:
                response = {
                    "status": "FAILURE",
                    "message": "Invalid recipient.",
                    "data": []
                }
                return self.render_to_json(response)

            if not receiver_list:
                response = {
                    "status": "FAILURE",
                    "message": "Recipient list empty",
                    "data": []
                }
                return self.render_to_json(response)

            if chat_type == 0:
                if BlockListUtil.check_if_user_blocked(remote_peers[0], remote_peers[1]):
                    response = {
                        "status": "FAILURE",
                        "message": "User Blocked",
                        "data": []
                    }
                    return self.render_to_json(response)
            elif chat_type == 1:
                if BlockListUtil.check_if_user_left_this_conversation(chat_id, request.user.pk):
                    response = {
                        "status": "FAILURE",
                        "message": "Not Authorized to send",
                        "data": []
                    }
                    return self.render_to_json(response)

            sender = request.user
            msg = Message()
            if msg_text:
                msg.msg = msg_text
            else:
                msg.msg = ""
            if chat_type == 0: ###p2p chat.
                msg.chat_type = 0 ###p2p chat
            elif chat_type == 1: ###Group chat.
                msg.chat_type = 1 ###group chat
            msg.is_read = 0 #unread
            msg.save()

            attachment_name = ""
            attatchment_url = ""
            thumbnail_url = ""

            if attachment:
                file_name = str(request.user.id)+"-"+str(uuid.uuid4())+"."+attachment._name[attachment._name.rindex(".")+1:]
                # print file_name
                with open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name), 'wb+') as destination:
                    for chunk in attachment.chunks():
                        destination.write(chunk)
                attached_file = File(open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name)))

                msg_attachment = MessageAttachment()
                msg_attachment.message = msg
                msg_attachment.attachment_name = attachment._name
                msg_attachment.attachment = attached_file
                msg_attachment.save()
                attatchment_url = settings.MEDIA_URL+str(msg_attachment.attachment)
                attachment_name = attachment._name
                thumbnail_url = ""
                if msg_attachment.thumbnail:
                    thumbnail_url = settings.MEDIA_URL+str(msg_attachment.thumbnail)

            if chat_type == 0:
                user_msg = UserMessage()
                user_msg.sender_id = sender.pk
                user_msg.receiver_id = receiver_list[0].pk
                if chat_type == 1:
                    for r in receiver_list:
                        if r.pk != user_msg.receiver_id:
                            user_msg.other_users.add(r)
                user_msg.message_id = msg.pk
                user_msg.chat_type = 0 ###p2p chat.
                user_msg.save()
            elif chat_type == 1:

                user_conv = Conversation.objects.filter(conversation_id=chat_id)
                if user_conv.exists():
                    conversation = user_conv.first()

                    temp_receiver_list = []
                    temp_receiver_ids = []
                    conv_users = [ int(cu) for cu in conversation.other_users.values_list('pk', flat=True) ]
                    for rcv in receiver_list:
                        if rcv.pk in conv_users:
                            temp_receiver_list += [ rcv ]
                            temp_receiver_ids += [ rcv.pk ]

                    for cu in conv_users:
                        if not cu in temp_receiver_ids:
                            temp_receiver_list += [ User.objects.get(pk=cu) ]


                    receiver_list = temp_receiver_list

                else:
                    conversation = Conversation()
                    conversation.conversation_id = chat_id
                    conversation.save()
                    for rcv in receiver_list:
                        conversation.other_users.add(rcv)
                user_msg = UserMessage()
                user_msg.sender_id = sender.pk
                user_msg.chat_type = 1 ###group chat.
                user_msg.conversation_id = conversation.pk
                user_msg.message_id = msg.pk
                user_msg.save()

            added_users = []
            content = {}
            if chat_type == 0: ###p2p chat

                user1,user2 = receiver_list[0],receiver_list[1]
                context = {
                    "message": msg,
                    "sender_me": request.user.pk == user1.pk,
                    "receiver_name": user2.champuser.fullname
                }
                template_name = "ajax/message_detail_row.html"
                template = loader.get_template(template_name)
                cntxt = Context(context)
                rendered_detail_row = template.render(cntxt)

                template_name = "ajax/chatbox_chat_row.html"
                template = loader.get_template(template_name)
                cntxt = Context(context)
                rendered_chat_row = template.render(cntxt)

                contxt_data = {
                    "user_message": user_msg,
                    "request": request,
                    "STATIC_URL": settings.STATIC_URL+"/",
                    "other_user": user2
                }
                template_name = "ajax/message_list_entry.html"
                template = loader.get_template(template_name)
                cntxt = Context(contxt_data)
                rendered_message_list_entry = template.render(cntxt)

                # unread_message_count = self.get_unread_message_count(request.user.pk) #UserMessage.objects.filter(chat_type=0, receiver_id=request.user.pk,message__is_read=0).count()

                content[user1.pk] = {
                    "id": user2.pk,
                    "chat_type": 0,
                    "title": user2.champuser.fullname,
                    "chat_popup": rendered_chat_row,
                    "msg_detail_entry": rendered_detail_row,
                    # "unread_message_count": unread_message_count,
                    "message_list_entry": rendered_message_list_entry,
                    "status": "SUCCESS"
                }

                context = {
                    "message": msg,
                    "sender_me": request.user.pk == user2.pk,
                    "receiver_name": user1.champuser.fullname
                }
                template_name = "ajax/message_detail_row.html"
                template = loader.get_template(template_name)
                cntxt = Context(context)
                rendered_detail_row = template.render(cntxt)

                template_name = "ajax/chatbox_chat_row.html"
                template = loader.get_template(template_name)
                cntxt = Context(context)
                rendered_chat_row = template.render(cntxt)

                contxt_data = {
                    "user_message": user_msg,
                    "request": request,
                    "STATIC_URL": settings.STATIC_URL+"/",
                    "other_user": user1
                }
                template_name = "ajax/message_list_entry.html"
                template = loader.get_template(template_name)
                cntxt = Context(contxt_data)
                rendered_message_list_entry = template.render(cntxt)

                # unread_message_count = self.get_unread_message_count(receiver_list[0].pk)

                content[user2.pk] = {
                    "id": user1.pk,
                    "chat_type": 0,
                    "title": user1.champuser.fullname,
                    "chat_popup": rendered_chat_row,
                    "msg_detail_entry": rendered_detail_row,
                    # "unread_message_count": unread_message_count,
                    "message_list_entry": rendered_message_list_entry,
                    "status": "SUCCESS"
                }

                added_users = [ user1.pk, user2.pk ]

            elif chat_type == 1:
                all_users = [ request.user ] + receiver_list

                for euser in all_users:
                    user_blocked = BlockListUtil.check_if_user_blocked(request.user.pk, euser.pk) if euser.pk != request.user.pk else False
                    user_left_this_conversation = BlockListUtil.check_if_user_left_this_conversation(chat_id, euser.pk)
                    if euser.pk in added_users or user_blocked or user_left_this_conversation:
                        continue
                    context = {
                        "message": msg,
                        "sender_me": euser.pk == request.user.pk
                    }
                    template_name = "ajax/message_detail_row.html"
                    template = loader.get_template(template_name)
                    cntxt = Context(context)
                    rendered_detail_row = template.render(cntxt)

                    template_name = "ajax/chatbox_chat_row.html"
                    template = loader.get_template(template_name)
                    cntxt = Context(context)
                    rendered_chat_row = template.render(cntxt)

                    contxt_data = {
                        "user_message": user_msg,
                        "request": request,
                        "STATIC_URL": settings.STATIC_URL+"/"
                    }
                    template_name = "ajax/message_list_entry.html"
                    template = loader.get_template(template_name)
                    cntxt = Context(contxt_data)
                    rendered_message_list_entry = template.render(cntxt)

                    # unread_message_count = self.get_unread_message_count(euser.pk)

                    content[euser.pk] = {
                        "id": chat_id,
                        "chat_type": 1,
                        "title": conversation.title if conversation.title else "Group Chat",
                        "chat_popup": rendered_chat_row,
                        "msg_detail_entry": rendered_detail_row,
                        "message_list_entry": rendered_message_list_entry,
                        # "unread_message_count": unread_message_count,
                        "remote_peers": remote_peers + ([ euser.pk ] if not euser.pk in remote_peers else []),
                        "status": "SUCCESS"
                    }
                    added_users += [ euser.pk ]

                for euser in added_users:
                    content[euser]["remote_peers"] = added_users

            # Now publish the status with data in MESSAGE_SENT signal.
            try:
                redis_client = RedisClient.Instance()
                if request.POST.get('dont_send_back_me') == "1":
                    added_users = [ i for i in added_users if i != request.user.pk ]

                publish_message = {
                    "content": content,
                    "receivers": added_users
                }
                publish_message["CHANNEL"] = CHANNEL["send_chat_message"]
                redis_client.publish_channel(CHANNEL["send_chat_message"], json.dumps(publish_message))
            except Exception as msg:
                pass

            response = {
                "status": "SUCCESS",
                "message": "Successful",
                "data": ""
            }
            return self.render_to_json(response)
        else:
            response = {
                "status": "FAILURE",
                "message": "Unauthorized or invalid access",
                "data": ""
            }
            return self.render_to_json(response)


class SendBatchMessagesAjaxView(JSONResponseMixin, View):

    def get_unread_message_count(self, user_id):
        query = "select * from user_message where sender_id=%s or receiver_id=%s group by sender_id,receiver_id order by date_created DESC;" % (user_id, user_id)
        user_messages = UserMessage.objects.raw(query) #.filter(Q(sender=self.request.user) | Q(receiver=self.request.user)).group_by('sender_id','receiver_id').order_by("-date_created")
        umessage_ids = []
        uids = []
        for umessage in user_messages:
            if umessage.sender_id != user_id:
                if not umessage.sender_id in uids:
                    # umessage_ids += [ umessage.pk ]
                    uids += [ umessage.sender_id ]
            else:
                if not umessage.receiver_id in uids:
                    # umessage_ids += [ umessage.pk ]
                    uids += [ umessage.receiver_id ]

        for uid in uids:
            umessage = UserMessage.objects.filter((Q(sender_id=user_id) & Q(receiver_id=uid) & Q(message__is_read=0)) | (Q(sender_id=uid) & Q(receiver_id=user_id) & Q(message__is_read=0))).order_by('-date_created')
            if umessage.exists():
                umessage = umessage.first()
                umessage_ids += [ umessage.pk ]

        return len(umessage_ids)

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(SendBatchMessagesAjaxView, self).dispatch(*args, **kwargs)

    def post(self,request, *args, **kwargs):
        if request.is_ajax() and request.user.is_authenticated():
            receivers = request.POST.get("receivers")
            msg_text = request.POST.get("message")
            attachment = request.FILES.get("file")
            receiver_list = []
            remote_peers = []
            try:
                for receiver in receivers.split(','):
                    if receiver:
                        receiver_id = int(receiver)
                        if not BlockListUtil.check_if_user_blocked(request.user.pk, receiver_id):
                            receiver_list += [ User.objects.get(pk=receiver_id) ]
                            remote_peers += [ receiver_id ]
                if not request.user.pk in remote_peers:
                    remote_peers += [ request.user.pk ]

                if not request.user in receiver_list:
                    receiver_list += [ request.user ]

                remote_peers = [ rp for rp in set(remote_peers) ]

            except Exception as msg:
                response = {
                    "status": "FAILURE",
                    "message": "Invalid recipient.",
                    "data": []
                }
                return self.render_to_json(response)

            if not receiver_list:
                response = {
                    "status": "FAILURE",
                    "message": "Recipient list empty",
                    "data": []
                }
                return self.render_to_json(response)

            sender = request.user
            content = {}
            for recevr in receiver_list:
                if recevr.pk == request.user.pk:
                    continue
                msg = Message()
                if msg_text:
                    msg.msg = msg_text
                else:
                    msg.msg = ""
                msg.chat_type = 0 ###p2p chat
                msg.is_read = 0 #unread
                msg.save()

                attachment_name = ""
                attatchment_url = ""
                thumbnail_url = ""

                if attachment:
                    file_name = str(request.user.id)+"-"+str(uuid.uuid4())+"."+attachment._name[attachment._name.rindex(".")+1:]
                    # print file_name
                    with open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name), 'wb+') as destination:
                        for chunk in attachment.chunks():
                            destination.write(chunk)
                    attached_file = File(open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name)))

                    msg_attachment = MessageAttachment()
                    msg_attachment.message = msg
                    msg_attachment.attachment_name = attachment._name
                    msg_attachment.attachment = attached_file
                    msg_attachment.save()
                    attatchment_url = settings.MEDIA_URL+str(msg_attachment.attachment)
                    attachment_name = attachment._name
                    thumbnail_url = ""
                    if msg_attachment.thumbnail:
                        thumbnail_url = settings.MEDIA_URL+str(msg_attachment.thumbnail)

                user_msg = UserMessage()
                user_msg.sender_id = sender.pk
                user_msg.receiver_id = recevr.pk
                user_msg.message_id = msg.pk
                user_msg.chat_type = 0 ###p2p chat.
                user_msg.save()

                context = {
                    "message": msg,
                    "sender_me": True,
                    "receiver_name": recevr.champuser.fullname
                }
                template_name = "ajax/message_detail_row.html"
                template = loader.get_template(template_name)
                cntxt = Context(context)
                rendered_detail_row = template.render(cntxt)

                template_name = "ajax/chatbox_chat_row.html"
                template = loader.get_template(template_name)
                cntxt = Context(context)
                rendered_chat_row = template.render(cntxt)

                contxt_data = {
                    "user_message": user_msg,
                    "request": request,
                    "STATIC_URL": settings.STATIC_URL+"/"
                }
                template_name = "ajax/message_list_entry.html"
                template = loader.get_template(template_name)
                cntxt = Context(contxt_data)
                rendered_message_list_entry = template.render(cntxt)

                content[request.user.pk] = {
                    "id": recevr.pk,
                    "chat_type": 0,
                    "title": recevr.champuser.fullname,
                    "chat_popup": rendered_chat_row,
                    "msg_detail_entry": rendered_detail_row,
                    # "unread_message_count": unread_message_count,
                    "message_list_entry": rendered_message_list_entry,
                    "status": "SUCCESS"
                }

                context = {
                    "message": msg,
                    "sender_me": False,
                    "receiver_name": request.user.champuser.fullname
                }
                template_name = "ajax/message_detail_row.html"
                template = loader.get_template(template_name)
                cntxt = Context(context)
                rendered_detail_row = template.render(cntxt)

                template_name = "ajax/chatbox_chat_row.html"
                template = loader.get_template(template_name)
                cntxt = Context(context)
                rendered_chat_row = template.render(cntxt)

                # unread_message_count = self.get_unread_message_count(recevr.pk) #UserMessage.objects.filter(chat_type=0, receiver_id=request.user.pk,message__is_read=0).count()

                content[recevr.pk] = {
                    "id": request.user.pk,
                    "chat_type": 0,
                    "title": request.user.champuser.fullname,
                    "chat_popup": rendered_chat_row,
                    "msg_detail_entry": rendered_detail_row,
                    # "unread_message_count": unread_message_count,
                    "message_list_entry": rendered_message_list_entry,
                    "status": "SUCCESS"
                }

                remote_peers = [ request.user.pk, recevr.pk ]

                ###Now publish the status with data in MESSAGE_SENT signal.
                try:
                    redis_client = RedisClient.Instance()
                    publish_message = {
                        "content": content,
                        "receivers": remote_peers
                    }
                    publish_message["CHANNEL"] = CHANNEL["send_chat_message"]
                    redis_client.publish_channel(CHANNEL["send_chat_message"], json.dumps(publish_message))
                except Exception,msg:
                    pass

            response = {
                "status": "SUCCESS",
                "message": "Successful",
                "data": ""
            }
            return self.render_to_json(response)


class MarkMessageMutateAjaxView(JSONResponseMixin, View):

    def get_unread_message_count(self, user_id):
        query = "select * from user_message where sender_id=%s or receiver_id=%s group by sender_id,receiver_id order by date_created DESC;" % (user_id, user_id)
        user_messages = UserMessage.objects.raw(query) #.filter(Q(sender=self.request.user) | Q(receiver=self.request.user)).group_by('sender_id','receiver_id').order_by("-date_created")
        umessage_ids = []
        uids = []
        for umessage in user_messages:
            if umessage.sender_id != user_id:
                if not umessage.sender_id in uids:
                    # umessage_ids += [ umessage.pk ]
                    uids += [ umessage.sender_id ]
            else:
                if not umessage.receiver_id in uids:
                    # umessage_ids += [ umessage.pk ]
                    uids += [ umessage.receiver_id ]

        for uid in uids:
            umessage = UserMessage.objects.filter((Q(sender_id=user_id) & Q(receiver_id=uid) & Q(message__is_read=0)) | (Q(sender_id=uid) & Q(receiver_id=user_id) & Q(message__is_read=0))).order_by('-date_created')
            if umessage.exists():
                umessage = umessage.first()
                umessage_ids += [ umessage.pk ]

        return len(umessage_ids)

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(MarkMessageMutateAjaxView, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        message_ids = request.POST.get("message_ids")
        chat_type = request.POST.get('chat_type')
        chat_id = request.POST.get('chat_id')
        if message_ids == "all":
            message_ids = []
        try:
            message_ids = [ int(mid) for mid in message_ids.split(',') ] if message_ids else message_ids
            chat_type = int(chat_type)
            if chat_type == 0:
                chat_id = int(chat_id)
        except:
            message_ids = None

        if message_ids is None:
            response = {
                "status": "FAILURE",
                "message": "Message ids not given.",
                "data": ""
            }
            return self.render_to_json(response)

        if chat_type is None:
            response = {
                "status": "FAILURE",
                "message": "Chat type requered.",
                "data": ""
            }
            return self.render_to_json(response)

        if message_ids:
            message_objects = Message.objects.filter(pk__in=message_ids)
            for message_object in message_objects:
                message_object.is_read = 1
                message_object.last_seen = Clock.utc_timestamp()
                message_object.save()

            ###Now publish the status with data in MESSAGE_SENT signal.
            try:
                redis_client = RedisClient.Instance()
                publish_message = {
                    "status": "SUCCESS",
                    "receiver_id": request.user.pk,
                    "unread_message_count": self.get_unread_message_count(request.user.pk)
                }
                publish_message["CHANNEL"] = CHANNEL["message_unread_count"]
                redis_client.publish_channel(CHANNEL["message_unread_count"], json.dumps(publish_message))
            except Exception,msg:
                pass

            response = {
                "status": "SUCCESS",
                "message": "Successful",
                "data": ""
            }
            return self.render_to_json(response)
        else:
            if chat_type == 0: ###p2p chat.
                message_objects = Message.objects.filter(Q(usermessage__chat_type=0) & (Q(usermessage__sender_id=chat_id) | Q(usermessage__receiver_id=chat_id)) & Q(usermessage__message__is_read=0))
                for message_object in message_objects:
                    message_object.is_read = 1
                    message_object.last_seen = Clock.utc_timestamp()
                    message_object.save()

                redis_client = RedisClient.Instance()
                publish_message = {
                    "status": "SUCCESS",
                    "receiver_id": request.user.pk,
                    "unread_message_count": self.get_unread_message_count(request.user.pk)
                }
                publish_message["CHANNEL"] = CHANNEL["message_unread_count"]
                redis_client.publish_channel(CHANNEL["message_unread_count"], json.dumps(publish_message))

                response = {
                    "status": "SUCCESS",
                    "message": "Successful",
                    "data": ""
                }
                return self.render_to_json(response)
            elif chat_type == 1:
                message_objects = Message.objects.filter(usermessage__chat_type=1,usermessage__conversation__conversation_id=chat_id, usermessage__message__is_read=0)
                for message_object in message_objects:
                    message_object.is_read = 1
                    message_object.last_seen = Clock.utc_timestamp()
                    message_object.save()

                redis_client = RedisClient.Instance()
                publish_message = {
                    "status": "SUCCESS",
                    "receiver_id": request.user.pk,
                    "unread_message_count": self.get_unread_message_count(request.user.pk)
                }
                publish_message["CHANNEL"] = CHANNEL["message_unread_count"]
                redis_client.publish_channel(CHANNEL["message_unread_count"], json.dumps(publish_message))

                response = {
                    "status": "SUCCESS",
                    "message": "Successful",
                    "data": ""
                }
                return self.render_to_json(response)
            response = {
                "status": "FAILURE",
                "message": "Invalid chat type.",
                "data": ""
            }
            return self.render_to_json(response)

class AddBuddyToMessageConversationAjaxView(JSONResponseMixin, View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(AddBuddyToMessageConversationAjaxView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        context = {}
        template_name = "ajax/add_buddy_to_message_conversation_dialog.html"
        return render(request,template_name,context)
    def post(self,request,*args,**kwargs):
        buddy_list = request.POST.getlist("buddy-list[]")
        conversation_id = request.POST.get("conversation_id")
        format = request.POST.get('format')
        user_id = request.user.pk
        conversation_objects = Conversation.objects.filter(conversation_id=conversation_id)

        if conversation_objects.exists():

            conversation_object = conversation_objects.first()

            user_in_this_conversation = (conversation_object.other_users.filter(pk__in=[ request.user.pk ]) or conversation_objects.filter(usermessage__sender_id__in=[ request.user.pk ]).exists())

            _added_users = []
            if conversation_objects.exists() and user_in_this_conversation and not conversation_object.users_left.filter(pk__in=[user_id]).exists():
                try:
                    for buddy_id in buddy_list:
                        user = User.objects.get(pk=int(buddy_id))
                        user_in_this_conversation = (conversation_object.other_users.filter(pk__in=[ user.pk ]) or conversation_objects.filter(usermessage__sender_id__in=[ user.pk ]).exists())
                        if not user_in_this_conversation and not conversation_object.users_left.filter(pk__in=[user.pk]).exists():
                            conversation_object.other_users.add(user)
                            _added_users += [ user.pk ]

                            since_objects = conversation_object.since_dates.filter(user_id=user.pk)
                            if since_objects.exists():
                                since_object = since_objects.first()
                                conversation_object.since_dates.remove(since_object)
                                since_object.delete()

                    names =  ','.join([ cu.fullname for cu in ChampUser.objects.filter(user_id__in=_added_users)])

                    message = Message()
                    message.chat_type = 4 ###auto generated.
                    message.is_read = 1
                    message.msg = "%s has added %s" % ( request.user.champuser.fullname, names )
                    message.save()

                    user_message = UserMessage()
                    user_message.sender_id = request.user.pk
                    user_message.chat_type = 4 ###Auto generated.
                    user_message.message_id = message.pk
                    user_message.conversation_id = conversation_object.pk
                    user_message.save()

                    receivers = [int(uid) for uid in conversation_object.other_users.values_list('pk', flat=True)]

                    redis_client = RedisClient.Instance()
                    publish_message = {
                        "status": "SUCCESS",
                        "chat_id": conversation_object.conversation_id,
                        "receivers": receivers
                    }
                    publish_message["CHANNEL"] = CHANNEL["conversation_buddy_added"]
                    redis_client.publish_channel(CHANNEL["conversation_buddy_added"], json.dumps(publish_message))

                    all_users = receivers

                    added_users = []

                    content = {}

                    for euser in all_users:
                        if not euser in added_users:
                            context = {
                                "message": message,
                                "sender_me": euser == request.user.pk
                            }
                            template_name = "ajax/message_detail_row.html"
                            template = loader.get_template(template_name)
                            cntxt = Context(context)
                            rendered_detail_row = template.render(cntxt)

                            template_name = "ajax/chatbox_chat_row.html"
                            template = loader.get_template(template_name)
                            cntxt = Context(context)
                            rendered_chat_row = template.render(cntxt)

                            contxt_data = {
                                "user_message": user_message,
                                "request": request,
                                "STATIC_URL": settings.STATIC_URL+"/"
                            }
                            template_name = "ajax/message_list_entry.html"
                            template = loader.get_template(template_name)
                            cntxt = Context(contxt_data)
                            rendered_message_list_entry = template.render(cntxt)

                            # unread_message_count = self.get_unread_message_count(euser.pk)

                            content[euser] = {
                                "id": conversation_id,
                                "chat_type": 1,
                                "title": "Group Chat",
                                "chat_popup": rendered_chat_row,
                                "msg_detail_entry": rendered_detail_row,
                                "message_list_entry": rendered_message_list_entry,
                                # "unread_message_count": unread_message_count,
                                "remote_peers": receivers,
                                "status": "SUCCESS"
                            }
                            added_users += [ euser ]

                    for euser in all_users:
                        content[euser]["remote_peers"] = added_users

                    ###Now publish the status with data in MESSAGE_SENT signal.
                    try:
                        redis_client = RedisClient.Instance()
                        publish_message = {
                            "content": content,
                            "receivers": added_users
                        }
                        publish_message["CHANNEL"] = CHANNEL["send_chat_message"]
                        redis_client.publish_channel(CHANNEL["send_chat_message"], json.dumps(publish_message))
                    except Exception,msg:
                        pass

                except Exception, msg:
                    pass
        if format == 'json':
            response = {
                "status": "SUCCESS",
                "message": "Successful",
                "data": ""
            }
            return self.render_to_json(response)
        return HttpResponseRedirect(reverse("message_conversation_detail_view",kwargs={ "conversation_id": conversation_id }))

class LeaveMessageConversationAjaxView(JSONResponseMixin, View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(LeaveMessageConversationAjaxView, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        conversation_id = request.POST.get("conversation_id")
        user_id = request.user.pk
        conversation_objects = Conversation.objects.filter(conversation_id=conversation_id)

        if conversation_objects.exists():

            conversation_object = conversation_objects.first()

            user_in_this_conversation = (conversation_object.other_users.filter(pk__in=[ request.user.pk ]).exists() or conversation_objects.filter(usermessage__sender_id__in=[ request.user.pk ]).exists())
            user_left = conversation_object.users_left.filter(pk__in=[user_id]).exists()
            if conversation_objects.exists() and user_in_this_conversation and not user_left:
                conversation_object.users_left.add(request.user)

                since_objects = conversation_object.since_dates.filter(user_id=request.user.pk)
                if since_objects.exists():
                    since_object = since_objects.first()
                    since_object.since_ts = Clock.utc_timestamp()
                    since_object.save()
                else:
                    conversation_since_date = ConversationSinceDate()
                    conversation_since_date.user_id = request.user.pk
                    conversation_since_date.since_ts = Clock.utc_timestamp()
                    conversation_since_date.save()

                    conversation_object.since_dates.add(conversation_since_date)

                message = Message()
                message.chat_type = 4 ###auto generated.
                message.is_read = 1
                message.msg = "%s has left the conversation" % ( request.user.champuser.fullname, )
                message.save()

                user_message = UserMessage()
                user_message.sender_id = request.user.pk
                user_message.chat_type = 4 ###Auto generated.
                user_message.message_id = message.pk
                user_message.conversation_id = conversation_object.pk
                user_message.save()

                receivers = [ user_id ]

                redis_client = RedisClient.Instance()
                publish_message = {
                    "status": "SUCCESS",
                    "chat_id": conversation_object.conversation_id,
                    "receivers": receivers
                }
                publish_message["CHANNEL"] = CHANNEL["conversation_buddy_left"]
                redis_client.publish_channel(CHANNEL["conversation_buddy_left"], json.dumps(publish_message))

                all_users = [ request.user.pk ] + [ int(cu) for cu in conversation_object.other_users.values_list('pk', flat=True) ]

                added_users = []

                content = {}

                for euser in all_users:
                    user_blocked = BlockListUtil.check_if_user_blocked(request.user.pk, euser) if request.user.pk != euser else False
                    user_left = BlockListUtil.check_if_user_left_this_conversation(conversation_object.conversation_id,euser)
                    if not euser in added_users and not user_left and not user_blocked:
                        context = {
                            "message": message,
                            "sender_me": euser == request.user.pk
                        }
                        template_name = "ajax/message_detail_row.html"
                        template = loader.get_template(template_name)
                        cntxt = Context(context)
                        rendered_detail_row = template.render(cntxt)

                        template_name = "ajax/chatbox_chat_row.html"
                        template = loader.get_template(template_name)
                        cntxt = Context(context)
                        rendered_chat_row = template.render(cntxt)

                        contxt_data = {
                            "user_message": user_message,
                            "request": request,
                            "STATIC_URL": settings.STATIC_URL+"/"
                        }
                        template_name = "ajax/message_list_entry.html"
                        template = loader.get_template(template_name)
                        cntxt = Context(contxt_data)
                        rendered_message_list_entry = template.render(cntxt)

                        # unread_message_count = self.get_unread_message_count(euser.pk)

                        content[euser] = {
                            "id": conversation_id,
                            "chat_type": 1,
                            "title": "Group Chat",
                            "chat_popup": rendered_chat_row,
                            "msg_detail_entry": rendered_detail_row,
                            "message_list_entry": rendered_message_list_entry,
                            # "unread_message_count": unread_message_count,
                            "remote_peers": receivers,
                            "status": "SUCCESS"
                        }
                        added_users += [ euser ]

                for euser in all_users:
                    if content.get(euser):
                        content[euser]["remote_peers"] = added_users

                ###Now publish the status with data in MESSAGE_SENT signal.
                try:
                    redis_client = RedisClient.Instance()
                    publish_message = {
                        "content": content,
                        "receivers": added_users
                    }
                    publish_message["CHANNEL"] = CHANNEL["send_chat_message"]
                    redis_client.publish_channel(CHANNEL["send_chat_message"], json.dumps(publish_message))
                except Exception,msg:
                    pass

                response = {
                    "status": "SUCCESS",
                    "message": "Successful",
                    "data": ""
                }
                return self.render_to_json(response)
        response = {
            "status": "FAILURE",
            "message": "Invalid chat type.",
            "data": ""
        }
        return self.render_to_json(response)

class DeleteMessageAjaxView(JSONResponseMixin, View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(DeleteMessageAjaxView, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        message_ids = request.POST.get("message_ids")

        try:
            message_ids = message_ids.split(',')
            message_ids = [ int(mid) for mid in message_ids if mid ]
        except:
            response = {
                "status": "FAILURE",
                "message": "Invalid parameter",
                "data": ""
            }
            return self.render_to_json(response)

        UserMessage.objects.filter(message__id__in=message_ids).delete()
        Message.objects.filter(pk__in=message_ids).delete()

        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": ""
        }
        return self.render_to_json(response)

class ChatSettingsAjaxView(JSONResponseMixin, View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ChatSettingsAjaxView, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            response = {
                "status": "SUCCESS",
                "message": "Successful",
                "data": ""
            }
            return self.render_to_json(response)
        chat_settings = ChatSettings.objects.filter(user_id=request.user.pk)
        if not chat_settings.exists():
            chat_setting = ChatSettings()
            chat_setting.user_id = request.user.pk
            chat_setting.save()
        else:
            chat_setting = chat_settings.first()

        action = request.POST.get("action")
        if action == "online_status":
            online_status = request.POST.get("status")
            if online_status == "online":
                chat_setting.online_status = 1
            else:
                chat_setting.online_status = 0
            chat_setting.save()

            try:
                redis_client = RedisClient.Instance()
                publish_message = {
                    "data": {
                        'status' : online_status,
                        'action' : action
                    },
                    "receivers": [ request.user.pk ]
                }
                publish_message["CHANNEL"] = CHANNEL["chat_settings_updated"]
                redis_client.publish_channel(CHANNEL["chat_settings_updated"], json.dumps(publish_message))
            except Exception as msg:
                pass
        elif action == "message_details_enter":
            status = int(request.POST.get("status"))
            if status == 1:
                chat_setting.enter_to_send = 1
            else:
                chat_setting.enter_to_send = 0
            chat_setting.save()

            try:
                redis_client = RedisClient.Instance()
                publish_message = {
                    "data": {
                        'status' : status,
                        'action' : action
                    },
                    "receivers": [ request.user.pk ]
                }
                publish_message["CHANNEL"] = CHANNEL["chat_settings_updated"]
                redis_client.publish_channel(CHANNEL["chat_settings_updated"], json.dumps(publish_message))
            except Exception,msg:
                pass

        elif action == "chatbox":
            chat_type = int(request.POST.get("chat_type"))
            chat_id = request.POST.get("chat_id")
            status = request.POST.get("status")
            active_status = 0
            if status == "show" or status == "hide":
                if status == "show":
                    active_status = 1
                if chat_setting.chatboxes.filter(chat_type=chat_type,chat_id=chat_id).exists():
                    chatbox = chat_setting.chatboxes.filter(chat_type=chat_type,chat_id=chat_id).first()
                    chatbox.active_status = active_status
                    chatbox.save()
                else:
                    chatbox = Chatbox()
                    chatbox.chat_type = chat_type
                    chatbox.chat_id = chat_id
                    chatbox.open_status = 1
                    chatbox.active_status = active_status
                    chatbox.save()

                    chat_setting.chatboxes.add(chatbox)

            elif status == "close":
                chat_type = int(request.POST.get("chat_type"))
                chat_id = request.POST.get("chat_id")
                if chat_setting.chatboxes.filter(chat_type=chat_type,chat_id=chat_id).exists():
                    chatbox = chat_setting.chatboxes.filter(chat_type=chat_type,chat_id=chat_id).first()
                    chat_setting.chatboxes.remove(chatbox)
                    chatbox.delete()

            chtbx_data = {}
            if chat_type == 0: ###p2p chat
                champ_user = ChampUser.objects.get(user_id=int(chat_id))
                chtbx_data['chat_type'] = 0
                chtbx_data['chat_id'] = int(chat_id)
                chtbx_data['remote_peers'] = chat_id
                chtbx_data['title'] = champ_user.fullname
                chtbx_data['online_status'] = champ_user.render_online_status_verbal
                chtbx_data["show"] = 1 if (active_status == 1) else 0
                chtbx_data['status'] = status
                chtbx_data['action'] = action
            else:
                conversation = Conversation.objects.filter(conversation_id=chat_id)
                if conversation.exists():
                    conversation = conversation.first()
                    remote_peers = [ str(user.pk) for user in conversation.other_users.all() ]
                    chtbx_data['chat_type'] = 1
                    chtbx_data['chat_id'] = chat_id
                    chtbx_data["remote_peers"] = ','.join(remote_peers)
                    chtbx_data['title'] = conversation.title if conversation.title else 'Group Chat'
                    chtbx_data['online_status'] = "offline"
                    chtbx_data["show"] = 1 if (active_status == 1) else 0
                    chtbx_data['status'] = status
                    chtbx_data['action'] = action
            if chtbx_data:
                ###Now publish the status with data in MESSAGE_SENT signal.
                try:
                    redis_client = RedisClient.Instance()
                    publish_message = {
                        "data": chtbx_data,
                        "receivers": [ request.user.pk ]
                    }
                    publish_message["CHANNEL"] = CHANNEL["chat_settings_updated"]
                    redis_client.publish_channel(CHANNEL["chat_settings_updated"], json.dumps(publish_message))
                except Exception,msg:
                    pass

        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": ""
        }
        return self.render_to_json(response)

class ConversationRenameAjaxView(JSONResponseMixin, View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ConversationRenameAjaxView, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        conversation_id = request.POST.get("chat_id")
        title = request.POST.get("title")
        user_id = request.user.pk
        conversation_objects = Conversation.objects.filter(conversation_id=conversation_id)

        if conversation_objects.exists():

            conversation_object = conversation_objects.first()

            user_in_this_conversation = (conversation_object.other_users.filter(pk__in=[ request.user.pk ]) or conversation_objects.filter(usermessage__sender_id__in=[ request.user.pk ]).exists())

            if conversation_objects.exists() and user_in_this_conversation and not conversation_object.users_left.filter(pk__in=[user_id]).exists():
                conversation_object.title = title
                conversation_object.save()

                message = Message()
                message.chat_type = 4 ###auto generated.
                message.is_read = 1
                message.msg = "%s has changed the conversation name to %s" % ( request.user.champuser.fullname, title )
                message.save()

                user_message = UserMessage()
                user_message.sender_id = request.user.pk
                user_message.chat_type = 4 ###Auto generated.
                user_message.message_id = message.pk
                user_message.conversation_id = conversation_object.pk
                user_message.save()

                receivers = [int(uid) for uid in conversation_object.other_users.values_list('pk', flat=True)]

                redis_client = RedisClient.Instance()
                publish_message = {
                    "status": "SUCCESS",
                    "chat_id": conversation_object.conversation_id,
                    "receivers": receivers,
                    "title": title
                }
                publish_message["CHANNEL"] = CHANNEL["conversation_title_change"]
                redis_client.publish_channel(CHANNEL["conversation_title_change"], json.dumps(publish_message))

                all_users = receivers

                added_users = []

                content = {}

                for euser in all_users:
                    if not euser in added_users:
                        context = {
                            "message": message,
                            "sender_me": euser == request.user.pk
                        }
                        template_name = "ajax/message_detail_row.html"
                        template = loader.get_template(template_name)
                        cntxt = Context(context)
                        rendered_detail_row = template.render(cntxt)

                        template_name = "ajax/chatbox_chat_row.html"
                        template = loader.get_template(template_name)
                        cntxt = Context(context)
                        rendered_chat_row = template.render(cntxt)

                        contxt_data = {
                            "user_message": user_message,
                            "request": request,
                            "STATIC_URL": settings.STATIC_URL+"/"
                        }
                        template_name = "ajax/message_list_entry.html"
                        template = loader.get_template(template_name)
                        cntxt = Context(contxt_data)
                        rendered_message_list_entry = template.render(cntxt)

                        # unread_message_count = self.get_unread_message_count(euser.pk)

                        content[euser] = {
                            "id": conversation_id,

                            "chat_type": 1,
                            "title": "Group Chat",
                            "chat_popup": rendered_chat_row,
                            "msg_detail_entry": rendered_detail_row,
                            "message_list_entry": rendered_message_list_entry,
                            # "unread_message_count": unread_message_count,
                            "remote_peers": receivers,
                            "status": "SUCCESS"
                        }
                        added_users += [ euser ]

                for euser in all_users:
                    content[euser]["remote_peers"] = added_users

                ###Now publish the status with data in MESSAGE_SENT signal.
                try:
                    redis_client = RedisClient.Instance()
                    publish_message = {
                        "content": content,
                        "receivers": added_users
                    }
                    publish_message["CHANNEL"] = CHANNEL["send_chat_message"]
                    redis_client.publish_channel(CHANNEL["send_chat_message"], json.dumps(publish_message))
                except Exception,msg:
                    pass

                response = {
                    "status": "SUCCESS",
                    "message": "Successful",
                    "data": ""
                }
                return self.render_to_json(response)
        response = {
            "status": "FAILURE",
            "message": "Invalid chat type.",
            "data": ""
        }
        return self.render_to_json(response)

class ManageChatBlockListAjaxView(JSONResponseMixin, View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ManageChatBlockListAjaxView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        context = {}
        user_blocklist_settings = UserBlockSettings.objects.filter(user_id = request.user.pk)
        if user_blocklist_settings.exists():
            user_blocklist_settings = user_blocklist_settings.first()
            context["blocked_users"] = user_blocklist_settings.blocked_users.all()
        return render(request,"ajax/manage_blocklist.html",context)

    def post(self, request, *args, **kwargs):
        action = request.POST.get("action")
        user_id = request.POST.get("user_id")
        user_object = None
        try:
            user_id = int(user_id)
            user_object = User.objects.get(pk=user_id)
        except:
            response = {
                "status": "FAILURE",
                "message": "Invalid user id",
                "data": ""
            }
            return self.render_to_json(response)
        user_blocklist_settings = UserBlockSettings.objects.filter(user_id = request.user.pk)
        if user_blocklist_settings.exists():
            user_blocklist_settings = user_blocklist_settings.first()
        else:
            user_blocklist_settings = UserBlockSettings()
            user_blocklist_settings.user_id = request.user.pk
            user_blocklist_settings.save()
        if action == "ADD":
            user_exists_2 = False
            user_blocklist_settings2 = UserBlockSettings.objects.filter(user_id = user_id)
            if user_blocklist_settings2.exists():
                user_blocklist_settings2 = user_blocklist_settings2.first()
                user_exists_2 = user_blocklist_settings2.blocked_users.filter(user_id=request.user.pk).exists()
            if not user_blocklist_settings.blocked_users.filter(user_id=user_id).exists() and not user_exists_2:
                block_user_entry = BlockUserEntry()
                block_user_entry.user_id = user_object.pk
                block_user_entry.save()
                user_blocklist_settings.blocked_users.add(block_user_entry)



                ###Now publish the status with data in MESSAGE_SENT signal.
                try:
                    content = {
                        request.user.pk: {
                            "blocked_user": user_id
                        },
                        user_id: {
                            "blocked_user": request.user.pk
                        }
                    }
                    redis_client = RedisClient.Instance()
                    publish_message = {
                        "content": content,
                        "receivers": [ request.user.pk, user_id ]
                    }
                    publish_message["CHANNEL"] = CHANNEL["user_blocked"]
                    redis_client.publish_channel(CHANNEL["user_blocked"], json.dumps(publish_message))
                except Exception,msg:
                    pass

                template_name = "ajax/block_list_item.html"
                template = loader.get_template(template_name)
                cntxt = Context({
                    "champuser": ChampUser.objects.get(user_id=user_id)
                })
                rendered = template.render(cntxt)

                response = {
                    "status": "SUCCESS",
                    "message": "Successful",
                    "data": rendered
                }
                return self.render_to_json(response)
            response = {
                "status": "FAILURE",
                "message": "Already added to the block list",
                "data": ""
            }
            return self.render_to_json(response)

        elif action == "REMOVE":
            if user_blocklist_settings.blocked_users.filter(user_id=user_id).exists():
                bus = user_blocklist_settings.blocked_users.filter(user_id=user_id).first()
                user_blocklist_settings.blocked_users.remove(bus)

                ###Now publish the status with data in MESSAGE_SENT signal.
                try:
                    content = {
                        request.user.pk: {
                            "blocked_user": user_id
                        },
                        user_id: {
                            "blocked_user": request.user.pk
                        }
                    }
                    redis_client = RedisClient.Instance()
                    publish_message = {
                        "content": content,
                        "receivers": [ request.user.pk, user_id ]
                    }
                    publish_message["CHANNEL"] = CHANNEL["user_unblocked"]
                    redis_client.publish_channel(CHANNEL["user_unblocked"], json.dumps(publish_message))
                except Exception,msg:
                    pass

            response = {
                "status": "SUCCESS",
                "message": "Successful",
                "data": ""
            }
            return self.render_to_json(response)
        response = {
            "status": "FAILURE",
            "message": "Invalid action type.",
            "data": ""
        }
        return self.render_to_json(response)


class UpdateWbChatboxStatus(JSONResponseMixin, View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(UpdateWbChatboxStatus, self).dispatch(*args, **kwargs)

    def post(self,request,*args,**kwargs):
        whiteboard_id = request.POST.get('whiteboard_id')
        status = request.POST.get('status')
        try:
            whiteboard_id = int(whiteboard_id)
            status = int(status)
        except:
            response = {
                "status": "FAILURE",
                "message": "Invalid data",
                "data": ""
            }
            return self.render_to_json(response)

        whiteboard_objects = Whiteboard.objects.filter(pk=whiteboard_id)
        if not whiteboard_objects.exists() or (status != 0 and status != 1):
            response = {
                "status": "FAILURE",
                "message": "Invalid data",
                "data": ""
            }
            return self.render_to_json(response)

        whiteboard = whiteboard_objects.first()
        if request.user.is_authenticated():
            chatbox_statuses = whiteboard.chatbox_statuses.filter(user_id=request.user.pk)
            if chatbox_statuses.exists():
                chatbox_status = chatbox_statuses.first()
                chatbox_status.status = status
                chatbox_status.save()
            else:
                chatbox_status = WbChatboxStatus()
                chatbox_status.user_id = request.user.pk
                chatbox_status.status = status
                chatbox_status.save()
                whiteboard.chatbox_statuses.add(chatbox_status)
            response = {
                "status": "SUCCESS",
                "message": "Successful",
                "data": ""
            }
            return self.render_to_json(response)
        response = {
            "status": "FAILURE",
            "message": "Unauthorized Access",
            "data": ""
        }
        return self.render_to_json(response)

class DisableWbVideoSharingAjaxView(JSONResponseMixin, View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(DisableWbVideoSharingAjaxView, self).dispatch(*args, **kwargs)

    def post(self,request,*args,**kwargs):
        lesson_id = request.POST.get("lesson_id")
        try:
            lesson_id = int(lesson_id)
            live_lesson = LiveLesson.objects.get(pk=lesson_id)
        except Exception,msg:
            response = {
                "status": "FAILURE",
                "message": "Invalid lesson id",
                "data": ""
            }
            return self.render_to_json(response)
        whiteboard = live_lesson.whiteboard
        if whiteboard.video_chat:
            usertoken = whiteboard.video_chat.tokens.filter(email_or_session=request.user.email)
            if usertoken.exists():
                usertoken = usertoken.first()
                whiteboard.video_chat.tokens.remove(usertoken)
                usertoken.delete()
                response = {
                    "status": "SUCCESS",
                    "message": "Successful",
                    "data": ""
                }
            response = {
                "status": "FAILURE",
                "message": "Token does not exist",
                "data": ""
            }
            return self.render_to_json(response)
        response = {
            "status": "FAILURE",
            "message": "Token does not exist",
            "data": ""
        }
        return self.render_to_json(response)



class TutorListAjaxView(JSONResponseMixin,View):
    def get(self,request,*args,**kwargs):
        type_name = None
        utype = request.GET.get("utype")
        source = request.GET.get("source")
        if utype == "student":
            type_name = UserTypes.student.value
        elif utype == "teacher":
            type_name = UserTypes.teacher.value

        exclude_list = request.GET.get("exclude")
        if not exclude_list:
            exclude_list = [request.user.pk]

        else:
            exclude_list = [int(id) for id in exclude_list.split(',') if id]
            exclude_list += [request.user.pk]


        keyword = request.GET.get('q')
        if type_name:
            exacts = ChampUser.objects.filter(type=Role.objects.get(name=type_name))

        from email.utils import parseaddr
        if "@" in parseaddr(keyword)[1]:
            exacts = ChampUser.objects.filter(user__email=keyword)
        else:
            exacts = ChampUser.objects.filter(fullname__iexact=keyword)

        tutor_list = ChampUser.objects.all()
        if type_name:
            tutor_list = tutor_list.filter(type=Role.objects.get(name=type_name))

        if source:
            if source == "JOB_INVITE":
                ###Check for already invited tutors.
                job_id = request.GET.get("job_id")
                try:
                    job_id = int(job_id)
                    job = Job.objects.get(pk=job_id)
                    already_applied_tutors = job.applications.values_list('tutor_id', flat=True)
                    invited_users = JobInvitation.objects.filter(job_id=job_id).values_list('user_id', flat=True)
                    tutor_list = tutor_list.exclude(pk__in=already_applied_tutors).exclude(pk__in=invited_users)
                except:
                    pass

        temp_tutor_list = tutor_list

        tutor_list = tutor_list.exclude(user_id__in=exclude_list).exclude(pk__in=exacts.values_list('pk',flat=True))
        total_result = temp_tutor_list.exclude(user_id__in=exclude_list).count()
        if keyword:
            tutor_list = tutor_list.filter(fullname__icontains=keyword)
            
        tutor_list = tutor_list.order_by("fullname")[:100]
        final_tutor_list = list(exacts)
        final_tutor_list += [ t for t in tutor_list ]

        ttutor = []
        for ft in final_tutor_list:
            if not BlockListUtil.check_if_user_blocked(request.user.pk, ft.pk):
                ttutor += [ ft ]


        final_tutor_list = ttutor[:20]
        _format = request.GET.get("fmt")
        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "total_count": total_result,
            "items": [{"id": tutor.user.pk, "name": tutor.fullname} for tutor in final_tutor_list]
        }
        return self.render_to_json(response)

class LoadMessagesDetailsAjaxView(JSONResponseMixin,View):
    def get(self,request,*args,**kwargs):
        receiver_id = int(request.GET.get("receiver_id"))
        last_msg_id = int(request.GET.get("start"))
        is_delete_enabled = int(request.GET.get("is_delete_enabled"))
        messages = Message.objects.filter(usermessage__in=UserMessage.objects.filter((Q(sender=request.user) & Q(receiver=User.objects.get(pk=receiver_id))) | (Q(sender=User.objects.get(pk=receiver_id))) & Q(receiver=request.user)))
        if last_msg_id != -1:
            messages = messages.filter(pk__lt=last_msg_id).order_by("-date_created")[:10]
        else:
            messages = messages.order_by("-date_created")[:10]
        # print(len(messages))
        contents = []
        if messages.exists():
            for message in messages:
                umessageset = message.usermessage_set
                context = {
                    "message": message,
                    "sender_me": umessageset.first().sender.pk == request.user.pk,
                    "receiver_name": umessageset.first().receiver.champuser.fullname,
                    "is_delete_enabled": is_delete_enabled
                }
                template_name = "ajax/message_detail_row.html"
                template = loader.get_template(template_name)
                cntxt = Context(context)
                rendered_detail_row = template.render(cntxt)
                contents += [ rendered_detail_row ]
        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": {
                "contents": contents
            }
        }
        return self.render_to_json(response)

class LoadConversationDetailsAjaxView(JSONResponseMixin,View):
    def get(self,request,*args,**kwargs):
        last_msg_id = int(request.GET.get("start"))
        conversation_id = kwargs.get("conversation_id")
        is_delete_enabled = int(request.GET.get("is_delete_enabled"))
        user_messages = UserMessage.objects.filter(conversation__conversation_id=conversation_id)
        messages = Message.objects.filter(usermessage__id__in=user_messages.values_list('pk', flat=True))
        if last_msg_id != -1:
            messages = messages.filter(pk__lt=last_msg_id).order_by("-date_created")[:10]
        else:
            messages = messages.order_by("-date_created")[:10]
        # print(len(messages))
        contents = []
        if messages.exists():
            for message in messages:
                umessageset = message.usermessage_set
                context = {
                    "message": message,
                    "sender_me": message.usermessage_set.first().sender.pk == request.user.pk,
                    # "receiver_name": message.usermessage_set.first().receiver.champuser.fullname
                    "is_delete_enabled": is_delete_enabled
                }
                template_name = "ajax/message_detail_row.html"
                template = loader.get_template(template_name)
                cntxt = Context(context)
                rendered_detail_row = template.render(cntxt)
                contents += [ rendered_detail_row ]
        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": {
                "contents": contents
            }
        }
        return self.render_to_json(response)


class TutorMessageAjaxView(JSONResponseMixin,View):
    def post(self,request,*args,**kwargs):
        response = {
            "status": "FAILURE",
            "message": "Failure",
            "data": []
        }
        whiteboard_id = request.POST.get('whiteboard_id')
        whiteboard_objects = Whiteboard.objects.filter(pk=int(whiteboard_id))
        if not whiteboard_objects.exists():
            response['message'] = 'Whiteboard does not exist'
            return self.render_to_json(response)

        whiteboard = whiteboard_objects.first()

        if not request.user.is_authenticated():
            response['message'] = 'Unauthorized access'
            return self.render_to_json(response)

        text = request.POST.get("tutor_chat_text")
        attachment = request.FILES.get("upload_file")

        if not text and not attachment:
            response['message'] = 'Missing data'
            return self.render_to_json(response)

        sender = request.user
        receivers = [ request.user.pk ]
        if whiteboard.lessonrequest_set.exists():
            live_lesson = whiteboard.lessonrequest_set.first()
            receivers += [ live_lesson.user.pk, live_lesson.request_user.pk ] #User.objects.get(pk=int(request.POST.get("wb_tutor_id")))
            for ou in live_lesson.other_users.all():
                receivers += [ ou.pk ]

        temp = []
        for r in receivers:
            if not r in temp:
                temp += [ r ]

        receivers = temp

        wb_tutor_chat = TutorChat()

        no_value_given = True

        if text:
            wb_tutor_chat.text = text
            no_value_given = False

        attachment_original_name = ""
        attatchment_url = "";

        if attachment:
            file_name = str(request.user.id)+"-"+str(uuid.uuid4())+"."+attachment._name[attachment._name.rindex(".")+1:]
            attachment_original_name = attachment
            with open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name), 'wb+') as destination:
                for chunk in attachment.chunks():
                    destination.write(chunk)
            attached_file = File(open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name)))

            wb_tutor_chat.attachment_original_name = attachment_original_name
            wb_tutor_chat.attachment = attachment
            attatchment_url = settings.MEDIA_URL+str(wb_tutor_chat.attachment)
            no_value_given = False

        if request.user.is_authenticated():
            wb_tutor_chat.sender_id = sender.pk

        if whiteboard and not no_value_given:
            wb_tutor_chat.save()
            whiteboard.tutor_chat.add(wb_tutor_chat)

            response["status"] = "SUCCESS"
            response["message"] = "Successful"

            contxt_data = {
                "tutor_chat": wb_tutor_chat,
                "request": request,
                "STATIC_URL": settings.STATIC_URL+"/"
            }
            template_name = "ajax/wb_tutor_chat_row.html"
            template = loader.get_template(template_name)
            cntxt = Context(contxt_data)
            message_entry = template.render(cntxt)

            # unread_message_count = self.get_unread_message_count(request.user.pk) #UserMessage.objects.filter(chat_type=0, receiver_id=request.user.pk,message__is_read=0).count()

            wb_receivers = User.objects.filter(pk__in=receivers)
            for rcv in wb_receivers:
                if rcv.pk != request.user.pk:
                    wb_tutor_chat.receivers.add(rcv)

            if receivers:
                ###Now publish the status with data in MESSAGE_SENT signal.
                try:
                    redis_client = RedisClient.Instance()
                    publish_message = {
                        'receivers': receivers,
                        'message_entry': message_entry
                    }
                    publish_message["CHANNEL"] = CHANNEL["wb_msg"]
                    response['data'] = publish_message
                    import json
                    redis_client.publish_channel(CHANNEL["wb_msg"], json.dumps(response))
                except Exception,msg:
                    print("Exception message: %s" % str(msg))
        return self.render_to_json(response)

