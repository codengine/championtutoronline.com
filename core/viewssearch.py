__author__ = 'codengine'

from django.views.generic.base import View
from django.http import HttpResponse
from core.models3 import *
import json

class SearchUserByKeyword(View):
	def get(self,request,*args,**kwargs):
		keyword = request.GET.get("term")
		excludes = request.GET.get("exclude")
		excludes = excludes.split(",")
		excludes = [int(i) for i in excludes]
		this_uid = None
		if request.user.is_authenticated():
			this_uid = request.user.pk
			excludes += [this_uid]
		users = ChampUser.objects.filter(fullname__icontains=keyword.strip()).exclude(user__id__in=excludes)[:10]
		json_response = []
		for user in users:
			json_response += [
				{
					"id" : user.user_id,
					"pimage": "/static/images/profile_img.png", 
					"label" : user.fullname,
					"value" : str(user.user_id) + "|"+user.fullname
				}
			]
		return HttpResponse(json.dumps(json_response))

class SearchMajorByKeyword(View):
    def get(self,request,*args,**kwargs):
        keyword = request.GET.get("term")