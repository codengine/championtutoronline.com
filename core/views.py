from decimal import Decimal
import hashlib
from django.contrib.auth import authenticate, login
from django.db import transaction
from django.core.files.base import File
from django.core.paginator import Paginator
from django.shortcuts import get_object_or_404, redirect
from django.template.context import RequestContext
from django.utils.safestring import mark_safe
from django.views.decorators.csrf import csrf_exempt

from core.library.currency_util import CurrencyUtil
from core.library.job_util import JobUtil
from core.library.rate_conversion_util import RateConversionUtil
from core.library.subject_util import SubjectUtil
from core.models import Wishlist
from core.templatetags.datefilters import format_datetime, convert_utc_timestamp_to_local_datetime
from core.templatetags.questiontagfilters import replace_last_comma_with_and
from easy_thumbnails.files import get_thumbnailer
from core.generics.ajaxmixins import JSONResponseMixin
from core.jobs import add_job_recommendation_job
from core.library.Clock import Clock
from core.library.email_scheduler import EmailScheduler
from core.library.email_template_util import EmailTemplateUtil
from core.library.encryption_manager import EncryptDecryptManager
from core.library.lesson_reminder import LessonReminder
from core.library.lesson_util import LessonUtil
from core.library.user_util import UserUtil
from engine.config import servers
from engine.config.servers import APP_SERVER_HOST, APP_SERVER_PORT
from django.http.response import HttpResponse
from forms import LoginForm,SignUpForm, ProfilePictureForm, SubjectMajorUpdateForm, \
    EducationForm, AboutMeUpdateForm, TeachingExpForm, ExtInterestForm,MyAccountForm,StudentEducationForm, JobPostForm, \
    StudentPrivateInfoForm, ContactUsForm
from django.http import HttpResponseRedirect
from django.utils.decorators import method_decorator
from common.decorators import user_login_required
from django.contrib.sessions.models import Session
from common.methods import check_login, get_buddies_online
from django.views.generic import *
from django.contrib.auth.decorators import login_required
from django_rq import get_scheduler
from django_bootstrap_calendar import utils as cal_utils
from notification.utils.notification_manager import NotificationManager
from payment.models import PaymentMethod
from payment.payment_processor import PaymentProcessor
from pyetherpad.padutil import *
from pysharejs.CodePadUtil import *
from pysharejs.models import *
from common.views import *
import uuid
import json
from core.generics.mixins import CommonMixin, LoginRequiredMixin, PermissionRequiredMixin
from core.enums import JobStatus, JobOfferStatus, TransactionType
from forms import LessonRequestForm
from datetime import datetime, timedelta, date
import dateutil.relativedelta as dt_util
from django.template import loader, Context
import otlib
from django.contrib import messages
from core.models3 import *
import os
from django.conf import settings
import pytz
from django_bootstrap_calendar import utils as cal_utils


class ProtectedView(object):

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if request.method.lower() in self.http_method_names:
            handler = getattr(self, request.method.lower(), self.http_method_not_allowed)
        else:
            handler = self.http_method_not_allowed
        return handler(request, *args, **kwargs)

class SignUpSuccessView(CommonMixin, TemplateView):
    template_name = "signup_success.html"


class LoginView(CommonMixin, View):
    def get(self,request,*args,**kwargs):
        if check_login(request):
            return HttpResponseRedirect(reverse("home_page"))
        email = request.GET.get('email')
        login_form = LoginForm({ "email": email })
        error_msg = 'Status Error Message'
        next_page = request.GET.get("next")
        signup_form = SignUpForm()
        msg = request.GET.get("signup")
        context = {
            'title':'Login - Championtutor Online',
            'login_form':login_form,
            'signup_form': signup_form,
            'error_msg':error_msg,
            'error':False,
            'next':next_page
        }
        preset = request.GET.get("password_reset")
        if msg == "success":
            del context["error_msg"]
            del context["error"]
            context["success"] = True
            context["success_msg"] = "You have successfully completed the registration process. To complete your request please check your email. Click on the email verification link. After your email verification is done successfully get back here to login."
        elif preset == "success":
            del context["error_msg"]
            del context["error"]
            context["success"] = True
            context["success_msg"] = "Your password has been changed successfully."
        return render(request, 'login.html',context)
    def post(self,request,*args,**kwargs):
        login_form = LoginForm(request.POST)
        if not login_form.is_valid():
            return HttpResponseRedirect(reverse("user_login"))
        if login_form.is_valid():
            if login_form.authenticate(request):
                    request.session['is_login'] = True
                    redirect_url = reverse("user_profile", args=(request.user.pk,))
                    if request.POST.get("next"):
                        redirect_url = request.POST["next"]

                    request.session['is_login'] = True
                    request.session['user_id'] = request.user.pk
                    request.session['email'] = request.user.email
                    #request.session['utype'] = request.user.champuser.type
                    if request.POST.get("rememberme"):
                        seven_days = 24*60*60*7
                        request.session.set_expiry(seven_days)
                    return HttpResponseRedirect(redirect_url)
            else:
                error_msg = 'Email or Password Invalid'
                return render(request, 'login.html', {'title':'Login - Championtutor Online','login_form':login_form,'error_msg':error_msg,'error':True})
        else:
            error_msg = 'Email or Password Invalid'
            return render(request, 'login.html', {'title':'Login - Championtutor Online','login_form':login_form,'error_msg':error_msg,'error':True})

class LogoutView(CommonMixin, View):
    def get(self,request,*args,**kwargs):
        #Get the user session object.
        champusers = ChampUser.objects.filter(user_id=request.user.pk)
        if champusers.exists():
            champuser = champusers.first()
            champuser.online_status = 0
            champuser.save()
        session_objs = Session.objects.filter(session_key=request.session.session_key)
        if session_objs:
            session_objs[0].delete()
        return HttpResponseRedirect(reverse("home_page"))

class SignUpView(CommonMixin, View):
    def get(self,request,*args,**kwargs):
        signup_form = SignUpForm()
        return render(request,"registration.html",{'form':signup_form})


class SocialSignUpFormView(View):

    def get(self, request, *args, **kwargs):
        form_data = {}
        if "social_info" in request.session:
            fb_id = request.session["social_info"]["fb_id"]
            name = request.session["social_info"]["name"]
            surname = request.session["social_info"]["surname"]
            email = request.session["social_info"]["email"]
            gender = request.session["social_info"]["gender"]
            timezone = request.session["social_info"]["timezone"]

            em = EncryptDecryptManager(settings.CIPHER_KEY)
            fb_id = em.decrypt(fb_id)

            tz_initial = TZUtil.get_supported_tz()
            import base64
            form_data = {
                "u_u":  EncryptDecryptManager.encode_b64(em.encrypt(fb_id)),
                "signup_email": email,
                "signup_name": name,
                "signup_surname": surname,
                "signup_timezones": tz_initial
            }

            #signup_form = SignUpForm(initial=form_data, tz_offset=timezone)

        return render(request,"social_registration.html",form_data)

    def post(self, request, *args, **kwargs):
        show_form = request.POST.get("show_form")

        if show_form == "true":
            fb_id = request.POST.get("fb_id")
            first_name = request.POST.get("first_name")
            middle_name = request.POST.get("middle_name")
            surname = request.POST.get("surname")
            email = request.POST.get("email")
            gender = request.POST.get("gender")
            timezone = request.POST.get("tz_offset")

            if "social_info" in request.session:
                del request.session["social_info"]

            user_objects = User.objects.filter(email=email)
            if user_objects.exists():
                user_object = user_objects.first()
                if user_object.is_active:
                    #user_object.backend='django.contrib.auth.backends.ModelBackend'
                    user_object = authenticate(email=email,facebook_id=fb_id)
                    if user_object is not None:
                        login(request, user_object)
                        seven_days = 24*60*60*7
                        request.session.set_expiry(seven_days)
                        request.session['is_login'] = True
                        request.session['user_id'] = request.user.pk
                        request.session['email'] = request.user.email
                        return HttpResponseRedirect(request.user.champuser.get_login_success_url())

            em = EncryptDecryptManager(settings.CIPHER_KEY)
            request.session["social_info"] = {
                "fb_id": EncryptDecryptManager.encode_b64(em.encrypt(fb_id)),
                "name": (first_name if first_name else "") + " " + (middle_name if middle_name else ""),
                "surname": surname,
                "email": email,
                "gender": gender,
                "timezone": timezone,
                "added": str(datetime.now())
            }

            return HttpResponseRedirect(reverse("social_sign_up"))


class MyAccountView(CommonMixin, View):

    @method_decorator(user_login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(self.__class__, self).dispatch(request, *args, **kwargs)

    def get(self,request,*args,**kwargs):
        data = {"email":request.user.email}
        ma_form = MyAccountForm(data)
        #pm_form = PaymentMethodForm()

        #payments_methods = PaymentMethod.objects.filter(user=request.user)

        context_data = { "ma_form": ma_form}

        timezones = Clock.get_all_timezones()

        #context_data["timezones"] = timezones

        #context_data["buddies"] = get_buddies_online(request)

        return render(request,"myaccount.html",context_data)
class BillingView(CommonMixin,View):
    @method_decorator(user_login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(self.__class__, self).dispatch(request, *args, **kwargs)

    def get(self,request,*args,**kwargs):
        payment_methods = PaymentMethod.objects.filter(customer__user__id=request.user.pk).order_by('date_created')
        if request.user.champuser.is_tutor:
            transactions = Transaction.objects.filter(user_2__id=request.user.pk).order_by('-date_created')
        else:
            transactions = Transaction.objects.filter(user__id=request.user.pk).order_by('-date_created')
        context = {
            'payment_methods': payment_methods,
            "sale_transactions": transactions
        }
        if request.user.champuser.is_tutor:
            context["wallet_balances"] = request.user.champuser.wallet.wallet_balance.all()
        return render(request,"billing.html",context)

class HomePage(CommonMixin, View):
    def get(self, request):
        if request.session.get('is_login'):
            return HttpResponseRedirect(reverse("user_profile", args= (request.user.id,))+"?tab=profile")
        login_form = LoginForm()
        signup_form = SignUpForm()
        context = {
            "title": "Championtutor Online",
            "login_form": login_form,
            "signup_form": signup_form,
            "settings_obj": GlobalSettings.objects.all().first()
        }
        timezones = Clock.get_all_timezones()


        country_name = None

        try:
            country_name = request.session["user_info"]["country"]
        except:
            country_name = "Other"

        context["timezones"] = timezones
        context["levels"] = SubjectLevel.objects.filter(name__in=SubjectUtil.get_levels_for_country(country_name))
        return render(request, 'home.html', context)

class WhiteboardView(LoginRequiredMixin, View):

    # @method_decorator(user_login_required)
    # def dispatch(self, request, *args, **kwargs):
    #    return super(self.__class__, self).dispatch(request, *args, **kwargs)

    def get_context_data(self,request):
        data = {
            "buddies": None
        }
        # _this_user_id = request.user.id
        # user_objs = ChampUser.objects.filter(user__id=_this_user_id)
        # if user_objs:
        #     user_obj = user_objs[0]
        #     #data["utype"] = user_obj.type
        #
        #     query = None
        #
        #     if user_obj.type.name == UserTypes.student.value:
        #         #query to get teachers with recent chat and also teachers who initiated a chat with this student.
        #         data["buddies"] = ChampUser.objects.filter(~Q(user=request.user),type=Role.objects.get(name=UserTypes.teacher.value))
        #         #query = "select * from champ_user where type_id=1 and user_id != %s" % _this_user_id
        #     else:
        #         #query to get students with recent chat and also students who initiated a chat with this teacher.
        #         #query = "select * from champ_user where type_id=2 and user_id != %s" % _this_user_id
        #         data["buddies"] = ChampUser.objects.filter(~Q(user=request.user),type=Role.objects.get(name=UserTypes.student.value))
        #
        #     # if query:
        #     #     data["buddies"] = User.objects.raw(query)

        return data


    def get(self,request,*args,**kwargs):
        #c = RequestContext(request)
        #teachers = User.objects.raw("select * from champ_user where id != '%s'" % request.session.get("user_id"))
        ###Create a whiteboard for this user.

        # whiteboard_objs = Whiteboard.objects.filter(user=request.user)
        # if not whiteboard_objs:
        #     whiteboard_obj = Whiteboard()
        #     whiteboard_obj.user = request.user
        #     whiteboard_obj.name = str(request.user.id) + str(uuid.uuid4())
        #     whiteboard_obj.created_date = datetime.now()
        #     whiteboard_obj.active = 1
        #     whiteboard_obj.save()
        #
        #     drawing_board = DrawingBoard()
        #     drawing_board.whiteboard = whiteboard_obj
        #     drawing_board.numeric_id = 1
        #     drawing_board.name = "Default Board"
        #     #drawing_board.create_date = datetime.now()
        #     drawing_board.save()
        #
        #     ###Create Pad Author and group.
        #     # pad_util = PadUtil()
        #     # champ_user_obj = ChampUser.objects.get(user_id=request.user.id)
        #     # pad_author_id = pad_util.create_or_get_padauthor(request.user.id,champ_user_obj.fullname)
        #     # print "Pad Author Created. Author ID: "+pad_author_id
        #     #
        #     # pad_group_id = pad_util.create_or_get_padgroup(request.user.id)
        #     # print "Pad Group Created. Pad Group ID: "+pad_group_id
        #     #
        #     # pad_id = pad_util.create_group_pad(pad_group_id,str(request.user.id)+""+str(uuid.uuid4()))
        #
        #     ###This is for testing purpose. All pads are made public now.
        #     ##pad_util.make_pad_public(pad_id)
        #     ppads = PublicPad.objects.all()
        #     if not ppads:
        #
        #         pad_util = PadUtil()
        #         pad_id = pad_util.create_public_pad(1)
        #
        #         public_pad = PublicPad()
        #         public_pad.pad_nid = 1
        #         public_pad.pad_created_by = request.user
        #         #public_pad.pad_create_date = datetime.now()
        #         public_pad.save()
        #
        #     pcpads = PublicCodePad.objects.all()
        #     if not pcpads:
        #         public_code_pad = PublicCodePad()
        #         public_code_pad.pad_nid = 1
        #         public_code_pad.pad_id = uuid.uuid4()
        #         public_code_pad.pad_name = "Default"
        #         public_code_pad.pad_created_by = request.user
        #         #public_code_pad.pad_create_date = datetime.now()
        #         public_code_pad.pad_language = settings.CODEPAD_DEFAULT_LANGUAGE
        #         public_code_pad.save()



        context_data = self.get_context_data(request)

        # pad_group = PadGroup.objects.get(user=request.user)
        # pads = Pad.objects.filter(pad_group=pad_group)

        pads = PublicPad.objects.all()

        context_data["text_pads"] = pads

        code_pads = CodePad.objects.all()

        context_data["code_pads"] = code_pads

        context_data["champ_user"] = ChampUser.objects.get(user_id=request.user.id)

        context_data["champ_userid"] = request.user.id

        context_data["wb_tutor_id"] = 2

        return render(request, 'whiteboard.html', context_data)

class WhiteboardDemo(View):
    def get(self,request,*args,**kwargs):
        if request.user.is_authenticated():
            if not DemoWhiteboard.objects.filter(email_or_session_key=request.user.email).exists():
                whiteboard = Whiteboard()
                whiteboard.access = WhiteboardAccess.private.value
                whiteboard.created_by = request.user
                whiteboard.name = uuid.uuid4()
                whiteboard.unique_id = uuid.uuid4()
                whiteboard.save()
                whiteboard.users.add(request.user)


                drawingboard = DrawingBoard()
                drawingboard.name = "1"
                drawingboard.numeric_id = 1
                drawingboard.save()
                whiteboard.drawingboards.add(drawingboard)

                DrawingboardLastNid.objects.filter(group=request.user.email).delete()

                dblastnid = DrawingboardLastNid()
                dblastnid.group = request.user.email
                dblastnid.nid = 1
                dblastnid.save()

                # ###Create Pad Author and group.
                pad_util = PadUtil()
                lesson_requested_by_author_id = pad_util.create_or_get_padauthor(request.user.pk,request.user.champuser.fullname)
                print("Pad Author Created. Author ID: "+lesson_requested_by_author_id)

                pad_group_id = pad_util.create_or_get_padgroup(request.user.email,request.user.pk,demo_pad=True)
                print("Pad Group Created. Pad Group ID: "+pad_group_id)

                pad_id = pad_util.create_group_pad(pad_group_id,pad_name=str(uuid.uuid4()),user_id=request.user.pk,author_id=lesson_requested_by_author_id)
                pad_object = Pad.objects.get(pad_id=pad_id)
                pad_util.make_pad_public(pad_id)
                whiteboard.text_pads.add(pad_object)

                codepad_util = CodePadUtil()
                code_pad_obj = codepad_util.create_codepad(request.user.email,"Javascript",creator=request.user)

                whiteboard.code_pads.add(code_pad_obj)


                demo_whiteboard = DemoWhiteboard()
                demo_whiteboard.email_or_session_key = request.user.email
                demo_whiteboard.whiteboard = whiteboard
                demo_whiteboard.save()
            demo_wboard = DemoWhiteboard.objects.get(email_or_session_key=request.user.email)
            whiteboard = demo_wboard.whiteboard
            chatbox_status = 0
            chatbox_statues = whiteboard.chatbox_statuses.filter(user_id=request.user.pk)
            if chatbox_statues.exists():
                chatbox_status = chatbox_statues.first().status
            context_data = {
                "whiteboard": whiteboard,
                "wb_chatbox_status": chatbox_status,
                "demo": True
            }
            return render(request, 'whiteboard.html', context_data)
        else:
            if not DemoWhiteboard.objects.filter(email_or_session_key=request.session.session_key).exists():
                whiteboard = Whiteboard()
                whiteboard.access = WhiteboardAccess.public.value
                whiteboard.name = uuid.uuid4()
                whiteboard.unique_id = uuid.uuid4()
                whiteboard.save()

                drawingboard = DrawingBoard()
                drawingboard.name = "1"
                drawingboard.numeric_id = 1
                drawingboard.save()
                whiteboard.drawingboards.add(drawingboard)

                DrawingboardLastNid.objects.filter(group=request.session.session_key).delete()

                dblastnid = DrawingboardLastNid()
                dblastnid.group = request.session.session_key
                dblastnid.nid = 1
                dblastnid.save()

                # ###Create Pad Author and group.
                pad_util = PadUtil()
                lesson_requested_by_author_id = pad_util.create_or_get_padauthor(request.session.session_key,"")
                print("Pad Author Created. Author ID: "+lesson_requested_by_author_id)

                pad_group_id = pad_util.create_or_get_padgroup(request.session.session_key,demo_pad=True)
                print("Pad Group Created. Pad Group ID: "+pad_group_id)

                pad_id = pad_util.create_group_pad(pad_group_id,pad_name=str(uuid.uuid4()),author_id=lesson_requested_by_author_id)
                pad_object = Pad.objects.get(pad_id=pad_id)
                pad_util.make_pad_public(pad_id)
                whiteboard.text_pads.add(pad_object)

                codepad_util = CodePadUtil()
                code_pad_obj = codepad_util.create_codepad(request.session.session_key,"Javascript")

                whiteboard.code_pads.add(code_pad_obj)

                demo_whiteboard = DemoWhiteboard()
                demo_whiteboard.email_or_session_key = request.session.session_key
                demo_whiteboard.whiteboard = whiteboard
                demo_whiteboard.save()
            demo_wboard = DemoWhiteboard.objects.get(email_or_session_key=request.session.session_key)
            whiteboard = demo_wboard.whiteboard
            context_data = {
                "whiteboard": whiteboard,
                "demo": True
            }
            return render(request, 'whiteboard.html', context_data)

class LiveLessonView(LoginRequiredMixin,View):
    def get(self,request,*args,**kwargs):
        lesson_id = kwargs.get('pk')
        live_lesson = LiveLesson.objects.get(pk=int(lesson_id))
        session_started = False
        my_token_found = False
        video_chat = None
        token = None
        video_settings = None
        my_video_enabled = False

        if live_lesson.whiteboard.video_chat:
            if live_lesson.whiteboard.video_chat.ot_session:
                session_started = True
                video_chat = live_lesson.whiteboard.video_chat
                if video_chat.tokens.filter(email_or_session=request.user.email).exists():
                    token = video_chat.tokens.filter(email_or_session=request.user.email).first().token
                    my_token_found = True
                    my_video_enabled = True

                if video_chat.user_settings.filter(email_or_session_key=request.user.email).exists():
                    video_settings = video_chat.user_settings.filter(email_or_session_key=request.user.email).first().settings
                    if video_settings.video_enabled == 0 and video_settings.audio_enabled == 0:
                        my_video_enabled = False

        lesson_countdown_time = None
        lesson_start_remaining_time = None
        lesson_start_time = None

        now_timestamp = Clock.utc_timestamp()
        lesson_timestamp = Clock.convert_utc_to_local_timestamp(request.user.champuser.timezone,live_lesson.lesson_time)

        if lesson_timestamp > now_timestamp:
            lesson_start_remaining_time = datetime.fromtimestamp(lesson_timestamp) - datetime.fromtimestamp(now_timestamp)

        lesson_countdown_time = live_lesson.duration*60

        if lesson_start_remaining_time:
            lesson_start_time = Clock.convert_seconds_to_hhmmss(lesson_start_remaining_time.seconds)

        lesson_countdown_tm = Clock.convert_seconds_to_hhmmss(lesson_countdown_time)

        wb_messages = live_lesson.whiteboard.tutor_chat.all().order_by('-id')[:20]
        chat_messages = []
        for wb_tutor_chat in wb_messages:
            contxt_data = {
                "tutor_chat": wb_tutor_chat,
                "request": request,
                "STATIC_URL": settings.STATIC_URL+"/"
            }
            template_name = "ajax/wb_tutor_chat_row.html"
            template = loader.get_template(template_name)
            cntxt = Context(contxt_data)
            message_entry = template.render(cntxt)
            chat_messages += [ message_entry ]

        chatbox_status = 0
        chatbox_statues = live_lesson.whiteboard.chatbox_statuses.filter(user_id=request.user.pk)
        if chatbox_statues.exists():
            chatbox_status = chatbox_statues.first().status

        lesson_payment_status = live_lesson.payment_status == 1

        all_participants = [ live_lesson.user, live_lesson.request_user ] + [ ou for ou in live_lesson.other_users.all() ]

        job = None
        lesson_details = None
        if live_lesson.job_set.exists():
            job = live_lesson.job_set.first()
            ctxt = {
                "job": job,
                "lesson": live_lesson,
                "payment_status": lesson_payment_status
            }
            template = loader.get_template("ajax/lesson_details_popup.html")
            cntxt = Context(ctxt)
            lesson_details = template.render(cntxt)

        context_data = {
            "live_lesson": live_lesson,
            "whiteboard": live_lesson.whiteboard,
            "wb_chatbox_status": chatbox_status,
            "drawingboards": live_lesson.whiteboard.drawingboards.all(),
            "sticky_notes": live_lesson.whiteboard.sticky_notes.filter(user_id=request.user.pk),
            "session_started": session_started,
            "video_chat": video_chat,
            "token": token,
            "video_settings": video_settings,
            "ot_api_key": otlib.read_ot_api_key(),
            "lesson_start_remaining_time": lesson_start_remaining_time.seconds if lesson_start_remaining_time else None,
            "lesson_start_hour": lesson_start_time[0] if lesson_start_time else None,
            "less_start_minute": lesson_start_time[1] if lesson_start_time else None,
            "lesson_start_seconds": lesson_start_time[2] if lesson_start_time else None,
            "lesson_countdown_time": lesson_countdown_time,
            "lesson_countdown_hour": lesson_countdown_tm[0],
            "less_countdown_minute": lesson_countdown_tm[1],
            "lesson_countdown_seconds": lesson_countdown_tm[2],
            "tutor_chat_messages": chat_messages,
            "payment_status": lesson_payment_status,
            "all_participants": all_participants,
            "live": live_lesson.is_live,
            "lesson_status": live_lesson.lesson_status,
            "my_token_found": my_token_found,
            "my_video_enabled": my_video_enabled,
            "lesson_details": lesson_details
        }
        if not lesson_payment_status:
            if live_lesson.job_set.exists():
                if live_lesson.job_set.first().payer.pk == request.user.pk:
                    context_data["payer"] = True
                    context_data["payment_methods"] = PaymentMethod.objects.filter(customer__user__id=request.user.pk)
        return render(request, 'whiteboard.html', context_data)



class ProfileView(DetailView):
    model = ChampUser
    template_name = 'profile.html'

    # def get_template_names(self):
    #     if self.object.type.name == 'teacher':
    #         return self.template_name
    #     else:
    #         return 'student_profile.html'

    def get(self, request, *args, **kwargs):
        current_tab = request.GET.get('tab')
        if not current_tab and not self.request.GET.get("view_as") and self.kwargs.get('pk') == self.request.user.pk:
            return HttpResponseRedirect(reverse("user_profile", kwargs={ "pk": int(self.kwargs.get('pk')) })+"?tab=invites")
        else:
            context = super(ProfileView, self).get(request, *args, **kwargs)
            return context


    def get_template_names(self):
        if self.object.is_deactivated():
            return [
                "404.html"
            ]
        if self.object.type.name == UserTypes.teacher.value:
            return ["profile.html"]
        else:
            if self.request.GET.get('view-mode') == 'edit':
                return ["student_profile_edit.html"]
            else:
                return ["student_profile.html"]

    def get_object(self, queryset=None):
        return ChampUser.objects.get(user_id=self.kwargs[self.pk_url_kwarg])

    def get_profile_summary(self):
        profile_summary = {}

        try:

            if self.object.is_tutor:
                ###Get total requests.
                lesson_requests = LessonRequest.objects.filter(request_user=self.object.user)
                profile_summary["all_time_requests"] = lesson_requests.count()

                today = Clock.utc_now()
                first_day_of_month = date(day=1, month=today.month, year=today.year)
                last_month = first_day_of_month - timedelta(days=1)
                last_month_time_stamp = Clock.convert_datetime_to_timestamp(last_month)
                profile_summary["this_month_requests"] = lesson_requests.filter(date_created__gt=last_month_time_stamp).count()
                profile_summary["current_month"] = today.strftime('%b')
                profile_summary["current_year"] = today.strftime('%Y')

                first_day_of_year = date(day=1, month=1, year=today.year)
                last_year = first_day_of_year - timedelta(days=1)
                last_year_time_stamp = Clock.convert_datetime_to_timestamp(last_year)
                profile_summary["this_year_requests"] = lesson_requests.filter(date_created__gt=last_year_time_stamp).count()

                live_lesson_approved = LiveLesson.objects.filter(request_user=self.object.user,approved=ApprovalStatus.approved.value).count()
                live_lesson_rejected = LiveLesson.objects.filter(request_user=self.object.user,approved=ApprovalStatus.rejected.value).count()
                live_lesson_cancelled = LiveLesson.objects.filter(request_user=self.object.user,approved=ApprovalStatus.cancelled.value).count()

                written_lesson_approved = WrittenLesson.objects.filter(request_user=self.object.user,status=LessonStatus.approved.value).count()
                written_lesson_rejected = WrittenLesson.objects.filter(request_user=self.object.user,status=LessonStatus.rejected.value).count()
                written_lesson_cancelled = WrittenLesson.objects.filter(request_user=self.object.user,status=LessonStatus.cancelled.value).count()

                profile_summary['request_accepted'] = live_lesson_approved + written_lesson_approved
                profile_summary['request_rejected'] = live_lesson_rejected + written_lesson_rejected
                profile_summary['request_cancelled'] = live_lesson_cancelled + written_lesson_cancelled

                last_tutored_lesson = LessonRequest.objects.filter(request_user_id=self.object.user.pk,status=LessonStatus.completed.value).order_by("-lesson_time")

                profile_summary["last_tutored"] = last_tutored_lesson.first().date_created if last_tutored_lesson.exists() else -1

                #  Calculate likelihood of reply and expected response time.
                job_invitations = JobInvitation.objects.filter(user_id=self.object.user.pk)
                pending_invitations_count = job_invitations.filter(status=JobInvitationStatus.active.value).count()
                all_invitation_count = job_invitations.count()
                likelihood_of_reply = 0
                if all_invitation_count:
                    likelihood_of_reply = int((float( all_invitation_count - pending_invitations_count ) / all_invitation_count) * 100)
                if likelihood_of_reply >= 100:
                    likelihood_of_reply = "99+"
                profile_summary['likelihood_reply'] = likelihood_of_reply

                responded_time_average = UserMessageResponseTime.average_response_time(self.object.user.pk)

                if responded_time_average and responded_time_average != -1:
                    responded_time_average = responded_time_average/3600

                if responded_time_average == -1:
                    profile_summary['expected_response_time'] = ' N/A'
                elif responded_time_average > 0 and responded_time_average < 1:
                    profile_summary['expected_response_time'] = ' < 1 hour'
                elif responded_time_average >= 1 and responded_time_average < 72:
                    profile_summary['expected_response_time'] = ' ~'+str(responded_time_average) + ' hour'
                else:
                    profile_summary['expected_response_time'] = ' > 72 hours'

                ###Get total earning summary.
                all_lessons = LessonRequest.objects.filter(request_user_id=self.object.user.pk, payment_status=1).select_related('base_transaction')
                earnings = {}
                for lesson in all_lessons:
                    tx = lesson.base_transaction
                    currency = tx.currency
                    if not currency in earnings.keys():
                        earnings[currency] = { 'amount': tx.amount, 'duration': float(lesson.duration)/60 }
                    else:
                        earnings[currency]['amount'] += tx.amount
                        earnings[currency]['duration'] += float(lesson.duration)/60

                    other_transactions = lesson.transactions.filter(transaction_status=TransactionStatus.approved.value,transaction_type=TransactionType.sale.value).exclude(pk=tx.pk)
                    extra_time_added = False
                    for _tx in other_transactions:
                        currency = _tx.currency
                        if not currency in earnings.keys():
                            earnings[currency] = { 'amount': _tx.amount, 'duration': float(lesson.extra_time)/60 }
                        else:
                            earnings[currency]['amount'] += _tx.amount
                            if not extra_time_added:
                                earnings[currency]['duration'] += float(lesson.extra_time)/60
                                extra_time_added = True

                profile_summary['earnings'] = earnings

                lessons = LessonRequest.objects.select_related().filter(request_user_id=self.object.user.pk)

                tz = self.object.user.champuser.timezone

                month_cal_year, month_cal_month = datetime.now().year, datetime.now().month
                month_cal = cal_utils.MonthCalendar(lessons, tz, table="year").\
                    formatmonth(month_cal_year,
                                month_cal_month,
                                css_class="yearly_cal",
                                withyear=True,
                                table="year",
                                formatheadmonthname=True)  # The current month
                profile_summary["month_calendar"] = mark_safe(month_cal)

            else:

                # Find posted questions.
                profile_summary['total_posted_questions'] = Question.objects.filter(created_by_id=self.object.user.pk).count()
                profile_summary['total_posted_question_answered'] = Question.objects.filter(Q(created_by_id=self.object.user.pk) & ~Q(answers=None) & ~Q(answers__answer_by__id=self.object.user.pk)).count()

                # Find total tutoring subjects with hours.
                tutoring_subjects = {}

                all_completed_lessons = LessonRequest.objects.filter(user_id=self.object.user.pk, status=4).prefetch_related('subjects') # All completed lessons.
                for c_lesson in all_completed_lessons:
                    subject_object = c_lesson.subjects.all().first()
                    if subject_object:
                        subject_name = subject_object.name
                        subject_name = subject_name.capitalize()
                        lesson_time = c_lesson.duration + c_lesson.extra_time # Lesson total duration in minutes

                        added_lesson_time = 0
                        if tutoring_subjects.get(subject_name):
                            added_lesson_time += tutoring_subjects[subject_name]['seconds']

                        lesson_time += added_lesson_time

                        lesson_time_hours = ""

                        if lesson_time < 60:
                            lesson_time_hours = str(lesson_time) + " minutes" if lesson_time > 1 else str(lesson_time) + " minute"
                        elif lesson_time == 60:
                            lesson_time_hours = "1 hour"
                        elif lesson_time > 60:
                            if lesson_time % 60 > 0:
                                lesson_time_hours = str( lesson_time / 60 ) + " hr " + str(lesson_time % 60) + " min"
                            else:
                                lesson_time_hours = str( lesson_time / 60 ) + " hours"

                        tutoring_subjects[subject_name] = {
                            'seconds': lesson_time,
                            'verbal': lesson_time_hours
                        }

                profile_summary['tutoring_subjects'] = tutoring_subjects

                # Find lesson Stats
                lesson_stats = {}

                # Get all live lessons.

                live_lesson_stats = {}

                live_lessons = LessonRequest.objects.filter(type=0, user_id=self.object.user.pk)

                live_lesson_stats['total_requests'] = live_lessons.count()


                tday = datetime.now()

                this_year_start = datetime(day=1, month=1, year=tday.year, hour=0, minute=0, second=0)
                this_year_start_ts = time.mktime(this_year_start.timetuple())

                now_ts = time.mktime(tday.timetuple())

                all_live_lessons_this_year = LessonRequest.objects.filter(type=0, user_id=self.object.user.pk, lesson_time__gte=this_year_start_ts, lesson_time__lte=now_ts)
                live_lesson_stats['total_requests_this_year'] = all_live_lessons_this_year.count()

                this_month_start = datetime(day=1, month=tday.month, year=tday.year, hour=0, minute=0, second=0)
                this_month_start_ts = time.mktime(this_year_start.timetuple())

                all_live_lessons_this_month = LessonRequest.objects.filter(type=0, user_id=self.object.user.pk, lesson_time__gte=this_month_start_ts, lesson_time__lte=now_ts)
                live_lesson_stats['total_requests_this_month'] = all_live_lessons_this_month.count()

                # Get all cancelled live lessons
                cancelled_live_lessons = LessonRequest.objects.filter(type=0, user_id=self.object.user.pk, status=2) # Cancelled means status 2
                live_lesson_stats['cancelled_requests'] = cancelled_live_lessons.count()

                # Get count of all engaged tutors.
                all_engaged_live_tutors = LessonRequest.objects.filter(type=0, user_id=self.object.user.pk).values('request_user_id').distinct()
                live_lesson_stats['all_engaged_tutors'] = all_engaged_live_tutors.count()

                lesson_stats['live'] = live_lesson_stats

                # Get all written lessons.

                written_lesson_stats = {}

                written_lessons = LessonRequest.objects.filter(type=1, user_id=self.object.user.pk)

                written_lesson_stats['total_requests'] = written_lessons.count()


                tday = datetime.now()

                this_year_start = datetime(day=1, month=1, year=tday.year, hour=0, minute=0, second=0)
                this_year_start_ts = time.mktime(this_year_start.timetuple())

                now_ts = time.mktime(tday.timetuple())

                all_written_lessons_this_year = LessonRequest.objects.filter(type=1, user_id=self.object.user.pk, due_date__gte=this_year_start_ts, due_date__lte=now_ts)
                written_lesson_stats['total_requests_this_year'] = all_written_lessons_this_year.count()

                this_month_start = datetime(day=1, month=tday.month, year=tday.year, hour=0, minute=0, second=0)
                this_month_start_ts = time.mktime(this_year_start.timetuple())

                all_written_lessons_this_month = LessonRequest.objects.filter(type=1, user_id=self.object.user.pk, due_date__gte=this_month_start_ts, due_date__lte=now_ts)
                written_lesson_stats['total_requests_this_month'] = all_written_lessons_this_month.count()

                # Get all cancelled written lessons
                cancelled_written_lessons = LessonRequest.objects.filter(type=1, user_id=self.object.user.pk, status=2) # Cancelled means status 2
                written_lesson_stats['cancelled_requests'] = cancelled_written_lessons.count()

                # Get count of all engaged tutors.
                all_engaged_written_tutors = LessonRequest.objects.filter(type=1, user_id=self.object.user.pk).values('request_user_id').distinct()
                written_lesson_stats['all_engaged_tutors'] = all_engaged_written_tutors.count()

                lesson_stats['written'] = written_lesson_stats

                profile_summary['lesson_stats'] = lesson_stats

                # Find last lesson
                last_lesson = LessonRequest.objects.filter(user_id=self.object.user.pk, status=4).order_by('-date_modified').first()
                profile_summary['last_lesson'] = Clock.utc_timestamp_to_local_datetime(last_lesson.date_modified, self.object.timezone).strftime('%B %d, %Y') if last_lesson else None

                # Find Tuition Since
                profile_summary['tuition_since'] = Clock.utc_timestamp_to_local_datetime(self.object.date_created, self.object.timezone).strftime('%B %d, %Y')

                # generate profile url
                profile_summary['profile_url'] = servers.APP_SERVER_HOST+":"+str(servers.APP_SERVER_PORT)+reverse('user_profile', kwargs={ 'pk': self.object.user.pk })

                lessons = LessonRequest.objects.select_related().filter(Q(user_id=self.object.user.pk)|
                                                                            Q(other_users__id__in=[self.object.user.pk]))
                tz = self.object.user.champuser.timezone

                month_cal_year, month_cal_month = datetime.now().year, datetime.now().month
                month_cal = cal_utils.MonthCalendar(lessons, tz, table="year").\
                    formatmonth(month_cal_year,
                                month_cal_month,
                                css_class="yearly_cal",
                                withyear=True,
                                table="year",
                                formatheadmonthname=True)  # The current month
                profile_summary["month_calendar"] = mark_safe(month_cal)
        except Exception as exp:
            error_log = ErrorLog()
            error_log.url = ''
            error_log.stacktrace = 'Exception inside get_profile_summary of profile/pk/. Details: %s' % str(exp)
            error_log.save()

        return profile_summary

    def get_profile_context(self,user_id):
        context = {}

        context['thumbnail_url'] = self.object.profile_picture.image_field if self.object.profile_picture else ''
        if Profile.objects.filter(user=self.object.user).exists():
            context['profile'] = Profile.objects.filter(user_id=self.object.user.pk).first()
        else:
             context['profile'] = Profile.objects.create(user_id=self.object.user.pk)

        context["champ_user"] = self.object
        context["object"] = self.object

        nric_verification_objs = NRICVerification.objects.filter(user_id=self.object.user.pk)
        profile_verification_status = 0
        if nric_verification_objs:
            profile_verification_status = nric_verification_objs[0].status

        context["profile_verification_status"] = profile_verification_status

        context["majors"] = UserMajor.objects.filter(user_id=self.object.user.pk)

        teaching_exp_objs = TeachingExperiences.objects.filter(user_id=self.object.user.pk)

        if teaching_exp_objs:
            context["teaching_experiences"] = teaching_exp_objs[0]
        else:
            context["teaching_experiences"] = None

        extra_curricular_interest_objs = ExtraCurricularInterest.objects.filter(user_id=self.object.user.pk)
        if extra_curricular_interest_objs:
            context["extra_curricular_interest"] = extra_curricular_interest_objs[0]
        else:
            context["extra_curricular_interest"] = None

        educations = Education.objects.filter(user_id=self.object.user.pk)
        context["educations"] = educations

        user_top_courses = UserTopCourses.objects.filter(user_id=self.object.user.pk)
        context["top_courses"] = user_top_courses

        Rate.initialize_rates(self.request,self.object)

        rate_objs = Rate.objects.filter(user_id=self.object.user.pk)

        context["profile_rates"] = rate_objs

        context["profile_min_rate"] = -1
        context["profile_max_rate"] = -1

        min_rate_ = rate_objs.aggregate(Min('rate'))['rate__min']
        max_rate_ = rate_objs.aggregate(Max('rate'))['rate__max']

        if min_rate_ and max_rate_:
            context["profile_min_rate"] = min_rate_
            context["profile_max_rate"] = max_rate_

        context['content_editable'] = True if self.object.user.id == self.request.user.id else False

        course = []
        for user_major in context["majors"]:
            course += [user_major.major]

        context['lesson_request_form'] = LessonRequestForm(course=course)

        context["buddies"] = [] #get_buddies_online(self.request)

        if self.object.type.name == UserTypes.student.value:
            context['educations'] = Education.objects.filter(user=self.object.user)
            context["education_form"] = StudentEducationForm({'hidden_pk':'-1','hidden_school':'-1',"school_type":"college/university",'hidden_major':'-1'})
            view_mode = self.request.GET.get("view-mode")
            if view_mode == "profile":
                context["section"] = "profile"
            elif view_mode == "jobs":
                context["section"] = "jobs"
                context["job_post_list"] = Job.objects.filter(posted_by_id=self.object.user.pk)
            elif view_mode == "q_post":
                context["section"] = "q_post"
            elif view_mode == "qs":
                context["section"] = "qs"
                context["q_post_list"] = Question.objects.filter(created_by_id=self.object.user.pk)
            else:
                context["job_post_form"] = JobPostForm()

        else:
            # context["profile_summary"] = self.get_profile_summary()
            context["job_invitation_count"] = JobInvitation.objects.filter(user_id=self.object.user.pk,status=JobInvitationStatus.active.value).count()
            context["job_recommendation_count"] = 0
            view_mode = self.request.GET.get("view-mode")
            if view_mode == "profile":
                context["section"] = "profile"
            else:
                context["section"] = "feed"
                if self.request.user.is_authenticated():
                    questions = Question.objects.filter(status=QuestionStatus.open.value)
                    questions = questions.filter(tags__name__in=[user_major.major.name for user_major in UserMajor.objects.filter(user_id=self.object.user.pk)])
                    job_data = []
                    for question in questions:
                        template = loader.get_template("ajax/job_feed_item.html")
                        cntxt = Context({ 'question': question ,"request": self.request})
                        rendered = template.render(cntxt)
                        job_data += [rendered]

                    context["job_data"] = job_data
                else:
                    context["job_data"] = []

        return context

    def get_feed_data(self):
        job_application_list = JobApplication.objects.filter(tutor_id=self.request.user.pk).order_by("-date_created")

        paginate_by = 10
        paginator_object = Paginator(job_application_list,paginate_by)
        page = paginator_object.page(1)
        object_list = page.object_list

        job_application_list = []
        for job_application in object_list:
            template = loader.get_template("ajax/job_application_item.html")
            application_details_link = None
            if job_application.job_set.all().exists():
                application_details_link = reverse("job_application_details_view",kwargs={ "job_id":job_application.job_set.all().first().pk,"pk":job_application.pk })
            cntxt = Context({ 'job_application': job_application ,"request": self.request, "application_details_link": application_details_link})
            rendered = template.render(cntxt)
            job_application_list += [rendered]
        return job_application_list

    def get_job_invitation_data(self):

        paginate_by = 10

        job_invitation_list = JobInvitation.objects.filter(user_id=self.request.user.pk).exclude(job__status=4)

        job_invitation_list = job_invitation_list.order_by("-date_created")
        paginator_object = Paginator(job_invitation_list,paginate_by)
        job_invitations_data = []
        for job_invitation in paginator_object.page(1):
            template = loader.get_template("ajax/job_invitation_item.html")
            cntxt = Context({ 'job_invitation': job_invitation ,"request": self.request})
            rendered = template.render(cntxt)
            job_invitations_data += [rendered]
        return job_invitations_data

    def get_job_recommendation_data(self):
        job_recommendation_list = JobRecommendation.objects.filter(user_id=self.request.user.pk) #.exclude(job_id__in=[jb.pk for jb in Job.objects.filter(applications__id__in=JobApplication.objects.filter(tutor_id=request.user.pk).values_list('pk',flat=True))]).order_by("-date_created")
        paginate_by = 10
        paginator_object = Paginator(job_recommendation_list,paginate_by)
        job_recommendations_data = []
        for job_recommendation in paginator_object.page(1):
            template = loader.get_template("ajax/job_recommendation_item.html")
            cntxt = Context({ 'job_recommendation': job_recommendation ,"request": self.request})
            rendered = template.render(cntxt)
            job_recommendations_data += [rendered]
        return job_recommendations_data

    def get_qfeed(self):
        question_feed_list = Question.objects.all().order_by("-date_created")
        major_list = [user_major.major.name for user_major in UserMajor.objects.filter(user_id=self.request.user.pk)]
        question_feed_list = question_feed_list.filter(tags__value__in=major_list).distinct()
        paginate_by = 10
        paginator = Paginator(question_feed_list,paginate_by)
        page = paginator.page(1)
        object_list = page.object_list

        feed_data = []
        for question in object_list:
            template = loader.get_template("ajax/job_feed_item.html")
            cntxt = Context({ 'question': question ,"request": self.request})
            rendered = template.render(cntxt)
            feed_data += [rendered]
        return feed_data

    def get_qparticipations(self):
        participation_list = Question.objects.filter(Q(answers__answer_by_id=self.request.user.pk) | Q(answers__comments__created_by_id=self.request.user.pk))
        paginate_by = 10
        paginator_object = Paginator(participation_list,paginate_by)
        page = paginator_object.page(1)
        participations = []
        for question in page.object_list:
            template = loader.get_template("ajax/job_feed_item.html")
            cntxt = Context({ 'question': question ,"request": self.request})
            rendered = template.render(cntxt)
            participations += [rendered]
        return participations

    def get_context_data(self, **kwargs):
        context = super(ProfileView, self).get_context_data(**kwargs)
        context['thumbnail_url'] = self.object.profile_picture.image_field if self.object.profile_picture else ''
        if Profile.objects.filter(user_id=self.object.user.pk).exists():
            context['profile'] = Profile.objects.filter(user_id=self.object.user.pk).first()
        else:
             context['profile'] = Profile.objects.create(user_id=self.object.pk)

        context["champ_user"] = self.object

        nric_verification_objs = NRICVerification.objects.filter(user=self.object)
        profile_verification_status = 0
        if nric_verification_objs:
            profile_verification_status = nric_verification_objs[0].status

        context["profile_verification_status"] = profile_verification_status

        context["majors"] = UserMajor.objects.filter(user=self.object)

        if self.request.user.is_authenticated():
            user_level_names = SubjectUtil.get_levels_for_country(country_name=self.request.user.champuser.nationality.name)
            context["levels"] = SubjectLevel.objects.filter(name__in=user_level_names)

        teaching_exp_objs = TeachingExperiences.objects.filter(user=self.object)

        if teaching_exp_objs:
            context["teaching_experiences"] = teaching_exp_objs[0]
        else:
            context["teaching_experiences"] = None

        extra_curricular_interest_objs = ExtraCurricularInterest.objects.filter(user=self.object)
        if extra_curricular_interest_objs:
            context["extra_curricular_interest"] = extra_curricular_interest_objs[0]
        else:
            context["extra_curricular_interest"] = None

        educations = Education.objects.filter(user=self.object)
        context["educations"] = educations

        user_top_courses = UserTopCourses.objects.filter(user=self.object)
        context["top_courses"] = user_top_courses

        Rate.initialize_rates(self.request,self.object)

        rate_objs = Rate.objects.filter(user=self.object)

        context["profile_rates"] = rate_objs

        context["profile_min_rate"] = -1
        context["profile_max_rate"] = -1

        min_rate_ = rate_objs.aggregate(Min('rate'))['rate__min']
        max_rate_ = rate_objs.aggregate(Max('rate'))['rate__max']

        if min_rate_ and max_rate_:
            context["profile_min_rate"] = min_rate_
            context["profile_max_rate"] = max_rate_

        context['content_editable'] = True if self.object.user.id == self.request.user.id else False
        if self.request.GET.get("view") and self.request.GET.get("view") == "public":
            context["content_editable"] = False

        course = []
        for user_major in context["majors"]:
            course += [user_major.major]

        context['lesson_request_form'] = LessonRequestForm(course=course)

        context["buddies"] = [] #get_buddies_online(self.request)
        context['content_editable'] = True if self.object.user.id == self.request.user.id else False
        context["profile_summary"] = self.get_profile_summary()

        if self.object.type.name == UserTypes.student.value:
            page = self.request.GET.get('page', 1)
            context["type"] = type = self.request.GET.get('type', 'Posted')

            my_questions = self.object.user.question_set.order_by("-date_created")
            context["questions"] = my_questions

            questions_answered = my_questions.filter(answers__isnull=False).distinct()
            if questions_answered:
                paginator = Paginator(questions_answered, 5)
                context["questions_answered"] = paginator.page(page) if type == "Answered" and int(page) <= paginator.num_pages else paginator.page(1)
            else:
                context["questions_answered"] = []

            # context["questions_answered"] = my_questions.filter(answers__isnull=False).distinct()

            questions_posted = my_questions.filter(answers__isnull=True).distinct()
            if questions_posted:
                paginator = Paginator(questions_posted, 5)
                context["questions_posted"] = paginator.page(page) if type == "Posted" and int(page) <= paginator.num_pages else paginator.page(1)
                # paginator = Paginator(questions_posted, 2)
            else:
                context["questions_posted"] = []

            context['educations'] = Education.objects.filter(user=self.object.user)
            context["education_form"] = StudentEducationForm({'hidden_pk':'-1','hidden_school':'-1',"school_type":"college/university",'hidden_major':'-1'})
            tab = self.request.GET.get("tab")

            if (self.request.GET.get('view_as') and self.request.GET.get(
                    'view_as') == "public" and self.object.user.id == self.request.user.id) or (
                self.object.user.id != self.request.user.id):
                if not "tab" in context:
                    context["tab"] = tab = "profile"
            else:
                context['tab'] = tab

            country_name = ''
            tzm = TimezoneMapper.objects.filter(tz=self.request.user.champuser.timezone)
            if tzm.exists():
                tzm = tzm.first()
                country_name = tzm.country.name
            currency_code = CurrencyUtil.get_currency_code_for_country(country_name)
            job_post_form_initial = {}
            job_post_form_data = {
                "currency": currency_code if currency_code in settings.BASE_CURRENCIES else "USD"
            }
            job_post_form_data["currency_list"] = CurrencyUtil.get_currency_list()
            user_currency = currency_code
            conversion_enabled = not(user_currency in settings.BASE_CURRENCIES)
            base_currency = currency_code if currency_code in settings.BASE_CURRENCIES else "USD"
            job_post_form_data["base_currency"] = base_currency
            job_post_form_data["conversion_enabled"] = conversion_enabled
            job_post_form_data["target_currency"] = user_currency if conversion_enabled else "USD"

            # profile_objects = Profile.objects.filter(user_id=self.object.user.pk)
            #
            # context["profile_summary"] = profile_objects.first().about if profile_objects.exists() else None

            subjet_level_name = SubjectUtil.get_levels_for_country(self.object.nationality.name)
            subjet_levels = SubjectLevel.objects.filter(name__in=subjet_level_name)
            context["tab"] = tab
            job_post_form_kwargs = {}
            if tab == "job-post":
                if self.request.GET.get("job-id"):
                    try:
                        job_id = int(self.request.GET.get("job-id"))
                        job = Job.objects.get(pk=job_id)
                        job_post_form_data["job"] = job
                        job_post_form_data["payer"] = job.payer
                        job_post_form_data['currency'] = job.preference.currency

                        job_post_form_initial["title"] = job.title
                        job_post_form_initial["description"] = job.description
                        job_post_form_initial["rate"] = job.preference.rate.rate_value
                        job_post_form_initial["rate2"] = job.preference.rate.rate_value2
                        job_post_form_initial["currency"] = job.preference.currency
                        job_post_form_initial["gender"] = job.preference.gender
                        job_post_form_initial["level"] = job.preference.level.pk
                        job_post_form_initial["duration"] = job.lesson_duration
                        job_post_form_initial["lesson_repeat"] = job.lesson_repeat
                        job_post_availability = "any"
                        if job.preference.availability == 2:
                            job_post_availability = "ready_to_teach"
                        elif job.preference.availability == 3:
                            job_post_availability = "written_lesson"
                        job_post_form_initial["availability"] = job_post_availability

                        lesson_start_datetime =  Clock.utc_timestamp_to_local_datetime(job.lesson_time, self.object.timezone)
                        lesson_start_date = lesson_start_datetime.date()
                        lesson_start_time_hour = ("0"+str(lesson_start_datetime.hour) if lesson_start_datetime.hour <= 9 and lesson_start_datetime.hour >= 0 else str(lesson_start_datetime.hour))
                        lesson_start_time_min = ("0"+str(lesson_start_datetime.minute) if lesson_start_datetime.minute >= 0 and lesson_start_datetime.minute <= 9 else str(lesson_start_datetime.minute))
                        lesson_start_time = lesson_start_time_hour + ":" + lesson_start_time_min

                        job_post_form_initial["lesson_start_date"] = lesson_start_date.strftime("%m/%d/%Y")
                        job_post_form_initial["job_end_date"] = Clock.utc_timestamp_to_local_datetime(job.end_date, self.object.timezone).date().strftime("%m/%d/%Y")

                        job_post_form_data["nationalities"] = job.preference.nationalities.all()

                        job_post_form_kwargs["subject_list"] = Major.objects.filter(level_id=job.preference.level.pk)

                        job_post_form_data["subjects"] = job.preference.subjects.all()
                        job_post_form_data["languages"] = job.preference.languages.all()
                        job_post_form_data["timezones"] = job.preference.timezones.all()
                        job_post_form_data["lesson_type"] = job.lesson_type
                        job_post_form_data["payer"] = job.payer
                        job_post_form_data["other_students"] = job.other_students.all()
                        job_post_form_data["lesson_start_time"] = lesson_start_time

                    except Exception as exp:
                        pass
            elif tab == "my-jobs":
                context["tab"] = "my-jobs"
                start_date = self.request.GET.get("start")
                end_date = self.request.GET.get("end")
                filter_ = self.request.GET.get("filter")
                try:
                    if start_date:
                        context["start_date"] = datetime.strptime(start_date, "%Y-%m-%d").date().strftime("%Y-%m-%d")
                except Exception as exp:
                    context["start_date"] = None

                try:
                    if end_date:
                        context["end_date"] = datetime.strptime(end_date, "%Y-%m-%d").date().strftime("%Y-%m-%d")
                except Exception as exp:
                    context["end_date"] = None

                if filter_:
                    context["filter"] = filter_

            elif tab == "my-tutors":
                context["tab"] = "my-tutors"
            elif tab == "profile":
                context["tab"] = "profile"
            elif tab == "post-question":
                context["tab"] = "post-question"
                if self.request.GET.get("question-id"):
                    try:
                        question_id = self.request.GET["question-id"]
                        question_object = Question.objects.get(pk=int(question_id))
                        context["question_object"] = question_object
                        context["qtags"] = question_object.tags.all()
                        context["qattachments"] = question_object.attached_file.all()
                    except Exception as exp:
                        pass
            elif tab == "my-questions":
                context["tab"] = "my-questions"
            # tz_mapper = TimezoneMapper.objects.filter(country__type="SupportedCountry", country_id=self.object.nationality.pk, tz=self.object.timezone)
            # job_post_form_data["timezones"] = tz_mapper
            job_post_form_kwargs["subjet_levels"] = subjet_levels
            job_post_form_data["job_post_form"] = JobPostForm(initial=job_post_form_initial, **job_post_form_kwargs)
            template = loader.get_template("forms/job_post_form.html")
            cntxt = RequestContext(self.request)
            cntxt.update(job_post_form_data)
            rendered = template.render(cntxt)
            context["job_post_form"] = rendered
        else:
            # context["profile_summary"] = self.get_profile_summary()
            context["job_invitation_count"] = JobInvitation.objects.filter(user=self.object.user,status=JobInvitationStatus.active.value).exclude(job__status=4).count()
            recommended_jobs = JobUtil.recommended_jobs_for_user(self.object.user.pk)
            context["job_recommendation_count"] = recommended_jobs.filter(~Q(applications__tutor__pk=self.object.user.pk)).count()
            context["job_application_count"] = JobApplication.objects.filter(tutor_id=self.object.user.pk,status=JobApplicationStatus.active.value).exclude(job__status=4).count()
            view_mode = self.request.GET.get("view-mode")
            if view_mode == "profile":
                context["section"] = "profile"
            else:
                context["section"] = "feed"

            job_data = []
            if self.request.user.is_authenticated():
                questions = Question.objects.filter(status=QuestionStatus.open.value)
                questions = questions.filter(tags__name__in=[user_major.major.name for user_major in UserMajor.objects.filter(user_id=self.object.user.pk)])
                paginator = Paginator(questions,8)
                q = questions[:8]
                for question in q:
                    template = loader.get_template("ajax/job_feed_item.html")
                    cntxt = Context({ 'question': question ,"request": self.request})
                    rendered = template.render(cntxt)
                    job_data += [rendered]

                context["job_data"] = job_data
                context["feed_page_count"] = paginator.num_pages
            else:
                context["job_data"] = []
                context["feed_page_count"] = 0

            context["action_tab"] = "all"
            question_feed_list = Question.objects.all().order_by("-date_created")
            major_list = [user_major.major.name for user_major in UserMajor.objects.filter(user_id=self.object.user.pk)]
            question_feed_list = question_feed_list.filter(tags__name__in=major_list).distinct()
            q_all = question_feed_list
            paginator = Paginator(question_feed_list,8)
            page = paginator.page(1)

            current_tab = self.request.GET.get('tab','applications')

            if ( self.request.GET.get('view_as') and self.request.GET.get(
                    'view_as') == "public" and self.object.user.id == self.request.user.id ) or ( self.object.user.id != self.request.user.id ):
                if not "selected_tab" in context:
                    context["selected_tab"] = current_tab = "profile"
            else:
                context['selected_tab'] = current_tab = current_tab
            if current_tab == 'profile':
                context['user_id'] = self.object.user.pk
            def vimeo_video_details(video_id):
                try:
                    import requests, json
                    req = requests.get('http://vimeo.com/api/v2/video/%s.json' % video_id)
                    if req.status_code == 200:
                        json_content = json.loads(req.content)
                        if json_content:
                            json_content = json_content[0]
                            return { 'duration': json_content['duration'], 'thumbnail': json_content['thumbnail_medium'] }
                    return {}
                except Exception as exp:
                    return {}

            def fetch_missed_video_details():
                videos = ProfileMyVideo.objects.filter(user_id=self.object.user.pk)
                for video in videos:
                    if video.host_type == "VIMEO" and not video.video_length:
                        details = vimeo_video_details(video.url.replace("videos", "").replace("/", ""))
                        if details.get('duration'):
                            hour,minute, seconds = 0, 0, 0
                            duration = int(details.get('duration'))
                            hour = duration / 3600
                            remaining_seconds = duration % 3600
                            minute = remaining_seconds / 60
                            seconds = remaining_seconds % 60
                            duration_str = ""
                            if hour:
                                duration_str += "%sh " % hour
                            if minute:
                                duration_str += "%sm " % minute
                            else:
                                duration_str += "0m "
                            if seconds:
                                duration_str += "%ss " % seconds

                            video.video_length = duration_str
                            video.save()


            fetch_missed_video_details()

            context["languages"] = Language.objects.all().exclude(pk__in=[lp.language.pk for lp in self.object.languages.all()])

            context["my_videos"] = ProfileMyVideo.objects.filter(user_id=self.object.user.pk)


            if (self.request.GET.get('view_as') and self.request.GET.get('view_as') == "public" and self.object.user.id == self.request.user.id) or self.object.user.pk != self.request.user.pk:
                my_lessons = LessonRequest.objects.filter(request_user_id=self.object.user.pk,review__isnull=False)
                my_review_data = []
                for lesson in my_lessons:
                    template = loader.get_template("ajax/my_review_data_item.html")
                    cntxt = Context({ 'lesson': lesson ,"request": self.request})
                    rendered = template.render(cntxt)
                    my_review_data += [rendered]
                context["champuser"] = self.object
                context["my_reviews"] = my_review_data

        #context['view_as'] = self.request.GET.get("private")
        if self.request.GET.get('view_as') and self.request.GET.get('view_as') == "public" and self.object.user.id == self.request.user.id:
            context['content_editable'] = False
            context['view_as'] = self.request.GET.get("view_as")

            if not self.object.is_tutor:
                context['tab'] = 'profile' if not 'tab' in context else context['tab']
            else:
                context['selected_tab'] = 'profile' if not 'selected_tab' in context else context['selected_tab']

        if not context['content_editable']:
            tmplt = "sections/about_content.html"
            pcontext = self.get_profile_context(self.object.pk)
            template = loader.get_template(tmplt)

            cntxt = Context(pcontext)
            rendered = template.render(cntxt)
            context["profile_content"] = rendered

            timezones = Clock.get_all_timezones()
            context["timezones"] = timezones

        if context['content_editable']:
            context["languages"] = Language.objects.all().exclude(pk__in=[lp.language.pk for lp in self.object.languages.all()])

        return context

class CalendarView(LoginRequiredMixin, View):
    def get(self,request,*args,**kwargs):
        context = {
            "events_url": "/calendar-events/json/",
            "view": "",
            "language": ""
        }
        return render(request,"calendar.html",context)

class FullCalendarView(LoginRequiredMixin, JSONResponseMixin, View):
    def get(self, request, *args,**kwargs):
        if request.user.champuser.is_tutor:
            lessons = LessonRequest.objects.select_related().filter(request_user_id=request.user.pk)
        else:
            lessons = LessonRequest.objects.select_related().filter(Q(user_id=request.user.pk)|
                                                                    Q(other_users__id__in=[request.user.pk]))
        tz = request.user.champuser.timezone
        now = datetime.utcnow()
        day = request.GET.get('day')
        if day:
            now = datetime.strptime(day, '%B %d, %Y')

        day_cal = cal_utils.day_calendar(now.date(), lessons, tz, table="week")  # Use week table since it has same formatting
        day_cal_header = now.strftime("%A, %d %B %Y")
        day_next = now.date() + timedelta(days=1)
        day_prev = now.date() - timedelta(days=1)

        week = cal_utils.week_range(now.date())
        week_cal = cal_utils.week_calendar(week, lessons, tz, table="week")
        week_cal_start_date = week[0].strftime("%d %B %Y")
        week_cal_last_date = week[-1].strftime("%d %B %Y")
        week_next = now.date() + timedelta(days=7)
        week_prev = now.date() - timedelta(days=7)

        month_cal_year, month_cal_month = now.year, now.month
        month_cal = cal_utils.MonthCalendar(lessons, tz, table="month").formatmonth(month_cal_year,
                                                                                    month_cal_month)  # The current month
        month_next = now + dt_util.relativedelta(months=1)
        month_prev = now - dt_util.relativedelta(months=1)

        year = now.year
        year_cal = cal_utils.MonthCalendar(lessons, tz, table="year").formatyear(year)
        year_next = now + dt_util.relativedelta(years=1)
        year_prev = now - dt_util.relativedelta(years=1)

        context = {
            'day_calendar': mark_safe(day_cal),
            'week_calendar': mark_safe(week_cal),
            'month_calendar': mark_safe(month_cal),
            'year_calendar': mark_safe(year_cal),

            'day_cal_header': day_cal_header,
            'week_cal_start_date': week_cal_start_date,
            'week_cal_last_date': week_cal_last_date,
            'year': year,

            'day_next': day_next.strftime('%B %d, %Y'),
            'day_prev': day_prev.strftime('%B %d, %Y'),
            'week_prev': week_prev.strftime('%B %d, %Y'),
            'week_next': week_next.strftime('%B %d, %Y'),
            'month_next': month_next.strftime('%B %d, %Y'),
            'month_prev': month_prev.strftime('%B %d, %Y'),
            'year_next': year_next.strftime('%B %d, %Y'),
            'year_prev': year_prev.strftime('%B %d, %Y')

        }
        if request.is_ajax():
            return self.render_to_json(context)

        return render(request, "full_calendar.html", context)

class ProfileAboutView(LoginRequiredMixin, View):
    def get(self,request,*args,**kwargs):
        context = {
            "section": "about",
            "champ_user": ChampUser.objects.get(user_id=request.user.pk),
            "about_data": Profile.objects.get(user=request.user).about,
            "content_editable": True if int(kwargs.get("pk")) == request.user.id else False
        }
        context["buddies"] = get_buddies_online(request)
        return render(request,"tutor_profile.html",context)

class SecureProfileView(LoginRequiredMixin, JSONResponseMixin, TemplateView):
    template_name = "security_question.html"

    def get_context_data(self, **kwargs):
        context = super(SecureProfileView, self).get_context_data(**kwargs)
        context["security_questions"] = SecurityQuestion.objects.all()
        security_question = UserSecurityQuestion.objects.filter(user__id=self.request.user.pk)
        context["security_question"] = security_question.first() if security_question.exists() else None
        return context

    def get(self, request, *args, **kwargs):
        self.request = request
        context = self.get_context_data(**kwargs)
        return super(SecureProfileView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return HttpResponseRedirect(reverse("user_login"))
        sq1 = request.POST.get("security_question1")
        sq_ans1 = request.POST.get("sq_answer1")
        sq_password = request.POST.get("sq_password")

        try:
            sq1 = int(sq1)
        except:
            response = {
                "status": "FAILURE",
                "message": "Security question update failed",
                "data": []
            }
            return self.render_to_json(response)

        user = authenticate(username=request.user.username, password=sq_password)
        if user is not None:
            if user.is_active:
                uss = UserSecurityQuestion.objects.filter(question__id=sq1, user__id=request.user.pk)
                if uss.exists():
                    uss = uss.first()
                    uss.answer = sq_ans1
                    uss.save()
                else:
                    UserSecurityQuestion.objects.filter(user__id=request.user.pk).delete()
                    uss = UserSecurityQuestion()
                    uss.user_id = request.user.pk
                    uss.question_id = sq1
                    uss.answer = sq_ans1
                    uss.save()
                response = {
                    "status": "SUCCESS",
                    "message": "Security question has been updated successfully",
                    "data": []
                }
                return self.render_to_json(response)
            else:
                response = {
                    "status": "FAILURE",
                    "message": "Login is required",
                    "data": []
                }
                return self.render_to_json(response)
        else:
            response = {
                "status": "FAILURE",
                "message": "Login is required",
                "data": []
            }
            return self.render_to_json(response)


class JobInvitationView(LoginRequiredMixin, View):
    def get(self,request,*args,**kwargs):
        champ_user = ChampUser.objects.get(user_id=request.user.pk)
        context = {
            "section": "job_invitation",
            "champ_user": champ_user,
            "object": champ_user,
            "about_data": Profile.objects.filter(user=request.user).first().about,
            "content_editable": True if int(kwargs.get("pk")) == request.user.id else False
        }
        context["buddies"] = get_buddies_online(request)

        job_invitations = JobInvitation.objects.filter(user=request.user,status=JobInvitationStatus.active.value)
        job_invitations_data = []
        for job_invitation in job_invitations:
            template = loader.get_template("ajax/job_invitation_item.html")
            cntxt = Context({ 'job_invitation': job_invitation ,"request": request})
            rendered = template.render(cntxt)
            job_invitations_data += [rendered]

        context["job_invitation_data"] = job_invitations_data

        return render(request,"profile.html",context)

class JobCreateView(LoginRequiredMixin,PermissionRequiredMixin, View):
    def get(self,request,*args,**kwargs):
        context = {}
        context["job_post_form"] = JobPostForm()
        return render(request,"job_create.html",context)
    def post(self,request,*args,**kwargs):
        job_id = request.POST.get("job_id")
        lesson_post_type = request.POST.get("job_post_lesson_type")
        title = request.POST.get("title")
        description = request.POST.get("description")
        nationality_list = request.POST.getlist("nationality-list[]")  ###ids
        gender = request.POST.get("gender")
        rate = request.POST.get("rate")
        rate2 = request.POST.get("rate2")
        currency = request.POST.get("currency")
        timezone_list = request.POST.getlist("timezone-list[]")
        language_list = request.POST.getlist("languages-list[]") ###ids
        subject_list = request.POST.getlist("subject-list[]") ###ids
        study_partners = request.POST.getlist("study_partners-list[]")
        payer = request.POST.get("payer")
        availability = request.POST.get("availability")
        end_date = request.POST.get("job_end_date")
        job_start_date = request.POST.get("lesson_start_date")
        lesson_duration = request.POST.get("duration")
        lesson_repeat = request.POST.get("lesson_repeat")
        lesson_time = request.POST.get("lesson_time")
        level = request.POST.get("level")

        with transaction.atomic():
            job = Job()
            job_id = int(job_id)

            if job_id != -1:
                job = Job.objects.get(pk=job_id)

            if lesson_post_type == "online":
                job.lesson_type = LessonTypes.live_lesson.value
            else:
                job.lesson_type = LessonTypes.written.value

            response_url = reverse("user_profile", kwargs={ "pk": self.request.user.pk })+"?tab=job-post"
            if job_id != -1:
                response_url = reverse("job_post_edit", kwargs={ "pk": job_id })

            if not title:
                messages.error(request,"Job title missing")
                return HttpResponseRedirect(response_url)

            if not description:
                messages.error(request,"Job description is missing")
                return HttpResponseRedirect(response_url)

            try:
                job_start_datetime = job_start_date + " " + lesson_time
                # job_start_datetime = datetime.strptime(job_start_datetime,"%m/%d/%Y %H:%M")
            except:
                messages.error(request,"Invalid start date given")
                return HttpResponseRedirect(response_url)

            if not currency:
                messages.error(request,"Currency is missing")
                return HttpResponseRedirect(response_url)

            # try:
            #     job_start_time = datetime.strptime(job_start_time,"%I:%M %p")
            # except:
            #     messages.error(request,"Invalid start time given")
            #     return HttpResponseRedirect(response_url)

            try:
                Decimal(rate)
                if rate2:
                    Decimal(rate2)
            except Exception as msg:
                print(msg)
                messages.error(request,"Please enter a valid rate")
                return HttpResponseRedirect(response_url)

            try:
                level = int(level)
                level_object = SubjectLevel.objects.get(pk=level)
            except Exception as msg:
                level_object = None
                messages.error(request,"Please enter a valid rate")
                return HttpResponseRedirect(response_url)

            try:
                current_user_tz = request.user.champuser.timezone

                utc_dt = Clock.local_to_utc_datetime(job_start_datetime, "%m/%d/%Y %H:%M", current_user_tz)

                lesson_timestamp = time.mktime(utc_dt.timetuple())

            except Exception as msg:
                lesson_timestamp = None

            try:
                current_user_tz = request.user.champuser.timezone

                utc_end_dt = Clock.local_to_utc_datetime(end_date, "%m/%d/%Y", current_user_tz)

                end_utc_timestamp = time.mktime(utc_end_dt.timetuple())

            except Exception as msg:
                end_utc_timestamp = None

            duration = int(lesson_duration)

            if not lesson_timestamp:
                messages.error(request,"A valid lesson start date time is required.")
                return HttpResponseRedirect(response_url)

            if not end_utc_timestamp:
                messages.error(request, "A valid lesson end date is required.")
                return HttpResponseRedirect(response_url)

            job.title = title
            job.description = description
            job.posted_by = request.user
            job.status = JobStatus.job_open.value
            job.lesson_time = lesson_timestamp
            job.lesson_duration = duration

            job.end_date = end_utc_timestamp
            job.payer_id = int(payer)
            job.lesson_repeat = int(lesson_repeat)

            job.save()

            job.job_attachment.clear()

            for file_name,content in request.FILES.items():
                if file_name.startswith("written_file") and len(file_name) > len("written_file"):
                    file = content
                    original_file_name = file._name
                    file_name = str(request.user.id)+"-"+str(uuid.uuid4())+"."+file._name[file._name.rindex(".")+1:]
                    with open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name), 'wb+') as destination:
                        for chunk in file.chunks():
                            destination.write(chunk)
                    file = File(open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name)))
                    job_attachment = JobAttachment()
                    job_attachment.attachment_name = original_file_name
                    job_attachment.attachment = file
                    job_attachment.save()
                    job.job_attachment.add(job_attachment)

            ###Now add job preference.
            job_preference = JobPreference()
            if job_id != -1:
                job_preference = job.preference

            if availability == "ready_to_teach":
                availability = 2
            elif availability == "written_lesson":
                availability = 3
            elif availability == "any":
                availability = 5
            else:
                raise Exception("Invalid available status.")

            job_preference.availability = availability

            job_preference.level_id = level_object.pk

            job_preference.save()

            nationality_list = [int(nid) for nid in nationality_list]
            country_objects = Country.objects.filter(pk__in=nationality_list)
            job_preference.nationalities.clear()
            job_preference.nationalities.add(*country_objects)

            hourly_rate_unit = None
            if rate.strip():
                hourly_rate_unit = HourlyRateUnit()
                hourly_rate_unit.currency = currency
                hourly_rate_unit.rate_value = Decimal(rate)
                if rate2:
                    hourly_rate_unit.rate_value2 = Decimal(rate2)
                hourly_rate_unit.save()

            if hourly_rate_unit:
                job_preference.rate = hourly_rate_unit

            job_preference.gender = gender.strip()

            # job_preference.timezones.clear()
            #
            # tzm_objects = []
            # timezone_list = [ int(tz_id) for tz_id in timezone_list ]
            # job_preference.timezones.clear()
            # for tz in timezone_list:
            #     tzm = TimezoneMapper.objects.get(pk=tz)
            #     tzm_objects += [ tzm ]
            # job_preference.timezones.add(*tzm_objects)

            job_preference.timezone_diffs.clear()

            tzm_objects = []
            timezone_list = [ int(tz_id) for tz_id in timezone_list ]
            for tzm in timezone_list:
                tzm = TimezoneDifference.objects.get(pk=tzm)
                tzm_objects += [ tzm ]

            job_preference.timezone_diffs.add(*tzm_objects)

            job_preference.currency = currency

            job_preference.languages.clear()

            language_list = [ int(lid) for lid in language_list ]

            language_objects = Language.objects.filter(pk__in=language_list)

            job_preference.languages.add(*language_objects)

            job_preference.subjects.clear()

            subject_objects = Major.objects.filter(name__in=subject_list,level_id=level_object.pk)

            job_preference.subjects.add(*subject_objects)

            job_preference.save()

            job.preference = job_preference

            job.save()

            job.other_students.clear()

            for study_partner in study_partners:
                partner = int(study_partner)
                job.other_students.add(User.objects.get(pk=partner))

            return HttpResponseRedirect(reverse("user_profile", kwargs={ "pk": self.request.user.pk })+"?tab=my-jobs")

        return HttpResponseRedirect(reverse("user_profile", kwargs={ "pk": self.request.user.pk })+"?tab=my-jobs")

class JobFeedView(LoginRequiredMixin, ListView):
    template_name = "job_feed.html"
    model = Job
    paginate_by = 1
    context_object_name = "job_post_list"

    def get_queryset(self):
        job_objects = Job.objects.filter(posted_by=self.request.user).order_by('-date_modified')
        return job_objects

    def get_context_data(self, **kwargs):
        #context = super(JobFeedView, self).get_context_data(**kwargs)
        context = {}
        job_objects = Job.objects.filter(posted_by=self.request.user).order_by('-date_modified')

        if self.request.GET.get("search") == "1":
            start_date = self.request.GET.get('start_date')
            end_date = self.request.GET.get('end_date')
            view = self.request.GET.get('view')

            try:
                start_dt = datetime.strptime(start_date,"%d/%m/%Y")
            except:
                start_dt = None
                messages.add_message(self.request, messages.ERROR, "Invalid start date given")

            try:
                end_dt = datetime.strptime(end_date,"%d/%m/%Y")
            except:
                end_dt = None
                messages.add_message(self.request, messages.ERROR, "Invalid end date given")

            if start_dt and end_dt:
                start_ts = Clock.convert_datetime_to_timestamp(start_dt)
                end_ts = Clock.convert_datetime_to_timestamp(end_dt)

                job_objects = job_objects.filter(date_created__gte=start_ts, date_created__lte=end_ts)

            if view and view != 'all' and view != 'active' and view != 'archived':
                messages.add_message(self.request, messages.ERROR, "Invalid view filter selected")
            else:
                now_ts = Clock.utc_timestamp()
                if view == 'active':
                    job_objects = job_objects.filter(end_date__gte=now_ts)
                elif view == 'archived':
                    job_objects = job_objects.filter(end_date__lt=now_ts)

            if not start_dt or not end_dt:
                job_objects = Job.objects.none()

            if view and view != 'all' and view != 'active' and view != 'archived':
                job_objects = Job.objects.none()

            context['start_date'] = start_date
            context['end_date'] = end_date
            context['view'] = view

        context["job_post_list"] = job_objects
        return context

class LessonPostDetails(LoginRequiredMixin,JSONResponseMixin, View):
    def get(self,request,*args,**kwargs):
        job = Job.objects.filter(pk=int(kwargs.get("pk")))

        if job.first().is_deleted:
            return render(request,"404.html",{})

        if request.GET.get("format") == "json":
            if job.exists():
                job = job.first()
                response = {
                    "status": "SUCCESS",
                    "message": "Successful",
                    "data": {
                        "title": job.title,
                        "job_type": "Live Lesson" if job.lesson_type == 0 else "Written Lesson",
                        "currency": job.preference.currency,
                        "rate": job.preference.rate.rate_value,
                        "start_date": job.lesson_time,
                        "duration": job.lesson_duration,
                        "description": job.description[:500]+"..." if len(job.description) >= 500 else job.description,
                        "job_url": reverse("job_post_details",kwargs={ "pk": kwargs.get("pk") }),
                        "students_url": reverse("user_profile",kwargs={ "pk": job.posted_by.pk })
                    }
                }
                return self.render_to_json(response)

        elif request.GET.get('format') == "lt": ###lt means load template
            template_name = "ajax/job_summary_details.html"
            template = loader.get_template(template_name)
            context = {}

            ###Check if student has any open jobs.
            context['job'] = job.first()

            cntxt = Context(context)
            rendered = template.render(cntxt)
            response = {
                "status": "SUCCESS",
                "message": "Successful",
                "data": {
                    "job_summary_content": rendered
                }
            }
            return self.render_to_json(response)

        job = Job.objects.get(pk=kwargs.get("pk"))
        context = {
            "object": job
        }

        template_name = 'marketplace_details.html'
        if request.user.pk == context['object'].posted_by.pk:
            context = {
                "job": job
            }
            template_name = "job_details.html"

        ###Check if he is invited or recommended.
        invite_type = None
        show_action_bar = False
        status = None
        application_url = None
        show_public_apply = True
        if job.posted_by.pk != request.user.pk:
            ji_objects = JobInvitation.objects.filter(job_id=kwargs.get("pk"),user_id=request.user.pk)
            jr_objects = JobRecommendation.objects.filter(job_id=kwargs.get("pk"),user_id=request.user.pk)
            if ji_objects.exists():
                invite_type = "INVITED"
                show_action_bar = True
                ji_object = ji_objects.first()
                status = ji_object.status

            else:
                job_recommended = JobUtil.check_if_job_is_recommended_for_user(self.request.user.pk, kwargs.get("pk"))
                if job_recommended:
                    invite_type = "RECOMMENDED"
                    status = 1 ###Pending
                    applied = Job.objects.filter(pk=kwargs.get('pk'), applications__tutor__pk=self.request.user.pk).exists()
                    if applied:
                        job_application = Job.objects.get(pk=kwargs.get('pk')).applications.filter(tutor_id=self.request.user.pk).first()
                        status = job_application.status

        # if JobApplication.objects.filter(job__id=kwargs.get("pk"), tutor_id=self.request.user.pk, status=JobRecommendationStatus.rejected_application.value).exists():
        #     show_public_apply = False

        if invite_type != "INVITED" and invite_type != "RECOMMENDED":
            applications = job.applications.filter(tutor_id=self.request.user.pk)
            if applications.exists():
                application = applications.first()
                status = application.status

        context["invite_type"] = invite_type
        context["show_action_bar"] = show_action_bar
        context["show_public_apply"] = show_public_apply
        context["status"] = status
        context["pending_count"] = job.applications.filter(status=2).count()
        context["invited_count"] = JobInvitation.objects.filter(job_id=job.pk,status=1).count()
        context["rejected_count"] = JobInvitation.objects.filter(Q(job_id=job.pk) & (Q(status=5) | Q(status=10) | Q(status=12))).count()
        context["shortlisted_count"] = JobInvitation.objects.filter(job_id=job.pk, status=9).count()
        context["hired_count"] = JobInvitation.objects.filter(job_id=job.pk,status=8).count()
        context["application_url"] = application_url

        price = job.preference.rate.rate_value
        price2 = job.preference.rate.rate_value2
        currency = job.preference.currency
        lesson_duration = job.lesson_duration
        start_date = job.lesson_time

        converted_currency = None
        converted_min_value = None
        converted_max_value = None
        applied = False

        country_name = ''
        tzm = TimezoneMapper.objects.filter(tz=request.user.champuser.timezone)
        if tzm.exists():
            tzm = tzm.first()
            country_name = tzm.country.name
        user_currency = CurrencyUtil.get_currency_code_for_country(country_name)

        if user_currency != currency:
            converted_currency = user_currency

            converted_min_value = RateConversionUtil.convert(currency, user_currency, price)
            converted_max_value = RateConversionUtil.convert(currency, user_currency, price2)

        job_applications = JobApplication.objects.filter(job__id=int(kwargs.get("pk")), tutor_id=request.user.pk)
        if job_applications.exists():
            job_application = job_applications.first()
            currency = job_application.settlement.currency
            price = job_application.settlement.price
            price2 = job_application.settlement.price2
            lesson_duration = job_application.duration
            start_date = job_application.start_time
            applied = True

        context['currency'] = currency
        context['price'] = price
        context['price2'] = price2
        context['lesson_duration'] = lesson_duration
        context['start_date'] = start_date
        context['converted_currency'] = converted_currency
        context['converted_min_value'] = converted_min_value
        context['converted_max_value'] = converted_max_value

        return render(request,template_name,context)

class JobOfferDetailsView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        template_name = 'job_offer_details.html'
        context = {}
        job_id = int(kwargs.get('job_id'))
        user_id = int(kwargs.get('user_id'))
        job = Job.objects.filter(pk=job_id)
        user = User.objects.filter(pk=user_id)
        if job.exists() and user.exists():
            job = job.first()
            user = user.first()

            application = job.applications.filter(tutor_id=user_id)
            if application.exists():
                application = application.first()

                job_offer = JobOffer.objects.filter(application_id=application.pk)
                if job_offer.exists():
                    job_offer = job_offer.first()

                    context['job'] = job
                    context['offer'] = job_offer
                    context['show_offer_actions'] = (user_id == request.user.pk)
                    # if user_id == request.user.pk:
                    #     context['show_offer_actions'] = ''

        return render(request, template_name, context )

    def post(self, request, *args, **kwargs):
        job_id = request.POST.get('job_id')
        offer_id = request.POST.get('offer_id')
        user_id = request.POST.get('user_id')

        job_id = int(job_id)
        offer_id = int(offer_id)
        try:
            user_id = int(user_id)
        except:
            user_id = request.user.pk
        ji_objects = JobInvitation.objects.filter(job_id=job_id,user_id=user_id)
        jr_objects = JobRecommendation.objects.filter(job_id=job_id,user_id=user_id)
        if ji_objects.exists():
            ji_object = ji_objects.first()
            ji_object.status = JobInvitationStatus.hired.value
            ji_object.save()

        elif jr_objects.exists():
            jr_object = jr_objects.first()
            jr_object.status = JobRecommendationStatus.hired.value
            jr_object.save()

        job = Job.objects.get(pk=job_id)
        applications = job.applications.filter(tutor_id=user_id)
        if applications.exists():
            application = applications.first()
            application.status = JobApplicationStatus.hired.value
            application.save()

        job_offer = JobOffer.objects.get(pk=offer_id)
        job_offer.status = JobOfferStatus.hired.value
        job_offer.save()

        ###Now create live lesson or written lesson
        job_type = job.lesson_type  ###0 for live lesson and 1 for written lesson.
        if job_type == 0: ###Live Lesson
            lesson_util = LessonUtil()
            ostudents = job.other_students.all()
            _temp = []
            for o_s in ostudents:
                _temp += [ o_s.pk ]
            other_students = _temp
            subject_id = None
            subject = Major.objects.filter(name=job_offer.subject)
            if subject.exists():
                subject_id = subject.first().pk
            if subject_id:
                repeat = "NO_REPEAT"
                if job_offer.repeat == 1:
                    repeat = "NO_REPEAT"
                elif job_offer.repeat == 2:
                    repeat = "REPEAT_DAILY"
                elif job_offer.repeat == 3:
                    repeat = "REPEAT_WEEKLY"
                approval_status = 1 ###Approved
                success, lesson_id = lesson_util.create_live_lesson(job.title,job.posted_by_id, request.user.pk, other_students, job_offer.start_date, job_offer.duration, subject_id, repeat, approval_status)
                job.lesson_id = lesson_id
                job.save()

                ###Now schedule a payment.
                ###Check if lesson is to be scheduled within 30 minutes. If so then process the transaction now else schedule a transaction at 30 minutes before the lesson
                now_ts = Clock.timestampnow()
                live_lesson = LiveLesson.objects.get(pk=lesson_id)
                lesson_ts = live_lesson.lesson_time

                schedule_reminder_dt = datetime.fromtimestamp(lesson_ts-60*15)

                LessonReminder.schedule_reminder(lesson_id, schedule_reminder_dt)

                if now_ts < lesson_ts:
                    if lesson_ts - now_ts <= 1800:
                        ###Process payment immediately.
                        payment_processor = PaymentProcessor()
                        transaction_amount = (float(job_offer.duration) / 60.0) * job_offer.rate
                        payment_processor.process_lesson_payment(lesson_id,'LIVE_LESSON', int(transaction_amount), job_offer.rate_currency)
                    else:
                        payment_processor = PaymentProcessor()
                        transaction_amount = (float(job_offer.duration) / 60.0) * job_offer.rate
                        # payment_processor.process_lesson_payment(lesson_id,'WRITTEN_LESSON', transaction_amount)
                        scheduler = get_scheduler()
                        schedule_dt = datetime.fromtimestamp(lesson_ts-1800)
                        scheduler.enqueue_at(schedule_dt,payment_processor.process_lesson_payment,lesson_id,'LIVE_LESSON', int(transaction_amount), job_offer.rate_currency)

        else:
            subject_id = None
            subject = Major.objects.filter(name=job_offer.subject)
            if subject.exists():
                subject_id = subject.first().pk
            if subject_id:
                repeat = "NO_REPEAT"
                if job_offer.repeat == 1:
                    repeat = "NO_REPEAT"
                elif job_offer.repeat == 2:
                    repeat = "REPEAT_DAILY"
                elif job_offer.repeat == 3:
                    repeat = "REPEAT_WEEKLY"
                approval_status = 1 ###Approved
                lesson_util = LessonUtil()
                success, lesson_id = lesson_util.create_written_lesson(job.title,job.posted_by_id, request.user.pk, job_offer.start_date, job_offer.duration, subject_id, repeat, approval_status, job = job)
                job.lesson_id = lesson_id
                job.save()
                ###Now schedule a payment.
                payment_processor = PaymentProcessor()
                transaction_amount = (float(job_offer.duration) / 60.0) * job_offer.rate
                payment_processor.process_lesson_payment(lesson_id,'WRITTEN_LESSON', int(transaction_amount), job_offer.rate_currency)


        return HttpResponseRedirect(reverse("lessons_view"))


class JobEditView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        job = Job.objects.get(pk=kwargs.get("pk"))
        context = {}
        available_status = "ready_to_teach"
        if job.preference.availability == AvailableStatus.available.value:
            available_status = "available"
        elif job.preference.availability == AvailableStatus.any.value:
            available_status = "any"
        job_form_data = {
            "title": job.title,
            "description": job.description,
            "job_end_date": Clock.utc_timestamp_to_local_datetime(job.end_date, request.user.champuser.timezone).strftime("%d/%m/%Y"),
            "lesson_start_date": Clock.utc_timestamp_to_local_datetime(job.lesson_time, request.user.champuser.timezone).strftime("%d/%m/%Y %H:%M"),
            "duration": job.lesson_duration,
            "gender": job.preference.gender,
            "rate": job.preference.rate.rate_value,
            "lesson_repeat": job.lesson_repeat,
            "currency": job.preference.currency,
            "availability": available_status
        }
        context["job_post_form"] = JobPostForm(initial=job_form_data)
        context["job"] = job
        return render(request,"job_edit.html",context)

class JobDeleteView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        job_id = kwargs.get("pk")
        job_objects = Job.objects.filter(pk=int(job_id))
        if job_objects.exists():
            job_object = job_objects.first()
            job_object.status = 4 #Deleted
            job_object.save()


        url = reverse('user_profile', kwargs={ 'pk': request.user.pk })+"?tab=my-jobs"
        return HttpResponseRedirect(url)


class QuestionPostDetails(LoginRequiredMixin,View):
    def get(self,request,*args,**kwargs):
        champ_user = ChampUser.objects.get(user_id=request.user.pk)
        context = {
            "section": "q_details",
            "champ_user": champ_user,
            "object": champ_user,
            "about_data": Profile.objects.get(user=request.user).about,
            "question": Question.objects.get(pk=kwargs.get("pk"))
        }
        context["buddies"] = get_buddies_online(request)

        return render(request,"profile.html",context)

class QuestionAnswerPostView(LoginRequiredMixin,View):
    def post(self,request,*args,**kwargs):
        qid = request.POST.get("qid")
        qanswer = request.POST.get("qanswer")

        question = Question.objects.get(pk=int(qid))
        if not question.answers.filter(answer_by_id=request.user.pk).exists():
            question_answer = QuestionAnswer()
            question_answer.answer = qanswer
            question_answer.answer_by = request.user
            question_answer.save()

            for file_name,content in request.FILES.items():
                if file_name.startswith("attached_file") and len(file_name) > len("attached_file"):
                    file = content
                    original_file_name = file._name
                    file_name = str(request.user.id)+"-"+str(uuid.uuid4())+"."+file._name[file._name.rindex(".")+1:]
                    with open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name), 'wb+') as destination:
                        for chunk in file.chunks():
                            destination.write(chunk)
                    # file = File(open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name)))
                    question_attachment = QuestionAttachment()
                    question_attachment.attachment_name = original_file_name
                    question_attachment.attachment = "%s/%s" % (settings.MSG_ATTACHMENT_DIR_NAME, file_name)
                    question_attachment.save()
                    question_answer.attachment.add(question_attachment)

            question.answers.add(question_answer)

            # target_users = [question.created_by.pk]
            # for answer in question.answers.all():
            #     if answer.answer_by.pk != request.user.pk and answer.answer_by.pk not in target_users:
            #         target_users += [answer.answer_by.pk]
            #     for like in answer.likes.all():
            #         if like.created_by.pk != request.user.pk and like.created_by.pk not in target_users:
            #             target_users += [like.created_by.pk]
            target_users = [question.created_by]
            for answer in question.answers.all():
                if answer.answer_by != request.user and answer.answer_by not in target_users:
                    target_users += [answer.answer_by]
                for like in answer.likes.all():
                    if like.created_by != request.user and like.created_by not in target_users:
                        target_users += [like.created_by]

            for target_user in target_users:
                NotificationManager.push_notification("question_reply", question.pk, request.user, target_user.pk, answer=question_answer)
                send_email_thread(target_user, question, answer.pk, None)
                # html, text = EmailTemplateUtil.render_question_thread_email(target_user, question, None)
                # email_subject = "How to solve this PSLE question? - CT Online"
                # email_object = {
                #     "recipients": ["ihtram.01@gmail.com"],
                #     'subject': email_subject,
                #     'html_body': html,
                #     'text_body': text
                # }
                # EmailScheduler.place_to_queue(email_object)

        return HttpResponseRedirect(reverse("question_details_view",kwargs={"pk": int(qid)}))


def send_email_thread(target_user, question, answer_id, comment_id):
    html, text = EmailTemplateUtil.render_question_thread_email(target_user, question, answer_id, comment_id)
    email_subject = "How to solve this PSLE question? - CT Online"
    email_object = {
        "recipients": [target_user.email],
        'subject': email_subject,
        'html_body': html,
        'text_body': text
        }
    EmailScheduler.place_to_queue(email_object)


class QuestionAnswerEditView(LoginRequiredMixin, View):
    def post(self,request,*args,**kwargs):
        qid = request.POST.get("qid")
        aid = request.POST.get("aid")
        answer = request.POST.get("answer")
        question_answer = QuestionAnswer.objects.get(pk=int(aid))
        if answer:
            question_answer.answer = answer
            question_answer.answer_by = request.user
            question_answer.save()
        data = {
            'text': answer,
            'date_modified': format_datetime(
                convert_utc_timestamp_to_local_datetime(int(question_answer.date_modified), request)),
        }
        response = {'status': 'successful', 'data': data}

        question = Question.objects.get(pk=int(qid))
        target_users = [question.created_by.pk]
        for answer in question.answers.all():
            if answer.answer_by.pk != request.user.pk and answer.answer_by.pk not in target_users:
                target_users += [answer.answer_by.pk]
                for like in answer.likes.all():
                    if like.created_by.pk != request.user.pk and like.created_by.pk not in target_users:
                        target_users += [like.created_by.pk]

        for user_id in target_users:
            NotificationManager.push_notification("question_reply_edit", question.pk, request.user, user_id, answer=question_answer)
        return HttpResponse(json.dumps(response))
        # return HttpResponseRedirect(reverse("question_details_view",kwargs={"pk": int(qid)}))


class QuestionCreateView(LoginRequiredMixin, PermissionRequiredMixin, View):
    def get(self,request,*args,**kwargs):
        context = {}
        return render(request,"student_question_create.html",context)
    def post(self,request,*args,**kwargs):

        url = reverse('user_profile', kwargs={ "pk": request.user.pk })+"?tab=my-questions"
        question_id = request.POST.get("question_id")
        q_title = request.POST.get("q_title")
        q_description = request.POST.get("q_description")
        q_tags = request.POST.getlist("qtag-list[]")
        deleted_files = request.POST.get("deleted_ids", "")

        if not q_title:
            messages.error(request, 'Question title missing')
            HttpResponseRedirect(url)

        if not q_description:
            messages.error(request, 'Question description missing')
            HttpResponseRedirect(url)

        try:
            question_id = int(question_id)
        except:
            question_id = None
        question = Question()
        if question_id:
            question = original_question_object = Question.objects.get(pk=question_id)
        question.created_by = request.user
        question.title = q_title
        question.description = q_description
        question.save()

        attached_files = question.attached_file.all()
        for attachment in attached_files:
            if str(attachment.id) in deleted_files:
                question.attached_file.remove(attachment)

        for file_name,content in request.FILES.items():
            if file_name.startswith("written_file") and len(file_name) > len("written_file"):
                file = content
                original_file_name = file._name
                file_name = str(request.user.id)+"-"+str(uuid.uuid4())+"."+file._name[file._name.rindex(".")+1:]
                with open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name), 'wb+') as destination:
                    for chunk in file.chunks():
                        destination.write(chunk)
                # file = File(open(os.path.join(settings.MSG_ATTACHMENT_DIR, file_name)))
                question_attachment = QuestionAttachment()
                question_attachment.attachment_name = original_file_name
                question_attachment.attachment = "%s/%s" % (settings.MSG_ATTACHMENT_DIR_NAME, file_name)
                question_attachment.save()
                question.attached_file.add(question_attachment)

        question.tags.clear()
        majors = []
        for q_tag in q_tags:
            major_object = Major.objects.get(pk=int(q_tag))
            question.tags.add(major_object)
            majors += [major_object]

        if question_id:
            target_users = []
            for answer in question.answers.all():
                if answer.answer_by.pk not in target_users:
                    target_users += [answer.answer_by.pk]
                    for like in answer.likes.all():
                        if like.created_by.pk != request.user.pk and like.created_by.pk not in target_users:
                            target_users += [like.created_by.pk]

            for user_id in target_users:
                NotificationManager.push_notification("question_edit", original_question_object, request.user, user_id)

            return HttpResponseRedirect(reverse("question_details_view", kwargs={"pk": question_id}))

        if not question_id:
            user_majors = UserMajor.objects.filter(major__in=majors)
            recipients = []
            for user_major in user_majors:
                if user_major.user not in recipients and user_major.user.pk != request.user.pk:
                    recipients += [user_major.user]

            for recipient in recipients:
                target_subjects = ""
                for u_major in user_majors:
                    if u_major.user == recipient and u_major.major in majors:
                        target_subjects = u_major.major.name if not target_subjects else target_subjects+", "+u_major.major.name
                target_subjects = replace_last_comma_with_and(target_subjects)
                NotificationManager.push_notification("question_post", question, request.user, recipient.pk, subject=target_subjects)

                ## send email
                if recipient.champuser.receive_email_notification:
                    html, text = EmailTemplateUtil.render_question_feed_email(recipient, question, None)
                    email_subject = "How to solve this PSLE question? - CT Online"
                    email_object = {
                        "recipients": [recipient.email],
                        'subject': email_subject,
                        'html_body': html,
                        'text_body': text
                    }
                    EmailScheduler.place_to_queue(email_object)

        messages.success(request, 'Question posted successfully')
        return HttpResponseRedirect(url)

# class QuestionFeedView(LoginRequiredMixin,View):
#     def get(self,request,*args,**kwargs):
#         context = {}
#         context["q_post_list"] = Question.objects.filter(created_by=self.request.user).order_by("-date_modified")
#         return render(request,"question_feed.html",context)


class QuestionFeedView(LoginRequiredMixin, ListView):
    template_name = "question_feed.html"
    model = Question
    paginate_by = 10
    context_object_name = "q_post_list"

    def get_queryset(self):
        return Question.objects.filter(created_by=self.request.user).order_by("-date_modified")

    def get_context_data(self, **kwargs):
        context = super(QuestionFeedView, self).get_context_data(**kwargs)
        return context

class QuestionDetailsView(LoginRequiredMixin, View):
    def get(self,request,*args,**kwargs):
        context = {}
        context["question"] = Question.objects.get(pk=kwargs.get("pk"))
        return render(request, "question_details.html",context)

class QuestionEditView(LoginRequiredMixin, View):
    def get(self,request,*args,**kwargs):
        context = {}
        context["question"] = Question.objects.get(pk=kwargs.get("pk"))
        user_level_names = SubjectUtil.get_levels_for_country(country_name=self.request.user.champuser.nationality.name)
        context["levels"] = SubjectLevel.objects.filter(name__in=user_level_names)
        return render(request,"question_edit.html",context)
class QuestionDeleteView(LoginRequiredMixin, View):
    def get(self,request,*args,**kwargs):
        Question.objects.filter(pk=kwargs.get("pk")).delete()
        return HttpResponseRedirect(reverse('user_profile', kwargs={ "pk": request.user.pk })+"?tab=my-questions")


class QuestionAnswerDeleteView(LoginRequiredMixin, View):

    def get_object(self):
        return get_object_or_404(QuestionAnswer, pk=self.kwargs.get("pk"))

    def get_question(self):
        return get_object_or_404(Question, pk=self.kwargs.get("question_pk"))

    def get_success_url(self):
        question = self.get_question()
        return reverse("question_details_view", args=(question.pk, ))

    def get(self,request,*args,**kwargs):
        obj = self.get_object()
        # delete answer
        obj.delete()
        return HttpResponseRedirect(self.get_success_url())


class QuestionAnswerCommentPostView(LoginRequiredMixin,View):
    def post(self,request,*args,**kwargs):
        qid = request.POST.get("qid")
        qaid = request.POST.get("qaid")
        text = request.POST.get("qcomment")
        question_answer = QuestionAnswer.objects.get(pk=int(qaid))
        qcomment = QuestionComment()
        qcomment.comment = text
        qcomment.created_by = request.user
        qcomment.save()
        question_answer.comments.add(qcomment)
        host_url = "%s:%s" % (APP_SERVER_HOST, APP_SERVER_PORT)
        profile_picture = qcomment.created_by.champuser.profile_picture
        date_created = format_datetime(convert_utc_timestamp_to_local_datetime(int(qcomment.date_created), request))
        date_modified = format_datetime(convert_utc_timestamp_to_local_datetime(int(qcomment.date_modified), request))

        data = {
            'user_pk': request.user.id,
            'comment_pk': qcomment.id,
            'question_pk': qid,
            'profile_picture': profile_picture.image_field.url if profile_picture else '',
            'user_full_name': qcomment.created_by.champuser.fullname,
            'comment': qcomment.comment,
            'timestamp_created': qcomment.date_created,
            'timestamp_modified': qcomment.date_modified,
            'date_created': date_created,
            'date_modified': date_modified,
            'profile_url': "%sprofile/%s" % (host_url, request.user.id),
            'summary_form_action': "%sqanswer/comment/edit/%s" %(host_url, qcomment.id),
            'comment_delete_url': "%sqanswer/comment/delete/%s/%s" %(host_url, qcomment.id, qid),
            'host_url': host_url,
            'comment_count': question_answer.comments.count()
        }

        response = {'status': 'successful', 'data': data}

        question = Question.objects.get(pk=int(qid))

        # target_users = [question.created_by.pk] if question.created_by.pk != request.user.id else []
        # for answer in question.answers.all():
        #     if answer.answer_by.pk != request.user.id and answer.answer_by.pk not in target_users:
        #         target_users += [answer.answer_by.pk]
        #     for like in answer.likes.all():
        #         if like.created_by.pk != request.user.id and like.created_by.pk not in target_users:
        #             target_users += [like.created_by.pk]

        target_users = [question.created_by] if question.created_by != request.user else []
        for answer in question.answers.all():
            if answer.answer_by != request.user and answer.answer_by not in target_users:
                target_users += [answer.answer_by]
            for like in answer.likes.all():
                if like.created_by != request.user and like.created_by not in target_users:
                    target_users += [like.created_by]

        for target_user in target_users:
            NotificationManager.push_notification("question_reply_comment", question.pk, request.user, target_user.pk, comment=qcomment)
            send_email_thread(target_user, question, None, qcomment.pk)

        return HttpResponse(json.dumps(response))
        # return HttpResponseRedirect(reverse("question_details_view",kwargs={"pk": int(qid)}))



class QuestionAnswerCommentEditView(LoginRequiredMixin,View):
    def post(self,request,*args,**kwargs):
        qid = request.POST.get("qid")
        cid = request.POST.get("cid")
        text = request.POST.get("comment")
        qcomment = QuestionComment.objects.get(pk=int(cid))
        qcomment.comment = text
        qcomment.created_by = request.user
        qcomment.save()

        data = {
            'text': qcomment.comment,
            'date_modified': format_datetime(
                convert_utc_timestamp_to_local_datetime(int(qcomment.date_modified), request)),
        }
        response = {'status': 'successful', 'data': data}
        return HttpResponse(json.dumps(response))
        # return HttpResponseRedirect(reverse("question_details_view",kwargs={"pk": int(qid)}))


class QuestionAnswerCommentDeleteView(LoginRequiredMixin, View):

    def get_object(self):
        return get_object_or_404(QuestionComment, pk=self.kwargs.get("pk"))

    def get_question(self):
        return get_object_or_404(Question, pk=self.kwargs.get("question_pk"))

    def get_success_url(self):
        question = self.get_question()
        return reverse("question_details_view", args=(question.pk, ))

    def get(self,request,*args,**kwargs):
        obj = self.get_object()
        obj.delete()
        return HttpResponseRedirect(self.get_success_url())



class JobRecommendationView(LoginRequiredMixin, View):
    def get(self,request,*args,**kwargs):
        champ_user = ChampUser.objects.get(user_id=request.user.pk)
        context = {
            "section": "job_recommendation",
            "champ_user": champ_user,
            "object": champ_user,
            "about_data": Profile.objects.get(user=request.user).about,
            "content_editable": True if int(kwargs.get("pk")) == request.user.id else False
        }
        context["buddies"] = get_buddies_online(request)

        job_recommendations = JobRecommendation.objects.filter(user=request.user,status=JobInvitationStatus.active.value)
        job_recommendations_data = []
        for job_recommendation in job_recommendations:
            template = loader.get_template("ajax/job_recommendation_item.html")
            cntxt = Context({ 'job_recommendation': job_recommendation ,"request": request})
            rendered = template.render(cntxt)
            job_recommendations_data += [rendered]

        context["job_recommendation_data"] = job_recommendations_data

        return render(request,"profile.html",context)


class ProfileVideoIntroView(LoginRequiredMixin, View):
    def get(self,request,*args,**kwargs):
        context = {
            "section": "video-intro",
            "champ_user": ChampUser.objects.get(user_id=request.user.pk),
            "content_editable": True if int(kwargs.get("pk")) == request.user.id else False
        }
        if ProfileMyVideo.objects.filter(user=request.user).exists():
            context["my_video_url"] = ProfileMyVideo.objects.get(user=request.user).url
        context["buddies"] = get_buddies_online(request)
        return render(request,"tutor_profile.html",context)

class UpdateProfileView(CommonMixin, ProtectedView, View):
    def post(self, request, params, *args, **kwargs):
        if params is not None:
            data = dict()
            if params == 'abt_me_form':
                form = AboutMeUpdateForm(request.POST)
                data['update_data_class'] = '.abt_me_text'
                data['update_btn_class'] = '.update_abt_me'
            if params == 'change_major_form':
                form = SubjectMajorUpdateForm(request.POST)
                data['update_data_class'] = '.change_major_text'
                data['update_btn_class'] = '.change_major'

            if params == 'change_texp_form':
                form = TeachingExpForm(request.POST)
                data['update_data_class'] = '.change_texp_text'
                data['update_btn_class'] = '.change_texp'

            if params == 'change_einterest_form':
                form = ExtInterestForm(request.POST)
                data['update_data_class'] = '.change_einterest_text'
                data['update_btn_class'] = '.change_einterest'
            if form.is_valid():
                profile = form.save(request)
                data['success'] = 'True'
                data['value'] = profile
                return HttpResponse(json.dumps(data), content_type="application/json")
            else:
                data = dict()
                data['success'] = 'False'
                data['value'] = 'Change Unsuccessful'
                return HttpResponse(json.dumps(data), content_type="application/json")



class ChangeProfilePictureView(CommonMixin, ProtectedView, FormView):
    template_name = "change_profile_picture.html"
    success_url = '/'
    form_class = ProfilePictureForm

    def get_success_url(self):
        return reverse('user_profile', args=str(self.request.user.pk))

    def get_context_data(self, **kwargs):
        return {
            "form": kwargs.get("form"),
            "cropping_media": kwargs.get("form").media
        }

    def get(self, request, image_id=None, *args, **kwargs):
        image = get_object_or_404(ProfilePicture, pk=image_id) if image_id else None
        form_class = self.get_form_class()
        form = form_class(instance=image)
        self.object = None
        #print(form.media)
        return self.render_to_response(self.get_context_data(form=form))

    def post(self, request, image_id=None, *args, **kwargs):
        image = get_object_or_404(ProfilePicture, pk=image_id) if image_id else None
        form = self.form_class(request.POST, request.FILES, instance=image)
        if form.is_valid():
            if not image:
                image = form.save()
                return HttpResponseRedirect(reverse('crop_pp', args=(image.pk,)))
            else:
                image = form.save()
                if request.user.is_authenticated():
                    users = ChampUser.objects.filter(user=request.user)
                    if users.exists():
                        user = users[0]
                        user.profile_picture = image
                        user.save()
                return redirect(self.get_success_url())
        else:
            return self.form_invalid(form)

class EmailRecoveryRequestView(TemplateView):
    template_name = "email_recovery.html"

    def get_context_data(self, **kwargs):
        context = super(EmailRecoveryRequestView, self).get_context_data(**kwargs)
        context["security_questions"] = SecurityQuestion.objects.all()
        return context

    def post(self, request, *args, **kwargs):

        if request.user.is_authenticated():
            return HttpResponseRedirect(reverse("user_profile", kwargs={ "pk": request.user.pk }))

        name = request.POST.get("email_recovery_name")
        email_recovery_surname = request.POST.get("email_recovery_surname")
        email_recovery_country = request.POST.get("email_recovery_country")
        security_question = request.POST.get("email_recovery_security_question")
        email_recovery_sq_answer = request.POST.get("email_recovery_sq_answer")
        email_recovery_bdate = request.POST.get("email_recovery_bdate")

        try:
            security_question = int(security_question)
            birthdate = datetime.strptime(email_recovery_bdate, "%m/%d/%Y")
        except:
            context = {
                "postback": False,
                "error": True,
                "message": "Please fill up the form properly"
            }
            return render(request,self.template_name,context)

        champ_users = ChampUser.objects.all()
        champ_users = champ_users.filter(fullname=name,lastname=email_recovery_surname,nationality__name=email_recovery_country)
        champ_users = champ_users.filter(user__usersecurityquestion__question__id=security_question,user__usersecurityquestion__answer=email_recovery_sq_answer)
        champ_users = champ_users.filter(birthdate=birthdate)

        if champ_users.exists():
            champ_user = champ_users.first()
            context = {
                "postback": True,
                "match": True,
                "email": champ_user.user.email,
                "fullname": champ_user.fullname,
                "surname": champ_user.lastname,
                "image": champ_user.render_profile_picture
            }
        else:
            context = {
                "postback": True,
                "match": False
            }
        return render(request,self.template_name,context)


class ResetPasswordRequestView(View):
    def get(self,request,*args,**kwargs):

        if request.user.is_authenticated():
            redirect_url = reverse("user_profile", args=(request.user.pk,))
            return HttpResponseRedirect(redirect_url)

        token = request.GET.get("token")
        context = {
            "verified": False,
            "type": "password",
            "token": token
        }
        if token:
            reset_tokens = ResetPasswordToken.objects.filter(token=token)
            if reset_tokens.exists():
                reset_token_object = reset_tokens.first()
                print(Clock.utc_now())
                print(datetime.fromtimestamp(reset_token_object.date_created))
                utc_now = Clock.utc_timestamp()
                tdiff = utc_now - reset_token_object.date_created
                if tdiff <= 18000000000:
                    context["verified"] = True
                    context["user_id"] = reset_token_object.user.pk
                if not context["verified"]:
                    if reset_token_object.used > 0:
                        context["token_used"] = True


        return render(request,"reset_password.html",context)

    def post(self, request, *args, **kwargs):

        if request.user.is_authenticated():
            redirect_url = reverse("user_profile", args=(request.user.pk,))
            return HttpResponseRedirect(redirect_url)

        email = request.POST.get("reset_email")
        try:
            associated_users = User.objects.filter(email=email)
            if associated_users.exists():
                for user in associated_users:
                    token = hashlib.sha224(str(user.pk)).hexdigest()
                    ResetPasswordToken.objects.create(token=token,user_id=user.id)

                    html, text = EmailTemplateUtil.render_password_reset_token_email(email,token)

                    email_object = {
                        "recipients": [email],
                        'subject': 'Password Reset',
                        'html_body': html,
                        'text_body': text
                    }

                    EmailScheduler.place_to_queue(email_object)


                return render(request,"reset_password.html",{ "reset_token_sent": True,"email": email })
            else:
                return render(request,"reset_password.html",{ "email_not_exists": True, "email": email })
        except Exception as e:
            return render(request,"reset_password.html",{ "error_occured": True, "email": email })


class PasswordResetConfirmView(View):
    def post(self, request, token=None, *arg, **kwargs):

        if request.user.is_authenticated():
            redirect_url = reverse("user_profile", args=(request.user.pk,))
            return HttpResponseRedirect(redirect_url)

        user_id = request.POST.get("user_id")
        token = request.POST.get("token")
        pass1 = request.POST.get("reset_pass")
        pass2 = request.POST.get("reset_pass2")
        if pass1 == pass2:
            reset_token = ResetPasswordToken.objects.filter(token=token,user_id=int(user_id),used=0)
            if reset_token.exists():
                user_objects = User.objects.filter(pk=int(user_id))
                if user_objects.exists():
                    user_object = user_objects.first()
                    user_object.set_password(pass1)
                    user_object.save()
                    reset_token = reset_token.first()
                    reset_token.used = reset_token.used + 1
                    reset_token.save()
                    return HttpResponseRedirect(reverse("user_login")+str("?password_reset=success"))
        context = {
            "reset_failed": True
        }
        return render(request,"reset_password.html",context)

class ProtectedFormView(CommonMixin, ProtectedView, FormView):
    template_name = 'create.html'

    def get_success_url(self):
        return reverse('user_profile', args=str(ChampUser.objects.get(user=self.request.user).id))

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            form.save(request)
            return self.form_valid(form)
        else:
            messages.error(request, 'Error occurred')
            return self.form_invalid(form)

class EducationAddView(CommonMixin, ProtectedView,FormView):
    form_class = EducationForm
    template_name = 'create.html'

    def get_success_url(self):
        return reverse('user_profile', args=str(ChampUser.objects.get(user=self.request.user).id))

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            instance = form.save()
            profile = Profile.objects.get(user__user=request.user)
            profile.education.add(instance)
            data = dict()
            data['added_edu'] = 'True'
            # <h3 class="hdng_inr"> {{ item.session }} {{ item.degree }} (<span class="chicago"> {{ item.institution }} </span>)

            data['session'] = instance.session
            data['degree'] = instance.degree
            data['institution'] = instance.institution
            data['update_data_class'] = '.add_new_edu'
            # data['update_btn_class'] = '.update_abt_me'
            return HttpResponse(json.dumps(data), content_type="application/json")
        else:
            messages.error(request, 'Error occurred')
            return self.form_invalid(form)

class EducationUpdateView(CommonMixin, ProtectedView, UpdateView):
    model = Education
    template_name = 'create.html'

class EducationDeleteView(CommonMixin, ProtectedView, DeleteView):
    model = Education

    def get_success_url(self):
        return reverse('user_profile', args=str(ChampUser.objects.get(user=self.request.user).id))

    def get(self, request, *args, **kwargs):
        return self.delete(request, *args, **kwargs)

class LessonReviewListView(LoginRequiredMixin, View):
    def get(self,request,*args,**kwargs):
        champ_user = ChampUser.objects.get(user_id=request.user.pk)
        context = {
            "section": "lesson_review_list",
            "champ_user": champ_user,
            "object": champ_user,
            "content_editable": True if request.user.is_authenticated() else False
        }
        context["buddies"] = get_buddies_online(request)

        live_lessons = LiveLesson.objects.filter(request_user=request.user).exclude(review__isnull=True)
        written_lessons = LiveLesson.objects.filter(request_user=request.user).exclude(review__isnull=True)

        reviews = []
        for live_lesson in live_lessons:
            reviews += [live_lesson.review]

        for written_lesson in written_lessons:
            reviews += [written_lesson.review]

        context["reviews"] = reviews

        return render(request,"profile.html",context)

class LessonReviewDetailsView(LoginRequiredMixin, View):
    def get(self,request,*args,**kwargs):
        champ_user = ChampUser.objects.get(user_id=request.user.pk)
        context = {
            "section": "lesson_review_details",
            "champ_user": champ_user,
            "object": champ_user,
            "content_editable": True if int(kwargs.get("pk")) == request.user.id else False
        }
        context["buddies"] = get_buddies_online(request)

        review = LessonReview.objects.get(pk=kwargs.get('pk'))

        context["reviews"] = review

        return render(request,"profile.html",context)

class InviteUsersLessonView(View):
    def post(self,request,*args,**kwargs):
        job_id = request.POST.get("job_id")
        if not job_id:
            messages.add_message(request, messages.ERROR, 'Invalid job id')
            url = reverse('user_profile',kwargs={'pk': request.user.pk})
            return HttpResponseRedirect(url)

        try:
            int(job_id)
        except Exception as msg:
            messages.add_message(request, messages.ERROR, 'Invalid job id')
            url = reverse('user_profile',kwargs={'pk': request.user.pk})
            return HttpResponseRedirect(url)

        job_objects = Job.objects.filter(pk=int(job_id))
        if not job_objects:
            messages.add_message(request, messages.ERROR, 'Invalid job id')
            url = reverse('user_profile',kwargs={'pk': request.user.pk})
            return HttpResponseRedirect(url)

        job = job_objects.first()

        user_list = request.POST.get("tutor_list")
        if not user_list:
            messages.add_message(request, messages.ERROR, 'No user has been chosen')
            url = reverse('user_profile',kwargs={'pk': request.user.pk})
            return HttpResponseRedirect(url)
        user_list = [int(id) for id in user_list.split(",") if id]
        recipients = []
        for u in user_list:
            user = User.objects.get(pk=int(u))
            job_invitation = JobInvitation()
            job_invitation.job = job
            job_invitation.invited_by = request.user
            job_invitation.user = user
            job_invitation.status = JobInvitationStatus.active.value
            job_invitation.save()
            recipients += [user.email]

        msg = """<p>You have got a job invitation from %s. Please click on the following link to accept the job invitation.
        <div>
            <a href="%sjob/details/%s/">%sjob/details/%s/</a>
        </div>
        </p>""" % (request.user.champuser.fullname, settings.SITE_URL,str(job.pk), settings.SITE_URL,str(job.pk))

        print(msg)

        email_object = {
            "recipients": recipients,
            'subject': 'Job Invitation',
            'html_body': '<html><head></head><body>%s</body></html>' % msg,
            'text_body': 'Text Body'
        }
        #print(email_object)
        # result = send_email_job.delay(email_object)

        messages.add_message(request, messages.INFO, 'Your invitation has been sent.')
        url = reverse('job_post_details',kwargs={'pk': job_id})
        return HttpResponseRedirect(url)

class JobRecommendationListView(LoginRequiredMixin,ListView):
    template_name = "job_recommendation.html"
    model = JobRecommendation
    paginate_by = 10
    context_object_name = "job_recommendation_list"

    def get_queryset(self):
        return JobRecommendation.objects.filter(user_id=self.request.user.pk).order_by('-date_modified')

    def get_context_data(self, **kwargs):
        context = super(JobRecommendationListView, self).get_context_data(**kwargs)
        return context

class EmailVerification(View):
    def get(self,request,*args,**kwargs):
        activation_code = request.GET.get("token")
        context = {}
        if activation_code:
            champ_user_objects = ChampUser.objects.filter(activation_code=activation_code)
            if champ_user_objects.exists():
                champ_user = champ_user_objects.first()
                now_time = Clock.utc_timestamp()
                create_time = champ_user.date_created
                if now_time - create_time <= 1800:
                    champ_user.user.is_active = True
                    champ_user.user.save()

                    user_object = authenticate(email=champ_user.user.email, server_token=settings.SERVER_TOKEN)
                    if user_object is not None:
                        login(request, user_object)
                        seven_days = 24 * 60 * 60 * 7
                        request.session.set_expiry(seven_days)
                        request.session['is_login'] = True
                        request.session['user_id'] = request.user.pk
                        request.session['email'] = request.user.email
                        return HttpResponseRedirect(request.user.champuser.get_login_success_url())
                else:
                    context["msg"] = "Email verification failed. The token has been expired."
            else:
                context["msg"] = "Email verification failed. The token is invalid."
        else:
            context["msg"] = "Token is required."
        return render(request,"email_verification.html",context)

class AccessViolation(LoginRequiredMixin,View):
    def get(self,request,*args,**kwargs):
        return render(request,"unauthorized_access.html",{})

class FAQView(View):
    def get(self,request,*args,**kwargs):
        return render(request,"faq.html",{})

class PrivacyPolicyView(View):

    def get(self,request,*args,**kwargs):

        return render(request,"privacy_policy.html",{})

class ContactUsView(JSONResponseMixin, View):

    def get(self,request,*args,**kwargs):

        context = {}
        context['CAPTCHA_SITE_KEY'] = settings.RECAPTCHA_PUBLIC_KEY

        return render(request,"contact_us.html",context)

    def post(self, request, *args, **kwargs):
        contact_form = ContactUsForm(request.POST,request=request)
        if contact_form.is_valid():
            contact_us = ContactUs()
            inquiry_type = 0
            if contact_form.cleaned_data['inquiry_type'] == 'tech_issue':
                inquiry_type = 0
            else:
                inquiry_type = 1
            contact_us.inquiry_type = inquiry_type
            contact_us.first_name = contact_form.cleaned_data['first_name']
            contact_us.email = contact_form.cleaned_data['email']
            contact_us.contact_no = contact_form.cleaned_data['contact_no']
            contact_us.subject = contact_form.cleaned_data['subject']
            contact_us.feedback = contact_form.cleaned_data['feedback']
            contact_us.save()

            # html, text = EmailTemplateUtil.render_contact_email(first_name, email, phone, subject, feedback, recipients)
            #
            # email_subject = "Feedback submitted by %s" % contact_us.email
            #
            # email_recipients = [ admin[0] for admin in settings.ADMINS ]
            #
            # email_object = {
            #     "recipients": email_recipients,
            #     'subject': email_subject,
            #     'html_body': html,
            #     'text_body': text
            # }
            #
            # EmailScheduler.place_to_queue(email_object)

            return self.render_to_json({
                "status": "SUCCESS",
                "message": "Successful",
                "data": {}
            })
        else:
            return self.render_to_json({
                "status": "FAILURE",
                "message": "Failed",
                "data": {}
            })

class UpdateStudentProfileSummary(JSONResponseMixin, TemplateView):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(UpdateStudentProfileSummary, self).dispatch(*args, **kwargs)

    template_name = "ajax/profile/student/about_summary.html"

    def get_context_data(self, **kwargs):
        context = super(UpdateStudentProfileSummary, self).get_context_data(**kwargs)

        profile_objects = Profile.objects.filter(user__user_id=self.request.user.pk)

        context["profile_summary"] = profile_objects.first().about if profile_objects.exists() else None

        return context

    def post(self, request, *args, **kwargs):

        response = {
            "status": "FAILURE",
            "message": "FAILURE",
            "data": []
        }

        if not request.user.is_authenticated():
            response['message'] = 'Unauthenticated access'
            return self.render_to_json(response)

        about_content = request.POST.get("data")
        if about_content:
            about_content = about_content.strip()
        else:
            about_content = ''

        profile_objects = Profile.objects.filter(user__user_id=request.user.pk)
        if profile_objects.exists():
            profile_object = profile_objects.first()
            profile_object.about = about_content
            profile_object.save()
        else:
            profile_object = Profile()
            profile_object.user_id = request.user.champuser.pk
            profile_object.about = about_content
            profile_object.save()

        response["status"] = "SUCCESS"
        response["message"] = "Successful."

        return self.render_to_json(response)


class UpdateStudentPrivateInfo(JSONResponseMixin, TemplateView):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(UpdateStudentPrivateInfo, self).dispatch(*args, **kwargs)

    template_name = "ajax/profile/student/student_private_info.html"

    def get_context_data(self, **kwargs):

        context = super(UpdateStudentPrivateInfo, self).get_context_data(**kwargs)

        birthdate = self.request.user.champuser.birthdate.strftime('%m/%d/%Y') if self.request.user.champuser.birthdate else None
        form_data = {
            "email": self.request.user.email,
            "gender": self.request.user.champuser.gender,
            "timezone": self.request.user.champuser.timezone,
            "birthdate": birthdate,
            "religion": self.request.user.champuser.religion,
            "nationality": self.request.user.champuser.nationality.name if self.request.user.champuser.nationality else None,
            "ethnicity": self.request.user.champuser.ethnicity
        }
        languages = self.request.user.champuser.languages.all()
        langs = [(ul.language.pk, ul.language.name) for ul in languages]
        lang_vals = [str(pk) for pk, lan in langs]
        langs_val = ",".join(lang_vals)
        form = StudentPrivateInfoForm(form_data)

        context.update({'form': form, "langs": langs, "langs_val": langs_val})

        return context

    def post(self, request, *args, **kwargs):

        response = {
            "status": "FAILURE",
            "message": "FAILURE",
            "data": []
        }

        if not request.user.is_authenticated():
            response['message'] = 'Unauthenticated access'
            return self.render_to_json(response)

        about_content = request.POST.get("data")
        if about_content:
            about_content = about_content.strip()
        else:
            about_content = ''



        profile_obj, created = Profile.objects.get_or_create(user_id=request.user.pk)

        profile_obj.about = about_content
        profile_obj.save()

        response["status"] = "SUCCESS"
        response["message"] = "Successful."

        return self.render_to_json(response)

class UpdateStudentEducation(JSONResponseMixin, TemplateView):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(UpdateStudentEducation, self).dispatch(*args, **kwargs)

    template_name = "ajax/profile/student/student_education.html"

    def get_context_data(self, **kwargs):

        context = super(UpdateStudentEducation, self).get_context_data(**kwargs)

        edu_id = self.request.GET.get("id")
        education_form_data = {}
        if edu_id:
            education_object = Education.objects.get(pk=int(edu_id))
            date_attended2 = None
            try:
                if education_object.end_year:
                    date_attended2 = datetime.fromtimestamp(education_object.end_year).date().strftime("%m/%d/%Y")
                else:
                    date_attended2 = "Present"
            except Exception as exp:
                pass
            education_form_data = {
                "hidden_pk": education_object.pk,
                "school_type": education_object.institution.type,
                "hidden_school": education_object.institution.pk,
                "school_name": education_object.institution.name,
                "is_current": education_object.is_current,
                "location": education_object.location,
                "date_attended": datetime.fromtimestamp(education_object.date_attended).date().strftime("%m/%d/%Y"),
                "date_attended2": date_attended2,
                "hidden_major": education_object.major.pk,
                "major": education_object.major.name
            }
            context['school_type'] = education_object.institution.type
            level_names = SubjectUtil.get_levels_for_country(self.request.user.champuser.nationality.name)
            context['levels'] = SubjectLevel.objects.filter(name__in=level_names)
            context['level'] = education_object.level_id
            subjects = Major.objects.none()
            if education_object.level_id:
                subjects = Major.objects.filter(level_id=education_object.level_id)
            context['subjects'] = subjects
            context['major'] = education_object.major.name if education_object.major else ''
        else:
            education_object = None
            level_names = SubjectUtil.get_levels_for_country(self.request.user.champuser.nationality.name)
            context['levels'] = SubjectLevel.objects.filter(name__in=level_names)
            context['subjects'] = Major.objects.none()

        education_form = StudentEducationForm(education_form_data)
        if education_object:
            date_attended = datetime.fromtimestamp(education_object.date_attended).strftime("%m/%d/%Y")
        else:
            date_attended = None
        context.update({"education_form": education_form, "date_attended": date_attended})

        return context

    def post(self, request, *args, **kwargs):

        response = {
            "status": "FAILURE",
            "message": "FAILURE",
            "data": []
        }

        if not request.user.is_authenticated():
            response['message'] = 'Unauthenticated access'
            return self.render_to_json(response)

        hidden_pk = request.POST.get("hidden_pk")
        school_type = request.POST.get('school_type')
        hidden_school = request.POST.get('hidden_school')
        school_name = request.POST.get('school_name')
        is_current = request.POST.get('is_current')
        location = request.POST.get('location')
        date_attended = request.POST.get('date_attended')
        date_attended2 = request.POST.get('date_attended2')
        level = request.POST.get('level')
        subject = request.POST.get('subject')
        new_subject_name = request.POST.get('new_subject')

        if school_type and school_name and date_attended:
            try:
                if hidden_pk:
                    education_object = Education.objects.get(pk=int(hidden_pk))
                else:
                    education_object = Education()

                school_objects = Schools.objects.filter(type=school_type, name=school_name)
                if school_objects.exists():
                    school_object= school_objects.first()
                else:
                    school_object = Schools()
                    school_object.type = school_type
                    school_object.name = school_name
                    school_object.added_by_id = request.user.pk
                    school_object.save()

                education_object.institution_id = school_object.pk
                if is_current == 'on':
                    education_object.is_current = True
                else:
                    education_object.is_current = False

                if location:
                    education_object.location = location

                if date_attended:
                    education_object.date_attended = time.mktime(datetime.strptime(date_attended, '%m/%d/%Y').timetuple())

                if date_attended2:
                    try:
                        education_object.end_year = time.mktime(
                            datetime.strptime(date_attended2, '%m/%d/%Y').timetuple())
                    except Exception as exp:
                        pass

                if level:
                    level_id = int(level)
                    level_objects = SubjectLevel.objects.filter(pk=level_id)
                    if level_objects.exists():
                        level_object = level_objects.first()
                        education_object.level_id = level_object.pk

                        if subject and subject != '-1':
                            major_objects = Major.objects.filter(name=subject, level_id=level_object.pk)
                            if major_objects.exists():
                                major_object = major_objects.first()
                                education_object.major_id = major_object.pk
                        else:
                            if new_subject_name:
                                if not Major.objects.filter(name=new_subject_name, level_id=level_object.pk).exists():
                                    m = Major()
                                    m.name = new_subject_name
                                    m.level_id = level_object.pk
                                    m.creator_id = request.user.pk
                                    m.save()

                                    education_object.major_id = m.pk
                                else:
                                    m = Major.objects.filter(name=new_subject_name, level_id=level_object.pk).first()
                                    education_object.major_id = m.pk

                education_object.user_id = request.user.pk
                education_object.save()
            except Exception as exp:
                response['message'] = 'Server error'
                return self.render_to_json(response)

        response["status"] = "SUCCESS"
        response["message"] = "Successful."

        return self.render_to_json(response)


class WishlistView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, "wishlist.html", {})

    def post(self, request, *args, **kwargs):
        url = reverse('user_profile', kwargs={"pk": request.user.pk})+"?tab=profile"
        type = request.POST.get("type")
        comments = request.POST.get("comments")
        if not comments:
            messages.error(request, 'Comments are missing')
            HttpResponseRedirect(url)

        wishlist = Wishlist()
        # wishlist.created_by = request.user
        wishlist.type = type
        wishlist.comments = comments
        wishlist.created_by_id = request.user.pk
        wishlist.save()

        messages.success(request, 'Wishlist saved successfully')
        return HttpResponseRedirect(url)
