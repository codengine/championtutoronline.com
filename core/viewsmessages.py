from django.core.paginator import Paginator
from django.shortcuts import render
from django.views.generic import *
from django.template import loader, Context
from core.generics.mixins import LoginRequiredMixin
from core.library.block_util import BlockListUtil
from core.models3 import *

__author__ = 'codengine'

class MessageView(LoginRequiredMixin,ListView):
    template_name = "message.html"
    model = UserMessage
    paginate_by = 13

    def get_queryset(self):
        super(MessageView,self).get_queryset()
        user_ids = []
        umessage_ids = []
        # query = "select user_message.id as id, sender_id, receiver_id, user_id from user_message, conversation_other_users where user_message.conversation_id in (select id from conversation_other_users where user_id=%s) or sender_id=%s or receiver_id=%s group by sender_id,receiver_id, user_id order by date_created DESC;" % (self.request.user.pk, self.request.user.pk, self.request.user.pk)
        # user_messages = UserMessage.objects.raw(query) #.filter(Q(sender=self.request.user) | Q(receiver=self.request.user)).group_by('sender_id','receiver_id').order_by("-date_created")
        # for umessage in user_messages:
        #     if umessage.sender_id != self.request.user.pk:
        #         if not umessage.sender_id in user_ids:
        #             user_ids += [ umessage.sender_id ]
        #     elif umessage.receiver_id and not umessage.receiver_id in user_ids:
        #         user_ids += [ umessage.receiver_id ]
        #     else:
        #         if not umessage.user_id in user_ids:
        #             user_ids += [ umessage.user_id ]
        #
        # user_ids = [ int(uid) for uid in user_ids if uid and int(uid) != self.request.user.pk ]
        # user_ids = [ uid for uid in set(user_ids)]
        #
        # for uid in user_ids:
        #     umessage = UserMessage.objects.filter((Q(sender_id=self.request.user.pk) & (Q(receiver_id=uid) |Q(conversation__other_users__id__in=[uid]) )) | (Q(sender_id=uid) & (Q(receiver_id=self.request.user.pk) | Q(conversation__other_users__id__in=[self.request.user.pk]) ))).order_by('-date_created').first()
        #     if umessage:
        #         umessage_ids += [ umessage.pk ]

        # query = """select id, chat_type,
        #     sender_id, receiver_id,conversation_id
        #     from user_message
        #     where conversation_id in
        #     (select id from conversation_other_users where user_id=%s)
        #     or sender_id=%s or receiver_id=%s group by sender_id,receiver_id,
        #     conversation_id order by date_created DESC;""" % (self.request.user.pk, self.request.user.pk, self.request.user.pk)

        query = """
            select * from (
            select id, chat_type,
            sender_id, receiver_id,conversation_id, date_created
            from user_message
            where conversation_id in
            (
                SELECT * FROM (
                        select id from conversation_other_users where user_id=%s order by id desc limit 1
                  ) as t
            )
            or sender_id=%s or receiver_id=%s  order by id DESC ) as tt
            group by sender_id,receiver_id,conversation_id;
        """ % (self.request.user.pk, self.request.user.pk, self.request.user.pk)

        user_messages = UserMessage.objects.raw(query)

        latest_user_messages = {}

        for um in user_messages:

            if um.chat_type == 1:
                latest_user_messages[um.conversation_id] = um
            else:
                sender_id = um.sender_id
                receiver_id = um.receiver_id
                if sender_id == self.request.user.pk:
                    if not latest_user_messages.get(receiver_id):
                        latest_user_messages[receiver_id] = um
                    else:
                        if um.pk > latest_user_messages[receiver_id].pk:
                            latest_user_messages[receiver_id] = um

                else:
                    if not latest_user_messages.get(sender_id):
                        latest_user_messages[sender_id] = um
                    else:
                        if um.pk > latest_user_messages[sender_id].pk:
                            latest_user_messages[sender_id] = um


        user_messages = latest_user_messages.values()

        conversation_added = []
        user_added = []
        for umessage in user_messages:
            if umessage.chat_type == 1:
                if not umessage.conversation_id in conversation_added:
                    conversation_added += [ umessage.conversation_id ]
                    umessage_ids += [ umessage.pk ]
            else:
                if umessage.sender.pk == self.request.user.pk:
                    if not umessage.receiver.pk in user_added:
                        user_added += [ umessage.receiver.pk ]
                        umessage_ids += [ umessage.pk ]
                else:
                    if not umessage.sender.pk in user_added:
                        user_added += [ umessage.sender.pk ]
                        umessage_ids += [ umessage.pk ]


        umessage_ids = [ umpk for umpk in set(umessage_ids)]

        user_messages = UserMessage.objects.filter(pk__in=umessage_ids).order_by('-message__date_created')
        return user_messages


    def get_context_data(self, **kwargs):
        context = super(MessageView, self).get_context_data(**kwargs)

        page = self.request.GET.get("page")
        object_list = []
        try:
            page = int(page) if page else 1
            paginator = Paginator(self.get_queryset(), self.paginate_by)
            page = paginator.page(page)
            object_list = page.object_list
        except Exception as msg:
            object_list = []

        def get_other_user(user_message):
            if user_message.chat_type == 0:
                if user_message.sender.pk == self.request.user.pk:
                    return user_message.receiver
                else:
                    return user_message.sender
            else:
                return user_message.conversation.conversation_id

        rendered_items = []
        for user_message in object_list:
            other_user = get_other_user(user_message)
            contxt_data = {
                "user_message": user_message,
                "request": self.request,
                "STATIC_URL": settings.STATIC_URL+"/",
                "other_user": other_user
            }
            template_name = "ajax/message_list_entry.html"
            template = loader.get_template(template_name)
            cntxt = Context(contxt_data)
            rendered = template.render(cntxt)
            rendered_items += [ rendered ]
        context['items'] = rendered_items
        return context


class MessageDetailView(LoginRequiredMixin, View):
    def get(self,request,*args,**kwargs):
        receiver_id = int(kwargs.get("user_id"))
        context = {
            "receiver_id": receiver_id
        }
        umessages = UserMessage.objects.filter((Q(sender_id=request.user.pk) & Q(receiver_id=receiver_id)) | (Q(sender_id=receiver_id) & Q(receiver_id=request.user.pk)))
        messages = Message.objects.filter(usermessage__in=umessages).order_by("-date_created")[:10]
        #print(len(messages))
        context["message_list"] = messages
        context["message_receiver"] = ChampUser.objects.get(user_id=receiver_id)
        # context["buddies"] = get_buddies_online(request)

        enter_to_send = True
        chat_settings = ChatSettings.objects.filter(user_id=request.user.pk)
        if chat_settings.exists():
            chat_setting = chat_settings.first()
            if chat_setting.enter_to_send == 0:
                enter_to_send = False
        context["enter_to_send"] = enter_to_send
        context["blocked"] = BlockListUtil.check_if_user_blocked(request.user.pk, receiver_id)
        return render(request, 'message_detail.html', context)

class MessageConversationDetailView(LoginRequiredMixin, View):

    def get(self,request,*args,**kwargs):
        conversation_id = kwargs.get("conversation_id")
        context = {
            "conversation_id": conversation_id
        }
        umessages = UserMessage.objects.filter(conversation__conversation_id=conversation_id)
        messages = Message.objects.filter(usermessage__id__in=umessages.values_list('pk', flat=True)) #.order_by("-date_created")[:10]
        #print(len(messages))

        conversation_object = Conversation.objects.filter(conversation_id=conversation_id)
        if conversation_object.exists():
            conversation_object = conversation_object.first()
            if conversation_object.users_left.filter(pk=request.user.pk).exists():
                since_dates = conversation_object.since_dates.filter(user_id=request.user.pk)
                if since_dates.exists():
                    max_since_ts = since_dates.first().since_ts
                    messages = messages.filter(date_created__lte=max_since_ts)
        messages = messages.order_by("-date_created")[:10]

        context["message_list"] = messages
        context["conversation_id"] = conversation_id
        member_ids = [ uid for uid in set([ user.pk for user in umessages.first().conversation.other_users.all() ] + [ umessages.first().sender.pk]) ]
        left_members = [ uid for uid in umessages.first().conversation.users_left.values_list('pk', flat=True) ]
        block_settings = UserBlockSettings.objects.filter(user_id=request.user.pk)
        blocked_users = []
        if block_settings.exists():
            block_settings = block_settings.first()
            blocked_users = block_settings.blocked_users.values_list('pk', flat=True)

        temp_ids = []
        for i in member_ids:
            if not i in left_members and not i in blocked_users:
                temp_ids += [ i ]

        member_ids = temp_ids

        context["member_list"] = User.objects.filter(pk__in=member_ids)
        context["total_members"] = len(member_ids)
        conversation_objects = Conversation.objects.filter(conversation_id=conversation_id)
        conversation_object = conversation_objects.first()
        context["permitted"] = True
        user_in_this_conversation = (conversation_object.other_users.filter(pk__in=[ request.user.pk ]).exists() or conversation_objects.filter(usermessage__sender_id__in=[ request.user.pk ]).exists())
        if user_in_this_conversation and conversation_object.users_left.filter(pk__in=[ request.user.pk ]).exists():
            context["user_left"] = True
        elif user_in_this_conversation and not conversation_object.users_left.filter(pk__in=[ request.user.pk ]).exists():
            context["user_active"] = True
        else:
            context["permitted"] = False
        context["title"] = conversation_object.title if conversation_object.title else "Group Chat"
        # context["buddies"] = get_buddies_online(request)
        enter_to_send = True
        chat_settings = ChatSettings.objects.filter(user_id=request.user.pk)
        if chat_settings.exists():
            chat_setting = chat_settings.first()
            if chat_setting.enter_to_send == 0:
                enter_to_send = False
        context["enter_to_send"] = enter_to_send
        return render(request, 'message_conversation_details.html', context)
