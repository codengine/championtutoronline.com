__author__ = 'codengine'

APPS = (
    'core',
    'authentication',
    'common',
    'pyetherpad',
    'pysharejs',
    'payment'
)
