__author__ = 'codengine'
from django.views.generic.base import View
from django.shortcuts import render
from django.http import HttpResponse
from models import *
import json
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from CodePadUtil import *

class SharePadAjaxView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(SharePadAjaxView, self).dispatch(*args, **kwargs)

    def get(self,request,*args,**kwargs):
        pass

    def post(self,request,*args,**kwargs):
        name = request.POST.get("name")
        lan = request.POST.get("lan")
        creator = request.user
        codepad_util_obj = CodePadUtil()
        pad_instance = codepad_util_obj.create_public_pad(name,creator,lan)
        return HttpResponse(json.dumps({"status":"SUCCESS","pad_nid":pad_instance.pad_nid,"pad_name": pad_instance.pad_name}))

