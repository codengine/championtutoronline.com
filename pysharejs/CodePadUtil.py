__author__ = 'codengine'

from models import *
import uuid
from datetime import datetime

class CodePadUtil:
    def __init__(self):
        pass

    def create_codepad(self,group_name,language,pad_name=None,creator=None):
        codepad_last_nid_objs = CodePadLastNid.objects.filter(group=group_name)
        last_nid = 1
        if codepad_last_nid_objs.exists():
            codepad_last_nid_obj = codepad_last_nid_objs.first()
            last_nid = codepad_last_nid_obj.nid
            last_nid = last_nid + 1
            codepad_last_nid_obj.nid = last_nid
            codepad_last_nid_obj.save()
        else:
            codepad_last_nid_obj = CodePadLastNid()
            codepad_last_nid_obj.group = group_name
            codepad_last_nid_obj.nid = 1
            codepad_last_nid_obj.save()

        code_pad = CodePad()
        code_pad.pad_group = str(group_name)
        code_pad.pad_nid = last_nid
        code_pad.pad_id = uuid.uuid4()
        if pad_name:
            code_pad.pad_name = pad_name
        else:
            code_pad.pad_name = str(last_nid)
        if creator:
            code_pad.pad_created_by = creator
        code_pad.pad_language = language
        code_pad.save()
        return code_pad

