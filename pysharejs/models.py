from django.db import models
from core.models3 import *
from pyetherpad.models import *

class CodePad(GenericModel):
    pad_nid = models.BigIntegerField()
    pad_group = models.CharField(max_length=200)
    pad_id = models.CharField(max_length=255)
    pad_name = models.CharField(max_length=255,null=True,blank=True)
    pad_created_by = models.ForeignKey(User,null=True)
    pad_language = models.CharField(max_length=255)

    class Meta:
        db_table=u'public_code_pad'

class CodePadLastNid(GenericModel):
    group = models.CharField(max_length=500) ###Group is lesson id when its's a protected lesson, user_id when it's private demo and session id when it's public demo lesson.
    nid = models.BigIntegerField()

    class Meta:
        db_table = u'codepad_last_nid'
