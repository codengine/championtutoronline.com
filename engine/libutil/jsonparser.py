__author__ = 'codengine'

import os
import json
from django.conf import settings

class CurrencyParser(object):
    def __init__(self):
        pass

    def parse_currency(self,data):
        currency = {}
        json_data_dict = json.loads(data)
        for key,obj in json_data_dict.items():
            currency[key] = obj["symbol"]
        return currency

    def get_currency_list(self):
        try:
            resource_url = os.path.join(settings.RESOURCE_DIR,"currency.json")
            contents = "";
            with open(resource_url,"r") as f:
                contents = f.read()
            return self.parse_currency(contents)
        except Exception,msg:
            print "Exception occurred. Exception message: "+str(msg)
            return {}


if __name__ == "__main__":
    content = """
    {
        "USD": {
        "symbol": "$",
            "name": "US Dollar",
            "symbol_native": "$",
            "decimal_digits": 2,
            "rounding": 0,
            "code": "USD",
            "name_plural": "US dollars"
        },
            "CAD": {
            "symbol": "CA$",
                "name": "Canadian Dollar",
                "symbol_native": "$",
                "decimal_digits": 2,
                "rounding": 0,
                "code": "CAD",
                "name_plural": "Canadian dollars"
        }
    }
    """

    print CurrencyParser().parse_currency(content)
