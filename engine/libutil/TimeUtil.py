__author__ = 'Sohel'

class TimeUtil(object):
    @classmethod
    def get_all_duration_list(cls):
        DURATION = []
        for i in range(15,181,5):
            time_str = str(i)+" min"
            if i >= 60:
                if i % 60 > 0:
                    time_str = str(i/60)+" hour "+str(i%60)+" min"
                else:
                    time_str = str(i/60)+" hour "
            DURATION += [(i,time_str)]
        return DURATION

