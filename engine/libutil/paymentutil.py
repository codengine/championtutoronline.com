from core.models3 import Currency

__author__ = 'Sohel'

class PaymentCurrencyUtil(object):

    @classmethod
    def get_all_currencies(cls):
        currencies = [(obj.code, obj.name) for obj in Currency.objects.all()]
        return currencies
