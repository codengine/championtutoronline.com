__author__ = 'codengine'

class PermissionManager(object):
    @classmethod
    def check_user_permission_on_wb(cls,user_id,whiteboard_id):
        return True

    @classmethod
    def has_perm_all(cls,user_id,whiteboard_id):
        return True

    @classmethod
    def has_read_permission(cls,user_id,whiteboard_id):
        return True

    @classmethod
    def get_required_permissions(cls,view_path):
        if view_path == "/job/create/":
            return ["student"]
        elif view_path == "/question/create/":
            return ["student"]

    @classmethod
    def has_view_permission(cls,request):
        if request.user.is_authenticated():
            if request.user.champuser.type.name == "teacher":
                return "teacher" in cls.get_required_permissions(request.path)
            elif request.user.champuser.type.name == "student":
                return "student" in cls.get_required_permissions(request.path)
        return False
