import pytz

from django.utils import timezone
from tzlocal import get_localzone

class TimezoneMiddleware(object):
    def process_request(self, request):
        try:
            tzname = request.user.champuser.timezone
            if tzname:
                timezone.activate(pytz.timezone(tzname))
            else:
                timezone.deactivate()
        except:
            try:
                tz = get_localzone()
                timezone.activate(tz)
            except:
                timezone.deactivate()