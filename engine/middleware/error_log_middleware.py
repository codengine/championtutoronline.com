from django.shortcuts import render
from core.models3 import ErrorLog

__author__ = 'codengine'

import traceback

class ErrorLogMiddleware(object):
    def process_exception(self, request, exception):
        current_url = request.get_full_path()
        current_trace = traceback.format_exc()
        error_log = ErrorLog()
        error_log.url = current_url
        error_log.stacktrace = current_trace
        error_log.save()
