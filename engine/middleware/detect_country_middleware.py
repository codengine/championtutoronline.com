import requests
import json

__author__ = 'codengine'

class DetectCountryMiddleware(object):
    def process_request(self, request):
        try:
            if not "user_info" in request.session or ( "user_info" in request.session and not request.session["user_info"].get("country") ):
                request_object = requests.get("http://ip-api.com/json")
                if request_object.status_code == 200:
                    content = request_object.content
                    json_content = json.loads(content)
                    country = json_content["country"]
                    if not "user_info" in request.session:
                        request.session["user_info"] = {
                            "country": country
                        }
                    else:
                        request.session["user_info"]["country"] = country
        except Exception as exp:
            pass

