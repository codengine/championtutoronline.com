__author__ = 'codengine'

class CreateSessionMiddleware(object):
    def process_request(self,request):
        if not request.session.exists(request.session.session_key):
            request.session.create()
