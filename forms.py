from core.library.Clock import Clock
from core.library.country_util import CountryUtil
from core.library.currency_util import CurrencyUtil
from core.library.lang_util import LanguageUtil
from core.library.tz_util import TZUtil
from core.models2 import Major
from core.models3 import Country, SubjectLevel, ContactUs
from core.models3 import Currency

__author__ = 'Codengine'

from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
import hashlib
from core.models3 import ChampUser, ProfilePicture, Profile, Education,LessonRequest,Courses,Language
from image_cropping import ImageCropWidget
from core.tools.form import DateSelectorWidget
import urllib
import urllib2
import json
from django import forms
from django.forms import ModelForm
from captcha.fields import ReCaptchaField
from django.conf import settings

class PasswordResetRequestForm(forms.Form):
    '''
    prompts user to input either username or email address of that user and sends him a email for resetting his password.
    '''
    email_or_username = forms.CharField(label='',required=True,widget=forms.TextInput(attrs={'placeholder':'Enter Email or User Name','autocomplete':'off','class':'form-control input-lg'}))


class SetPasswordForm(forms.Form):
    """
    A form that lets a user change set their password without entering the old
    password
    """
    error_messages = {
        'password_mismatch': ("The two password fields didn't match."),
        }
    new_password1 = forms.CharField(label=("New password"),
                                    widget=forms.PasswordInput)
    new_password2 = forms.CharField(label=("New password confirmation"),
                                    widget=forms.PasswordInput)

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                    )
        return password2

class LoginBase(forms.Form):
    email = forms.CharField(label='',required=True,widget=forms.TextInput(attrs={'name':'email', 'class': 'email','placeholder':'Email ID','autocomplete':'off'}))
    password = forms.CharField(label='', required=True , widget=forms.PasswordInput(attrs={'name':'password', 'class': 'password', "required":"true",'placeholder':'Password'}))
    remember = forms.BooleanField(required=False)

class SignUpForm(forms.Form):
    signup_email = forms.CharField(label='',required=True,widget=forms.TextInput(attrs={'name':'email', "class": "signup_email", "required":"true", 'placeholder':'Email ID'}))
    signup_password = forms.CharField(label='', required=True , widget=forms.PasswordInput(attrs={'name':'password', "class": "signup_password", "required":"true", 'placeholder':'Password'}))
    #signup_remember = forms.BooleanField(required=False)
    signup_name = forms.CharField(label='', required=True ,max_length=30,widget=forms.TextInput(attrs={'name':'name', "class": "signup_name","required": "true", 'placeholder': 'Name'}))
    signup_surname = forms.CharField(label='', required=True ,max_length=30,widget=forms.TextInput(attrs={'name':'surname', "class": "signup_surname","required":"true",'placeholder': 'Surname'}))
    #mobile = forms.CharField(label='', required=True ,max_length=30,widget=forms.TextInput(attrs={'name':'mobile','placeholder': 'Mobile Number','autocomplete':'off','data-title':'','class':'form-control input-lg'}))
    signup_password_repeat = forms.CharField(label='', required=False , widget=forms.PasswordInput(attrs={'name':'password_repeat', "class": "signup_password_repeat","required":"true",'placeholder':'Confirm Password'}))
    CHOICES=[('--','Register As A'),('student','Student'),('teacher','Teacher')]
    signup_role = forms.ChoiceField(label='', required=True,choices=CHOICES, widget=forms.Select(attrs={'name':'role', "required":"true","class": "signup_role",'title':'This field is required.'}))

    timezones = TZUtil.get_supported_tz()

    signup_timezones = forms.ChoiceField(label='', required=True,choices=timezones, widget=forms.Select(attrs={'name':'timezone', "class": "signup_timezone", 'title':'This field is required.','class':'tz'}))

    message = forms.CharField(label='',required=False,widget=forms.Textarea(attrs={}))

    def __init__(self, *args, **kwargs):
        initial_tz = kwargs.get("tz_offset")
        prefix = kwargs.get("prefix")
        if prefix:
            self._prefix = prefix
        else:
            self._prefix = ""
        if "tz_offset" in kwargs:
            del kwargs["tz_offset"]
        super(SignUpForm, self).__init__(*args, **kwargs)


        tz_initial = TZUtil.get_supported_tz()

        signup_timezones = forms.ChoiceField(label='', required=True,choices=tz_initial, widget=forms.Select(attrs={'name':'role','title':'This field is required.','class':'tz'}))
        self.fields["signup_timezones"] = signup_timezones

class LoginForm(LoginBase):

    def authenticate(self, request):
        data = self.cleaned_data
        try:
            c_user = ChampUser.objects.filter(user__email = data['email']).first()
            user = authenticate(username=c_user.user.username, password=data["password"])
            if user is not None:
                if user.is_active:
                    login(request, user)
                    if not data['remember']:
                        request.session.set_expiry(0)
                    else:
                        seven_days = 24*60*60*7
                        request.session.set_expiry(seven_days)
                    return True
                else:
                    #self.add_error('username', "Your account has been disabled!")
                    pass
            else:
                #self.add_error('username', "Your username and password were incorrect.")
                pass
            return False
        except Exception as e:
            print e

    def authenticate_stuff(self, request):
        data = self.cleaned_data
        try:
            c_user = ChampUser.objects.filter(user__email = data['email'],user__is_staff=True)
            if c_user.exists():
                c_user = c_user.first()
                user = authenticate(username=c_user.user.username, password=data["password"])
                if user is not None:
                    if user.is_active:
                        login(request, user)
                        if not data['remember']:
                            request.session.set_expiry(0)
                        else:
                            seven_days = 24*60*60*7
                            request.session.set_expiry(seven_days)
                        return True
                    else:
                        #self.add_error('username', "Your account has been disabled!")
                        pass
            else:
                #self.add_error('username', "Your username and password were incorrect.")
                pass
            return False
        except Exception as e:
            print e


class ProfilePictureForm(forms.ModelForm):

        def save(self, commit=True):
            super(ProfilePictureForm, self).save(commit)

            return self.instance

        class Meta:
            model = ProfilePicture


class SubjectMajorUpdateForm(forms.Form):
    major_subjects = forms.CharField(widget=forms.TextInput)

    def save(self, request):
        profile = Profile.objects.get(user=ChampUser.objects.get(user=request.user))
        profile.major_subject = self.cleaned_data['major_subjects']
        profile.save()
        return profile.major_subject

class AboutMeUpdateForm(forms.Form):
    about_me = forms.CharField(widget=forms.Textarea())

    def save(self, request):
        profile = Profile.objects.get(user=ChampUser.objects.get(user=request.user))
        profile.description = self.cleaned_data['about_me']
        profile.save()
        return profile.description

class TeachingExpForm(forms.Form):
    data = forms.CharField(widget=forms.Textarea())

    def save(self, request):
        profile = Profile.objects.get(user=ChampUser.objects.get(user=request.user))
        profile.teaching_exp = self.cleaned_data['data']
        profile.save()
        return profile.teaching_exp


class ExtInterestForm(forms.Form):
    data = forms.CharField(widget=forms.Textarea())

    def save(self, request):
        profile = Profile.objects.get(user=ChampUser.objects.get(user=request.user))
        profile.ext_interest = self.cleaned_data['data']
        profile.save()
        return profile.ext_interest

class EducationForm(forms.ModelForm):
    class Meta:
        model = Education

class MyAccountForm(forms.Form):
    email = forms.CharField(label='',required=True,widget=forms.TextInput(attrs={'name':'email','placeholder':'Email Address','autocomplete':'off','class':'form-control input-lg change-password-input','title':'','data-placement':'top','data-toggle':'tooltip','data-original-title':'','readonly':''}))
    old_password = forms.CharField(label='', required=True , widget=forms.PasswordInput(attrs={'id':'id_password','name':'password','placeholder':'Old Password','class':'form-control input-lg change-password-input','title':'','data-placement':'top','data-toggle':'tooltip','data-original-title':''}))
    new_password = forms.CharField(label='', required=True , widget=forms.PasswordInput(attrs={'id':'id_new_password','name':'new_password','placeholder':'New Password','class':'form-control input-lg change-password-input','title':'','data-placement':'top','data-toggle':'tooltip','data-original-title':''}))
    new_password_again = forms.CharField(label='', required=True , widget=forms.PasswordInput(attrs={'id':'id_new_password_again','name':'new_password_again','placeholder':'New Password Again','class':'form-control input-lg change-password-input','title':'','data-placement':'top','data-toggle':'tooltip','data-original-title':''}))

class PaymentMethodForm(forms.Form):
    first_name = forms.CharField(label='',required=True,widget=forms.TextInput(attrs={'name':'first_name','placeholder':'First Name','autocomplete':'off','class':'form-control input-lg','title':'','data-placement':'top','data-toggle':'tooltip','data-original-title':''}))
    last_name = forms.CharField(label='',required=True,widget=forms.TextInput(attrs={'name':'last_name','placeholder':'Last Name','autocomplete':'off','class':'form-control input-lg','title':'','data-placement':'top','data-toggle':'tooltip','data-original-title':''}))
    card_type = forms.ChoiceField(choices=[("V","Visa"),("M","Master")],widget=forms.Select(attrs={'name':'card_type','placeholder':'Card Type','autocomplete':'off','class':'form-control input-lg','title':'','data-placement':'top','data-toggle':'tooltip','data-original-title':''}))
    card_number = forms.CharField(label='',required=True,widget=forms.TextInput(attrs={'name':'card_number','placeholder':'Card Number','autocomplete':'off','class':'form-control input-lg','title':'','data-placement':'top','data-toggle':'tooltip','data-original-title':''}))
    exp_date = forms.CharField(label='',required=True,widget=forms.TextInput(attrs={'name':'exp_date','placeholder':'Expired Date','autocomplete':'off','id':'datetimepicker','class':'form-control input-lg','title':'','data-placement':'top','data-toggle':'tooltip','data-original-title':''}))
    cvn = forms.CharField(label='',required=True,widget=forms.TextInput(attrs={'name':'cvn','placeholder':'CVN','autocomplete':'off','class':'form-control input-lg','title':'','data-placement':'top','data-toggle':'tooltip','data-original-title':''}))
    # primary = forms.ChoiceField(choices=[("primary","Primary Card"),("not_primary","Not Primary")],widget=forms.Select(attrs={'name':'card_type','placeholder':'Card Type','autocomplete':'off','class':'form-control input-lg','title':'','data-placement':'top','data-toggle':'tooltip','data-original-title':''}))
    # active = forms.BooleanField(required=False,widget=forms.CheckboxInput(attrs={'class':'checkbox' }))

# class TSubjectForm(forms.ModelForm):
#     class Meta:
#         model = TopSubject

class LessonRequestForm(ModelForm):

    def __init__(self,*args,**kwargs):

        major_objs = kwargs.get("course") if kwargs.get("course") else []

        kwargs.pop("course")

        super(LessonRequestForm,self).__init__(*args,**kwargs)
        CHOICES=[
            ('--','Select Duration')
        ]
        for i in range(15,181,5):
            time_str = str(i)+" min"
            if i >= 60:
                if i % 60 > 0:
                    time_str = str(i/60)+" hour "+str(i%60)+" min"
                else:
                    time_str = str(i/60)+" hour "
            CHOICES += [(i,time_str)]

        majors = [
            ('--','Select Subject')
        ]
        for major_obj in major_objs:
            majors += [(major_obj.pk,major_obj.name)]

        lesson_time_choices = []

        format_n = lambda n: "0"+str(n) if n >= 0 and n <=9 else str(n)

        for i in range(24):
            for j in range(0,60,5):
                lesson_time_choices += [(i*60 + j,format_n(i)+":"+format_n(j))]

        repeat_choices = [
            ("single","Single"),
            ("daily","Daily"),
            ("weekly","Weekly")
        ]

        self.fields['lesson_date'] = forms.DateField(required=True,widget=forms.TextInput(attrs={'placeholder':'Select Date','name':'lesson-date','class':'form-control lesson-request-input','id':'id_lesson_date'}))
        self.fields['lesson_time'] = forms.TimeField(required=True,widget=forms.TextInput(attrs={'placeholder':'Select Time','name':'lesson-time','class':'form-control lesson-request-input','id':'id_lesson_time'}))
        # self.fields['lesson_time'] = forms.ChoiceField(required=True,choices=lesson_time_choices, widget=forms.Select(attrs={'name':'lesson-time','class':'form-control lesson-request-input','id':'id_lesson_time'}))
        self.fields['duration'] = forms.ChoiceField(required=True,choices=CHOICES, widget=forms.Select(attrs={'name':'lesson-length','class':'form-control lesson-request-select','id':'id_lesson_length'}))
        self.fields['repeat'] = forms.ChoiceField(required=True,choices=repeat_choices, widget=forms.Select(attrs={'name':'lesson-repeat','class':'form-control lesson-request-select','id':'id_lesson_repeat'}))
        # self.fields['course'] = forms.ChoiceField(required=True,choices=majors, widget=forms.Select(attrs={'name':'lesson-subject','class':'form-control lesson-request-select','id':'id_lesson_subject'}))

    class Meta:
        model = LessonRequest
        fields = ['lesson_date','lesson_time','duration']

class StudentPrivateInfoForm(forms.Form):
    from django.conf import settings

    countries = [(country.pk,country.name) for country in Country.objects.filter(type=Country.__name__)]
    timezones = [(value,label) for value,label,offset in Clock.get_all_timezones()]
    supported_languages = [(lang.pk,lang.name) for lang in Language.objects.all()]
    email = forms.CharField(label='',required=True,widget=forms.TextInput(attrs={'name':'email','placeholder':'Email Address','autocomplete':'off','class':'gpa_edit','title':'','data-placement':'top','data-toggle':'tooltip','data-original-title':'','readonly':''}))
    old_password = forms.CharField(label='', required=True , widget=forms.PasswordInput(attrs={'id':'id_password','name':'password','placeholder':'Password','class':'gpa_edit','title':'','data-placement':'top','data-toggle':'tooltip','data-original-title':''}))
    new_password = forms.CharField(label='', required=True , widget=forms.PasswordInput(attrs={'id':'id_new_password','name':'new_password','placeholder':'New Password','class':'gpa_edit ','title':'','data-placement':'top','data-toggle':'tooltip','data-original-title':''}))
    new_password_again = forms.CharField(label='', required=True , widget=forms.PasswordInput(attrs={'id':'id_new_password_again','name':'new_password_again','placeholder':'New Password Again','class':'gpa_edit','title':'','data-placement':'top','data-toggle':'tooltip','data-original-title':''}))
    gender = forms.ChoiceField(required=True,choices=[("male","male"),("female","female")], widget=forms.Select(attrs={'name':'gender','class':'gpa_edit','id':'id_gender'}))
    birthdate = forms.DateField(required=True,widget=forms.TextInput(attrs={'placeholder':'Select Date','name':'birthdate','class':'gpa_edit','id':'id_birthdate'}))
    religion = forms.CharField(label='',required=False,widget=forms.TextInput(attrs={'name':'religion','placeholder':'Religion/Beliefs','autocomplete':'off','class':'gpa_edit','title':'','data-placement':'top','data-toggle':'tooltip','data-original-title':''}))
    languages = forms.ChoiceField(required=True,choices=supported_languages, widget=forms.Select(attrs={'name':'language','class':'gpa_edit','id':'id_language'}))
    nationality = forms.ChoiceField(required=True,choices=countries, widget=forms.Select(attrs={'name':'citizenship','class':'gpa_edit','id':'id_citizenship'}))
    ethnicity = forms.CharField(label='',required=True,widget=forms.TextInput(attrs={'name':'ethnicity','placeholder':'Ethnicity','autocomplete':'off','class':'gpa_edit','title':'','data-placement':'top','data-toggle':'tooltip','data-original-title':''}))
    timezone = forms.ChoiceField(label='', required=True, choices= timezones ,widget=forms.Select(attrs={'name':'timezone','placeholder':'Timezone','autocomplete':'off','class':'gpa_edit','title':'','data-placement':'top','data-toggle':'tooltip','data-original-title':''}))

class StudentEducationForm(forms.Form):
    def __init__(self, initial=None, **kwargs):
        super(StudentEducationForm, self).__init__(initial=initial)
        if kwargs.get("subjet_levels"):
            self.subjet_levels = [("", "Select Level")] + [(l.pk, l.name) for l in kwargs.pop("subjet_levels")]
        else:
            self.subjet_levels = [("", "Select Level")] + [(l.pk, l.name) for l in SubjectLevel.objects.all()]

        self.fields["level"] = forms.ChoiceField(required=True, choices=self.subjet_levels, widget=forms.Select(
            attrs={'name': 'job_post_level', 'placeholder': 'Enter level', 'class': 'select2 job_post_select'}))

        if kwargs.get("subject_list"):
            self.subject_list = [(l.pk, l.name) for l in kwargs.pop("subject_list")]
        else:
            self.subject_list = [("", "Select Subject")] + [(l.pk, l.name) for l in Major.objects.none()]
        self.fields["subject"] = forms.ChoiceField(label='', required=True, choices=self.subject_list,
                                                   widget=forms.Select(attrs={'name': 'job_post_job_subject',
                                                                              'placeholder': 'Enter subject',
                                                                              'class': ''}))

    school_type_choices = (
        ('College/University', 'College/University'),
        ('High School', 'High School'),
    )
    hidden_pk = forms.CharField(label='',required=True,widget=forms.HiddenInput())
    school_type = forms.ChoiceField(widget=forms.RadioSelect, choices=school_type_choices,initial={"college/university":"College/University"})
    hidden_school = forms.CharField(label='',required=True,widget=forms.HiddenInput())
    school_name = forms.CharField(label='',required=True,widget=forms.TextInput(attrs={'name':'school_name','placeholder':'School Name','autocomplete':'off','class':'gpa_edit','title':'','data-placement':'top','data-toggle':'tooltip','data-original-title':''}))
    is_current = forms.BooleanField(required=False,initial=False,label='This is my current school')
    location = forms.CharField(label='',required=True,widget=forms.TextInput(attrs={'name':'location','placeholder':'Enter Location','autocomplete':'off','class':'gpa_edit','title':'','data-placement':'top','data-toggle':'tooltip','data-original-title':''}))
    date_attended = forms.DateField(required=True,widget=forms.TextInput(attrs={'placeholder':'From','name':'date_attended', 'readonly': 'readonly', 'class':'gpa_edit date_attended','id':'id_date_attended'}))
    date_attended2 = forms.DateField(required=True, widget=forms.TextInput(
        attrs={'placeholder': 'To', 'name': 'date_attended', 'readonly': 'readonly', 'class': 'gpa_edit date_attended',
               'id': 'id_date_attended2'}))
    hidden_major = forms.CharField(label='',required=True,widget=forms.HiddenInput())
    major = forms.CharField(label='',required=True,widget=forms.TextInput(attrs={'name':'major','placeholder':'Enter Major','autocomplete':'off','class':'gpa_edit','title':'','data-placement':'top','data-toggle':'tooltip','data-original-title':''}))
    new_subject = forms.CharField(label='',required=True,widget=forms.TextInput(attrs={'name':'new_subject','placeholder':'Enter Subject Name','autocomplete':'off','class':'gpa_edit'}))


class JobPostForm(forms.Form):


    def __init__(self, initial=None, **kwargs):
        super(JobPostForm, self).__init__(initial=initial)
        if kwargs.get("subjet_levels"):
            self.subjet_levels = [ ("", "Select Level") ] + [ (l.pk, l.name) for l in kwargs.pop("subjet_levels") ]
        else:
            self.subjet_levels =  []

        self.fields["level"] = forms.ChoiceField(required=True,choices=self.subjet_levels, widget=forms.Select(attrs={'name':'job_post_level', 'placeholder': 'Enter level', 'class':'select2 job_post_select'}))

        if kwargs.get("subject_list"):
            self.subject_list = [ (l.pk, l.name) for l in kwargs.pop("subject_list") ]
        else:
            self.subject_list =  [( "", "Select Subject" )]
        self.fields["subject"] = forms.ChoiceField(label='',required=True, choices=self.subject_list, widget=forms.Select(attrs={'name':'job_post_job_subject', 'placeholder':'Enter subject','class':''}))



    countries =  [ (-1,"No Preference") ]+CountryUtil.get_supported_countries() #  [(country.pk,country.name) for country in Country.objects.filter(type=Country.__name__)]
    timezones =  [(-1, "No Preference")] + TZUtil.get_timezone_differences() #  [(value,label) for value,label,offset in Clock.get_all_timezones()]
    supported_languages = [(-1, "No Preference")] + LanguageUtil.get_all_languages()
    currencies = CurrencyUtil.get_supported_currencies()
    DURATION=[
            # ('--','Select Duration')
        ]
    for i in range(15,181,5):
        time_str = str(i)+" min"
        if i >= 60:
            if i % 60 > 0:
                time_str = str(i/60)+" hour "+str(i%60)+" min"
            else:
                time_str = str(i/60)+" hour "
        DURATION += [(i,time_str)]
    rates = [
        ("1-5","$1 - $5"),
        ("6-10","$6 - $10"),
        ("11-15","$11 - $15"),
        ("15-20","$15 - $20"),
        ("20-25","$20 - $25"),
        ("25-30","$25 - $30"),
        ("31-1000000","above $30")]

    title = forms.CharField(label='',required=True,widget=forms.TextInput(attrs={'name':'job_post_job_title','placeholder':'Lesson title','class':''}))
    description = forms.CharField(label='',required=True,widget=forms.Textarea(attrs={'name':'job_post_job_description','placeholder':'I will require a tutor who knows Math subject very well','class':'champ_jobsedit_textarea'}))
    # end_date_month = forms.ChoiceField(required=True,choices=month_list, widget=forms.Select(attrs={'name':'job_post_end_month','class':'form-control inline'}))
    # end_date_day = forms.CharField(label='', required=True, widget=forms.TextInput(attrs={'name':'job_post_end_day','class':'form-control inline', 'placeholder':'Day'}))
    # end_date_year = forms.CharField(label='', required=True, widget=forms.TextInput(attrs={'name':'job_post_end_year','class':'form-control inline','placeholder':'Year'}))
    nationality = forms.ChoiceField(required=True,choices=countries, widget=forms.Select(attrs={'name':'job_post_nationality','class':'select2 add_multiple nationality'}))
    gender = forms.ChoiceField(required=True,choices=[("any","No Preference"),("male","male"),("female","female")], widget=forms.Select(attrs={'name':'gender','class':'select2'}))
    rate = forms.CharField(required=True, widget=forms.TextInput(attrs={'name':'job_post_rate', 'type': 'number', 'min': '0', 'placeholder':'0','class':'job-post-rate'}))
    rate2 = forms.CharField(required=True, widget=forms.TextInput(attrs={'name':'job_post_rate2', 'type': 'number', 'min': '0', 'placeholder':'1000','class':'job-post-rate'}))
    currency = forms.CharField(required=True, widget=forms.TextInput(attrs={'name':'job_post_currency','class':''}))
    languages = forms.ChoiceField(required=True,choices=supported_languages, widget=forms.Select(attrs={'name':'language','class':'select2 add_multiple languages'}))
    timezone = forms.ChoiceField(label='', required=True, choices= timezones ,widget=forms.Select(attrs={'name':'timezone','placeholder':'Timezone','autocomplete':'off','class':'select2 add_multiple timezone'}))
    availability = forms.ChoiceField(required=True,choices=[("any","No Preference"),("ready_to_teach","Ready to teach now"),("written_lesson","Written Lesson")], widget=forms.Select(attrs={'name':'availability','class':'select2'}))
    duration = forms.ChoiceField(required=True,choices=DURATION, widget=forms.Select(attrs={'name':'lesson-length','class':'select2','id':'id_lesson_length'}))
    lesson_start_date = forms.CharField(required=True,widget=forms.TextInput(attrs={'placeholder':'Enter Start Date', 'readonly': 'readonly', 'class':'','id':'datepicker1'}))
    # lesson_start_time = forms.TimeField(required=True,widget=forms.TextInput(attrs={'placeholder':'Select Time','name':'lesson-time','class':'start_time form-control','id':'id_lesson_start_time'}))
    job_end_date = forms.CharField(required=True,widget=forms.TextInput(attrs={'placeholder':'Enter Expiry Date', 'readonly': 'readonly', 'class':'job_end_time','id':'datepicker'}))
    lesson_repeat = forms.ChoiceField(required=True,choices=[("1","No Repeat"),("2","Daily"),("3","Weekly"), ( "4", "Twice a week" ), ( "5", "Bi-weekly" )], widget=forms.Select(attrs={'name':'lesson_repeat','class':'select2'}))


class ContactUsForm(forms.Form):
    inquiry_choices = (
        ('tech_issue', 'Technical Issue'),
        ('general_inquiry', 'General Inquiry')
    )
    inquiry_type = forms.ChoiceField(label='Inquiry Type', widget=forms.RadioSelect(), choices=inquiry_choices)
    first_name = forms.CharField(label='', required=True, widget=forms.TextInput(
        attrs={'name': 'first_name', 'placeholder': 'First Name', 'class': ''}))

    email = forms.CharField(label='', required=True, widget=forms.TextInput(
        attrs={'name': 'email', 'placeholder': 'Email', 'class': ''}))

    contact_no = forms.CharField(label='', required=True, widget=forms.TextInput(
        attrs={'name': 'contact_no', 'placeholder': 'Contact No', 'class': ''}))

    subject = forms.CharField(label='', required=True, widget=forms.TextInput(
        attrs={'name': 'subject', 'placeholder': 'Subject', 'class': ''}))

    feedback = forms.CharField(label='', required=True, widget=forms.Textarea(
        attrs={'name': 'feedback',
               'placeholder': 'Feedback'}))

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(ContactUsForm, self).__init__(*args, **kwargs)

    def clean(self):
        super(ContactUsForm, self).clean()

        # test the google recaptcha
        url = "https://www.google.com/recaptcha/api/siteverify"
        values = {
            'secret': settings.RECAPTCHA_PRIVATE_KEY,
            'response': self.request.POST.get(u'g-recaptcha-response', None),
            'remoteip': self.request.META.get("REMOTE_ADDR", None),
        }
        data = urllib.urlencode(values)
        req = urllib2.Request(url, data)
        response = urllib2.urlopen(req)
        result = json.loads(response.read())

        # result["success"] will be True on a success
        if not result["success"]:
            raise forms.ValidationError(u'Only humans are allowed to submit this form.')

        return self.cleaned_data

