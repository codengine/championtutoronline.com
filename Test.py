from suds.client import Client
# wsdl_url = 'http://127.0.0.1:8003/static/paypal/wsdl/sandbox/PayPalSvc.wsdl'
from suds.sax.attribute import Attribute
from suds.plugin import MessagePlugin
class DocdataAPIVersionPlugin(MessagePlugin):
    def marshalled(self, context):
        body = context.envelope.getChild('Body')[0]
        print(body)
        body.attributes.append(Attribute("version", "124"))

url = 'https://www.sandbox.paypal.com/wsdl/PayPalSvc.wsdl'
client = Client(url)
# print(client)
# print(client)
api_user_name = 'wilson7tan-facilitator_api1.gmail.com'
api_password = 'RASJAZ4KUDGYJQJB'
api_signature = 'AELl94hr4eiGqpYSopMz2Zmn6MDAA8H-svpZP1l-MmNrgelowoAYAQYj'
client.set_options(port='PayPalAPI',location='https://api-3t.sandbox.paypal.com/2.0/')
CustomSecurityHeaderType = client.factory.create('ns3:CustomSecurityHeaderType')
UserIdPasswordType = client.factory.create('ns3:UserIdPasswordType')
UserIdPasswordType.Username = api_user_name
UserIdPasswordType.Password = api_password
UserIdPasswordType.Signature = api_signature
CustomSecurityHeaderType.Credentials = UserIdPasswordType
# print(CustomSecurityHeaderType)
client.set_options(port='PayPalAPIAA', soapheaders=CustomSecurityHeaderType)
# print(client)
# GetBalanceReqeustType = client.factory.create('GetBalanceRequestType')
# GetBalanceReqeustType.ReturnAllCurrencies = '0'
# GetBalanceReqeustType.Version = '124.0'
# response = client.service.GetBalance(GetBalanceReqeustType)
# print(response)

DoDirectPaymentRequestType = {
    "PaymentAction": "Sale",
    "CreditCard": {
        "CreditCardType": "Visa",
        "CreditCardNumber": "4417119669820331",
        "ExpMonth": '11',
        "ExpYear": '2018',
        "CVV2": "874",
        "CardOwner": {
            "Payer": "codengineb@gmail.com"
        }
    },
    "PaymentDetails": {
        "OrderTotal": {
            "currencyID": "USD",
            "amount": '10'
        }
    },
    "IPAddress": "45.113.238.77",
    "Version": "124.0"
}

response = client.service.DoDirectPayment(DoDirectPaymentRequestType)
print(response)



# Suds ( https://fedorahosted.org/suds/ )  version: 0.4 GA  build: R699-20100913
#
# Service ( PayPalAPIInterfaceService ) tns="urn:ebay:api:PayPalAPI"
#    Prefixes (4)
#       ns0 = "urn:ebay:api:PayPalAPI"
#       ns1 = "urn:ebay:apis:CoreComponentTypes"
#       ns2 = "urn:ebay:apis:EnhancedDataTypes"
#       ns3 = "urn:ebay:apis:eBLBaseComponents"
#    Ports (2):
#       (PayPalAPI)
#          Methods (25):
#             AddressVerify(AddressVerifyRequestType AddressVerifyRequest, )
#             BMButtonSearch(BMButtonSearchRequestType BMButtonSearchRequest, )
#             BMCreateButton(BMCreateButtonRequestType BMCreateButtonRequest, )
#             BMGetButtonDetails(BMGetButtonDetailsRequestType BMGetButtonDetailsRequest, )
#             BMGetInventory(BMGetInventoryRequestType BMGetInventoryRequest, )
#             BMManageButtonStatus(BMManageButtonStatusRequestType BMManageButtonStatusRequest, )
#             BMSetInventory(BMSetInventoryRequestType BMSetInventoryRequest, )
#             BMUpdateButton(BMUpdateButtonRequestType BMUpdateButtonRequest, )
#             BillAgreementUpdate(BAUpdateRequestType BAUpdateRequest, )
#             BillUser(BillUserRequestType BillUserRequest, )
#             CancelRecoup(CancelRecoupRequestType CancelRecoupRequest, )
#             CompleteRecoup(CompleteRecoupRequestType CompleteRecoupRequest, )
#             CreateMobilePayment(CreateMobilePaymentRequestType CreateMobilePaymentRequest, )
#             DoMobileCheckoutPayment(DoMobileCheckoutPaymentRequestType DoMobileCheckoutPaymentRequest, )
#             EnterBoarding(EnterBoardingRequestType EnterBoardingRequest, )
#             GetBalance(GetBalanceRequestType GetBalanceRequest, )
#             GetBoardingDetails(GetBoardingDetailsRequestType GetBoardingDetailsRequest, )
#             GetMobileStatus(GetMobileStatusRequestType GetMobileStatusRequest, )
#             GetPalDetails(GetPalDetailsRequestType GetPalDetailsRequest, )
#             GetTransactionDetails(GetTransactionDetailsRequestType GetTransactionDetailsRequest, )
#             InitiateRecoup(InitiateRecoupRequestType InitiateRecoupRequest, )
#             MassPay(MassPayRequestType MassPayRequest, )
#             RefundTransaction(RefundTransactionRequestType RefundTransactionRequest, )
#             SetMobileCheckout(SetMobileCheckoutRequestType SetMobileCheckoutRequest, )
#             TransactionSearch(TransactionSearchRequestType TransactionSearchRequest, )
#          Types (461):
#             ns3:APIAuthenticationType
#             ns3:APICredentialsType
#             ns3:APIType
#             ns3:AbstractRequestType
#             ns3:AbstractResponseType
#             ns3:AccountEntryType
#             ns3:AccountStateCodeType
#             ns3:AccountSummaryType
#             ns3:AckCodeType
#             ns3:ActivationDetailsType
#             ns3:AdditionalAccountType
#             ns3:AdditionalFeeType
#             ns3:AddressNormalizationStatusCodeType
#             ns3:AddressOwnerCodeType
#             ns3:AddressStatusCodeType
#             ns3:AddressType
#             AddressVerifyRequestType
#             AddressVerifyResponseType
#             ns3:AirlineItineraryType
#             ns3:AllowedPaymentMethodType
#             ns1:AmountType
#             ns3:ApprovalSubTypeType
#             ns3:ApprovalTypeType
#             ns3:AttributeSetType
#             ns3:AttributeType
#             ns3:AuctionInfoType
#             ns3:AuctionTypeCodeType
#             ns3:AuthFlowTokenType
#             ns3:AuthorizationId
#             ns3:AuthorizationInfoType
#             ns3:AuthorizationRequestType
#             ns3:AuthorizationResponseType
#             ns3:AutoBillType
#             ns3:AverageMonthlyVolumeType
#             ns3:AverageTransactionPriceType
#             BAUpdateRequestType
#             ns3:BAUpdateResponseDetailsType
#             BAUpdateResponseType
#             BMButtonSearchRequestType
#             BMButtonSearchResponseType
#             BMCreateButtonRequestType
#             BMCreateButtonResponseType
#             BMGetButtonDetailsRequestType
#             BMGetButtonDetailsResponseType
#             BMGetInventoryRequestType
#             BMGetInventoryResponseType
#             ns3:BMLOfferInfoType
#             BMManageButtonStatusRequestType
#             BMManageButtonStatusResponseType
#             BMSetInventoryRequestType
#             BMSetInventoryResponseType
#             BMUpdateButtonRequestType
#             BMUpdateButtonResponseType
#             ns3:BalanceCodeType
#             ns3:BankAccountDetailsType
#             ns3:BankAccountTypeType
#             ns3:BankIDCodeType
#             ns1:BasicAmountType
#             ns3:BillOutstandingAmountRequestDetailsType
#             BillOutstandingAmountRequestType
#             ns3:BillOutstandingAmountResponseDetailsType
#             BillOutstandingAmountResponseType
#             BillUserRequestType
#             BillUserResponseType
#             ns3:BillingAgreementDetailsType
#             ns3:BillingApprovalDetailsType
#             ns3:BillingCodeType
#             ns3:BillingPeriodDetailsType
#             ns3:BillingPeriodDetailsType_Update
#             ns3:BillingPeriodType
#             ns3:BoardingStatusType
#             ns3:BusinessCategoryType
#             ns3:BusinessInfoType
#             ns3:BusinessOwnerInfoType
#             ns3:BusinessSubCategoryType
#             ns3:BusinessTypeType
#             ns3:ButtonCodeType
#             ns3:ButtonImageType
#             ns3:ButtonSearchResultType
#             ns3:ButtonStatusType
#             ns3:ButtonSubTypeType
#             ns3:ButtonTypeType
#             ns3:BuyNowTextType
#             ns3:BuyerDetailType
#             ns3:BuyerDetailsType
#             ns3:BuyerPaymentMethodCodeType
#             ns3:BuyerProtectionCodeType
#             ns3:BuyerType
#             ns3:CalculatedShippingRateType
#             CancelRecoupRequestType
#             CancelRecoupResponseType
#             ns3:CategoryArrayType
#             ns3:CategoryType
#             ns3:ChannelType
#             ns3:CharityType
#             ns3:CheckoutStatusCodeType
#             ns3:CompleteCodeType
#             CompleteRecoupRequestType
#             CompleteRecoupResponseType
#             ns3:CountryCodeType
#             ns3:CoupleType
#             ns3:CoupledBucketsType
#             ns3:CoupledPaymentInfoType
#             CreateBillingAgreementRequestType
#             CreateBillingAgreementResponseType
#             ns3:CreateMobilePaymentRequestDetailsType
#             CreateMobilePaymentRequestType
#             CreateMobilePaymentResponseType
#             ns3:CreateRecurringPaymentsProfileRequestDetailsType
#             CreateRecurringPaymentsProfileRequestType
#             ns3:CreateRecurringPaymentsProfileResponseDetailsType
#             CreateRecurringPaymentsProfileResponseType
#             ns3:CreditCardDetailsType
#             ns3:CreditCardNumberTypeType
#             ns3:CreditCardTypeType
#             ns3:CrossPromotionsType
#             ns3:CurrencyCodeType
#             ns3:CustomSecurityHeaderType
#             ns3:DepositTypeCodeType
#             ns3:DetailLevelCodeType
#             ns3:DeviceDetailsType
#             ns3:DiscountInfoType
#             ns3:DiscountType
#             ns3:DisplayControlDetailsType
#             DoAuthorizationRequestType
#             DoAuthorizationResponseType
#             DoCancelRequestType
#             DoCancelResponseType
#             DoCaptureRequestType
#             ns3:DoCaptureResponseDetailsType
#             DoCaptureResponseType
#             ns3:DoDirectPaymentRequestDetailsType
#             DoDirectPaymentRequestType
#             DoDirectPaymentResponseType
#             ns3:DoExpressCheckoutPaymentRequestDetailsType
#             DoExpressCheckoutPaymentRequestType
#             ns3:DoExpressCheckoutPaymentResponseDetailsType
#             DoExpressCheckoutPaymentResponseType
#             DoMobileCheckoutPaymentRequestType
#             ns3:DoMobileCheckoutPaymentResponseDetailsType
#             DoMobileCheckoutPaymentResponseType
#             ns3:DoNonReferencedCreditRequestDetailsType
#             DoNonReferencedCreditRequestType
#             ns3:DoNonReferencedCreditResponseDetailsType
#             DoNonReferencedCreditResponseType
#             DoReauthorizationRequestType
#             DoReauthorizationResponseType
#             ns3:DoReferenceTransactionRequestDetailsType
#             DoReferenceTransactionRequestType
#             ns3:DoReferenceTransactionResponseDetailsType
#             DoReferenceTransactionResponseType
#             DoUATPAuthorizationRequestType
#             DoUATPAuthorizationResponseType
#             DoUATPExpressCheckoutPaymentRequestType
#             DoUATPExpressCheckoutPaymentResponseType
#             DoVoidRequestType
#             DoVoidResponseType
#             ns3:DyneticClientType
#             ns3:EbayCheckoutType
#             ns3:EbayItemPaymentDetailsItemType
#             ns3:EmailAddressType
#             ns2:EnhancedCancelRecoupRequestDetailsType
#             ns2:EnhancedCheckoutDataType
#             ns2:EnhancedCompleteRecoupRequestDetailsType
#             ns2:EnhancedCompleteRecoupResponseDetailsType
#             ns3:EnhancedDataType
#             ns2:EnhancedInitiateRecoupRequestDetailsType
#             ns2:EnhancedItemDataType
#             ns2:EnhancedPayerInfoType
#             ns2:EnhancedPaymentDataType
#             ns2:EnhancedPaymentInfoType
#             ns3:EnterBoardingRequestDetailsType
#             EnterBoardingRequestType
#             EnterBoardingResponseType
#             ns3:EnterBoardingTokenType
#             ns3:ErrorParameterType
#             ns3:ErrorType
#             ns3:EscrowCodeType
#             ns3:ExecuteCheckoutOperationsRequestDetailsType
#             ExecuteCheckoutOperationsRequestType
#             ns3:ExecuteCheckoutOperationsResponseDetailsType
#             ExecuteCheckoutOperationsResponseType
#             ns3:ExpressCheckoutTokenType
#             ns3:ExternalPartnerTrackingDetailsType
#             ns3:ExternalRememberMeOptInDetailsType
#             ExternalRememberMeOptOutRequestType
#             ExternalRememberMeOptOutResponseType
#             ns3:ExternalRememberMeOwnerDetailsType
#             ns3:ExternalRememberMeStatusDetailsType
#             ns3:FMFDetailsType
#             ns3:FMFPendingTransactionActionType
#             ns3:FailedPaymentActionType
#             ns3:FaultDetailsType
#             ns3:FeeType
#             ns3:FeedbackRatingStarCodeType
#             ns3:FeesType
#             ns3:FlatShippingRateType
#             ns3:FlightDetailsType
#             ns3:FlowControlDetailsType
#             ns3:FundingSourceDetailsType
#             ns3:GalleryTypeCodeType
#             ns3:GeneralPaymentMethodCodeType
#             GetAccessPermissionDetailsRequestType
#             ns3:GetAccessPermissionDetailsResponseDetailsType
#             GetAccessPermissionDetailsResponseType
#             GetAuthDetailsRequestType
#             ns3:GetAuthDetailsResponseDetailsType
#             GetAuthDetailsResponseType
#             GetBalanceRequestType
#             GetBalanceResponseType
#             GetBillingAgreementCustomerDetailsRequestType
#             ns3:GetBillingAgreementCustomerDetailsResponseDetailsType
#             GetBillingAgreementCustomerDetailsResponseType
#             GetBoardingDetailsRequestType
#             ns3:GetBoardingDetailsResponseDetailsType
#             GetBoardingDetailsResponseType
#             GetExpressCheckoutDetailsRequestType
#             ns3:GetExpressCheckoutDetailsResponseDetailsType
#             GetExpressCheckoutDetailsResponseType
#             ns3:GetIncentiveEvaluationRequestDetailsType
#             GetIncentiveEvaluationRequestType
#             ns3:GetIncentiveEvaluationResponseDetailsType
#             GetIncentiveEvaluationResponseType
#             ns3:GetMobileStatusRequestDetailsType
#             GetMobileStatusRequestType
#             GetMobileStatusResponseType
#             GetPalDetailsRequestType
#             GetPalDetailsResponseType
#             GetRecurringPaymentsProfileDetailsRequestType
#             ns3:GetRecurringPaymentsProfileDetailsResponseDetailsType
#             GetRecurringPaymentsProfileDetailsResponseType
#             GetTransactionDetailsRequestType
#             GetTransactionDetailsResponseType
#             ns3:GiftServicesCodeType
#             ns3:HitCounterCodeType
#             ns3:IdentificationInfoType
#             ns3:IdentityTokenInfoType
#             ns3:IncentiveAppliedDetailsType
#             ns3:IncentiveAppliedStatusType
#             ns3:IncentiveAppliedToType
#             ns3:IncentiveApplyIndicationType
#             ns3:IncentiveBucketType
#             ns3:IncentiveDetailType
#             ns3:IncentiveDetailsType
#             ns3:IncentiveInfoType
#             ns3:IncentiveItemType
#             ns3:IncentiveRequestCodeType
#             ns3:IncentiveRequestDetailLevelCodeType
#             ns3:IncentiveRequestDetailsType
#             ns3:IncentiveSiteAppliedOnType
#             ns3:IncentiveTypeCodeType
#             ns3:InfoSharingDirectivesType
#             InitiateRecoupRequestType
#             InitiateRecoupResponseType
#             InstallmentDetailsType
#             ns3:InstrumentDetailsType
#             ns3:InsuranceOptionCodeType
#             ns3:InvoiceItemType
#             ns3:ItemArrayType
#             ns3:ItemCategoryType
#             ns3:ItemIDType
#             ns3:ItemTrackingDetailsType
#             ns3:ItemType
#             ns3:LandingPageType
#             ns3:LanguageCodeType
#             ns3:ListOfAttributeSetType
#             ns3:ListingDesignerType
#             ns3:ListingDetailsType
#             ns3:ListingDurationCodeType
#             ns3:ListingEnhancementsCodeType
#             ns3:ListingTypeCodeType
#             ns3:LocationType
#             ManagePendingTransactionStatusRequestType
#             ManagePendingTransactionStatusResponseType
#             ns3:ManageRecurringPaymentsProfileStatusRequestDetailsType
#             ManageRecurringPaymentsProfileStatusRequestType
#             ns3:ManageRecurringPaymentsProfileStatusResponseDetailsType
#             ManageRecurringPaymentsProfileStatusResponseType
#             ns3:MarketingCategoryType
#             MassPayRequestItemType
#             MassPayRequestType
#             MassPayResponseType
#             ns3:MatchStatusCodeType
#             ns1:MeasureType
#             ns3:MerchandizingPrefCodeType
#             ns3:MerchantDataType
#             ns3:MerchantPullIDType
#             ns3:MerchantPullInfoType
#             ns3:MerchantPullPaymentCodeType
#             ns3:MerchantPullPaymentResponseType
#             ns3:MerchantPullPaymentType
#             ns3:MerchantPullStatusCodeType
#             ns3:MerchantStoreDetailsType
#             ns3:MobileIDInfoType
#             ns3:MobilePaymentCodeType
#             ns3:MobileRecipientCodeType
#             ns3:ModifiedFieldType
#             ns3:ModifyCodeType
#             ns3:NameType
#             ns3:OfferCouponInfoType
#             ns3:OfferDetailsType
#             OptionDetailsType
#             OptionSelectionDetailsType
#             ns3:OptionTrackingDetailsType
#             ns3:OptionType
#             ns3:OptionTypeListType
#             ns3:OrderDetailsType
#             ns3:OrderID
#             ns3:OtherPaymentMethodDetailsType
#             ns3:POSTransactionCodeType
#             ns3:PaginationResultType
#             ns3:PaginationType
#             ns3:PayPalUserStatusCodeType
#             ns3:PayerInfoType
#             ns3:PaymentActionCodeType
#             ns3:PaymentCategoryType
#             ns3:PaymentCodeType
#             ns3:PaymentDetailsItemType
#             ns3:PaymentDetailsType
#             ns3:PaymentDirectivesType
#             ns3:PaymentInfoType
#             ns3:PaymentItemInfoType
#             ns3:PaymentItemType
#             ns3:PaymentMeansType
#             ns3:PaymentNotificationServiceCodeType
#             ns3:PaymentReasonType
#             ns3:PaymentRequestInfoType
#             ns3:PaymentStatusCodeType
#             ns3:PaymentStatusIDCodeType
#             ns3:PaymentTransactionClassCodeType
#             ns3:PaymentTransactionCodeType
#             ns3:PaymentTransactionSearchResultType
#             ns3:PaymentTransactionStatusCodeType
#             ns3:PaymentTransactionType
#             ns3:PaymentType
#             ns3:PendingStatusCodeType
#             ns3:PercentageRevenueFromOnlineSalesType
#             ns3:PersonNameType
#             ns3:PhoneNumberType
#             ns3:PhotoDisplayCodeType
#             ns3:ProductCategoryType
#             ns3:PromotedItemType
#             ns3:PromotionItemPriceTypeCodeType
#             ns3:PromotionItemSelectionCodeType
#             ns3:PromotionMethodCodeType
#             ns3:PromotionSchemeCodeType
#             ns3:PurchasePurposeTypeCodeType
#             ns1:QuantityType
#             ns3:ReceiverInfoCodeType
#             ns3:ReceiverInfoType
#             ns3:RecurringFlagType
#             ns3:RecurringPaymentsProfileDetailsType
#             ns3:RecurringPaymentsProfileStatusType
#             ns3:RecurringPaymentsSummaryType
#             ns3:RedeemedOfferType
#             ns3:ReferenceCreditCardDetailsType
#             ns3:RefreshTokenStatusDetailsType
#             ns3:RefundInfoType
#             ns3:RefundSourceCodeType
#             RefundTransactionRequestType
#             RefundTransactionResponseType
#             ns3:RefundType
#             ns3:RememberMeIDInfoType
#             ns3:ReversalReasonCodeType
#             ns3:ReverseTransactionRequestDetailsType
#             ReverseTransactionRequestType
#             ns3:ReverseTransactionResponseDetailsType
#             ReverseTransactionResponseType
#             ns3:ReviseStatusType
#             ns3:RiskFilterDetailsType
#             ns3:RiskFilterListType
#             ns3:SalesTaxType
#             ns3:SalesVenueType
#             ns3:SalutationType
#             ns3:ScheduleDetailsType
#             ns3:SchedulingInfoType
#             ns3:SellerDetailsType
#             ns3:SellerLevelCodeType
#             ns3:SellerPaymentMethodCodeType
#             ns3:SellerType
#             ns3:SellingStatusType
#             ns3:SenderDetailsType
#             ns3:SetAccessPermissionsRequestDetailsType
#             SetAccessPermissionsRequestType
#             SetAccessPermissionsResponseType
#             ns3:SetAuthFlowParamRequestDetailsType
#             SetAuthFlowParamRequestType
#             SetAuthFlowParamResponseType
#             ns3:SetCustomerBillingAgreementRequestDetailsType
#             SetCustomerBillingAgreementRequestType
#             SetCustomerBillingAgreementResponseType
#             ns3:SetDataRequestType
#             ns3:SetDataResponseType
#             ns3:SetEbayMobileCheckoutRequestDetailsType
#             ns3:SetExpressCheckoutRequestDetailsType
#             SetExpressCheckoutRequestType
#             SetExpressCheckoutResponseType
#             ns3:SetMobileCheckoutRequestDetailsType
#             SetMobileCheckoutRequestType
#             SetMobileCheckoutResponseType
#             ns3:SeverityCodeType
#             ns3:ShippingCarrierDetailsType
#             ns3:ShippingDetailsType
#             ns3:ShippingInfoType
#             ns3:ShippingOptionCodeType
#             ns3:ShippingOptionType
#             ns3:ShippingPackageCodeType
#             ns3:ShippingRatesTypeCodeType
#             ns3:ShippingRegionCodeType
#             ns3:ShippingServiceCodeType
#             ns3:ShippingTermsCodeType
#             ns3:SiteCodeType
#             ns3:SiteHostedPictureType
#             ns3:SolutionTypeType
#             ns3:StatusChangeActionType
#             ns3:StoreVisibilityStatusType
#             ns3:StorefrontType
#             ns3:SubscribeTextType
#             ns3:SubscriptionInfoType
#             ns3:SubscriptionTermsType
#             ns3:SuffixType
#             ns3:TaxIdDetailsType
#             ns3:TaxInfoType
#             ns3:ThreeDSecureInfoType
#             ns3:ThreeDSecureRequestType
#             ns3:ThreeDSecureResponseType
#             ns3:TotalType
#             ns3:TransactionEntityType
#             ns3:TransactionId
#             TransactionSearchRequestType
#             TransactionSearchResponseType
#             ns3:TransactionStatusType
#             ns3:TransactionType
#             ns3:TransactionsType
#             ns3:TupleType
#             ns3:UATPDetailsType
#             ns1:UUIDType
#             ns3:UnitCodeType
#             ns3:UnitOfMeasure
#             UpdateAccessPermissionsRequestType
#             UpdateAccessPermissionsResponseType
#             UpdateAuthorizationRequestType
#             UpdateAuthorizationResponseType
#             ns3:UpdateRecurringPaymentsProfileRequestDetailsType
#             UpdateRecurringPaymentsProfileRequestType
#             ns3:UpdateRecurringPaymentsProfileResponseDetailsType
#             UpdateRecurringPaymentsProfileResponseType
#             ns3:UserChannelCodeType
#             ns3:UserIDType
#             ns3:UserIdPasswordType
#             ns3:UserSelectedFundingSourceType
#             ns3:UserSelectedOptionType
#             ns3:UserStatusCodeType
#             ns3:UserType
#             ns3:UserWithdrawalLimitTypeType
#             ns3:VATDetailsType
#             ns3:VATStatusCodeType
#             ns3:ValType
#             ns3:VendorHostedPictureType
#             ns3:WalletItemType
#             ns3:WalletItemsType
#       (PayPalAPIAA)
#          Methods (32):
#             BillOutstandingAmount(BillOutstandingAmountRequestType BillOutstandingAmountRequest, )
#             CreateBillingAgreement(CreateBillingAgreementRequestType CreateBillingAgreementRequest, )
#             CreateRecurringPaymentsProfile(CreateRecurringPaymentsProfileRequestType CreateRecurringPaymentsProfileRequest, )
#             DoAuthorization(DoAuthorizationRequestType DoAuthorizationRequest, )
#             DoCancel(DoCancelRequestType DoCancelRequest, )
#             DoCapture(DoCaptureRequestType DoCaptureRequest, )
#             DoDirectPayment(DoDirectPaymentRequestType DoDirectPaymentRequest, )
#             DoExpressCheckoutPayment(DoExpressCheckoutPaymentRequestType DoExpressCheckoutPaymentRequest, )
#             DoNonReferencedCredit(DoNonReferencedCreditRequestType DoNonReferencedCreditRequest, )
#             DoReauthorization(DoReauthorizationRequestType DoReauthorizationRequest, )
#             DoReferenceTransaction(DoReferenceTransactionRequestType DoReferenceTransactionRequest, )
#             DoUATPAuthorization(DoUATPAuthorizationRequestType DoUATPAuthorizationRequest, )
#             DoUATPExpressCheckoutPayment(DoUATPExpressCheckoutPaymentRequestType DoUATPExpressCheckoutPaymentRequest, )
#             DoVoid(DoVoidRequestType DoVoidRequest, )
#             ExecuteCheckoutOperations(ExecuteCheckoutOperationsRequestType ExecuteCheckoutOperationsRequest, )
#             ExternalRememberMeOptOut(ExternalRememberMeOptOutRequestType ExternalRememberMeOptOutRequest, )
#             GetAccessPermissionDetails(GetAccessPermissionDetailsRequestType GetAccessPermissionDetailsRequest, )
#             GetAuthDetails(GetAuthDetailsRequestType GetAuthDetailsRequest, )
#             GetBillingAgreementCustomerDetails(GetBillingAgreementCustomerDetailsRequestType GetBillingAgreementCustomerDetailsRequest, )
#             GetExpressCheckoutDetails(GetExpressCheckoutDetailsRequestType GetExpressCheckoutDetailsRequest, )
#             GetIncentiveEvaluation(GetIncentiveEvaluationRequestType GetIncentiveEvaluationRequest, )
#             GetRecurringPaymentsProfileDetails(GetRecurringPaymentsProfileDetailsRequestType GetRecurringPaymentsProfileDetailsRequest, )
#             ManagePendingTransactionStatus(ManagePendingTransactionStatusRequestType ManagePendingTransactionStatusRequest, )
#             ManageRecurringPaymentsProfileStatus(ManageRecurringPaymentsProfileStatusRequestType ManageRecurringPaymentsProfileStatusRequest, )
#             ReverseTransaction(ReverseTransactionRequestType ReverseTransactionRequest, )
#             SetAccessPermissions(SetAccessPermissionsRequestType SetAccessPermissionsRequest, )
#             SetAuthFlowParam(SetAuthFlowParamRequestType SetAuthFlowParamRequest, )
#             SetCustomerBillingAgreement(SetCustomerBillingAgreementRequestType SetCustomerBillingAgreementRequest, )
#             SetExpressCheckout(SetExpressCheckoutRequestType SetExpressCheckoutRequest, )
#             UpdateAccessPermissions(UpdateAccessPermissionsRequestType UpdateAccessPermissionsRequest, )
#             UpdateAuthorization(UpdateAuthorizationRequestType UpdateAuthorizationRequest, )
#             UpdateRecurringPaymentsProfile(UpdateRecurringPaymentsProfileRequestType UpdateRecurringPaymentsProfileRequest, )
#          Types (461):
#             ns3:APIAuthenticationType
#             ns3:APICredentialsType
#             ns3:APIType
#             ns3:AbstractRequestType
#             ns3:AbstractResponseType
#             ns3:AccountEntryType
#             ns3:AccountStateCodeType
#             ns3:AccountSummaryType
#             ns3:AckCodeType
#             ns3:ActivationDetailsType
#             ns3:AdditionalAccountType
#             ns3:AdditionalFeeType
#             ns3:AddressNormalizationStatusCodeType
#             ns3:AddressOwnerCodeType
#             ns3:AddressStatusCodeType
#             ns3:AddressType
#             AddressVerifyRequestType
#             AddressVerifyResponseType
#             ns3:AirlineItineraryType
#             ns3:AllowedPaymentMethodType
#             ns1:AmountType
#             ns3:ApprovalSubTypeType
#             ns3:ApprovalTypeType
#             ns3:AttributeSetType
#             ns3:AttributeType
#             ns3:AuctionInfoType
#             ns3:AuctionTypeCodeType
#             ns3:AuthFlowTokenType
#             ns3:AuthorizationId
#             ns3:AuthorizationInfoType
#             ns3:AuthorizationRequestType
#             ns3:AuthorizationResponseType
#             ns3:AutoBillType
#             ns3:AverageMonthlyVolumeType
#             ns3:AverageTransactionPriceType
#             BAUpdateRequestType
#             ns3:BAUpdateResponseDetailsType
#             BAUpdateResponseType
#             BMButtonSearchRequestType
#             BMButtonSearchResponseType
#             BMCreateButtonRequestType
#             BMCreateButtonResponseType
#             BMGetButtonDetailsRequestType
#             BMGetButtonDetailsResponseType
#             BMGetInventoryRequestType
#             BMGetInventoryResponseType
#             ns3:BMLOfferInfoType
#             BMManageButtonStatusRequestType
#             BMManageButtonStatusResponseType
#             BMSetInventoryRequestType
#             BMSetInventoryResponseType
#             BMUpdateButtonRequestType
#             BMUpdateButtonResponseType
#             ns3:BalanceCodeType
#             ns3:BankAccountDetailsType
#             ns3:BankAccountTypeType
#             ns3:BankIDCodeType
#             ns1:BasicAmountType
#             ns3:BillOutstandingAmountRequestDetailsType
#             BillOutstandingAmountRequestType
#             ns3:BillOutstandingAmountResponseDetailsType
#             BillOutstandingAmountResponseType
#             BillUserRequestType
#             BillUserResponseType
#             ns3:BillingAgreementDetailsType
#             ns3:BillingApprovalDetailsType
#             ns3:BillingCodeType
#             ns3:BillingPeriodDetailsType
#             ns3:BillingPeriodDetailsType_Update
#             ns3:BillingPeriodType
#             ns3:BoardingStatusType
#             ns3:BusinessCategoryType
#             ns3:BusinessInfoType
#             ns3:BusinessOwnerInfoType
#             ns3:BusinessSubCategoryType
#             ns3:BusinessTypeType
#             ns3:ButtonCodeType
#             ns3:ButtonImageType
#             ns3:ButtonSearchResultType
#             ns3:ButtonStatusType
#             ns3:ButtonSubTypeType
#             ns3:ButtonTypeType
#             ns3:BuyNowTextType
#             ns3:BuyerDetailType
#             ns3:BuyerDetailsType
#             ns3:BuyerPaymentMethodCodeType
#             ns3:BuyerProtectionCodeType
#             ns3:BuyerType
#             ns3:CalculatedShippingRateType
#             CancelRecoupRequestType
#             CancelRecoupResponseType
#             ns3:CategoryArrayType
#             ns3:CategoryType
#             ns3:ChannelType
#             ns3:CharityType
#             ns3:CheckoutStatusCodeType
#             ns3:CompleteCodeType
#             CompleteRecoupRequestType
#             CompleteRecoupResponseType
#             ns3:CountryCodeType
#             ns3:CoupleType
#             ns3:CoupledBucketsType
#             ns3:CoupledPaymentInfoType
#             CreateBillingAgreementRequestType
#             CreateBillingAgreementResponseType
#             ns3:CreateMobilePaymentRequestDetailsType
#             CreateMobilePaymentRequestType
#             CreateMobilePaymentResponseType
#             ns3:CreateRecurringPaymentsProfileRequestDetailsType
#             CreateRecurringPaymentsProfileRequestType
#             ns3:CreateRecurringPaymentsProfileResponseDetailsType
#             CreateRecurringPaymentsProfileResponseType
#             ns3:CreditCardDetailsType
#             ns3:CreditCardNumberTypeType
#             ns3:CreditCardTypeType
#             ns3:CrossPromotionsType
#             ns3:CurrencyCodeType
#             ns3:CustomSecurityHeaderType
#             ns3:DepositTypeCodeType
#             ns3:DetailLevelCodeType
#             ns3:DeviceDetailsType
#             ns3:DiscountInfoType
#             ns3:DiscountType
#             ns3:DisplayControlDetailsType
#             DoAuthorizationRequestType
#             DoAuthorizationResponseType
#             DoCancelRequestType
#             DoCancelResponseType
#             DoCaptureRequestType
#             ns3:DoCaptureResponseDetailsType
#             DoCaptureResponseType
#             ns3:DoDirectPaymentRequestDetailsType
#             DoDirectPaymentRequestType
#             DoDirectPaymentResponseType
#             ns3:DoExpressCheckoutPaymentRequestDetailsType
#             DoExpressCheckoutPaymentRequestType
#             ns3:DoExpressCheckoutPaymentResponseDetailsType
#             DoExpressCheckoutPaymentResponseType
#             DoMobileCheckoutPaymentRequestType
#             ns3:DoMobileCheckoutPaymentResponseDetailsType
#             DoMobileCheckoutPaymentResponseType
#             ns3:DoNonReferencedCreditRequestDetailsType
#             DoNonReferencedCreditRequestType
#             ns3:DoNonReferencedCreditResponseDetailsType
#             DoNonReferencedCreditResponseType
#             DoReauthorizationRequestType
#             DoReauthorizationResponseType
#             ns3:DoReferenceTransactionRequestDetailsType
#             DoReferenceTransactionRequestType
#             ns3:DoReferenceTransactionResponseDetailsType
#             DoReferenceTransactionResponseType
#             DoUATPAuthorizationRequestType
#             DoUATPAuthorizationResponseType
#             DoUATPExpressCheckoutPaymentRequestType
#             DoUATPExpressCheckoutPaymentResponseType
#             DoVoidRequestType
#             DoVoidResponseType
#             ns3:DyneticClientType
#             ns3:EbayCheckoutType
#             ns3:EbayItemPaymentDetailsItemType
#             ns3:EmailAddressType
#             ns2:EnhancedCancelRecoupRequestDetailsType
#             ns2:EnhancedCheckoutDataType
#             ns2:EnhancedCompleteRecoupRequestDetailsType
#             ns2:EnhancedCompleteRecoupResponseDetailsType
#             ns3:EnhancedDataType
#             ns2:EnhancedInitiateRecoupRequestDetailsType
#             ns2:EnhancedItemDataType
#             ns2:EnhancedPayerInfoType
#             ns2:EnhancedPaymentDataType
#             ns2:EnhancedPaymentInfoType
#             ns3:EnterBoardingRequestDetailsType
#             EnterBoardingRequestType
#             EnterBoardingResponseType
#             ns3:EnterBoardingTokenType
#             ns3:ErrorParameterType
#             ns3:ErrorType
#             ns3:EscrowCodeType
#             ns3:ExecuteCheckoutOperationsRequestDetailsType
#             ExecuteCheckoutOperationsRequestType
#             ns3:ExecuteCheckoutOperationsResponseDetailsType
#             ExecuteCheckoutOperationsResponseType
#             ns3:ExpressCheckoutTokenType
#             ns3:ExternalPartnerTrackingDetailsType
#             ns3:ExternalRememberMeOptInDetailsType
#             ExternalRememberMeOptOutRequestType
#             ExternalRememberMeOptOutResponseType
#             ns3:ExternalRememberMeOwnerDetailsType
#             ns3:ExternalRememberMeStatusDetailsType
#             ns3:FMFDetailsType
#             ns3:FMFPendingTransactionActionType
#             ns3:FailedPaymentActionType
#             ns3:FaultDetailsType
#             ns3:FeeType
#             ns3:FeedbackRatingStarCodeType
#             ns3:FeesType
#             ns3:FlatShippingRateType
#             ns3:FlightDetailsType
#             ns3:FlowControlDetailsType
#             ns3:FundingSourceDetailsType
#             ns3:GalleryTypeCodeType
#             ns3:GeneralPaymentMethodCodeType
#             GetAccessPermissionDetailsRequestType
#             ns3:GetAccessPermissionDetailsResponseDetailsType
#             GetAccessPermissionDetailsResponseType
#             GetAuthDetailsRequestType
#             ns3:GetAuthDetailsResponseDetailsType
#             GetAuthDetailsResponseType
#             GetBalanceRequestType
#             GetBalanceResponseType
#             GetBillingAgreementCustomerDetailsRequestType
#             ns3:GetBillingAgreementCustomerDetailsResponseDetailsType
#             GetBillingAgreementCustomerDetailsResponseType
#             GetBoardingDetailsRequestType
#             ns3:GetBoardingDetailsResponseDetailsType
#             GetBoardingDetailsResponseType
#             GetExpressCheckoutDetailsRequestType
#             ns3:GetExpressCheckoutDetailsResponseDetailsType
#             GetExpressCheckoutDetailsResponseType
#             ns3:GetIncentiveEvaluationRequestDetailsType
#             GetIncentiveEvaluationRequestType
#             ns3:GetIncentiveEvaluationResponseDetailsType
#             GetIncentiveEvaluationResponseType
#             ns3:GetMobileStatusRequestDetailsType
#             GetMobileStatusRequestType
#             GetMobileStatusResponseType
#             GetPalDetailsRequestType
#             GetPalDetailsResponseType
#             GetRecurringPaymentsProfileDetailsRequestType
#             ns3:GetRecurringPaymentsProfileDetailsResponseDetailsType
#             GetRecurringPaymentsProfileDetailsResponseType
#             GetTransactionDetailsRequestType
#             GetTransactionDetailsResponseType
#             ns3:GiftServicesCodeType
#             ns3:HitCounterCodeType
#             ns3:IdentificationInfoType
#             ns3:IdentityTokenInfoType
#             ns3:IncentiveAppliedDetailsType
#             ns3:IncentiveAppliedStatusType
#             ns3:IncentiveAppliedToType
#             ns3:IncentiveApplyIndicationType
#             ns3:IncentiveBucketType
#             ns3:IncentiveDetailType
#             ns3:IncentiveDetailsType
#             ns3:IncentiveInfoType
#             ns3:IncentiveItemType
#             ns3:IncentiveRequestCodeType
#             ns3:IncentiveRequestDetailLevelCodeType
#             ns3:IncentiveRequestDetailsType
#             ns3:IncentiveSiteAppliedOnType
#             ns3:IncentiveTypeCodeType
#             ns3:InfoSharingDirectivesType
#             InitiateRecoupRequestType
#             InitiateRecoupResponseType
#             InstallmentDetailsType
#             ns3:InstrumentDetailsType
#             ns3:InsuranceOptionCodeType
#             ns3:InvoiceItemType
#             ns3:ItemArrayType
#             ns3:ItemCategoryType
#             ns3:ItemIDType
#             ns3:ItemTrackingDetailsType
#             ns3:ItemType
#             ns3:LandingPageType
#             ns3:LanguageCodeType
#             ns3:ListOfAttributeSetType
#             ns3:ListingDesignerType
#             ns3:ListingDetailsType
#             ns3:ListingDurationCodeType
#             ns3:ListingEnhancementsCodeType
#             ns3:ListingTypeCodeType
#             ns3:LocationType
#             ManagePendingTransactionStatusRequestType
#             ManagePendingTransactionStatusResponseType
#             ns3:ManageRecurringPaymentsProfileStatusRequestDetailsType
#             ManageRecurringPaymentsProfileStatusRequestType
#             ns3:ManageRecurringPaymentsProfileStatusResponseDetailsType
#             ManageRecurringPaymentsProfileStatusResponseType
#             ns3:MarketingCategoryType
#             MassPayRequestItemType
#             MassPayRequestType
#             MassPayResponseType
#             ns3:MatchStatusCodeType
#             ns1:MeasureType
#             ns3:MerchandizingPrefCodeType
#             ns3:MerchantDataType
#             ns3:MerchantPullIDType
#             ns3:MerchantPullInfoType
#             ns3:MerchantPullPaymentCodeType
#             ns3:MerchantPullPaymentResponseType
#             ns3:MerchantPullPaymentType
#             ns3:MerchantPullStatusCodeType
#             ns3:MerchantStoreDetailsType
#             ns3:MobileIDInfoType
#             ns3:MobilePaymentCodeType
#             ns3:MobileRecipientCodeType
#             ns3:ModifiedFieldType
#             ns3:ModifyCodeType
#             ns3:NameType
#             ns3:OfferCouponInfoType
#             ns3:OfferDetailsType
#             OptionDetailsType
#             OptionSelectionDetailsType
#             ns3:OptionTrackingDetailsType
#             ns3:OptionType
#             ns3:OptionTypeListType
#             ns3:OrderDetailsType
#             ns3:OrderID
#             ns3:OtherPaymentMethodDetailsType
#             ns3:POSTransactionCodeType
#             ns3:PaginationResultType
#             ns3:PaginationType
#             ns3:PayPalUserStatusCodeType
#             ns3:PayerInfoType
#             ns3:PaymentActionCodeType
#             ns3:PaymentCategoryType
#             ns3:PaymentCodeType
#             ns3:PaymentDetailsItemType
#             ns3:PaymentDetailsType
#             ns3:PaymentDirectivesType
#             ns3:PaymentInfoType
#             ns3:PaymentItemInfoType
#             ns3:PaymentItemType
#             ns3:PaymentMeansType
#             ns3:PaymentNotificationServiceCodeType
#             ns3:PaymentReasonType
#             ns3:PaymentRequestInfoType
#             ns3:PaymentStatusCodeType
#             ns3:PaymentStatusIDCodeType
#             ns3:PaymentTransactionClassCodeType
#             ns3:PaymentTransactionCodeType
#             ns3:PaymentTransactionSearchResultType
#             ns3:PaymentTransactionStatusCodeType
#             ns3:PaymentTransactionType
#             ns3:PaymentType
#             ns3:PendingStatusCodeType
#             ns3:PercentageRevenueFromOnlineSalesType
#             ns3:PersonNameType
#             ns3:PhoneNumberType
#             ns3:PhotoDisplayCodeType
#             ns3:ProductCategoryType
#             ns3:PromotedItemType
#             ns3:PromotionItemPriceTypeCodeType
#             ns3:PromotionItemSelectionCodeType
#             ns3:PromotionMethodCodeType
#             ns3:PromotionSchemeCodeType
#             ns3:PurchasePurposeTypeCodeType
#             ns1:QuantityType
#             ns3:ReceiverInfoCodeType
#             ns3:ReceiverInfoType
#             ns3:RecurringFlagType
#             ns3:RecurringPaymentsProfileDetailsType
#             ns3:RecurringPaymentsProfileStatusType
#             ns3:RecurringPaymentsSummaryType
#             ns3:RedeemedOfferType
#             ns3:ReferenceCreditCardDetailsType
#             ns3:RefreshTokenStatusDetailsType
#             ns3:RefundInfoType
#             ns3:RefundSourceCodeType
#             RefundTransactionRequestType
#             RefundTransactionResponseType
#             ns3:RefundType
#             ns3:RememberMeIDInfoType
#             ns3:ReversalReasonCodeType
#             ns3:ReverseTransactionRequestDetailsType
#             ReverseTransactionRequestType
#             ns3:ReverseTransactionResponseDetailsType
#             ReverseTransactionResponseType
#             ns3:ReviseStatusType
#             ns3:RiskFilterDetailsType
#             ns3:RiskFilterListType
#             ns3:SalesTaxType
#             ns3:SalesVenueType
#             ns3:SalutationType
#             ns3:ScheduleDetailsType
#             ns3:SchedulingInfoType
#             ns3:SellerDetailsType
#             ns3:SellerLevelCodeType
#             ns3:SellerPaymentMethodCodeType
#             ns3:SellerType
#             ns3:SellingStatusType
#             ns3:SenderDetailsType
#             ns3:SetAccessPermissionsRequestDetailsType
#             SetAccessPermissionsRequestType
#             SetAccessPermissionsResponseType
#             ns3:SetAuthFlowParamRequestDetailsType
#             SetAuthFlowParamRequestType
#             SetAuthFlowParamResponseType
#             ns3:SetCustomerBillingAgreementRequestDetailsType
#             SetCustomerBillingAgreementRequestType
#             SetCustomerBillingAgreementResponseType
#             ns3:SetDataRequestType
#             ns3:SetDataResponseType
#             ns3:SetEbayMobileCheckoutRequestDetailsType
#             ns3:SetExpressCheckoutRequestDetailsType
#             SetExpressCheckoutRequestType
#             SetExpressCheckoutResponseType
#             ns3:SetMobileCheckoutRequestDetailsType
#             SetMobileCheckoutRequestType
#             SetMobileCheckoutResponseType
#             ns3:SeverityCodeType
#             ns3:ShippingCarrierDetailsType
#             ns3:ShippingDetailsType
#             ns3:ShippingInfoType
#             ns3:ShippingOptionCodeType
#             ns3:ShippingOptionType
#             ns3:ShippingPackageCodeType
#             ns3:ShippingRatesTypeCodeType
#             ns3:ShippingRegionCodeType
#             ns3:ShippingServiceCodeType
#             ns3:ShippingTermsCodeType
#             ns3:SiteCodeType
#             ns3:SiteHostedPictureType
#             ns3:SolutionTypeType
#             ns3:StatusChangeActionType
#             ns3:StoreVisibilityStatusType
#             ns3:StorefrontType
#             ns3:SubscribeTextType
#             ns3:SubscriptionInfoType
#             ns3:SubscriptionTermsType
#             ns3:SuffixType
#             ns3:TaxIdDetailsType
#             ns3:TaxInfoType
#             ns3:ThreeDSecureInfoType
#             ns3:ThreeDSecureRequestType
#             ns3:ThreeDSecureResponseType
#             ns3:TotalType
#             ns3:TransactionEntityType
#             ns3:TransactionId
#             TransactionSearchRequestType
#             TransactionSearchResponseType
#             ns3:TransactionStatusType
#             ns3:TransactionType
#             ns3:TransactionsType
#             ns3:TupleType
#             ns3:UATPDetailsType
#             ns1:UUIDType
#             ns3:UnitCodeType
#             ns3:UnitOfMeasure
#             UpdateAccessPermissionsRequestType
#             UpdateAccessPermissionsResponseType
#             UpdateAuthorizationRequestType
#             UpdateAuthorizationResponseType
#             ns3:UpdateRecurringPaymentsProfileRequestDetailsType
#             UpdateRecurringPaymentsProfileRequestType
#             ns3:UpdateRecurringPaymentsProfileResponseDetailsType
#             UpdateRecurringPaymentsProfileResponseType
#             ns3:UserChannelCodeType
#             ns3:UserIDType
#             ns3:UserIdPasswordType
#             ns3:UserSelectedFundingSourceType
#             ns3:UserSelectedOptionType
#             ns3:UserStatusCodeType
#             ns3:UserType
#             ns3:UserWithdrawalLimitTypeType
#             ns3:VATDetailsType
#             ns3:VATStatusCodeType
#             ns3:ValType
#             ns3:VendorHostedPictureType
#             ns3:WalletItemType
#             ns3:WalletItemsType
#
#
#
